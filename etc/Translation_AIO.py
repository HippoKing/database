import os,json

PATH_ASSETS=os.path.join(os.path.realpath(__file__),*[os.pardir,'assets'])
dest = os.path.join(os.path.realpath(__file__),*[os.pardir,'res','Translation'])
os.makedirs(dest,exist_ok=True)

def main():
	ss = Spreadsheet(os.path.join(dest,'Translation.xlsx'))
	p_cn = os.path.join(PATH_ASSETS,*['china','ExportAssetBundle','configdata','china'])
	p_gl = os.path.join(PATH_ASSETS,*['global','ExportAssetBundle','configdata','global'])
	for fp in os.listdir(p_cn):
		f_cn = os.path.join(p_cn,fp)
		f_gl = os.path.join(p_gl, fp)
		if not os.path.isfile(f_cn) or not os.path.isfile(f_gl) or fp[:10]!='ConfigData' or fp[-9:]!='Info.json':
			continue
		
		data = {
			item['ID']:FilterStrings(item)
			for item in json.loads(open(f_cn,'rb').read())
		}
		for item in json.loads(open(f_gl,'rb').read()):
			if item['ID'] in data:
				data[item['ID']].update(
					{'%s_gl'%key:val for key,val in FilterStrings(item).items()}
				)
			else:
				data[item['ID']] = {'%s_gl'%key:val for key,val in FilterStrings(item).items()}
				
		if data:
			#open(os.path.join(dest,fp[10:-9]+'.json'),'wb').write(json.dumps(data,ensure_ascii=False,indent='\t').encode('utf8'))
			try:
				ss.add_sheet(fp[10:-9],LocDicToArray(data))
			except Exception as e:
				print(e)
	ss.save()

def FilterStrings(item):
	return {key:val for key,val in item.items() if type(val) == str and '_' not in val}

def LocDicToArray(obj):
	headers=['system',*list(obj[list(obj.keys())[0]].keys())]
	array=[
		[
			key,*[
				entry[lang] if lang in entry else ''
				for lang in headers[1:]
			]
		]
		for key,entry in obj.items()
	]
	array.insert(0,headers)
	return array

class Spreadsheet():
	def __init__(self,fp = ''):
		import xlsxwriter
		# Create a workbook and add a worksheet.
		self.book = xlsxwriter.Workbook(fp)
		self.fp = fp

	def add_sheet(self,name,data=None):
		self.book.add_worksheet(name)
		if data:
			self.add_data(name,data)

	def add_data(self,sheet,data):
		sh = self.book.get_worksheet_by_name(sheet)
		for y,row in enumerate(data):
			for x,text in enumerate(row):
				sh.write(y, x, text)

	def save(self,filename=''):
		self.book.close()

main()