import json
from _main import LoadConfig, SaveOut, IfAddText

	# IsDebuff	~	true ~ can be dispelled
	# IsEnhance	~
	# Time	~	buff duration in turns
	# Cond_HP_Target	~	3-troops,	2~hero,	1~all	#	has to fullfill condition?
	# Cond_HP_Operator	~	3-troops,	2~hero,	1~all	#	receives the effect?
	# Cond_HP_Value		~	/100 = %					#	value for the hp condition
	# ConditionType		~	enum						#	type of condition for param	
	# ConditionParam	~	array	of condition		#	params for the condition type	(specific for every type)	
	# BuffType			~								#	buff/effect type
	# BuffTypeParamX	~								#	params for the buff type
	# SelfNoExtraTime	~								#	true ~	another turn after action
	# SubType			~	number, orig. type			#	buff type for replacement check
	# ReplaceRule		~	2,3							#	number of possible stacks
	# ReplacePriority	~								#	??? higher priotity replaces lower priority ???
	# FightTags			~	array of enum				#	battle debuffs, stun, no guard, and so on
	# PropertyX_ID		~	enum, hero1/2, troop 1/2	#	stat which will be changed and how Add/Mul
	# PropertyX_Value	~	Mul -> /100 for %			#	degree of the change
	# CDBuff_ID			~	links to other buff			#	cooldown buff

def Buff(buff):
	text=''
	# IsDebuff	~
	# IsEnhance	~

	# Cond_HP_Target	~	3-troops,	2~hero,	1~all	#	has to fullfill condition?
	# Cond_HP_Operator	~	3-==	2~>,	1~<			#	receives the effect?
	# Cond_HP_Value		~	/100 = %					#	value for the hp condition
	if buff["CondHPTarget"] and buff["CondHPOperator"]:
		text+='When %s HP is %s %s%%, '%(
			CondHPTarget[buff["CondHPTarget"]],
			CondHPOperator[buff["CondHPOperator"]],
			round(buff['CondHPValue']/100)
		)

	# ConditionType		~	enum						#	type of condition for param	
	# ConditionParam	~	array	of condition		#	params for the condition type	(specific for every type)


	# SubType			~	number, orig. type			#	buff type for replacement check
	# ReplacePriority	~								#	??? higher priotity replaces lower priority ???
	# CDBuff_ID			~	links to other buff			#	cooldown buff

	# BuffType			~								#	buff/effect type
	# BuffTypeParamX	~								#	params for the buff type


	# PropertyX_ID		~	enum, hero1/2, troop 1/2	#	stat which will be changed and how Add/Mul
	# PropertyX_Value	~	Mul -> /100 for %			#	degree of the change
	pid = 	'Property%sID'
	pv	=	'Property%sValue'
	pbuffs=[]
	for i in range(1,7):
		if pid%i in buff and buff[pv%i] and buff[pid%i][-4:]!='None':
			p,target,stat=buff[pid%i][:-3].split('_')
			typ=buff[pid%i][-3:]

			pbuffs.append('%s%s %s (%s)'%(
				'+' if buff[pv%i]>0 else '',
				'%s%%'%(str(buff[pv%i]/100).split('.',1)[0]) if typ == 'Mul' or stat[:4]=='Crit' else buff[pv%i],
				PROPERTIES[stat],
				target.title()
				)
			)
	text+= ', '.join(pbuffs)

	# Time	~	buff duration in turns
	text+= IfAddText(buff,'Time',' for %s turns').replace(' for 999 turns','')
	text+='.\n'

	# ReplaceRule		~	2,3							#	number of possible stacks
	text+= IfAddText(buff, 'ReplaceRule',	'Can stack up to %s times.')

	# FightTags			~	array of enum				#	battle debuffs, stun, no guard, and so on
	ftags=[]
	if "FightTags" in buff:
		for ft in buff['FightTags']:
			ftags.append(FIGHTTAGS[ft[9:]])
		if not buff['IsDebuff']:
			ftags.append('Cannot be dispelled.')
	text+= ' '.join(ftags)

	# SelfNoExtraTime	~								#	true ~	another turn after action
	text+= IfAddText(buff, 'SelfNoExtraTime', '\nGrants another turn.')

	text=text.rstrip('\n')
	return [buff['Name'],buff['Desc'],text]

CondHPTarget={
	1:	'Troop',
	2:	'Hero',
	3:	'Soldier'
}

CondHPOperator={
	1:	'at',
	2:	'above',
	3:	'below'
}

FIGHTTAGS={
	'None'				:	'',
	'IngoreGuard'		:	'Ignores Guard.',
	'BanGuard'			:	'Cannot Guard.',
	'BanActiveSkill'	:	'Cannot use active skills.',
	'BanPassiveSkill'	:	'Nullifies passive skills.',
	'BanHeal'			:	'Cannot receive healing.',
	'BanDamage'			:	'Immune to damage.',
	'BanBuff'			:	'Cannot receive buffs.',
	'BanDeBuff'			:	'Immune to all debuffs.',
	'BanPunch'			:	'BanPunch',
	'Stun'				:	'Cannot attack.',
	'Hide'				:	'?Hide?',
	'BanPercentDamage'	:	'Immune to fixed damage.',
	'BanMeleePunish'	:	'Unaffected by melee damage reduction.',
	'OnlyAttackHero'	:	'Directly attacks an enemy hero.',
	'BanSoldierHeal'	:	'Soldiers cannot receive healing.',
	'BanNewTurn'		:	'BanNewTurn',
	'IgnoreCost'		:	'?IgnoreCost?',
	'BanHPPercentDamage':	'?BanHPPercentDamage?',
}

PROPERTIES={
	'HPCmd'				:	'HP Command',
	'ATCmd'				:	'ATK Command',
	'DFCmd'				:	'DEF Command',
	'MagicCmd'			:	'INT Command',	
	'MagicDFCmd'		:	'MDEF Command',
	'DEXCmd'			:	'Skill Command',
	'HP'				:	'HP',
	'AT'				:	'ATK',
	'DF'				:	'DEF',
	'Magic'				:	'INT',
	'MagicDF'			:	'MDEF',
	'DEX'				:	'Skill',
	'Heal'				:	'Healing',
	'HealRecv'			:	'Healing received',
	'PsyDmg'			:	'phys. DMG',
	'PsyDmgRecv'		:	'phys. DMG received',
	'MagicDmg'			:	'mag. DMG',
	'MagicDmgRecv'		:	'mag. DMG received',
	'BFSkillDmg'		:	'AOE DMG',
	'RecvBFSkillDmg'	:	'AOE DMG received',
	'TrueDmg'			:	'fixed DMG',
	'RecvTrueDmg'		:	'fixed DMG received',
	'SuperPsyDmgRecv'	:	'super phys. DMG received',
	'SuperMagicDmgRecv'	:	'super mag. DMG received',
	'RangeDmgRecv'		:	'ranged DMG',
	'SkillDmg'			:	'Skill DMG',
	'BFMovePoint'		:	'Move',
	'BFAttackDistance'	:	'Attack Range',
	'PsyDistance'		:	'phys. Skill Range',
	'MagicDistance'		:	'mag. Skill Range',
	'PsyBFSkillRange'	:	'phys. AOE Skill Range',
	'MagicBFSkillRange'	:	'mag. AOE Skill Range',
	'CriticalRate'		:	'Critical Rate',
	'RecvCriticalRate'	:	'Critical Rate received',
	'CriticalDamage'	:	'Critical DMG',
	'IgnoreDF'			:	'DEF Ignor',
	'RageKill'			:	'',
	'RageBeAttack'		:	'',
	'RageEndTurn'		:	'',
}

if __name__ == "__main__":
	bdata=LoadConfig('ConfigDataBuffInfo.json')
	cbuff=[Buff(data) for data in bdata]
	SaveOut('Buff',cbuff)
	
'''
syntax = "proto2";
package ConfigDataBuffInfo;
enum ENUMFightTag {
	FightTag_None = 0;
	FightTag_IngoreGuard = 1;
	FightTag_BanGuard = 2;
	FightTag_BanActiveSkill = 3;
	FightTag_BanPassiveSkill = 4;
	FightTag_BanHeal = 5;
	FightTag_BanDamage = 6;
	FightTag_BanBuff = 7;
	FightTag_BanDeBuff = 8;
	FightTag_BanPunch = 9;
	FightTag_Stun = 10;
	FightTag_Hide = 11;
	FightTag_BanPercentDamage = 12;
	FightTag_BanMeleePunish = 13;
	FightTag_OnlyAttackHero = 14;
	FightTag_BanSoldierHeal = 15;
	FightTag_BanNewTurn = 16;
	FightTag_IgnoreCost = 17;
	FightTag_BanHPPercentDamage = 18;
}

enum ENUMPropertyModifyType {
	PropertyModifyType_None = 0;
	PropertyModifyType_Hero1_HPAdd = 1;
	PropertyModifyType_Hero1_ATAdd = 2;
	PropertyModifyType_Hero1_DFAdd = 3;
	PropertyModifyType_Hero1_MagicAdd = 4;
	PropertyModifyType_Hero1_MagicDFAdd = 5;
	PropertyModifyType_Hero1_DEXAdd = 6;
	PropertyModifyType_Hero1_HPCmdAdd = 7;
	PropertyModifyType_Hero1_ATCmdAdd = 8;
	PropertyModifyType_Hero1_DFCmdAdd = 9;
	PropertyModifyType_Hero1_MagicDFCmdAdd = 10;
	PropertyModifyType_Hero1_BFMovePointAdd = 11;
	PropertyModifyType_Hero1_BFAttackDistanceAdd = 12;
	PropertyModifyType_Hero1_CriticalDamageAdd = 13;
	PropertyModifyType_Hero1_CriticalRateAdd = 14;
	PropertyModifyType_Hero1_HPMul = 15;
	PropertyModifyType_Hero1_ATMul = 16;
	PropertyModifyType_Hero1_DFMul = 17;
	PropertyModifyType_Hero1_MagicMul = 18;
	PropertyModifyType_Hero1_MagicDFMul = 19;
	PropertyModifyType_Hero1_DEXMul = 20;
	PropertyModifyType_Hero2_HPAdd = 21;
	PropertyModifyType_Hero2_ATAdd = 22;
	PropertyModifyType_Hero2_DFAdd = 23;
	PropertyModifyType_Hero2_MagicAdd = 24;
	PropertyModifyType_Hero2_MagicDFAdd = 25;
	PropertyModifyType_Hero2_DEXAdd = 26;
	PropertyModifyType_Hero2_HPCmdAdd = 27;
	PropertyModifyType_Hero2_ATCmdAdd = 28;
	PropertyModifyType_Hero2_DFCmdAdd = 29;
	PropertyModifyType_Hero2_MagicDFCmdAdd = 30;
	PropertyModifyType_Hero2_BFMovePointAdd = 31;
	PropertyModifyType_Hero2_BFAttackDistanceAdd = 32;
	PropertyModifyType_Hero2_MagicDistanceAdd = 33;
	PropertyModifyType_Hero2_PsyDistanceAdd = 34;
	PropertyModifyType_Hero2_CriticalDamageAdd = 35;
	PropertyModifyType_Hero2_CriticalRateAdd = 36;
	PropertyModifyType_Hero2_RageEndTurnAdd = 37;
	PropertyModifyType_Hero2_RageBeAttackAdd = 38;
	PropertyModifyType_Hero2_RageKillAdd = 39;
	PropertyModifyType_Hero2_HPMul = 40;
	PropertyModifyType_Hero2_ATMul = 41;
	PropertyModifyType_Hero2_DFMul = 42;
	PropertyModifyType_Hero2_MagicMul = 43;
	PropertyModifyType_Hero2_MagicDFMul = 44;
	PropertyModifyType_Hero2_DEXMul = 45;
	PropertyModifyType_Hero2_PsyDmgMul = 46;
	PropertyModifyType_Hero2_PsyDmgRecvMul = 47;
	PropertyModifyType_Hero2_SuperPsyDmgRecvMul = 48;
	PropertyModifyType_Hero2_HealMul = 49;
	PropertyModifyType_Hero2_HealRecvMul = 50;
	PropertyModifyType_Hero2_MagicDmgMul = 51;
	PropertyModifyType_Hero2_MagicDmgRecvMul = 52;
	PropertyModifyType_Hero2_SuperMagicDmgRecvMul = 53;
	PropertyModifyType_Hero2_IgnoreDFMul = 54;
	PropertyModifyType_Hero2_SkillDmgMul = 55;
	PropertyModifyType_Hero2_BFSkillDmgMul = 56;
	PropertyModifyType_Soldier1_HPAdd = 57;
	PropertyModifyType_Soldier1_ATAdd = 58;
	PropertyModifyType_Soldier1_DFAdd = 59;
	PropertyModifyType_Soldier1_MagicDFAdd = 60;
	PropertyModifyType_Soldier1_BFMovePointAdd = 61;
	PropertyModifyType_Soldier1_BFAttackDistanceAdd = 62;
	PropertyModifyType_Soldier1_CriticalDamageAdd = 63;
	PropertyModifyType_Soldier1_CriticalRateAdd = 64;
	PropertyModifyType_Soldier1_HPMul = 65;
	PropertyModifyType_Soldier1_ATMul = 66;
	PropertyModifyType_Soldier1_DFMul = 67;
	PropertyModifyType_Soldier1_MagicDFMul = 68;
	PropertyModifyType_Soldier2_HPAdd = 69;
	PropertyModifyType_Soldier2_ATAdd = 70;
	PropertyModifyType_Soldier2_DFAdd = 71;
	PropertyModifyType_Soldier2_MagicDFAdd = 72;
	PropertyModifyType_Soldier2_BFMovePointAdd = 73;
	PropertyModifyType_Soldier2_BFAttackDistanceAdd = 74;
	PropertyModifyType_Soldier2_CriticalDamageAdd = 75;
	PropertyModifyType_Soldier2_CriticalRateAdd = 76;
	PropertyModifyType_Soldier2_HPMul = 77;
	PropertyModifyType_Soldier2_ATMul = 78;
	PropertyModifyType_Soldier2_DFMul = 79;
	PropertyModifyType_Soldier2_MagicDFMul = 80;
	PropertyModifyType_Soldier2_PsyDmgMul = 81;
	PropertyModifyType_Soldier2_PsyDmgRecvMul = 82;
	PropertyModifyType_Soldier2_SuperPsyDmgRecvMul = 83;
	PropertyModifyType_Soldier2_MagicDmgRecvMul = 84;
	PropertyModifyType_Soldier2_SuperMagicDmgRecvMul = 85;
	PropertyModifyType_Soldier2_IgnoreDFMul = 86;
	PropertyModifyType_Hero3_HPAdd = 87;
	PropertyModifyType_Hero3_ATAdd = 88;
	PropertyModifyType_Hero3_DFAdd = 89;
	PropertyModifyType_Hero3_MagicAdd = 90;
	PropertyModifyType_Hero3_MagicDFAdd = 91;
	PropertyModifyType_Hero3_DEXAdd = 92;
	PropertyModifyType_Hero3_HPCmdAdd = 93;
	PropertyModifyType_Hero3_ATCmdAdd = 94;
	PropertyModifyType_Hero3_DFCmdAdd = 95;
	PropertyModifyType_Hero3_MagicDFCmdAdd = 96;
	PropertyModifyType_Hero3_CriticalDamageAdd = 97;
	PropertyModifyType_Hero3_CriticalRateAdd = 98;
	PropertyModifyType_Hero3_HPMul = 99;
	PropertyModifyType_Hero3_ATMul = 100;
	PropertyModifyType_Hero3_DFMul = 101;
	PropertyModifyType_Hero3_MagicMul = 102;
	PropertyModifyType_Hero3_MagicDFMul = 103;
	PropertyModifyType_Hero3_DEXMul = 104;
	PropertyModifyType_Hero2_RangeDmgRecvMul = 105;
	PropertyModifyType_Soldier2_RangeDmgRecvMul = 106;
	PropertyModifyType_Hero2_RecvCriticalRateAdd = 107;
	PropertyModifyType_Soldier2_RecvCriticalRateAdd = 108;
	PropertyModifyType_Soldier2_MagicDmgMul = 109;
	PropertyModifyType_Hero2_TrueDmgMul = 110;
	PropertyModifyType_Hero2_RecvTrueDmgMul = 111;
	PropertyModifyType_Hero2_RecvBFSkillDmgMul = 112;
	PropertyModifyType_Soldier2_RecvBFSkillDmgMul = 113;
	PropertyModifyType_Hero2_MagicBFSkillRangeAdd = 114;
	PropertyModifyType_Hero2_PsyBFSkillRangeAdd = 115;
	PropertyModifyType_Count = 116;
}

enum ENUMBuffType {
	BuffType_None = 0;
	BuffType_PropertiesModify = 1;
	BuffType_Charge = 2;
	BuffType_FieldArmy = 3;
	BuffType_Immune = 4;
	BuffType_DamageRebound = 5;
	BuffType_CombatPropertiesModify = 6;
	BuffType_CombatAttachBuff = 7;
	BuffType_DamageAttachBuff = 8;
	BuffType_DamageRemoveBuff = 9;
	BuffType_CombatHeal = 10;
	BuffType_Guard = 11;
	BuffType_DoubleAttack = 12;
	BuffType_DoubleMove = 13;
	BuffType_Punch = 14;
	BuffType_DamageRemoveCD = 15;
	BuffType_HealOverTime = 16;
	BuffType_DamageOverTime = 17;
	BuffType_NewTurn = 18;
	BuffType_RemoveDebuff = 19;
	BuffType_TerrainAdv = 20;
	BuffType_Restrain = 21;
	BuffType_CombatHealOther = 22;
	BuffType_AddBuff = 23;
	BuffType_CombatDamage = 24;
	BuffType_MagicAttack = 25;
	BuffType_TeamBuff = 26;
	BuffType_BFSkillAttachBuff = 27;
	BuffType_BFSkill = 28;
	BuffType_NerverDie = 29;
	BuffType_BattlePropertiesSet = 30;
	BuffType_Drag = 31;
	BuffType_PropertiesExchange = 32;
	BuffType_DamageAfterDamage = 33;
	BuffType_HeroAura = 34;
	BuffType_TagAura = 35;
	BuffType_RelationModify = 36;
	BuffType_BuffPropertiesModify = 37;
	BuffType_AddBuffSuper = 38;
	BuffType_Removebuff = 39;
	BuffType_BuffPack = 40;
	BuffType_Replace = 41;
	BuffType_ArmyChange = 42;
	BuffType_MoveChange = 43;
	BuffType_DamageHealOther = 44;
	BuffType_PhysicalAttack = 45;
	BuffType_HealChange = 46;
	BuffType_IgnoreRestrain = 47;
	BuffType_HealDamage = 48;
	BuffType_KillAttachBuff = 49;
	BuffType_DoubleSkill = 50;
	BuffType_DieBFSkill = 51;
	BuffType_BeforeBFSkillAttachBuff = 52;
	BuffType_SummonPropertiesExchange = 53;
	BuffType_BuffTimechange = 54;
	BuffType_DistancePropertiesModify = 55;
}

enum ENUMBuffConditionType {
	BuffConditionType_None = 0;
	BuffConditionType_IsAlone = 1;
	BuffConditionType_NotAlone = 2;
	BuffConditionType_Terrain = 3;
	BuffConditionType_HeroArmy = 4;
	BuffConditionType_TerrainIsDF = 5;
	BuffConditionType_ArmyCombination = 6;
	BuffConditionType_HeroJob = 7;
	BuffConditionType_HeroInfo = 8;
	BuffConditionType_IsMove = 9;
}

message ConfigDataBuffInfo {
	required int32 ID = 2;
	required string Name = 3;
	required string Desc = 4;
	required bool IsDebuff = 5;
	required bool IsEnhance = 6;
	required int32 Time = 7;
	required int32 Cond_HP_Target = 8;
	required int32 Cond_HP_Operator = 9;
	required int32 Cond_HP_Value = 10;
	required ENUMBuffConditionType ConditionType = 11;
	repeated int32 ConditionParam = 12;
	required ENUMBuffType BuffType = 13;
	required int32 BuffTypeParam1 = 14;
	required int32 BuffTypeParam2 = 15;
	required int32 BuffTypeParam3 = 16;
	repeated int32 BuffTypeParam4 = 17;
	required ENUMPropertyModifyType BuffTypeParam5 = 18;
	repeated int32 BuffTypeParam6 = 19;
	required bool SelfNoExtraTime = 20;
	required int32 SubType = 21;
	required int32 ReplaceRule = 22;
	required int32 ReplacePriority = 23;
	repeated ENUMFightTag FightTags = 24;
	required ENUMPropertyModifyType Property1_ID = 25;
	required int32 Property1_Value = 26;
	required ENUMPropertyModifyType Property2_ID = 27;
	required int32 Property2_Value = 28;
	required ENUMPropertyModifyType Property3_ID = 29;
	required int32 Property3_Value = 30;
	required ENUMPropertyModifyType Property4_ID = 31;
	required int32 Property4_Value = 32;
	required int32 CDBuff_ID = 33;
	required string Effect_Attach = 34;
	required string Effect_Process = 35;
	required string Effect_Acting = 36;
	required string Effect_ActingTarget = 37;
	required string Icon = 38;
	required bool IconDisplay = 39;
}

message Items {
	repeated ConfigDataBuffInfo items = 1;
}
'''