import re

r = re.compile(r'sub_(.+?)\(.+?(,.+?(\d+)\))?;\n(.+\n)?.+?(\d) =.+wrapper_0\("(.+?)"\);(\n.+\n  return sub_AFE210\(v1, (\d+)\);)?')

#	1	~	typ
#	2	~	/
#	3	~	ID (msg)
#	4	~	required?
#	5	~	vx = new_wrapper
#	6	~	name
#	7	~	/
#	8	~	ID (enum)
def scrap(path):
	text = open(path,'rt').read()

	msgs={}
	enums={}
	sub=False
	for match in r.finditer(text):
		if match[1]=='AFE4F4':
			sub=True
			name=match[6]
		elif sub and match[1]=='AFE234':	#	proto
			if name not in msgs:
				msgs[name]={}

			msgs[name][int(match[3])]={
				'name':	match[6],
				'req':	bool(match.groups()[3])	# group 1 = 0
			}
		elif sub and match[1]=='AFE124':	#	enum
			if name not in enums:
				enums[name]={}
			if name in match[6]:
				enums[name][match[8]]=match[6]
			else:
				continue

		else:
			sub=False

	return msgs, enums