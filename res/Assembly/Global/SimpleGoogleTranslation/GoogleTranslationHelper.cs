﻿// Decompiled with JetBrains decompiler
// Type: SimpleGoogleTranslation.GoogleTranslationHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SimpleGoogleTranslation
{
  public class GoogleTranslationHelper : MonoBehaviour
  {
    private static Dictionary<string, string> m_transCache = new Dictionary<string, string>();
    private static GoogleTranslationHelper s_instance;

    public static void Trans(string target, string source, Action<List<string>> callback)
    {
      GoogleTranslationHelper.Trans((string) null, target, source, callback);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Trans(
      string sourceLanguage,
      string target,
      string source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Trans(
      string sourceLanguage,
      string target,
      List<string> source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Translate(
      string sourceLanguage,
      string target,
      List<string> source,
      Action<List<string>> callback)
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ProcessTrans(
      string sourceLanguage,
      string target,
      List<string> source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Detect(string source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Detect(List<string> source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DetectLanguage(List<string> source, Action<List<string>> callback)
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ProcessDetect(List<string> source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void DoCache(string source, string target, string result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetCache(string source, string target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetCacheKey(string key, string target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CheckInited()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
