﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Log.LogManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Log
{
  public class LogManager
  {
    public bool NeedEngineLog = true;
    private static LogManager m_instance;
    public bool NeedFileLog;
    public bool IsCallingEngineLog;

    [MethodImpl((MethodImplOptions) 32768)]
    private LogManager()
    {
      Application.logMessageReceived += new Application.LogCallback(this.OnReceiveUnityEngineLog);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReceiveUnityEngineLog(string log, string stackTrace, LogType type)
    {
      if (this.FileLogger == null || this.IsCallingEngineLog || !this.NeedFileLog)
        return;
      if (type == LogType.Log)
        this.FileLogger.WriteLog(log, "D");
      else if (type == LogType.Warning)
        this.FileLogger.WriteLog(log, "W");
      else
        this.FileLogger.WriteLog(log, "E");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LogManager CreateLogManager()
    {
      if (LogManager.m_instance == null)
        LogManager.m_instance = new LogManager();
      return LogManager.m_instance;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(bool needEngineLog, bool needFileLog, string logFileRoot, string logName)
    {
      this.NeedEngineLog = needEngineLog;
      this.NeedFileLog = needFileLog;
      if (needFileLog && this.FileLogger == null)
        this.FileLogger = new FileLogger(logFileRoot, logName);
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    public static LogManager Instance
    {
      get
      {
        return LogManager.m_instance;
      }
    }

    public FileLogger FileLogger { get; private set; }
  }
}
