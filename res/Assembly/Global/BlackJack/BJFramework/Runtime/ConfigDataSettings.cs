﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ConfigDataSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class ConfigDataSettings
  {
    public string ConfigDataLoaderTypeDNName = "Assembly-CSharp@BlackJack.ConfigData.ClientConfigDataLoader";
    public bool ConfigDataAssetAllowNullSetting = true;
    public string ConfigDataAssetTargetPath = "Assets/GameProject/RuntimeAssets/ConfigData/ConfigData_ABS";
    [NonSerialized]
    public string RuntimeConfigDataAssetTargetPath = "Assets/GameProject/RuntimeAssets/ConfigData/ConfigData_ABS";
    [Header("初始化的线程个数")]
    public int InitThreadCount = 4;
    [Header("初始化时加载多少个yeildreturn一次")]
    public int InitloadCountForSingleYield = 20;
    public string StringTableManagerTypeDNName;
    public bool ConfigDataAllowMD5NotMach;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSettings()
    {
    }

    public string GetConfigDataAssetTargetPath()
    {
      return this.RuntimeConfigDataAssetTargetPath;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetConfigDataAssetPathNoPostfix(string fileName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetConfigDataAssetPath(string fileName)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
