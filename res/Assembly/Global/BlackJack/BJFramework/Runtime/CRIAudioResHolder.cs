﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CRIAudioResHolder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [AddComponentMenu("CRIWARE/BlackJack/CRI Audio Res Holder")]
  public class CRIAudioResHolder : MonoBehaviour
  {
    [Header("播放延时")]
    public float DelayTime;
    public string m_criCueName;

    [MethodImpl((MethodImplOptions) 32768)]
    public CRIAudioResHolder()
    {
    }

    public void Play()
    {
      this.StartCoroutine(this.PlayWithDelay());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayWithDelay()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
