﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.ReloginBySessionUITaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public abstract class ReloginBySessionUITaskBase : UITaskBase
  {
    protected bool m_waitForStartGameSessionLogin;
    protected bool m_raiseCriticalDataCacheDirty;

    public ReloginBySessionUITaskBase(string name)
      : base(name)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void InitlizeBeforeManagerStartIt()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartRelogin()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void ShowErrorMsg(string key)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ShowWaitForReloginConfirmUI()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ShowWaitForReloginProcessingUI()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowWaitReturnToLoginConfirmUI()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnRetryLoginButtonClicked(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnReturnToLoginConfirmButtonClicked(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool IsNeedCheckLocalDataCache()
    {
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnLoginBySessionTokenAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnPlayerInfoInitAck(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerDataUnsyncNotify()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerNetworkError(int err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    public static event Action EventOnReloginSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action EventOnReturnToLoginConfirmed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
