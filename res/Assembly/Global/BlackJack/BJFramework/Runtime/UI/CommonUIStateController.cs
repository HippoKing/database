﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.CommonUIStateController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class CommonUIStateController : MonoBehaviour
  {
    public List<UIStateDesc> UIStateDescList = new List<UIStateDesc>();
    public List<ColorSetDesc> UIStateColorSetList = new List<ColorSetDesc>();
    public List<ButtonEx> UIRelateButtonList = new List<ButtonEx>();
    [HideInInspector]
    public List<GameObject> DisableGoList = new List<GameObject>();
    private List<TweenMain> m_allTweens = new List<TweenMain>();
    public bool EnablePressColor;
    public int PressColorIndex;
    public Animator AnimationController;
    [HideInInspector]
    public int LastStateIndex;
    private bool isGathed;
    private UIStateDesc m_currUIState;
    private int m_animatorTriggerNameId;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIStateController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToUIState(string stateName, bool notPlayTweens = false, bool allowToRefreshSameState = true)
    {
      UIStateDesc uiStateDescByName = this.GetUIStateDescByName(stateName);
      if (uiStateDescByName == null)
      {
        Debug.LogError(string.Format("CommonUIStateController::SetToUIState UIState Find Fail:  GoName:{0}  stateName:{1}", (object) this.gameObject.name, (object) stateName));
      }
      else
      {
        if (this.m_currUIState != null && this.m_currUIState.StateName == stateName && !allowToRefreshSameState)
          return;
        this.m_currUIState = uiStateDescByName;
        UIStateDesc currUiState = this.m_currUIState;
        bool flag = false;
        this.m_currUIState.TweenFinished = flag;
        int num = flag ? 1 : 0;
        currUiState.AnimationFinished = num != 0;
        if (!this.isGathed)
          this.ReGatherDynamicGo();
        foreach (GameObject disableGo in this.DisableGoList)
          disableGo.SetActive(false);
        foreach (UIStateDesc uiStateDesc in this.UIStateDescList)
        {
          if (uiStateDesc.StateName == stateName)
          {
            foreach (GameObject toShowGameObject in uiStateDesc.SetToShowGameObjectList)
            {
              if ((UnityEngine.Object) toShowGameObject != (UnityEngine.Object) null)
                toShowGameObject.SetActive(true);
            }
            if (uiStateDesc.StateColorSetIndex >= 0 && this.UIStateColorSetList.Count > 0 && uiStateDesc.StateColorSetIndex < this.UIStateColorSetList.Count)
            {
              ColorSetDesc uiStateColorSet = this.UIStateColorSetList[uiStateDesc.StateColorSetIndex];
              this.ChangeToColor(uiStateColorSet.UIStateColorDescList);
              this.NotifyColorChanged(uiStateColorSet.UIStateColorDescList);
            }
            foreach (UIStateGradientColorDesc gradientDesc in uiStateDesc.GradientDescList)
            {
              if ((UnityEngine.Object) gradientDesc.GradientComptent != (UnityEngine.Object) null && gradientDesc.GradientComptent.enabled)
              {
                gradientDesc.GradientComptent.enabled = false;
                gradientDesc.GradientComptent.m_color1 = gradientDesc.Color1;
                gradientDesc.GradientComptent.m_color2 = gradientDesc.Color2;
                gradientDesc.GradientComptent.enabled = true;
              }
            }
            if ((UnityEngine.Object) this.AnimationController == (UnityEngine.Object) null)
              this.OnStateExit(this.m_currUIState.StateName);
            if (uiStateDesc.TweenAnimationList.Count == 0 || notPlayTweens)
            {
              this.OnAllUIStateTweensFinished();
            }
            else
            {
              foreach (TweenMain tweenAnimation in uiStateDesc.TweenAnimationList)
              {
                if (!((UnityEngine.Object) tweenAnimation == (UnityEngine.Object) null))
                  tweenAnimation.gameObject.SetActive(true);
              }
              foreach (TweenMain tweenAnimation in uiStateDesc.TweenAnimationList)
              {
                if (!((UnityEngine.Object) tweenAnimation == (UnityEngine.Object) null))
                {
                  tweenAnimation.ResetToBeginning();
                  tweenAnimation.PlayForward();
                }
              }
              foreach (TweenMain allTween in this.m_allTweens)
              {
                if (!((UnityEngine.Object) allTween == (UnityEngine.Object) null) && !uiStateDesc.TweenAnimationList.Contains(allTween))
                  allTween.enabled = false;
              }
            }
            if ((UnityEngine.Object) this.AnimationController != (UnityEngine.Object) null)
            {
              if (this.m_animatorTriggerNameId != 0)
                this.AnimationController.ResetTrigger(this.m_animatorTriggerNameId);
              this.m_animatorTriggerNameId = Animator.StringToHash(uiStateDesc.StateName);
              this.AnimationController.SetTrigger(this.m_animatorTriggerNameId);
              this.AnimationController.Update(0.0f);
            }
          }
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeToColor(List<UIStateColorDesc> colorDescList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NotifyColorChanged(List<UIStateColorDesc> colorDescList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNextState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReGatherDynamicGo()
    {
      this.DisableGoList.Clear();
      this.m_allTweens.Clear();
      foreach (UIStateDesc uiStateDesc in this.UIStateDescList)
      {
        foreach (GameObject toShowGameObject in uiStateDesc.SetToShowGameObjectList)
        {
          if ((UnityEngine.Object) toShowGameObject != (UnityEngine.Object) null && !this.DisableGoList.Contains(toShowGameObject))
            this.DisableGoList.Add(toShowGameObject);
        }
        foreach (TweenMain tweenAnimation in uiStateDesc.TweenAnimationList)
        {
          if (!this.m_allTweens.Contains(tweenAnimation))
            this.m_allTweens.Add(tweenAnimation);
        }
      }
      this.ReGatherColorChangeImageAndText();
      foreach (UIStateDesc uiStateDesc in this.UIStateDescList)
      {
        if (uiStateDesc.TweenAnimationList.Count != 0)
        {
          TweenMain tweenMain = (TweenMain) null;
          float num = float.MinValue;
          foreach (TweenMain tweenAnimation in uiStateDesc.TweenAnimationList)
          {
            if (!((UnityEngine.Object) tweenAnimation == (UnityEngine.Object) null) && tweenAnimation.style == TweenMain.Style.Once && (double) tweenAnimation.duration + (double) tweenAnimation.delay > (double) num)
            {
              num = tweenAnimation.duration + tweenAnimation.delay;
              tweenMain = tweenAnimation;
            }
          }
          if ((UnityEngine.Object) tweenMain != (UnityEngine.Object) null)
          {
            tweenMain.OnFinished.RemoveAllListeners();
            tweenMain.OnFinished.AddListener(new UnityAction(this.OnAllUIStateTweensFinished));
          }
        }
      }
      this.isGathed = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStateExit(string stateName)
    {
      UIStateDesc uiStateDescByName = this.GetUIStateDescByName(stateName);
      if (uiStateDescByName == null)
        Debug.LogError(string.Format("OnStateExit() receive a unknown stateName: {0}.", (object) stateName));
      else if (this.m_currUIState != uiStateDescByName)
      {
        Debug.LogError(string.Format("Current state is not: {0}.", (object) stateName));
      }
      else
      {
        uiStateDescByName.AnimationFinished = true;
        if (!uiStateDescByName.TweenFinished || CommonUIStateController.m_onUIStateEndEvent == null)
          return;
        CommonUIStateController.m_onUIStateEndEvent(this.gameObject, stateName);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlaySound(string soundName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetActionForUIStateFinshed(string stateName, Action action)
    {
      UIStateDesc uiStateDescByName = this.GetUIStateDescByName(stateName);
      if (uiStateDescByName == null)
        return false;
      uiStateDescByName.m_eventFinished = action;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateDesc GetUIStateDescByName(string name)
    {
      foreach (UIStateDesc uiStateDesc in this.UIStateDescList)
      {
        if (uiStateDesc.StateName == name)
          return uiStateDesc;
      }
      return (UIStateDesc) null;
    }

    public UIStateDesc GetCurrentUIState()
    {
      return this.m_currUIState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReGatherColorChangeImageAndText()
    {
      foreach (ColorSetDesc uiStateColorSet in this.UIStateColorSetList)
      {
        foreach (UIStateColorDesc uiStateColorDesc in uiStateColorSet.UIStateColorDescList)
        {
          uiStateColorDesc.ChangeColorImageList.Clear();
          uiStateColorDesc.ChangeColorTextList.Clear();
          if ((UnityEngine.Object) uiStateColorDesc.ChangeColorGo != (UnityEngine.Object) null)
          {
            foreach (Image component in uiStateColorDesc.ChangeColorGo.GetComponents<Image>())
            {
              if ((UnityEngine.Object) component != (UnityEngine.Object) null && !uiStateColorDesc.ChangeColorImageList.Contains(component))
                uiStateColorDesc.ChangeColorImageList.Add(component);
            }
            foreach (Text component in uiStateColorDesc.ChangeColorGo.GetComponents<Text>())
            {
              if ((UnityEngine.Object) component != (UnityEngine.Object) null && !uiStateColorDesc.ChangeColorTextList.Contains(component))
                uiStateColorDesc.ChangeColorTextList.Add(component);
            }
          }
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllUIStateTweensFinished()
    {
      if (this.m_currUIState == null)
        return;
      this.m_currUIState.TweenFinished = true;
      if (!this.m_currUIState.AnimationFinished || CommonUIStateController.m_onUIStateEndEvent == null)
        return;
      CommonUIStateController.m_onUIStateEndEvent(this.gameObject, this.m_currUIState.StateName);
    }

    [HideInInspector]
    public static event Action<GameObject, string> m_onUIStateEndEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
