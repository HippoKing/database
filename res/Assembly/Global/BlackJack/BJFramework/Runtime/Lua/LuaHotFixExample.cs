﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaHotFixExample
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [CustomLuaClass]
  public class LuaHotFixExample
  {
    private ObjectLuaHotFixState m_hotfixState;
    private BJLuaObjHelper m_luaObjHelper;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetRefParamLuaHotFixExampleInt32LuaTestStruct_hotfix;
    private LuaFunction m_GetOutParamLuaHotFixExampleInt32LuaTestStruct_hotfix;
    private LuaFunction m_WithOutParamLuaHotFixExampleInt32LuaTestStruct_hotfix;
    private LuaFunction m_ReturnVoid_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaHotFixExample()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WithOutParam(
      int notOutParam,
      out LuaHotFixExample classParam,
      out LuaTestStruct valueTypeParam,
      out int intParam,
      out float floatParam,
      out bool boolParam,
      out ObjectLuaHotFixState enumParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetOutParam(
      out LuaHotFixExample classParam,
      LuaHotFixExample notOutClassParam,
      out LuaTestStruct valueTypeParam,
      LuaTestStruct notOutvalueTypeParam,
      out int intParam,
      int notOutIntParam,
      out float floatParam,
      float notOutfloatParam,
      out bool boolParam,
      bool notOutboolParam,
      out ObjectLuaHotFixState enumParam,
      ObjectLuaHotFixState notOutEnumParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetOutParam(
      out LuaHotFixExample classParam,
      out int intParam,
      out LuaTestStruct valueTypeParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRefParam(
      out LuaHotFixExample classParam,
      ref int intParam,
      out LuaTestStruct valueTypeParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnVoid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix(LuaTable module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
