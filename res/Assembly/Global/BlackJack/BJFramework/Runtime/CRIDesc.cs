﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CRIDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class CRIDesc
  {
    public string m_acfFullPath;
    public List<CRIDesc.SheetDesc> m_sheetList;

    [MethodImpl((MethodImplOptions) 32768)]
    public CRIDesc()
    {
    }

    public class SheetDesc
    {
      public string m_sheetName;
      public string m_acbFullPath;
      public string m_awbFullPath;
    }
  }
}
