﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.MapSceneTaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.Utils;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  [HotFix]
  public class MapSceneTaskBase : Task, IDynamicAssetProvider
  {
    protected MapSceneUpdatePipeLineCtxBase m_pipeCtxDefault;
    protected List<MapSceneUpdatePipeLineCtxBase> m_runingPipeLineCtxList;
    protected List<MapSceneUpdatePipeLineCtxBase> m_unusingPipeLineCtxList;
    protected List<MapSceneUpdatePipeLineCtxBase> m_pipeLineWait2Start;
    protected SceneLayerBase[] m_layerArray;
    protected Dictionary<string, MapSceneTaskBase.DynamicResCacheItem> m_dynamicResCacheDict;
    protected List<string> m_wait2ReleaseResList;
    protected TinyCorutineHelper m_corutineHelper;
    private int m_unloadDynamicResTimeInterval;
    private DateTime m_nextUnLoadDynamicResTime;
    [DoNotToLua]
    private MapSceneTaskBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_InitPipeCtxDefault_hotfix;
    private LuaFunction m_OnStartObject_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnPause_hotfix;
    private LuaFunction m_StartUpdatePipeLineMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_AllocPipeLineCtx_hotfix;
    private LuaFunction m_CreatePipeLineCtx_hotfix;
    private LuaFunction m_StartPipeLineCtxMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_ReleasePipeLineCtxMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_IsNeedUpdateDataCacheMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_UpdateDataCacheMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_PreProcessBeforeResLoadMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_StartLoadStaticResMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_OnLoadStaticResCompletedMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_IsNeedCollectAllDynamicResForLoadMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_CollectAllDynamicResForLoadMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_StartLoadDynamicResList`1MapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_CalculateDynamicResReallyNeedLoadList`1_hotfix;
    private LuaFunction m_OnLoadDynamicResCompletedDictionary`2MapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_IsLoadAllResCompletedMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_OnLoadAllResCompletedMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompletedMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_StartUpdateViewMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_UpdateViewMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_PostUpdateViewMapSceneUpdatePipeLineCtxBase_hotfix;
    private LuaFunction m_HideAllView_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_ClearContextOnPause_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_ReleaseDynamicResCacheString_hotfix;
    private LuaFunction m_LockDynamicResCacheString_hotfix;
    private LuaFunction m_UnlockDynamicResCacheString_hotfix;
    private LuaFunction m_UnlockAllDynamicResCache_hotfix;
    private LuaFunction m_TickDynamicResCache_hotfix;
    private LuaFunction m_BlackJack\u002EBJFramework\u002ERuntime\u002ETaskNs\u002EIDynamicAssetProvider\u002EAllocDynamicAssetString_hotfix;
    private LuaFunction m_BlackJack\u002EBJFramework\u002ERuntime\u002ETaskNs\u002EIDynamicAssetProvider\u002EAllocDynamicAssetString_hotfix;
    private LuaFunction m_BlackJack\u002EBJFramework\u002ERuntime\u002ETaskNs\u002EIDynamicAssetProvider\u002EReleaseDynamicAssetString_hotfix;
    private LuaFunction m_BlackJack\u002EBJFramework\u002ERuntime\u002ETaskNs\u002EIDynamicAssetProvider\u002ECheckAssetString_hotfix;
    private LuaFunction m_get_m_mainLayer_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public MapSceneTaskBase(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void InitPipeCtxDefault()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool StartUpdatePipeLine(MapSceneUpdatePipeLineCtxBase pipeCtx = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual MapSceneUpdatePipeLineCtxBase AllocPipeLineCtx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual MapSceneUpdatePipeLineCtxBase CreatePipeLineCtx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool StartPipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ReleasePipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedUpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PreProcessBeforeResLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartLoadStaticRes(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadStaticResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedCollectAllDynamicResForLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<string> CollectAllDynamicResForLoad(
      MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartLoadDynamicRes(
      List<string> resPathList,
      MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadDynamicResCompleted(
      Dictionary<string, UnityEngine.Object> resDict,
      MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void InitLayerStateOnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearContextOnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ReleaseDynamicResCache(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void LockDynamicResCache(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnlockDynamicResCache(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnlockAllDynamicResCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickDynamicResCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    UnityEngine.Object IDynamicAssetProvider.AllocDynamicAsset(
      string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    T IDynamicAssetProvider.AllocDynamicAsset<T>(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void IDynamicAssetProvider.ReleaseDynamicAsset(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    bool IDynamicAssetProvider.CheckAsset(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected SceneLayerBase m_mainLayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected virtual MapSceneTaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public MapSceneTaskBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_Start(object param)
    {
      return this.Start(param);
    }

    private void __callBase_Stop()
    {
      this.Stop();
    }

    private void __callBase_Pause()
    {
      this.Pause();
    }

    private bool __callBase_Resume(object param)
    {
      return this.Resume(param);
    }

    private void __callBase_ClearOnStopEvent()
    {
      this.ClearOnStopEvent();
    }

    private void __callBase_ExecAfterTicks(Action action, ulong delayTickCount)
    {
      this.ExecAfterTicks(action, delayTickCount);
    }

    private bool __callBase_OnStart(object param)
    {
      return base.OnStart(param);
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(object param)
    {
      return this.OnResume(param);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnTick()
    {
      base.OnTick();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public struct LayerDesc
    {
      public string m_layerName;
      public string m_layerResPath;
      public bool m_isUILayer;
    }

    public class DynamicResCacheItem
    {
      public UnityEngine.Object m_res;
      public uint m_ref;
      public bool m_waitForRelease;
      public bool m_isLocked;
    }

    public class LuaExportHelper
    {
      private MapSceneTaskBase m_owner;

      public LuaExportHelper(MapSceneTaskBase owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_Start(object param)
      {
        return this.m_owner.__callBase_Start(param);
      }

      public void __callBase_Stop()
      {
        this.m_owner.__callBase_Stop();
      }

      public void __callBase_Pause()
      {
        this.m_owner.__callBase_Pause();
      }

      public bool __callBase_Resume(object param)
      {
        return this.m_owner.__callBase_Resume(param);
      }

      public void __callBase_ClearOnStopEvent()
      {
        this.m_owner.__callBase_ClearOnStopEvent();
      }

      public void __callBase_ExecAfterTicks(Action action, ulong delayTickCount)
      {
        this.m_owner.__callBase_ExecAfterTicks(action, delayTickCount);
      }

      public bool __callBase_OnStart(object param)
      {
        return this.m_owner.__callBase_OnStart(param);
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(object param)
      {
        return this.m_owner.__callBase_OnResume(param);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnTick()
      {
        this.m_owner.__callBase_OnTick();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public MapSceneUpdatePipeLineCtxBase m_pipeCtxDefault
      {
        get
        {
          return this.m_owner.m_pipeCtxDefault;
        }
        set
        {
          this.m_owner.m_pipeCtxDefault = value;
        }
      }

      public List<MapSceneUpdatePipeLineCtxBase> m_runingPipeLineCtxList
      {
        get
        {
          return this.m_owner.m_runingPipeLineCtxList;
        }
        set
        {
          this.m_owner.m_runingPipeLineCtxList = value;
        }
      }

      public List<MapSceneUpdatePipeLineCtxBase> m_unusingPipeLineCtxList
      {
        get
        {
          return this.m_owner.m_unusingPipeLineCtxList;
        }
        set
        {
          this.m_owner.m_unusingPipeLineCtxList = value;
        }
      }

      public List<MapSceneUpdatePipeLineCtxBase> m_pipeLineWait2Start
      {
        get
        {
          return this.m_owner.m_pipeLineWait2Start;
        }
        set
        {
          this.m_owner.m_pipeLineWait2Start = value;
        }
      }

      public SceneLayerBase[] m_layerArray
      {
        get
        {
          return this.m_owner.m_layerArray;
        }
        set
        {
          this.m_owner.m_layerArray = value;
        }
      }

      public Dictionary<string, MapSceneTaskBase.DynamicResCacheItem> m_dynamicResCacheDict
      {
        get
        {
          return this.m_owner.m_dynamicResCacheDict;
        }
        set
        {
          this.m_owner.m_dynamicResCacheDict = value;
        }
      }

      public List<string> m_wait2ReleaseResList
      {
        get
        {
          return this.m_owner.m_wait2ReleaseResList;
        }
        set
        {
          this.m_owner.m_wait2ReleaseResList = value;
        }
      }

      public TinyCorutineHelper m_corutineHelper
      {
        get
        {
          return this.m_owner.m_corutineHelper;
        }
        set
        {
          this.m_owner.m_corutineHelper = value;
        }
      }

      public int m_unloadDynamicResTimeInterval
      {
        get
        {
          return this.m_owner.m_unloadDynamicResTimeInterval;
        }
        set
        {
          this.m_owner.m_unloadDynamicResTimeInterval = value;
        }
      }

      public DateTime m_nextUnLoadDynamicResTime
      {
        get
        {
          return this.m_owner.m_nextUnLoadDynamicResTime;
        }
        set
        {
          this.m_owner.m_nextUnLoadDynamicResTime = value;
        }
      }

      public SceneLayerBase m_mainLayer
      {
        get
        {
          return this.m_owner.m_mainLayer;
        }
      }

      public MapSceneTaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public void InitPipeCtxDefault()
      {
        this.m_owner.InitPipeCtxDefault();
      }

      public bool OnStart(object param)
      {
        return this.m_owner.OnStart(param);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public bool StartUpdatePipeLine(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        return this.m_owner.StartUpdatePipeLine(pipeCtx);
      }

      public MapSceneUpdatePipeLineCtxBase AllocPipeLineCtx()
      {
        return this.m_owner.AllocPipeLineCtx();
      }

      public MapSceneUpdatePipeLineCtxBase CreatePipeLineCtx()
      {
        return this.m_owner.CreatePipeLineCtx();
      }

      public bool StartPipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        return this.m_owner.StartPipeLineCtx(pipeCtx);
      }

      public void ReleasePipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.ReleasePipeLineCtx(pipeCtx);
      }

      public bool IsNeedUpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        return this.m_owner.IsNeedUpdateDataCache(pipeCtx);
      }

      public void UpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.UpdateDataCache(pipeCtx);
      }

      public void PreProcessBeforeResLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.PreProcessBeforeResLoad(pipeCtx);
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public void StartLoadStaticRes(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.StartLoadStaticRes(pipeCtx);
      }

      public void OnLoadStaticResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.OnLoadStaticResCompleted(pipeCtx);
      }

      public bool IsNeedCollectAllDynamicResForLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        return this.m_owner.IsNeedCollectAllDynamicResForLoad(pipeCtx);
      }

      public List<string> CollectAllDynamicResForLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        return this.m_owner.CollectAllDynamicResForLoad(pipeCtx);
      }

      public void StartLoadDynamicRes(
        List<string> resPathList,
        MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.StartLoadDynamicRes(resPathList, pipeCtx);
      }

      public HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
      {
        return this.m_owner.CalculateDynamicResReallyNeedLoad(resPathList);
      }

      public void OnLoadDynamicResCompleted(
        Dictionary<string, UnityEngine.Object> resDict,
        MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.OnLoadDynamicResCompleted(resDict, pipeCtx);
      }

      public bool IsLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        return this.m_owner.IsLoadAllResCompleted(pipeCtx);
      }

      public void OnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.OnLoadAllResCompleted(pipeCtx);
      }

      public void InitLayerStateOnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted(pipeCtx);
      }

      public void StartUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.StartUpdateView(pipeCtx);
      }

      public void UpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.UpdateView(pipeCtx);
      }

      public void PostUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
      {
        this.m_owner.PostUpdateView(pipeCtx);
      }

      public void HideAllView()
      {
        this.m_owner.HideAllView();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void ClearContextOnPause()
      {
        this.m_owner.ClearContextOnPause();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void ReleaseDynamicResCache(string path)
      {
        this.m_owner.ReleaseDynamicResCache(path);
      }

      public void LockDynamicResCache(string path)
      {
        this.m_owner.LockDynamicResCache(path);
      }

      public void UnlockDynamicResCache(string path)
      {
        this.m_owner.UnlockDynamicResCache(path);
      }

      public void UnlockAllDynamicResCache()
      {
        this.m_owner.UnlockAllDynamicResCache();
      }

      public void TickDynamicResCache()
      {
        this.m_owner.TickDynamicResCache();
      }
    }
  }
}
