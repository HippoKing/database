﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Turple`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class Turple<T1, T2, T3>
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public Turple(T1 elem1, T2 elem2, T3 elem3)
    {
    }

    public T1 Elem1 { get; set; }

    public T2 Elem2 { get; set; }

    public T3 Elem3 { get; set; }
  }
}
