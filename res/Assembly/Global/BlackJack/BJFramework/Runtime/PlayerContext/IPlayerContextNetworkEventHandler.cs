﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.IPlayerContextNetworkEventHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  public interface IPlayerContextNetworkEventHandler
  {
    void OnGameServerConnected();

    void OnGameServerDisconnected();

    void OnGameServerError(int err, string excepionInfo = null);

    void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken);

    void OnLoginBySessionTokenAck(int result);

    void OnGameServerMessage(object msg, int msgId);
  }
}
