﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.AutoBindAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [AttributeUsage(AttributeTargets.Field)]
  public class AutoBindAttribute : Attribute
  {
    public readonly string m_path;
    public readonly AutoBindAttribute.InitState m_initState;
    public readonly bool m_optional;

    [MethodImpl((MethodImplOptions) 32768)]
    public AutoBindAttribute(string path, AutoBindAttribute.InitState initState = AutoBindAttribute.InitState.NotInit, bool optional = false)
    {
      this.m_path = path;
      this.m_initState = initState;
      this.m_optional = optional;
    }

    public enum InitState
    {
      NotInit,
      Active,
      Inactive,
    }
  }
}
