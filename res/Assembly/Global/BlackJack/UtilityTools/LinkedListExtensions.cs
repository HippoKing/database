﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.LinkedListExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.UtilityTools
{
  public static class LinkedListExtensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static LinkedListNode<T> Find<T>(
      this LinkedList<T> list,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LinkedListNode<T> AddAfterFromLast<T>(
      this LinkedList<T> list,
      T element,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LinkedListNode<T> AddAfterFromSpecific<T>(
      this LinkedList<T> list,
      T element,
      LinkedListNode<T> specific,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CorrectNode<T>(
      this LinkedList<T> list,
      LinkedListNode<T> correctNode,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
