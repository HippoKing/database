﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataDialogInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextDialog_ID;
    private int _FrameType;
    private string _PlaceName;
    private string _CharName;
    private int _CharImage_ID;
    private int _Position;
    private int _EnterType;
    private string _BGM;
    private int _LeaveType;
    private bool _TogetherLeave;
    private int _LeaveTime;
    private string _PreAnimation;
    private string _PreFacialAnimation;
    private string _IdleAnimation;
    private string _IdleFacialAnimation;
    private string _Background;
    private string _BackgroundImage;
    private int _BackgroundX;
    private string _Voice;
    private string _Words;
    private List<int> _BeforeEnterEffectGroup1;
    private List<int> _BeforeEnterEffectGroup2;
    private List<int> _BeforeEnterEffectGroup3;
    private int _BeforeEnterInsertEffect;
    private List<int> _BeforeTalkEffectGroup1;
    private List<int> _BeforeTalkEffectGroup2;
    private List<int> _BeforeTalkEffectGroup3;
    private int _BeforeTalkInsertEffect;
    private List<int> _AfterTalkEffectGroup1;
    private List<int> _AfterTalkEffectGroup2;
    private List<int> _AfterTalkEffectGroup3;
    private int _AfterTalkInsertEffect;
    private IExtension extensionObject;
    public ConfigDataDialogInfo m_prevDialogInfo;
    public ConfigDataDialogInfo m_nextDialogInfo;
    public ConfigDataCharImageInfo m_charImageInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextDialog_ID")]
    public int NextDialog_ID
    {
      get
      {
        return this._NextDialog_ID;
      }
      set
      {
        this._NextDialog_ID = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FrameType")]
    public int FrameType
    {
      get
      {
        return this._FrameType;
      }
      set
      {
        this._FrameType = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlaceName")]
    public string PlaceName
    {
      get
      {
        return this._PlaceName;
      }
      set
      {
        this._PlaceName = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "CharName")]
    public string CharName
    {
      get
      {
        return this._CharName;
      }
      set
      {
        this._CharName = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImage_ID")]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Position")]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnterType")]
    public int EnterType
    {
      get
      {
        return this._EnterType;
      }
      set
      {
        this._EnterType = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "BGM")]
    public string BGM
    {
      get
      {
        return this._BGM;
      }
      set
      {
        this._BGM = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaveType")]
    public int LeaveType
    {
      get
      {
        return this._LeaveType;
      }
      set
      {
        this._LeaveType = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "TogetherLeave")]
    public bool TogetherLeave
    {
      get
      {
        return this._TogetherLeave;
      }
      set
      {
        this._TogetherLeave = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaveTime")]
    public int LeaveTime
    {
      get
      {
        return this._LeaveTime;
      }
      set
      {
        this._LeaveTime = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "PreAnimation")]
    public string PreAnimation
    {
      get
      {
        return this._PreAnimation;
      }
      set
      {
        this._PreAnimation = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, IsRequired = true, Name = "PreFacialAnimation")]
    public string PreFacialAnimation
    {
      get
      {
        return this._PreFacialAnimation;
      }
      set
      {
        this._PreFacialAnimation = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "IdleAnimation")]
    public string IdleAnimation
    {
      get
      {
        return this._IdleAnimation;
      }
      set
      {
        this._IdleAnimation = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "IdleFacialAnimation")]
    public string IdleFacialAnimation
    {
      get
      {
        return this._IdleFacialAnimation;
      }
      set
      {
        this._IdleFacialAnimation = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "Background")]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, IsRequired = true, Name = "BackgroundImage")]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundX")]
    public int BackgroundX
    {
      get
      {
        return this._BackgroundX;
      }
      set
      {
        this._BackgroundX = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, IsRequired = true, Name = "Voice")]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, IsRequired = true, Name = "Words")]
    public string Words
    {
      get
      {
        return this._Words;
      }
      set
      {
        this._Words = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, Name = "BeforeEnterEffectGroup1")]
    public List<int> BeforeEnterEffectGroup1
    {
      get
      {
        return this._BeforeEnterEffectGroup1;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, Name = "BeforeEnterEffectGroup2")]
    public List<int> BeforeEnterEffectGroup2
    {
      get
      {
        return this._BeforeEnterEffectGroup2;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, Name = "BeforeEnterEffectGroup3")]
    public List<int> BeforeEnterEffectGroup3
    {
      get
      {
        return this._BeforeEnterEffectGroup3;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BeforeEnterInsertEffect")]
    public int BeforeEnterInsertEffect
    {
      get
      {
        return this._BeforeEnterInsertEffect;
      }
      set
      {
        this._BeforeEnterInsertEffect = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, Name = "BeforeTalkEffectGroup1")]
    public List<int> BeforeTalkEffectGroup1
    {
      get
      {
        return this._BeforeTalkEffectGroup1;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, Name = "BeforeTalkEffectGroup2")]
    public List<int> BeforeTalkEffectGroup2
    {
      get
      {
        return this._BeforeTalkEffectGroup2;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, Name = "BeforeTalkEffectGroup3")]
    public List<int> BeforeTalkEffectGroup3
    {
      get
      {
        return this._BeforeTalkEffectGroup3;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BeforeTalkInsertEffect")]
    public int BeforeTalkInsertEffect
    {
      get
      {
        return this._BeforeTalkInsertEffect;
      }
      set
      {
        this._BeforeTalkInsertEffect = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, Name = "AfterTalkEffectGroup1")]
    public List<int> AfterTalkEffectGroup1
    {
      get
      {
        return this._AfterTalkEffectGroup1;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, Name = "AfterTalkEffectGroup2")]
    public List<int> AfterTalkEffectGroup2
    {
      get
      {
        return this._AfterTalkEffectGroup2;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, Name = "AfterTalkEffectGroup3")]
    public List<int> AfterTalkEffectGroup3
    {
      get
      {
        return this._AfterTalkEffectGroup3;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AfterTalkInsertEffect")]
    public int AfterTalkInsertEffect
    {
      get
      {
        return this._AfterTalkInsertEffect;
      }
      set
      {
        this._AfterTalkInsertEffect = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
