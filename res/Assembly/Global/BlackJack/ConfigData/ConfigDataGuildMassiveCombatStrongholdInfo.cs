﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGuildMassiveCombatStrongholdInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataGuildMassiveCombatStrongholdInfo")]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataGuildMassiveCombatStrongholdInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _UIName;
    private string _Resources;
    private string _TeamName;
    private List<int> _LevelList;
    private int _EnemyLevel;
    private int _EnemyCount;
    private int _EnemyPoints;
    private int _PerfectWinPointsBonus;
    private List<Goods> _Bonus;
    private List<int> _UpHeroTag_IDS;
    private int _UpSkill_ID;
    private IExtension extensionObject;
    public ConfigDataGuildMassiveCombatDifficultyInfo m_difficultyInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatStrongholdInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "UIName")]
    public string UIName
    {
      get
      {
        return this._UIName;
      }
      set
      {
        this._UIName = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Resources")]
    public string Resources
    {
      get
      {
        return this._Resources;
      }
      set
      {
        this._Resources = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "LevelList")]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnemyLevel")]
    public int EnemyLevel
    {
      get
      {
        return this._EnemyLevel;
      }
      set
      {
        this._EnemyLevel = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnemyCount")]
    public int EnemyCount
    {
      get
      {
        return this._EnemyCount;
      }
      set
      {
        this._EnemyCount = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnemyPoints")]
    public int EnemyPoints
    {
      get
      {
        return this._EnemyPoints;
      }
      set
      {
        this._EnemyPoints = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PerfectWinPointsBonus")]
    public int PerfectWinPointsBonus
    {
      get
      {
        return this._PerfectWinPointsBonus;
      }
      set
      {
        this._PerfectWinPointsBonus = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "Bonus")]
    public List<Goods> Bonus
    {
      get
      {
        return this._Bonus;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "UpHeroTag_IDS")]
    public List<int> UpHeroTag_IDS
    {
      get
      {
        return this._UpHeroTag_IDS;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UpSkill_ID")]
    public int UpSkill_ID
    {
      get
      {
        return this._UpSkill_ID;
      }
      set
      {
        this._UpSkill_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
