﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBehaviorChangeRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBehaviorChangeRule")]
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataBehaviorChangeRule : IExtensible
  {
    private int _ID;
    private BehaviorCondition _ChangeCondition;
    private string _CCParam;
    private int _NextBehaviorID;
    private IExtension extensionObject;
    public ConfigDataBehavior.ParamData CCParamData;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehaviorChangeRule()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChangeCondition")]
    public BehaviorCondition ChangeCondition
    {
      get
      {
        return this._ChangeCondition;
      }
      set
      {
        this._ChangeCondition = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "CCParam")]
    public string CCParam
    {
      get
      {
        return this._CCParam;
      }
      set
      {
        this._CCParam = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextBehaviorID")]
    public int NextBehaviorID
    {
      get
      {
        return this._NextBehaviorID;
      }
      set
      {
        this._NextBehaviorID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
