﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityScenarioLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityScenarioLevelInfo")]
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataCollectionActivityScenarioLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Notes;
    private int _NextScenario_ID;
    private int _DaysBeforeActivate;
    private int _MonsterLevel;
    private int _BattleID;
    private int _DialogBeforeBattle;
    private int _DialogAfterBattle;
    private List<WayPointState> _WayPointsStateAfterBattle;
    private List<WayPointResource> _WayPointsResourceAfterBattle;
    private string _ActorResourceAfterBattle;
    private int _PlayerExp;
    private int _HeroExp;
    private int _GoldReward;
    private int _EnergyCostSuccess;
    private int _EnergyCostFail;
    private List<Goods> _FirstClearDrop;
    private int _DropDisplayCount;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;
    public ConfigDataCollectionActivityWaypointInfo WaypointInfo;
    public ConfigDataCollectionActivityInfo CollectionActivity;
    public int m_scenarioDepth;
    public ConfigDataCollectionActivityScenarioLevelInfo m_nextScenarioInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Notes")]
    public string Notes
    {
      get
      {
        return this._Notes;
      }
      set
      {
        this._Notes = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextScenario_ID")]
    public int NextScenario_ID
    {
      get
      {
        return this._NextScenario_ID;
      }
      set
      {
        this._NextScenario_ID = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DaysBeforeActivate")]
    public int DaysBeforeActivate
    {
      get
      {
        return this._DaysBeforeActivate;
      }
      set
      {
        this._DaysBeforeActivate = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleID")]
    public int BattleID
    {
      get
      {
        return this._BattleID;
      }
      set
      {
        this._BattleID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogBeforeBattle")]
    public int DialogBeforeBattle
    {
      get
      {
        return this._DialogBeforeBattle;
      }
      set
      {
        this._DialogBeforeBattle = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogAfterBattle")]
    public int DialogAfterBattle
    {
      get
      {
        return this._DialogAfterBattle;
      }
      set
      {
        this._DialogAfterBattle = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "WayPointsStateAfterBattle")]
    public List<WayPointState> WayPointsStateAfterBattle
    {
      get
      {
        return this._WayPointsStateAfterBattle;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "WayPointsResourceAfterBattle")]
    public List<WayPointResource> WayPointsResourceAfterBattle
    {
      get
      {
        return this._WayPointsResourceAfterBattle;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "ActorResourceAfterBattle")]
    public string ActorResourceAfterBattle
    {
      get
      {
        return this._ActorResourceAfterBattle;
      }
      set
      {
        this._ActorResourceAfterBattle = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExp")]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExp")]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoldReward")]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyCostSuccess")]
    public int EnergyCostSuccess
    {
      get
      {
        return this._EnergyCostSuccess;
      }
      set
      {
        this._EnergyCostSuccess = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyCostFail")]
    public int EnergyCostFail
    {
      get
      {
        return this._EnergyCostFail;
      }
      set
      {
        this._EnergyCostFail = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, Name = "FirstClearDrop")]
    public List<Goods> FirstClearDrop
    {
      get
      {
        return this._FirstClearDrop;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropDisplayCount")]
    public int DropDisplayCount
    {
      get
      {
        return this._DropDisplayCount;
      }
      set
      {
        this._DropDisplayCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
