﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattlePerformInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattlePerformInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataBattlePerformInfo : IExtensible
  {
    private int _ID;
    private int _NextPerform_ID;
    private BattlePerformType _PerformType;
    private int _Param1;
    private int _Param2;
    private List<ParamPosition> _Param3;
    private List<int> _Param4;
    private string _Param5;
    private List<int> _Param6;
    private IExtension extensionObject;
    public ConfigDataBattlePerformInfo m_prevPerformInfo;
    public ConfigDataBattlePerformInfo m_nextPerformInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlePerformInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextPerform_ID")]
    public int NextPerform_ID
    {
      get
      {
        return this._NextPerform_ID;
      }
      set
      {
        this._NextPerform_ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PerformType")]
    public BattlePerformType PerformType
    {
      get
      {
        return this._PerformType;
      }
      set
      {
        this._PerformType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param1")]
    public int Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param2")]
    public int Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Param3")]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Param4")]
    public List<int> Param4
    {
      get
      {
        return this._Param4;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Param5")]
    public string Param5
    {
      get
      {
        return this._Param5;
      }
      set
      {
        this._Param5 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "Param6")]
    public List<int> Param6
    {
      get
      {
        return this._Param6;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
