﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardSelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardSelectType")]
  public enum CardSelectType
  {
    [ProtoEnum(Name = "CardSelectType_SingleSelect", Value = 1)] CardSelectType_SingleSelect = 1,
    [ProtoEnum(Name = "CardSelectType_TenSelect", Value = 2)] CardSelectType_TenSelect = 2,
    [ProtoEnum(Name = "CardSelectType_BothSelect", Value = 3)] CardSelectType_BothSelect = 3,
  }
}
