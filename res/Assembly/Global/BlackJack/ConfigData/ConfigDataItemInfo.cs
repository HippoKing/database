﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataItemInfo")]
  [Serializable]
  public class ConfigDataItemInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rank;
    private int _SellGold;
    private ItemDisplayType _DisplayType;
    private ItemFuncType _FuncType;
    private int _FuncTypeParam1;
    private int _FuncTypeParam2;
    private int _FuncTypeParam3;
    private List<int> _FuncTypeParam4;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private int _AlchemyGold;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SellGold")]
    public int SellGold
    {
      get
      {
        return this._SellGold;
      }
      set
      {
        this._SellGold = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayType")]
    public ItemDisplayType DisplayType
    {
      get
      {
        return this._DisplayType;
      }
      set
      {
        this._DisplayType = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FuncType")]
    public ItemFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FuncTypeParam1")]
    public int FuncTypeParam1
    {
      get
      {
        return this._FuncTypeParam1;
      }
      set
      {
        this._FuncTypeParam1 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FuncTypeParam2")]
    public int FuncTypeParam2
    {
      get
      {
        return this._FuncTypeParam2;
      }
      set
      {
        this._FuncTypeParam2 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FuncTypeParam3")]
    public int FuncTypeParam3
    {
      get
      {
        return this._FuncTypeParam3;
      }
      set
      {
        this._FuncTypeParam3 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "FuncTypeParam4")]
    public List<int> FuncTypeParam4
    {
      get
      {
        return this._FuncTypeParam4;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetPathDesc")]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AlchemyGold")]
    public int AlchemyGold
    {
      get
      {
        return this._AlchemyGold;
      }
      set
      {
        this._AlchemyGold = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomDropRewardID")]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayRewardCount")]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
