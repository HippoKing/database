﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCooperateBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataCooperateBattleInfo")]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataCooperateBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _TeamName;
    private string _Image;
    private string _BriefView;
    private List<int> _LevelList;
    private string _OpenHour;
    private string _CloseHour;
    private string _OpenHour2;
    private string _CloseHour2;
    private List<int> _OpenWeekDays;
    private IExtension extensionObject;
    public List<ConfigDataCooperateBattleLevelInfo> m_levelInfos;
    public TimeSpan OpenTimeSpan;
    public TimeSpan CloseTimeSpan;
    public TimeSpan OpenTimeSpan2;
    public TimeSpan CloseTimeSpan2;
    public List<DayOfWeek> OpenDaysOfWeek;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "BriefView")]
    public string BriefView
    {
      get
      {
        return this._BriefView;
      }
      set
      {
        this._BriefView = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "LevelList")]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenHour")]
    public string OpenHour
    {
      get
      {
        return this._OpenHour;
      }
      set
      {
        this._OpenHour = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "CloseHour")]
    public string CloseHour
    {
      get
      {
        return this._CloseHour;
      }
      set
      {
        this._CloseHour = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenHour2")]
    public string OpenHour2
    {
      get
      {
        return this._OpenHour2;
      }
      set
      {
        this._OpenHour2 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "CloseHour2")]
    public string CloseHour2
    {
      get
      {
        return this._CloseHour2;
      }
      set
      {
        this._CloseHour2 = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "OpenWeekDays")]
    public List<int> OpenWeekDays
    {
      get
      {
        return this._OpenWeekDays;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
