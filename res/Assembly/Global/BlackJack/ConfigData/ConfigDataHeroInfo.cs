﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataHeroInfo")]
  [Serializable]
  public class ConfigDataHeroInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _Name_Eng;
    private bool _Useable;
    private int _Sex;
    private int _Star;
    private int _Rank;
    private List<HeroInfoStarToRank> _StarToRank;
    private int _FragmentItem_ID;
    private int _ExchangedFragmentCount;
    private int _Soldier_ID;
    private List<int> _SelectableSoldier_ID;
    private List<int> _Skills_ID;
    private List<int> _HiddenSkills_ID;
    private int _HPCmd_INI;
    private int _DFCmd_INI;
    private int _ATCmd_INI;
    private int _MagicDFCmd_INI;
    private string _CmdRating;
    private List<int> _HPStar;
    private List<int> _ATStar;
    private List<int> _MagicStar;
    private List<int> _DFStar;
    private List<int> _MagicDFStar;
    private List<int> _DEXStar;
    private int _JobConnection_ID;
    private List<int> _UseableJobConnections_ID;
    private int _CharImage_ID;
    private int _HeroInformation_ID;
    private List<int> _TechCanLearnSoldiers_ID;
    private List<int> _Skins_ID;
    private IExtension extensionObject;
    public ConfigDataSoldierInfo m_soldierInfo;
    public ConfigDataSkillInfo[] m_skillInfos;
    public ConfigDataSkillInfo[] m_hiddenSkillInfos;
    public ConfigDataJobConnectionInfo m_jobConnectionInfo;
    public ConfigDataJobConnectionInfo[] m_useableJobConnectionInfos;
    public ConfigDataItemInfo m_fragmentItemInfo;
    public ConfigDataCharImageInfo m_charImageInfo;
    public ConfigDataCharImageInfo[] m_starToCharImageInfos;
    public ConfigDataHeroInformationInfo m_informationInfo;
    public List<int> m_heroTagIds;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name_Eng")]
    public string Name_Eng
    {
      get
      {
        return this._Name_Eng;
      }
      set
      {
        this._Name_Eng = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Useable")]
    public bool Useable
    {
      get
      {
        return this._Useable;
      }
      set
      {
        this._Useable = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Sex")]
    public int Sex
    {
      get
      {
        return this._Sex;
      }
      set
      {
        this._Sex = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Star")]
    public int Star
    {
      get
      {
        return this._Star;
      }
      set
      {
        this._Star = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "StarToRank")]
    public List<HeroInfoStarToRank> StarToRank
    {
      get
      {
        return this._StarToRank;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FragmentItem_ID")]
    public int FragmentItem_ID
    {
      get
      {
        return this._FragmentItem_ID;
      }
      set
      {
        this._FragmentItem_ID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExchangedFragmentCount")]
    public int ExchangedFragmentCount
    {
      get
      {
        return this._ExchangedFragmentCount;
      }
      set
      {
        this._ExchangedFragmentCount = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Soldier_ID")]
    public int Soldier_ID
    {
      get
      {
        return this._Soldier_ID;
      }
      set
      {
        this._Soldier_ID = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, Name = "SelectableSoldier_ID")]
    public List<int> SelectableSoldier_ID
    {
      get
      {
        return this._SelectableSoldier_ID;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, Name = "Skills_ID")]
    public List<int> Skills_ID
    {
      get
      {
        return this._Skills_ID;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, Name = "HiddenSkills_ID")]
    public List<int> HiddenSkills_ID
    {
      get
      {
        return this._HiddenSkills_ID;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HPCmd_INI")]
    public int HPCmd_INI
    {
      get
      {
        return this._HPCmd_INI;
      }
      set
      {
        this._HPCmd_INI = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DFCmd_INI")]
    public int DFCmd_INI
    {
      get
      {
        return this._DFCmd_INI;
      }
      set
      {
        this._DFCmd_INI = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ATCmd_INI")]
    public int ATCmd_INI
    {
      get
      {
        return this._ATCmd_INI;
      }
      set
      {
        this._ATCmd_INI = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDFCmd_INI")]
    public int MagicDFCmd_INI
    {
      get
      {
        return this._MagicDFCmd_INI;
      }
      set
      {
        this._MagicDFCmd_INI = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, IsRequired = true, Name = "CmdRating")]
    public string CmdRating
    {
      get
      {
        return this._CmdRating;
      }
      set
      {
        this._CmdRating = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, Name = "HPStar")]
    public List<int> HPStar
    {
      get
      {
        return this._HPStar;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, Name = "ATStar")]
    public List<int> ATStar
    {
      get
      {
        return this._ATStar;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, Name = "MagicStar")]
    public List<int> MagicStar
    {
      get
      {
        return this._MagicStar;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, Name = "DFStar")]
    public List<int> DFStar
    {
      get
      {
        return this._DFStar;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, Name = "MagicDFStar")]
    public List<int> MagicDFStar
    {
      get
      {
        return this._MagicDFStar;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, Name = "DEXStar")]
    public List<int> DEXStar
    {
      get
      {
        return this._DEXStar;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JobConnection_ID")]
    public int JobConnection_ID
    {
      get
      {
        return this._JobConnection_ID;
      }
      set
      {
        this._JobConnection_ID = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, Name = "UseableJobConnections_ID")]
    public List<int> UseableJobConnections_ID
    {
      get
      {
        return this._UseableJobConnections_ID;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImage_ID")]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroInformation_ID")]
    public int HeroInformation_ID
    {
      get
      {
        return this._HeroInformation_ID;
      }
      set
      {
        this._HeroInformation_ID = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, Name = "TechCanLearnSoldiers_ID")]
    public List<int> TechCanLearnSoldiers_ID
    {
      get
      {
        return this._TechCanLearnSoldiers_ID;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, Name = "Skins_ID")]
    public List<int> Skins_ID
    {
      get
      {
        return this._Skins_ID;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRank(int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetCharImageInfo(int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
