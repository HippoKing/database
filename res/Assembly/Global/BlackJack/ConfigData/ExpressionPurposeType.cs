﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ExpressionPurposeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ExpressionPurposeType")]
  public enum ExpressionPurposeType
  {
    [ProtoEnum(Name = "ExpressionPurposeType_Chat", Value = 1)] ExpressionPurposeType_Chat = 1,
    [ProtoEnum(Name = "ExpressionPurposeType_Combat", Value = 2)] ExpressionPurposeType_Combat = 2,
  }
}
