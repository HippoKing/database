﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RafflePoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RafflePoolType")]
  public enum RafflePoolType
  {
    [ProtoEnum(Name = "RafflePoolType_None", Value = 0)] RafflePoolType_None,
    [ProtoEnum(Name = "RafflePoolType_Raffle", Value = 1)] RafflePoolType_Raffle,
    [ProtoEnum(Name = "RafflePoolType_Tarot", Value = 2)] RafflePoolType_Tarot,
  }
}
