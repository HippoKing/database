﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.StoreType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "StoreType")]
  public enum StoreType
  {
    [ProtoEnum(Name = "StoreType_None", Value = 0)] StoreType_None,
    [ProtoEnum(Name = "StoreType_Static", Value = 1)] StoreType_Static,
    [ProtoEnum(Name = "StoreType_Random", Value = 2)] StoreType_Random,
    [ProtoEnum(Name = "StoreType_Recharge", Value = 3)] StoreType_Recharge,
    [ProtoEnum(Name = "StoreType_Gift", Value = 4)] StoreType_Gift,
  }
}
