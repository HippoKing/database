﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SelectTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SelectTarget")]
  public enum SelectTarget
  {
    [ProtoEnum(Name = "SelectTarget_EnemyNearest", Value = 1)] SelectTarget_EnemyNearest = 1,
    [ProtoEnum(Name = "SelectTarget_SelfPosition", Value = 2)] SelectTarget_SelfPosition = 2,
    [ProtoEnum(Name = "SelectTarget_DefaultSelection", Value = 3)] SelectTarget_DefaultSelection = 3,
    [ProtoEnum(Name = "SelectTarget_SameAsLeader", Value = 4)] SelectTarget_SameAsLeader = 4,
    [ProtoEnum(Name = "SelectTarget_PositionNearest", Value = 5)] SelectTarget_PositionNearest = 5,
    [ProtoEnum(Name = "SelectTarget_MemberIndex", Value = 6)] SelectTarget_MemberIndex = 6,
    [ProtoEnum(Name = "SelectTarget_MemberHeroID", Value = 7)] SelectTarget_MemberHeroID = 7,
    [ProtoEnum(Name = "SelectTarget_TargetInSkillRange", Value = 8)] SelectTarget_TargetInSkillRange = 8,
    [ProtoEnum(Name = "SelectTarget_EnemyWithBuffN", Value = 9)] SelectTarget_EnemyWithBuffN = 9,
    [ProtoEnum(Name = "SelectTarget_EnemyIDFilter", Value = 10)] SelectTarget_EnemyIDFilter = 10, // 0x0000000A
    [ProtoEnum(Name = "SelectTarget_Leader", Value = 11)] SelectTarget_Leader = 11, // 0x0000000B
    [ProtoEnum(Name = "SelectTarget_RandomReachableEmptyPosition", Value = 12)] SelectTarget_RandomReachableEmptyPosition = 12, // 0x0000000C
    [ProtoEnum(Name = "SelectTarget_EnemyBlockWayToMoveTarget", Value = 13)] SelectTarget_EnemyBlockWayToMoveTarget = 13, // 0x0000000D
  }
}
