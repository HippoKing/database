﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleArmyRandomRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleArmyRandomRuleType")]
  public enum BattleArmyRandomRuleType
  {
    [ProtoEnum(Name = "BattleArmyRandomRuleType_None", Value = 0)] BattleArmyRandomRuleType_None,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_EveryTime", Value = 1)] BattleArmyRandomRuleType_EveryTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_DailyTime", Value = 2)] BattleArmyRandomRuleType_DailyTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_ClimbTower", Value = 3)] BattleArmyRandomRuleType_ClimbTower,
  }
}
