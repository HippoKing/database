﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCardPoolGroupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataCardPoolGroupInfo")]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataCardPoolGroupInfo : IExtensible
  {
    private int _ID;
    private int _CardPoolID;
    private int _FirstSingleSelectWeight;
    private int _DefaultSingleSelectWeight;
    private int _DefaultTenSelectWeight;
    private int _FirstTenSelectWeight;
    private int _TenSelectAccumulateWeight;
    private IExtension extensionObject;
    public Dictionary<int, ICardPoolItemWeight> CardItemList;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolGroupInfo()
    {
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CardPoolID")]
    public int CardPoolID
    {
      get
      {
        return this._CardPoolID;
      }
      set
      {
        this._CardPoolID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstSingleSelectWeight")]
    public int FirstSingleSelectWeight
    {
      get
      {
        return this._FirstSingleSelectWeight;
      }
      set
      {
        this._FirstSingleSelectWeight = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefaultSingleSelectWeight")]
    public int DefaultSingleSelectWeight
    {
      get
      {
        return this._DefaultSingleSelectWeight;
      }
      set
      {
        this._DefaultSingleSelectWeight = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefaultTenSelectWeight")]
    public int DefaultTenSelectWeight
    {
      get
      {
        return this._DefaultTenSelectWeight;
      }
      set
      {
        this._DefaultTenSelectWeight = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstTenSelectWeight")]
    public int FirstTenSelectWeight
    {
      get
      {
        return this._FirstTenSelectWeight;
      }
      set
      {
        this._FirstTenSelectWeight = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TenSelectAccumulateWeight")]
    public int TenSelectAccumulateWeight
    {
      get
      {
        return this._TenSelectAccumulateWeight;
      }
      set
      {
        this._TenSelectAccumulateWeight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
