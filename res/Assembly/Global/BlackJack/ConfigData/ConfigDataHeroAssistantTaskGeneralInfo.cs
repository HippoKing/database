﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskGeneralInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskGeneralInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskGeneralInfo : IExtensible
  {
    private int _ID;
    private int _WorkSeconds1;
    private int _DropCount1;
    private int _WorkSeconds2;
    private int _DropCount2;
    private int _WorkSeconds3;
    private int _DropCount3;
    private int _RecommandHeroMultiply;
    private int _RecommandHeroAdd;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskGeneralInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WorkSeconds1")]
    public int WorkSeconds1
    {
      get
      {
        return this._WorkSeconds1;
      }
      set
      {
        this._WorkSeconds1 = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropCount1")]
    public int DropCount1
    {
      get
      {
        return this._DropCount1;
      }
      set
      {
        this._DropCount1 = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WorkSeconds2")]
    public int WorkSeconds2
    {
      get
      {
        return this._WorkSeconds2;
      }
      set
      {
        this._WorkSeconds2 = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropCount2")]
    public int DropCount2
    {
      get
      {
        return this._DropCount2;
      }
      set
      {
        this._DropCount2 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WorkSeconds3")]
    public int WorkSeconds3
    {
      get
      {
        return this._WorkSeconds3;
      }
      set
      {
        this._WorkSeconds3 = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropCount3")]
    public int DropCount3
    {
      get
      {
        return this._DropCount3;
      }
      set
      {
        this._DropCount3 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RecommandHeroMultiply")]
    public int RecommandHeroMultiply
    {
      get
      {
        return this._RecommandHeroMultiply;
      }
      set
      {
        this._RecommandHeroMultiply = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RecommandHeroAdd")]
    public int RecommandHeroAdd
    {
      get
      {
        return this._RecommandHeroAdd;
      }
      set
      {
        this._RecommandHeroAdd = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
