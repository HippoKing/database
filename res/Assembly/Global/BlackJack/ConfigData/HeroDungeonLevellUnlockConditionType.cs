﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroDungeonLevellUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroDungeonLevellUnlockConditionType")]
  public enum HeroDungeonLevellUnlockConditionType
  {
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_None", Value = 0)] HeroDungeonLevellUnlockConditionType_None,
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel,
  }
}
