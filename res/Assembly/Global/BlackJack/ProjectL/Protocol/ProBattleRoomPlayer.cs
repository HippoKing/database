﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBattleRoomPlayer")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBattleRoomPlayer : IExtensible
  {
    private string _UserId;
    private int _ChannelId;
    private string _Name;
    private int _HeadIcon;
    private int _Level;
    private int _PlayerBattleStatus;
    private ulong _SessionId;
    private bool _Offline;
    private readonly List<ProTrainingTech> _Techs;
    private ProRealTimePVPUserInfo _RealTimePVPInfo;
    private bool _Blessing;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleRoomPlayer()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "UserId")]
    public string UserId
    {
      get
      {
        return this._UserId;
      }
      set
      {
        this._UserId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      get
      {
        return this._ChannelId;
      }
      set
      {
        this._ChannelId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      get
      {
        return this._HeadIcon;
      }
      set
      {
        this._HeadIcon = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerBattleStatus")]
    public int PlayerBattleStatus
    {
      get
      {
        return this._PlayerBattleStatus;
      }
      set
      {
        this._PlayerBattleStatus = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SessionId")]
    public ulong SessionId
    {
      get
      {
        return this._SessionId;
      }
      set
      {
        this._SessionId = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "Offline")]
    public bool Offline
    {
      get
      {
        return this._Offline;
      }
      set
      {
        this._Offline = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "Techs")]
    public List<ProTrainingTech> Techs
    {
      get
      {
        return this._Techs;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = false, Name = "RealTimePVPInfo")]
    [DefaultValue(null)]
    public ProRealTimePVPUserInfo RealTimePVPInfo
    {
      get
      {
        return this._RealTimePVPInfo;
      }
      set
      {
        this._RealTimePVPInfo = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = false, Name = "Blessing")]
    [DefaultValue(false)]
    public bool Blessing
    {
      get
      {
        return this._Blessing;
      }
      set
      {
        this._Blessing = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
