﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.TeamRoomViewReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "TeamRoomViewReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class TeamRoomViewReq : IExtensible
  {
    private int _GameFunctionTypeId;
    private int _ChapterId;
    private int _LocationId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomViewReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      get
      {
        return this._GameFunctionTypeId;
      }
      set
      {
        this._GameFunctionTypeId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ChapterId")]
    public int ChapterId
    {
      get
      {
        return this._ChapterId;
      }
      set
      {
        this._ChapterId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LocationId")]
    [DefaultValue(0)]
    public int LocationId
    {
      get
      {
        return this._LocationId;
      }
      set
      {
        this._LocationId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
