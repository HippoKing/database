﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuildMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProGuildMember")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProGuildMember : IExtensible
  {
    private string _Name;
    private int _Level;
    private int _Title;
    private int _TotalActivities;
    private bool _Online;
    private long _LogoutTime;
    private int _TopHeroBattlePower;
    private string _UserId;
    private int _HeadIcon;
    private int _ThisWeekActivities;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMember()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Title")]
    public int Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalActivities")]
    public int TotalActivities
    {
      get
      {
        return this._TotalActivities;
      }
      set
      {
        this._TotalActivities = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Online")]
    public bool Online
    {
      get
      {
        return this._Online;
      }
      set
      {
        this._Online = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LogoutTime")]
    public long LogoutTime
    {
      get
      {
        return this._LogoutTime;
      }
      set
      {
        this._LogoutTime = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TopHeroBattlePower")]
    public int TopHeroBattlePower
    {
      get
      {
        return this._TopHeroBattlePower;
      }
      set
      {
        this._TopHeroBattlePower = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "UserId")]
    public string UserId
    {
      get
      {
        return this._UserId;
      }
      set
      {
        this._UserId = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      get
      {
        return this._HeadIcon;
      }
      set
      {
        this._HeadIcon = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ThisWeekActivities")]
    public int ThisWeekActivities
    {
      get
      {
        return this._ThisWeekActivities;
      }
      set
      {
        this._ThisWeekActivities = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
