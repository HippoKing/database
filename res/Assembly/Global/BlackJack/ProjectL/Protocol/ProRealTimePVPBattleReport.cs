﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRealTimePVPBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProRealTimePVPBattleReport")]
  [Serializable]
  public class ProRealTimePVPBattleReport : IExtensible
  {
    private int _Version;
    private ulong _InstanceId;
    private int _BattleType;
    private int _BattleId;
    private int _RandomSeed;
    private int _BPRule;
    private readonly List<ProBattleCommand> _Commands;
    private int _ReportType;
    private readonly List<ProRealTimePVPBattleReportPlayerData> _Datas;
    private bool _Win;
    private long _CreateTime;
    private bool _IsCancel;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "Version")]
    [DefaultValue(0)]
    public int Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [DefaultValue(0.0f)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleType")]
    [DefaultValue(0)]
    public int BattleType
    {
      get
      {
        return this._BattleType;
      }
      set
      {
        this._BattleType = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleId")]
    [DefaultValue(0)]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RandomSeed")]
    [DefaultValue(0)]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BPRule")]
    [DefaultValue(0)]
    public int BPRule
    {
      get
      {
        return this._BPRule;
      }
      set
      {
        this._BPRule = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Commands")]
    public List<ProBattleCommand> Commands
    {
      get
      {
        return this._Commands;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ReportType")]
    public int ReportType
    {
      get
      {
        return this._ReportType;
      }
      set
      {
        this._ReportType = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "Datas")]
    public List<ProRealTimePVPBattleReportPlayerData> Datas
    {
      get
      {
        return this._Datas;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Win")]
    public bool Win
    {
      get
      {
        return this._Win;
      }
      set
      {
        this._Win = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CreateTime")]
    [DefaultValue(0)]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsCancel")]
    public bool IsCancel
    {
      get
      {
        return this._IsCancel;
      }
      set
      {
        this._IsCancel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
