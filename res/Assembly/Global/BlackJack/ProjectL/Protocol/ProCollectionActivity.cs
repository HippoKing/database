﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProCollectionActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProCollectionActivity")]
  [Serializable]
  public class ProCollectionActivity : IExtensible
  {
    private ulong _ActivityInstanceId;
    private readonly List<int> _FinishedChallengeLevelIds;
    private int _LastFinishedScenarioId;
    private readonly List<int> _FinishedLootLevelIds;
    private int _CurrentWayPointId;
    private readonly List<ProCollectionActivityPlayerExchangeInfo> _ExchangeInfoList;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCollectionActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivityInstanceId")]
    public ulong ActivityInstanceId
    {
      get
      {
        return this._ActivityInstanceId;
      }
      set
      {
        this._ActivityInstanceId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "FinishedChallengeLevelIds")]
    public List<int> FinishedChallengeLevelIds
    {
      get
      {
        return this._FinishedChallengeLevelIds;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastFinishedScenarioId")]
    public int LastFinishedScenarioId
    {
      get
      {
        return this._LastFinishedScenarioId;
      }
      set
      {
        this._LastFinishedScenarioId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "FinishedLootLevelIds")]
    public List<int> FinishedLootLevelIds
    {
      get
      {
        return this._FinishedLootLevelIds;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentWayPointId")]
    public int CurrentWayPointId
    {
      get
      {
        return this._CurrentWayPointId;
      }
      set
      {
        this._CurrentWayPointId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "ExchangeInfoList")]
    public List<ProCollectionActivityPlayerExchangeInfo> ExchangeInfoList
    {
      get
      {
        return this._ExchangeInfoList;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
