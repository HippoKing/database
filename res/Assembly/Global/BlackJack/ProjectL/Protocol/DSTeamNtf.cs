﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSTeamNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSTeamNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSTeamNtf : IExtensible
  {
    private int _RoomId;
    private int _GameFunctionTypeId;
    private int _LocationId;
    private int _WaitingFunctionTypeId;
    private int _WaitingLocationId;
    private readonly List<ProTeamRoomInviteInfo> _InviteInfos;
    private long _LastInviteSendTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSTeamNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RoomId")]
    public int RoomId
    {
      get
      {
        return this._RoomId;
      }
      set
      {
        this._RoomId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      get
      {
        return this._GameFunctionTypeId;
      }
      set
      {
        this._GameFunctionTypeId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LocationId")]
    public int LocationId
    {
      get
      {
        return this._LocationId;
      }
      set
      {
        this._LocationId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "WaitingFunctionTypeId")]
    public int WaitingFunctionTypeId
    {
      get
      {
        return this._WaitingFunctionTypeId;
      }
      set
      {
        this._WaitingFunctionTypeId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "WaitingLocationId")]
    public int WaitingLocationId
    {
      get
      {
        return this._WaitingLocationId;
      }
      set
      {
        this._WaitingLocationId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "InviteInfos")]
    public List<ProTeamRoomInviteInfo> InviteInfos
    {
      get
      {
        return this._InviteInfos;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LastInviteSendTime")]
    [DefaultValue(0)]
    public long LastInviteSendTime
    {
      get
      {
        return this._LastInviteSendTime;
      }
      set
      {
        this._LastInviteSendTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
