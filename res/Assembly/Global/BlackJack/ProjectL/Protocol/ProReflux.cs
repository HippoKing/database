﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProReflux
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProReflux")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProReflux : IExtensible
  {
    private int _MissionPoints;
    private readonly List<int> _RewardClaimedSlots;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProReflux()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionPoints")]
    public int MissionPoints
    {
      get
      {
        return this._MissionPoints;
      }
      set
      {
        this._MissionPoints = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "RewardClaimedSlots")]
    public List<int> RewardClaimedSlots
    {
      get
      {
        return this._RewardClaimedSlots;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
