﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalConfig
  {
    private static LocalConfig s_instance;
    private static string m_fileName;
    private LocalConfigData m_data;
    private const int m_maxRecentLoginServerCount = 2;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFileName(string name)
    {
      LocalConfig.m_fileName = name;
    }

    public bool IsAgreeDataNotice
    {
      get
      {
        return this.m_data.IsAgreeDataNotice;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Save()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyVolume(string category, bool isInitialize = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Apply(bool isInitialize = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRecentLoginServerID(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattle(BattleType battleType, bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAutoBattle(BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearAutoBattle()
    {
      this.m_data.AutoBattleBits = 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestUIId(int testType, int testId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestUIId(int testType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleDanmakuState(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListSortType(int type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsOnlyShowCurJobSkin(bool onlyShowCurJobSkin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsSetSkinToAllSoldier(bool setSkinToAllSoldier)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGMUser(bool isGMUser)
    {
      // ISSUE: unable to decompile the method.
    }

    public LocalConfigData Data
    {
      get
      {
        return this.m_data;
      }
    }

    public static LocalConfig Instance
    {
      set
      {
        LocalConfig.s_instance = value;
      }
      get
      {
        return LocalConfig.s_instance;
      }
    }
  }
}
