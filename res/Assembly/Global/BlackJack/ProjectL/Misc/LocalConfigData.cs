﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalConfigData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalConfigData
  {
    public bool MusicOn;
    public bool SoundOn;
    public bool VoiceOn;
    public bool IsFasBattle;
    public int SkipCombatMode;
    public bool ClickActorEndAction;
    public bool ShowBattleDanmaku;
    public int AutoBattleBits;
    public bool OnlyShowCurJobSkin;
    public bool SetSkinToAllSoldier;
    public int HeroListSortType;
    public bool NotifyEnergyRecover;
    public bool NotifyRandomEvent;
    public bool NotifyArenaTicket;
    public bool NotifyStoreRefresh;
    public bool PowerSaveModeOn;
    public bool ScreenRecordOn;
    public int[] RecentLoginServerIDs;
    public string AccountName;
    public string Password;
    public bool IsDeveloper;
    public bool IsFastEnterGame;
    public bool EnableUserGuide;
    public int TestListType;
    public int[] TestListItemIds;
    public int TestMonsterLevel;
    public bool TestMarginFix;
    public string LastBecomeStrongHeroStarLevel;
    public string LastBecomeStrongPlayerLevel;
    public string LastBecomeStrongEquipmentLevel;
    public float MusicVolume;
    public float SoundVolumn;
    public float VoiceVolume;
    public bool IsAgreeDataNotice;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
