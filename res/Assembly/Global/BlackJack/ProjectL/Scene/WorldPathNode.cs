﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldPathNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class WorldPathNode
  {
    public int m_waypointId;
    public Vector2 m_position;

    public bool IsSameState(WorldPathNode rhs)
    {
      return this.m_waypointId == rhs.m_waypointId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetGoalHeuristic(WorldPathfinder pathfinder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetCost(WorldPathfinder pathfinder, WorldPathNode successor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetSuccessors(WorldPathfinder pathfinder, WorldPathNode parentNode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSuccessor(WorldPathfinder pathfinder, int id, WorldPathNode parentNode)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
