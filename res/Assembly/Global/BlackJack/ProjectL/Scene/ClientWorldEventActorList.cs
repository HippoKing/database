﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldEventActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldEventActorList
  {
    public static void RemoveAll(List<ClientWorldEventActor> list)
    {
      EntityList.RemoveAll<ClientWorldEventActor>(list);
    }

    public static void RemoveDeleted(List<ClientWorldEventActor> list)
    {
      EntityList.RemoveDeleted<ClientWorldEventActor>(list);
    }

    public static void Tick(List<ClientWorldEventActor> list)
    {
      EntityList.Tick<ClientWorldEventActor>(list);
    }

    public static void TickGraphic(List<ClientWorldEventActor> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldEventActor>(list, dt);
    }

    public static void Draw(List<ClientWorldEventActor> list)
    {
      EntityList.Draw<ClientWorldEventActor>(list);
    }

    public static void Pause(List<ClientWorldEventActor> list, bool pause)
    {
      EntityList.Pause<ClientWorldEventActor>(list, pause);
    }
  }
}
