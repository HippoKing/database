﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldRegionList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldRegionList
  {
    public static void RemoveAll(List<ClientWorldRegion> list)
    {
      EntityList.RemoveAll<ClientWorldRegion>(list);
    }

    public static void RemoveDeleted(List<ClientWorldRegion> list)
    {
      EntityList.RemoveDeleted<ClientWorldRegion>(list);
    }

    public static void Tick(List<ClientWorldRegion> list)
    {
      EntityList.Tick<ClientWorldRegion>(list);
    }

    public static void TickGraphic(List<ClientWorldRegion> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldRegion>(list, dt);
    }

    public static void Draw(List<ClientWorldRegion> list)
    {
      EntityList.Draw<ClientWorldRegion>(list);
    }

    public static void Pause(List<ClientWorldRegion> list, bool pause)
    {
      EntityList.Pause<ClientWorldRegion>(list, pause);
    }
  }
}
