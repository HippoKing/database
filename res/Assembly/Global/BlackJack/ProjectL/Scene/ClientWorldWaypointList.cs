﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldWaypointList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldWaypointList
  {
    public static void RemoveAll(List<ClientWorldWaypoint> list)
    {
      EntityList.RemoveAll<ClientWorldWaypoint>(list);
    }

    public static void RemoveDeleted(List<ClientWorldWaypoint> list)
    {
      EntityList.RemoveDeleted<ClientWorldWaypoint>(list);
    }

    public static void Tick(List<ClientWorldWaypoint> list)
    {
      EntityList.Tick<ClientWorldWaypoint>(list);
    }

    public static void TickGraphic(List<ClientWorldWaypoint> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldWaypoint>(list, dt);
    }

    public static void Draw(List<ClientWorldWaypoint> list)
    {
      EntityList.Draw<ClientWorldWaypoint>(list);
    }

    public static void Pause(List<ClientWorldWaypoint> list, bool pause)
    {
      EntityList.Pause<ClientWorldWaypoint>(list, pause);
    }
  }
}
