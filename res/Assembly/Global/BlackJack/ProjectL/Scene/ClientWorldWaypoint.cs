﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldWaypoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.UI;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class ClientWorldWaypoint : Entity
  {
    private ClientWorld m_clientWorld;
    private ConfigDataWaypointInfo m_waypointInfo;
    private WayPointStatus m_status;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private bool m_isVisible;
    private WorldWaypointUIController m_uiController;
    private CommonUIStateController m_graphicUIStateController;
    private Vector3 m_uiInitScale;
    private float m_graphicInitScale;
    private bool m_isPointerDown;
    private float m_scale;
    [DoNotToLua]
    private ClientWorldWaypoint.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeClientWorldConfigDataWaypointInfo_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_DrawArrowVector3Vector3Color_hotfix;
    private LuaFunction m_DoPauseBoolean_hotfix;
    private LuaFunction m_LocateVector2_hotfix;
    private LuaFunction m_SetStatusWayPointStatus_hotfix;
    private LuaFunction m_GetStatus_hotfix;
    private LuaFunction m_SetScenarioStateConfigDataWaypointInfoConfigDataScenarioInfo_hotfix;
    private LuaFunction m_SetGraphicStateString_hotfix;
    private LuaFunction m_SetCanClickBoolean_hotfix;
    private LuaFunction m_GetClickTransform_hotfix;
    private LuaFunction m_PlayAnimationStringBoolean_hotfix;
    private LuaFunction m_SetVisibleBoolean_hotfix;
    private LuaFunction m_IsVisible_hotfix;
    private LuaFunction m_SetUISiblingIndexInt32_hotfix;
    private LuaFunction m_OnUIPointerDown_hotfix;
    private LuaFunction m_OnUIPointerUp_hotfix;
    private LuaFunction m_OnUIClick_hotfix;
    private LuaFunction m_get_Position_hotfix;
    private LuaFunction m_get_WaypointInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientWorld world, ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawArrow(Vector3 p0, Vector3 p1, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(WayPointStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScenarioState(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataScenarioInfo lastFinishedScenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetClickTransform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUISiblingIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIPointerDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIPointerUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2 Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataWaypointInfo WaypointInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ClientWorldWaypoint.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Dispose()
    {
      base.Dispose();
    }

    private void __callBase_Tick()
    {
      base.Tick();
    }

    private void __callBase_TickGraphic(float dt)
    {
      base.TickGraphic(dt);
    }

    private void __callBase_Draw()
    {
      base.Draw();
    }

    private void __callBase_Pause(bool pause)
    {
      this.Pause(pause);
    }

    private void __callBase_DoPause(bool pause)
    {
      base.DoPause(pause);
    }

    private void __callBase_DeleteMe()
    {
      this.DeleteMe();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ClientWorldWaypoint m_owner;

      public LuaExportHelper(ClientWorldWaypoint owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Dispose()
      {
        this.m_owner.__callBase_Dispose();
      }

      public void __callBase_Tick()
      {
        this.m_owner.__callBase_Tick();
      }

      public void __callBase_TickGraphic(float dt)
      {
        this.m_owner.__callBase_TickGraphic(dt);
      }

      public void __callBase_Draw()
      {
        this.m_owner.__callBase_Draw();
      }

      public void __callBase_Pause(bool pause)
      {
        this.m_owner.__callBase_Pause(pause);
      }

      public void __callBase_DoPause(bool pause)
      {
        this.m_owner.__callBase_DoPause(pause);
      }

      public void __callBase_DeleteMe()
      {
        this.m_owner.__callBase_DeleteMe();
      }

      public ClientWorld m_clientWorld
      {
        get
        {
          return this.m_owner.m_clientWorld;
        }
        set
        {
          this.m_owner.m_clientWorld = value;
        }
      }

      public ConfigDataWaypointInfo m_waypointInfo
      {
        get
        {
          return this.m_owner.m_waypointInfo;
        }
        set
        {
          this.m_owner.m_waypointInfo = value;
        }
      }

      public WayPointStatus m_status
      {
        get
        {
          return this.m_owner.m_status;
        }
        set
        {
          this.m_owner.m_status = value;
        }
      }

      public GenericGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public Vector2 m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public bool m_isVisible
      {
        get
        {
          return this.m_owner.m_isVisible;
        }
        set
        {
          this.m_owner.m_isVisible = value;
        }
      }

      public WorldWaypointUIController m_uiController
      {
        get
        {
          return this.m_owner.m_uiController;
        }
        set
        {
          this.m_owner.m_uiController = value;
        }
      }

      public CommonUIStateController m_graphicUIStateController
      {
        get
        {
          return this.m_owner.m_graphicUIStateController;
        }
        set
        {
          this.m_owner.m_graphicUIStateController = value;
        }
      }

      public Vector3 m_uiInitScale
      {
        get
        {
          return this.m_owner.m_uiInitScale;
        }
        set
        {
          this.m_owner.m_uiInitScale = value;
        }
      }

      public float m_graphicInitScale
      {
        get
        {
          return this.m_owner.m_graphicInitScale;
        }
        set
        {
          this.m_owner.m_graphicInitScale = value;
        }
      }

      public bool m_isPointerDown
      {
        get
        {
          return this.m_owner.m_isPointerDown;
        }
        set
        {
          this.m_owner.m_isPointerDown = value;
        }
      }

      public float m_scale
      {
        get
        {
          return this.m_owner.m_scale;
        }
        set
        {
          this.m_owner.m_scale = value;
        }
      }

      public void DrawArrow(Vector3 p0, Vector3 p1, Color color)
      {
        this.m_owner.DrawArrow(p0, p1, color);
      }

      public void PlayAnimation(string name, bool loop)
      {
        this.m_owner.PlayAnimation(name, loop);
      }

      public void OnUIPointerDown()
      {
        this.m_owner.OnUIPointerDown();
      }

      public void OnUIPointerUp()
      {
        this.m_owner.OnUIPointerUp();
      }

      public void OnUIClick()
      {
        this.m_owner.OnUIClick();
      }
    }
  }
}
