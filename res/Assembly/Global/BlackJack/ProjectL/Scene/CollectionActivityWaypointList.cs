﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityWaypointList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class CollectionActivityWaypointList
  {
    public static void RemoveAll(List<CollectionActivityWaypoint> list)
    {
      EntityList.RemoveAll<CollectionActivityWaypoint>(list);
    }

    public static void RemoveDeleted(List<CollectionActivityWaypoint> list)
    {
      EntityList.RemoveDeleted<CollectionActivityWaypoint>(list);
    }

    public static void Tick(List<CollectionActivityWaypoint> list)
    {
      EntityList.Tick<CollectionActivityWaypoint>(list);
    }

    public static void TickGraphic(List<CollectionActivityWaypoint> list, float dt)
    {
      EntityList.TickGraphic<CollectionActivityWaypoint>(list, dt);
    }

    public static void Draw(List<CollectionActivityWaypoint> list)
    {
      EntityList.Draw<CollectionActivityWaypoint>(list);
    }

    public static void Pause(List<CollectionActivityWaypoint> list, bool pause)
    {
      EntityList.Pause<CollectionActivityWaypoint>(list, pause);
    }
  }
}
