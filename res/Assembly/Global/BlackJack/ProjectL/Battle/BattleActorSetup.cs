﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleActorSetup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattleActorSetup
  {
    public ConfigDataHeroInfo HeroInfo;
    public ConfigDataJobConnectionInfo JobConnectionInfo;
    public ConfigDataSoldierInfo SoldierInfo;
    public ConfigDataSkillInfo[] SkillInfos;
    public ConfigDataJobInfo[] MasterJobInfos;
    public BattleActorEquipment[] Equipments;
    public ConfigDataSkillInfo[] ResonanceSkillInfos;
    public ConfigDataSkillInfo[] FetterSkillInfos;
    public int HeroLevel;
    public int HeroStar;
    public int JobLevel;
    public int SoldierCount;
    public GridPosition Position;
    public int Direction;
    public bool IsNpc;
    public int BehaviorId;
    public int GroupId;
    public int ActionValue;
    public ConfigDataCharImageSkinResourceInfo HeroCharImageSkinResourceInfo;
    public ConfigDataModelSkinResourceInfo HeroModelSkinResourceInfo;
    public ConfigDataModelSkinResourceInfo SoldierModelSkinResourceInfo;
    public List<ConfigDataSkillInfo> ExtraPassiveSkillInfos;
    public ConfigDataSkillInfo ExtraTalentSkillInfo;
    public int HeroHealthPoint;
    public int SoldierHealthPoint;
    public int ActorId;
    public int PlayerIndex;
    [DoNotToLua]
    private BattleActorSetup.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSetup()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleActorSetup.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleActorSetup m_owner;

      public LuaExportHelper(BattleActorSetup owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
