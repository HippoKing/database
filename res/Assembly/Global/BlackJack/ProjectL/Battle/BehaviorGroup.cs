﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BehaviorGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BehaviorGroup
  {
    private int m_ID;
    private BattleTeam m_team;
    private List<BattleActor> m_actors;
    private ConfigDataGroupBehavior m_curBehaviorCfg;
    private BattleActor m_leader;
    private int m_leaderNormalActionPriority;
    private List<int> m_singleBehaviors;
    [DoNotToLua]
    private BehaviorGroup.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorInt32BattleTeam_hotfix;
    private LuaFunction m_get_ID_hotfix;
    private LuaFunction m_AddActorBattleActor_hotfix;
    private LuaFunction m_get_Leader_hotfix;
    private LuaFunction m_SetBahviorInt32_hotfix;
    private LuaFunction m_get_Behavior_hotfix;
    private LuaFunction m_get_Actors_hotfix;
    private LuaFunction m_get_InstanceID_hotfix;
    private LuaFunction m_CheckGroupMemberDead_hotfix;
    private LuaFunction m_DoBehavor_hotfix;
    private LuaFunction m_DoOtherBehavorCheckBehaviorCondition_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorGroup(int id, BattleTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleActor Leader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBahvior(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataGroupBehavior Behavior
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleActor> Actors
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string InstanceID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckGroupMemberDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoBehavor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoOtherBehavorCheck(BehaviorCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BehaviorGroup.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BehaviorGroup m_owner;

      public LuaExportHelper(BehaviorGroup owner)
      {
        this.m_owner = owner;
      }

      public int m_ID
      {
        get
        {
          return this.m_owner.m_ID;
        }
        set
        {
          this.m_owner.m_ID = value;
        }
      }

      public BattleTeam m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public List<BattleActor> m_actors
      {
        get
        {
          return this.m_owner.m_actors;
        }
        set
        {
          this.m_owner.m_actors = value;
        }
      }

      public ConfigDataGroupBehavior m_curBehaviorCfg
      {
        get
        {
          return this.m_owner.m_curBehaviorCfg;
        }
        set
        {
          this.m_owner.m_curBehaviorCfg = value;
        }
      }

      public BattleActor m_leader
      {
        get
        {
          return this.m_owner.m_leader;
        }
        set
        {
          this.m_owner.m_leader = value;
        }
      }

      public int m_leaderNormalActionPriority
      {
        get
        {
          return this.m_owner.m_leaderNormalActionPriority;
        }
        set
        {
          this.m_owner.m_leaderNormalActionPriority = value;
        }
      }

      public List<int> m_singleBehaviors
      {
        get
        {
          return this.m_owner.m_singleBehaviors;
        }
        set
        {
          this.m_owner.m_singleBehaviors = value;
        }
      }

      public bool CheckGroupMemberDead()
      {
        return this.m_owner.CheckGroupMemberDead();
      }

      public void DoOtherBehavorCheck(BehaviorCondition condition)
      {
        this.m_owner.DoOtherBehavorCheck(condition);
      }
    }
  }
}
