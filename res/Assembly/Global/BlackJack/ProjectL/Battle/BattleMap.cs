﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleMap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class BattleMap
  {
    public const int MoveRegion = 1;
    public const int AttackRegion = 2;
    public const int SkillRegion = 3;
    public const int TeleportRegion = 4;
    private int m_width;
    private int m_height;
    [DoNotToLua]
    private BattleMapCell[,] m_cells;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(int w, int h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitializeTerrain(
      ConfigDataBattlefieldInfo battlefieldInfo,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsValidPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActor(GridPosition p, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTerrain(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetTerrain(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegion(GridPosition p, int region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRegion(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegionBit(GridPosition p, int bit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRegionBit(GridPosition p, int bit)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasRegionBit(GridPosition p, int bit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMovePointCost(GridPosition p, MoveType moveType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetMovePointCost(ConfigDataTerrainInfo terrain, MoveType moveType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleMapCell GetCell(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Width
    {
      get
      {
        return this.m_width;
      }
    }

    public int Height
    {
      get
      {
        return this.m_height;
      }
    }
  }
}
