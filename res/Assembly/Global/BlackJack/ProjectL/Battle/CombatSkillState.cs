﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatSkillState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class CombatSkillState
  {
    public ConfigDataSkillInfo m_skillInfo;
    public ushort m_hitId;
    public int m_hitCount;
    public int m_preAttackHitCount;
    public List<DelayHit> m_delayHits;
    public bool m_isToHeroCritical;
    public bool m_isToSoldierCritical;
    [DoNotToLua]
    private CombatSkillState.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_AddDelayHitInt32Vector2iCombatActor_hotfix;
    private LuaFunction m_GetDelayHitFrameMax_hotfix;
    private LuaFunction m_IsCriticalBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatSkillState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDelayHit(int frame, Vector2i pos, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDelayHitFrameMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCritical(bool targetIsHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CombatSkillState.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CombatSkillState m_owner;

      public LuaExportHelper(CombatSkillState owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
