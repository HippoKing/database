﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattleTeam
  {
    private int m_initNotNpcActorCount;
    private int m_deadActorCount;
    private List<BattleActor> m_actors;
    private BattleBase m_battle;
    private int m_teamNumber;
    private Dictionary<int, int> m_groupBehaviorDict;
    private List<BehaviorGroup> m_groups;
    [DoNotToLua]
    private BattleTeam.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorBattleBaseInt32_hotfix;
    private LuaFunction m_RemoveDeleted_hotfix;
    private LuaFunction m_CreateActor_hotfix;
    private LuaFunction m_RemoveAll_hotfix;
    private LuaFunction m_GetActors_hotfix;
    private LuaFunction m_GetActorsWithoutIdInt32_hotfix;
    private LuaFunction m_GetActorByIdInt32_hotfix;
    private LuaFunction m_GetActorByHeroIdInt32Boolean_hotfix;
    private LuaFunction m_StartBattle_hotfix;
    private LuaFunction m_StopBattleBoolean_hotfix;
    private LuaFunction m_HasAliveActor_hotfix;
    private LuaFunction m_HasNotActionFinishedActor_hotfix;
    private LuaFunction m_HasFinishActionNpcActor_hotfix;
    private LuaFunction m_GetOtherTeam_hotfix;
    private LuaFunction m_OnMyActorDieBattleActor_hotfix;
    private LuaFunction m_OnMyActorRetreatBattleActorInt32StringBoolean_hotfix;
    private LuaFunction m_ComputeDeadActorCountList`1NpcCondition_hotfix;
    private LuaFunction m_ComputeAliveActorCountList`1NpcCondition_hotfix;
    private LuaFunction m_IsGroupDieInt32_hotfix;
    private LuaFunction m_NextTurn_hotfix;
    private LuaFunction m_get_Battle_hotfix;
    private LuaFunction m_get_TeamNumber_hotfix;
    private LuaFunction m_get_InitNotNpcActorCount_hotfix;
    private LuaFunction m_get_DeadActorCount_hotfix;
    private LuaFunction m_SetGroupBehaviorDictDictionary`2_hotfix;
    private LuaFunction m_AddGroupBehaviorsDictionary`2_hotfix;
    private LuaFunction m_CreateGroups_hotfix;
    private LuaFunction m_GetGroupInt32_hotfix;
    private LuaFunction m_CreateGroupInt32_hotfix;
    private LuaFunction m_DoGroupBehavior_hotfix;
    private LuaFunction m_UpdateGroups_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam(BattleBase battle, int teamNumer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveDeleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor CreateActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetActorsWithoutId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorByHeroId(int heroId, bool ignoreDead = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAliveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasNotActionFinishedActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFinishActionNpcActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam GetOtherTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorRetreat(
      BattleActor actor,
      int effectType,
      string fxName,
      bool notifyListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeDeadActorCount(List<int> heroIds, NpcCondition npcCondition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeAliveActorCount(List<int> heroIds, NpcCondition npcCondition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGroupDie(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int InitNotNpcActorCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DeadActorCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupBehaviorDict(Dictionary<int, int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGroupBehaviors(Dictionary<int, int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorGroup GetGroup(int groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorGroup CreateGroup(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoGroupBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleTeam.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleTeam m_owner;

      public LuaExportHelper(BattleTeam owner)
      {
        this.m_owner = owner;
      }

      public int m_initNotNpcActorCount
      {
        get
        {
          return this.m_owner.m_initNotNpcActorCount;
        }
        set
        {
          this.m_owner.m_initNotNpcActorCount = value;
        }
      }

      public int m_deadActorCount
      {
        get
        {
          return this.m_owner.m_deadActorCount;
        }
        set
        {
          this.m_owner.m_deadActorCount = value;
        }
      }

      public List<BattleActor> m_actors
      {
        get
        {
          return this.m_owner.m_actors;
        }
        set
        {
          this.m_owner.m_actors = value;
        }
      }

      public BattleBase m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public int m_teamNumber
      {
        get
        {
          return this.m_owner.m_teamNumber;
        }
        set
        {
          this.m_owner.m_teamNumber = value;
        }
      }

      public Dictionary<int, int> m_groupBehaviorDict
      {
        get
        {
          return this.m_owner.m_groupBehaviorDict;
        }
        set
        {
          this.m_owner.m_groupBehaviorDict = value;
        }
      }

      public List<BehaviorGroup> m_groups
      {
        get
        {
          return this.m_owner.m_groups;
        }
        set
        {
          this.m_owner.m_groups = value;
        }
      }

      public BehaviorGroup GetGroup(int groupID)
      {
        return this.m_owner.GetGroup(groupID);
      }

      public BehaviorGroup CreateGroup(int id)
      {
        return this.m_owner.CreateGroup(id);
      }
    }
  }
}
