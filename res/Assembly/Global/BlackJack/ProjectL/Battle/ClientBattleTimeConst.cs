﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ClientBattleTimeConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class ClientBattleTimeConst
  {
    public const int StartCombatEffectTime = 500;
    public const int StopCombatEffectTime = 500;
    public const int SkillFadeTime = 300;
    public const int ActorActStartCombatTime = 700;
    public const int ActorActStopCombatTime = 300;
    public const int ActorActImmuneTime = 500;
    public const int ActorActStartGuardDelay = 800;
    public const int ActorActStopGuardDelay = 300;
    public const int ActorActPassiveSkillTime = 1000;
    public const int ActorActPassiveSkillHitTime = 400;
    public const int ActorActAppearTime = 500;
    public const int ActorActDisappearTime = 500;
    public const int ActorActTelelportDisappearDelayTime = 500;
    public const int ActorActTelelportAppearDelayTime = 500;
    public const int ActorActSummonDelayTime = 500;
    public const int ActorActChangeTeamTime = 500;
    public const int ActorActPunchMoveTime = 400;
    public const int ActorActExchangeMoveTime = 300;
    public const int ActorActReplaceTime = 500;
    public const int ActorActDieTime = 300;
    public const int ActorActDieHideTime = 200;
    public const int TargetIconTime = 500;
    public const float FastBattleTimeScale = 1.3f;
  }
}
