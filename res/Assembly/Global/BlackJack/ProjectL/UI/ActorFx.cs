﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActorFx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ActorFx
  {
    public const string Death1 = "Spine/Effect_ABS/effect_death/EffectDeath1Prefab.prefab";
    public const string Death2 = "Spine/Effect_ABS/effect_death/EffectDeath2Prefab.prefab";
    public const string Death = "FX/Common_ABS/CommonFX/common_die.prefab";
    public const string PlayerTeleportDisappear = "FX/Common_ABS/CommonFX/common_Teleport_2start.prefab";
    public const string PlayerTeleportAppear = "FX/Common_ABS/CommonFX/common_Teleport_2end.prefab";
    public const string EventAppear = "FX/Common_ABS/CommonFX/common_Refresh.prefab";
    public const string StagePosition0A = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchMovePlaceEmpty_G.prefab";
    public const string StagePosition0B = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchMovePlaceInMan_G.prefab";
    public const string StagePosition0C = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchEnd_G.prefab";
    public const string StagePosition1A = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchMovePlaceEmpty_R.prefab";
    public const string StagePosition1B = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchMovePlaceInMan_R.prefab";
    public const string StagePosition1C = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchEnd_R.prefab";
    public const string StagePosition2A = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchMovePlaceEmpty_B.prefab";
    public const string StagePosition2B = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchMovePlaceInMan_B.prefab";
    public const string StagePosition2C = "FX/Common_ABS/CommonFX/OnTouchMovePlaceInMan/OnTouchEnd_B.prefab";
  }
}
