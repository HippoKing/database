﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityExchangeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CollectionActivityExchangeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_openServiceActivityStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ItemList/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendButton;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Detail/TitleToggleGroup/ViewPort/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pageContent;
    [AutoBind("./Detail/QuestGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_missionsScrollRect;
    [AutoBind("./Detail/QuestGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionsScrollViewContent;
    [AutoBind("./Prafabs", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_prefabPool;
    [AutoBind("./Detail/ResidueTimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_residueTimeValueText;
    [AutoBind("./ItemList/ItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyRoot;
    [AutoBind("./Detail/RightTop/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    private IConfigDataLoader m_configDataLoader;
    private List<CollectionActivityExchangePageUIController> m_pageCtrlList;
    private List<CollectionActivityExchangeMissionItemUIController> m_missionItemCtrlList;
    private List<CollectionActivityExchangeCurrencyItemUIController> m_currencyItemCtrlList;
    private HeroCharUIController m_heroCharUIController;
    private int m_selectedPageIndex;
    private List<CollectionActivityPage> m_cachedPageList;
    private List<CollectionActivityCurrency> m_cachedResourceList;
    private const string c_pageItemPoolName = "DayTabItem";
    private const string c_consumeItemPoolName = "ConsumeItem";
    private const string c_missionItemPoolName = "MissionItem";
    private const string c_currencyItemPoolName = "ResourceItem";
    [DoNotToLua]
    private CollectionActivityExchangeUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_InitPrefabPools_hotfix;
    private LuaFunction m_OnObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_UpdateViewCollectionActivityExchangeModel_hotfix;
    private LuaFunction m_ResetView_hotfix;
    private LuaFunction m_ResetPageSelect_hotfix;
    private LuaFunction m_OnPageButtonClickInt32_hotfix;
    private LuaFunction m_SelectPageInt32_hotfix;
    private LuaFunction m_SetPageCtrlSelectedInt32Boolean_hotfix;
    private LuaFunction m_GetPageCtrlInt32_hotfix;
    private LuaFunction m_UpdatePages_hotfix;
    private LuaFunction m_UpdateCoverHero_hotfix;
    private LuaFunction m_UpdateMissionItemsBoolean_hotfix;
    private LuaFunction m_UpdateCurrencyItems_hotfix;
    private LuaFunction m_OnMissionGetFromConsumeItemPool_hotfix;
    private LuaFunction m_OnMissionReturnFromConsumeItemPoolCollectionAcitivityExchangeConsumeItemUIController_hotfix;
    private LuaFunction m_OnCheckPageCanClickInt32_hotfix;
    private LuaFunction m_OnMissionGetButtonClickCollectionActivityExchangeMissionItemUIController_hotfix;
    private LuaFunction m_SetResidueTimeTimeSpan_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnRecommendButtonClicked_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnRecommendButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnRecommendButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnMissionGetAction`1_hotfix;
    private LuaFunction m_remove_EventOnMissionGetAction`1_hotfix;
    private LuaFunction m_add_FuncOnCheckPageCanClickFunc`2_hotfix;
    private LuaFunction m_remove_FuncOnCheckPageCanClickFunc`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPrefabPools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnObjectCreated(string poolName, GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(CollectionActivityExchangeModel model)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetPageSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPageButtonClick(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectPage(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPageCtrlSelected(int index, bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityExchangePageUIController GetPageCtrl(
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePages()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCoverHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionItems(bool needResetScroll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrencyItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionAcitivityExchangeConsumeItemUIController OnMissionGetFromConsumeItemPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionReturnFromConsumeItemPool(
      CollectionAcitivityExchangeConsumeItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool OnCheckPageCanClick(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionGetButtonClick(
      CollectionActivityExchangeMissionItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetResidueTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRecommendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionActivityExchangeMissionItemUIController> EventOnMissionGet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<int, bool> FuncOnCheckPageCanClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityExchangeUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRecommendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRecommendButtonClick()
    {
      this.EventOnRecommendButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMissionGet(
      CollectionActivityExchangeMissionItemUIController obj)
    {
    }

    private void __clearDele_EventOnMissionGet(
      CollectionActivityExchangeMissionItemUIController obj)
    {
      this.EventOnMissionGet = (Action<CollectionActivityExchangeMissionItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool __callDele_FuncOnCheckPageCanClick(int arg1)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_FuncOnCheckPageCanClick(int arg1)
    {
      this.FuncOnCheckPageCanClick = (Func<int, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityExchangeUIController m_owner;

      public LuaExportHelper(CollectionActivityExchangeUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnRecommendButtonClick()
      {
        this.m_owner.__callDele_EventOnRecommendButtonClick();
      }

      public void __clearDele_EventOnRecommendButtonClick()
      {
        this.m_owner.__clearDele_EventOnRecommendButtonClick();
      }

      public void __callDele_EventOnMissionGet(
        CollectionActivityExchangeMissionItemUIController obj)
      {
        this.m_owner.__callDele_EventOnMissionGet(obj);
      }

      public void __clearDele_EventOnMissionGet(
        CollectionActivityExchangeMissionItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnMissionGet(obj);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool __callDele_FuncOnCheckPageCanClick(int arg1)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __clearDele_FuncOnCheckPageCanClick(int arg1)
      {
        this.m_owner.__clearDele_FuncOnCheckPageCanClick(arg1);
      }

      public CommonUIStateController m_openServiceActivityStateCtrl
      {
        get
        {
          return this.m_owner.m_openServiceActivityStateCtrl;
        }
        set
        {
          this.m_owner.m_openServiceActivityStateCtrl = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_recommendButton
      {
        get
        {
          return this.m_owner.m_recommendButton;
        }
        set
        {
          this.m_owner.m_recommendButton = value;
        }
      }

      public GameObject m_charGameObject
      {
        get
        {
          return this.m_owner.m_charGameObject;
        }
        set
        {
          this.m_owner.m_charGameObject = value;
        }
      }

      public GameObject m_pageContent
      {
        get
        {
          return this.m_owner.m_pageContent;
        }
        set
        {
          this.m_owner.m_pageContent = value;
        }
      }

      public ScrollRect m_missionsScrollRect
      {
        get
        {
          return this.m_owner.m_missionsScrollRect;
        }
        set
        {
          this.m_owner.m_missionsScrollRect = value;
        }
      }

      public GameObject m_missionsScrollViewContent
      {
        get
        {
          return this.m_owner.m_missionsScrollViewContent;
        }
        set
        {
          this.m_owner.m_missionsScrollViewContent = value;
        }
      }

      public EasyObjectPool m_prefabPool
      {
        get
        {
          return this.m_owner.m_prefabPool;
        }
        set
        {
          this.m_owner.m_prefabPool = value;
        }
      }

      public Text m_residueTimeValueText
      {
        get
        {
          return this.m_owner.m_residueTimeValueText;
        }
        set
        {
          this.m_owner.m_residueTimeValueText = value;
        }
      }

      public GameObject m_currencyRoot
      {
        get
        {
          return this.m_owner.m_currencyRoot;
        }
        set
        {
          this.m_owner.m_currencyRoot = value;
        }
      }

      public Text m_titleText
      {
        get
        {
          return this.m_owner.m_titleText;
        }
        set
        {
          this.m_owner.m_titleText = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public List<CollectionActivityExchangePageUIController> m_pageCtrlList
      {
        get
        {
          return this.m_owner.m_pageCtrlList;
        }
        set
        {
          this.m_owner.m_pageCtrlList = value;
        }
      }

      public List<CollectionActivityExchangeMissionItemUIController> m_missionItemCtrlList
      {
        get
        {
          return this.m_owner.m_missionItemCtrlList;
        }
        set
        {
          this.m_owner.m_missionItemCtrlList = value;
        }
      }

      public List<CollectionActivityExchangeCurrencyItemUIController> m_currencyItemCtrlList
      {
        get
        {
          return this.m_owner.m_currencyItemCtrlList;
        }
        set
        {
          this.m_owner.m_currencyItemCtrlList = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public int m_selectedPageIndex
      {
        get
        {
          return this.m_owner.m_selectedPageIndex;
        }
        set
        {
          this.m_owner.m_selectedPageIndex = value;
        }
      }

      public List<CollectionActivityPage> m_cachedPageList
      {
        get
        {
          return this.m_owner.m_cachedPageList;
        }
        set
        {
          this.m_owner.m_cachedPageList = value;
        }
      }

      public List<CollectionActivityCurrency> m_cachedResourceList
      {
        get
        {
          return this.m_owner.m_cachedResourceList;
        }
        set
        {
          this.m_owner.m_cachedResourceList = value;
        }
      }

      public static string c_pageItemPoolName
      {
        get
        {
          return "DayTabItem";
        }
      }

      public static string c_consumeItemPoolName
      {
        get
        {
          return "ConsumeItem";
        }
      }

      public static string c_missionItemPoolName
      {
        get
        {
          return "MissionItem";
        }
      }

      public static string c_currencyItemPoolName
      {
        get
        {
          return "ResourceItem";
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitPrefabPools()
      {
        this.m_owner.InitPrefabPools();
      }

      public void OnObjectCreated(string poolName, GameObject obj)
      {
        this.m_owner.OnObjectCreated(poolName, obj);
      }

      public void OnPageButtonClick(int index)
      {
        this.m_owner.OnPageButtonClick(index);
      }

      public void SelectPage(int index)
      {
        this.m_owner.SelectPage(index);
      }

      public void SetPageCtrlSelected(int index, bool flag)
      {
        this.m_owner.SetPageCtrlSelected(index, flag);
      }

      public CollectionActivityExchangePageUIController GetPageCtrl(
        int index)
      {
        return this.m_owner.GetPageCtrl(index);
      }

      public void UpdatePages()
      {
        this.m_owner.UpdatePages();
      }

      public void UpdateCoverHero()
      {
        this.m_owner.UpdateCoverHero();
      }

      public void UpdateMissionItems(bool needResetScroll)
      {
        this.m_owner.UpdateMissionItems(needResetScroll);
      }

      public void UpdateCurrencyItems()
      {
        this.m_owner.UpdateCurrencyItems();
      }

      public CollectionAcitivityExchangeConsumeItemUIController OnMissionGetFromConsumeItemPool()
      {
        return this.m_owner.OnMissionGetFromConsumeItemPool();
      }

      public void OnMissionReturnFromConsumeItemPool(
        CollectionAcitivityExchangeConsumeItemUIController obj)
      {
        this.m_owner.OnMissionReturnFromConsumeItemPool(obj);
      }

      public bool OnCheckPageCanClick(int index)
      {
        return this.m_owner.OnCheckPageCanClick(index);
      }

      public void OnMissionGetButtonClick(
        CollectionActivityExchangeMissionItemUIController ctrl)
      {
        this.m_owner.OnMissionGetButtonClick(ctrl);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnRecommendButtonClicked()
      {
        this.m_owner.OnRecommendButtonClicked();
      }
    }
  }
}
