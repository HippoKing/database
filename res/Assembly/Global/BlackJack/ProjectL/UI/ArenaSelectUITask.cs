﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaSelectUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaSelectUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ArenaSelectUIController m_arenaSelectUIController;
    private bool m_isBattleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaSelectUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnResume(UIIntent intent)
    {
      WorldUITask.ShowWorld(true);
      return base.OnResume(intent);
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitArenaSelectUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitArenaSelectUIController();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitArenaSelectUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitArenaSelectUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUIController_OnShowOfflineArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUIController_OnShowOnlineArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ArenaUIType, bool, UIIntent> EventOnShowArena
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
