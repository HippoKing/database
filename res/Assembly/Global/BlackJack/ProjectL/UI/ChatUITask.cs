﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ChatUITask : UITask
  {
    private bool m_isTalkButtonDown;
    private bool m_isExitTalkButtonRect;
    private ProjectLPlayerContext m_playerCtx;
    private List<ChatComponent.ChatMessageClient> m_currentChatList;
    private ChatChannel m_currentTableType;
    private string m_chatTagetID;
    private string m_groupChatID;
    private IConfigDataLoader m_configDataLoader;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private bool m_isPrivateShowHistoryRecord;
    private bool m_isDragSrcowView;
    private bool m_isGroupShowHistoryRecord;
    private ChatUIController m_chatUICtrl;
    private ChatExpressionUIController m_expressionCtrl;
    private string m_currentChatUserID;
    public const string ParamsKey_AppointChannel = "AppointEnterChannel";
    public const string ParamsKey_ChatUserId = "PrivateChatPlayerGameUserId";
    public const string ParamsKey_GroupChatId = "GroupChatId";
    public const string ExpressionMark = "#";
    public const string ExpressionMark2 = "＃";
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    [DoNotToLua]
    private ChatUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnPause_hotfix;
    private LuaFunction m_OnNewIntentUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnGetChatTargetInfoEndFroResumeUIIntent_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_IsNeedUpdateDataCache_hotfix;
    private LuaFunction m_UpdateDataCache_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_OnSmallExpressionClickSmallExpressionItemContrller_hotfix;
    private LuaFunction m_OnExpressionBgButtonClick_hotfix;
    private LuaFunction m_OnExpressionFunctionButtonClick_hotfix;
    private LuaFunction m_OnRoomIdInputEndString_hotfix;
    private LuaFunction m_OnItemVoiceButtonClickChatMessageClient_hotfix;
    private LuaFunction m_OnPlayerIconClickChatMessageGameObject_hotfix;
    private LuaFunction m_ShowPlayerSimpleInfoGameObject_hotfix;
    private LuaFunction m_OnTalkButtonHold_hotfix;
    private LuaFunction m_CheckMicrophonePermission_hotfix;
    private LuaFunction m_OnTalkButtonClickUp_hotfix;
    private LuaFunction m_OnExitTalkButton_hotfix;
    private LuaFunction m_OnEnterTalkButton_hotfix;
    private LuaFunction m_OnTableChangeChatChannel_hotfix;
    private LuaFunction m_OnChatTargetChangedChatChannelStringString_hotfix;
    private LuaFunction m_OnSendButtonClickString_hotfix;
    private LuaFunction m_RemoveEmojiInMessageString_hotfix;
    private LuaFunction m_SendChatContentString_hotfix;
    private LuaFunction m_OnCloseButtonClick_hotfix;
    private LuaFunction m_ReturnPrevUITask_hotfix;
    private LuaFunction m_OnVoiceRecordTimeoutChatChannel_hotfix;
    private LuaFunction m_GetDataFromIntentUIIntent_hotfix;
    private LuaFunction m_TryGetPrivateChatPlayerInfoListList`1Action_hotfix;
    private LuaFunction m_TryGetPrivateChatPlayerInfoListFilterBlacklistString_hotfix;
    private LuaFunction m_TryGetChatGroupInfoListAction_hotfix;
    private LuaFunction m_OnChatMessageNtfChatMessage_hotfix;
    private LuaFunction m_OnChatMessageAckInt32_hotfix;
    private LuaFunction m_OnChatGroupUpdateNtfProChatGroupInfo_hotfix;
    private LuaFunction m_EventOnFriendGetSocialRelationNtfInt32_hotfix;
    private LuaFunction m_IsPipelineStateMaskNeedUpdatePipeLineStateMaskType_hotfix;
    private LuaFunction m_EnablePipelineStateMaskPipeLineStateMaskType_hotfix;
    private LuaFunction m_GetCurrentChatListByTableType_hotfix;
    private LuaFunction m_TickForVoiceRecordTime_hotfix;
    private LuaFunction m_TickForHistoryRecordShow_hotfix;
    private LuaFunction m_TickForVoiceAnim_hotfix;
    private LuaFunction m_PlayVoiceAnim_hotfix;
    private LuaFunction m_StopAllVoiceAnim_hotfix;
    private LuaFunction m_OnChatVoiceStartPlayChatMessageClient_hotfix;
    private LuaFunction m_DelayInstancePrefab_hotfix;
    private LuaFunction m_RefreshRedState_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ChatUITask StartChatUITaskWithNormalMode(UIIntent returnToIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ChatUITask StartChatUITaskToEnterAppointChannel(
      ChatChannel channel,
      UIIntent returnToIntent = null,
      string id = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PauseUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool OnGetChatTargetInfoEndFroResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSmallExpressionClick(SmallExpressionItemContrller ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionFunctionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRoomIdInputEnd(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemVoiceButtonClick(ChatComponent.ChatMessageClient voiceInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerIconClick(ChatMessage chatInfo, GameObject playerIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPlayerSimpleInfo(GameObject playerIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonHold()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckMicrophonePermission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitTalkButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterTalkButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTableChange(ChatChannel currentTableType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatTargetChanged(ChatChannel channel, string id, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClick(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string RemoveEmojiInMessage(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendChatContent(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReturnPrevUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceRecordTimeout(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetDataFromIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryGetPrivateChatPlayerInfoList(List<string> playerIdList, Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryGetPrivateChatPlayerInfoListFilterBlacklist(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryGetChatGroupInfoList(Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatMessageNtf(ChatMessage info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatMessageAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatGroupUpdateNtf(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EventOnFriendGetSocialRelationNtf(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPipelineStateMaskNeedUpdate(ChatUITask.PipeLineStateMaskType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EnablePipelineStateMask(ChatUITask.PipeLineStateMaskType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ChatComponent.ChatMessageClient> GetCurrentChatListByTableType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForVoiceRecordTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForHistoryRecordShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForVoiceAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayVoiceAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAllVoiceAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatVoiceStartPlay(ChatComponent.ChatMessageClient voiceMsg)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayInstancePrefab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshRedState()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ChatUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return base.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return base.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum PipeLineStateMaskType
    {
      IsInit,
      NewChatInfo,
      TableChange,
      ChooseNewChatTarget,
      ShowHistoryRecord,
      ChatTargetRefresh,
    }

    public class LuaExportHelper
    {
      private ChatUITask m_owner;

      public LuaExportHelper(ChatUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public bool m_isTalkButtonDown
      {
        get
        {
          return this.m_owner.m_isTalkButtonDown;
        }
        set
        {
          this.m_owner.m_isTalkButtonDown = value;
        }
      }

      public bool m_isExitTalkButtonRect
      {
        get
        {
          return this.m_owner.m_isExitTalkButtonRect;
        }
        set
        {
          this.m_owner.m_isExitTalkButtonRect = value;
        }
      }

      public ProjectLPlayerContext m_playerCtx
      {
        get
        {
          return this.m_owner.m_playerCtx;
        }
        set
        {
          this.m_owner.m_playerCtx = value;
        }
      }

      public List<ChatComponent.ChatMessageClient> m_currentChatList
      {
        get
        {
          return this.m_owner.m_currentChatList;
        }
        set
        {
          this.m_owner.m_currentChatList = value;
        }
      }

      public ChatChannel m_currentTableType
      {
        get
        {
          return this.m_owner.m_currentTableType;
        }
        set
        {
          this.m_owner.m_currentTableType = value;
        }
      }

      public string m_chatTagetID
      {
        get
        {
          return this.m_owner.m_chatTagetID;
        }
        set
        {
          this.m_owner.m_chatTagetID = value;
        }
      }

      public string m_groupChatID
      {
        get
        {
          return this.m_owner.m_groupChatID;
        }
        set
        {
          this.m_owner.m_groupChatID = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PlayerSimpleInfoUITask m_playerSimpleInfoUITask
      {
        get
        {
          return this.m_owner.m_playerSimpleInfoUITask;
        }
        set
        {
          this.m_owner.m_playerSimpleInfoUITask = value;
        }
      }

      public bool m_isPrivateShowHistoryRecord
      {
        get
        {
          return this.m_owner.m_isPrivateShowHistoryRecord;
        }
        set
        {
          this.m_owner.m_isPrivateShowHistoryRecord = value;
        }
      }

      public bool m_isDragSrcowView
      {
        get
        {
          return this.m_owner.m_isDragSrcowView;
        }
        set
        {
          this.m_owner.m_isDragSrcowView = value;
        }
      }

      public bool m_isGroupShowHistoryRecord
      {
        get
        {
          return this.m_owner.m_isGroupShowHistoryRecord;
        }
        set
        {
          this.m_owner.m_isGroupShowHistoryRecord = value;
        }
      }

      public ChatUIController m_chatUICtrl
      {
        get
        {
          return this.m_owner.m_chatUICtrl;
        }
        set
        {
          this.m_owner.m_chatUICtrl = value;
        }
      }

      public ChatExpressionUIController m_expressionCtrl
      {
        get
        {
          return this.m_owner.m_expressionCtrl;
        }
        set
        {
          this.m_owner.m_expressionCtrl = value;
        }
      }

      public string m_currentChatUserID
      {
        get
        {
          return this.m_owner.m_currentChatUserID;
        }
        set
        {
          this.m_owner.m_currentChatUserID = value;
        }
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public bool OnGetChatTargetInfoEndFroResume(UIIntent intent)
      {
        return this.m_owner.OnGetChatTargetInfoEndFroResume(intent);
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public bool IsNeedUpdateDataCache()
      {
        return this.m_owner.IsNeedUpdateDataCache();
      }

      public void UpdateDataCache()
      {
        this.m_owner.UpdateDataCache();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void OnExpressionBgButtonClick()
      {
        this.m_owner.OnExpressionBgButtonClick();
      }

      public void OnExpressionFunctionButtonClick()
      {
        this.m_owner.OnExpressionFunctionButtonClick();
      }

      public void OnItemVoiceButtonClick(ChatComponent.ChatMessageClient voiceInfo)
      {
        this.m_owner.OnItemVoiceButtonClick(voiceInfo);
      }

      public void OnPlayerIconClick(ChatMessage chatInfo, GameObject playerIcon)
      {
        this.m_owner.OnPlayerIconClick(chatInfo, playerIcon);
      }

      public void ShowPlayerSimpleInfo(GameObject playerIcon)
      {
        this.m_owner.ShowPlayerSimpleInfo(playerIcon);
      }

      public void OnTalkButtonHold()
      {
        this.m_owner.OnTalkButtonHold();
      }

      public IEnumerator CheckMicrophonePermission()
      {
        return this.m_owner.CheckMicrophonePermission();
      }

      public void OnTalkButtonClickUp()
      {
        this.m_owner.OnTalkButtonClickUp();
      }

      public void OnExitTalkButton()
      {
        this.m_owner.OnExitTalkButton();
      }

      public void OnEnterTalkButton()
      {
        this.m_owner.OnEnterTalkButton();
      }

      public void OnTableChange(ChatChannel currentTableType)
      {
        this.m_owner.OnTableChange(currentTableType);
      }

      public void OnChatTargetChanged(ChatChannel channel, string id, string name)
      {
        this.m_owner.OnChatTargetChanged(channel, id, name);
      }

      public void OnSendButtonClick(string content)
      {
        this.m_owner.OnSendButtonClick(content);
      }

      public string RemoveEmojiInMessage(string msg)
      {
        return this.m_owner.RemoveEmojiInMessage(msg);
      }

      public void SendChatContent(string content)
      {
        this.m_owner.SendChatContent(content);
      }

      public void OnCloseButtonClick()
      {
        this.m_owner.OnCloseButtonClick();
      }

      public void ReturnPrevUITask()
      {
        this.m_owner.ReturnPrevUITask();
      }

      public void OnVoiceRecordTimeout(ChatChannel channel)
      {
        this.m_owner.OnVoiceRecordTimeout(channel);
      }

      public void GetDataFromIntent(UIIntent intent)
      {
        this.m_owner.GetDataFromIntent(intent);
      }

      public void TryGetPrivateChatPlayerInfoList(List<string> playerIdList, Action action)
      {
        this.m_owner.TryGetPrivateChatPlayerInfoList(playerIdList, action);
      }

      public void TryGetPrivateChatPlayerInfoListFilterBlacklist(string userID)
      {
        this.m_owner.TryGetPrivateChatPlayerInfoListFilterBlacklist(userID);
      }

      public void TryGetChatGroupInfoList(Action action)
      {
        this.m_owner.TryGetChatGroupInfoList(action);
      }

      public void OnChatMessageNtf(ChatMessage info)
      {
        this.m_owner.OnChatMessageNtf(info);
      }

      public void OnChatMessageAck(int result)
      {
        this.m_owner.OnChatMessageAck(result);
      }

      public void OnChatGroupUpdateNtf(ProChatGroupInfo chatGroupInfo)
      {
        this.m_owner.OnChatGroupUpdateNtf(chatGroupInfo);
      }

      public void EventOnFriendGetSocialRelationNtf(int result)
      {
        this.m_owner.EventOnFriendGetSocialRelationNtf(result);
      }

      public bool IsPipelineStateMaskNeedUpdate(ChatUITask.PipeLineStateMaskType state)
      {
        return this.m_owner.IsPipelineStateMaskNeedUpdate(state);
      }

      public void EnablePipelineStateMask(ChatUITask.PipeLineStateMaskType state)
      {
        this.m_owner.EnablePipelineStateMask(state);
      }

      public List<ChatComponent.ChatMessageClient> GetCurrentChatListByTableType()
      {
        return this.m_owner.GetCurrentChatListByTableType();
      }

      public void TickForVoiceRecordTime()
      {
        this.m_owner.TickForVoiceRecordTime();
      }

      public void TickForHistoryRecordShow()
      {
        this.m_owner.TickForHistoryRecordShow();
      }

      public void TickForVoiceAnim()
      {
        this.m_owner.TickForVoiceAnim();
      }

      public void PlayVoiceAnim()
      {
        this.m_owner.PlayVoiceAnim();
      }

      public void StopAllVoiceAnim()
      {
        this.m_owner.StopAllVoiceAnim();
      }

      public void OnChatVoiceStartPlay(ChatComponent.ChatMessageClient voiceMsg)
      {
        this.m_owner.OnChatVoiceStartPlay(voiceMsg);
      }

      public IEnumerator DelayInstancePrefab()
      {
        return this.m_owner.DelayInstancePrefab();
      }

      public void RefreshRedState()
      {
        this.m_owner.RefreshRedState();
      }
    }
  }
}
