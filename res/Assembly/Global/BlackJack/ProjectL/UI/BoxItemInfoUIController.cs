﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BoxItemInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BoxItemInfoUIController : UIControllerBase
  {
    [AutoBind("./ItemGoodsDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodsDummy;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./PresentImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentLogo;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public BoxItemInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBoxItemInfo(Goods good, bool isPresent)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
