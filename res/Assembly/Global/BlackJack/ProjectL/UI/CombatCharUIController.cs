﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CombatCharUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class CombatCharUIController : UIControllerBase
  {
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    private UISpineGraphic m_spineGraphic;
    private string m_animationName;
    private string m_loopAnimationName;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    private void OnDisable()
    {
      this.DestroyGraphic();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string animation, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetAnimationName()
    {
      return this.m_animationName;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdate()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
    }
  }
}
