﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCommentItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroCommentItemUIController : UIControllerBase
  {
    [AutoBind("./PraisedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerPraisedIcon;
    [AutoBind("./LVText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLvText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerNameBtn;
    [AutoBind("./CommentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerCommentText;
    [AutoBind("./PraisedNumText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerPraisedNumText;
    [AutoBind("./PraisedButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerPraisedBtn;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitCommentItem(HeroCommentEntry comment, bool isShowPraisedIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerNameButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerPraisedButtonClick()
    {
    }

    public event Action<HeroCommentEntry> EventOnNameClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ulong> EventOnPraisedBtnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroCommentEntry Comment { private set; get; }

    public string CommenterId { private set; get; }

    public ulong CommentInstanceId { private set; get; }
  }
}
