﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using MarchingBytes;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TestUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./SystemInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_systemInfoButton;
    [AutoBind("./GoddessDialogButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goddessDialogButton;
    [AutoBind("./ClearUserGuideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearUserGuideButton;
    [AutoBind("./CompleteUserGuideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_completeUserGuideButton;
    [AutoBind("./BecomeStrongButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_becomeStrongButton;
    [AutoBind("./BecomeStrongPlayerLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongPlayerLevelInput;
    [AutoBind("./BecomeStrongHeroStarLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongHeroStarLevelInput;
    [AutoBind("./BecomeStrongEquipmentLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongEquipmentLevelInput;
    [AutoBind("./ClearStageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearStageButton;
    [AutoBind("./ReportBugButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_openReportBugPanelButton;
    [AutoBind("./iPhoneXToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_iPhoneXToggle;
    [AutoBind("./TestToggles/BattleToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleToggle;
    [AutoBind("./TestToggles/DialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dialogToggle;
    [AutoBind("./TestToggles/BattleDialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleDialogToggle;
    [AutoBind("./TestToggles/UserGuideDialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_userGuideDialogToggle;
    [AutoBind("./TestList/LoopVerticalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_ListItemPool;
    [AutoBind("./TestList/LoopVerticalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_LoopScroolRect;
    [AutoBind("./MonsterLevel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monsterLevelGameObject;
    [AutoBind("./MonsterLevel/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_monsterLevelInputField;
    [AutoBind("./StartTestButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startTestBattleButton;
    [AutoBind("./TestId/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_TestIdInputField;
    [AutoBind("./TestId/Next", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TestIdNextSearch;
    [AutoBind("./TestId/Home", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TestListHomeBtn;
    [AutoBind("./SystemInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_systemInfoGameObject;
    [AutoBind("./SystemInfo/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_systemInfoBackgroundButton;
    [AutoBind("./SystemInfo/ScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private UnityEngine.UI.Text m_systemInfoText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/TestListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testListItemPrefab;
    [AutoBind("./ServerDateTime", AutoBindAttribute.InitState.Active, false)]
    private UnityEngine.UI.Text m_serverDateTimeText;
    [AutoBind("./ReportBugPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeReportBugPanelButton;
    [AutoBind("./ReportBugPanel", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_reportBugPanelGameObject;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneHour", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneHourLogTime;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneDay", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneDayLogTime;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneWeek", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneWeekLogTime;
    [AutoBind("./ReportBugPanel/Panel/Description/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_bugDescriptionInputField;
    [AutoBind("./ReportBugPanel/Panel/ReportButtons/Cancel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelReportBugButton;
    [AutoBind("./ReportBugPanel/Panel/ReportButtons/Submit", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_reportBugButton;
    [AutoBind("./FinishAllTasksButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_finishAllTasksButton;
    private List<TestListItemUIController> m_testListItemUIControllers;
    private bool m_isIgnoreToggleEvent;
    private List<KeyValuePair<int, string>> m_CacheTestList;
    private TestListItemUIController m_CurrSelectCtrl;
    private int m_nSearchTestListId;
    [DoNotToLua]
    private TestUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetTestListTypeTestListType_hotfix;
    private LuaFunction m_AddTestListItemInt32String_hotfix;
    private LuaFunction m_ClearTestList_hotfix;
    private LuaFunction m_GetSelectedTestListItemUIController_hotfix;
    private LuaFunction m_SetMonsterLevelInt32_hotfix;
    private LuaFunction m_GetMonsterLevel_hotfix;
    private LuaFunction m_SetServerDataTimeDateTime_hotfix;
    private LuaFunction m_ShowSystemInfo_hotfix;
    private LuaFunction m_HideSystemInfo_hotfix;
    private LuaFunction m_InitLoopScrollViewRect_hotfix;
    private LuaFunction m_OnPoolObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_RefreshLoopVerticalScrollViewTestListType_hotfix;
    private LuaFunction m_OnTestItemFillUIControllerBase_hotfix;
    private LuaFunction m_OnTestItemClickUIControllerBase_hotfix;
    private LuaFunction m_DoToggleBooleanTestListType_hotfix;
    private LuaFunction m_OnBattleToggleBoolean_hotfix;
    private LuaFunction m_OnDialogToggleBoolean_hotfix;
    private LuaFunction m_OnBattleDialogToggleBoolean_hotfix;
    private LuaFunction m_OnUserGuideDialogToggleBoolean_hotfix;
    private LuaFunction m_OnTestListHomeBtnClick_hotfix;
    private LuaFunction m_OnTestIdNextBtnClick_hotfix;
    private LuaFunction m_OnTestIdInputString_hotfix;
    private LuaFunction m_OnStartTestButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnSystemInfoButtonClick_hotfix;
    private LuaFunction m_OnGoddessDialogButtonClick_hotfix;
    private LuaFunction m_OnClearUserGuideButtonClick_hotfix;
    private LuaFunction m_OnBecomeStrongButtonClick_hotfix;
    private LuaFunction m_OnFinishAllTasksButtonClick_hotfix;
    private LuaFunction m_FinishAllTasks_hotfix;
    private LuaFunction m_OnCancelReportBugButtonClick_hotfix;
    private LuaFunction m_OniPhoneXToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnReportBugButtonClick_hotfix;
    private LuaFunction m_GetReportBugLogHours_hotfix;
    private LuaFunction m_OnOpenReportBugPanelButtonClick_hotfix;
    private LuaFunction m_OnCompleteUserGuideButtonClick_hotfix;
    private LuaFunction m_OnClearStageButtonClick_hotfix;
    private LuaFunction m_OnSystemInfoBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnChangeTestListAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangeTestListAction`1_hotfix;
    private LuaFunction m_add_EventOnStartTestAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartTestAction`1_hotfix;
    private LuaFunction m_add_EventOnShowSystemInfoAction_hotfix;
    private LuaFunction m_remove_EventOnShowSystemInfoAction_hotfix;
    private LuaFunction m_add_EventOnCloseSystemInfoAction_hotfix;
    private LuaFunction m_remove_EventOnCloseSystemInfoAction_hotfix;
    private LuaFunction m_add_EventOnShowGoddessDialogAction_hotfix;
    private LuaFunction m_remove_EventOnShowGoddessDialogAction_hotfix;
    private LuaFunction m_add_EventOnClearUserGuideAction_hotfix;
    private LuaFunction m_remove_EventOnClearUserGuideAction_hotfix;
    private LuaFunction m_add_EventOnCompleteUserGuideAction_hotfix;
    private LuaFunction m_remove_EventOnCompleteUserGuideAction_hotfix;
    private LuaFunction m_add_EventOnClearStageAction_hotfix;
    private LuaFunction m_remove_EventOnClearStageAction_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnReportBugAction`2_hotfix;
    private LuaFunction m_remove_EventOnReportBugAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private TestUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestListType(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTestListItem(int id, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTestList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TestListItemUIController GetSelectedTestListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMonsterLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerDataTime(DateTime dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BuildSystemInfoText(bool textColor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendLine(StringBuilder sb, bool textColor, string name, string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshLoopVerticalScrollView(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestItemFill(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestItemClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoToggle(bool on, TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDialogToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleDialogToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUserGuideDialogToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestListHomeBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestIdNextBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestIdInput(string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartTestButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSystemInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoddessDialogButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearUserGuideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBecomeStrongButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFinishAllTasksButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator FinishAllTasks()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelReportBugButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OniPhoneXToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportBugButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private double GetReportBugLogHours()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenReportBugPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int MaxPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UpgradePlayerLevel(int targetPlayerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator SpeedUpAllHeros(int targetHeroLevel, int targetStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SetEnchantProperty(
      ulong equipmentInstanceId,
      string values,
      Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BecomeStrong(
      int targetPlayerLevel,
      int targetStarLevel,
      int targetEquipmentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator TenLevelUpEquipment(
      ulong equipmentInstanceId,
      Action OnEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SpeedUpHero(
      int heroID,
      int targetHeroLevel,
      int targetStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator ResetHero(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<int> GetHeroJobConnSequence(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<int> GetJobSequence(int rootJobConnectionID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetEquipmentLevelLimit(int equipmentStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsEquipismentMaxLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator StarLevelUpTopRankEquipments(
      int ssrMaterialEquipmentId,
      int srMaterialEquipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddItem4StarLevelUpEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddGoods(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator StarLevelUpEquipment(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator EnhanceAllTopRankEquipments(int targetLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator EnhanceEquipment(
      ulong instanceId,
      List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllTopRankEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddHero(List<int> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator HeroBreak(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BuyFixedStoreItem(
      int fixedStoreID,
      int fixedStoreItemGoodsID,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BuyFixedStoreItem(
      int fixedStoreID,
      int fixedStoreItemGoodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator TransferHeroJob(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllItem(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UpgradeHeroJobLevel(Hero hero, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator SendGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UseItem(GoodsType type, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddHeroExp(
      int heroId,
      GoodsType itemType,
      int itemID,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ErrorCodeToMessage(int code)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteUserGuideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearStageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSystemInfoBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TestListType> EventOnChangeTestList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnStartTest
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSystemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseSystemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowGoddessDialog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCompleteUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, double> EventOnReportBug
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TestUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeTestList(TestListType obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeTestList(TestListType obj)
    {
      this.EventOnChangeTestList = (Action<TestListType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartTest(int obj)
    {
    }

    private void __clearDele_EventOnStartTest(int obj)
    {
      this.EventOnStartTest = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowSystemInfo()
    {
      this.EventOnShowSystemInfo = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCloseSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCloseSystemInfo()
    {
      this.EventOnCloseSystemInfo = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowGoddessDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowGoddessDialog()
    {
      this.EventOnShowGoddessDialog = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClearUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClearUserGuide()
    {
      this.EventOnClearUserGuide = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCompleteUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCompleteUserGuide()
    {
      this.EventOnCompleteUserGuide = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClearStage()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClearStage()
    {
      this.EventOnClearStage = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReportBug(string arg1, double arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReportBug(string arg1, double arg2)
    {
      this.EventOnReportBug = (Action<string, double>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TestUIController m_owner;

      public LuaExportHelper(TestUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnChangeTestList(TestListType obj)
      {
        this.m_owner.__callDele_EventOnChangeTestList(obj);
      }

      public void __clearDele_EventOnChangeTestList(TestListType obj)
      {
        this.m_owner.__clearDele_EventOnChangeTestList(obj);
      }

      public void __callDele_EventOnStartTest(int obj)
      {
        this.m_owner.__callDele_EventOnStartTest(obj);
      }

      public void __clearDele_EventOnStartTest(int obj)
      {
        this.m_owner.__clearDele_EventOnStartTest(obj);
      }

      public void __callDele_EventOnShowSystemInfo()
      {
        this.m_owner.__callDele_EventOnShowSystemInfo();
      }

      public void __clearDele_EventOnShowSystemInfo()
      {
        this.m_owner.__clearDele_EventOnShowSystemInfo();
      }

      public void __callDele_EventOnCloseSystemInfo()
      {
        this.m_owner.__callDele_EventOnCloseSystemInfo();
      }

      public void __clearDele_EventOnCloseSystemInfo()
      {
        this.m_owner.__clearDele_EventOnCloseSystemInfo();
      }

      public void __callDele_EventOnShowGoddessDialog()
      {
        this.m_owner.__callDele_EventOnShowGoddessDialog();
      }

      public void __clearDele_EventOnShowGoddessDialog()
      {
        this.m_owner.__clearDele_EventOnShowGoddessDialog();
      }

      public void __callDele_EventOnClearUserGuide()
      {
        this.m_owner.__callDele_EventOnClearUserGuide();
      }

      public void __clearDele_EventOnClearUserGuide()
      {
        this.m_owner.__clearDele_EventOnClearUserGuide();
      }

      public void __callDele_EventOnCompleteUserGuide()
      {
        this.m_owner.__callDele_EventOnCompleteUserGuide();
      }

      public void __clearDele_EventOnCompleteUserGuide()
      {
        this.m_owner.__clearDele_EventOnCompleteUserGuide();
      }

      public void __callDele_EventOnClearStage()
      {
        this.m_owner.__callDele_EventOnClearStage();
      }

      public void __clearDele_EventOnClearStage()
      {
        this.m_owner.__clearDele_EventOnClearStage();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnReportBug(string arg1, double arg2)
      {
        this.m_owner.__callDele_EventOnReportBug(arg1, arg2);
      }

      public void __clearDele_EventOnReportBug(string arg1, double arg2)
      {
        this.m_owner.__clearDele_EventOnReportBug(arg1, arg2);
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_systemInfoButton
      {
        get
        {
          return this.m_owner.m_systemInfoButton;
        }
        set
        {
          this.m_owner.m_systemInfoButton = value;
        }
      }

      public Button m_goddessDialogButton
      {
        get
        {
          return this.m_owner.m_goddessDialogButton;
        }
        set
        {
          this.m_owner.m_goddessDialogButton = value;
        }
      }

      public Button m_clearUserGuideButton
      {
        get
        {
          return this.m_owner.m_clearUserGuideButton;
        }
        set
        {
          this.m_owner.m_clearUserGuideButton = value;
        }
      }

      public Button m_completeUserGuideButton
      {
        get
        {
          return this.m_owner.m_completeUserGuideButton;
        }
        set
        {
          this.m_owner.m_completeUserGuideButton = value;
        }
      }

      public Button m_becomeStrongButton
      {
        get
        {
          return this.m_owner.m_becomeStrongButton;
        }
        set
        {
          this.m_owner.m_becomeStrongButton = value;
        }
      }

      public InputField m_becomeStrongPlayerLevelInput
      {
        get
        {
          return this.m_owner.m_becomeStrongPlayerLevelInput;
        }
        set
        {
          this.m_owner.m_becomeStrongPlayerLevelInput = value;
        }
      }

      public InputField m_becomeStrongHeroStarLevelInput
      {
        get
        {
          return this.m_owner.m_becomeStrongHeroStarLevelInput;
        }
        set
        {
          this.m_owner.m_becomeStrongHeroStarLevelInput = value;
        }
      }

      public InputField m_becomeStrongEquipmentLevelInput
      {
        get
        {
          return this.m_owner.m_becomeStrongEquipmentLevelInput;
        }
        set
        {
          this.m_owner.m_becomeStrongEquipmentLevelInput = value;
        }
      }

      public Button m_clearStageButton
      {
        get
        {
          return this.m_owner.m_clearStageButton;
        }
        set
        {
          this.m_owner.m_clearStageButton = value;
        }
      }

      public Button m_openReportBugPanelButton
      {
        get
        {
          return this.m_owner.m_openReportBugPanelButton;
        }
        set
        {
          this.m_owner.m_openReportBugPanelButton = value;
        }
      }

      public Toggle m_iPhoneXToggle
      {
        get
        {
          return this.m_owner.m_iPhoneXToggle;
        }
        set
        {
          this.m_owner.m_iPhoneXToggle = value;
        }
      }

      public Toggle m_battleToggle
      {
        get
        {
          return this.m_owner.m_battleToggle;
        }
        set
        {
          this.m_owner.m_battleToggle = value;
        }
      }

      public Toggle m_dialogToggle
      {
        get
        {
          return this.m_owner.m_dialogToggle;
        }
        set
        {
          this.m_owner.m_dialogToggle = value;
        }
      }

      public Toggle m_battleDialogToggle
      {
        get
        {
          return this.m_owner.m_battleDialogToggle;
        }
        set
        {
          this.m_owner.m_battleDialogToggle = value;
        }
      }

      public Toggle m_userGuideDialogToggle
      {
        get
        {
          return this.m_owner.m_userGuideDialogToggle;
        }
        set
        {
          this.m_owner.m_userGuideDialogToggle = value;
        }
      }

      public EasyObjectPool m_ListItemPool
      {
        get
        {
          return this.m_owner.m_ListItemPool;
        }
        set
        {
          this.m_owner.m_ListItemPool = value;
        }
      }

      public LoopVerticalScrollRect m_LoopScroolRect
      {
        get
        {
          return this.m_owner.m_LoopScroolRect;
        }
        set
        {
          this.m_owner.m_LoopScroolRect = value;
        }
      }

      public GameObject m_monsterLevelGameObject
      {
        get
        {
          return this.m_owner.m_monsterLevelGameObject;
        }
        set
        {
          this.m_owner.m_monsterLevelGameObject = value;
        }
      }

      public InputField m_monsterLevelInputField
      {
        get
        {
          return this.m_owner.m_monsterLevelInputField;
        }
        set
        {
          this.m_owner.m_monsterLevelInputField = value;
        }
      }

      public Button m_startTestBattleButton
      {
        get
        {
          return this.m_owner.m_startTestBattleButton;
        }
        set
        {
          this.m_owner.m_startTestBattleButton = value;
        }
      }

      public InputField m_TestIdInputField
      {
        get
        {
          return this.m_owner.m_TestIdInputField;
        }
        set
        {
          this.m_owner.m_TestIdInputField = value;
        }
      }

      public Button m_TestIdNextSearch
      {
        get
        {
          return this.m_owner.m_TestIdNextSearch;
        }
        set
        {
          this.m_owner.m_TestIdNextSearch = value;
        }
      }

      public Button m_TestListHomeBtn
      {
        get
        {
          return this.m_owner.m_TestListHomeBtn;
        }
        set
        {
          this.m_owner.m_TestListHomeBtn = value;
        }
      }

      public GameObject m_systemInfoGameObject
      {
        get
        {
          return this.m_owner.m_systemInfoGameObject;
        }
        set
        {
          this.m_owner.m_systemInfoGameObject = value;
        }
      }

      public Button m_systemInfoBackgroundButton
      {
        get
        {
          return this.m_owner.m_systemInfoBackgroundButton;
        }
        set
        {
          this.m_owner.m_systemInfoBackgroundButton = value;
        }
      }

      public UnityEngine.UI.Text m_systemInfoText
      {
        get
        {
          return this.m_owner.m_systemInfoText;
        }
        set
        {
          this.m_owner.m_systemInfoText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_testListItemPrefab
      {
        get
        {
          return this.m_owner.m_testListItemPrefab;
        }
        set
        {
          this.m_owner.m_testListItemPrefab = value;
        }
      }

      public UnityEngine.UI.Text m_serverDateTimeText
      {
        get
        {
          return this.m_owner.m_serverDateTimeText;
        }
        set
        {
          this.m_owner.m_serverDateTimeText = value;
        }
      }

      public Button m_closeReportBugPanelButton
      {
        get
        {
          return this.m_owner.m_closeReportBugPanelButton;
        }
        set
        {
          this.m_owner.m_closeReportBugPanelButton = value;
        }
      }

      public GameObject m_reportBugPanelGameObject
      {
        get
        {
          return this.m_owner.m_reportBugPanelGameObject;
        }
        set
        {
          this.m_owner.m_reportBugPanelGameObject = value;
        }
      }

      public Toggle m_oneHourLogTime
      {
        get
        {
          return this.m_owner.m_oneHourLogTime;
        }
        set
        {
          this.m_owner.m_oneHourLogTime = value;
        }
      }

      public Toggle m_oneDayLogTime
      {
        get
        {
          return this.m_owner.m_oneDayLogTime;
        }
        set
        {
          this.m_owner.m_oneDayLogTime = value;
        }
      }

      public Toggle m_oneWeekLogTime
      {
        get
        {
          return this.m_owner.m_oneWeekLogTime;
        }
        set
        {
          this.m_owner.m_oneWeekLogTime = value;
        }
      }

      public InputField m_bugDescriptionInputField
      {
        get
        {
          return this.m_owner.m_bugDescriptionInputField;
        }
        set
        {
          this.m_owner.m_bugDescriptionInputField = value;
        }
      }

      public Button m_cancelReportBugButton
      {
        get
        {
          return this.m_owner.m_cancelReportBugButton;
        }
        set
        {
          this.m_owner.m_cancelReportBugButton = value;
        }
      }

      public Button m_reportBugButton
      {
        get
        {
          return this.m_owner.m_reportBugButton;
        }
        set
        {
          this.m_owner.m_reportBugButton = value;
        }
      }

      public Button m_finishAllTasksButton
      {
        get
        {
          return this.m_owner.m_finishAllTasksButton;
        }
        set
        {
          this.m_owner.m_finishAllTasksButton = value;
        }
      }

      public List<TestListItemUIController> m_testListItemUIControllers
      {
        get
        {
          return this.m_owner.m_testListItemUIControllers;
        }
        set
        {
          this.m_owner.m_testListItemUIControllers = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public List<KeyValuePair<int, string>> m_CacheTestList
      {
        get
        {
          return this.m_owner.m_CacheTestList;
        }
        set
        {
          this.m_owner.m_CacheTestList = value;
        }
      }

      public TestListItemUIController m_CurrSelectCtrl
      {
        get
        {
          return this.m_owner.m_CurrSelectCtrl;
        }
        set
        {
          this.m_owner.m_CurrSelectCtrl = value;
        }
      }

      public int m_nSearchTestListId
      {
        get
        {
          return this.m_owner.m_nSearchTestListId;
        }
        set
        {
          this.m_owner.m_nSearchTestListId = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
      {
        TestUIController.ScrollToItem(scrollRect, itemCount, idx);
      }

      public TestListItemUIController GetSelectedTestListItemUIController()
      {
        return this.m_owner.GetSelectedTestListItemUIController();
      }

      public static void AppendLine(StringBuilder sb, bool textColor, string name, string value)
      {
        TestUIController.AppendLine(sb, textColor, name, value);
      }

      public void InitLoopScrollViewRect()
      {
        this.m_owner.InitLoopScrollViewRect();
      }

      public void OnPoolObjectCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjectCreated(poolName, go);
      }

      public void OnTestItemFill(UIControllerBase ctrl)
      {
        this.m_owner.OnTestItemFill(ctrl);
      }

      public void OnTestItemClick(UIControllerBase ctrl)
      {
        this.m_owner.OnTestItemClick(ctrl);
      }

      public void DoToggle(bool on, TestListType listType)
      {
        this.m_owner.DoToggle(on, listType);
      }

      public void OnBattleToggle(bool on)
      {
        this.m_owner.OnBattleToggle(on);
      }

      public void OnDialogToggle(bool on)
      {
        this.m_owner.OnDialogToggle(on);
      }

      public void OnBattleDialogToggle(bool on)
      {
        this.m_owner.OnBattleDialogToggle(on);
      }

      public void OnUserGuideDialogToggle(bool on)
      {
        this.m_owner.OnUserGuideDialogToggle(on);
      }

      public void OnTestListHomeBtnClick()
      {
        this.m_owner.OnTestListHomeBtnClick();
      }

      public void OnTestIdNextBtnClick()
      {
        this.m_owner.OnTestIdNextBtnClick();
      }

      public void OnTestIdInput(string value)
      {
        this.m_owner.OnTestIdInput(value);
      }

      public void OnStartTestButtonClick()
      {
        this.m_owner.OnStartTestButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnSystemInfoButtonClick()
      {
        this.m_owner.OnSystemInfoButtonClick();
      }

      public void OnGoddessDialogButtonClick()
      {
        this.m_owner.OnGoddessDialogButtonClick();
      }

      public void OnClearUserGuideButtonClick()
      {
        this.m_owner.OnClearUserGuideButtonClick();
      }

      public void OnBecomeStrongButtonClick()
      {
        this.m_owner.OnBecomeStrongButtonClick();
      }

      public void OnFinishAllTasksButtonClick()
      {
        this.m_owner.OnFinishAllTasksButtonClick();
      }

      public IEnumerator FinishAllTasks()
      {
        return this.m_owner.FinishAllTasks();
      }

      public void OnCancelReportBugButtonClick()
      {
        this.m_owner.OnCancelReportBugButtonClick();
      }

      public void OniPhoneXToggleValueChanged(bool isOn)
      {
        this.m_owner.OniPhoneXToggleValueChanged(isOn);
      }

      public void OnReportBugButtonClick()
      {
        this.m_owner.OnReportBugButtonClick();
      }

      public double GetReportBugLogHours()
      {
        return this.m_owner.GetReportBugLogHours();
      }

      public void OnOpenReportBugPanelButtonClick()
      {
        this.m_owner.OnOpenReportBugPanelButtonClick();
      }

      public static IEnumerator UpgradePlayerLevel(int targetPlayerLevel)
      {
        return TestUIController.UpgradePlayerLevel(targetPlayerLevel);
      }

      public static IEnumerator SpeedUpAllHeros(int targetHeroLevel, int targetStarLevel)
      {
        return TestUIController.SpeedUpAllHeros(targetHeroLevel, targetStarLevel);
      }

      public static IEnumerator BecomeStrong(
        int targetPlayerLevel,
        int targetStarLevel,
        int targetEquipmentLevel)
      {
        return TestUIController.BecomeStrong(targetPlayerLevel, targetStarLevel, targetEquipmentLevel);
      }

      public static List<int> GetHeroJobConnSequence(int heroID)
      {
        return TestUIController.GetHeroJobConnSequence(heroID);
      }

      public static List<int> GetJobSequence(int rootJobConnectionID)
      {
        return TestUIController.GetJobSequence(rootJobConnectionID);
      }

      public static int GetEquipmentLevelLimit(int equipmentStarLevel)
      {
        return TestUIController.GetEquipmentLevelLimit(equipmentStarLevel);
      }

      public static bool IsEquipismentMaxLevel(EquipmentBagItem equipment)
      {
        return TestUIController.IsEquipismentMaxLevel(equipment);
      }

      public static IEnumerator StarLevelUpTopRankEquipments(
        int ssrMaterialEquipmentId,
        int srMaterialEquipmentId)
      {
        return TestUIController.StarLevelUpTopRankEquipments(ssrMaterialEquipmentId, srMaterialEquipmentId);
      }

      public static IEnumerator AddItem4StarLevelUpEquipment(ulong instanceId)
      {
        return TestUIController.AddItem4StarLevelUpEquipment(instanceId);
      }

      public static IEnumerator AddGoods(List<Goods> goods)
      {
        return TestUIController.AddGoods(goods);
      }

      public static IEnumerator StarLevelUpEquipment(ulong instanceId, ulong materialId)
      {
        return TestUIController.StarLevelUpEquipment(instanceId, materialId);
      }

      public static IEnumerator EnhanceAllTopRankEquipments(int targetLevel)
      {
        return TestUIController.EnhanceAllTopRankEquipments(targetLevel);
      }

      public static IEnumerator EnhanceEquipment(
        ulong instanceId,
        List<ulong> materialIds)
      {
        return TestUIController.EnhanceEquipment(instanceId, materialIds);
      }

      public static IEnumerator AddAllTopRankEquipments()
      {
        return TestUIController.AddAllTopRankEquipments();
      }

      public static IEnumerator AddAllHeros()
      {
        return TestUIController.AddAllHeros();
      }

      public static IEnumerator AddHero(List<int> heros)
      {
        return TestUIController.AddHero(heros);
      }

      public static IEnumerator HeroBreak(int id)
      {
        return TestUIController.HeroBreak(id);
      }

      public static IEnumerator BuyFixedStoreItem(
        int fixedStoreID,
        int fixedStoreItemGoodsID,
        int count)
      {
        return TestUIController.BuyFixedStoreItem(fixedStoreID, fixedStoreItemGoodsID, count);
      }

      public static IEnumerator BuyFixedStoreItem(
        int fixedStoreID,
        int fixedStoreItemGoodsID)
      {
        return TestUIController.BuyFixedStoreItem(fixedStoreID, fixedStoreItemGoodsID);
      }

      public static IEnumerator TransferHeroJob(int heroId, int jobConnectionId)
      {
        return TestUIController.TransferHeroJob(heroId, jobConnectionId);
      }

      public static IEnumerator AddAllItem(int count)
      {
        return TestUIController.AddAllItem(count);
      }

      public static IEnumerator UpgradeHeroJobLevel(Hero hero, int jobConnectionId)
      {
        return TestUIController.UpgradeHeroJobLevel(hero, jobConnectionId);
      }

      public static IEnumerator SendGMCommand(string cmd)
      {
        return TestUIController.SendGMCommand(cmd);
      }

      public static IEnumerator UseItem(GoodsType type, int id, int count)
      {
        return TestUIController.UseItem(type, id, count);
      }

      public static IEnumerator AddHeroExp(
        int heroId,
        GoodsType itemType,
        int itemID,
        int count)
      {
        return TestUIController.AddHeroExp(heroId, itemType, itemID, count);
      }

      public static string ErrorCodeToMessage(int code)
      {
        return TestUIController.ErrorCodeToMessage(code);
      }

      public void OnCompleteUserGuideButtonClick()
      {
        this.m_owner.OnCompleteUserGuideButtonClick();
      }

      public void OnClearStageButtonClick()
      {
        this.m_owner.OnClearStageButtonClick();
      }

      public void OnSystemInfoBackgroundButtonClick()
      {
        this.m_owner.OnSystemInfoBackgroundButtonClick();
      }
    }
  }
}
