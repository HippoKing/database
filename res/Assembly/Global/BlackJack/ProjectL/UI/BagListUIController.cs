﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BagListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BagListUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Equipment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Fragment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_fragmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/JobMaterial", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobMaterialToggle;
    [AutoBind("./GoldMetallurgyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_alchemyButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./BagListPanel/BagListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemTemplateRoot;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewBagItemContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/BgContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointBgContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointItem;
    [AutoBind("./BagListPanel/NoItemPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noItemPanelObj;
    [AutoBind("./BagListPanel/BagInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagInfoPanelObj;
    [AutoBind("./BagListPanel/BagInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bagInfoStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/UseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_useButton;
    [AutoBind("./BagListPanel/BagInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/Count/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./BagListPanel/BagInfoPanel/Desc/ValueTextScrollView/Mask/Content/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./BagListPanel/BagInfoPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./BagListPanel/BagInfoPanel/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemGetButton;
    [AutoBind("./CountLimit", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bagCountLimitStateCtrl;
    [AutoBind("./CountLimit/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bagCountText;
    [AutoBind("./CountLimit/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bagMaxCountText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/ExplainFrontToggle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentInfoExplainText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/ForgeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentForgeButton;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentSkillStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillLvValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillUnlockCoditionText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillCharNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentSkillCharNameBGImage;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillDescText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentSkillContentStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/EquipGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentLimitStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentLimitContent;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/EquipGroup/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentEquipUnlimitText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropertyGroup;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropATGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropATValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropDFGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropDFValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropHPGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropHPValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropMagiccGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropMagicValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropMagicDFGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropMagicDFValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropDexGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropDexValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentPropGroupStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropEnchantmentGroup;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropEnchantmentGroupResonanceGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipmentPropEnchantmentGroupRuneIconImage;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropEnchantmentGroupRuneNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./UseItemsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_subBagInfoPanelObj;
    [AutoBind("./UseItemsPanel/PanelDetail/Minus", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMinusButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Add", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemAddButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Max", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMaxButton;
    [AutoBind("./UseItemsPanel/PanelDetail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_subItemNumInputField;
    [AutoBind("./UseItemsPanel/PanelDetail/UseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemUseButton;
    [AutoBind("./UseItemsPanel/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemPanelReturnButton;
    [AutoBind("./AddAllItemButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_addAllItemButton;
    [AutoBind("./AddAllEquipmentButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_addAllEquipmentButton;
    [AutoBind("./AddBagItemButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_addItemButton;
    [AutoBind("./ClearBagButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_clearBagItemButton;
    [AutoBind("./BagItemInputField", AutoBindAttribute.InitState.Inactive, false)]
    private InputField m_bagItemInputField;
    [AutoBind("./SpeedUpButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_speedUpButton;
    private BagListUIController.DisplayType m_displayType;
    private BagItemBase m_lastClickBagItem;
    private BagListUIController.DisplayType m_lastClickBagItemType;
    private List<BagItemBase> m_bagItemCache;
    private List<BagItemUIController> m_bagItemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private BagListUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitLoopScrollViewRect_hotfix;
    private LuaFunction m_OnPoolObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateViewInBagListDisplayTypeUInt64_hotfix;
    private LuaFunction m_IsBagItemOfDisplayTypeBagItemBase_hotfix;
    private LuaFunction m_ShowNoItemPanelObjBoolean_hotfix;
    private LuaFunction m_BagItemComparerBagItemBaseBagItemBase_hotfix;
    private LuaFunction m_OnBagItemClickUIControllerBase_hotfix;
    private LuaFunction m_OnBagItemNeedFillUIControllerBase_hotfix;
    private LuaFunction m_SetInfoPanelBagItemBase_hotfix;
    private LuaFunction m_SetEquipmentInfoEquipmentBagItem_hotfix;
    private LuaFunction m_SetEquipmentSkillInfoEquipmentBagItem_hotfix;
    private LuaFunction m_SetEquipmentLimitInfoEquipmentBagItem_hotfix;
    private LuaFunction m_SetEquipmentEnchantInfoEquipmentBagItem_hotfix;
    private LuaFunction m_SetEquipmentPropItemPropertyModifyTypeInt32Int32Int32_hotfix;
    private LuaFunction m_SetBagCountLimit_hotfix;
    private LuaFunction m_OnGetItemButtonClick_hotfix;
    private LuaFunction m_OnEquipmentForgeButton_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_ResetBagUIView_hotfix;
    private LuaFunction m_OnAlchemyButtonClick_hotfix;
    private LuaFunction m_OnDescLockButtonClick_hotfix;
    private LuaFunction m_OnUseButtonClick_hotfix;
    private LuaFunction m_OnSubItemUseItemClick_hotfix;
    private LuaFunction m_CloseSubItemUsePanel_hotfix;
    private LuaFunction m_OnInputEditEndString_hotfix;
    private LuaFunction m_OnItemMinusButtonClick_hotfix;
    private LuaFunction m_OnItemAddButtonClick_hotfix;
    private LuaFunction m_OnItemMaxButtonClick_hotfix;
    private LuaFunction m_OnAddAllItemButtonClick_hotfix;
    private LuaFunction m_OnAddAllEquipmentButtonClick_hotfix;
    private LuaFunction m_OnAddItemButtonClick_hotfix;
    private LuaFunction m_OnClearBagButtonClick_hotfix;
    private LuaFunction m_OnSpeedUpButtonClick_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_CloseBagInfoPanel_hotfix;
    private LuaFunction m_OnItemToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnJobMaterialToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnEquipmentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnFragmentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnToggleChanged_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnClearBagAction_hotfix;
    private LuaFunction m_remove_EventOnClearBagAction_hotfix;
    private LuaFunction m_add_EventOnSpeedUpAction`3_hotfix;
    private LuaFunction m_remove_EventOnSpeedUpAction`3_hotfix;
    private LuaFunction m_add_EventOnAddAllItemAction_hotfix;
    private LuaFunction m_remove_EventOnAddAllItemAction_hotfix;
    private LuaFunction m_add_EventOnAddAllEquipmentAction_hotfix;
    private LuaFunction m_remove_EventOnAddAllEquipmentAction_hotfix;
    private LuaFunction m_add_EventOnAlchemyButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnAlchemyButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnAddItemAction`1_hotfix;
    private LuaFunction m_remove_EventOnAddItemAction`1_hotfix;
    private LuaFunction m_add_EventOnUseAction`4_hotfix;
    private LuaFunction m_remove_EventOnUseAction`4_hotfix;
    private LuaFunction m_add_EventOnEquipmentForgeAction`3_hotfix;
    private LuaFunction m_remove_EventOnEquipmentForgeAction`3_hotfix;
    private LuaFunction m_add_EventOnShowGetPathAction`2_hotfix;
    private LuaFunction m_remove_EventOnShowGetPathAction`2_hotfix;
    private LuaFunction m_add_EventOnLockButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnLockButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInBagList(
      BagListUIController.DisplayType displayType,
      ulong clickInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagItemOfDisplayType(BagItemBase itm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNoItemPanelObj(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int BagItemComparer(BagItemBase item1, BagItemBase item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInfoPanel(BagItemBase bagItemBase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentSkillInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentLimitInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentEnchantInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropItem(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBagCountLimit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentForgeButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBagUIView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSubItemUseItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSubItemUsePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInputEditEnd(string inputString)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMinusButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddAllItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddAllEquipmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearBagButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpeedUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseBagInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFragmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearBag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong, BagListUIController.DisplayType> EventOnSpeedUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddAllItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddAllEquipment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAlchemyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int, BagListUIController.DisplayType> EventOnUse
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong, BagListUIController.DisplayType> EventOnEquipmentForge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BagItemBase, BagListUIController.DisplayType> EventOnShowGetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BagListUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClearBag()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClearBag()
    {
      this.EventOnClearBag = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSpeedUp(
      int arg1,
      ulong arg2,
      BagListUIController.DisplayType arg3)
    {
    }

    private void __clearDele_EventOnSpeedUp(
      int arg1,
      ulong arg2,
      BagListUIController.DisplayType arg3)
    {
      this.EventOnSpeedUp = (Action<int, ulong, BagListUIController.DisplayType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddAllItem()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddAllItem()
    {
      this.EventOnAddAllItem = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddAllEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddAllEquipment()
    {
      this.EventOnAddAllEquipment = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAlchemyButtonClick()
    {
      this.EventOnAlchemyButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddItem(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddItem(string obj)
    {
      this.EventOnAddItem = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUse(
      GoodsType arg1,
      int arg2,
      int arg3,
      BagListUIController.DisplayType arg4)
    {
    }

    private void __clearDele_EventOnUse(
      GoodsType arg1,
      int arg2,
      int arg3,
      BagListUIController.DisplayType arg4)
    {
      this.EventOnUse = (Action<GoodsType, int, int, BagListUIController.DisplayType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEquipmentForge(
      int arg1,
      ulong arg2,
      BagListUIController.DisplayType arg3)
    {
    }

    private void __clearDele_EventOnEquipmentForge(
      int arg1,
      ulong arg2,
      BagListUIController.DisplayType arg3)
    {
      this.EventOnEquipmentForge = (Action<int, ulong, BagListUIController.DisplayType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowGetPath(
      BagItemBase arg1,
      BagListUIController.DisplayType arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowGetPath(
      BagItemBase arg1,
      BagListUIController.DisplayType arg2)
    {
      this.EventOnShowGetPath = (Action<BagItemBase, BagListUIController.DisplayType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLockButtonClick(ulong obj)
    {
    }

    private void __clearDele_EventOnLockButtonClick(ulong obj)
    {
      this.EventOnLockButtonClick = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum DisplayType
    {
      None,
      Item,
      Fragment,
      JobMaterial,
      Equipment,
    }

    public class LuaExportHelper
    {
      private BagListUIController m_owner;

      public LuaExportHelper(BagListUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnClearBag()
      {
        this.m_owner.__callDele_EventOnClearBag();
      }

      public void __clearDele_EventOnClearBag()
      {
        this.m_owner.__clearDele_EventOnClearBag();
      }

      public void __callDele_EventOnSpeedUp(
        int arg1,
        ulong arg2,
        BagListUIController.DisplayType arg3)
      {
        this.m_owner.__callDele_EventOnSpeedUp(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnSpeedUp(
        int arg1,
        ulong arg2,
        BagListUIController.DisplayType arg3)
      {
        this.m_owner.__clearDele_EventOnSpeedUp(arg1, arg2, arg3);
      }

      public void __callDele_EventOnAddAllItem()
      {
        this.m_owner.__callDele_EventOnAddAllItem();
      }

      public void __clearDele_EventOnAddAllItem()
      {
        this.m_owner.__clearDele_EventOnAddAllItem();
      }

      public void __callDele_EventOnAddAllEquipment()
      {
        this.m_owner.__callDele_EventOnAddAllEquipment();
      }

      public void __clearDele_EventOnAddAllEquipment()
      {
        this.m_owner.__clearDele_EventOnAddAllEquipment();
      }

      public void __callDele_EventOnAlchemyButtonClick()
      {
        this.m_owner.__callDele_EventOnAlchemyButtonClick();
      }

      public void __clearDele_EventOnAlchemyButtonClick()
      {
        this.m_owner.__clearDele_EventOnAlchemyButtonClick();
      }

      public void __callDele_EventOnAddItem(string obj)
      {
        this.m_owner.__callDele_EventOnAddItem(obj);
      }

      public void __clearDele_EventOnAddItem(string obj)
      {
        this.m_owner.__clearDele_EventOnAddItem(obj);
      }

      public void __callDele_EventOnUse(
        GoodsType arg1,
        int arg2,
        int arg3,
        BagListUIController.DisplayType arg4)
      {
        this.m_owner.__callDele_EventOnUse(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnUse(
        GoodsType arg1,
        int arg2,
        int arg3,
        BagListUIController.DisplayType arg4)
      {
        this.m_owner.__clearDele_EventOnUse(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnEquipmentForge(
        int arg1,
        ulong arg2,
        BagListUIController.DisplayType arg3)
      {
        this.m_owner.__callDele_EventOnEquipmentForge(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnEquipmentForge(
        int arg1,
        ulong arg2,
        BagListUIController.DisplayType arg3)
      {
        this.m_owner.__clearDele_EventOnEquipmentForge(arg1, arg2, arg3);
      }

      public void __callDele_EventOnShowGetPath(
        BagItemBase arg1,
        BagListUIController.DisplayType arg2)
      {
        this.m_owner.__callDele_EventOnShowGetPath(arg1, arg2);
      }

      public void __clearDele_EventOnShowGetPath(
        BagItemBase arg1,
        BagListUIController.DisplayType arg2)
      {
        this.m_owner.__clearDele_EventOnShowGetPath(arg1, arg2);
      }

      public void __callDele_EventOnLockButtonClick(ulong obj)
      {
        this.m_owner.__callDele_EventOnLockButtonClick(obj);
      }

      public void __clearDele_EventOnLockButtonClick(ulong obj)
      {
        this.m_owner.__clearDele_EventOnLockButtonClick(obj);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Toggle m_itemToggle
      {
        get
        {
          return this.m_owner.m_itemToggle;
        }
        set
        {
          this.m_owner.m_itemToggle = value;
        }
      }

      public Toggle m_equipmentToggle
      {
        get
        {
          return this.m_owner.m_equipmentToggle;
        }
        set
        {
          this.m_owner.m_equipmentToggle = value;
        }
      }

      public Toggle m_fragmentToggle
      {
        get
        {
          return this.m_owner.m_fragmentToggle;
        }
        set
        {
          this.m_owner.m_fragmentToggle = value;
        }
      }

      public Toggle m_jobMaterialToggle
      {
        get
        {
          return this.m_owner.m_jobMaterialToggle;
        }
        set
        {
          this.m_owner.m_jobMaterialToggle = value;
        }
      }

      public Button m_alchemyButton
      {
        get
        {
          return this.m_owner.m_alchemyButton;
        }
        set
        {
          this.m_owner.m_alchemyButton = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public LoopVerticalScrollRect m_loopScrollView
      {
        get
        {
          return this.m_owner.m_loopScrollView;
        }
        set
        {
          this.m_owner.m_loopScrollView = value;
        }
      }

      public EasyObjectPool m_listItemPool
      {
        get
        {
          return this.m_owner.m_listItemPool;
        }
        set
        {
          this.m_owner.m_listItemPool = value;
        }
      }

      public GameObject m_listScrollViewItemTemplateRoot
      {
        get
        {
          return this.m_owner.m_listScrollViewItemTemplateRoot;
        }
        set
        {
          this.m_owner.m_listScrollViewItemTemplateRoot = value;
        }
      }

      public GameObject m_scrollViewBagItemContent
      {
        get
        {
          return this.m_owner.m_scrollViewBagItemContent;
        }
        set
        {
          this.m_owner.m_scrollViewBagItemContent = value;
        }
      }

      public GameObject m_bagListPointBgContent
      {
        get
        {
          return this.m_owner.m_bagListPointBgContent;
        }
        set
        {
          this.m_owner.m_bagListPointBgContent = value;
        }
      }

      public GameObject m_bagListPointItem
      {
        get
        {
          return this.m_owner.m_bagListPointItem;
        }
        set
        {
          this.m_owner.m_bagListPointItem = value;
        }
      }

      public GameObject m_noItemPanelObj
      {
        get
        {
          return this.m_owner.m_noItemPanelObj;
        }
        set
        {
          this.m_owner.m_noItemPanelObj = value;
        }
      }

      public GameObject m_bagInfoPanelObj
      {
        get
        {
          return this.m_owner.m_bagInfoPanelObj;
        }
        set
        {
          this.m_owner.m_bagInfoPanelObj = value;
        }
      }

      public CommonUIStateController m_bagInfoStateCtrl
      {
        get
        {
          return this.m_owner.m_bagInfoStateCtrl;
        }
        set
        {
          this.m_owner.m_bagInfoStateCtrl = value;
        }
      }

      public Button m_useButton
      {
        get
        {
          return this.m_owner.m_useButton;
        }
        set
        {
          this.m_owner.m_useButton = value;
        }
      }

      public Text m_itemNameText
      {
        get
        {
          return this.m_owner.m_itemNameText;
        }
        set
        {
          this.m_owner.m_itemNameText = value;
        }
      }

      public Text m_itemCountText
      {
        get
        {
          return this.m_owner.m_itemCountText;
        }
        set
        {
          this.m_owner.m_itemCountText = value;
        }
      }

      public Text m_itemDescText
      {
        get
        {
          return this.m_owner.m_itemDescText;
        }
        set
        {
          this.m_owner.m_itemDescText = value;
        }
      }

      public Image m_itemIconImage
      {
        get
        {
          return this.m_owner.m_itemIconImage;
        }
        set
        {
          this.m_owner.m_itemIconImage = value;
        }
      }

      public Button m_itemGetButton
      {
        get
        {
          return this.m_owner.m_itemGetButton;
        }
        set
        {
          this.m_owner.m_itemGetButton = value;
        }
      }

      public CommonUIStateController m_bagCountLimitStateCtrl
      {
        get
        {
          return this.m_owner.m_bagCountLimitStateCtrl;
        }
        set
        {
          this.m_owner.m_bagCountLimitStateCtrl = value;
        }
      }

      public Text m_bagCountText
      {
        get
        {
          return this.m_owner.m_bagCountText;
        }
        set
        {
          this.m_owner.m_bagCountText = value;
        }
      }

      public Text m_bagMaxCountText
      {
        get
        {
          return this.m_owner.m_bagMaxCountText;
        }
        set
        {
          this.m_owner.m_bagMaxCountText = value;
        }
      }

      public Text m_equipmentInfoExplainText
      {
        get
        {
          return this.m_owner.m_equipmentInfoExplainText;
        }
        set
        {
          this.m_owner.m_equipmentInfoExplainText = value;
        }
      }

      public Button m_equipmentForgeButton
      {
        get
        {
          return this.m_owner.m_equipmentForgeButton;
        }
        set
        {
          this.m_owner.m_equipmentForgeButton = value;
        }
      }

      public CommonUIStateController m_equipmentSkillStateCtrl
      {
        get
        {
          return this.m_owner.m_equipmentSkillStateCtrl;
        }
        set
        {
          this.m_owner.m_equipmentSkillStateCtrl = value;
        }
      }

      public Text m_equipmentSkillNameText
      {
        get
        {
          return this.m_owner.m_equipmentSkillNameText;
        }
        set
        {
          this.m_owner.m_equipmentSkillNameText = value;
        }
      }

      public Text m_equipmentSkillLvValueText
      {
        get
        {
          return this.m_owner.m_equipmentSkillLvValueText;
        }
        set
        {
          this.m_owner.m_equipmentSkillLvValueText = value;
        }
      }

      public Text m_equipmentSkillUnlockCoditionText
      {
        get
        {
          return this.m_owner.m_equipmentSkillUnlockCoditionText;
        }
        set
        {
          this.m_owner.m_equipmentSkillUnlockCoditionText = value;
        }
      }

      public Text m_equipmentSkillCharNameText
      {
        get
        {
          return this.m_owner.m_equipmentSkillCharNameText;
        }
        set
        {
          this.m_owner.m_equipmentSkillCharNameText = value;
        }
      }

      public GameObject m_equipmentSkillCharNameBGImage
      {
        get
        {
          return this.m_owner.m_equipmentSkillCharNameBGImage;
        }
        set
        {
          this.m_owner.m_equipmentSkillCharNameBGImage = value;
        }
      }

      public Text m_equipmentSkillDescText
      {
        get
        {
          return this.m_owner.m_equipmentSkillDescText;
        }
        set
        {
          this.m_owner.m_equipmentSkillDescText = value;
        }
      }

      public CommonUIStateController m_equipmentSkillContentStateCtrl
      {
        get
        {
          return this.m_owner.m_equipmentSkillContentStateCtrl;
        }
        set
        {
          this.m_owner.m_equipmentSkillContentStateCtrl = value;
        }
      }

      public CommonUIStateController m_equipmentLimitStateCtrl
      {
        get
        {
          return this.m_owner.m_equipmentLimitStateCtrl;
        }
        set
        {
          this.m_owner.m_equipmentLimitStateCtrl = value;
        }
      }

      public GameObject m_equipmentLimitContent
      {
        get
        {
          return this.m_owner.m_equipmentLimitContent;
        }
        set
        {
          this.m_owner.m_equipmentLimitContent = value;
        }
      }

      public Text m_equipmentEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_equipmentEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_equipmentEquipUnlimitText = value;
        }
      }

      public GameObject m_equipmentPropertyGroup
      {
        get
        {
          return this.m_owner.m_equipmentPropertyGroup;
        }
        set
        {
          this.m_owner.m_equipmentPropertyGroup = value;
        }
      }

      public GameObject m_equipmentPropATGo
      {
        get
        {
          return this.m_owner.m_equipmentPropATGo;
        }
        set
        {
          this.m_owner.m_equipmentPropATGo = value;
        }
      }

      public Text m_equipmentPropATValueText
      {
        get
        {
          return this.m_owner.m_equipmentPropATValueText;
        }
        set
        {
          this.m_owner.m_equipmentPropATValueText = value;
        }
      }

      public GameObject m_equipmentPropDFGo
      {
        get
        {
          return this.m_owner.m_equipmentPropDFGo;
        }
        set
        {
          this.m_owner.m_equipmentPropDFGo = value;
        }
      }

      public Text m_equipmentPropDFValueText
      {
        get
        {
          return this.m_owner.m_equipmentPropDFValueText;
        }
        set
        {
          this.m_owner.m_equipmentPropDFValueText = value;
        }
      }

      public GameObject m_equipmentPropHPGo
      {
        get
        {
          return this.m_owner.m_equipmentPropHPGo;
        }
        set
        {
          this.m_owner.m_equipmentPropHPGo = value;
        }
      }

      public Text m_equipmentPropHPValueText
      {
        get
        {
          return this.m_owner.m_equipmentPropHPValueText;
        }
        set
        {
          this.m_owner.m_equipmentPropHPValueText = value;
        }
      }

      public GameObject m_equipmentPropMagiccGo
      {
        get
        {
          return this.m_owner.m_equipmentPropMagiccGo;
        }
        set
        {
          this.m_owner.m_equipmentPropMagiccGo = value;
        }
      }

      public Text m_equipmentPropMagicValueText
      {
        get
        {
          return this.m_owner.m_equipmentPropMagicValueText;
        }
        set
        {
          this.m_owner.m_equipmentPropMagicValueText = value;
        }
      }

      public GameObject m_equipmentPropMagicDFGo
      {
        get
        {
          return this.m_owner.m_equipmentPropMagicDFGo;
        }
        set
        {
          this.m_owner.m_equipmentPropMagicDFGo = value;
        }
      }

      public Text m_equipmentPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_equipmentPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_equipmentPropMagicDFValueText = value;
        }
      }

      public GameObject m_equipmentPropDexGo
      {
        get
        {
          return this.m_owner.m_equipmentPropDexGo;
        }
        set
        {
          this.m_owner.m_equipmentPropDexGo = value;
        }
      }

      public Text m_equipmentPropDexValueText
      {
        get
        {
          return this.m_owner.m_equipmentPropDexValueText;
        }
        set
        {
          this.m_owner.m_equipmentPropDexValueText = value;
        }
      }

      public CommonUIStateController m_equipmentPropGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_equipmentPropGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_equipmentPropGroupStateCtrl = value;
        }
      }

      public GameObject m_equipmentPropEnchantmentGroup
      {
        get
        {
          return this.m_owner.m_equipmentPropEnchantmentGroup;
        }
        set
        {
          this.m_owner.m_equipmentPropEnchantmentGroup = value;
        }
      }

      public GameObject m_equipmentPropEnchantmentGroupResonanceGo
      {
        get
        {
          return this.m_owner.m_equipmentPropEnchantmentGroupResonanceGo;
        }
        set
        {
          this.m_owner.m_equipmentPropEnchantmentGroupResonanceGo = value;
        }
      }

      public CommonUIStateController m_equipmentPropEnchantmentGroupRuneStateCtrl
      {
        get
        {
          return this.m_owner.m_equipmentPropEnchantmentGroupRuneStateCtrl;
        }
        set
        {
          this.m_owner.m_equipmentPropEnchantmentGroupRuneStateCtrl = value;
        }
      }

      public Image m_equipmentPropEnchantmentGroupRuneIconImage
      {
        get
        {
          return this.m_owner.m_equipmentPropEnchantmentGroupRuneIconImage;
        }
        set
        {
          this.m_owner.m_equipmentPropEnchantmentGroupRuneIconImage = value;
        }
      }

      public Text m_equipmentPropEnchantmentGroupRuneNameText
      {
        get
        {
          return this.m_owner.m_equipmentPropEnchantmentGroupRuneNameText;
        }
        set
        {
          this.m_owner.m_equipmentPropEnchantmentGroupRuneNameText = value;
        }
      }

      public Button m_descLockButton
      {
        get
        {
          return this.m_owner.m_descLockButton;
        }
        set
        {
          this.m_owner.m_descLockButton = value;
        }
      }

      public GameObject m_subBagInfoPanelObj
      {
        get
        {
          return this.m_owner.m_subBagInfoPanelObj;
        }
        set
        {
          this.m_owner.m_subBagInfoPanelObj = value;
        }
      }

      public Button m_subItemMinusButton
      {
        get
        {
          return this.m_owner.m_subItemMinusButton;
        }
        set
        {
          this.m_owner.m_subItemMinusButton = value;
        }
      }

      public Button m_subItemAddButton
      {
        get
        {
          return this.m_owner.m_subItemAddButton;
        }
        set
        {
          this.m_owner.m_subItemAddButton = value;
        }
      }

      public Button m_subItemMaxButton
      {
        get
        {
          return this.m_owner.m_subItemMaxButton;
        }
        set
        {
          this.m_owner.m_subItemMaxButton = value;
        }
      }

      public InputField m_subItemNumInputField
      {
        get
        {
          return this.m_owner.m_subItemNumInputField;
        }
        set
        {
          this.m_owner.m_subItemNumInputField = value;
        }
      }

      public Button m_subItemUseButton
      {
        get
        {
          return this.m_owner.m_subItemUseButton;
        }
        set
        {
          this.m_owner.m_subItemUseButton = value;
        }
      }

      public Button m_subItemPanelReturnButton
      {
        get
        {
          return this.m_owner.m_subItemPanelReturnButton;
        }
        set
        {
          this.m_owner.m_subItemPanelReturnButton = value;
        }
      }

      public Button m_addAllItemButton
      {
        get
        {
          return this.m_owner.m_addAllItemButton;
        }
        set
        {
          this.m_owner.m_addAllItemButton = value;
        }
      }

      public Button m_addAllEquipmentButton
      {
        get
        {
          return this.m_owner.m_addAllEquipmentButton;
        }
        set
        {
          this.m_owner.m_addAllEquipmentButton = value;
        }
      }

      public Button m_addItemButton
      {
        get
        {
          return this.m_owner.m_addItemButton;
        }
        set
        {
          this.m_owner.m_addItemButton = value;
        }
      }

      public Button m_clearBagItemButton
      {
        get
        {
          return this.m_owner.m_clearBagItemButton;
        }
        set
        {
          this.m_owner.m_clearBagItemButton = value;
        }
      }

      public InputField m_bagItemInputField
      {
        get
        {
          return this.m_owner.m_bagItemInputField;
        }
        set
        {
          this.m_owner.m_bagItemInputField = value;
        }
      }

      public Button m_speedUpButton
      {
        get
        {
          return this.m_owner.m_speedUpButton;
        }
        set
        {
          this.m_owner.m_speedUpButton = value;
        }
      }

      public BagListUIController.DisplayType m_displayType
      {
        get
        {
          return this.m_owner.m_displayType;
        }
        set
        {
          this.m_owner.m_displayType = value;
        }
      }

      public BagItemBase m_lastClickBagItem
      {
        get
        {
          return this.m_owner.m_lastClickBagItem;
        }
        set
        {
          this.m_owner.m_lastClickBagItem = value;
        }
      }

      public BagListUIController.DisplayType m_lastClickBagItemType
      {
        get
        {
          return this.m_owner.m_lastClickBagItemType;
        }
        set
        {
          this.m_owner.m_lastClickBagItemType = value;
        }
      }

      public List<BagItemBase> m_bagItemCache
      {
        get
        {
          return this.m_owner.m_bagItemCache;
        }
        set
        {
          this.m_owner.m_bagItemCache = value;
        }
      }

      public List<BagItemUIController> m_bagItemCtrlList
      {
        get
        {
          return this.m_owner.m_bagItemCtrlList;
        }
        set
        {
          this.m_owner.m_bagItemCtrlList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitLoopScrollViewRect()
      {
        this.m_owner.InitLoopScrollViewRect();
      }

      public void OnPoolObjectCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjectCreated(poolName, go);
      }

      public bool IsBagItemOfDisplayType(BagItemBase itm)
      {
        return this.m_owner.IsBagItemOfDisplayType(itm);
      }

      public void ShowNoItemPanelObj(bool isShow)
      {
        this.m_owner.ShowNoItemPanelObj(isShow);
      }

      public int BagItemComparer(BagItemBase item1, BagItemBase item2)
      {
        return this.m_owner.BagItemComparer(item1, item2);
      }

      public void OnBagItemClick(UIControllerBase itemCtrl)
      {
        this.m_owner.OnBagItemClick(itemCtrl);
      }

      public void OnBagItemNeedFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnBagItemNeedFill(itemCtrl);
      }

      public void SetInfoPanel(BagItemBase bagItemBase)
      {
        this.m_owner.SetInfoPanel(bagItemBase);
      }

      public void SetEquipmentInfo(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentInfo(equipment);
      }

      public void SetEquipmentSkillInfo(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentSkillInfo(equipment);
      }

      public void SetEquipmentLimitInfo(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentLimitInfo(equipment);
      }

      public void SetEquipmentEnchantInfo(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentEnchantInfo(equipment);
      }

      public void SetEquipmentPropItem(
        PropertyModifyType type,
        int value,
        int addValue,
        int level)
      {
        this.m_owner.SetEquipmentPropItem(type, value, addValue, level);
      }

      public void SetBagCountLimit()
      {
        this.m_owner.SetBagCountLimit();
      }

      public void OnGetItemButtonClick()
      {
        this.m_owner.OnGetItemButtonClick();
      }

      public void OnEquipmentForgeButton()
      {
        this.m_owner.OnEquipmentForgeButton();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnAlchemyButtonClick()
      {
        this.m_owner.OnAlchemyButtonClick();
      }

      public void OnDescLockButtonClick()
      {
        this.m_owner.OnDescLockButtonClick();
      }

      public void OnUseButtonClick()
      {
        this.m_owner.OnUseButtonClick();
      }

      public void OnSubItemUseItemClick()
      {
        this.m_owner.OnSubItemUseItemClick();
      }

      public void CloseSubItemUsePanel()
      {
        this.m_owner.CloseSubItemUsePanel();
      }

      public void OnInputEditEnd(string inputString)
      {
        this.m_owner.OnInputEditEnd(inputString);
      }

      public void OnItemMinusButtonClick()
      {
        this.m_owner.OnItemMinusButtonClick();
      }

      public void OnItemAddButtonClick()
      {
        this.m_owner.OnItemAddButtonClick();
      }

      public void OnItemMaxButtonClick()
      {
        this.m_owner.OnItemMaxButtonClick();
      }

      public void OnAddAllItemButtonClick()
      {
        this.m_owner.OnAddAllItemButtonClick();
      }

      public void OnAddAllEquipmentButtonClick()
      {
        this.m_owner.OnAddAllEquipmentButtonClick();
      }

      public void OnAddItemButtonClick()
      {
        this.m_owner.OnAddItemButtonClick();
      }

      public void OnClearBagButtonClick()
      {
        this.m_owner.OnClearBagButtonClick();
      }

      public void OnSpeedUpButtonClick()
      {
        this.m_owner.OnSpeedUpButtonClick();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void OnItemToggleValueChanged(bool on)
      {
        this.m_owner.OnItemToggleValueChanged(on);
      }

      public void OnJobMaterialToggleValueChanged(bool on)
      {
        this.m_owner.OnJobMaterialToggleValueChanged(on);
      }

      public void OnEquipmentToggleValueChanged(bool on)
      {
        this.m_owner.OnEquipmentToggleValueChanged(on);
      }

      public void OnFragmentToggleValueChanged(bool on)
      {
        this.m_owner.OnFragmentToggleValueChanged(on);
      }

      public void OnToggleChanged()
      {
        this.m_owner.OnToggleChanged();
      }
    }
  }
}
