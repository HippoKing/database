﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ProjectLUITaskRegister
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ProjectLUITaskRegister
  {
    [DoNotToLua]
    private ProjectLUITaskRegister.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_RegisterUITasksForLowMem_hotfix;
    private LuaFunction m_RegisterUITasksNormal_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLUITaskRegister()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterUITasksForLowMem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterUITasksNormal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void RegisterUITask(string name, UIGroup group)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void SetUITaskGroupConflict(UIGroup group1, UIGroup group2)
    {
      UIManager.Instance.SetUITaskGroupConflict((uint) group1, (uint) group2);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void RegisterUITask(string name, UIGroup4LowMem group)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void SetUITaskGroupConflict(UIGroup4LowMem group1, UIGroup4LowMem group2)
    {
      UIManager.Instance.SetUITaskGroupConflict((uint) group1, (uint) group2);
    }

    [DoNotToLua]
    public ProjectLUITaskRegister.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ProjectLUITaskRegister m_owner;

      public LuaExportHelper(ProjectLUITaskRegister owner)
      {
        this.m_owner = owner;
      }

      public void RegisterUITasksForLowMem()
      {
        this.m_owner.RegisterUITasksForLowMem();
      }

      public void RegisterUITasksNormal()
      {
        this.m_owner.RegisterUITasksNormal();
      }

      public static void RegisterUITask(string name, UIGroup group)
      {
        ProjectLUITaskRegister.RegisterUITask(name, group);
      }

      public static void SetUITaskGroupConflict(UIGroup group1, UIGroup group2)
      {
        ProjectLUITaskRegister.SetUITaskGroupConflict(group1, group2);
      }

      public static void RegisterUITask(string name, UIGroup4LowMem group)
      {
        ProjectLUITaskRegister.RegisterUITask(name, group);
      }

      public static void SetUITaskGroupConflict(UIGroup4LowMem group1, UIGroup4LowMem group2)
      {
        ProjectLUITaskRegister.SetUITaskGroupConflict(group1, group2);
      }
    }
  }
}
