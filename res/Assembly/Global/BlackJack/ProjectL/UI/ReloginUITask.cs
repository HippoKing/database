﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReloginUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ReloginUITask : ReloginBySessionUITaskBase
  {
    private static bool s_isEnable = true;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ReloginUIController m_reloginUIController;
    private bool m_isAutoRelogin;
    private static Action s_prevReloginSuccessCallBack;

    [MethodImpl((MethodImplOptions) 32768)]
    public ReloginUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
    }

    public static bool Enable
    {
      get
      {
        return ReloginUITask.s_isEnable;
      }
      set
      {
        ReloginUITask.s_isEnable = value;
      }
    }

    protected override void StartRelogin()
    {
      LoginUITask.CheckServerMaitainState();
      base.StartRelogin();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ShowErrorMsg(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnRetryLoginButtonClicked(UIControllerBase ctrl)
    {
      base.OnRetryLoginButtonClicked(ctrl);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnReturnToLoginConfirmButtonClicked(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
