﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareActorInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattlePrepareActorInfoUIController : UIControllerBase
  {
    [AutoBind("./Hero", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGameObject;
    [AutoBind("./Hero/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Hero/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroArmyIcon;
    [AutoBind("./Hero/Job/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroJobText;
    [AutoBind("./Hero/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLevelText;
    [AutoBind("./Hero/Range/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroRangeText;
    [AutoBind("./Hero/Move/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMoveText;
    [AutoBind("./HeroPanelGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroPanelGroupCtrl;
    [AutoBind("./HeroPanelGroup/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIcon;
    [AutoBind("./Hero/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroHPText;
    [AutoBind("./Hero/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHPImage;
    [AutoBind("./Hero/DEX/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDEXText;
    [AutoBind("./Hero/Attack/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroAttackText;
    [AutoBind("./Hero/Defense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDefenseText;
    [AutoBind("./Hero/Magic/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicText;
    [AutoBind("./Hero/MagicDefense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicDefenseText;
    [AutoBind("./Hero/SkillInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroSkillInfoButton;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/TalentSkill", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillInfoGroupTalentSkillImage;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo2;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo3;
    [AutoBind("./Hero/ChangeSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillChangeButton;
    [AutoBind("./SelectSkill", AutoBindAttribute.InitState.Active, false)]
    private GameObject m_selectSkillPanelGo;
    [AutoBind("./SkillList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailObj;
    [AutoBind("./SkillList/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailListObj;
    [AutoBind("./SkillList/List/Content/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentTalentObj;
    [AutoBind("./SkillList/List/Content/Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillListContentTalentIcon;
    [AutoBind("./SkillList/List/Content/Talent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillListContentTalentNameText;
    [AutoBind("./SkillList/List/Content/Talent/DescTextScrollRect/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillListContentTalentDescText;
    [AutoBind("./SkillList/List/Content/LineImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillListContentLineImage;
    [AutoBind("./SkillList/List/Content/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentListObj;
    [AutoBind("./SkillList/List/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentObj;
    [AutoBind("./SkillList/List/NoSkills", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListNoSkillsObj;
    [AutoBind("./Prefabs/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDescPrefabObj;
    [AutoBind("./Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGo;
    [AutoBind("./Soldier/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierChangeButton;
    [AutoBind("./SelectSoldier/ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeSelectSoldierButton;
    [AutoBind("./SelectSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_selectSoldierPanelUIStateController;
    [AutoBind("./SelectSoldier/SoldierItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldiersContent;
    [AutoBind("./SelectSoldier/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectSoldierDetailPanel;
    [AutoBind("./Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicGo;
    [AutoBind("./Soldier/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierArmyTypeImg;
    [AutoBind("./Soldier/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./Soldier/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierInfoBtn;
    [AutoBind("./Soldier/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPText;
    [AutoBind("./Soldier/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierPropHPImage;
    [AutoBind("./Soldier/Prop/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFText;
    [AutoBind("./Soldier/Prop/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATText;
    [AutoBind("./Soldier/Prop/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFText;
    [AutoBind("./Soldier/Prop/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddText;
    [AutoBind("./Soldier/Prop/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddText;
    [AutoBind("./Soldier/Prop/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddText;
    [AutoBind("./Soldier/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelUIStateController;
    [AutoBind("./Soldier/SoldierDetailPanel/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailIconImg;
    [AutoBind("./Soldier/SoldierDetailPanel/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailGraphicGo;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailRangeText;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailMoveText;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailTypeBgImg;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailTypeBgImg2;
    [AutoBind("./Soldier/SoldierDetailPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailTitleText;
    [AutoBind("./Soldier/SoldierDetailPanel/Desc/DescTextScroll/Mask/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailDescText;
    [AutoBind("./Soldier/SoldierDetailPanel/Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailWeakText;
    [AutoBind("./Soldier/SoldierDetailPanel/Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailStrongText;
    [AutoBind("./Soldier/SoldierDetailPanel/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailHPText;
    [AutoBind("./Soldier/SoldierDetailPanel/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailDFText;
    [AutoBind("./Soldier/SoldierDetailPanel/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailATText;
    [AutoBind("./Soldier/SoldierDetailPanel/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailMagicDFText;
    [AutoBind("./Soldier/SoldierDetailPanel/HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailHPAddText;
    [AutoBind("./Soldier/SoldierDetailPanel/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailDFAddText;
    [AutoBind("./Soldier/SoldierDetailPanel/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailATAddText;
    [AutoBind("./Soldier/SoldierDetailPanel/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailMagicDFAddText;
    private BattleHero m_hero;
    private List<TrainingTech> m_trainingTechs;
    private int m_team;
    private bool m_isNpc;
    private bool m_canChangeSoldier;
    private bool m_canChangeSkill;
    private int m_heroHp;
    private int m_soldierHp;
    private ConfigDataSkillInfo m_extraTalentSkillInfo;
    private bool m_isOpened;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private List<int> m_selectSkillIds;
    private BattleSoldierItemUIController m_lastClickSoldierItem;
    private int m_maxSkillCost;
    private UISpineGraphic m_soldierGraphic;
    private UISpineGraphic m_soldierDetailGraphic;
    private GameObjectPool<SkillDesc> m_skillDescPool;
    private HeroDetailSelectSkillUIController m_selectSkillPanelUICtrl;
    [DoNotToLua]
    private BattlePrepareActorInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_OpenBoolean_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_IsOpened_hotfix;
    private LuaFunction m_SetActorInfoBattleHeroList`1Int32BooleanBooleanBooleanConfigDataSkillInfoInt32Int32_hotfix;
    private LuaFunction m_GetHeroId_hotfix;
    private LuaFunction m_GetTeam_hotfix;
    private LuaFunction m_CanChangeSoldier_hotfix;
    private LuaFunction m_CanChangeSkill_hotfix;
    private LuaFunction m_SetSkills_hotfix;
    private LuaFunction m_SetSkillsAndTalentList_hotfix;
    private LuaFunction m_SetSoldier_hotfix;
    private LuaFunction m_SetSoldierDetailPanel_hotfix;
    private LuaFunction m_ShowSelectSoldierPanel_hotfix;
    private LuaFunction m_CloseSelectSoldierPanel_hotfix;
    private LuaFunction m_OnSoldierDetailSelectButtonClickBattleSoldierItemUIController_hotfix;
    private LuaFunction m_OnSoldierItemClickBattleSoldierItemUIController_hotfix;
    private LuaFunction m_CalcPropAddValueInt32Int32_hotfix;
    private LuaFunction m_GetSoliderSelectButtonRectTransformInt32_hotfix;
    private LuaFunction m_SelectSoliderInt32_hotfix;
    private LuaFunction m_SetPropLevelItemImgImageChar_hotfix;
    private LuaFunction m_CreateSpineGraphicStringSingleVector2GameObjectList`1_hotfix;
    private LuaFunction m_DestroySpineGraphicUISpineGraphic_hotfix;
    private LuaFunction m_OnChangeSkillButtonClick_hotfix;
    private LuaFunction m_SelectSkillPanelUICtrl_OnHeroSkillsSelectInt32List`1Boolean_hotfix;
    private LuaFunction m_OnChangeSoldierButtonClick_hotfix;
    private LuaFunction m_add_EventOnShowSelectSoldierPanelAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowSelectSoldierPanelAction`1_hotfix;
    private LuaFunction m_add_EventOnChangeSkillAction`2_hotfix;
    private LuaFunction m_remove_EventOnChangeSkillAction`2_hotfix;
    private LuaFunction m_add_EventOnChangeSoldierAction`2_hotfix;
    private LuaFunction m_remove_EventOnChangeSoldierAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattlePrepareActorInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(bool left)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorInfo(
      BattleHero hero,
      List<TrainingTech> techs,
      int team,
      bool isNpc,
      bool canChangeSoldier,
      bool canChangeSkill,
      ConfigDataSkillInfo extraTalentSkillInfo = null,
      int heroHp = -1,
      int soldierHp = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanChangeSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanChangeSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillsAndTalentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectSoldierPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSelectSoldierPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierDetailSelectButtonClick(BattleSoldierItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick(BattleSoldierItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropAddValue(int v0, int v1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetSoliderSelectButtonRectTransform(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SelectSolider(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropLevelItemImg(Image img, char level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectSkillPanelUICtrl_OnHeroSkillsSelect(
      int heroId,
      List<int> skillIdList,
      bool isSkillChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSoldierButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleHero> EventOnShowSelectSoldierPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero, List<int>> EventOnChangeSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero, int> EventOnChangeSoldier
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattlePrepareActorInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowSelectSoldierPanel(BattleHero obj)
    {
    }

    private void __clearDele_EventOnShowSelectSoldierPanel(BattleHero obj)
    {
      this.EventOnShowSelectSoldierPanel = (Action<BattleHero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeSkill(BattleHero arg1, List<int> arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeSkill(BattleHero arg1, List<int> arg2)
    {
      this.EventOnChangeSkill = (Action<BattleHero, List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeSoldier(BattleHero arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeSoldier(BattleHero arg1, int arg2)
    {
      this.EventOnChangeSoldier = (Action<BattleHero, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattlePrepareActorInfoUIController m_owner;

      public LuaExportHelper(BattlePrepareActorInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnShowSelectSoldierPanel(BattleHero obj)
      {
        this.m_owner.__callDele_EventOnShowSelectSoldierPanel(obj);
      }

      public void __clearDele_EventOnShowSelectSoldierPanel(BattleHero obj)
      {
        this.m_owner.__clearDele_EventOnShowSelectSoldierPanel(obj);
      }

      public void __callDele_EventOnChangeSkill(BattleHero arg1, List<int> arg2)
      {
        this.m_owner.__callDele_EventOnChangeSkill(arg1, arg2);
      }

      public void __clearDele_EventOnChangeSkill(BattleHero arg1, List<int> arg2)
      {
        this.m_owner.__clearDele_EventOnChangeSkill(arg1, arg2);
      }

      public void __callDele_EventOnChangeSoldier(BattleHero arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnChangeSoldier(arg1, arg2);
      }

      public void __clearDele_EventOnChangeSoldier(BattleHero arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnChangeSoldier(arg1, arg2);
      }

      public GameObject m_heroGameObject
      {
        get
        {
          return this.m_owner.m_heroGameObject;
        }
        set
        {
          this.m_owner.m_heroGameObject = value;
        }
      }

      public Text m_heroNameText
      {
        get
        {
          return this.m_owner.m_heroNameText;
        }
        set
        {
          this.m_owner.m_heroNameText = value;
        }
      }

      public Image m_heroArmyIcon
      {
        get
        {
          return this.m_owner.m_heroArmyIcon;
        }
        set
        {
          this.m_owner.m_heroArmyIcon = value;
        }
      }

      public Text m_heroJobText
      {
        get
        {
          return this.m_owner.m_heroJobText;
        }
        set
        {
          this.m_owner.m_heroJobText = value;
        }
      }

      public Text m_heroLevelText
      {
        get
        {
          return this.m_owner.m_heroLevelText;
        }
        set
        {
          this.m_owner.m_heroLevelText = value;
        }
      }

      public Text m_heroRangeText
      {
        get
        {
          return this.m_owner.m_heroRangeText;
        }
        set
        {
          this.m_owner.m_heroRangeText = value;
        }
      }

      public Text m_heroMoveText
      {
        get
        {
          return this.m_owner.m_heroMoveText;
        }
        set
        {
          this.m_owner.m_heroMoveText = value;
        }
      }

      public CommonUIStateController m_heroPanelGroupCtrl
      {
        get
        {
          return this.m_owner.m_heroPanelGroupCtrl;
        }
        set
        {
          this.m_owner.m_heroPanelGroupCtrl = value;
        }
      }

      public Image m_heroIcon
      {
        get
        {
          return this.m_owner.m_heroIcon;
        }
        set
        {
          this.m_owner.m_heroIcon = value;
        }
      }

      public Text m_heroHPText
      {
        get
        {
          return this.m_owner.m_heroHPText;
        }
        set
        {
          this.m_owner.m_heroHPText = value;
        }
      }

      public Image m_heroHPImage
      {
        get
        {
          return this.m_owner.m_heroHPImage;
        }
        set
        {
          this.m_owner.m_heroHPImage = value;
        }
      }

      public Text m_heroDEXText
      {
        get
        {
          return this.m_owner.m_heroDEXText;
        }
        set
        {
          this.m_owner.m_heroDEXText = value;
        }
      }

      public Text m_heroAttackText
      {
        get
        {
          return this.m_owner.m_heroAttackText;
        }
        set
        {
          this.m_owner.m_heroAttackText = value;
        }
      }

      public Text m_heroDefenseText
      {
        get
        {
          return this.m_owner.m_heroDefenseText;
        }
        set
        {
          this.m_owner.m_heroDefenseText = value;
        }
      }

      public Text m_heroMagicText
      {
        get
        {
          return this.m_owner.m_heroMagicText;
        }
        set
        {
          this.m_owner.m_heroMagicText = value;
        }
      }

      public Text m_heroMagicDefenseText
      {
        get
        {
          return this.m_owner.m_heroMagicDefenseText;
        }
        set
        {
          this.m_owner.m_heroMagicDefenseText = value;
        }
      }

      public Button m_heroSkillInfoButton
      {
        get
        {
          return this.m_owner.m_heroSkillInfoButton;
        }
        set
        {
          this.m_owner.m_heroSkillInfoButton = value;
        }
      }

      public Image m_skillInfoGroupTalentSkillImage
      {
        get
        {
          return this.m_owner.m_skillInfoGroupTalentSkillImage;
        }
        set
        {
          this.m_owner.m_skillInfoGroupTalentSkillImage = value;
        }
      }

      public GameObject m_skillInfoGroupSkillGo
      {
        get
        {
          return this.m_owner.m_skillInfoGroupSkillGo;
        }
        set
        {
          this.m_owner.m_skillInfoGroupSkillGo = value;
        }
      }

      public GameObject m_skillInfoGroupSkillGo2
      {
        get
        {
          return this.m_owner.m_skillInfoGroupSkillGo2;
        }
        set
        {
          this.m_owner.m_skillInfoGroupSkillGo2 = value;
        }
      }

      public GameObject m_skillInfoGroupSkillGo3
      {
        get
        {
          return this.m_owner.m_skillInfoGroupSkillGo3;
        }
        set
        {
          this.m_owner.m_skillInfoGroupSkillGo3 = value;
        }
      }

      public Button m_skillChangeButton
      {
        get
        {
          return this.m_owner.m_skillChangeButton;
        }
        set
        {
          this.m_owner.m_skillChangeButton = value;
        }
      }

      public GameObject m_selectSkillPanelGo
      {
        get
        {
          return this.m_owner.m_selectSkillPanelGo;
        }
        set
        {
          this.m_owner.m_selectSkillPanelGo = value;
        }
      }

      public GameObject m_skillDetailObj
      {
        get
        {
          return this.m_owner.m_skillDetailObj;
        }
        set
        {
          this.m_owner.m_skillDetailObj = value;
        }
      }

      public GameObject m_skillDetailListObj
      {
        get
        {
          return this.m_owner.m_skillDetailListObj;
        }
        set
        {
          this.m_owner.m_skillDetailListObj = value;
        }
      }

      public GameObject m_skillListContentTalentObj
      {
        get
        {
          return this.m_owner.m_skillListContentTalentObj;
        }
        set
        {
          this.m_owner.m_skillListContentTalentObj = value;
        }
      }

      public Image m_skillListContentTalentIcon
      {
        get
        {
          return this.m_owner.m_skillListContentTalentIcon;
        }
        set
        {
          this.m_owner.m_skillListContentTalentIcon = value;
        }
      }

      public Text m_skillListContentTalentNameText
      {
        get
        {
          return this.m_owner.m_skillListContentTalentNameText;
        }
        set
        {
          this.m_owner.m_skillListContentTalentNameText = value;
        }
      }

      public Text m_skillListContentTalentDescText
      {
        get
        {
          return this.m_owner.m_skillListContentTalentDescText;
        }
        set
        {
          this.m_owner.m_skillListContentTalentDescText = value;
        }
      }

      public Image m_skillListContentLineImage
      {
        get
        {
          return this.m_owner.m_skillListContentLineImage;
        }
        set
        {
          this.m_owner.m_skillListContentLineImage = value;
        }
      }

      public GameObject m_skillListContentListObj
      {
        get
        {
          return this.m_owner.m_skillListContentListObj;
        }
        set
        {
          this.m_owner.m_skillListContentListObj = value;
        }
      }

      public GameObject m_skillListContentObj
      {
        get
        {
          return this.m_owner.m_skillListContentObj;
        }
        set
        {
          this.m_owner.m_skillListContentObj = value;
        }
      }

      public GameObject m_skillListNoSkillsObj
      {
        get
        {
          return this.m_owner.m_skillListNoSkillsObj;
        }
        set
        {
          this.m_owner.m_skillListNoSkillsObj = value;
        }
      }

      public GameObject m_skillDescPrefabObj
      {
        get
        {
          return this.m_owner.m_skillDescPrefabObj;
        }
        set
        {
          this.m_owner.m_skillDescPrefabObj = value;
        }
      }

      public GameObject m_soldierGo
      {
        get
        {
          return this.m_owner.m_soldierGo;
        }
        set
        {
          this.m_owner.m_soldierGo = value;
        }
      }

      public Button m_soldierChangeButton
      {
        get
        {
          return this.m_owner.m_soldierChangeButton;
        }
        set
        {
          this.m_owner.m_soldierChangeButton = value;
        }
      }

      public Button m_closeSelectSoldierButton
      {
        get
        {
          return this.m_owner.m_closeSelectSoldierButton;
        }
        set
        {
          this.m_owner.m_closeSelectSoldierButton = value;
        }
      }

      public CommonUIStateController m_selectSoldierPanelUIStateController
      {
        get
        {
          return this.m_owner.m_selectSoldierPanelUIStateController;
        }
        set
        {
          this.m_owner.m_selectSoldierPanelUIStateController = value;
        }
      }

      public GameObject m_soldiersContent
      {
        get
        {
          return this.m_owner.m_soldiersContent;
        }
        set
        {
          this.m_owner.m_soldiersContent = value;
        }
      }

      public GameObject m_selectSoldierDetailPanel
      {
        get
        {
          return this.m_owner.m_selectSoldierDetailPanel;
        }
        set
        {
          this.m_owner.m_selectSoldierDetailPanel = value;
        }
      }

      public GameObject m_soldierGraphicGo
      {
        get
        {
          return this.m_owner.m_soldierGraphicGo;
        }
        set
        {
          this.m_owner.m_soldierGraphicGo = value;
        }
      }

      public Image m_soldierArmyTypeImg
      {
        get
        {
          return this.m_owner.m_soldierArmyTypeImg;
        }
        set
        {
          this.m_owner.m_soldierArmyTypeImg = value;
        }
      }

      public Text m_soldierNameText
      {
        get
        {
          return this.m_owner.m_soldierNameText;
        }
        set
        {
          this.m_owner.m_soldierNameText = value;
        }
      }

      public Button m_soldierInfoBtn
      {
        get
        {
          return this.m_owner.m_soldierInfoBtn;
        }
        set
        {
          this.m_owner.m_soldierInfoBtn = value;
        }
      }

      public Text m_soldierPropHPText
      {
        get
        {
          return this.m_owner.m_soldierPropHPText;
        }
        set
        {
          this.m_owner.m_soldierPropHPText = value;
        }
      }

      public Image m_soldierPropHPImage
      {
        get
        {
          return this.m_owner.m_soldierPropHPImage;
        }
        set
        {
          this.m_owner.m_soldierPropHPImage = value;
        }
      }

      public Text m_soldierPropDFText
      {
        get
        {
          return this.m_owner.m_soldierPropDFText;
        }
        set
        {
          this.m_owner.m_soldierPropDFText = value;
        }
      }

      public Text m_soldierPropATText
      {
        get
        {
          return this.m_owner.m_soldierPropATText;
        }
        set
        {
          this.m_owner.m_soldierPropATText = value;
        }
      }

      public Text m_soldierMagicDFText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFText = value;
        }
      }

      public Text m_soldierDFAddText
      {
        get
        {
          return this.m_owner.m_soldierDFAddText;
        }
        set
        {
          this.m_owner.m_soldierDFAddText = value;
        }
      }

      public Text m_soldierATAddText
      {
        get
        {
          return this.m_owner.m_soldierATAddText;
        }
        set
        {
          this.m_owner.m_soldierATAddText = value;
        }
      }

      public Text m_soldierMagicDFAddText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFAddText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFAddText = value;
        }
      }

      public CommonUIStateController m_soldierDetailPanelUIStateController
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelUIStateController;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelUIStateController = value;
        }
      }

      public Image m_soldierDetailIconImg
      {
        get
        {
          return this.m_owner.m_soldierDetailIconImg;
        }
        set
        {
          this.m_owner.m_soldierDetailIconImg = value;
        }
      }

      public GameObject m_soldierDetailGraphicGo
      {
        get
        {
          return this.m_owner.m_soldierDetailGraphicGo;
        }
        set
        {
          this.m_owner.m_soldierDetailGraphicGo = value;
        }
      }

      public Text m_soldierDetailRangeText
      {
        get
        {
          return this.m_owner.m_soldierDetailRangeText;
        }
        set
        {
          this.m_owner.m_soldierDetailRangeText = value;
        }
      }

      public Text m_soldierDetailMoveText
      {
        get
        {
          return this.m_owner.m_soldierDetailMoveText;
        }
        set
        {
          this.m_owner.m_soldierDetailMoveText = value;
        }
      }

      public Image m_soldierDetailTypeBgImg
      {
        get
        {
          return this.m_owner.m_soldierDetailTypeBgImg;
        }
        set
        {
          this.m_owner.m_soldierDetailTypeBgImg = value;
        }
      }

      public Image m_soldierDetailTypeBgImg2
      {
        get
        {
          return this.m_owner.m_soldierDetailTypeBgImg2;
        }
        set
        {
          this.m_owner.m_soldierDetailTypeBgImg2 = value;
        }
      }

      public Text m_soldierDetailTitleText
      {
        get
        {
          return this.m_owner.m_soldierDetailTitleText;
        }
        set
        {
          this.m_owner.m_soldierDetailTitleText = value;
        }
      }

      public Text m_soldierDetailDescText
      {
        get
        {
          return this.m_owner.m_soldierDetailDescText;
        }
        set
        {
          this.m_owner.m_soldierDetailDescText = value;
        }
      }

      public Text m_soldierDetailWeakText
      {
        get
        {
          return this.m_owner.m_soldierDetailWeakText;
        }
        set
        {
          this.m_owner.m_soldierDetailWeakText = value;
        }
      }

      public Text m_soldierDetailStrongText
      {
        get
        {
          return this.m_owner.m_soldierDetailStrongText;
        }
        set
        {
          this.m_owner.m_soldierDetailStrongText = value;
        }
      }

      public Text m_soldierDetailHPText
      {
        get
        {
          return this.m_owner.m_soldierDetailHPText;
        }
        set
        {
          this.m_owner.m_soldierDetailHPText = value;
        }
      }

      public Text m_soldierDetailDFText
      {
        get
        {
          return this.m_owner.m_soldierDetailDFText;
        }
        set
        {
          this.m_owner.m_soldierDetailDFText = value;
        }
      }

      public Text m_soldierDetailATText
      {
        get
        {
          return this.m_owner.m_soldierDetailATText;
        }
        set
        {
          this.m_owner.m_soldierDetailATText = value;
        }
      }

      public Text m_soldierDetailMagicDFText
      {
        get
        {
          return this.m_owner.m_soldierDetailMagicDFText;
        }
        set
        {
          this.m_owner.m_soldierDetailMagicDFText = value;
        }
      }

      public Text m_soldierDetailHPAddText
      {
        get
        {
          return this.m_owner.m_soldierDetailHPAddText;
        }
        set
        {
          this.m_owner.m_soldierDetailHPAddText = value;
        }
      }

      public Text m_soldierDetailDFAddText
      {
        get
        {
          return this.m_owner.m_soldierDetailDFAddText;
        }
        set
        {
          this.m_owner.m_soldierDetailDFAddText = value;
        }
      }

      public Text m_soldierDetailATAddText
      {
        get
        {
          return this.m_owner.m_soldierDetailATAddText;
        }
        set
        {
          this.m_owner.m_soldierDetailATAddText = value;
        }
      }

      public Text m_soldierDetailMagicDFAddText
      {
        get
        {
          return this.m_owner.m_soldierDetailMagicDFAddText;
        }
        set
        {
          this.m_owner.m_soldierDetailMagicDFAddText = value;
        }
      }

      public BattleHero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public List<TrainingTech> m_trainingTechs
      {
        get
        {
          return this.m_owner.m_trainingTechs;
        }
        set
        {
          this.m_owner.m_trainingTechs = value;
        }
      }

      public int m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public bool m_isNpc
      {
        get
        {
          return this.m_owner.m_isNpc;
        }
        set
        {
          this.m_owner.m_isNpc = value;
        }
      }

      public bool m_canChangeSoldier
      {
        get
        {
          return this.m_owner.m_canChangeSoldier;
        }
        set
        {
          this.m_owner.m_canChangeSoldier = value;
        }
      }

      public bool m_canChangeSkill
      {
        get
        {
          return this.m_owner.m_canChangeSkill;
        }
        set
        {
          this.m_owner.m_canChangeSkill = value;
        }
      }

      public int m_heroHp
      {
        get
        {
          return this.m_owner.m_heroHp;
        }
        set
        {
          this.m_owner.m_heroHp = value;
        }
      }

      public int m_soldierHp
      {
        get
        {
          return this.m_owner.m_soldierHp;
        }
        set
        {
          this.m_owner.m_soldierHp = value;
        }
      }

      public ConfigDataSkillInfo m_extraTalentSkillInfo
      {
        get
        {
          return this.m_owner.m_extraTalentSkillInfo;
        }
        set
        {
          this.m_owner.m_extraTalentSkillInfo = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public List<int> m_selectSkillIds
      {
        get
        {
          return this.m_owner.m_selectSkillIds;
        }
        set
        {
          this.m_owner.m_selectSkillIds = value;
        }
      }

      public BattleSoldierItemUIController m_lastClickSoldierItem
      {
        get
        {
          return this.m_owner.m_lastClickSoldierItem;
        }
        set
        {
          this.m_owner.m_lastClickSoldierItem = value;
        }
      }

      public int m_maxSkillCost
      {
        get
        {
          return this.m_owner.m_maxSkillCost;
        }
        set
        {
          this.m_owner.m_maxSkillCost = value;
        }
      }

      public UISpineGraphic m_soldierGraphic
      {
        get
        {
          return this.m_owner.m_soldierGraphic;
        }
        set
        {
          this.m_owner.m_soldierGraphic = value;
        }
      }

      public UISpineGraphic m_soldierDetailGraphic
      {
        get
        {
          return this.m_owner.m_soldierDetailGraphic;
        }
        set
        {
          this.m_owner.m_soldierDetailGraphic = value;
        }
      }

      public GameObjectPool<SkillDesc> m_skillDescPool
      {
        get
        {
          return this.m_owner.m_skillDescPool;
        }
        set
        {
          this.m_owner.m_skillDescPool = value;
        }
      }

      public HeroDetailSelectSkillUIController m_selectSkillPanelUICtrl
      {
        get
        {
          return this.m_owner.m_selectSkillPanelUICtrl;
        }
        set
        {
          this.m_owner.m_selectSkillPanelUICtrl = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void SetSkillsAndTalentList()
      {
        this.m_owner.SetSkillsAndTalentList();
      }

      public void SetSoldierDetailPanel()
      {
        this.m_owner.SetSoldierDetailPanel();
      }

      public void CloseSelectSoldierPanel()
      {
        this.m_owner.CloseSelectSoldierPanel();
      }

      public void OnSoldierDetailSelectButtonClick(BattleSoldierItemUIController ctrl)
      {
        this.m_owner.OnSoldierDetailSelectButtonClick(ctrl);
      }

      public void OnSoldierItemClick(BattleSoldierItemUIController ctrl)
      {
        this.m_owner.OnSoldierItemClick(ctrl);
      }

      public string CalcPropAddValue(int v0, int v1)
      {
        return this.m_owner.CalcPropAddValue(v0, v1);
      }

      public void SetPropLevelItemImg(Image img, char level)
      {
        this.m_owner.SetPropLevelItemImg(img, level);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UISpineGraphic CreateSpineGraphic(
        string assetName,
        float scale,
        Vector2 offset,
        GameObject parent,
        List<ReplaceAnim> replaceAnims)
      {
        // ISSUE: unable to decompile the method.
      }

      public void DestroySpineGraphic(UISpineGraphic g)
      {
        this.m_owner.DestroySpineGraphic(g);
      }

      public void OnChangeSkillButtonClick()
      {
        this.m_owner.OnChangeSkillButtonClick();
      }

      public void SelectSkillPanelUICtrl_OnHeroSkillsSelect(
        int heroId,
        List<int> skillIdList,
        bool isSkillChanged)
      {
        this.m_owner.SelectSkillPanelUICtrl_OnHeroSkillsSelect(heroId, skillIdList, isSkillChanged);
      }

      public void OnChangeSoldierButtonClick()
      {
        this.m_owner.OnChangeSoldierButtonClick();
      }
    }
  }
}
