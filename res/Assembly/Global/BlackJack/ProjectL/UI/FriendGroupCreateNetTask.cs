﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendGroupCreateNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FriendGroupCreateNetTask : UINetTask
  {
    private string m_groupNmae;
    private List<string> m_userIDList;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendGroupCreateNetTask(string groupName, List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFriendGroupCreateAck(
      int result,
      ProChatGroupInfo chatGroupInfo,
      ProChatUserInfo userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public ProChatGroupInfo ChatGroupInfo { private set; get; }

    public ProChatUserInfo FailedUser { private set; get; }
  }
}
