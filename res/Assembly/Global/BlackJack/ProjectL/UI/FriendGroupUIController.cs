﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendGroupUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FriendGroupUIController : UIControllerBase
  {
    [AutoBind("./GroupIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_groupIconImage;
    [AutoBind("./GroupIconImage/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_groupHeadFrameTransform;
    [AutoBind("./ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./WatchStaffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchStaffButton;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlinePlayerNumText;
    [AutoBind("./GroupNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_groupNameText;
    private IConfigDataLoader m_configDataLoader;
    private ProChatGroupCompactInfo m_groupInfo;
    [DoNotToLua]
    private FriendGroupUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetGroupInfoProChatGroupCompactInfo_hotfix;
    private LuaFunction m_GetGroupInfo_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnWatchStaffButtonClick_hotfix;
    private LuaFunction m_add_EventOnWatchGroupStaffButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnWatchGroupStaffButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnChatButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnChatButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendGroupUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupInfo(ProChatGroupCompactInfo groupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatGroupCompactInfo GetGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchStaffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FriendGroupUIController> EventOnWatchGroupStaffButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendGroupUIController> EventOnChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FriendGroupUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWatchGroupStaffButtonClick(FriendGroupUIController obj)
    {
    }

    private void __clearDele_EventOnWatchGroupStaffButtonClick(FriendGroupUIController obj)
    {
      this.EventOnWatchGroupStaffButtonClick = (Action<FriendGroupUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChatButtonClick(FriendGroupUIController obj)
    {
    }

    private void __clearDele_EventOnChatButtonClick(FriendGroupUIController obj)
    {
      this.EventOnChatButtonClick = (Action<FriendGroupUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FriendGroupUIController m_owner;

      public LuaExportHelper(FriendGroupUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnWatchGroupStaffButtonClick(FriendGroupUIController obj)
      {
        this.m_owner.__callDele_EventOnWatchGroupStaffButtonClick(obj);
      }

      public void __clearDele_EventOnWatchGroupStaffButtonClick(FriendGroupUIController obj)
      {
        this.m_owner.__clearDele_EventOnWatchGroupStaffButtonClick(obj);
      }

      public void __callDele_EventOnChatButtonClick(FriendGroupUIController obj)
      {
        this.m_owner.__callDele_EventOnChatButtonClick(obj);
      }

      public void __clearDele_EventOnChatButtonClick(FriendGroupUIController obj)
      {
        this.m_owner.__clearDele_EventOnChatButtonClick(obj);
      }

      public Image m_groupIconImage
      {
        get
        {
          return this.m_owner.m_groupIconImage;
        }
        set
        {
          this.m_owner.m_groupIconImage = value;
        }
      }

      public Transform m_groupHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_groupHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_groupHeadFrameTransform = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public Button m_watchStaffButton
      {
        get
        {
          return this.m_owner.m_watchStaffButton;
        }
        set
        {
          this.m_owner.m_watchStaffButton = value;
        }
      }

      public Text m_onlinePlayerNumText
      {
        get
        {
          return this.m_owner.m_onlinePlayerNumText;
        }
        set
        {
          this.m_owner.m_onlinePlayerNumText = value;
        }
      }

      public Text m_groupNameText
      {
        get
        {
          return this.m_owner.m_groupNameText;
        }
        set
        {
          this.m_owner.m_groupNameText = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProChatGroupCompactInfo m_groupInfo
      {
        get
        {
          return this.m_owner.m_groupInfo;
        }
        set
        {
          this.m_owner.m_groupInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnWatchStaffButtonClick()
      {
        this.m_owner.OnWatchStaffButtonClick();
      }
    }
  }
}
