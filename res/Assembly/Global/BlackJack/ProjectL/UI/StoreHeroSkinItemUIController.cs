﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreHeroSkinItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class StoreHeroSkinItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_storeItemButton;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_heroSkinIcon;
    [AutoBind("./HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_heroName;
    [AutoBind("./OtherNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_skinName;
    [AutoBind("./PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_priceText;
    [AutoBind("./Tape/TSurplusText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_restTime;
    [AutoBind("./TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_timeGo;
    [AutoBind("./TimeLimit/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_timeValueText;
    [AutoBind("./TimeLimit/TimeTitleText", AutoBindAttribute.InitState.Inactive, false)]
    public Text m_timeTitleText;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_itemStateCtrl;
    public ConfigDataFixedStoreItemInfo m_storeItemConfig;
    public StoreType m_storeType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreHeroSkinItemUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroSkinItemInfo(FixedStoreItem fixedStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemState()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreItemClick()
    {
    }

    public event Action<StoreHeroSkinItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnSkinItemOutOfDate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
