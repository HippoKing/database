﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleDanmakuUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleDanmakuUIController : UIControllerBase
  {
    [AutoBind("./Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_danmakuContent;
    [AutoBind("./Prefab/CommonText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_danmakuCommonText;
    [AutoBind("./Prefab/MyText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_danmakuMyText;
    private List<int> m_danmakuYPosList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleDanmakuUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCurTurnDanmaku(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowDanmakus(List<DanmakuEntry> danmakus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOneDanmaku(DanmakuEntry danmaku)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetRandomYPOsition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsYPositonAvailable(int y, int listCount)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
