﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RecommendGiftBoxToggleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RecommendGiftBoxToggleUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_giftBoxToggle;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./New", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMark;
    [AutoBind("./BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buyTimes;
    [AutoBind("./BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buyTimesAnimation;
    [AutoBind("./BuyTimes/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyTimesText;
    [AutoBind("./RemainingDays", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_remainingDaysGameObject;
    [AutoBind("./RemainingDays/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_remainingDaysText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public GiftStoreItem m_giftStoreItem;
    private DateTime m_endTime;
    [DoNotToLua]
    private RecommendGiftBoxToggleUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetToggleOn_hotfix;
    private LuaFunction m_UpdateData_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateViewRedMark_hotfix;
    private LuaFunction m_InitGiftStoreItem_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_UpdateRemoveTime_hotfix;
    private LuaFunction m_OnGiftBoxClick_hotfix;
    private LuaFunction m_IsBoughtLimit_hotfix;
    private LuaFunction m_add_EventOnGiftBoxClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGiftBoxClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GiftStoreItem giftStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemoveTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftBoxClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBoughtLimit()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RecommendGiftBoxToggleUIController> EventOnGiftBoxClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RecommendGiftBoxToggleUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGiftBoxClick(RecommendGiftBoxToggleUIController obj)
    {
    }

    private void __clearDele_EventOnGiftBoxClick(RecommendGiftBoxToggleUIController obj)
    {
      this.EventOnGiftBoxClick = (Action<RecommendGiftBoxToggleUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RecommendGiftBoxToggleUIController m_owner;

      public LuaExportHelper(RecommendGiftBoxToggleUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnGiftBoxClick(RecommendGiftBoxToggleUIController obj)
      {
        this.m_owner.__callDele_EventOnGiftBoxClick(obj);
      }

      public void __clearDele_EventOnGiftBoxClick(RecommendGiftBoxToggleUIController obj)
      {
        this.m_owner.__clearDele_EventOnGiftBoxClick(obj);
      }

      public Toggle m_giftBoxToggle
      {
        get
        {
          return this.m_owner.m_giftBoxToggle;
        }
        set
        {
          this.m_owner.m_giftBoxToggle = value;
        }
      }

      public Text m_titleText
      {
        get
        {
          return this.m_owner.m_titleText;
        }
        set
        {
          this.m_owner.m_titleText = value;
        }
      }

      public GameObject m_redMark
      {
        get
        {
          return this.m_owner.m_redMark;
        }
        set
        {
          this.m_owner.m_redMark = value;
        }
      }

      public GameObject m_buyTimes
      {
        get
        {
          return this.m_owner.m_buyTimes;
        }
        set
        {
          this.m_owner.m_buyTimes = value;
        }
      }

      public CommonUIStateController m_buyTimesAnimation
      {
        get
        {
          return this.m_owner.m_buyTimesAnimation;
        }
        set
        {
          this.m_owner.m_buyTimesAnimation = value;
        }
      }

      public Text m_buyTimesText
      {
        get
        {
          return this.m_owner.m_buyTimesText;
        }
        set
        {
          this.m_owner.m_buyTimesText = value;
        }
      }

      public GameObject m_remainingDaysGameObject
      {
        get
        {
          return this.m_owner.m_remainingDaysGameObject;
        }
        set
        {
          this.m_owner.m_remainingDaysGameObject = value;
        }
      }

      public Text m_remainingDaysText
      {
        get
        {
          return this.m_owner.m_remainingDaysText;
        }
        set
        {
          this.m_owner.m_remainingDaysText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DateTime m_endTime
      {
        get
        {
          return this.m_owner.m_endTime;
        }
        set
        {
          this.m_owner.m_endTime = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void UpdateRemoveTime()
      {
        this.m_owner.UpdateRemoveTime();
      }

      public void OnGiftBoxClick()
      {
        this.m_owner.OnGiftBoxClick();
      }
    }
  }
}
