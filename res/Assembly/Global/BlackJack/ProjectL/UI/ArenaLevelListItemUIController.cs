﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ArenaLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_arenaLevelUIStateCtrl;
    [AutoBind("./Low", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_lowUIStateCtrl;
    [AutoBind("./Low/ArenalLevel1_3", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_lowLevelUIStateCtrl;
    [AutoBind("./Low/ArenalLevel1_3/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_lowLevelIconImage;
    [AutoBind("./Low/ArenalLevel1_3/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lowLevelText;
    [AutoBind("./Low/ArenalLevel1_3/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lowRewardGroupGameObject;
    [AutoBind("./Low/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lowArenaPointText;
    [AutoBind("./Middle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_middleUIStateCtrl;
    [AutoBind("./Middle/ArenalLevel4_6", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_middleLevelUIStateCtrl;
    [AutoBind("./Middle/ArenalLevel4_6/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_middleLevelIconImage;
    [AutoBind("./Middle/ArenalLevel4_6/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_middleLevelText;
    [AutoBind("./Middle/ArenalLevel4_6/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_middleRewardGroupGameObject;
    [AutoBind("./Middle/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_middleArenaPointText;
    [AutoBind("./High", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_highUIStateCtrl;
    [AutoBind("./High/ArenalLevel7_9", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_highLevelUIStateCtrl;
    [AutoBind("./High/ArenalLevel7_9/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_highLevelIconImage;
    [AutoBind("./High/ArenalLevel7_9/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_highLevelText;
    [AutoBind("./High/ArenalLevel7_9/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_highRewardGroupGameObject;
    [AutoBind("./High/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_highArenaPointText;
    [AutoBind("./Top", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_topUIStateCtrl;
    [AutoBind("./Top/ArenalLevel10/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_topLevelIconImage;
    [AutoBind("./Top/ArenalLevel10/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_topLevelText;
    [AutoBind("./Top/ArenalLevel10/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topRewardGroupGameObject;
    [AutoBind("./Top/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_topArenaPointText;
    [DoNotToLua]
    private ArenaLevelListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetArenaLevelInfoConfigDataArenaLevelInfoGameObjectInt32_hotfix;
    private LuaFunction m_SetDanInfoConfigDataRealTimePVPDanInfoGameObjectInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaLevelInfo(
      ConfigDataArenaLevelInfo arenaLevelInfo,
      GameObject rewardGoodsPrefab,
      int playerArenaLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDanInfo(
      ConfigDataRealTimePVPDanInfo danInfo,
      GameObject rewardGoodsPrefab,
      int playerDanId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ArenaLevelListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ArenaLevelListItemUIController m_owner;

      public LuaExportHelper(ArenaLevelListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_arenaLevelUIStateCtrl
      {
        get
        {
          return this.m_owner.m_arenaLevelUIStateCtrl;
        }
        set
        {
          this.m_owner.m_arenaLevelUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_lowUIStateCtrl
      {
        get
        {
          return this.m_owner.m_lowUIStateCtrl;
        }
        set
        {
          this.m_owner.m_lowUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_lowLevelUIStateCtrl
      {
        get
        {
          return this.m_owner.m_lowLevelUIStateCtrl;
        }
        set
        {
          this.m_owner.m_lowLevelUIStateCtrl = value;
        }
      }

      public Image m_lowLevelIconImage
      {
        get
        {
          return this.m_owner.m_lowLevelIconImage;
        }
        set
        {
          this.m_owner.m_lowLevelIconImage = value;
        }
      }

      public Text m_lowLevelText
      {
        get
        {
          return this.m_owner.m_lowLevelText;
        }
        set
        {
          this.m_owner.m_lowLevelText = value;
        }
      }

      public GameObject m_lowRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_lowRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_lowRewardGroupGameObject = value;
        }
      }

      public Text m_lowArenaPointText
      {
        get
        {
          return this.m_owner.m_lowArenaPointText;
        }
        set
        {
          this.m_owner.m_lowArenaPointText = value;
        }
      }

      public CommonUIStateController m_middleUIStateCtrl
      {
        get
        {
          return this.m_owner.m_middleUIStateCtrl;
        }
        set
        {
          this.m_owner.m_middleUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_middleLevelUIStateCtrl
      {
        get
        {
          return this.m_owner.m_middleLevelUIStateCtrl;
        }
        set
        {
          this.m_owner.m_middleLevelUIStateCtrl = value;
        }
      }

      public Image m_middleLevelIconImage
      {
        get
        {
          return this.m_owner.m_middleLevelIconImage;
        }
        set
        {
          this.m_owner.m_middleLevelIconImage = value;
        }
      }

      public Text m_middleLevelText
      {
        get
        {
          return this.m_owner.m_middleLevelText;
        }
        set
        {
          this.m_owner.m_middleLevelText = value;
        }
      }

      public GameObject m_middleRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_middleRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_middleRewardGroupGameObject = value;
        }
      }

      public Text m_middleArenaPointText
      {
        get
        {
          return this.m_owner.m_middleArenaPointText;
        }
        set
        {
          this.m_owner.m_middleArenaPointText = value;
        }
      }

      public CommonUIStateController m_highUIStateCtrl
      {
        get
        {
          return this.m_owner.m_highUIStateCtrl;
        }
        set
        {
          this.m_owner.m_highUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_highLevelUIStateCtrl
      {
        get
        {
          return this.m_owner.m_highLevelUIStateCtrl;
        }
        set
        {
          this.m_owner.m_highLevelUIStateCtrl = value;
        }
      }

      public Image m_highLevelIconImage
      {
        get
        {
          return this.m_owner.m_highLevelIconImage;
        }
        set
        {
          this.m_owner.m_highLevelIconImage = value;
        }
      }

      public Text m_highLevelText
      {
        get
        {
          return this.m_owner.m_highLevelText;
        }
        set
        {
          this.m_owner.m_highLevelText = value;
        }
      }

      public GameObject m_highRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_highRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_highRewardGroupGameObject = value;
        }
      }

      public Text m_highArenaPointText
      {
        get
        {
          return this.m_owner.m_highArenaPointText;
        }
        set
        {
          this.m_owner.m_highArenaPointText = value;
        }
      }

      public CommonUIStateController m_topUIStateCtrl
      {
        get
        {
          return this.m_owner.m_topUIStateCtrl;
        }
        set
        {
          this.m_owner.m_topUIStateCtrl = value;
        }
      }

      public Image m_topLevelIconImage
      {
        get
        {
          return this.m_owner.m_topLevelIconImage;
        }
        set
        {
          this.m_owner.m_topLevelIconImage = value;
        }
      }

      public Text m_topLevelText
      {
        get
        {
          return this.m_owner.m_topLevelText;
        }
        set
        {
          this.m_owner.m_topLevelText = value;
        }
      }

      public GameObject m_topRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_topRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_topRewardGroupGameObject = value;
        }
      }

      public Text m_topArenaPointText
      {
        get
        {
          return this.m_owner.m_topArenaPointText;
        }
        set
        {
          this.m_owner.m_topArenaPointText = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }
    }
  }
}
