﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInvitationRefusedNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TeamRoomInvitationRefusedNetTask : UINetTask
  {
    private ulong m_inviterSessionId;
    private int m_inviterChannelId;
    private int m_roomId;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInvitationRefusedNetTask(
      ulong inviterSessionId,
      int inviterChannelId,
      int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnTeamRoomInvitationRefusedAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
