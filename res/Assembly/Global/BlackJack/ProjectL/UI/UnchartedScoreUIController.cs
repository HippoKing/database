﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedScoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UnchartedScoreUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamUIStateController;
    [AutoBind("./PlayerResource/DailyReward/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyRewardCountText;
    [AutoBind("./ActivitiesName/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./ActivityInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activityUIStateController;
    [AutoBind("./Background/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_backgroundImage;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGo;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./LevelList/ToggleGroup/ScoreToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_scoreLevelToggle;
    [AutoBind("./LevelList/ToggleGroup/ChallengeToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_challengeLevelToggle;
    [AutoBind("./RewardGroup/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardButton;
    [AutoBind("./RewardGroup/RecommendHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendHeroButton;
    [AutoBind("./RewardGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./RewardPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_scoreRewardUIStateController;
    [AutoBind("./RewardPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreRewardBGButton;
    [AutoBind("./RewardPanel/Detail/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scoreRewardScollRect;
    [AutoBind("./RewardPanel/Detail/CountScore/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreRewardScoreNameText;
    [AutoBind("./RewardPanel/Detail/CountScore/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreRewardScoreText;
    [AutoBind("./RecommendHeroPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_recommendHeroUIStateController;
    [AutoBind("./RecommendHeroPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendHeroBGButton;
    [AutoBind("./RecommendHeroPanel/Detail/HeroGroupScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_recommendHeroGroupScrollRect;
    [AutoBind("./RecommendHeroPanel/Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_recommendHeroDescText;
    [AutoBind("./RecommendHeroPanel/Detail/NoHeroText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_recommendHeroNoHeroText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/UnchartedScoreLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unchartedScoreLevelListItemPrefab;
    [AutoBind("./Prefabs/RewardItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scoreRewardItemPrefab;
    [AutoBind("./Prefabs/RecommendHeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_recommendHeroItemPrefab;
    private bool m_isIgnoreToggleEvent;
    private ConfigDataUnchartedScoreInfo m_unchartedScoreInfo;
    private UISpineGraphic m_graphic;
    [DoNotToLua]
    private UnchartedScoreUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_SetBattleTypeBattleTypeBoolean_hotfix;
    private LuaFunction m_SetScoreInt32_hotfix;
    private LuaFunction m_SetUnchartedScoreInfoConfigDataUnchartedScoreInfoConfigDataUnchartedScoreModelInfo_hotfix;
    private LuaFunction m_SetAllUnchartedScoreLevelListItemsIEnumerable`1_hotfix;
    private LuaFunction m_SetAllUnchartedChallengeLevelListItemsIEnumerable`1_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataUnchartedScoreModelInfo_hotfix;
    private LuaFunction m_DestroySpineGraphic_hotfix;
    private LuaFunction m_SetDailyRewardCountInt32Int32_hotfix;
    private LuaFunction m_ShowScoreReward_hotfix;
    private LuaFunction m_Co_ScrollToItemScrollRectInt32Int32_hotfix;
    private LuaFunction m_HideScoreReward_hotfix;
    private LuaFunction m_ShowRecommendHero_hotfix;
    private LuaFunction m_HideRecommendHero_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnTeamButtonClick_hotfix;
    private LuaFunction m_OnRewardButtonClick_hotfix;
    private LuaFunction m_OnRecommendHeroButtonClick_hotfix;
    private LuaFunction m_OnScoreLevelToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnChallengeLevelToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnScoreRewardCloseButtonClick_hotfix;
    private LuaFunction m_OnRecommendHeroBGButtonClick_hotfix;
    private LuaFunction m_UnchartedScoreLevelListItem_OnStartButtonClickUnchartedScoreLevelListItemUIController_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnShowTeamAction_hotfix;
    private LuaFunction m_remove_EventOnShowTeamAction_hotfix;
    private LuaFunction m_add_EventOnChangeBattleTypeAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangeBattleTypeAction`1_hotfix;
    private LuaFunction m_add_EventOnStartUnchartedScoreLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartUnchartedScoreLevelAction`1_hotfix;
    private LuaFunction m_add_EventOnStartUnchartedChallengeLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartUnchartedChallengeLevelAction`1_hotfix;

    private UnchartedScoreUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleType(BattleType battleType, bool isOpening)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScore(int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnchartedScoreInfo(
      ConfigDataUnchartedScoreInfo unchartedScoreInfo,
      ConfigDataUnchartedScoreModelInfo unchartedScoreModelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllUnchartedScoreLevelListItems(IEnumerable<ConfigDataScoreLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllUnchartedChallengeLevelListItems(
      IEnumerable<ConfigDataChallengeLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataUnchartedScoreModelInfo unchartedScoreModelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyRewardCount(int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowScoreReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideScoreReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRecommendHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideRecommendHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreLevelToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChallengeLevelToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendHeroBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedScoreLevelListItem_OnStartButtonClick(
      UnchartedScoreLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType> EventOnChangeBattleType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataScoreLevelInfo> EventOnStartUnchartedScoreLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataChallengeLevelInfo> EventOnStartUnchartedChallengeLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public UnchartedScoreUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowTeam()
    {
      this.EventOnShowTeam = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeBattleType(BattleType obj)
    {
    }

    private void __clearDele_EventOnChangeBattleType(BattleType obj)
    {
      this.EventOnChangeBattleType = (Action<BattleType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartUnchartedScoreLevel(ConfigDataScoreLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartUnchartedScoreLevel(ConfigDataScoreLevelInfo obj)
    {
      this.EventOnStartUnchartedScoreLevel = (Action<ConfigDataScoreLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartUnchartedChallengeLevel(ConfigDataChallengeLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartUnchartedChallengeLevel(ConfigDataChallengeLevelInfo obj)
    {
      this.EventOnStartUnchartedChallengeLevel = (Action<ConfigDataChallengeLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UnchartedScoreUIController m_owner;

      public LuaExportHelper(UnchartedScoreUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnShowTeam()
      {
        this.m_owner.__callDele_EventOnShowTeam();
      }

      public void __clearDele_EventOnShowTeam()
      {
        this.m_owner.__clearDele_EventOnShowTeam();
      }

      public void __callDele_EventOnChangeBattleType(BattleType obj)
      {
        this.m_owner.__callDele_EventOnChangeBattleType(obj);
      }

      public void __clearDele_EventOnChangeBattleType(BattleType obj)
      {
        this.m_owner.__clearDele_EventOnChangeBattleType(obj);
      }

      public void __callDele_EventOnStartUnchartedScoreLevel(ConfigDataScoreLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartUnchartedScoreLevel(obj);
      }

      public void __clearDele_EventOnStartUnchartedScoreLevel(ConfigDataScoreLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartUnchartedScoreLevel(obj);
      }

      public void __callDele_EventOnStartUnchartedChallengeLevel(ConfigDataChallengeLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartUnchartedChallengeLevel(obj);
      }

      public void __clearDele_EventOnStartUnchartedChallengeLevel(ConfigDataChallengeLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartUnchartedChallengeLevel(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_teamButton
      {
        get
        {
          return this.m_owner.m_teamButton;
        }
        set
        {
          this.m_owner.m_teamButton = value;
        }
      }

      public CommonUIStateController m_teamUIStateController
      {
        get
        {
          return this.m_owner.m_teamUIStateController;
        }
        set
        {
          this.m_owner.m_teamUIStateController = value;
        }
      }

      public Text m_dailyRewardCountText
      {
        get
        {
          return this.m_owner.m_dailyRewardCountText;
        }
        set
        {
          this.m_owner.m_dailyRewardCountText = value;
        }
      }

      public Text m_activityNameText
      {
        get
        {
          return this.m_owner.m_activityNameText;
        }
        set
        {
          this.m_owner.m_activityNameText = value;
        }
      }

      public CommonUIStateController m_activityUIStateController
      {
        get
        {
          return this.m_owner.m_activityUIStateController;
        }
        set
        {
          this.m_owner.m_activityUIStateController = value;
        }
      }

      public Image m_backgroundImage
      {
        get
        {
          return this.m_owner.m_backgroundImage;
        }
        set
        {
          this.m_owner.m_backgroundImage = value;
        }
      }

      public GameObject m_charGo
      {
        get
        {
          return this.m_owner.m_charGo;
        }
        set
        {
          this.m_owner.m_charGo = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public Toggle m_scoreLevelToggle
      {
        get
        {
          return this.m_owner.m_scoreLevelToggle;
        }
        set
        {
          this.m_owner.m_scoreLevelToggle = value;
        }
      }

      public Toggle m_challengeLevelToggle
      {
        get
        {
          return this.m_owner.m_challengeLevelToggle;
        }
        set
        {
          this.m_owner.m_challengeLevelToggle = value;
        }
      }

      public Button m_rewardButton
      {
        get
        {
          return this.m_owner.m_rewardButton;
        }
        set
        {
          this.m_owner.m_rewardButton = value;
        }
      }

      public Button m_recommendHeroButton
      {
        get
        {
          return this.m_owner.m_recommendHeroButton;
        }
        set
        {
          this.m_owner.m_recommendHeroButton = value;
        }
      }

      public Text m_scoreText
      {
        get
        {
          return this.m_owner.m_scoreText;
        }
        set
        {
          this.m_owner.m_scoreText = value;
        }
      }

      public CommonUIStateController m_scoreRewardUIStateController
      {
        get
        {
          return this.m_owner.m_scoreRewardUIStateController;
        }
        set
        {
          this.m_owner.m_scoreRewardUIStateController = value;
        }
      }

      public Button m_scoreRewardBGButton
      {
        get
        {
          return this.m_owner.m_scoreRewardBGButton;
        }
        set
        {
          this.m_owner.m_scoreRewardBGButton = value;
        }
      }

      public ScrollRect m_scoreRewardScollRect
      {
        get
        {
          return this.m_owner.m_scoreRewardScollRect;
        }
        set
        {
          this.m_owner.m_scoreRewardScollRect = value;
        }
      }

      public Text m_scoreRewardScoreNameText
      {
        get
        {
          return this.m_owner.m_scoreRewardScoreNameText;
        }
        set
        {
          this.m_owner.m_scoreRewardScoreNameText = value;
        }
      }

      public Text m_scoreRewardScoreText
      {
        get
        {
          return this.m_owner.m_scoreRewardScoreText;
        }
        set
        {
          this.m_owner.m_scoreRewardScoreText = value;
        }
      }

      public CommonUIStateController m_recommendHeroUIStateController
      {
        get
        {
          return this.m_owner.m_recommendHeroUIStateController;
        }
        set
        {
          this.m_owner.m_recommendHeroUIStateController = value;
        }
      }

      public Button m_recommendHeroBGButton
      {
        get
        {
          return this.m_owner.m_recommendHeroBGButton;
        }
        set
        {
          this.m_owner.m_recommendHeroBGButton = value;
        }
      }

      public ScrollRect m_recommendHeroGroupScrollRect
      {
        get
        {
          return this.m_owner.m_recommendHeroGroupScrollRect;
        }
        set
        {
          this.m_owner.m_recommendHeroGroupScrollRect = value;
        }
      }

      public Text m_recommendHeroDescText
      {
        get
        {
          return this.m_owner.m_recommendHeroDescText;
        }
        set
        {
          this.m_owner.m_recommendHeroDescText = value;
        }
      }

      public Text m_recommendHeroNoHeroText
      {
        get
        {
          return this.m_owner.m_recommendHeroNoHeroText;
        }
        set
        {
          this.m_owner.m_recommendHeroNoHeroText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_unchartedScoreLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_unchartedScoreLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_unchartedScoreLevelListItemPrefab = value;
        }
      }

      public GameObject m_scoreRewardItemPrefab
      {
        get
        {
          return this.m_owner.m_scoreRewardItemPrefab;
        }
        set
        {
          this.m_owner.m_scoreRewardItemPrefab = value;
        }
      }

      public GameObject m_recommendHeroItemPrefab
      {
        get
        {
          return this.m_owner.m_recommendHeroItemPrefab;
        }
        set
        {
          this.m_owner.m_recommendHeroItemPrefab = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public ConfigDataUnchartedScoreInfo m_unchartedScoreInfo
      {
        get
        {
          return this.m_owner.m_unchartedScoreInfo;
        }
        set
        {
          this.m_owner.m_unchartedScoreInfo = value;
        }
      }

      public UISpineGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void CreateSpineGraphic(
        ConfigDataUnchartedScoreModelInfo unchartedScoreModelInfo)
      {
        this.m_owner.CreateSpineGraphic(unchartedScoreModelInfo);
      }

      public void DestroySpineGraphic()
      {
        this.m_owner.DestroySpineGraphic();
      }

      public IEnumerator Co_ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
      {
        return this.m_owner.Co_ScrollToItem(scrollRect, itemCount, idx);
      }

      public static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
      {
        UnchartedScoreUIController.ScrollToItem(scrollRect, itemCount, idx);
      }

      public void HideScoreReward()
      {
        this.m_owner.HideScoreReward();
      }

      public void HideRecommendHero()
      {
        this.m_owner.HideRecommendHero();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnTeamButtonClick()
      {
        this.m_owner.OnTeamButtonClick();
      }

      public void OnRewardButtonClick()
      {
        this.m_owner.OnRewardButtonClick();
      }

      public void OnRecommendHeroButtonClick()
      {
        this.m_owner.OnRecommendHeroButtonClick();
      }

      public void OnScoreLevelToggleValueChanged(bool on)
      {
        this.m_owner.OnScoreLevelToggleValueChanged(on);
      }

      public void OnChallengeLevelToggleValueChanged(bool on)
      {
        this.m_owner.OnChallengeLevelToggleValueChanged(on);
      }

      public void OnScoreRewardCloseButtonClick()
      {
        this.m_owner.OnScoreRewardCloseButtonClick();
      }

      public void OnRecommendHeroBGButtonClick()
      {
        this.m_owner.OnRecommendHeroBGButtonClick();
      }

      public void UnchartedScoreLevelListItem_OnStartButtonClick(
        UnchartedScoreLevelListItemUIController ctrl)
      {
        this.m_owner.UnchartedScoreLevelListItem_OnStartButtonClick(ctrl);
      }
    }
  }
}
