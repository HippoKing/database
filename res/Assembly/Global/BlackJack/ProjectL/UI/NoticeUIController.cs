﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class NoticeUIController : UIControllerBase
  {
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_noticeButton;
    [AutoBind("./Button/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeDetailGo;
    [AutoBind("./Button/Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_noticeText;
    [AutoBind("./Button/Detail/VoiceImage", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_noticeVoiceImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private static bool s_isForceHide;
    private static NoticeUIController s_instance;
    private const float NoticeTweenSpeed = 100f;
    private const float ShowNoticeInterval = 1f;
    private const float ShowNoticeTime = 5f;
    private Vector3 m_noticeTextInitPos;
    private float m_noticeTextInitWidth;
    private Coroutine m_delayHideNoticeCoroutine;
    private TweenPos m_noticeShowTween;
    private TweenPos m_noticeMoveTween;
    private NoticeText m_currentNoticeText;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private NoticeUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_IsNoticeAvailableChatChannel_hotfix;
    private LuaFunction m_ShowNoticeStringStringChatChannelBoolean_hotfix;
    private LuaFunction m_SetPositionStateString_hotfix;
    private LuaFunction m_ShowNextNotice_hotfix;
    private LuaFunction m_OnNoticeShowTweenFinished_hotfix;
    private LuaFunction m_OnNoticeMoveTweenFinished_hotfix;
    private LuaFunction m_DelayHideNoticeSingleSingle_hotfix;
    private LuaFunction m_OnNoticeButtonClick_hotfix;
    private LuaFunction m_add_EventOnNoticeClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnNoticeClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNoticeAvailable(ChatChannel chatChannel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNotice(string txt, string stateName, ChatChannel chatChannel, bool isVoiceMsg = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPositionState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeShowTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeMoveTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayHideNotice(float time, float additiveTime = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsForceHide
    {
      get
      {
        return NoticeUIController.s_isForceHide;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<NoticeText> EventOnNoticeClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public NoticeUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      base.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnNoticeClick(NoticeText obj)
    {
    }

    private void __clearDele_EventOnNoticeClick(NoticeText obj)
    {
      this.EventOnNoticeClick = (Action<NoticeText>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private NoticeUIController m_owner;

      public LuaExportHelper(NoticeUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnNoticeClick(NoticeText obj)
      {
        this.m_owner.__callDele_EventOnNoticeClick(obj);
      }

      public void __clearDele_EventOnNoticeClick(NoticeText obj)
      {
        this.m_owner.__clearDele_EventOnNoticeClick(obj);
      }

      public Button m_noticeButton
      {
        get
        {
          return this.m_owner.m_noticeButton;
        }
        set
        {
          this.m_owner.m_noticeButton = value;
        }
      }

      public GameObject m_noticeDetailGo
      {
        get
        {
          return this.m_owner.m_noticeDetailGo;
        }
        set
        {
          this.m_owner.m_noticeDetailGo = value;
        }
      }

      public EmojiText m_noticeText
      {
        get
        {
          return this.m_owner.m_noticeText;
        }
        set
        {
          this.m_owner.m_noticeText = value;
        }
      }

      public GameObject m_noticeVoiceImage
      {
        get
        {
          return this.m_owner.m_noticeVoiceImage;
        }
        set
        {
          this.m_owner.m_noticeVoiceImage = value;
        }
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public SmallExpressionParseDesc m_expressionParseDesc
      {
        get
        {
          return this.m_owner.m_expressionParseDesc;
        }
        set
        {
          this.m_owner.m_expressionParseDesc = value;
        }
      }

      public PrefabResourceContainer m_expressionResContainer
      {
        get
        {
          return this.m_owner.m_expressionResContainer;
        }
        set
        {
          this.m_owner.m_expressionResContainer = value;
        }
      }

      public static bool s_isForceHide
      {
        get
        {
          return NoticeUIController.s_isForceHide;
        }
        set
        {
          NoticeUIController.s_isForceHide = value;
        }
      }

      public static NoticeUIController s_instance
      {
        get
        {
          return NoticeUIController.s_instance;
        }
        set
        {
          NoticeUIController.s_instance = value;
        }
      }

      public static float NoticeTweenSpeed
      {
        get
        {
          return 100f;
        }
      }

      public static float ShowNoticeInterval
      {
        get
        {
          return 1f;
        }
      }

      public static float ShowNoticeTime
      {
        get
        {
          return 5f;
        }
      }

      public Vector3 m_noticeTextInitPos
      {
        get
        {
          return this.m_owner.m_noticeTextInitPos;
        }
        set
        {
          this.m_owner.m_noticeTextInitPos = value;
        }
      }

      public float m_noticeTextInitWidth
      {
        get
        {
          return this.m_owner.m_noticeTextInitWidth;
        }
        set
        {
          this.m_owner.m_noticeTextInitWidth = value;
        }
      }

      public Coroutine m_delayHideNoticeCoroutine
      {
        get
        {
          return this.m_owner.m_delayHideNoticeCoroutine;
        }
        set
        {
          this.m_owner.m_delayHideNoticeCoroutine = value;
        }
      }

      public TweenPos m_noticeShowTween
      {
        get
        {
          return this.m_owner.m_noticeShowTween;
        }
        set
        {
          this.m_owner.m_noticeShowTween = value;
        }
      }

      public TweenPos m_noticeMoveTween
      {
        get
        {
          return this.m_owner.m_noticeMoveTween;
        }
        set
        {
          this.m_owner.m_noticeMoveTween = value;
        }
      }

      public NoticeText m_currentNoticeText
      {
        get
        {
          return this.m_owner.m_currentNoticeText;
        }
        set
        {
          this.m_owner.m_currentNoticeText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public bool IsNoticeAvailable(ChatChannel chatChannel)
      {
        return this.m_owner.IsNoticeAvailable(chatChannel);
      }

      public void ShowNextNotice()
      {
        this.m_owner.ShowNextNotice();
      }

      public void OnNoticeShowTweenFinished()
      {
        this.m_owner.OnNoticeShowTweenFinished();
      }

      public void OnNoticeMoveTweenFinished()
      {
        this.m_owner.OnNoticeMoveTweenFinished();
      }

      public IEnumerator DelayHideNotice(float time, float additiveTime)
      {
        return this.m_owner.DelayHideNotice(time, additiveTime);
      }

      public void OnNoticeButtonClick()
      {
        this.m_owner.OnNoticeButtonClick();
      }
    }
  }
}
