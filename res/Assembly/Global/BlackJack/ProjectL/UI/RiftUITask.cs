﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private RiftBackgroundUIController m_riftBackgroundUIController;
    private RiftChapterUIController m_riftChapterUIController;
    private RiftLevelUIController m_riftLevelUIController;
    private PlayerResourceUIController m_riftChapterPlayerResourceUIController;
    private PlayerResourceUIController m_riftLevelPlayerResourceUIController;
    private ConfigDataRiftChapterInfo m_chapterInfo;
    private bool m_needReturnToChapter;
    private bool m_needPlayRiftLevelOpenAnimation;
    private BattleLevelInfoUITask m_battleLevelInfoUITask;
    private ChestUITask m_chestUITask;
    private RiftUITask.ViewState m_viewState;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftBackgroundUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitRiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChapters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowChapterProgress(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    private void HideChapterProgress(ConfigDataRiftChapterInfo chapterInfo)
    {
      this.m_riftChapterUIController.HideChapterProgress();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectChapter(ConfigDataRiftChapterInfo chapterInfo, bool playOpenAnim)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void RiftChapterUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Rift);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void RiftChapterUIController_OnSelectChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      this.SelectChapter(chapterInfo, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnSwitchChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnGoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitRiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRiftLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStarReward(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetStarReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenRiftChapterHard(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleLevelInfoUITask(
      ConfigDataRiftLevelInfo riftLevelInfo,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void RiftLevelUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Rift);
    }

    private void RiftLevelUIController_OnReturnToWorld()
    {
      this.RiftChapterUIController_OnReturnToWorld();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnChangeHard()
    {
      // ISSUE: unable to decompile the method.
    }

    private void RiftLevelUIController_OnSelectRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo,
      NeedGoods needGoods = null)
    {
      this.StartBattleLevelInfoUITask(riftLevelInfo, needGoods);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnGetStarReward(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnGoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnStop(Task task)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUITask_OnClose()
    {
    }

    private void BattleLevelInfoUITask_OnRiftRaidComplete(int levelId)
    {
      this.m_riftLevelPlayerResourceUIController.UpdatePlayerResource();
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public enum ViewState
    {
      None,
      Chapter,
      RiftLevel,
    }
  }
}
