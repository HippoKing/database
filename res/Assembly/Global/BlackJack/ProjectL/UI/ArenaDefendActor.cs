﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaDefendActor
  {
    private GridPosition m_position;
    private int m_direction;
    private int m_soldierCount;
    private BattleHero m_hero;
    private ArenaDefendBattle m_battle;
    private Colori m_tweenFromColor;
    private Colori m_tweenToColor;
    private float m_tweenColorTime;
    private GenericGraphic[] m_graphics;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendActor(ArenaDefendBattle uiTask)
    {
    }

    public void Destroy()
    {
      this.DestroyGraphics();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphics()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGraphics()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool UpdateGraphic(
      int idx,
      string assetName,
      float scale,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleHero GetHero()
    {
      return this.m_hero;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(GridPosition p, int dir)
    {
    }

    public GridPosition GetPosition()
    {
      return this.m_position;
    }

    public int GetDirection()
    {
      return this.m_direction;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TweenColor(Colori c)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GetGraphicOffset(int idx, int dir)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
