﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AnikiUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AnikiUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./SecretBlessing/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blessingButton;
    [AutoBind("./PlayerResource/DailyReward/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyRewardCountText;
    [AutoBind("./GymList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gymListGameObject;
    [AutoBind("./LevelList/NameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gymNameText;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/AnikiGymListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_anikiGymListItemPrefab;
    [AutoBind("./Prefabs/AnikiLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_anikiLevelListItemPrefab;
    private List<AnikiGymListItemUIController> m_anikiGymListItems;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private AnikiUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_AddAnikiGymListItemConfigDataAnikiGymInfoBoolean_hotfix;
    private LuaFunction m_ClearAnikiGymListItems_hotfix;
    private LuaFunction m_SetAllAnikiLevelListItemList`1_hotfix;
    private LuaFunction m_SetSelectedAnikiGymConfigDataAnikiGymInfo_hotfix;
    private LuaFunction m_SetDailyRewardCountInt32Int32_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnAddTicketButtonClick_hotfix;
    private LuaFunction m_OnTeamButtonClick_hotfix;
    private LuaFunction m_OnBlessingButtonClick_hotfix;
    private LuaFunction m_AnikiGymListItem_OnToggleValueChangedBooleanAnikiGymListItemUIController_hotfix;
    private LuaFunction m_AnikiLevelListItem_OnStartButtonClickAnikiLevelListItemUIController_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnAddTicketAction_hotfix;
    private LuaFunction m_remove_EventOnAddTicketAction_hotfix;
    private LuaFunction m_add_EventOnShowTeamAction_hotfix;
    private LuaFunction m_remove_EventOnShowTeamAction_hotfix;
    private LuaFunction m_add_EventOnSelectAnikiGymAction`1_hotfix;
    private LuaFunction m_remove_EventOnSelectAnikiGymAction`1_hotfix;
    private LuaFunction m_add_EventOnStartAnikiLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartAnikiLevelAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private AnikiUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnikiGymListItem(ConfigDataAnikiGymInfo anikiGymInfo, bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAnikiGymListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllAnikiLevelListItem(List<ConfigDataAnikiLevelInfo> levelInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectedAnikiGym(ConfigDataAnikiGymInfo anikiGymInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyRewardCount(int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlessingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiGymListItem_OnToggleValueChanged(bool isOn, AnikiGymListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiLevelListItem_OnStartButtonClick(AnikiLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataAnikiGymInfo> EventOnSelectAnikiGym
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataAnikiLevelInfo> EventOnStartAnikiLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public AnikiUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddTicket()
    {
      this.EventOnAddTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowTeam()
    {
      this.EventOnShowTeam = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectAnikiGym(ConfigDataAnikiGymInfo obj)
    {
    }

    private void __clearDele_EventOnSelectAnikiGym(ConfigDataAnikiGymInfo obj)
    {
      this.EventOnSelectAnikiGym = (Action<ConfigDataAnikiGymInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartAnikiLevel(ConfigDataAnikiLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartAnikiLevel(ConfigDataAnikiLevelInfo obj)
    {
      this.EventOnStartAnikiLevel = (Action<ConfigDataAnikiLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AnikiUIController m_owner;

      public LuaExportHelper(AnikiUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnAddTicket()
      {
        this.m_owner.__callDele_EventOnAddTicket();
      }

      public void __clearDele_EventOnAddTicket()
      {
        this.m_owner.__clearDele_EventOnAddTicket();
      }

      public void __callDele_EventOnShowTeam()
      {
        this.m_owner.__callDele_EventOnShowTeam();
      }

      public void __clearDele_EventOnShowTeam()
      {
        this.m_owner.__clearDele_EventOnShowTeam();
      }

      public void __callDele_EventOnSelectAnikiGym(ConfigDataAnikiGymInfo obj)
      {
        this.m_owner.__callDele_EventOnSelectAnikiGym(obj);
      }

      public void __clearDele_EventOnSelectAnikiGym(ConfigDataAnikiGymInfo obj)
      {
        this.m_owner.__clearDele_EventOnSelectAnikiGym(obj);
      }

      public void __callDele_EventOnStartAnikiLevel(ConfigDataAnikiLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartAnikiLevel(obj);
      }

      public void __clearDele_EventOnStartAnikiLevel(ConfigDataAnikiLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartAnikiLevel(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_teamButton
      {
        get
        {
          return this.m_owner.m_teamButton;
        }
        set
        {
          this.m_owner.m_teamButton = value;
        }
      }

      public Button m_blessingButton
      {
        get
        {
          return this.m_owner.m_blessingButton;
        }
        set
        {
          this.m_owner.m_blessingButton = value;
        }
      }

      public Text m_dailyRewardCountText
      {
        get
        {
          return this.m_owner.m_dailyRewardCountText;
        }
        set
        {
          this.m_owner.m_dailyRewardCountText = value;
        }
      }

      public GameObject m_gymListGameObject
      {
        get
        {
          return this.m_owner.m_gymListGameObject;
        }
        set
        {
          this.m_owner.m_gymListGameObject = value;
        }
      }

      public Text m_gymNameText
      {
        get
        {
          return this.m_owner.m_gymNameText;
        }
        set
        {
          this.m_owner.m_gymNameText = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_anikiGymListItemPrefab
      {
        get
        {
          return this.m_owner.m_anikiGymListItemPrefab;
        }
        set
        {
          this.m_owner.m_anikiGymListItemPrefab = value;
        }
      }

      public GameObject m_anikiLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_anikiLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_anikiLevelListItemPrefab = value;
        }
      }

      public List<AnikiGymListItemUIController> m_anikiGymListItems
      {
        get
        {
          return this.m_owner.m_anikiGymListItems;
        }
        set
        {
          this.m_owner.m_anikiGymListItems = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnAddTicketButtonClick()
      {
        this.m_owner.OnAddTicketButtonClick();
      }

      public void OnTeamButtonClick()
      {
        this.m_owner.OnTeamButtonClick();
      }

      public void OnBlessingButtonClick()
      {
        this.m_owner.OnBlessingButtonClick();
      }

      public void AnikiGymListItem_OnToggleValueChanged(
        bool isOn,
        AnikiGymListItemUIController ctrl)
      {
        this.m_owner.AnikiGymListItem_OnToggleValueChanged(isOn, ctrl);
      }

      public void AnikiLevelListItem_OnStartButtonClick(AnikiLevelListItemUIController ctrl)
      {
        this.m_owner.AnikiLevelListItem_OnStartButtonClick(ctrl);
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }
    }
  }
}
