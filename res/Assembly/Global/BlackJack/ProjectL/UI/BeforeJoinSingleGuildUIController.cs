﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BeforeJoinSingleGuildUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BeforeJoinSingleGuildUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildButton;
    [AutoBind("./SecletImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_guildCreateButton;
    [AutoBind("./SociatyNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sociatyNameText;
    [AutoBind("./PeopleValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peopleValueText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_LevelRequireText;
    [AutoBind("./VitalityText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_vitalityText;
    [AutoBind("./Apply", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_applyGameObject;
    private Action<BeforeJoinSingleGuildUIController> OnEventSelect;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public GuildSearchInfo m_guildSearchInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      GuildSearchInfo guildSearchInfo,
      Action<BeforeJoinSingleGuildUIController> selectClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelect(bool isSelect)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetApply(bool isApply)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh(GuildSearchInfo guildSearchInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
    }
  }
}
