﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScrollItemBaseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ScrollItemBaseUIController : UIControllerBase
  {
    public Action<UIControllerBase> EventOnUIItemNeedFill;
    public Action<UIControllerBase> EventOnUIItemClick;
    public Action<UIControllerBase> EventOnUIItemDoubleClick;
    public Action<UIControllerBase> EventOnUIItem3DTouch;
    public int m_itemIndex;
    public UIControllerBase m_ownerCtrl;
    private ThreeDTouchEventListener m_3dTouchEventTrigger;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(UIControllerBase ownerCtrl, bool isCareItemClick = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ScrollCellIndex(int itemIndex)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItemDoubleClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItem3DTouch()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ItemIndex
    {
      get
      {
        return this.m_itemIndex;
      }
      set
      {
        this.m_itemIndex = value;
      }
    }
  }
}
