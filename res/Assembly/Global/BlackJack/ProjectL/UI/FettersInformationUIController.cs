﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersInformationUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FettersInformationUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleLife", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggleLife;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleVoice", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggleVoice;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleLife/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleLifeRedPoint;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleVoice/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleVoiceRedPoint;
    [AutoBind("./InfoPanel/LifeIntroductionScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_lifeScrollRect;
    [AutoBind("./InfoPanel/VoiceScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_voiceScrollRect;
    [AutoBind("./InfoPanel/LifeIntroductionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lifeContent;
    [AutoBind("./InfoPanel/VoiceScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_voiceContent;
    [AutoBind("./InfoPanel/VoiceScrollView/CV/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceCVNameText;
    [AutoBind("./NameInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./NameInfo/EngNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroEngNameText;
    [AutoBind("./NameInfo/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroRankImage;
    private Hero m_hero;
    private ConfigDataHeroInformationInfo m_heroInformationInfo;
    private List<FettersInformationLifeItemUIController> biographyCtrlList;
    private List<FettersInformationVoiceItemUIController> voiceCtrlList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersInformationUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnDisable()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersInformation(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroLifeList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroVoiceList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateToggleNewTag()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeofermanceVoiceButtonClick(FettersInformationVoiceItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleLifeValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleVoiceValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HaveNewInfomation(ConfigDataHeroInformationInfo heroInfomationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HaveNewBiography(ConfigDataHeroInformationInfo heroInfomationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HaveNewVoice(ConfigDataHeroInformationInfo heroInfomationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> EventOnVoiceItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
