﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AppScoreUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class AppScoreUITask : UITask
  {
    private AppScoreUIController m_appScoreUIController;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const int APPSCORE_RIFTID = 104;
    public const string SHOW_APPSCORE_KEY = "SHOW_APPSCORE";

    [MethodImpl((MethodImplOptions) 32768)]
    public AppScoreUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppScoreUIController_OnGoScore()
    {
      // ISSUE: unable to decompile the method.
    }

    private void AppScoreUIController_OnCloseAppScore()
    {
      this.CloseSelf();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSelf()
    {
    }

    public static void ShowAppScoreUI(UIIntent currIntent)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CanScore()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
