﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePauseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattlePauseUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Objective/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Objective/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exitButton;
    [AutoBind("./Objective/AchievementButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementButton;
    [AutoBind("./Objective/SetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_settingButton;
    [AutoBind("./Objective/StrategyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_strategyButton;
    [AutoBind("./Objective/AchievementButton/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementButtonText;
    [AutoBind("./Objective/Turn", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_turnGameObject;
    [AutoBind("./Objective/Turn/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_turnText;
    [AutoBind("./Objective/WinDesc/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionGroupGameObject;
    [AutoBind("./Objective/LoseDesc/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_loseConditionGroupGameObject;
    [AutoBind("./Objective/StarDesc/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starConditionGroupGameObject;
    [AutoBind("./Objective/Map/Terrain", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapTerrainGameObject;
    [AutoBind("./Objective/Map/Region", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapRegionGameObject;
    [AutoBind("./Objective/Map/Actor", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActorGameObject;
    [AutoBind("./Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementUIStateController;
    [AutoBind("./Achievement/Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementScrollRect;
    [AutoBind("./Achievement/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementBackgroundButton;
    [AutoBind("./Strategy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_strategyUIStateController;
    [AutoBind("./Strategy/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_strategyBackgroundButton;
    [AutoBind("./Strategy/Panel/Left/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_strategyContentText;
    [AutoBind("./Strategy/Panel/Right/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_danmakuContent;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/Terrain", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapTerrainPrefab;
    [AutoBind("./Prefabs/Actor0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActor0Prefab;
    [AutoBind("./Prefabs/Actor1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActor1Prefab;
    [AutoBind("./Prefabs/Actor2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActor2Prefab;
    [AutoBind("./Prefabs/Reach", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapReachPrefab;
    [AutoBind("./Prefabs/ConditionInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_conditionPrefab;
    [AutoBind("./Prefabs/DanmakuItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_danmakuItemPrefab;
    private List<DanmakuItemUIController> m_danmakuItemUICtrlList;
    private List<BattleAchievementItemUIController> m_achievementItems;
    private int m_achievementCountMax;
    private int m_mapWidth;
    private int m_mapHeight;
    private Vector2 m_mapTileSize;
    private bool m_isOpened;
    [DoNotToLua]
    private BattlePauseUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_IsOpened_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_ShowStrategyButtonBoolean_hotfix;
    private LuaFunction m_SetWinLoseConditionsStringString_hotfix;
    private LuaFunction m_SetConditionInfoGameObjectString_hotfix;
    private LuaFunction m_AddConditionGameObjectString_hotfix;
    private LuaFunction m_SetStarConditionBattleTypeInt32Int32_hotfix;
    private LuaFunction m_SetAchievementBattleLevelAchievementbeBattleType_hotfix;
    private LuaFunction m_SetAchievementCountInt32Int32_hotfix;
    private LuaFunction m_ShowAchievement_hotfix;
    private LuaFunction m_HideAchievement_hotfix;
    private LuaFunction m_AddAchievementItemConfigDataBattleAchievementInfoList`1GameObjectBoolean_hotfix;
    private LuaFunction m_ClearAchievementItems_hotfix;
    private LuaFunction m_SetTurnInt32Int32_hotfix;
    private LuaFunction m_SetMapBattleMap_hotfix;
    private LuaFunction m_ClearMap_hotfix;
    private LuaFunction m_SetMapActorsList`1List`1List`1_hotfix;
    private LuaFunction m_CreateMapActorsList`1GameObject_hotfix;
    private LuaFunction m_ClearMapActors_hotfix;
    private LuaFunction m_SetMapReachRegionList`1_hotfix;
    private LuaFunction m_ClearMapReachRegion_hotfix;
    private LuaFunction m_ClearMapAll_hotfix;
    private LuaFunction m_GridPositionToMapPositionInt32Int32_hotfix;
    private LuaFunction m_OnExitButtonClick_hotfix;
    private LuaFunction m_OnAchievementButtonClick_hotfix;
    private LuaFunction m_OnStrategyButtonClick_hotfix;
    private LuaFunction m_UpdateStrategyPanel_hotfix;
    private LuaFunction m_Co_StartDanmakuItemsTweenPos_hotfix;
    private LuaFunction m_OnStrategyBackgroundButtonClick_hotfix;
    private LuaFunction m_OnSetButtonClick_hotfix;
    private LuaFunction m_OnCloseButtonClick_hotfix;
    private LuaFunction m_OnAchievementBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnExitAction_hotfix;
    private LuaFunction m_remove_EventOnExitAction_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnShowPlayerSettingAction_hotfix;
    private LuaFunction m_remove_EventOnShowPlayerSettingAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattlePauseUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStrategyButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWinLoseConditions(string winDesc, string loseDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConditionInfo(GameObject parent, string conditionStrs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCondition(GameObject parent, string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStarCondition(BattleType battleType, int starTurnMax, int starDeadMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievement(BattleLevelAchievement[] achievements, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievementCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddAchievementItem(
      ConfigDataBattleAchievementInfo achievementInfo,
      List<Goods> rewards,
      GameObject prefab,
      bool complete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAchievementItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTurn(int turn, int turnMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMap(BattleMap map)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMapActors(
      List<GridPosition> team0,
      List<GridPosition> team1,
      List<GridPosition> teamNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapActors(List<GridPosition> positions, GameObject prefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMapReachRegion(List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapReachRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearMapAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GridPositionToMapPosition(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrategyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStrategyPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_StartDanmakuItemsTweenPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrategyBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnExit
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowPlayerSetting
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattlePauseUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnExit()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnExit()
    {
      this.EventOnExit = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPlayerSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPlayerSetting()
    {
      this.EventOnShowPlayerSetting = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattlePauseUIController m_owner;

      public LuaExportHelper(BattlePauseUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnExit()
      {
        this.m_owner.__callDele_EventOnExit();
      }

      public void __clearDele_EventOnExit()
      {
        this.m_owner.__clearDele_EventOnExit();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnShowPlayerSetting()
      {
        this.m_owner.__callDele_EventOnShowPlayerSetting();
      }

      public void __clearDele_EventOnShowPlayerSetting()
      {
        this.m_owner.__clearDele_EventOnShowPlayerSetting();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Button m_exitButton
      {
        get
        {
          return this.m_owner.m_exitButton;
        }
        set
        {
          this.m_owner.m_exitButton = value;
        }
      }

      public Button m_achievementButton
      {
        get
        {
          return this.m_owner.m_achievementButton;
        }
        set
        {
          this.m_owner.m_achievementButton = value;
        }
      }

      public Button m_settingButton
      {
        get
        {
          return this.m_owner.m_settingButton;
        }
        set
        {
          this.m_owner.m_settingButton = value;
        }
      }

      public Button m_strategyButton
      {
        get
        {
          return this.m_owner.m_strategyButton;
        }
        set
        {
          this.m_owner.m_strategyButton = value;
        }
      }

      public Text m_achievementButtonText
      {
        get
        {
          return this.m_owner.m_achievementButtonText;
        }
        set
        {
          this.m_owner.m_achievementButtonText = value;
        }
      }

      public GameObject m_turnGameObject
      {
        get
        {
          return this.m_owner.m_turnGameObject;
        }
        set
        {
          this.m_owner.m_turnGameObject = value;
        }
      }

      public Text m_turnText
      {
        get
        {
          return this.m_owner.m_turnText;
        }
        set
        {
          this.m_owner.m_turnText = value;
        }
      }

      public GameObject m_winConditionGroupGameObject
      {
        get
        {
          return this.m_owner.m_winConditionGroupGameObject;
        }
        set
        {
          this.m_owner.m_winConditionGroupGameObject = value;
        }
      }

      public GameObject m_loseConditionGroupGameObject
      {
        get
        {
          return this.m_owner.m_loseConditionGroupGameObject;
        }
        set
        {
          this.m_owner.m_loseConditionGroupGameObject = value;
        }
      }

      public GameObject m_starConditionGroupGameObject
      {
        get
        {
          return this.m_owner.m_starConditionGroupGameObject;
        }
        set
        {
          this.m_owner.m_starConditionGroupGameObject = value;
        }
      }

      public GameObject m_mapTerrainGameObject
      {
        get
        {
          return this.m_owner.m_mapTerrainGameObject;
        }
        set
        {
          this.m_owner.m_mapTerrainGameObject = value;
        }
      }

      public GameObject m_mapRegionGameObject
      {
        get
        {
          return this.m_owner.m_mapRegionGameObject;
        }
        set
        {
          this.m_owner.m_mapRegionGameObject = value;
        }
      }

      public GameObject m_mapActorGameObject
      {
        get
        {
          return this.m_owner.m_mapActorGameObject;
        }
        set
        {
          this.m_owner.m_mapActorGameObject = value;
        }
      }

      public CommonUIStateController m_achievementUIStateController
      {
        get
        {
          return this.m_owner.m_achievementUIStateController;
        }
        set
        {
          this.m_owner.m_achievementUIStateController = value;
        }
      }

      public ScrollRect m_achievementScrollRect
      {
        get
        {
          return this.m_owner.m_achievementScrollRect;
        }
        set
        {
          this.m_owner.m_achievementScrollRect = value;
        }
      }

      public Button m_achievementBackgroundButton
      {
        get
        {
          return this.m_owner.m_achievementBackgroundButton;
        }
        set
        {
          this.m_owner.m_achievementBackgroundButton = value;
        }
      }

      public CommonUIStateController m_strategyUIStateController
      {
        get
        {
          return this.m_owner.m_strategyUIStateController;
        }
        set
        {
          this.m_owner.m_strategyUIStateController = value;
        }
      }

      public Button m_strategyBackgroundButton
      {
        get
        {
          return this.m_owner.m_strategyBackgroundButton;
        }
        set
        {
          this.m_owner.m_strategyBackgroundButton = value;
        }
      }

      public Text m_strategyContentText
      {
        get
        {
          return this.m_owner.m_strategyContentText;
        }
        set
        {
          this.m_owner.m_strategyContentText = value;
        }
      }

      public GameObject m_danmakuContent
      {
        get
        {
          return this.m_owner.m_danmakuContent;
        }
        set
        {
          this.m_owner.m_danmakuContent = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_mapTerrainPrefab
      {
        get
        {
          return this.m_owner.m_mapTerrainPrefab;
        }
        set
        {
          this.m_owner.m_mapTerrainPrefab = value;
        }
      }

      public GameObject m_mapActor0Prefab
      {
        get
        {
          return this.m_owner.m_mapActor0Prefab;
        }
        set
        {
          this.m_owner.m_mapActor0Prefab = value;
        }
      }

      public GameObject m_mapActor1Prefab
      {
        get
        {
          return this.m_owner.m_mapActor1Prefab;
        }
        set
        {
          this.m_owner.m_mapActor1Prefab = value;
        }
      }

      public GameObject m_mapActor2Prefab
      {
        get
        {
          return this.m_owner.m_mapActor2Prefab;
        }
        set
        {
          this.m_owner.m_mapActor2Prefab = value;
        }
      }

      public GameObject m_mapReachPrefab
      {
        get
        {
          return this.m_owner.m_mapReachPrefab;
        }
        set
        {
          this.m_owner.m_mapReachPrefab = value;
        }
      }

      public GameObject m_conditionPrefab
      {
        get
        {
          return this.m_owner.m_conditionPrefab;
        }
        set
        {
          this.m_owner.m_conditionPrefab = value;
        }
      }

      public GameObject m_danmakuItemPrefab
      {
        get
        {
          return this.m_owner.m_danmakuItemPrefab;
        }
        set
        {
          this.m_owner.m_danmakuItemPrefab = value;
        }
      }

      public List<DanmakuItemUIController> m_danmakuItemUICtrlList
      {
        get
        {
          return this.m_owner.m_danmakuItemUICtrlList;
        }
        set
        {
          this.m_owner.m_danmakuItemUICtrlList = value;
        }
      }

      public List<BattleAchievementItemUIController> m_achievementItems
      {
        get
        {
          return this.m_owner.m_achievementItems;
        }
        set
        {
          this.m_owner.m_achievementItems = value;
        }
      }

      public int m_achievementCountMax
      {
        get
        {
          return this.m_owner.m_achievementCountMax;
        }
        set
        {
          this.m_owner.m_achievementCountMax = value;
        }
      }

      public int m_mapWidth
      {
        get
        {
          return this.m_owner.m_mapWidth;
        }
        set
        {
          this.m_owner.m_mapWidth = value;
        }
      }

      public int m_mapHeight
      {
        get
        {
          return this.m_owner.m_mapHeight;
        }
        set
        {
          this.m_owner.m_mapHeight = value;
        }
      }

      public Vector2 m_mapTileSize
      {
        get
        {
          return this.m_owner.m_mapTileSize;
        }
        set
        {
          this.m_owner.m_mapTileSize = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetConditionInfo(GameObject parent, string conditionStrs)
      {
        this.m_owner.SetConditionInfo(parent, conditionStrs);
      }

      public void AddCondition(GameObject parent, string str)
      {
        this.m_owner.AddCondition(parent, str);
      }

      public void ShowAchievement()
      {
        this.m_owner.ShowAchievement();
      }

      public void HideAchievement()
      {
        this.m_owner.HideAchievement();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddAchievementItem(
        ConfigDataBattleAchievementInfo achievementInfo,
        List<Goods> rewards,
        GameObject prefab,
        bool complete)
      {
        // ISSUE: unable to decompile the method.
      }

      public void ClearAchievementItems()
      {
        this.m_owner.ClearAchievementItems();
      }

      public void ClearMap()
      {
        this.m_owner.ClearMap();
      }

      public void CreateMapActors(List<GridPosition> positions, GameObject prefab)
      {
        this.m_owner.CreateMapActors(positions, prefab);
      }

      public void ClearMapActors()
      {
        this.m_owner.ClearMapActors();
      }

      public void ClearMapReachRegion()
      {
        this.m_owner.ClearMapReachRegion();
      }

      public Vector2 GridPositionToMapPosition(int x, int y)
      {
        return this.m_owner.GridPositionToMapPosition(x, y);
      }

      public void OnExitButtonClick()
      {
        this.m_owner.OnExitButtonClick();
      }

      public void OnAchievementButtonClick()
      {
        this.m_owner.OnAchievementButtonClick();
      }

      public void OnStrategyButtonClick()
      {
        this.m_owner.OnStrategyButtonClick();
      }

      public void UpdateStrategyPanel()
      {
        this.m_owner.UpdateStrategyPanel();
      }

      public IEnumerator Co_StartDanmakuItemsTweenPos()
      {
        return this.m_owner.Co_StartDanmakuItemsTweenPos();
      }

      public void OnStrategyBackgroundButtonClick()
      {
        this.m_owner.OnStrategyBackgroundButtonClick();
      }

      public void OnSetButtonClick()
      {
        this.m_owner.OnSetButtonClick();
      }

      public void OnCloseButtonClick()
      {
        this.m_owner.OnCloseButtonClick();
      }

      public void OnAchievementBackgroundButtonClick()
      {
        this.m_owner.OnAchievementBackgroundButtonClick();
      }
    }
  }
}
