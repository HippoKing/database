﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroItemUIController : UIControllerBase
  {
    [AutoBind("./CollectProgressBar/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroCollectProgressImg;
    [AutoBind("./CollectProgressBar/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroCollectProgressText;
    [AutoBind("./SelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectFrameImg;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroTypeImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroTypeImg;
    [AutoBind("./HeroLvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    [AutoBind("./SSRFrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrFrameEffect;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frameImg;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_redMark;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    public HeroItemUIController.HeroItemState m_curHeroItemState;
    public Hero m_hero;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private HeroItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitValuesInHeroItemUnlockedHeroInt32Int32_hotfix;
    private LuaFunction m_InitValuesInHeroItemLockedHeroInt32Int32Int32_hotfix;
    private LuaFunction m_InitValuesInHeroItemInt32Int32Int32Int32Int32_hotfix;
    private LuaFunction m_SetUnlockCardRedPoint_hotfix;
    private LuaFunction m_ShowSelectFrameImageBoolean_hotfix;
    private LuaFunction m_OnHeroItemClick_hotfix;
    private LuaFunction m_add_EventOnHeroItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnHeroItemClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitValuesInHeroItemUnlocked(Hero hero, int heroLvText, int heroStarNum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitValuesInHeroItemLocked(
      Hero hero,
      int starNums,
      int collectCurNum,
      int collectTotalNum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitValuesInHeroItem(
      int lvText = 1,
      int starNum = 1,
      int curNum = 0,
      int totalNum = 1,
      int state = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnlockCardRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectFrameImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroItemUIController> EventOnHeroItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroItemClick(HeroItemUIController obj)
    {
    }

    private void __clearDele_EventOnHeroItemClick(HeroItemUIController obj)
    {
      this.EventOnHeroItemClick = (Action<HeroItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum HeroItemState
    {
      Get,
      NotGet,
      NotGetAndGlowing,
    }

    public class LuaExportHelper
    {
      private HeroItemUIController m_owner;

      public LuaExportHelper(HeroItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnHeroItemClick(HeroItemUIController obj)
      {
        this.m_owner.__callDele_EventOnHeroItemClick(obj);
      }

      public void __clearDele_EventOnHeroItemClick(HeroItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnHeroItemClick(obj);
      }

      public Image m_heroCollectProgressImg
      {
        get
        {
          return this.m_owner.m_heroCollectProgressImg;
        }
        set
        {
          this.m_owner.m_heroCollectProgressImg = value;
        }
      }

      public Text m_heroCollectProgressText
      {
        get
        {
          return this.m_owner.m_heroCollectProgressText;
        }
        set
        {
          this.m_owner.m_heroCollectProgressText = value;
        }
      }

      public Image m_selectFrameImg
      {
        get
        {
          return this.m_owner.m_selectFrameImg;
        }
        set
        {
          this.m_owner.m_selectFrameImg = value;
        }
      }

      public GameObject m_heroStar
      {
        get
        {
          return this.m_owner.m_heroStar;
        }
        set
        {
          this.m_owner.m_heroStar = value;
        }
      }

      public Image m_heroTypeImg
      {
        get
        {
          return this.m_owner.m_heroTypeImg;
        }
        set
        {
          this.m_owner.m_heroTypeImg = value;
        }
      }

      public Text m_heroLvText
      {
        get
        {
          return this.m_owner.m_heroLvText;
        }
        set
        {
          this.m_owner.m_heroLvText = value;
        }
      }

      public GameObject m_ssrFrameEffect
      {
        get
        {
          return this.m_owner.m_ssrFrameEffect;
        }
        set
        {
          this.m_owner.m_ssrFrameEffect = value;
        }
      }

      public Image m_frameImg
      {
        get
        {
          return this.m_owner.m_frameImg;
        }
        set
        {
          this.m_owner.m_frameImg = value;
        }
      }

      public Image m_heroIconImg
      {
        get
        {
          return this.m_owner.m_heroIconImg;
        }
        set
        {
          this.m_owner.m_heroIconImg = value;
        }
      }

      public Image m_redMark
      {
        get
        {
          return this.m_owner.m_redMark;
        }
        set
        {
          this.m_owner.m_redMark = value;
        }
      }

      public CommonUIStateController m_uiStateCtrl
      {
        get
        {
          return this.m_owner.m_uiStateCtrl;
        }
        set
        {
          this.m_owner.m_uiStateCtrl = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitValuesInHeroItem(
        int lvText,
        int starNum,
        int curNum,
        int totalNum,
        int state)
      {
        this.m_owner.InitValuesInHeroItem(lvText, starNum, curNum, totalNum, state);
      }

      public void OnHeroItemClick()
      {
        this.m_owner.OnHeroItemClick();
      }
    }
  }
}
