﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NoticeUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class NoticeUITask : UITask
  {
    private string m_currentNoticeInfo;
    private string m_currentNoticeStateName;
    private ChatChannel m_chatChannel;
    private bool m_currentNoticeIsVoice;
    private NoticeUIController m_noticeUICtrl;
    public static readonly string ParamKey_NoticeInfo;
    public static readonly string ParamKey_NoticeStateName;
    public static readonly string ParamKey_NoticeChatChannel;
    public static readonly string ParamKey_NoticeIsVoice;
    public static bool m_isEnableClick;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public NoticeUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      string contentStr,
      string uiStateName,
      ChatChannel chatChannel,
      bool isVoiceMsg = false)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void EnableUIInput(bool isEnable, bool? isGlobalEnable = null)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NoticeUIController_OnNoticeClick(NoticeText currentNoticeText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePositionState()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static NoticeUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
