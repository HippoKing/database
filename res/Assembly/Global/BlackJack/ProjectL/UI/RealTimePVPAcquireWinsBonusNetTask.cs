﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RealTimePVPAcquireWinsBonusNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RealTimePVPAcquireWinsBonusNetTask : UINetTask
  {
    private int bonusId;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPAcquireWinsBonusNetTask(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRealTimePVPAcquireWinsBonusAck(int result, BattleReward reward)
    {
    }

    public BattleReward Reward { private set; get; }
  }
}
