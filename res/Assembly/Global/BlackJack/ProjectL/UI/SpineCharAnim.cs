﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SpineCharAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SpineCharAnim
  {
    public const string Idle_Normal = "idle_Normal";
    public const string Idle_Angry = "idle_Angry";
    public const string Idle_Injury = "idle_Injury";
    public const string Idle_Sad = "idle_Sad";
    public const string Idle_Shy = "idle_Shy";
    public const string Idle_Sigh = "idle_Sigh";
    public const string Idle_Smile = "idle_Smile";
    public const string Idle_Battle_Normal = "idle_Battle_Normal";
    public const string Idle_Battle_Mighty = "idle_Battle_Mighty";
    public const string Idle_Battle_Weak = "idle_Battle_Weak";
  }
}
