﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MissionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class MissionItemUIController : UIControllerBase
  {
    public ScrollItemBaseUIController m_scrollItemBaseUICtrl;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_achievementIconGo;
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_titleText;
    [AutoBind("./Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_descText;
    [AutoBind("./Detail/CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    [AutoBind("./Detail/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_getButton;
    [AutoBind("./Detail/ProgressBar/ProgressImg", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_progressImg;
    [AutoBind("./Detail/RewardList", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_rewardListGo;
    private Mission m_mission;
    private bool m_isMissionFinished;
    private bool m_isMissionCompleted;
    private ProjectLPlayerContext playerCtx;
    [DoNotToLua]
    private MissionItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitAction`1Action`1Action`1_hotfix;
    private LuaFunction m_InitMissionItemMissionBooleanBoolean_hotfix;
    private LuaFunction m_SetMissionInfo_hotfix;
    private LuaFunction m_OnGetButtonClick_hotfix;
    private LuaFunction m_OnGotoButtonClick_hotfix;
    private LuaFunction m_add_EventOnGotoButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnGetButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      Action<UIControllerBase> actionOnFillUpdate,
      Action<int> actionOnGetRewardButtonClick,
      Action<GetPathData> actionOnGotoLayerButtonClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitMissionItem(Mission mission, bool isMissionFinished, bool isMissionCompleted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private event Action<GetPathData> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action<int> EventOnGetButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public MissionItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoButtonClick(GetPathData obj)
    {
    }

    private void __clearDele_EventOnGotoButtonClick(GetPathData obj)
    {
      this.EventOnGotoButtonClick = (Action<GetPathData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetButtonClick(int obj)
    {
    }

    private void __clearDele_EventOnGetButtonClick(int obj)
    {
      this.EventOnGetButtonClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private MissionItemUIController m_owner;

      public LuaExportHelper(MissionItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnGotoButtonClick(GetPathData obj)
      {
        this.m_owner.__callDele_EventOnGotoButtonClick(obj);
      }

      public void __clearDele_EventOnGotoButtonClick(GetPathData obj)
      {
        this.m_owner.__clearDele_EventOnGotoButtonClick(obj);
      }

      public void __callDele_EventOnGetButtonClick(int obj)
      {
        this.m_owner.__callDele_EventOnGetButtonClick(obj);
      }

      public void __clearDele_EventOnGetButtonClick(int obj)
      {
        this.m_owner.__clearDele_EventOnGetButtonClick(obj);
      }

      public Mission m_mission
      {
        get
        {
          return this.m_owner.m_mission;
        }
        set
        {
          this.m_owner.m_mission = value;
        }
      }

      public bool m_isMissionFinished
      {
        get
        {
          return this.m_owner.m_isMissionFinished;
        }
        set
        {
          this.m_owner.m_isMissionFinished = value;
        }
      }

      public bool m_isMissionCompleted
      {
        get
        {
          return this.m_owner.m_isMissionCompleted;
        }
        set
        {
          this.m_owner.m_isMissionCompleted = value;
        }
      }

      public ProjectLPlayerContext playerCtx
      {
        get
        {
          return this.m_owner.playerCtx;
        }
        set
        {
          this.m_owner.playerCtx = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetMissionInfo()
      {
        this.m_owner.SetMissionInfo();
      }

      public void OnGetButtonClick()
      {
        this.m_owner.OnGetButtonClick();
      }

      public void OnGotoButtonClick()
      {
        this.m_owner.OnGotoButtonClick();
      }

      public event Action<GetPathData> EventOnGotoButtonClick
      {
        add
        {
          this.m_owner.EventOnGotoButtonClick += value;
        }
        remove
        {
          this.m_owner.EventOnGotoButtonClick += value;
        }
      }

      public event Action<int> EventOnGetButtonClick
      {
        add
        {
          this.m_owner.EventOnGetButtonClick += value;
        }
        remove
        {
          this.m_owner.EventOnGetButtonClick += value;
        }
      }
    }
  }
}
