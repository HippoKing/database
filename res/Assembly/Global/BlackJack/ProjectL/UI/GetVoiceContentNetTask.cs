﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GetVoiceContentNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GetVoiceContentNetTask : UINetTask
  {
    private ulong m_instanceId;
    private ChatChannel m_channel;
    private ChatVoiceMessage m_voiceContent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GetVoiceContentNetTask(ChatChannel channel, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnGetVoiceContentAck(ChatVoiceMessage voiceContent)
    {
      this.m_voiceContent = voiceContent;
      this.Stop();
    }

    public ChatVoiceMessage VoiceContent
    {
      get
      {
        return this.m_voiceContent;
      }
    }
  }
}
