﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInviteUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamRoomInviteUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_friendToggle;
    [AutoBind("./Panel/ToggleGroup/Nearly", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recentToggle;
    [AutoBind("./Panel/ToggleGroup/Sociaty", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildToggle;
    [AutoBind("./Panel/ToggleGroup/Sociaty/UnopenMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildToggleUnopenMask;
    [AutoBind("./Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_inviteScrollRect;
    [AutoBind("./Panel/InvitedCount/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteCountTitleText;
    [AutoBind("./Panel/InvitedCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteCountValueText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/PlayerListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_invitePlayerListItemPrefab;
    private List<TeamRoomInvitePlayerListItemUIController> m_playerListItems;
    private bool m_isIgnoreToggleEvent;
    private bool m_isOpened;
    [DoNotToLua]
    private TeamRoomInviteUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_CloseAction_hotfix;
    private LuaFunction m_SetPlayerTypeTeamRoomInvitePlayerTypeBoolean_hotfix;
    private LuaFunction m_SetPlayersList`1_hotfix;
    private LuaFunction m_IsRecentTeamBattlePlayerString_hotfix;
    private LuaFunction m_AddPlayerListItemStringStringInt32Int32Boolean_hotfix;
    private LuaFunction m_ClearPlayerListItems_hotfix;
    private LuaFunction m_UpdatePlayerStatusStringTeamRoomPlayerInviteState_hotfix;
    private LuaFunction m_ShowGildToggleUnopenMaskBoolean_hotfix;
    private LuaFunction m_OnFriendToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnRecentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnGuildToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnGuildUnopenMaskClick_hotfix;
    private LuaFunction m_OnConfirmButtonClick_hotfix;
    private LuaFunction m_OnCancelButtonClick_hotfix;
    private LuaFunction m_add_EventOnConfirmAction`1_hotfix;
    private LuaFunction m_remove_EventOnConfirmAction`1_hotfix;
    private LuaFunction m_add_EventOnCancelAction_hotfix;
    private LuaFunction m_remove_EventOnCancelAction_hotfix;
    private LuaFunction m_add_EventOnChangePlayerTypeAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangePlayerTypeAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomInviteUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerType(TeamRoomInvitePlayerType playerType, bool canChangePlayerType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayers(List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRecentTeamBattlePlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayerListItem(
      string userId,
      string name,
      int headIconId,
      int level,
      bool isRecent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayerListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerStatus(string userId, TeamRoomPlayerInviteState inviteState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGildToggleUnopenMask(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildUnopenMaskClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<string>> EventOnConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomInvitePlayerType> EventOnChangePlayerType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamRoomInviteUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnConfirm(List<string> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnConfirm(List<string> obj)
    {
      this.EventOnConfirm = (Action<List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCancel()
    {
      this.EventOnCancel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangePlayerType(TeamRoomInvitePlayerType obj)
    {
    }

    private void __clearDele_EventOnChangePlayerType(TeamRoomInvitePlayerType obj)
    {
      this.EventOnChangePlayerType = (Action<TeamRoomInvitePlayerType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamRoomInviteUIController m_owner;

      public LuaExportHelper(TeamRoomInviteUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnConfirm(List<string> obj)
      {
        this.m_owner.__callDele_EventOnConfirm(obj);
      }

      public void __clearDele_EventOnConfirm(List<string> obj)
      {
        this.m_owner.__clearDele_EventOnConfirm(obj);
      }

      public void __callDele_EventOnCancel()
      {
        this.m_owner.__callDele_EventOnCancel();
      }

      public void __clearDele_EventOnCancel()
      {
        this.m_owner.__clearDele_EventOnCancel();
      }

      public void __callDele_EventOnChangePlayerType(TeamRoomInvitePlayerType obj)
      {
        this.m_owner.__callDele_EventOnChangePlayerType(obj);
      }

      public void __clearDele_EventOnChangePlayerType(TeamRoomInvitePlayerType obj)
      {
        this.m_owner.__clearDele_EventOnChangePlayerType(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Toggle m_friendToggle
      {
        get
        {
          return this.m_owner.m_friendToggle;
        }
        set
        {
          this.m_owner.m_friendToggle = value;
        }
      }

      public Toggle m_recentToggle
      {
        get
        {
          return this.m_owner.m_recentToggle;
        }
        set
        {
          this.m_owner.m_recentToggle = value;
        }
      }

      public Toggle m_guildToggle
      {
        get
        {
          return this.m_owner.m_guildToggle;
        }
        set
        {
          this.m_owner.m_guildToggle = value;
        }
      }

      public Button m_guildToggleUnopenMask
      {
        get
        {
          return this.m_owner.m_guildToggleUnopenMask;
        }
        set
        {
          this.m_owner.m_guildToggleUnopenMask = value;
        }
      }

      public Button m_confirmButton
      {
        get
        {
          return this.m_owner.m_confirmButton;
        }
        set
        {
          this.m_owner.m_confirmButton = value;
        }
      }

      public Button m_cancelButton
      {
        get
        {
          return this.m_owner.m_cancelButton;
        }
        set
        {
          this.m_owner.m_cancelButton = value;
        }
      }

      public ScrollRect m_inviteScrollRect
      {
        get
        {
          return this.m_owner.m_inviteScrollRect;
        }
        set
        {
          this.m_owner.m_inviteScrollRect = value;
        }
      }

      public Text m_inviteCountTitleText
      {
        get
        {
          return this.m_owner.m_inviteCountTitleText;
        }
        set
        {
          this.m_owner.m_inviteCountTitleText = value;
        }
      }

      public Text m_inviteCountValueText
      {
        get
        {
          return this.m_owner.m_inviteCountValueText;
        }
        set
        {
          this.m_owner.m_inviteCountValueText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_invitePlayerListItemPrefab
      {
        get
        {
          return this.m_owner.m_invitePlayerListItemPrefab;
        }
        set
        {
          this.m_owner.m_invitePlayerListItemPrefab = value;
        }
      }

      public List<TeamRoomInvitePlayerListItemUIController> m_playerListItems
      {
        get
        {
          return this.m_owner.m_playerListItems;
        }
        set
        {
          this.m_owner.m_playerListItems = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public bool IsRecentTeamBattlePlayer(string userId)
      {
        return this.m_owner.IsRecentTeamBattlePlayer(userId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddPlayerListItem(
        string userId,
        string name,
        int headIconId,
        int level,
        bool isRecent)
      {
      }

      public void ClearPlayerListItems()
      {
        this.m_owner.ClearPlayerListItems();
      }

      public void OnFriendToggleValueChanged(bool isOn)
      {
        this.m_owner.OnFriendToggleValueChanged(isOn);
      }

      public void OnRecentToggleValueChanged(bool isOn)
      {
        this.m_owner.OnRecentToggleValueChanged(isOn);
      }

      public void OnGuildToggleValueChanged(bool isOn)
      {
        this.m_owner.OnGuildToggleValueChanged(isOn);
      }

      public void OnGuildUnopenMaskClick()
      {
        this.m_owner.OnGuildUnopenMaskClick();
      }

      public void OnConfirmButtonClick()
      {
        this.m_owner.OnConfirmButtonClick();
      }

      public void OnCancelButtonClick()
      {
        this.m_owner.OnCancelButtonClick();
      }
    }
  }
}
