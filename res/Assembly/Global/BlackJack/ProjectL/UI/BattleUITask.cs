﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.Scene;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleUITask : UITask, IClientBattleListener
  {
    private const int BattleDanmakuLayerIndex = 0;
    private const int BattleRoomLayerIndex = 1;
    private const int BattleLayerIndex = 2;
    private const int BattleActorInfoLayerIndex = 3;
    private const int BattlePrepareLayerIndex = 4;
    private const int PVPBattlePrepareLayerIndex = 5;
    private const int BattlePrepareActorInfoLayerIndex = 6;
    private const int ActionOrderLayerIndex = 7;
    private const int BattlePauseLayerIndex = 8;
    private const int BattleTreasureDialogLayerIndex = 9;
    private const int CombatLayerIndex = 10;
    private const int PreCombatLayerIndex = 11;
    private const int BattleCommonLayerIndex = 12;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private BattleUIController m_battleUIController;
    private BattleDanmakuUIController m_battleDanmakuUIController;
    private BattleActorInfoUIController m_battleActorInfoUIController;
    private BattlePrepareUIController m_battlePrepareUIController;
    private PVPBattlePrepareUIController m_pvpBattlePrepareUIController;
    private BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController;
    private ActionOrderUIController m_actionOrderUIController;
    private BattlePauseUIController m_battlePauseUIController;
    private BattleTreasureDialogUIController m_battleTreasureDialogUIController;
    private CombatUIController m_combatUIController;
    private PreCombatUIController m_preCombatUIController;
    private BattleRoomUIController m_battleRoomUIController;
    private BattleCommonUIController m_battleCommonUIController;
    private BattleMapUIController m_battleMapUIController;
    private BattleSceneUIController m_battleSceneUIController;
    private CombatSceneUIController m_combatSceneUIController;
    private GetRewardGoodsUITask m_getRewardGoodsUITask;
    private BattleDialogUITask m_battleDialogUITask;
    private BattleResultUITask m_battleResultUITask;
    private BattleResultScoreUITask m_battleResultScoreUITask;
    private BattleLoseUITask m_battleLoseUITask;
    private BattleReportEndUITask m_battleReportEndUITask;
    private ClientBattle m_clientBattle;
    private IConfigDataLoader m_configDataLoader;
    private BattleUIState m_uiState;
    private ClientBattleActor m_activeActor;
    private GridPosition m_activeActorInitPosition;
    private int m_activeActorInitDirection;
    private int m_skillIndex;
    private GridPosition m_skillTargetPosition;
    private GridPosition m_skillTargetPosition2;
    private GridPosition m_combatStartPosition;
    private GridPosition m_combatTargetPosition;
    private bool m_isBattleCutsceneFade;
    private bool m_isCombatCutsceneFade;
    private DateTime m_battleMapClickTime;
    private int m_showDangerRegionTeam;
    private List<int> m_showDangerRegionActorIds;
    private BattleActor m_preCombatTargetActor;
    private bool m_saveShowTopUI;
    private bool m_saveShowBottomUI;
    private bool m_disableSaveProcessingBattle;
    private BattleTeamSetup m_battleTeamSetup0;
    private BattleTeamSetup m_battleTeamSetup1;
    private List<GridPosition> m_teamPositions0;
    private List<GridPosition> m_teamPositions1;
    private List<GridPosition> m_teamNpcPositions0;
    private List<BattleHero> m_playerBattleHeros;
    private List<int> m_tempIntList;
    private List<string> m_tempStringList;
    private HashSet<GridPosition> m_dangerRegion;
    private List<TrainingTech> m_trainingTechs;
    private List<TrainingTech> m_tempTrainingTechs;
    private List<Goods> m_tempGoodsList;
    private List<int> m_userGuideEnforceHeroIds;
    private List<int> m_arenaAttackerHeroIds;
    private List<int> m_myBattleHeroIds;
    private BattleLoadState m_loadState;
    private BattlePerformState m_battlePerformState;
    private bool m_isStartBattleAutoBattle;
    private int m_nowSeconds;
    private DateTime m_myActionTimeout;
    private DateTime m_otherActionTimeout;
    private bool m_isMyActionTimeoutActive;
    private bool m_isActionTimeoutAutoBattle;
    private bool m_isAutoBattleOnce;
    private List<int> m_pendingHeroSetupNtfs;
    private GridPosition m_selectProtectHeroPos;
    private GridPosition m_selectBanHeroPos;
    private List<object> m_collectAssetObjects;
    private List<string> m_collectedCharImageSpineAssets;
    private RandomNumber m_armyRandomNumber;
    private ProBattleReport m_battleReport;
    private int m_battleStopTurn;
    private bool m_isSendBattleCheatGMCommand;
    private bool m_isStartInBattleRoom;
    private DateTime m_battleStartTime;
    private List<BattleUITask.RegretStep> m_regretSteps;
    private List<BattleCommand> m_regretBattleCommands;
    private int m_regretFinalStep;
    private int m_regretCurrentStep;
    private int m_regretCancelStep;
    private int m_regretFinalTurn;
    private int m_regretCurrentTurn;
    private int m_regretCameraFocusActorId;
    private bool m_isBattleLoseRegret;
    private bool m_isTryActivateRegret;
    [DoNotToLua]
    private BattleUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_LogCurrentBattleId_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_HideAllView_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_IsLoading_hotfix;
    private LuaFunction m_CreateClientBattle_hotfix;
    private LuaFunction m_DestroyClientBattle_hotfix;
    private LuaFunction m_PrepareClientBattleBoolean_hotfix;
    private LuaFunction m_StartClientBattle_hotfix;
    private LuaFunction m_RebuildClientBattle_hotfix;
    private LuaFunction m_StopBattleBooleanBoolean_hotfix;
    private LuaFunction m_SendBattleRoomInitLog_hotfix;
    private LuaFunction m_GetBattleTeamSetupInt32_hotfix;
    private LuaFunction m_GetTeamPositionsInt32_hotfix;
    private LuaFunction m_SetCombatHpInt32_hotfix;
    private LuaFunction m_ExitBattleReturnToWorld_hotfix;
    private LuaFunction m_ShowErrorMessageAndExitBattleInt32_hotfix;
    private LuaFunction m_ShowErrorMessageAndExitBattleStringTableId_hotfix;
    private LuaFunction m_SaveProcessingBattle_hotfix;
    private LuaFunction m_BattlePrepareBeforeShowResult_hotfix;
    private LuaFunction m_IsMeArenaBattleTeam1_hotfix;
    private LuaFunction m_CanUseChat_hotfix;
    private LuaFunction m_CanUseDanmaku_hotfix;
    private LuaFunction m_get_ClientBattle_hotfix;
    private LuaFunction m_BattlePauseUIController_OnClose_hotfix;
    private LuaFunction m_BattlePauseUIController_OnShowPlayerSetting_hotfix;
    private LuaFunction m_BattlePauseUIController_OnExit_hotfix;
    private LuaFunction m_ExitBattleDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_TestUI_ExitBattle_hotfix;
    private LuaFunction m_TestUI_RestartBattle_hotfix;
    private LuaFunction m_TestUI_ReplayBattle_hotfix;
    private LuaFunction m_TestUI_PrepareBattle_hotfix;
    private LuaFunction m_TestUI_StopBattleBoolean_hotfix;
    private LuaFunction m_PlayerContext_OnChatMessageNtfChatMessage_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomInviteNtfTeamRoomInviteInfo_hotfix;
    private LuaFunction m_PlayerContext_OnBattlePracticeInvitedNtfPVPInviteInfo_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_CollectBattlePrepareAssets_hotfix;
    private LuaFunction m_CollectBattleAssets_hotfix;
    private LuaFunction m_CollectBattlePrepareHerosList`1List`1_hotfix;
    private LuaFunction m_CollectBattlefieldAssetsConfigDataBattlefieldInfo_hotfix;
    private LuaFunction m_CollectTerrainAssetsConfigDataTerrainInfo_hotfix;
    private LuaFunction m_CollectCharImageAssetsConfigDataCharImageInfoConfigDataCharImageSkinResourceInfo_hotfix;
    private LuaFunction m_CollectCharSpineAssetsConfigDataCharImageInfoConfigDataCharImageSkinResourceInfo_hotfix;
    private LuaFunction m_CollectHeadImageAssetsConfigDataCharImageInfo_hotfix;
    private LuaFunction m_CollectHeroAssetsConfigDataJobConnectionInfoConfigDataModelSkinResourceInfoInt32_hotfix;
    private LuaFunction m_CollectSoldierAssetsConfigDataSoldierInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_CollectSkinAssetsConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_CollectSkillAssetsConfigDataSkillInfo_hotfix;
    private LuaFunction m_CollectBuffAssetsConfigDataBuffInfo_hotfix;
    private LuaFunction m_CollectDefaultHeroAssetsConfigDataHeroInfo_hotfix;
    private LuaFunction m_CollectBattleEventAssetsConfigDataBattleEventTriggerInfo_hotfix;
    private LuaFunction m_CollectBattleEventActionAssetsConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_CollectBattlePerformAssetsConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m__CollectBattlePerformAssetsConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_CollectTrainingTechAssetsList`1_hotfix;
    private LuaFunction m_IsAlreadyCollectAssetObject_hotfix;
    private LuaFunction m_ClearAlreadyCollectAssets_hotfix;
    private LuaFunction m_StartBattleUI_hotfix;
    private LuaFunction m_ClientActorTryMoveToClientBattleActorGridPositionInt32_hotfix;
    private LuaFunction m_AddCommandIfMovedClientBattleActor_hotfix;
    private LuaFunction m_FindAttackPositionInt32GridPosition_hotfix;
    private LuaFunction m_CanAction_hotfix;
    private LuaFunction m_GetMapBattleActorGridPosition_hotfix;
    private LuaFunction m_ResetActorEffect_hotfix;
    private LuaFunction m_CanUseSkillAtPositionClientBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_FindMoveRegion_hotfix;
    private LuaFunction m_ShowMoveAndAttackRegionBattleActorGridPosition_hotfix;
    private LuaFunction m_ShowMoveAndAttackRegion_hotfix;
    private LuaFunction m_ShowMoveRegionBattleActorGridPosition_hotfix;
    private LuaFunction m_ShowMoveRegion_hotfix;
    private LuaFunction m_UpdateDangerRegion_hotfix;
    private LuaFunction m_ShowDangerTeamRegionInt32_hotfix;
    private LuaFunction m_ShowDangerRegionList`1_hotfix;
    private LuaFunction m_ShowMovePathClientBattleActorGridPosition_hotfix;
    private LuaFunction m_GetSummonMoveTypeInt32_hotfix;
    private LuaFunction m_ShowSkillRegionClientBattleActorInt32_hotfix;
    private LuaFunction m_AddPositionToListInt32Int32List`1_hotfix;
    private LuaFunction m_ShowSkillRegion_hotfix;
    private LuaFunction m_ShowSkillRangeClientBattleActorInt32GridPosition_hotfix;
    private LuaFunction m_ShowSkillRange_hotfix;
    private LuaFunction m_ShowTeleportRegionClientBattleActorInt32GridPosition_hotfix;
    private LuaFunction m_ShowTeleportRegion_hotfix;
    private LuaFunction m_ShowAttackTargetsClientBattleActor_hotfix;
    private LuaFunction m_ShowSkillTargetsClientBattleActorInt32_hotfix;
    private LuaFunction m_ShowSkillTargets_hotfix;
    private LuaFunction m_ShowBattleTreasureDialogConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_ShowBattleTreasureRewardConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_ShowPreCombatBattleActorBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_ShowFastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_ShowActorInfoBattleActor_hotfix;
    private LuaFunction m_HideActorInfo_hotfix;
    private LuaFunction m_ShowSelectionMarkAndTerrainGridPosition_hotfix;
    private LuaFunction m_SetUIStateNone_hotfix;
    private LuaFunction m_SetUIStateSelectActionActor_hotfix;
    private LuaFunction m_SetUIStateMove_hotfix;
    private LuaFunction m_ShowSkills_hotfix;
    private LuaFunction m_SetUIStateExtraMove_hotfix;
    private LuaFunction m_SetUIStateSelectSkillTarget_hotfix;
    private LuaFunction m_SetUIStateConfirmSkill_hotfix;
    private LuaFunction m_SetUIStateSelectTeleportPosition1_hotfix;
    private LuaFunction m_SetUIStateSelectTeleportPosition2_hotfix;
    private LuaFunction m_SetUIStateWaitOtherPlayer_hotfix;
    private LuaFunction m_ShowCanActionActorsUI_hotfix;
    private LuaFunction m_HideCanActionActorsUI_hotfix;
    private LuaFunction m_DoAutoBattle_hotfix;
    private LuaFunction m_SetAutoBattleBoolean_hotfix;
    private LuaFunction m_CameraFocusActorInt32_hotfix;
    private LuaFunction m_BattleUIController_OnAutoBattleBoolean_hotfix;
    private LuaFunction m_BattleUIController_OnFastBattleBoolean_hotfix;
    private LuaFunction m_BattleUIController_OnSkipCombatSkipCombatMode_hotfix;
    private LuaFunction m_BattleUIController_OnShowDangerBoolean_hotfix;
    private LuaFunction m_BattleUIController_OnEndAllAction_hotfix;
    private LuaFunction m_EndAllActionDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_BattleUIController_OnEndAction_hotfix;
    private LuaFunction m_BattleUIController_OnShowActorInfo_hotfix;
    private LuaFunction m_BattleUIController_OnSelectSkillInt32_hotfix;
    private LuaFunction m_BattleUIController_OnUseSkill_hotfix;
    private LuaFunction m_BattleUIController_OnCancelSkill_hotfix;
    private LuaFunction m_BattleUIController_OnShowChat_hotfix;
    private LuaFunction m_BattleUIController_OnShowHelp_hotfix;
    private LuaFunction m_BattleUIController_OnPauseBattle_hotfix;
    private LuaFunction m_BattleUIController_OnShowArmyRelation_hotfix;
    private LuaFunction m_BattleUIController_OnPointerDownInputButtonVector2_hotfix;
    private LuaFunction m_BattleUIController_OnPointerUpInputButtonVector2_hotfix;
    private LuaFunction m_BattleUIController_OnShowCurTurnDanmakuInt32_hotfix;
    private LuaFunction m_BattleUIController_OnCloseCurTurnDanmaku_hotfix;
    private LuaFunction m_BattleUIController_OnShowOneDanmakuDanmakuEntry_hotfix;
    private LuaFunction m_BattleUIController_OnPointerClickInputButtonVector2_hotfix;
    private LuaFunction m_OnBattleMapClickGridPositionBoolean_hotfix;
    private LuaFunction m_ShouldShowActorInfoBattleActor_hotfix;
    private LuaFunction m_BattleUIController_On3DTouchVector2_hotfix;
    private LuaFunction m_PlaySelectHeroSoundBattleActor_hotfix;
    private LuaFunction m_BattleDialogUITask_OnCloseBoolean_hotfix;
    private LuaFunction m_BattleTreasureDialogUIController_OnClose_hotfix;
    private LuaFunction m_GetRewardGoodsUITask_OnStopTask_hotfix;
    private LuaFunction m_GetRewardGoodsUITask_OnClose_hotfix;
    private LuaFunction m_CombatUIController_OnAutoBattleBoolean_hotfix;
    private LuaFunction m_PreCombatUIController_OnOk_hotfix;
    private LuaFunction m_PreCombatUIController_OnCancel_hotfix;
    private LuaFunction m_PreCombatUIController_OnStop_hotfix;
    private LuaFunction m_BattleSceneUIController_OnPointerDownPointerEventData_hotfix;
    private LuaFunction m_BattleSceneUIController_OnPointerUpPointerEventData_hotfix;
    private LuaFunction m_BattleSceneUIController_OnPointerClickPointerEventData_hotfix;
    private LuaFunction m_BattleSceneUIController_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_BattleSceneUIController_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_BattleSceneUIController_OnDragPointerEventData_hotfix;
    private LuaFunction m_BattleSceneUIController_On3DTouchVector2_hotfix;
    private LuaFunction m_SendBattleCommands_hotfix;
    private LuaFunction m_UpdateBattleRoomPlayerHeroStatus_hotfix;
    private LuaFunction m_StartBattleRoomMyActionCountdown_hotfix;
    private LuaFunction m_StopBattleRoomMyActionCountdown_hotfix;
    private LuaFunction m_ActivateBattleRoomMyActionCountdownBoolean_hotfix;
    private LuaFunction m_UpdateBattleRoomMyActionCountdown_hotfix;
    private LuaFunction m_StartBattleRoomOtherActionCountdownInt32_hotfix;
    private LuaFunction m_StopBattleRoomOtherActionCountdown_hotfix;
    private LuaFunction m_ActivateBattleRoomOtherActionCountdownBoolean_hotfix;
    private LuaFunction m_UpdateBattleRoomOtherActionCountdown_hotfix;
    private LuaFunction m_BattleRoomSetAutoBattleBoolean_hotfix;
    private LuaFunction m_ProcessBattlePendingNtfs_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPlayerStatusChangedNtfUInt64_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPlayerQuitNtfUInt64BattleRoomQuitReason_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomBattleCommandExecuteNtfUInt64_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomTeamBattleFinishNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPVPBattleFinishNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomRealTimePVPBattleFinishNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomGuildMassiveCombatBattleFinishNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPVPBattleJoinNtf_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_ReloginUITask_OnReloginSuccess_hotfix;
    private LuaFunction m_GetBattleRoom_hotfix;
    private LuaFunction m_UpdateBattleRoomPlayerHeroCount_hotfix;
    private LuaFunction m_UpdateBattleRoomPrepareTimeout_hotfix;
    private LuaFunction m_UpdateRealtimePVPBattlePrepareTimeout_hotfix;
    private LuaFunction m_UpdateRealtimePVPBattlePrepareStatus_hotfix;
    private LuaFunction m_UpdateStageActorTagBattlePrepareStageActor_hotfix;
    private LuaFunction m_ProcessBattlePreparePendingNtfsBoolean_hotfix;
    private LuaFunction m_LoadAndUpdateBattleRoomStageActorsList`1Boolean_hotfix;
    private LuaFunction m_UpdateBattleRoomStageActorsInt32BattleHeroBoolean_hotfix;
    private LuaFunction m_BattleRoomBattleStart_hotfix;
    private LuaFunction m_PVPBattlePrepareUIController_OnPrepareConfirm_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomHeroSetupNtfList`1_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomDataChangeNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomTeamBattleStartNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPVPBattleStartNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomRealTimePVPBattleStartNtf_hotfix;
    private LuaFunction m_ShowBattleResultBattleTypeInt32BattleRewardBooleanList`1BattleLevelAchievementbe_hotfix;
    private LuaFunction m_ShowBattleLose_hotfix;
    private LuaFunction m_ShowBattleReportEndArenaBattleReport_hotfix;
    private LuaFunction m_ShowBattleReportEndRealTimePVPBattleReport_hotfix;
    private LuaFunction m_BuildBattleReport_hotfix;
    private LuaFunction m_CheckBattleResult_hotfix;
    private LuaFunction m_SendBattleCheatGMCommandBoolean_hotfix;
    private LuaFunction m_BattleResultEnd_hotfix;
    private LuaFunction m_BattleRoomInviteTeammateDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_BattleUIController_OnWinOrLoseEnd_hotfix;
    private LuaFunction m_BattleResultUITask_OnStopTask_hotfix;
    private LuaFunction m_BattleResultUITask_OnClose_hotfix;
    private LuaFunction m_BattleResultScoreUITask_OnStopTask_hotfix;
    private LuaFunction m_BattleResultScoreUITask_OnClose_hotfix;
    private LuaFunction m_BattleLoseUITask_OnStopTask_hotfix;
    private LuaFunction m_BattleLoseUITask_OnClose_hotfix;
    private LuaFunction m_BattleReportEndUITask_OnStopTask_hotfix;
    private LuaFunction m_BattleReportEndUITask_OnClose_hotfix;
    private LuaFunction m_BattleReportEndUITask_OnPlayAgain_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_InitBattleSceneUIController_hotfix;
    private LuaFunction m_UninitBattleSceneUIController_hotfix;
    private LuaFunction m_InitBattleUIController_hotfix;
    private LuaFunction m_UninitBattleUIController_hotfix;
    private LuaFunction m_InitCombatUIController_hotfix;
    private LuaFunction m_UninitCombatUIController_hotfix;
    private LuaFunction m_InitPreCombatUIController_hotfix;
    private LuaFunction m_UninitPreCombatUIController_hotfix;
    private LuaFunction m_InitBattleRoomUIController_hotfix;
    private LuaFunction m_UninitBattleRoomUIController_hotfix;
    private LuaFunction m_InitBattleCommonUIController_hotfix;
    private LuaFunction m_UninitBattleCommonUIController_hotfix;
    private LuaFunction m_InitBattlePrepareUIController_hotfix;
    private LuaFunction m_UninitBattlePrepareUIController_hotfix;
    private LuaFunction m_OnStartBattle_hotfix;
    private LuaFunction m_OnStopBattleBooleanBoolean_hotfix;
    private LuaFunction m_BattleLoseUseRegretDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_OnShowBattleWinConditionConfigDataBattleWinConditionInfoGridPosition_hotfix;
    private LuaFunction m_OnHideBattleWinConditionConfigDataBattleWinConditionInfoGridPosition_hotfix;
    private LuaFunction m_OnShowBattleLoseConditionConfigDataBattleLoseConditionInfoGridPosition_hotfix;
    private LuaFunction m_OnHideBattleLoseConditionConfigDataBattleLoseConditionInfoGridPosition_hotfix;
    private LuaFunction m_OnNextTurnInt32_hotfix;
    private LuaFunction m_OnNextTurnAnimationEndInt32_hotfix;
    private LuaFunction m_OnNextTeamInt32_hotfix;
    private LuaFunction m_OnNextPlayerInt32_hotfix;
    private LuaFunction m_OnNextActorBattleActor_hotfix;
    private LuaFunction m_OnClientActorActiveClientBattleActorBooleanInt32Int32_hotfix;
    private LuaFunction m_OnClientActorMoveClientBattleActor_hotfix;
    private LuaFunction m_OnClientActorTryMoveClientBattleActor_hotfix;
    private LuaFunction m_OnClientActorNoActClientBattleActor_hotfix;
    private LuaFunction m_OnClientActorTargetClientBattleActorConfigDataSkillInfoGridPositionInt32_hotfix;
    private LuaFunction m_OnClientActorSkillClientBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnClientActorSkillEndClientBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnClientActorSkillHitClientBattleActorConfigDataSkillInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnClientActorBuffHitClientBattleActorConfigDataBuffInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnClientActorTerrainHitClientBattleActorConfigDataTerrainInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnClientActorImmuneClientBattleActor_hotfix;
    private LuaFunction m_OnClientActorPassiveSkillClientBattleActorBuffState_hotfix;
    private LuaFunction m_OnClientActorGuardClientBattleActorClientBattleActor_hotfix;
    private LuaFunction m_OnClientActorDieClientBattleActor_hotfix;
    private LuaFunction m_OnClientActorAppearClientBattleActor_hotfix;
    private LuaFunction m_OnCancelCombat_hotfix;
    private LuaFunction m_OnPreStartCombatBattleActorBattleActor_hotfix;
    private LuaFunction m_OnLoadCombatCharImagesBattleActorBattleActorAction_hotfix;
    private LuaFunction m_OnStartCombatBattleActorBattleActorBoolean_hotfix;
    private LuaFunction m_OnPreStopCombat_hotfix;
    private LuaFunction m_OnStopCombat_hotfix;
    private LuaFunction m_OnPrepareFastCombatBattleActorBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnStartFastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_OnCombatActorHitCombatActorCombatActorConfigDataSkillInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnStartSkillCutsceneConfigDataSkillInfoConfigDataCutsceneInfoInt32_hotfix;
    private LuaFunction m_OnStartPassiveSkillCutsceneBuffStateInt32_hotfix;
    private LuaFunction m_OnStopSkillCutscene_hotfix;
    private LuaFunction m_OnStartBattleDialogConfigDataBattleDialogInfo_hotfix;
    private LuaFunction m_BattleDialogUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_OnStartBattleTreasureDialogConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_OnShowBattleTreasureRewardConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_OnStartBattlePerform_hotfix;
    private LuaFunction m_OnStopBattlePerform_hotfix;
    private LuaFunction m_OnScreenEffectString_hotfix;
    private LuaFunction m_StartBattleTeamSetNetTaskBattleTypeInt32List`1_hotfix;
    private LuaFunction m_StartLevelAttackNetTask_hotfix;
    private LuaFunction m_StartLevelWayPointMoveNetTaskConfigDataWaypointInfo_hotfix;
    private LuaFunction m_StartLevelScenarioHandleNetTaskConfigDataScenarioInfo_hotfix;
    private LuaFunction m_StartRiftLevelAttackNetTaskConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_StartHeroDungeonLevelAttackNetTaskConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_StartAnikiLevelAttackNetTaskConfigDataAnikiLevelInfo_hotfix;
    private LuaFunction m_StartThearchyLevelAttackNetTaskConfigDataThearchyTrialLevelInfo_hotfix;
    private LuaFunction m_StartMemoryCorridorLevelAttackNetTaskConfigDataMemoryCorridorLevelInfo_hotfix;
    private LuaFunction m_StartHeroTrainningLevelAttackNetTaskConfigDataHeroTrainningLevelInfo_hotfix;
    private LuaFunction m_StartHeroPhantomLevelAttackNetTaskConfigDataHeroPhantomLevelInfo_hotfix;
    private LuaFunction m_StartTreasureLevelAttackNetTaskConfigDataTreasureLevelInfo_hotfix;
    private LuaFunction m_StartUnchartedScoreLevelAttackNetTaskConfigDataScoreLevelInfo_hotfix;
    private LuaFunction m_StartUnchartedChallengeLevelAttackNetTaskConfigDataChallengeLevelInfo_hotfix;
    private LuaFunction m_StartClimbTowerLevelAttackNetTaskConfigDataTowerFloorInfo_hotfix;
    private LuaFunction m_StartEternalShrineLevelAttackNetTaskConfigDataEternalShrineLevelInfo_hotfix;
    private LuaFunction m_StartCollectionLevelAttackNetTaskGameFunctionTypeInt32_hotfix;
    private LuaFunction m_StartGuildMassiveCombatAttackNetTaskConfigDataGuildMassiveCombatLevelInfoList`1_hotfix;
    private LuaFunction m_StartArenaOpponentAttackFightingNetTask_hotfix;
    private LuaFunction m_HandleLevelAttackNetTaskResultInt32_hotfix;
    private LuaFunction m_StartLevelFinishedNetTask_hotfix;
    private LuaFunction m_StartWayPointBattleFinishedNetTaskConfigDataWaypointInfo_hotfix;
    private LuaFunction m_StartRiftLevelBattleFinishedNetTaskConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_StartHeroDungeonLevelBattleFinishedNetTaskConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_StartAnikiLevelBattleFinishedNetTaskConfigDataAnikiLevelInfo_hotfix;
    private LuaFunction m_StartThearchyLevelBattleFinishedNetTaskConfigDataThearchyTrialLevelInfo_hotfix;
    private LuaFunction m_StartTreasureLevelBattleFinishedNetTaskConfigDataTreasureLevelInfo_hotfix;
    private LuaFunction m_StartMemoryCorridorLevelBattleFinishedNetTaskConfigDataMemoryCorridorLevelInfo_hotfix;
    private LuaFunction m_StartHeroTrainningLevelBattleFinishedNetTaskConfigDataHeroTrainningLevelInfo_hotfix;
    private LuaFunction m_StartHeroPhantomLevelBattleFinishedNetTaskConfigDataHeroPhantomLevelInfo_hotfix;
    private LuaFunction m_StartUnchartedScoreLevelBattleFinishedNetTaskConfigDataScoreLevelInfo_hotfix;
    private LuaFunction m_StartUnchartedChallengeLevelBattleFinishedNetTaskConfigDataChallengeLevelInfo_hotfix;
    private LuaFunction m_StartClimbTowerLevelBattleFinishedNetTaskConfigDataTowerFloorInfo_hotfix;
    private LuaFunction m_StartEternalShrineLevelBattleFinishedNetTaskConfigDataEternalShrineLevelInfo_hotfix;
    private LuaFunction m_StartCollectionLevelFinishNetTaskGameFunctionTypeInt32_hotfix;
    private LuaFunction m_StartGuildMassiveCombatAttackFinishedNetTaskConfigDataGuildMassiveCombatLevelInfo_hotfix;
    private LuaFunction m_StartArenaBattleFinishedNetTask_hotfix;
    private LuaFunction m_HandleBattleFinishedNetTaskResultInt32BooleanBattleRewardInt32BooleanList`1BattleLevelAchievementbe_hotfix;
    private LuaFunction m_StartBattleCancelNetTask_hotfix;
    private LuaFunction m_StartDanmakuPostNetTaskAction`1_hotfix;
    private LuaFunction m_StartBattleRoomHeroSetupNetTaskInt32Int32_hotfix;
    private LuaFunction m_StartBattleRoomHeroSwapNetTaskInt32Int32_hotfix;
    private LuaFunction m_StartBattleRoomHeroSetoffNetTaskInt32_hotfix;
    private LuaFunction m_StartBattleRoomPlayerStatusChangeNetTaskPlayerBattleStatusAction_hotfix;
    private LuaFunction m_StartBattleRoomQuitNetTask_hotfix;
    private LuaFunction m_StartBattleRoomEndCurrentBPTurnNetTaskAction_hotfix;
    private LuaFunction m_StartBattleRoomPlayerActionBeginNetTask_hotfix;
    private LuaFunction m_PrepareBattleUI_hotfix;
    private LuaFunction m_SetupReachRegionBoolean_hotfix;
    private LuaFunction m_SetupBattlePauseUIControllerList`1_hotfix;
    private LuaFunction m_SetupBattlePauseUIAchievements_hotfix;
    private LuaFunction m_GetWinConditionTargetPositionConfigDataBattleWinConditionInfo_hotfix;
    private LuaFunction m_GetMyStageActorCountMax_hotfix;
    private LuaFunction m_SetupMyHerosConfigDataBattleInfoInt32Boolean_hotfix;
    private LuaFunction m_FillMyHerosList`1Int32List`1_hotfix;
    private LuaFunction m_SetupStageActorsBoolean_hotfix;
    private LuaFunction m_AddUseableHeros_hotfix;
    private LuaFunction m_GetTowerBattleRuleInfo_hotfix;
    private LuaFunction m_GetTowerBonusHeroGroupInfo_hotfix;
    private LuaFunction m_IsTowerPowerUpHeroBattleHero_hotfix;
    private LuaFunction m_GetGuildPlayerMassiveCombatInfo_hotfix;
    private LuaFunction m_GetGuildMassiveCombatPreferredHeroTagIds_hotfix;
    private LuaFunction m_IsGuildMassiveCombatCampUpHeroBattleHero_hotfix;
    private LuaFunction m_IsEternalShrineCampUpHeroBattleHero_hotfix;
    private LuaFunction m_IsCollectionActivityDropUpHeroBattleHero_hotfix;
    private LuaFunction m_GetHeroTagTypeBattleHero_hotfix;
    private LuaFunction m_LoadArenaAttackerHeroActionValueBattleHero_hotfix;
    private LuaFunction m_ShowStagePositions_hotfix;
    private LuaFunction m_SetupBattlePrepareTreasures_hotfix;
    private LuaFunction m_BuildBattleTeamSetups_hotfix;
    private LuaFunction m_ModifyBattleTeamSetups_hotfix;
    private LuaFunction m_BuildBattleTeamSetupInt32Boolean_hotfix;
    private LuaFunction m_GetSoldierCountInt32_hotfix;
    private LuaFunction m_SetTeamAndStartBattle_hotfix;
    private LuaFunction m_StartBattlePrepareLoadState_hotfix;
    private LuaFunction m_StartBattleLoadState_hotfix;
    private LuaFunction m_ShowMoveAndAttackRegion_PrepareGridPosition_hotfix;
    private LuaFunction m_UpdateArenaAttackerHeroIds_hotfix;
    private LuaFunction m_GetPlayerTrainingTechsInt32Int32Boolean_hotfix;
    private LuaFunction m_GetPlayerLevelInt32Int32_hotfix;
    private LuaFunction m_GetPlayerSessionIdInt32_hotfix;
    private LuaFunction m_GetMyPlayerIndex_hotfix;
    private LuaFunction m_BattlePrepareCanChangeSkill_hotfix;
    private LuaFunction m_IsBattlePrepareDisableCameraMove_hotfix;
    private LuaFunction m_UpdateBattlePower_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnPauseBattle_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnShowArmyRelation_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnStart_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnShowRecommend_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnShowActionOrder_hotfix;
    private LuaFunction m_HeroNotFullDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnTestOnStage_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnShowMyActorInfoBattleHero_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnHideActorInfo_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnStageActorChange_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnHeroOnStageBattleHeroGridPositionInt32_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnActorOffStageBattlePrepareStageActor_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnStageActorMoveBattlePrepareStageActorGridPosition_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnStageActorSwapBattlePrepareStageActorBattlePrepareStageActor_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnUpdateBattlePower_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnShowChat_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnShowHelp_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnPointerDownInputButtonVector2_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnPointerUpInputButtonVector2_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnPointerClickInputButtonVector2_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnBeginDragHero_hotfix;
    private LuaFunction m_BattlePrepareUIController_OnEndDragHero_hotfix;
    private LuaFunction m_BattlePrepareActorInfoUIController_OnShowSelectSoldierPanelBattleHero_hotfix;
    private LuaFunction m_BattlePrepareActorInfoUIController_OnChangeSkillBattleHeroList`1_hotfix;
    private LuaFunction m_BattlePrepareActorInfoUIController_OnChangeSoldierBattleHeroInt32_hotfix;
    private LuaFunction m_ActionOrderUIController_OnConfirm_hotfix;
    private LuaFunction m_SetUIStateRegret_hotfix;
    private LuaFunction m_UpdateRegretButton_hotfix;
    private LuaFunction m_ClearRegret_hotfix;
    private LuaFunction m_IsRegretActive_hotfix;
    private LuaFunction m_CanUseRegret_hotfix;
    private LuaFunction m_RegretGotoStepInt32_hotfix;
    private LuaFunction m_FindRegretStepIndexByStepInt32_hotfix;
    private LuaFunction m_FindRegretStepIndexByTurnInt32_hotfix;
    private LuaFunction m_ActivateRegret_hotfix;
    private LuaFunction m_BattleUIController_OnRegretActive_hotfix;
    private LuaFunction m_BattleUIController_OnRegretConfirm_hotfix;
    private LuaFunction m_BattleUIController_OnRegretCancel_hotfix;
    private LuaFunction m_BattleUIController_OnRegretBackward_hotfix;
    private LuaFunction m_BattleUIController_OnRegretForward_hotfix;
    private LuaFunction m_BattleUIController_OnRegretPrevTurn_hotfix;
    private LuaFunction m_BattleUIController_OnRegretNextTurn_hotfix;
    private LuaFunction m_UserGuide_GetEnforceHeros_hotfix;
    private LuaFunction m_SetUserGuideBattleSettings_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LogCurrentBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StopSubUITask(System.Type taskType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLoading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrepareClientBattle(bool prepareBattleUI = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RebuildClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBattle(bool win, bool skipPerform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleRoomInitLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleTeamSetup GetBattleTeamSetup(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GridPosition> GetTeamPositions(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCombatHp(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExitBattleReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void UnloadAssetsAndStartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void WorldUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowErrorMessageAndExitBattle(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowErrorMessageAndExitBattle(StringTableId strId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareBeforeShowResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsMeArenaBattleTeam1()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    public ClientBattle ClientBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePauseUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePauseUIController_OnShowPlayerSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePauseUIController_OnExit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExitBattleDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_ExitBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_RestartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_ReplayBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_PrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatMessageNtf(ChatMessage msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInviteNtf(TeamRoomInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattlePracticeInvitedNtf(PVPInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlePrepareAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlePrepareHeros(List<BattleHero> heros0, List<BattleHero> heros1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectTerrainAssets(ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectCharImageAssets(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectCharSpineAssets(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAssets(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo heroSkinResInfo,
      int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSoldierAssets(
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSkinAssets(ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSkillAssets(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffAssets(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectDefaultHeroAssets(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleEventAssets(ConfigDataBattleEventTriggerInfo eventTriggerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleEventActionAssets(ConfigDataBattleEventActionInfo eventActionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlePerformAssets(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _CollectBattlePerformAssets(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectTrainingTechAssets(List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAlreadyCollectAsset(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAlreadyCollectAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClientActorTryMoveTo(ClientBattleActor ca, GridPosition p, int finalDir = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCommandIfMoved(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindAttackPosition(int attackDistance, GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor GetMapBattleActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetActorEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseSkillAtPosition(
      ClientBattleActor ca,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindMoveRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveAndAttackRegion(BattleActor actor, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveAndAttackRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveRegion(BattleActor actor, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDangerRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDangerTeamRegion(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDangerRegion(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMovePath(ClientBattleActor ca, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MoveType GetSummonMoveType(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillRegion(ClientBattleActor ca, int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPositionToList(int x, int y, List<GridPosition> positionList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillRange(ClientBattleActor ca, int skillIndex, GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillRange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTeleportRegion(ClientBattleActor ca, int skillIndex, GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTeleportRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAttackTargets(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargets(ClientBattleActor ca, int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPreCombat(
      BattleActor a0,
      BattleActor a1,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowActorInfo(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSelectionMarkAndTerrain(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateNone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectActionActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateExtraMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectSkillTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateConfirmSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectTeleportPosition1()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectTeleportPosition2()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateWaitOtherPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCanActionActorsUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideCanActionActorsUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoAutoBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CameraFocusActor(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnFastBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnSkipCombat(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowDanger(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnEndAllAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EndAllActionDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnEndAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnSelectSkill(int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnUseSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnCancelSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowCurTurnDanmaku(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnCloseCurTurnDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowOneDanmaku(DanmakuEntry danmaku)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleMapClick(GridPosition p, bool isDoubleClick = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ShouldShowActorInfo(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_On3DTouch(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySelectHeroSound(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnClose(bool isSkip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleTreasureDialogUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CombatUIController_OnAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreCombatUIController_OnOk()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreCombatUIController_OnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreCombatUIController_OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_On3DTouch(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomPlayerHeroStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBattleRoomMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivateBattleRoomMyActionCountdown(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomOtherActionCountdown(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBattleRoomOtherActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivateBattleRoomOtherActionCountdown(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomOtherActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomSetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessBattlePendingNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPlayerStatusChangedNtf(ulong playerSessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPlayerQuitNtf(
      ulong playerSessionId,
      BattleRoomQuitReason reason)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomBattleCommandExecuteNtf(ulong playerSessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomTeamBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomRealTimePVPBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomGuildMassiveCombatBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReloginUITask_OnReloginSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleRoom GetBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomPlayerHeroCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomPrepareTimeout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRealtimePVPBattlePrepareTimeout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRealtimePVPBattlePrepareStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageActorTag(BattlePrepareStageActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessBattlePreparePendingNtfs(bool playFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadAndUpdateBattleRoomStageActors(List<int> posList, bool playFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomStageActors(int posIdx, BattleHero hero, bool playFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PVPBattlePrepareUIController_OnPrepareConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomHeroSetupNtf(List<int> posList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomDataChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomTeamBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomRealTimePVPBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleResult(
      BattleType battleType,
      int stars,
      BattleReward battleReward,
      bool isFirstWin,
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleLose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleReportEnd(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleReportEnd(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ProBattleReport BuildBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckBattleResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleCheatGMCommand(bool isCheat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomInviteTeammateDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnWinOrLoseEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultScoreUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultScoreUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLoseUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLoseUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPlayAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPreCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitPreCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopBattle(bool win, bool skipPerform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLoseUseRegretDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnShowBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHideBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnShowBattleLoseCondition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHideBattleLoseCondition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextTurnAnimationEnd(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextTeam(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextPlayer(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorActive(ClientBattleActor a, bool newStep, int step, int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorMove(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorTryMove(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorNoAct(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorTarget(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition pos,
      int armyRelationValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorSkill(ClientBattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorSkillEnd(ClientBattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorSkillHit(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorBuffHit(
      ClientBattleActor a,
      ConfigDataBuffInfo buffInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorTerrainHit(
      ClientBattleActor a,
      ConfigDataTerrainInfo terrainInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorImmune(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorPassiveSkill(ClientBattleActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorGuard(ClientBattleActor a, ClientBattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorDie(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorAppear(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCancelCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPreStartCombat(BattleActor a, BattleActor b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnLoadCombatCharImages(BattleActor a, BattleActor b, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartCombat(BattleActor a, BattleActor b, bool splitScreen)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPrepareFastCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartFastCombat(FastCombatActorInfo a, FastCombatActorInfo b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopSkillCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnShowBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public static event Action<ClientBattleActor> m_onActiveActorEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<ClientBattleActor> m_onDeactiveActorEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleTeamSetNetTask(BattleType battleType, int battleId, List<int> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelAttackNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelWayPointMoveNetTask(ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelScenarioHandleNetTask(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftLevelAttackNetTask(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelAttackNetTask(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAnikiLevelAttackNetTask(ConfigDataAnikiLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartThearchyLevelAttackNetTask(ConfigDataThearchyTrialLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMemoryCorridorLevelAttackNetTask(ConfigDataMemoryCorridorLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroTrainningLevelAttackNetTask(ConfigDataHeroTrainningLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroPhantomLevelAttackNetTask(ConfigDataHeroPhantomLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTreasureLevelAttackNetTask(ConfigDataTreasureLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedScoreLevelAttackNetTask(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedChallengeLevelAttackNetTask(ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerLevelAttackNetTask(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartEternalShrineLevelAttackNetTask(ConfigDataEternalShrineLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionLevelAttackNetTask(GameFunctionType levelType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildMassiveCombatAttackNetTask(
      ConfigDataGuildMassiveCombatLevelInfo levelInfo,
      List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaOpponentAttackFightingNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleLevelAttackNetTaskResult(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelFinishedNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWayPointBattleFinishedNetTask(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftLevelBattleFinishedNetTask(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelBattleFinishedNetTask(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAnikiLevelBattleFinishedNetTask(ConfigDataAnikiLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartThearchyLevelBattleFinishedNetTask(ConfigDataThearchyTrialLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTreasureLevelBattleFinishedNetTask(ConfigDataTreasureLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMemoryCorridorLevelBattleFinishedNetTask(
      ConfigDataMemoryCorridorLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroTrainningLevelBattleFinishedNetTask(
      ConfigDataHeroTrainningLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroPhantomLevelBattleFinishedNetTask(ConfigDataHeroPhantomLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedScoreLevelBattleFinishedNetTask(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedChallengeLevelBattleFinishedNetTask(
      ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerLevelBattleFinishedNetTask(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartEternalShrineLevelBattleFinishedNetTask(
      ConfigDataEternalShrineLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionLevelFinishNetTask(GameFunctionType levelType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildMassiveCombatAttackFinishedNetTask(
      ConfigDataGuildMassiveCombatLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleFinishedNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBattleFinishedNetTaskResult(
      int result,
      bool isWin,
      BattleReward reward,
      int stars = 0,
      bool isFirstWin = false,
      List<int> gotAchievements = null,
      BattleLevelAchievement[] achievements = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleCancelNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDanmakuPostNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomHeroSetupNetTask(int heroId, int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomHeroSwapNetTask(int position1, int position2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomHeroSetoffNetTask(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomPlayerStatusChangeNetTask(PlayerBattleStatus status, Action onOk)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomQuitNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomEndCurrentBPTurnNetTask(Action onOk)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomPlayerActionBeginNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattleUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GridPosition> SetupReachRegion(bool isSkipBattlePrepare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupBattlePauseUIController(List<GridPosition> reachRegion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupBattlePauseUIAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetWinConditionTargetPosition(
      ConfigDataBattleWinConditionInfo winConditionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMyStageActorCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> SetupMyHeros(
      ConfigDataBattleInfo battleInfo,
      int number,
      bool isSkipBattlePrepare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FillMyHeros(List<int> heroIds, int count, List<int> disableHeroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupStageActors(bool isSkipBattlePrepare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddUseableHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ConfigDataUnchartedScoreInfo GetUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetHeroUnchartedScoreBonus(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataTowerBattleRuleInfo GetTowerBattleRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataTowerBonusHeroGroupInfo GetTowerBonusHeroGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTowerPowerUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GuildPlayerMassiveCombatInfo GetGuildPlayerMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> GetGuildMassiveCombatPreferredHeroTagIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombatCampUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEternalShrineCampUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsCollectionActivityDropUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StageActorTagType GetHeroTagType(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadArenaAttackerHeroActionValue(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CompareHero(BattleHero h1, BattleHero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroUnchartdScoreBonus(BattleHero h1, BattleHero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupBattlePrepareTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsListElementsEqual(List<int> list0, List<int> list1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BuildBattleTeamSetups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ModifyBattleTeamSetups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BuildBattleTeamSetup(int team, bool saveHeroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSoldierCount(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTeamAndStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattlePrepareLoadState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleLoadState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveAndAttackRegion_Prepare(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateArenaAttackerHeroIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<TrainingTech> GetPlayerTrainingTechs(
      int team,
      int playerIndex,
      bool isNpc = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPlayerLevel(int team, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ulong GetPlayerSessionId(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMyPlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool BattlePrepareCanChangeSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBattlePrepareDisableCameraMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowRecommend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroNotFullDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnTestOnStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowMyActorInfo(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnHideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnHeroOnStage(
      BattleHero hero,
      GridPosition pos,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnActorOffStage(BattlePrepareStageActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorMove(
      BattlePrepareStageActor sa,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorSwap(
      BattlePrepareStageActor sa1,
      BattlePrepareStageActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnUpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattlePrepareUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnBeginDragHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnEndDragHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSkill(
      BattleHero hero,
      List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSoldier(BattleHero hero, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionOrderUIController_OnConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRegretButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRegretActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegretGotoStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindRegretStepIndexByStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindRegretStepIndexByTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivateRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretBackward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretForward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretPrevTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretNextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> UserGuide_GetEnforceHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUserGuideBattleSettings()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_m_onActiveActorEvent(ClientBattleActor obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_m_onActiveActorEvent(ClientBattleActor obj)
    {
      BattleUITask.m_onActiveActorEvent = (Action<ClientBattleActor>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_m_onDeactiveActorEvent(ClientBattleActor obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_m_onDeactiveActorEvent(ClientBattleActor obj)
    {
      BattleUITask.m_onDeactiveActorEvent = (Action<ClientBattleActor>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public struct RegretStep
    {
      public int Step;
      public int Turn;
      public int ActorId;

      [MethodImpl((MethodImplOptions) 32768)]
      public RegretStep(int step, int turn, int actorId)
      {
      }
    }

    public class LuaExportHelper
    {
      private BattleUITask m_owner;

      public LuaExportHelper(BattleUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public static void __callDele_m_onActiveActorEvent(ClientBattleActor obj)
      {
        BattleUITask.__callDele_m_onActiveActorEvent(obj);
      }

      public static void __clearDele_m_onActiveActorEvent(ClientBattleActor obj)
      {
        BattleUITask.__clearDele_m_onActiveActorEvent(obj);
      }

      public static void __callDele_m_onDeactiveActorEvent(ClientBattleActor obj)
      {
        BattleUITask.__callDele_m_onDeactiveActorEvent(obj);
      }

      public static void __clearDele_m_onDeactiveActorEvent(ClientBattleActor obj)
      {
        BattleUITask.__clearDele_m_onDeactiveActorEvent(obj);
      }

      public static int BattleDanmakuLayerIndex
      {
        get
        {
          return 0;
        }
      }

      public static int BattleRoomLayerIndex
      {
        get
        {
          return 1;
        }
      }

      public static int BattleLayerIndex
      {
        get
        {
          return 2;
        }
      }

      public static int BattleActorInfoLayerIndex
      {
        get
        {
          return 3;
        }
      }

      public static int BattlePrepareLayerIndex
      {
        get
        {
          return 4;
        }
      }

      public static int PVPBattlePrepareLayerIndex
      {
        get
        {
          return 5;
        }
      }

      public static int BattlePrepareActorInfoLayerIndex
      {
        get
        {
          return 6;
        }
      }

      public static int ActionOrderLayerIndex
      {
        get
        {
          return 7;
        }
      }

      public static int BattlePauseLayerIndex
      {
        get
        {
          return 8;
        }
      }

      public static int BattleTreasureDialogLayerIndex
      {
        get
        {
          return 9;
        }
      }

      public static int CombatLayerIndex
      {
        get
        {
          return 10;
        }
      }

      public static int PreCombatLayerIndex
      {
        get
        {
          return 11;
        }
      }

      public static int BattleCommonLayerIndex
      {
        get
        {
          return 12;
        }
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public BattleUIController m_battleUIController
      {
        get
        {
          return this.m_owner.m_battleUIController;
        }
        set
        {
          this.m_owner.m_battleUIController = value;
        }
      }

      public BattleDanmakuUIController m_battleDanmakuUIController
      {
        get
        {
          return this.m_owner.m_battleDanmakuUIController;
        }
        set
        {
          this.m_owner.m_battleDanmakuUIController = value;
        }
      }

      public BattleActorInfoUIController m_battleActorInfoUIController
      {
        get
        {
          return this.m_owner.m_battleActorInfoUIController;
        }
        set
        {
          this.m_owner.m_battleActorInfoUIController = value;
        }
      }

      public BattlePrepareUIController m_battlePrepareUIController
      {
        get
        {
          return this.m_owner.m_battlePrepareUIController;
        }
        set
        {
          this.m_owner.m_battlePrepareUIController = value;
        }
      }

      public PVPBattlePrepareUIController m_pvpBattlePrepareUIController
      {
        get
        {
          return this.m_owner.m_pvpBattlePrepareUIController;
        }
        set
        {
          this.m_owner.m_pvpBattlePrepareUIController = value;
        }
      }

      public BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController
      {
        get
        {
          return this.m_owner.m_battlePrepareActorInfoUIController;
        }
        set
        {
          this.m_owner.m_battlePrepareActorInfoUIController = value;
        }
      }

      public ActionOrderUIController m_actionOrderUIController
      {
        get
        {
          return this.m_owner.m_actionOrderUIController;
        }
        set
        {
          this.m_owner.m_actionOrderUIController = value;
        }
      }

      public BattlePauseUIController m_battlePauseUIController
      {
        get
        {
          return this.m_owner.m_battlePauseUIController;
        }
        set
        {
          this.m_owner.m_battlePauseUIController = value;
        }
      }

      public BattleTreasureDialogUIController m_battleTreasureDialogUIController
      {
        get
        {
          return this.m_owner.m_battleTreasureDialogUIController;
        }
        set
        {
          this.m_owner.m_battleTreasureDialogUIController = value;
        }
      }

      public CombatUIController m_combatUIController
      {
        get
        {
          return this.m_owner.m_combatUIController;
        }
        set
        {
          this.m_owner.m_combatUIController = value;
        }
      }

      public PreCombatUIController m_preCombatUIController
      {
        get
        {
          return this.m_owner.m_preCombatUIController;
        }
        set
        {
          this.m_owner.m_preCombatUIController = value;
        }
      }

      public BattleRoomUIController m_battleRoomUIController
      {
        get
        {
          return this.m_owner.m_battleRoomUIController;
        }
        set
        {
          this.m_owner.m_battleRoomUIController = value;
        }
      }

      public BattleCommonUIController m_battleCommonUIController
      {
        get
        {
          return this.m_owner.m_battleCommonUIController;
        }
        set
        {
          this.m_owner.m_battleCommonUIController = value;
        }
      }

      public BattleMapUIController m_battleMapUIController
      {
        get
        {
          return this.m_owner.m_battleMapUIController;
        }
        set
        {
          this.m_owner.m_battleMapUIController = value;
        }
      }

      public BattleSceneUIController m_battleSceneUIController
      {
        get
        {
          return this.m_owner.m_battleSceneUIController;
        }
        set
        {
          this.m_owner.m_battleSceneUIController = value;
        }
      }

      public CombatSceneUIController m_combatSceneUIController
      {
        get
        {
          return this.m_owner.m_combatSceneUIController;
        }
        set
        {
          this.m_owner.m_combatSceneUIController = value;
        }
      }

      public GetRewardGoodsUITask m_getRewardGoodsUITask
      {
        get
        {
          return this.m_owner.m_getRewardGoodsUITask;
        }
        set
        {
          this.m_owner.m_getRewardGoodsUITask = value;
        }
      }

      public BattleDialogUITask m_battleDialogUITask
      {
        get
        {
          return this.m_owner.m_battleDialogUITask;
        }
        set
        {
          this.m_owner.m_battleDialogUITask = value;
        }
      }

      public BattleResultUITask m_battleResultUITask
      {
        get
        {
          return this.m_owner.m_battleResultUITask;
        }
        set
        {
          this.m_owner.m_battleResultUITask = value;
        }
      }

      public BattleResultScoreUITask m_battleResultScoreUITask
      {
        get
        {
          return this.m_owner.m_battleResultScoreUITask;
        }
        set
        {
          this.m_owner.m_battleResultScoreUITask = value;
        }
      }

      public BattleLoseUITask m_battleLoseUITask
      {
        get
        {
          return this.m_owner.m_battleLoseUITask;
        }
        set
        {
          this.m_owner.m_battleLoseUITask = value;
        }
      }

      public BattleReportEndUITask m_battleReportEndUITask
      {
        get
        {
          return this.m_owner.m_battleReportEndUITask;
        }
        set
        {
          this.m_owner.m_battleReportEndUITask = value;
        }
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public BattleUIState m_uiState
      {
        get
        {
          return this.m_owner.m_uiState;
        }
        set
        {
          this.m_owner.m_uiState = value;
        }
      }

      public ClientBattleActor m_activeActor
      {
        get
        {
          return this.m_owner.m_activeActor;
        }
        set
        {
          this.m_owner.m_activeActor = value;
        }
      }

      public GridPosition m_activeActorInitPosition
      {
        get
        {
          return this.m_owner.m_activeActorInitPosition;
        }
        set
        {
          this.m_owner.m_activeActorInitPosition = value;
        }
      }

      public int m_activeActorInitDirection
      {
        get
        {
          return this.m_owner.m_activeActorInitDirection;
        }
        set
        {
          this.m_owner.m_activeActorInitDirection = value;
        }
      }

      public int m_skillIndex
      {
        get
        {
          return this.m_owner.m_skillIndex;
        }
        set
        {
          this.m_owner.m_skillIndex = value;
        }
      }

      public GridPosition m_skillTargetPosition
      {
        get
        {
          return this.m_owner.m_skillTargetPosition;
        }
        set
        {
          this.m_owner.m_skillTargetPosition = value;
        }
      }

      public GridPosition m_skillTargetPosition2
      {
        get
        {
          return this.m_owner.m_skillTargetPosition2;
        }
        set
        {
          this.m_owner.m_skillTargetPosition2 = value;
        }
      }

      public GridPosition m_combatStartPosition
      {
        get
        {
          return this.m_owner.m_combatStartPosition;
        }
        set
        {
          this.m_owner.m_combatStartPosition = value;
        }
      }

      public GridPosition m_combatTargetPosition
      {
        get
        {
          return this.m_owner.m_combatTargetPosition;
        }
        set
        {
          this.m_owner.m_combatTargetPosition = value;
        }
      }

      public bool m_isBattleCutsceneFade
      {
        get
        {
          return this.m_owner.m_isBattleCutsceneFade;
        }
        set
        {
          this.m_owner.m_isBattleCutsceneFade = value;
        }
      }

      public bool m_isCombatCutsceneFade
      {
        get
        {
          return this.m_owner.m_isCombatCutsceneFade;
        }
        set
        {
          this.m_owner.m_isCombatCutsceneFade = value;
        }
      }

      public DateTime m_battleMapClickTime
      {
        get
        {
          return this.m_owner.m_battleMapClickTime;
        }
        set
        {
          this.m_owner.m_battleMapClickTime = value;
        }
      }

      public int m_showDangerRegionTeam
      {
        get
        {
          return this.m_owner.m_showDangerRegionTeam;
        }
        set
        {
          this.m_owner.m_showDangerRegionTeam = value;
        }
      }

      public List<int> m_showDangerRegionActorIds
      {
        get
        {
          return this.m_owner.m_showDangerRegionActorIds;
        }
        set
        {
          this.m_owner.m_showDangerRegionActorIds = value;
        }
      }

      public BattleActor m_preCombatTargetActor
      {
        get
        {
          return this.m_owner.m_preCombatTargetActor;
        }
        set
        {
          this.m_owner.m_preCombatTargetActor = value;
        }
      }

      public bool m_saveShowTopUI
      {
        get
        {
          return this.m_owner.m_saveShowTopUI;
        }
        set
        {
          this.m_owner.m_saveShowTopUI = value;
        }
      }

      public bool m_saveShowBottomUI
      {
        get
        {
          return this.m_owner.m_saveShowBottomUI;
        }
        set
        {
          this.m_owner.m_saveShowBottomUI = value;
        }
      }

      public bool m_disableSaveProcessingBattle
      {
        get
        {
          return this.m_owner.m_disableSaveProcessingBattle;
        }
        set
        {
          this.m_owner.m_disableSaveProcessingBattle = value;
        }
      }

      public BattleTeamSetup m_battleTeamSetup0
      {
        get
        {
          return this.m_owner.m_battleTeamSetup0;
        }
        set
        {
          this.m_owner.m_battleTeamSetup0 = value;
        }
      }

      public BattleTeamSetup m_battleTeamSetup1
      {
        get
        {
          return this.m_owner.m_battleTeamSetup1;
        }
        set
        {
          this.m_owner.m_battleTeamSetup1 = value;
        }
      }

      public List<GridPosition> m_teamPositions0
      {
        get
        {
          return this.m_owner.m_teamPositions0;
        }
        set
        {
          this.m_owner.m_teamPositions0 = value;
        }
      }

      public List<GridPosition> m_teamPositions1
      {
        get
        {
          return this.m_owner.m_teamPositions1;
        }
        set
        {
          this.m_owner.m_teamPositions1 = value;
        }
      }

      public List<GridPosition> m_teamNpcPositions0
      {
        get
        {
          return this.m_owner.m_teamNpcPositions0;
        }
        set
        {
          this.m_owner.m_teamNpcPositions0 = value;
        }
      }

      public List<BattleHero> m_playerBattleHeros
      {
        get
        {
          return this.m_owner.m_playerBattleHeros;
        }
        set
        {
          this.m_owner.m_playerBattleHeros = value;
        }
      }

      public List<int> m_tempIntList
      {
        get
        {
          return this.m_owner.m_tempIntList;
        }
        set
        {
          this.m_owner.m_tempIntList = value;
        }
      }

      public List<string> m_tempStringList
      {
        get
        {
          return this.m_owner.m_tempStringList;
        }
        set
        {
          this.m_owner.m_tempStringList = value;
        }
      }

      public HashSet<GridPosition> m_dangerRegion
      {
        get
        {
          return this.m_owner.m_dangerRegion;
        }
        set
        {
          this.m_owner.m_dangerRegion = value;
        }
      }

      public List<TrainingTech> m_trainingTechs
      {
        get
        {
          return this.m_owner.m_trainingTechs;
        }
        set
        {
          this.m_owner.m_trainingTechs = value;
        }
      }

      public List<TrainingTech> m_tempTrainingTechs
      {
        get
        {
          return this.m_owner.m_tempTrainingTechs;
        }
        set
        {
          this.m_owner.m_tempTrainingTechs = value;
        }
      }

      public List<Goods> m_tempGoodsList
      {
        get
        {
          return this.m_owner.m_tempGoodsList;
        }
        set
        {
          this.m_owner.m_tempGoodsList = value;
        }
      }

      public List<int> m_userGuideEnforceHeroIds
      {
        get
        {
          return this.m_owner.m_userGuideEnforceHeroIds;
        }
        set
        {
          this.m_owner.m_userGuideEnforceHeroIds = value;
        }
      }

      public List<int> m_arenaAttackerHeroIds
      {
        get
        {
          return this.m_owner.m_arenaAttackerHeroIds;
        }
        set
        {
          this.m_owner.m_arenaAttackerHeroIds = value;
        }
      }

      public List<int> m_myBattleHeroIds
      {
        get
        {
          return this.m_owner.m_myBattleHeroIds;
        }
        set
        {
          this.m_owner.m_myBattleHeroIds = value;
        }
      }

      public BattleLoadState m_loadState
      {
        get
        {
          return this.m_owner.m_loadState;
        }
        set
        {
          this.m_owner.m_loadState = value;
        }
      }

      public BattlePerformState m_battlePerformState
      {
        get
        {
          return this.m_owner.m_battlePerformState;
        }
        set
        {
          this.m_owner.m_battlePerformState = value;
        }
      }

      public bool m_isStartBattleAutoBattle
      {
        get
        {
          return this.m_owner.m_isStartBattleAutoBattle;
        }
        set
        {
          this.m_owner.m_isStartBattleAutoBattle = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public DateTime m_myActionTimeout
      {
        get
        {
          return this.m_owner.m_myActionTimeout;
        }
        set
        {
          this.m_owner.m_myActionTimeout = value;
        }
      }

      public DateTime m_otherActionTimeout
      {
        get
        {
          return this.m_owner.m_otherActionTimeout;
        }
        set
        {
          this.m_owner.m_otherActionTimeout = value;
        }
      }

      public bool m_isMyActionTimeoutActive
      {
        get
        {
          return this.m_owner.m_isMyActionTimeoutActive;
        }
        set
        {
          this.m_owner.m_isMyActionTimeoutActive = value;
        }
      }

      public bool m_isActionTimeoutAutoBattle
      {
        get
        {
          return this.m_owner.m_isActionTimeoutAutoBattle;
        }
        set
        {
          this.m_owner.m_isActionTimeoutAutoBattle = value;
        }
      }

      public bool m_isAutoBattleOnce
      {
        get
        {
          return this.m_owner.m_isAutoBattleOnce;
        }
        set
        {
          this.m_owner.m_isAutoBattleOnce = value;
        }
      }

      public List<int> m_pendingHeroSetupNtfs
      {
        get
        {
          return this.m_owner.m_pendingHeroSetupNtfs;
        }
        set
        {
          this.m_owner.m_pendingHeroSetupNtfs = value;
        }
      }

      public GridPosition m_selectProtectHeroPos
      {
        get
        {
          return this.m_owner.m_selectProtectHeroPos;
        }
        set
        {
          this.m_owner.m_selectProtectHeroPos = value;
        }
      }

      public GridPosition m_selectBanHeroPos
      {
        get
        {
          return this.m_owner.m_selectBanHeroPos;
        }
        set
        {
          this.m_owner.m_selectBanHeroPos = value;
        }
      }

      public List<object> m_collectAssetObjects
      {
        get
        {
          return this.m_owner.m_collectAssetObjects;
        }
        set
        {
          this.m_owner.m_collectAssetObjects = value;
        }
      }

      public List<string> m_collectedCharImageSpineAssets
      {
        get
        {
          return this.m_owner.m_collectedCharImageSpineAssets;
        }
        set
        {
          this.m_owner.m_collectedCharImageSpineAssets = value;
        }
      }

      public RandomNumber m_armyRandomNumber
      {
        get
        {
          return this.m_owner.m_armyRandomNumber;
        }
        set
        {
          this.m_owner.m_armyRandomNumber = value;
        }
      }

      public ProBattleReport m_battleReport
      {
        get
        {
          return this.m_owner.m_battleReport;
        }
        set
        {
          this.m_owner.m_battleReport = value;
        }
      }

      public int m_battleStopTurn
      {
        get
        {
          return this.m_owner.m_battleStopTurn;
        }
        set
        {
          this.m_owner.m_battleStopTurn = value;
        }
      }

      public bool m_isSendBattleCheatGMCommand
      {
        get
        {
          return this.m_owner.m_isSendBattleCheatGMCommand;
        }
        set
        {
          this.m_owner.m_isSendBattleCheatGMCommand = value;
        }
      }

      public bool m_isStartInBattleRoom
      {
        get
        {
          return this.m_owner.m_isStartInBattleRoom;
        }
        set
        {
          this.m_owner.m_isStartInBattleRoom = value;
        }
      }

      public DateTime m_battleStartTime
      {
        get
        {
          return this.m_owner.m_battleStartTime;
        }
        set
        {
          this.m_owner.m_battleStartTime = value;
        }
      }

      public List<BattleUITask.RegretStep> m_regretSteps
      {
        get
        {
          return this.m_owner.m_regretSteps;
        }
        set
        {
          this.m_owner.m_regretSteps = value;
        }
      }

      public List<BattleCommand> m_regretBattleCommands
      {
        get
        {
          return this.m_owner.m_regretBattleCommands;
        }
        set
        {
          this.m_owner.m_regretBattleCommands = value;
        }
      }

      public int m_regretFinalStep
      {
        get
        {
          return this.m_owner.m_regretFinalStep;
        }
        set
        {
          this.m_owner.m_regretFinalStep = value;
        }
      }

      public int m_regretCurrentStep
      {
        get
        {
          return this.m_owner.m_regretCurrentStep;
        }
        set
        {
          this.m_owner.m_regretCurrentStep = value;
        }
      }

      public int m_regretCancelStep
      {
        get
        {
          return this.m_owner.m_regretCancelStep;
        }
        set
        {
          this.m_owner.m_regretCancelStep = value;
        }
      }

      public int m_regretFinalTurn
      {
        get
        {
          return this.m_owner.m_regretFinalTurn;
        }
        set
        {
          this.m_owner.m_regretFinalTurn = value;
        }
      }

      public int m_regretCurrentTurn
      {
        get
        {
          return this.m_owner.m_regretCurrentTurn;
        }
        set
        {
          this.m_owner.m_regretCurrentTurn = value;
        }
      }

      public int m_regretCameraFocusActorId
      {
        get
        {
          return this.m_owner.m_regretCameraFocusActorId;
        }
        set
        {
          this.m_owner.m_regretCameraFocusActorId = value;
        }
      }

      public bool m_isBattleLoseRegret
      {
        get
        {
          return this.m_owner.m_isBattleLoseRegret;
        }
        set
        {
          this.m_owner.m_isBattleLoseRegret = value;
        }
      }

      public bool m_isTryActivateRegret
      {
        get
        {
          return this.m_owner.m_isTryActivateRegret;
        }
        set
        {
          this.m_owner.m_isTryActivateRegret = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void LogCurrentBattleId()
      {
        this.m_owner.LogCurrentBattleId();
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void HideAllView()
      {
        this.m_owner.HideAllView();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public static void StopSubUITask(System.Type taskType)
      {
        BattleUITask.StopSubUITask(taskType);
      }

      public bool IsLoading()
      {
        return this.m_owner.IsLoading();
      }

      public void CreateClientBattle()
      {
        this.m_owner.CreateClientBattle();
      }

      public void DestroyClientBattle()
      {
        this.m_owner.DestroyClientBattle();
      }

      public void PrepareClientBattle(bool prepareBattleUI)
      {
        this.m_owner.PrepareClientBattle(prepareBattleUI);
      }

      public void StartClientBattle()
      {
        this.m_owner.StartClientBattle();
      }

      public void RebuildClientBattle()
      {
        this.m_owner.RebuildClientBattle();
      }

      public void StopBattle(bool win, bool skipPerform)
      {
        this.m_owner.StopBattle(win, skipPerform);
      }

      public void SendBattleRoomInitLog()
      {
        this.m_owner.SendBattleRoomInitLog();
      }

      public BattleTeamSetup GetBattleTeamSetup(int team)
      {
        return this.m_owner.GetBattleTeamSetup(team);
      }

      public List<GridPosition> GetTeamPositions(int team)
      {
        return this.m_owner.GetTeamPositions(team);
      }

      public void SetCombatHp(int teamNumber)
      {
        this.m_owner.SetCombatHp(teamNumber);
      }

      public void ExitBattleReturnToWorld()
      {
        this.m_owner.ExitBattleReturnToWorld();
      }

      public static void UnloadAssetsAndStartWorldUITask()
      {
        BattleUITask.UnloadAssetsAndStartWorldUITask();
      }

      public static IEnumerator Co_UnloadAssetsAndStartWorldUITask()
      {
        return BattleUITask.Co_UnloadAssetsAndStartWorldUITask();
      }

      public static void WorldUITask_OnLoadAllResCompleted()
      {
        BattleUITask.WorldUITask_OnLoadAllResCompleted();
      }

      public void ShowErrorMessageAndExitBattle(int errorCode)
      {
        this.m_owner.ShowErrorMessageAndExitBattle(errorCode);
      }

      public void ShowErrorMessageAndExitBattle(StringTableId strId)
      {
        this.m_owner.ShowErrorMessageAndExitBattle(strId);
      }

      public void SaveProcessingBattle()
      {
        this.m_owner.SaveProcessingBattle();
      }

      public void BattlePrepareBeforeShowResult()
      {
        this.m_owner.BattlePrepareBeforeShowResult();
      }

      public bool IsMeArenaBattleTeam1()
      {
        return this.m_owner.IsMeArenaBattleTeam1();
      }

      public bool CanUseChat()
      {
        return this.m_owner.CanUseChat();
      }

      public bool CanUseDanmaku()
      {
        return this.m_owner.CanUseDanmaku();
      }

      public void BattlePauseUIController_OnClose()
      {
        this.m_owner.BattlePauseUIController_OnClose();
      }

      public void BattlePauseUIController_OnShowPlayerSetting()
      {
        this.m_owner.BattlePauseUIController_OnShowPlayerSetting();
      }

      public void BattlePauseUIController_OnExit()
      {
        this.m_owner.BattlePauseUIController_OnExit();
      }

      public void ExitBattleDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.ExitBattleDialogBoxCallback(r);
      }

      public void TestUI_ExitBattle()
      {
        this.m_owner.TestUI_ExitBattle();
      }

      public void TestUI_RestartBattle()
      {
        this.m_owner.TestUI_RestartBattle();
      }

      public void TestUI_ReplayBattle()
      {
        this.m_owner.TestUI_ReplayBattle();
      }

      public void TestUI_PrepareBattle()
      {
        this.m_owner.TestUI_PrepareBattle();
      }

      public void TestUI_StopBattle(bool win)
      {
        this.m_owner.TestUI_StopBattle(win);
      }

      public void PlayerContext_OnChatMessageNtf(ChatMessage msg)
      {
        this.m_owner.PlayerContext_OnChatMessageNtf(msg);
      }

      public void PlayerContext_OnTeamRoomInviteNtf(TeamRoomInviteInfo inviteInfo)
      {
        this.m_owner.PlayerContext_OnTeamRoomInviteNtf(inviteInfo);
      }

      public void PlayerContext_OnBattlePracticeInvitedNtf(PVPInviteInfo inviteInfo)
      {
        this.m_owner.PlayerContext_OnBattlePracticeInvitedNtf(inviteInfo);
      }

      public void CollectBattlePrepareAssets()
      {
        this.m_owner.CollectBattlePrepareAssets();
      }

      public void CollectBattleAssets()
      {
        this.m_owner.CollectBattleAssets();
      }

      public void CollectBattlePrepareHeros(List<BattleHero> heros0, List<BattleHero> heros1)
      {
        this.m_owner.CollectBattlePrepareHeros(heros0, heros1);
      }

      public void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
      {
        this.m_owner.CollectBattlefieldAssets(battlefieldInfo);
      }

      public void CollectTerrainAssets(ConfigDataTerrainInfo terrainInfo)
      {
        this.m_owner.CollectTerrainAssets(terrainInfo);
      }

      public void CollectCharImageAssets(
        ConfigDataCharImageInfo charImageInfo,
        ConfigDataCharImageSkinResourceInfo skinResInfo)
      {
        this.m_owner.CollectCharImageAssets(charImageInfo, skinResInfo);
      }

      public void CollectCharSpineAssets(
        ConfigDataCharImageInfo charImageInfo,
        ConfigDataCharImageSkinResourceInfo skinResInfo)
      {
        this.m_owner.CollectCharSpineAssets(charImageInfo, skinResInfo);
      }

      public void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
      {
        this.m_owner.CollectHeadImageAssets(charImageInfo);
      }

      public void CollectHeroAssets(
        ConfigDataJobConnectionInfo jobConnectionInfo,
        ConfigDataModelSkinResourceInfo heroSkinResInfo,
        int heroStar)
      {
        this.m_owner.CollectHeroAssets(jobConnectionInfo, heroSkinResInfo, heroStar);
      }

      public void CollectSoldierAssets(
        ConfigDataSoldierInfo soldierInfo,
        ConfigDataModelSkinResourceInfo soldierSkinResInfo)
      {
        this.m_owner.CollectSoldierAssets(soldierInfo, soldierSkinResInfo);
      }

      public void CollectSkinAssets(ConfigDataModelSkinResourceInfo skinResInfo)
      {
        this.m_owner.CollectSkinAssets(skinResInfo);
      }

      public void CollectSkillAssets(ConfigDataSkillInfo skillInfo)
      {
        this.m_owner.CollectSkillAssets(skillInfo);
      }

      public void CollectBuffAssets(ConfigDataBuffInfo buffInfo)
      {
        this.m_owner.CollectBuffAssets(buffInfo);
      }

      public void CollectDefaultHeroAssets(ConfigDataHeroInfo heroInfo)
      {
        this.m_owner.CollectDefaultHeroAssets(heroInfo);
      }

      public void CollectBattleEventAssets(ConfigDataBattleEventTriggerInfo eventTriggerInfo)
      {
        this.m_owner.CollectBattleEventAssets(eventTriggerInfo);
      }

      public void CollectBattleEventActionAssets(ConfigDataBattleEventActionInfo eventActionInfo)
      {
        this.m_owner.CollectBattleEventActionAssets(eventActionInfo);
      }

      public void CollectBattlePerformAssets(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.CollectBattlePerformAssets(performInfo);
      }

      public void _CollectBattlePerformAssets(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner._CollectBattlePerformAssets(performInfo);
      }

      public void CollectTrainingTechAssets(List<TrainingTech> techs)
      {
        this.m_owner.CollectTrainingTechAssets(techs);
      }

      public bool IsAlreadyCollectAsset(object obj)
      {
        return this.m_owner.IsAlreadyCollectAsset(obj);
      }

      public void ClearAlreadyCollectAssets()
      {
        this.m_owner.ClearAlreadyCollectAssets();
      }

      public void StartBattleUI()
      {
        this.m_owner.StartBattleUI();
      }

      public void ClientActorTryMoveTo(ClientBattleActor ca, GridPosition p, int finalDir)
      {
        this.m_owner.ClientActorTryMoveTo(ca, p, finalDir);
      }

      public void AddCommandIfMoved(ClientBattleActor ca)
      {
        this.m_owner.AddCommandIfMoved(ca);
      }

      public GridPosition FindAttackPosition(int attackDistance, GridPosition targetPos)
      {
        return this.m_owner.FindAttackPosition(attackDistance, targetPos);
      }

      public bool CanAction()
      {
        return this.m_owner.CanAction();
      }

      public BattleActor GetMapBattleActor(GridPosition p)
      {
        return this.m_owner.GetMapBattleActor(p);
      }

      public void ResetActorEffect()
      {
        this.m_owner.ResetActorEffect();
      }

      public bool CanUseSkillAtPosition(
        ClientBattleActor ca,
        ConfigDataSkillInfo skillInfo,
        GridPosition p)
      {
        return this.m_owner.CanUseSkillAtPosition(ca, skillInfo, p);
      }

      public void FindMoveRegion()
      {
        this.m_owner.FindMoveRegion();
      }

      public void ShowMoveAndAttackRegion(BattleActor actor, GridPosition startPos)
      {
        this.m_owner.ShowMoveAndAttackRegion(actor, startPos);
      }

      public void ShowMoveAndAttackRegion()
      {
        this.m_owner.ShowMoveAndAttackRegion();
      }

      public void ShowMoveRegion(BattleActor actor, GridPosition startPos)
      {
        this.m_owner.ShowMoveRegion(actor, startPos);
      }

      public void ShowMoveRegion()
      {
        this.m_owner.ShowMoveRegion();
      }

      public void UpdateDangerRegion()
      {
        this.m_owner.UpdateDangerRegion();
      }

      public void ShowDangerTeamRegion(int team)
      {
        this.m_owner.ShowDangerTeamRegion(team);
      }

      public void ShowDangerRegion(List<int> actorIds)
      {
        this.m_owner.ShowDangerRegion(actorIds);
      }

      public void ShowMovePath(ClientBattleActor ca, GridPosition startPos)
      {
        this.m_owner.ShowMovePath(ca, startPos);
      }

      public MoveType GetSummonMoveType(int heroId)
      {
        return this.m_owner.GetSummonMoveType(heroId);
      }

      public void ShowSkillRegion(ClientBattleActor ca, int skillIndex)
      {
        this.m_owner.ShowSkillRegion(ca, skillIndex);
      }

      public void AddPositionToList(int x, int y, List<GridPosition> positionList)
      {
        this.m_owner.AddPositionToList(x, y, positionList);
      }

      public void ShowSkillRegion()
      {
        this.m_owner.ShowSkillRegion();
      }

      public void ShowSkillRange(ClientBattleActor ca, int skillIndex, GridPosition targetPos)
      {
        this.m_owner.ShowSkillRange(ca, skillIndex, targetPos);
      }

      public void ShowSkillRange()
      {
        this.m_owner.ShowSkillRange();
      }

      public void ShowTeleportRegion(ClientBattleActor ca, int skillIndex, GridPosition targetPos)
      {
        this.m_owner.ShowTeleportRegion(ca, skillIndex, targetPos);
      }

      public void ShowTeleportRegion()
      {
        this.m_owner.ShowTeleportRegion();
      }

      public void ShowAttackTargets(ClientBattleActor ca)
      {
        this.m_owner.ShowAttackTargets(ca);
      }

      public void ShowSkillTargets(ClientBattleActor ca, int skillIndex)
      {
        this.m_owner.ShowSkillTargets(ca, skillIndex);
      }

      public void ShowSkillTargets()
      {
        this.m_owner.ShowSkillTargets();
      }

      public void ShowBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
      {
        this.m_owner.ShowBattleTreasureDialog(treasureInfo);
      }

      public void ShowBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
      {
        this.m_owner.ShowBattleTreasureReward(treasureInfo);
      }

      public void ShowPreCombat(
        BattleActor a0,
        BattleActor a1,
        ConfigDataSkillInfo attackerSkillInfo)
      {
        this.m_owner.ShowPreCombat(a0, a1, attackerSkillInfo);
      }

      public void ShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
      {
        this.m_owner.ShowFastCombat(a0, a1);
      }

      public void ShowActorInfo(BattleActor a)
      {
        this.m_owner.ShowActorInfo(a);
      }

      public void HideActorInfo()
      {
        this.m_owner.HideActorInfo();
      }

      public void ShowSelectionMarkAndTerrain(GridPosition p)
      {
        this.m_owner.ShowSelectionMarkAndTerrain(p);
      }

      public void SetUIStateNone()
      {
        this.m_owner.SetUIStateNone();
      }

      public void SetUIStateSelectActionActor()
      {
        this.m_owner.SetUIStateSelectActionActor();
      }

      public void SetUIStateMove()
      {
        this.m_owner.SetUIStateMove();
      }

      public void ShowSkills()
      {
        this.m_owner.ShowSkills();
      }

      public void SetUIStateExtraMove()
      {
        this.m_owner.SetUIStateExtraMove();
      }

      public void SetUIStateSelectSkillTarget()
      {
        this.m_owner.SetUIStateSelectSkillTarget();
      }

      public void SetUIStateConfirmSkill()
      {
        this.m_owner.SetUIStateConfirmSkill();
      }

      public void SetUIStateSelectTeleportPosition1()
      {
        this.m_owner.SetUIStateSelectTeleportPosition1();
      }

      public void SetUIStateSelectTeleportPosition2()
      {
        this.m_owner.SetUIStateSelectTeleportPosition2();
      }

      public void SetUIStateWaitOtherPlayer()
      {
        this.m_owner.SetUIStateWaitOtherPlayer();
      }

      public void ShowCanActionActorsUI()
      {
        this.m_owner.ShowCanActionActorsUI();
      }

      public void HideCanActionActorsUI()
      {
        this.m_owner.HideCanActionActorsUI();
      }

      public void DoAutoBattle()
      {
        this.m_owner.DoAutoBattle();
      }

      public void SetAutoBattle(bool on)
      {
        this.m_owner.SetAutoBattle(on);
      }

      public void CameraFocusActor(int actorId)
      {
        this.m_owner.CameraFocusActor(actorId);
      }

      public void BattleUIController_OnAutoBattle(bool on)
      {
        this.m_owner.BattleUIController_OnAutoBattle(on);
      }

      public void BattleUIController_OnFastBattle(bool on)
      {
        this.m_owner.BattleUIController_OnFastBattle(on);
      }

      public void BattleUIController_OnSkipCombat(SkipCombatMode mode)
      {
        this.m_owner.BattleUIController_OnSkipCombat(mode);
      }

      public void BattleUIController_OnShowDanger(bool on)
      {
        this.m_owner.BattleUIController_OnShowDanger(on);
      }

      public void BattleUIController_OnEndAllAction()
      {
        this.m_owner.BattleUIController_OnEndAllAction();
      }

      public void EndAllActionDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.EndAllActionDialogBoxCallback(r);
      }

      public void BattleUIController_OnEndAction()
      {
        this.m_owner.BattleUIController_OnEndAction();
      }

      public void BattleUIController_OnShowActorInfo()
      {
        this.m_owner.BattleUIController_OnShowActorInfo();
      }

      public void BattleUIController_OnSelectSkill(int skillIndex)
      {
        this.m_owner.BattleUIController_OnSelectSkill(skillIndex);
      }

      public void BattleUIController_OnUseSkill()
      {
        this.m_owner.BattleUIController_OnUseSkill();
      }

      public void BattleUIController_OnCancelSkill()
      {
        this.m_owner.BattleUIController_OnCancelSkill();
      }

      public void BattleUIController_OnShowChat()
      {
        this.m_owner.BattleUIController_OnShowChat();
      }

      public void BattleUIController_OnShowHelp()
      {
        this.m_owner.BattleUIController_OnShowHelp();
      }

      public void BattleUIController_OnPauseBattle()
      {
        this.m_owner.BattleUIController_OnPauseBattle();
      }

      public void BattleUIController_OnShowArmyRelation()
      {
        this.m_owner.BattleUIController_OnShowArmyRelation();
      }

      public void BattleUIController_OnPointerDown(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.BattleUIController_OnPointerDown(button, position);
      }

      public void BattleUIController_OnPointerUp(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.BattleUIController_OnPointerUp(button, position);
      }

      public void BattleUIController_OnShowCurTurnDanmaku(int turn)
      {
        this.m_owner.BattleUIController_OnShowCurTurnDanmaku(turn);
      }

      public void BattleUIController_OnCloseCurTurnDanmaku()
      {
        this.m_owner.BattleUIController_OnCloseCurTurnDanmaku();
      }

      public void BattleUIController_OnShowOneDanmaku(DanmakuEntry danmaku)
      {
        this.m_owner.BattleUIController_OnShowOneDanmaku(danmaku);
      }

      public void BattleUIController_OnPointerClick(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.BattleUIController_OnPointerClick(button, position);
      }

      public bool ShouldShowActorInfo(BattleActor a)
      {
        return this.m_owner.ShouldShowActorInfo(a);
      }

      public void BattleUIController_On3DTouch(Vector2 pos)
      {
        this.m_owner.BattleUIController_On3DTouch(pos);
      }

      public void PlaySelectHeroSound(BattleActor a)
      {
        this.m_owner.PlaySelectHeroSound(a);
      }

      public void BattleDialogUITask_OnClose(bool isSkip)
      {
        this.m_owner.BattleDialogUITask_OnClose(isSkip);
      }

      public void BattleTreasureDialogUIController_OnClose()
      {
        this.m_owner.BattleTreasureDialogUIController_OnClose();
      }

      public void GetRewardGoodsUITask_OnStop(Task task)
      {
        this.m_owner.GetRewardGoodsUITask_OnStop(task);
      }

      public void GetRewardGoodsUITask_OnClose()
      {
        this.m_owner.GetRewardGoodsUITask_OnClose();
      }

      public void CombatUIController_OnAutoBattle(bool on)
      {
        this.m_owner.CombatUIController_OnAutoBattle(on);
      }

      public void PreCombatUIController_OnOk()
      {
        this.m_owner.PreCombatUIController_OnOk();
      }

      public void PreCombatUIController_OnCancel()
      {
        this.m_owner.PreCombatUIController_OnCancel();
      }

      public void PreCombatUIController_OnStop()
      {
        this.m_owner.PreCombatUIController_OnStop();
      }

      public void BattleSceneUIController_OnPointerDown(PointerEventData eventData)
      {
        this.m_owner.BattleSceneUIController_OnPointerDown(eventData);
      }

      public void BattleSceneUIController_OnPointerUp(PointerEventData eventData)
      {
        this.m_owner.BattleSceneUIController_OnPointerUp(eventData);
      }

      public void BattleSceneUIController_OnPointerClick(PointerEventData eventData)
      {
        this.m_owner.BattleSceneUIController_OnPointerClick(eventData);
      }

      public void BattleSceneUIController_OnBeginDrag(PointerEventData eventData)
      {
        this.m_owner.BattleSceneUIController_OnBeginDrag(eventData);
      }

      public void BattleSceneUIController_OnEndDrag(PointerEventData eventData)
      {
        this.m_owner.BattleSceneUIController_OnEndDrag(eventData);
      }

      public void BattleSceneUIController_OnDrag(PointerEventData eventData)
      {
        this.m_owner.BattleSceneUIController_OnDrag(eventData);
      }

      public void BattleSceneUIController_On3DTouch(Vector2 p)
      {
        this.m_owner.BattleSceneUIController_On3DTouch(p);
      }

      public void SendBattleCommands()
      {
        this.m_owner.SendBattleCommands();
      }

      public void UpdateBattleRoomPlayerHeroStatus()
      {
        this.m_owner.UpdateBattleRoomPlayerHeroStatus();
      }

      public void StartBattleRoomMyActionCountdown()
      {
        this.m_owner.StartBattleRoomMyActionCountdown();
      }

      public void StopBattleRoomMyActionCountdown()
      {
        this.m_owner.StopBattleRoomMyActionCountdown();
      }

      public void ActivateBattleRoomMyActionCountdown(bool isActive)
      {
        this.m_owner.ActivateBattleRoomMyActionCountdown(isActive);
      }

      public void UpdateBattleRoomMyActionCountdown()
      {
        this.m_owner.UpdateBattleRoomMyActionCountdown();
      }

      public void StartBattleRoomOtherActionCountdown(int playerIndex)
      {
        this.m_owner.StartBattleRoomOtherActionCountdown(playerIndex);
      }

      public void StopBattleRoomOtherActionCountdown()
      {
        this.m_owner.StopBattleRoomOtherActionCountdown();
      }

      public void ActivateBattleRoomOtherActionCountdown(bool isActive)
      {
        this.m_owner.ActivateBattleRoomOtherActionCountdown(isActive);
      }

      public void UpdateBattleRoomOtherActionCountdown()
      {
        this.m_owner.UpdateBattleRoomOtherActionCountdown();
      }

      public void BattleRoomSetAutoBattle(bool on)
      {
        this.m_owner.BattleRoomSetAutoBattle(on);
      }

      public void ProcessBattlePendingNtfs()
      {
        this.m_owner.ProcessBattlePendingNtfs();
      }

      public void PlayerContext_OnBattleRoomPlayerStatusChangedNtf(ulong playerSessionId)
      {
        this.m_owner.PlayerContext_OnBattleRoomPlayerStatusChangedNtf(playerSessionId);
      }

      public void PlayerContext_OnBattleRoomPlayerQuitNtf(
        ulong playerSessionId,
        BattleRoomQuitReason reason)
      {
        this.m_owner.PlayerContext_OnBattleRoomPlayerQuitNtf(playerSessionId, reason);
      }

      public void PlayerContext_OnBattleRoomBattleCommandExecuteNtf(ulong playerSessionId)
      {
        this.m_owner.PlayerContext_OnBattleRoomBattleCommandExecuteNtf(playerSessionId);
      }

      public void PlayerContext_OnBattleRoomTeamBattleFinishNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomTeamBattleFinishNtf();
      }

      public void PlayerContext_OnBattleRoomPVPBattleFinishNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomPVPBattleFinishNtf();
      }

      public void PlayerContext_OnBattleRoomRealTimePVPBattleFinishNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomRealTimePVPBattleFinishNtf();
      }

      public void PlayerContext_OnBattleRoomGuildMassiveCombatBattleFinishNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomGuildMassiveCombatBattleFinishNtf();
      }

      public void PlayerContext_OnBattleRoomPVPBattleJoinNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomPVPBattleJoinNtf();
      }

      public void PlayerContext_OnPlayerInfoInitEnd()
      {
        this.m_owner.PlayerContext_OnPlayerInfoInitEnd();
      }

      public void ReloginUITask_OnReloginSuccess()
      {
        this.m_owner.ReloginUITask_OnReloginSuccess();
      }

      public BattleRoom GetBattleRoom()
      {
        return this.m_owner.GetBattleRoom();
      }

      public void UpdateBattleRoomPlayerHeroCount()
      {
        this.m_owner.UpdateBattleRoomPlayerHeroCount();
      }

      public void UpdateBattleRoomPrepareTimeout()
      {
        this.m_owner.UpdateBattleRoomPrepareTimeout();
      }

      public void UpdateRealtimePVPBattlePrepareTimeout()
      {
        this.m_owner.UpdateRealtimePVPBattlePrepareTimeout();
      }

      public void UpdateRealtimePVPBattlePrepareStatus()
      {
        this.m_owner.UpdateRealtimePVPBattlePrepareStatus();
      }

      public void UpdateStageActorTag(BattlePrepareStageActor sa)
      {
        this.m_owner.UpdateStageActorTag(sa);
      }

      public void ProcessBattlePreparePendingNtfs(bool playFx)
      {
        this.m_owner.ProcessBattlePreparePendingNtfs(playFx);
      }

      public void LoadAndUpdateBattleRoomStageActors(List<int> posList, bool playFx)
      {
        this.m_owner.LoadAndUpdateBattleRoomStageActors(posList, playFx);
      }

      public void UpdateBattleRoomStageActors(int posIdx, BattleHero hero, bool playFx)
      {
        this.m_owner.UpdateBattleRoomStageActors(posIdx, hero, playFx);
      }

      public void BattleRoomBattleStart()
      {
        this.m_owner.BattleRoomBattleStart();
      }

      public void PVPBattlePrepareUIController_OnPrepareConfirm()
      {
        this.m_owner.PVPBattlePrepareUIController_OnPrepareConfirm();
      }

      public void PlayerContext_OnBattleRoomHeroSetupNtf(List<int> posList)
      {
        this.m_owner.PlayerContext_OnBattleRoomHeroSetupNtf(posList);
      }

      public void PlayerContext_OnBattleRoomDataChangeNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomDataChangeNtf();
      }

      public void PlayerContext_OnBattleRoomTeamBattleStartNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomTeamBattleStartNtf();
      }

      public void PlayerContext_OnBattleRoomPVPBattleStartNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomPVPBattleStartNtf();
      }

      public void PlayerContext_OnBattleRoomRealTimePVPBattleStartNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomRealTimePVPBattleStartNtf();
      }

      public void ShowBattleResult(
        BattleType battleType,
        int stars,
        BattleReward battleReward,
        bool isFirstWin,
        List<int> gotAchievements,
        BattleLevelAchievement[] achievements)
      {
        this.m_owner.ShowBattleResult(battleType, stars, battleReward, isFirstWin, gotAchievements, achievements);
      }

      public void ShowBattleLose()
      {
        this.m_owner.ShowBattleLose();
      }

      public void ShowBattleReportEnd(ArenaBattleReport battleReport)
      {
        this.m_owner.ShowBattleReportEnd(battleReport);
      }

      public void ShowBattleReportEnd(RealTimePVPBattleReport battleReport)
      {
        this.m_owner.ShowBattleReportEnd(battleReport);
      }

      public ProBattleReport BuildBattleReport()
      {
        return this.m_owner.BuildBattleReport();
      }

      public bool CheckBattleResult()
      {
        return this.m_owner.CheckBattleResult();
      }

      public void SendBattleCheatGMCommand(bool isCheat)
      {
        this.m_owner.SendBattleCheatGMCommand(isCheat);
      }

      public void BattleResultEnd()
      {
        this.m_owner.BattleResultEnd();
      }

      public void BattleRoomInviteTeammateDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.BattleRoomInviteTeammateDialogBoxCallback(r);
      }

      public void BattleUIController_OnWinOrLoseEnd()
      {
        this.m_owner.BattleUIController_OnWinOrLoseEnd();
      }

      public void BattleResultUITask_OnStop(Task task)
      {
        this.m_owner.BattleResultUITask_OnStop(task);
      }

      public void BattleResultUITask_OnClose()
      {
        this.m_owner.BattleResultUITask_OnClose();
      }

      public void BattleResultScoreUITask_OnStop(Task task)
      {
        this.m_owner.BattleResultScoreUITask_OnStop(task);
      }

      public void BattleResultScoreUITask_OnClose()
      {
        this.m_owner.BattleResultScoreUITask_OnClose();
      }

      public void BattleLoseUITask_OnStop(Task task)
      {
        this.m_owner.BattleLoseUITask_OnStop(task);
      }

      public void BattleLoseUITask_OnClose()
      {
        this.m_owner.BattleLoseUITask_OnClose();
      }

      public void BattleReportEndUITask_OnStop(Task task)
      {
        this.m_owner.BattleReportEndUITask_OnStop(task);
      }

      public void BattleReportEndUITask_OnClose()
      {
        this.m_owner.BattleReportEndUITask_OnClose();
      }

      public void BattleReportEndUITask_OnPlayAgain()
      {
        this.m_owner.BattleReportEndUITask_OnPlayAgain();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void InitBattleSceneUIController()
      {
        this.m_owner.InitBattleSceneUIController();
      }

      public void UninitBattleSceneUIController()
      {
        this.m_owner.UninitBattleSceneUIController();
      }

      public void InitBattleUIController()
      {
        this.m_owner.InitBattleUIController();
      }

      public void UninitBattleUIController()
      {
        this.m_owner.UninitBattleUIController();
      }

      public void InitCombatUIController()
      {
        this.m_owner.InitCombatUIController();
      }

      public void UninitCombatUIController()
      {
        this.m_owner.UninitCombatUIController();
      }

      public void InitPreCombatUIController()
      {
        this.m_owner.InitPreCombatUIController();
      }

      public void UninitPreCombatUIController()
      {
        this.m_owner.UninitPreCombatUIController();
      }

      public void InitBattleRoomUIController()
      {
        this.m_owner.InitBattleRoomUIController();
      }

      public void UninitBattleRoomUIController()
      {
        this.m_owner.UninitBattleRoomUIController();
      }

      public void InitBattleCommonUIController()
      {
        this.m_owner.InitBattleCommonUIController();
      }

      public void UninitBattleCommonUIController()
      {
        this.m_owner.UninitBattleCommonUIController();
      }

      public void InitBattlePrepareUIController()
      {
        this.m_owner.InitBattlePrepareUIController();
      }

      public void UninitBattlePrepareUIController()
      {
        this.m_owner.UninitBattlePrepareUIController();
      }

      public void BattleLoseUseRegretDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.BattleLoseUseRegretDialogBoxCallback(r);
      }

      public void BattleDialogUITask_OnLoadAllResCompleted()
      {
        this.m_owner.BattleDialogUITask_OnLoadAllResCompleted();
      }

      public void StartBattleTeamSetNetTask(BattleType battleType, int battleId, List<int> heros)
      {
        this.m_owner.StartBattleTeamSetNetTask(battleType, battleId, heros);
      }

      public void StartLevelAttackNetTask()
      {
        this.m_owner.StartLevelAttackNetTask();
      }

      public void StartLevelWayPointMoveNetTask(ConfigDataWaypointInfo waypointInfo)
      {
        this.m_owner.StartLevelWayPointMoveNetTask(waypointInfo);
      }

      public void StartLevelScenarioHandleNetTask(ConfigDataScenarioInfo scenarioInfo)
      {
        this.m_owner.StartLevelScenarioHandleNetTask(scenarioInfo);
      }

      public void StartRiftLevelAttackNetTask(ConfigDataRiftLevelInfo levelInfo)
      {
        this.m_owner.StartRiftLevelAttackNetTask(levelInfo);
      }

      public void StartHeroDungeonLevelAttackNetTask(ConfigDataHeroDungeonLevelInfo levelInfo)
      {
        this.m_owner.StartHeroDungeonLevelAttackNetTask(levelInfo);
      }

      public void StartAnikiLevelAttackNetTask(ConfigDataAnikiLevelInfo levelInfo)
      {
        this.m_owner.StartAnikiLevelAttackNetTask(levelInfo);
      }

      public void StartThearchyLevelAttackNetTask(ConfigDataThearchyTrialLevelInfo levelInfo)
      {
        this.m_owner.StartThearchyLevelAttackNetTask(levelInfo);
      }

      public void StartMemoryCorridorLevelAttackNetTask(ConfigDataMemoryCorridorLevelInfo levelInfo)
      {
        this.m_owner.StartMemoryCorridorLevelAttackNetTask(levelInfo);
      }

      public void StartHeroTrainningLevelAttackNetTask(ConfigDataHeroTrainningLevelInfo levelInfo)
      {
        this.m_owner.StartHeroTrainningLevelAttackNetTask(levelInfo);
      }

      public void StartHeroPhantomLevelAttackNetTask(ConfigDataHeroPhantomLevelInfo levelInfo)
      {
        this.m_owner.StartHeroPhantomLevelAttackNetTask(levelInfo);
      }

      public void StartTreasureLevelAttackNetTask(ConfigDataTreasureLevelInfo levelInfo)
      {
        this.m_owner.StartTreasureLevelAttackNetTask(levelInfo);
      }

      public void StartUnchartedScoreLevelAttackNetTask(ConfigDataScoreLevelInfo levelInfo)
      {
        this.m_owner.StartUnchartedScoreLevelAttackNetTask(levelInfo);
      }

      public void StartUnchartedChallengeLevelAttackNetTask(ConfigDataChallengeLevelInfo levelInfo)
      {
        this.m_owner.StartUnchartedChallengeLevelAttackNetTask(levelInfo);
      }

      public void StartClimbTowerLevelAttackNetTask(ConfigDataTowerFloorInfo floorInfo)
      {
        this.m_owner.StartClimbTowerLevelAttackNetTask(floorInfo);
      }

      public void StartEternalShrineLevelAttackNetTask(ConfigDataEternalShrineLevelInfo levelInfo)
      {
        this.m_owner.StartEternalShrineLevelAttackNetTask(levelInfo);
      }

      public void StartCollectionLevelAttackNetTask(GameFunctionType levelType, int levelId)
      {
        this.m_owner.StartCollectionLevelAttackNetTask(levelType, levelId);
      }

      public void StartGuildMassiveCombatAttackNetTask(
        ConfigDataGuildMassiveCombatLevelInfo levelInfo,
        List<int> heroIds)
      {
        this.m_owner.StartGuildMassiveCombatAttackNetTask(levelInfo, heroIds);
      }

      public void StartArenaOpponentAttackFightingNetTask()
      {
        this.m_owner.StartArenaOpponentAttackFightingNetTask();
      }

      public void HandleLevelAttackNetTaskResult(int result)
      {
        this.m_owner.HandleLevelAttackNetTaskResult(result);
      }

      public void StartLevelFinishedNetTask()
      {
        this.m_owner.StartLevelFinishedNetTask();
      }

      public void StartWayPointBattleFinishedNetTask(ConfigDataWaypointInfo wayPointInfo)
      {
        this.m_owner.StartWayPointBattleFinishedNetTask(wayPointInfo);
      }

      public void StartRiftLevelBattleFinishedNetTask(ConfigDataRiftLevelInfo levelInfo)
      {
        this.m_owner.StartRiftLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartHeroDungeonLevelBattleFinishedNetTask(
        ConfigDataHeroDungeonLevelInfo levelInfo)
      {
        this.m_owner.StartHeroDungeonLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartAnikiLevelBattleFinishedNetTask(ConfigDataAnikiLevelInfo levelInfo)
      {
        this.m_owner.StartAnikiLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartThearchyLevelBattleFinishedNetTask(ConfigDataThearchyTrialLevelInfo levelInfo)
      {
        this.m_owner.StartThearchyLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartTreasureLevelBattleFinishedNetTask(ConfigDataTreasureLevelInfo levelInfo)
      {
        this.m_owner.StartTreasureLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartMemoryCorridorLevelBattleFinishedNetTask(
        ConfigDataMemoryCorridorLevelInfo levelInfo)
      {
        this.m_owner.StartMemoryCorridorLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartHeroTrainningLevelBattleFinishedNetTask(
        ConfigDataHeroTrainningLevelInfo levelInfo)
      {
        this.m_owner.StartHeroTrainningLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartHeroPhantomLevelBattleFinishedNetTask(
        ConfigDataHeroPhantomLevelInfo levelInfo)
      {
        this.m_owner.StartHeroPhantomLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartUnchartedScoreLevelBattleFinishedNetTask(ConfigDataScoreLevelInfo levelInfo)
      {
        this.m_owner.StartUnchartedScoreLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartUnchartedChallengeLevelBattleFinishedNetTask(
        ConfigDataChallengeLevelInfo levelInfo)
      {
        this.m_owner.StartUnchartedChallengeLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartClimbTowerLevelBattleFinishedNetTask(ConfigDataTowerFloorInfo floorInfo)
      {
        this.m_owner.StartClimbTowerLevelBattleFinishedNetTask(floorInfo);
      }

      public void StartEternalShrineLevelBattleFinishedNetTask(
        ConfigDataEternalShrineLevelInfo levelInfo)
      {
        this.m_owner.StartEternalShrineLevelBattleFinishedNetTask(levelInfo);
      }

      public void StartCollectionLevelFinishNetTask(GameFunctionType levelType, int levelId)
      {
        this.m_owner.StartCollectionLevelFinishNetTask(levelType, levelId);
      }

      public void StartGuildMassiveCombatAttackFinishedNetTask(
        ConfigDataGuildMassiveCombatLevelInfo levelInfo)
      {
        this.m_owner.StartGuildMassiveCombatAttackFinishedNetTask(levelInfo);
      }

      public void StartArenaBattleFinishedNetTask()
      {
        this.m_owner.StartArenaBattleFinishedNetTask();
      }

      public void HandleBattleFinishedNetTaskResult(
        int result,
        bool isWin,
        BattleReward reward,
        int stars,
        bool isFirstWin,
        List<int> gotAchievements,
        BattleLevelAchievement[] achievements)
      {
        this.m_owner.HandleBattleFinishedNetTaskResult(result, isWin, reward, stars, isFirstWin, gotAchievements, achievements);
      }

      public void StartBattleCancelNetTask()
      {
        this.m_owner.StartBattleCancelNetTask();
      }

      public void StartDanmakuPostNetTask(Action<int> onEnd)
      {
        this.m_owner.StartDanmakuPostNetTask(onEnd);
      }

      public void StartBattleRoomHeroSetupNetTask(int heroId, int position)
      {
        this.m_owner.StartBattleRoomHeroSetupNetTask(heroId, position);
      }

      public void StartBattleRoomHeroSwapNetTask(int position1, int position2)
      {
        this.m_owner.StartBattleRoomHeroSwapNetTask(position1, position2);
      }

      public void StartBattleRoomHeroSetoffNetTask(int position)
      {
        this.m_owner.StartBattleRoomHeroSetoffNetTask(position);
      }

      public void StartBattleRoomPlayerStatusChangeNetTask(PlayerBattleStatus status, Action onOk)
      {
        this.m_owner.StartBattleRoomPlayerStatusChangeNetTask(status, onOk);
      }

      public void StartBattleRoomQuitNetTask()
      {
        this.m_owner.StartBattleRoomQuitNetTask();
      }

      public void StartBattleRoomEndCurrentBPTurnNetTask(Action onOk)
      {
        this.m_owner.StartBattleRoomEndCurrentBPTurnNetTask(onOk);
      }

      public void StartBattleRoomPlayerActionBeginNetTask()
      {
        this.m_owner.StartBattleRoomPlayerActionBeginNetTask();
      }

      public List<GridPosition> SetupReachRegion(bool isSkipBattlePrepare)
      {
        return this.m_owner.SetupReachRegion(isSkipBattlePrepare);
      }

      public void SetupBattlePauseUIController(List<GridPosition> reachRegion)
      {
        this.m_owner.SetupBattlePauseUIController(reachRegion);
      }

      public void SetupBattlePauseUIAchievements()
      {
        this.m_owner.SetupBattlePauseUIAchievements();
      }

      public GridPosition GetWinConditionTargetPosition(
        ConfigDataBattleWinConditionInfo winConditionInfo)
      {
        return this.m_owner.GetWinConditionTargetPosition(winConditionInfo);
      }

      public int GetMyStageActorCountMax()
      {
        return this.m_owner.GetMyStageActorCountMax();
      }

      public List<int> SetupMyHeros(
        ConfigDataBattleInfo battleInfo,
        int number,
        bool isSkipBattlePrepare)
      {
        return this.m_owner.SetupMyHeros(battleInfo, number, isSkipBattlePrepare);
      }

      public void FillMyHeros(List<int> heroIds, int count, List<int> disableHeroIds)
      {
        this.m_owner.FillMyHeros(heroIds, count, disableHeroIds);
      }

      public void SetupStageActors(bool isSkipBattlePrepare)
      {
        this.m_owner.SetupStageActors(isSkipBattlePrepare);
      }

      public void AddUseableHeros()
      {
        this.m_owner.AddUseableHeros();
      }

      public static ConfigDataUnchartedScoreInfo GetUnchartedScoreInfo()
      {
        return BattleUITask.GetUnchartedScoreInfo();
      }

      public static int GetHeroUnchartedScoreBonus(BattleHero hero)
      {
        return BattleUITask.GetHeroUnchartedScoreBonus(hero);
      }

      public ConfigDataTowerBattleRuleInfo GetTowerBattleRuleInfo()
      {
        return this.m_owner.GetTowerBattleRuleInfo();
      }

      public ConfigDataTowerBonusHeroGroupInfo GetTowerBonusHeroGroupInfo()
      {
        return this.m_owner.GetTowerBonusHeroGroupInfo();
      }

      public bool IsTowerPowerUpHero(BattleHero hero)
      {
        return this.m_owner.IsTowerPowerUpHero(hero);
      }

      public GuildPlayerMassiveCombatInfo GetGuildPlayerMassiveCombatInfo()
      {
        return this.m_owner.GetGuildPlayerMassiveCombatInfo();
      }

      public List<int> GetGuildMassiveCombatPreferredHeroTagIds()
      {
        return this.m_owner.GetGuildMassiveCombatPreferredHeroTagIds();
      }

      public bool IsGuildMassiveCombatCampUpHero(BattleHero hero)
      {
        return this.m_owner.IsGuildMassiveCombatCampUpHero(hero);
      }

      public bool IsEternalShrineCampUpHero(BattleHero hero)
      {
        return this.m_owner.IsEternalShrineCampUpHero(hero);
      }

      public bool IsCollectionActivityDropUpHero(BattleHero hero)
      {
        return this.m_owner.IsCollectionActivityDropUpHero(hero);
      }

      public StageActorTagType GetHeroTagType(BattleHero hero)
      {
        return this.m_owner.GetHeroTagType(hero);
      }

      public void LoadArenaAttackerHeroActionValue(BattleHero hero)
      {
        this.m_owner.LoadArenaAttackerHeroActionValue(hero);
      }

      public static int CompareHeroUnchartdScoreBonus(BattleHero h1, BattleHero h2)
      {
        return BattleUITask.CompareHeroUnchartdScoreBonus(h1, h2);
      }

      public void ShowStagePositions()
      {
        this.m_owner.ShowStagePositions();
      }

      public void SetupBattlePrepareTreasures()
      {
        this.m_owner.SetupBattlePrepareTreasures();
      }

      public static bool IsListElementsEqual(List<int> list0, List<int> list1)
      {
        return BattleUITask.IsListElementsEqual(list0, list1);
      }

      public void BuildBattleTeamSetups()
      {
        this.m_owner.BuildBattleTeamSetups();
      }

      public void ModifyBattleTeamSetups()
      {
        this.m_owner.ModifyBattleTeamSetups();
      }

      public void BuildBattleTeamSetup(int team, bool saveHeroList)
      {
        this.m_owner.BuildBattleTeamSetup(team, saveHeroList);
      }

      public int GetSoldierCount(int team)
      {
        return this.m_owner.GetSoldierCount(team);
      }

      public void SetTeamAndStartBattle()
      {
        this.m_owner.SetTeamAndStartBattle();
      }

      public void StartBattlePrepareLoadState()
      {
        this.m_owner.StartBattlePrepareLoadState();
      }

      public void StartBattleLoadState()
      {
        this.m_owner.StartBattleLoadState();
      }

      public void ShowMoveAndAttackRegion_Prepare(GridPosition pos)
      {
        this.m_owner.ShowMoveAndAttackRegion_Prepare(pos);
      }

      public void UpdateArenaAttackerHeroIds()
      {
        this.m_owner.UpdateArenaAttackerHeroIds();
      }

      public static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
      {
        return BattleUITask.CompareHeroActionValue(hero0, hero1);
      }

      public List<TrainingTech> GetPlayerTrainingTechs(
        int team,
        int playerIndex,
        bool isNpc)
      {
        return this.m_owner.GetPlayerTrainingTechs(team, playerIndex, isNpc);
      }

      public int GetPlayerLevel(int team, int playerIndex)
      {
        return this.m_owner.GetPlayerLevel(team, playerIndex);
      }

      public ulong GetPlayerSessionId(int playerIndex)
      {
        return this.m_owner.GetPlayerSessionId(playerIndex);
      }

      public int GetMyPlayerIndex()
      {
        return this.m_owner.GetMyPlayerIndex();
      }

      public bool BattlePrepareCanChangeSkill()
      {
        return this.m_owner.BattlePrepareCanChangeSkill();
      }

      public bool IsBattlePrepareDisableCameraMove()
      {
        return this.m_owner.IsBattlePrepareDisableCameraMove();
      }

      public void UpdateBattlePower()
      {
        this.m_owner.UpdateBattlePower();
      }

      public void BattlePrepareUIController_OnPauseBattle()
      {
        this.m_owner.BattlePrepareUIController_OnPauseBattle();
      }

      public void BattlePrepareUIController_OnShowArmyRelation()
      {
        this.m_owner.BattlePrepareUIController_OnShowArmyRelation();
      }

      public void BattlePrepareUIController_OnStart()
      {
        this.m_owner.BattlePrepareUIController_OnStart();
      }

      public void BattlePrepareUIController_OnShowRecommend()
      {
        this.m_owner.BattlePrepareUIController_OnShowRecommend();
      }

      public void BattlePrepareUIController_OnShowActionOrder()
      {
        this.m_owner.BattlePrepareUIController_OnShowActionOrder();
      }

      public void HeroNotFullDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.HeroNotFullDialogBoxCallback(r);
      }

      public void BattlePrepareUIController_OnTestOnStage()
      {
        this.m_owner.BattlePrepareUIController_OnTestOnStage();
      }

      public void BattlePrepareUIController_OnShowMyActorInfo(BattleHero hero)
      {
        this.m_owner.BattlePrepareUIController_OnShowMyActorInfo(hero);
      }

      public void BattlePrepareUIController_OnHideActorInfo()
      {
        this.m_owner.BattlePrepareUIController_OnHideActorInfo();
      }

      public void BattlePrepareUIController_OnStageActorChange()
      {
        this.m_owner.BattlePrepareUIController_OnStageActorChange();
      }

      public void BattlePrepareUIController_OnHeroOnStage(
        BattleHero hero,
        GridPosition pos,
        int team)
      {
        this.m_owner.BattlePrepareUIController_OnHeroOnStage(hero, pos, team);
      }

      public void BattlePrepareUIController_OnActorOffStage(BattlePrepareStageActor sa)
      {
        this.m_owner.BattlePrepareUIController_OnActorOffStage(sa);
      }

      public void BattlePrepareUIController_OnStageActorMove(
        BattlePrepareStageActor sa,
        GridPosition p)
      {
        this.m_owner.BattlePrepareUIController_OnStageActorMove(sa, p);
      }

      public void BattlePrepareUIController_OnStageActorSwap(
        BattlePrepareStageActor sa1,
        BattlePrepareStageActor sa2)
      {
        this.m_owner.BattlePrepareUIController_OnStageActorSwap(sa1, sa2);
      }

      public void BattlePrepareUIController_OnUpdateBattlePower()
      {
        this.m_owner.BattlePrepareUIController_OnUpdateBattlePower();
      }

      public void BattlePrepareUIController_OnShowChat()
      {
        this.m_owner.BattlePrepareUIController_OnShowChat();
      }

      public void BattlePrepareUIController_OnShowHelp()
      {
        this.m_owner.BattlePrepareUIController_OnShowHelp();
      }

      public void BattlePrepareUIController_OnPointerDown(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.BattlePrepareUIController_OnPointerDown(button, position);
      }

      public void BattlePrepareUIController_OnPointerUp(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.BattlePrepareUIController_OnPointerUp(button, position);
      }

      public void BattlePrepareUIController_OnBeginDragHero()
      {
        this.m_owner.BattlePrepareUIController_OnBeginDragHero();
      }

      public void BattlePrepareUIController_OnEndDragHero()
      {
        this.m_owner.BattlePrepareUIController_OnEndDragHero();
      }

      public void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero hero)
      {
        this.m_owner.BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(hero);
      }

      public void BattlePrepareActorInfoUIController_OnChangeSkill(
        BattleHero hero,
        List<int> skillIds)
      {
        this.m_owner.BattlePrepareActorInfoUIController_OnChangeSkill(hero, skillIds);
      }

      public void BattlePrepareActorInfoUIController_OnChangeSoldier(BattleHero hero, int soldierId)
      {
        this.m_owner.BattlePrepareActorInfoUIController_OnChangeSoldier(hero, soldierId);
      }

      public void ActionOrderUIController_OnConfirm()
      {
        this.m_owner.ActionOrderUIController_OnConfirm();
      }

      public void SetUIStateRegret()
      {
        this.m_owner.SetUIStateRegret();
      }

      public void UpdateRegretButton()
      {
        this.m_owner.UpdateRegretButton();
      }

      public void ClearRegret()
      {
        this.m_owner.ClearRegret();
      }

      public bool IsRegretActive()
      {
        return this.m_owner.IsRegretActive();
      }

      public bool CanUseRegret()
      {
        return this.m_owner.CanUseRegret();
      }

      public void RegretGotoStep(int step)
      {
        this.m_owner.RegretGotoStep(step);
      }

      public int FindRegretStepIndexByStep(int step)
      {
        return this.m_owner.FindRegretStepIndexByStep(step);
      }

      public int FindRegretStepIndexByTurn(int turn)
      {
        return this.m_owner.FindRegretStepIndexByTurn(turn);
      }

      public void ActivateRegret()
      {
        this.m_owner.ActivateRegret();
      }

      public void BattleUIController_OnRegretActive()
      {
        this.m_owner.BattleUIController_OnRegretActive();
      }

      public void BattleUIController_OnRegretConfirm()
      {
        this.m_owner.BattleUIController_OnRegretConfirm();
      }

      public void BattleUIController_OnRegretCancel()
      {
        this.m_owner.BattleUIController_OnRegretCancel();
      }

      public void BattleUIController_OnRegretBackward()
      {
        this.m_owner.BattleUIController_OnRegretBackward();
      }

      public void BattleUIController_OnRegretForward()
      {
        this.m_owner.BattleUIController_OnRegretForward();
      }

      public void BattleUIController_OnRegretPrevTurn()
      {
        this.m_owner.BattleUIController_OnRegretPrevTurn();
      }

      public void BattleUIController_OnRegretNextTurn()
      {
        this.m_owner.BattleUIController_OnRegretNextTurn();
      }

      public List<int> UserGuide_GetEnforceHeros()
      {
        return this.m_owner.UserGuide_GetEnforceHeros();
      }

      public void SetUserGuideBattleSettings()
      {
        this.m_owner.SetUserGuideBattleSettings();
      }
    }
  }
}
