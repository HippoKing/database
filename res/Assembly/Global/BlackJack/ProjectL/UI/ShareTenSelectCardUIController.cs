﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ShareTenSelectCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ShareTenSelectCardUIController : UIControllerBase
  {
    [AutoBind("./HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroup;
    [AutoBind("./Share/PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./Share/PlayerInfo/Lvbg/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLvText;
    [AutoBind("./Share/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverNameText;
    private List<Transform> m_heroDummyParentList;
    private List<GameObject> m_heroGameObject;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public ShareTenSelectCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh(List<int> m_heroIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSharePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int Compare(int leftHeroID, int rightHeroID)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
