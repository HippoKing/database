﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CurrencyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CurrencyUIController : UIControllerBase
  {
    [AutoBind("./Currency1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency1Obj;
    [AutoBind("./Currency1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency1Icon;
    [AutoBind("./Currency1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency1CountText;
    [AutoBind("./Currency1/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1AddButton;
    [AutoBind("./Currency1/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1DescriptionButton;
    [AutoBind("./Currency2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency2Obj;
    [AutoBind("./Currency2/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency2Icon;
    [AutoBind("./Currency2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency2CountText;
    [AutoBind("./Currency2/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2AddButton;
    [AutoBind("./Currency2/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2DescriptionButton;
    [AutoBind("./Currency3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency3Obj;
    [AutoBind("./Currency3/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency3Icon;
    [AutoBind("./Currency3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency3CountText;
    [AutoBind("./Currency3/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3AddButton;
    [AutoBind("./Currency3/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3DescriptionButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private Dictionary<int, GoodsType> m_currencyDic;

    [MethodImpl((MethodImplOptions) 32768)]
    public CurrencyUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefaultState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrency(int pos, GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAddButtonState(
      GameObject currencyGameObject,
      Image icon,
      Text countText,
      Button descriptionButton,
      Button addButton,
      GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GoodsType> EventOnAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
