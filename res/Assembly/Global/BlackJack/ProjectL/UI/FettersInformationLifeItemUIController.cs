﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersInformationLifeItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FettersInformationLifeItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockContentText;
    [AutoBind("./Title/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockTitleText;
    [AutoBind("./Lock/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockText;
    public ConfigDataHeroBiographyInfo m_heroBiographyInfo;
    public bool? IsNew;
    private bool m_isLock;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLifeItem(int biographyId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateIsNewValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNewTagActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBiographyRead(int id)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
