﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldEventMissionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class WorldEventMissionUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./DangerOrNormal", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_dangerUIStateController;
    [AutoBind("./Mission/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Mission/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./Mission/MonsterLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_monsterLevelText;
    [AutoBind("./Mission/Energy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Mission/Reward/Goods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGameObject;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Mission/EnterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enterButton;
    [AutoBind("./Mission/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Mission", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_missionUIStateController;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Char/0/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_charImage;
    private UISpineGraphic m_spineGraphic;

    private WorldEventMissionUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEvent(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRewards(List<Goods> goodsList)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharImage(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyCharImage()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnBackgroundButtonClick()
    {
      this.OnCancelButtonClick();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
    }

    public event Action EventOnEnterMission
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
