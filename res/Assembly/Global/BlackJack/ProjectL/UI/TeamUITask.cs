﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TeamUIController m_teamUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private CreateTeamRoomUIController m_createTeamRoomUIController;
    private TeamRoomInfoUITask m_teamRoomInfoUITask;
    private GameFunctionType m_lastRefreshGameFunctionType;
    private int m_lastRefreshChapterId;
    private int m_lastRefreshLocationId;
    private DateTime m_lastRefreshTeamRoomTime;
    private int m_collectionActivityId;
    [DoNotToLua]
    private TeamUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_InitTeamUIController_hotfix;
    private LuaFunction m_UninitTeamUIController_hotfix;
    private LuaFunction m_InitCreateTeamRoomUIController_hotfix;
    private LuaFunction m_UninitCreateTeamRoomUIController_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_StartTeamRoomInviteNetTaskList`1_hotfix;
    private LuaFunction m_CheckTeamRoomInviteAgain_hotfix;
    private LuaFunction m_CheckOpenTeamRoomInfoUI_hotfix;
    private LuaFunction m_StartTeamRoomInfoUITask_hotfix;
    private LuaFunction m_TeamRoomInfoUITask_OnClose_hotfix;
    private LuaFunction m_RefreshTeamRoomBoolean_hotfix;
    private LuaFunction m_TeamUIController_OnReturn_hotfix;
    private LuaFunction m_TeamUIController_OnShowHelp_hotfix;
    private LuaFunction m_TeamUIController_OnRefreshTeamRoom_hotfix;
    private LuaFunction m_TeamUIController_OnShowCreateTeamRoom_hotfix;
    private LuaFunction m_TeamUIController_OnAutoMatch_hotfix;
    private LuaFunction m_TeamUIController_OnAutoMatchCancel_hotfix;
    private LuaFunction m_AutoMatchCancelDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_TeamUIController_OnSelectGameFunctionTypeAndLocationGameFunctionTypeInt32_hotfix;
    private LuaFunction m_TeamUIController_OnJoinTeamRoomInt32GameFunctionTypeInt32_hotfix;
    private LuaFunction m_HandleJoinTeamRoomResultErrorInt32_hotfix;
    private LuaFunction m_CreateTeamRoomUIController_OnCreateTeamRoomTeamRoomSetting_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomAutoMatchInfoNtfInt32_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UIIntentReturnable CreateIntent(
      UIIntent fromIntent,
      GameFunctionType gameFunctionType = GameFunctionType.GameFunctionType_None,
      int chapterId = 0,
      int locationId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCreateTeamRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitCreateTeamRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInviteNetTask(List<string> userIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckTeamRoomInviteAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenTeamRoomInfoUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshTeamRoom(bool checkRefreshTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnRefreshTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnShowCreateTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnAutoMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnAutoMatchCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoMatchCancelDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnSelectGameFunctionTypeAndLocation(
      GameFunctionType gameFunctionType,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnJoinTeamRoom(
      int roomId,
      GameFunctionType gameFunctionType,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleJoinTeamRoomResultError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateTeamRoomUIController_OnCreateTeamRoom(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomAutoMatchInfoNtf(int frontOfYouWaitingNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return this.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamUITask m_owner;

      public LuaExportHelper(TeamUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public TeamUIController m_teamUIController
      {
        get
        {
          return this.m_owner.m_teamUIController;
        }
        set
        {
          this.m_owner.m_teamUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public CreateTeamRoomUIController m_createTeamRoomUIController
      {
        get
        {
          return this.m_owner.m_createTeamRoomUIController;
        }
        set
        {
          this.m_owner.m_createTeamRoomUIController = value;
        }
      }

      public TeamRoomInfoUITask m_teamRoomInfoUITask
      {
        get
        {
          return this.m_owner.m_teamRoomInfoUITask;
        }
        set
        {
          this.m_owner.m_teamRoomInfoUITask = value;
        }
      }

      public GameFunctionType m_lastRefreshGameFunctionType
      {
        get
        {
          return this.m_owner.m_lastRefreshGameFunctionType;
        }
        set
        {
          this.m_owner.m_lastRefreshGameFunctionType = value;
        }
      }

      public int m_lastRefreshChapterId
      {
        get
        {
          return this.m_owner.m_lastRefreshChapterId;
        }
        set
        {
          this.m_owner.m_lastRefreshChapterId = value;
        }
      }

      public int m_lastRefreshLocationId
      {
        get
        {
          return this.m_owner.m_lastRefreshLocationId;
        }
        set
        {
          this.m_owner.m_lastRefreshLocationId = value;
        }
      }

      public DateTime m_lastRefreshTeamRoomTime
      {
        get
        {
          return this.m_owner.m_lastRefreshTeamRoomTime;
        }
        set
        {
          this.m_owner.m_lastRefreshTeamRoomTime = value;
        }
      }

      public int m_collectionActivityId
      {
        get
        {
          return this.m_owner.m_collectionActivityId;
        }
        set
        {
          this.m_owner.m_collectionActivityId = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void InitTeamUIController()
      {
        this.m_owner.InitTeamUIController();
      }

      public void UninitTeamUIController()
      {
        this.m_owner.UninitTeamUIController();
      }

      public void InitCreateTeamRoomUIController()
      {
        this.m_owner.InitCreateTeamRoomUIController();
      }

      public void UninitCreateTeamRoomUIController()
      {
        this.m_owner.UninitCreateTeamRoomUIController();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void StartTeamRoomInviteNetTask(List<string> userIds)
      {
        this.m_owner.StartTeamRoomInviteNetTask(userIds);
      }

      public bool CheckTeamRoomInviteAgain()
      {
        return this.m_owner.CheckTeamRoomInviteAgain();
      }

      public bool CheckOpenTeamRoomInfoUI()
      {
        return this.m_owner.CheckOpenTeamRoomInfoUI();
      }

      public void StartTeamRoomInfoUITask()
      {
        this.m_owner.StartTeamRoomInfoUITask();
      }

      public void TeamRoomInfoUITask_OnClose()
      {
        this.m_owner.TeamRoomInfoUITask_OnClose();
      }

      public void RefreshTeamRoom(bool checkRefreshTime)
      {
        this.m_owner.RefreshTeamRoom(checkRefreshTime);
      }

      public void TeamUIController_OnReturn()
      {
        this.m_owner.TeamUIController_OnReturn();
      }

      public void TeamUIController_OnShowHelp()
      {
        this.m_owner.TeamUIController_OnShowHelp();
      }

      public void TeamUIController_OnRefreshTeamRoom()
      {
        this.m_owner.TeamUIController_OnRefreshTeamRoom();
      }

      public void TeamUIController_OnShowCreateTeamRoom()
      {
        this.m_owner.TeamUIController_OnShowCreateTeamRoom();
      }

      public void TeamUIController_OnAutoMatch()
      {
        this.m_owner.TeamUIController_OnAutoMatch();
      }

      public void TeamUIController_OnAutoMatchCancel()
      {
        this.m_owner.TeamUIController_OnAutoMatchCancel();
      }

      public void AutoMatchCancelDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.AutoMatchCancelDialogBoxCallback(r);
      }

      public void TeamUIController_OnSelectGameFunctionTypeAndLocation(
        GameFunctionType gameFunctionType,
        int locationId)
      {
        this.m_owner.TeamUIController_OnSelectGameFunctionTypeAndLocation(gameFunctionType, locationId);
      }

      public void TeamUIController_OnJoinTeamRoom(
        int roomId,
        GameFunctionType gameFunctionType,
        int locationId)
      {
        this.m_owner.TeamUIController_OnJoinTeamRoom(roomId, gameFunctionType, locationId);
      }

      public void HandleJoinTeamRoomResultError(int result)
      {
        this.m_owner.HandleJoinTeamRoomResultError(result);
      }

      public void CreateTeamRoomUIController_OnCreateTeamRoom(TeamRoomSetting setting)
      {
        this.m_owner.CreateTeamRoomUIController_OnCreateTeamRoom(setting);
      }

      public void PlayerContext_OnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf()
      {
        this.m_owner.PlayerContext_OnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf();
      }

      public void PlayerContext_OnTeamRoomAutoMatchInfoNtf(int frontOfYouWaitingNums)
      {
        this.m_owner.PlayerContext_OnTeamRoomAutoMatchInfoNtf(frontOfYouWaitingNums);
      }

      public void PlayerContext_OnPlayerInfoInitEnd()
      {
        this.m_owner.PlayerContext_OnPlayerInfoInitEnd();
      }
    }
  }
}
