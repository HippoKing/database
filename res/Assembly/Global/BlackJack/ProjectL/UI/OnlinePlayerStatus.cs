﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.OnlinePlayerStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class OnlinePlayerStatus
  {
    [DoNotToLua]
    private OnlinePlayerStatus.LuaExportHelper luaExportHelper;

    [DoNotToLua]
    public OnlinePlayerStatus.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    public class LuaExportHelper
    {
      private OnlinePlayerStatus m_owner;

      public LuaExportHelper(OnlinePlayerStatus owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
