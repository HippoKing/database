﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScrollSnapCenter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ScrollSnapCenter : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
  {
    public float m_itemSize;
    public ScrollSnapDirection m_scrollSnapDir;
    public ScrollSnapPageType m_scrollSnapPageType;
    public float m_dragPercentage;
    private bool m_isSnaping;
    private Vector2 m_snapPosition;
    private int m_itemCount;
    private ScrollRect m_scrollRect;
    private int m_currentCenterIndex;
    private Vector2 m_beginDragPosition;
    private Vector2 m_endFragPosition;
    [DoNotToLua]
    private ScrollSnapCenter.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_LateUpdate_hotfix;
    private LuaFunction m_SetItemCountInt32_hotfix;
    private LuaFunction m_GetItemCount_hotfix;
    private LuaFunction m_ComputeItemPositionInt32_hotfix;
    private LuaFunction m_GetCenterItemIndexUnclamped_hotfix;
    private LuaFunction m_GetCenterItemIndex_hotfix;
    private LuaFunction m_SnapInt32Boolean_hotfix;
    private LuaFunction m_GetItemIndexByDragDistance_hotfix;
    private LuaFunction m_ComputeSnapCenterPositionInt32_hotfix;
    private LuaFunction m_SnapVector2_hotfix;
    private LuaFunction m_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_add_EventOnCenterItemChangedAction`1_hotfix;
    private LuaFunction m_remove_EventOnCenterItemChangedAction`1_hotfix;
    private LuaFunction m_add_EventOnEndDragAction_hotfix;
    private LuaFunction m_remove_EventOnEndDragAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ScrollSnapCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetItemCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 ComputeItemPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCenterItemIndexUnclamped()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCenterItemIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Snap(int idx, bool smooth = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetItemIndexByDragDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeSnapCenterPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Snap(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnCenterItemChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ScrollSnapCenter.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCenterItemChanged(int obj)
    {
    }

    private void __clearDele_EventOnCenterItemChanged(int obj)
    {
      this.EventOnCenterItemChanged = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndDrag()
    {
      this.EventOnEndDrag = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ScrollSnapCenter m_owner;

      public LuaExportHelper(ScrollSnapCenter owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnCenterItemChanged(int obj)
      {
        this.m_owner.__callDele_EventOnCenterItemChanged(obj);
      }

      public void __clearDele_EventOnCenterItemChanged(int obj)
      {
        this.m_owner.__clearDele_EventOnCenterItemChanged(obj);
      }

      public void __callDele_EventOnEndDrag()
      {
        this.m_owner.__callDele_EventOnEndDrag();
      }

      public void __clearDele_EventOnEndDrag()
      {
        this.m_owner.__clearDele_EventOnEndDrag();
      }

      public bool m_isSnaping
      {
        get
        {
          return this.m_owner.m_isSnaping;
        }
        set
        {
          this.m_owner.m_isSnaping = value;
        }
      }

      public Vector2 m_snapPosition
      {
        get
        {
          return this.m_owner.m_snapPosition;
        }
        set
        {
          this.m_owner.m_snapPosition = value;
        }
      }

      public int m_itemCount
      {
        get
        {
          return this.m_owner.m_itemCount;
        }
        set
        {
          this.m_owner.m_itemCount = value;
        }
      }

      public ScrollRect m_scrollRect
      {
        get
        {
          return this.m_owner.m_scrollRect;
        }
        set
        {
          this.m_owner.m_scrollRect = value;
        }
      }

      public int m_currentCenterIndex
      {
        get
        {
          return this.m_owner.m_currentCenterIndex;
        }
        set
        {
          this.m_owner.m_currentCenterIndex = value;
        }
      }

      public Vector2 m_beginDragPosition
      {
        get
        {
          return this.m_owner.m_beginDragPosition;
        }
        set
        {
          this.m_owner.m_beginDragPosition = value;
        }
      }

      public Vector2 m_endFragPosition
      {
        get
        {
          return this.m_owner.m_endFragPosition;
        }
        set
        {
          this.m_owner.m_endFragPosition = value;
        }
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }

      public void LateUpdate()
      {
        this.m_owner.LateUpdate();
      }

      public int GetItemIndexByDragDistance()
      {
        return this.m_owner.GetItemIndexByDragDistance();
      }

      public Vector2 ComputeSnapCenterPosition(int idx)
      {
        return this.m_owner.ComputeSnapCenterPosition(idx);
      }

      public void Snap(Vector2 p)
      {
        this.m_owner.Snap(p);
      }
    }
  }
}
