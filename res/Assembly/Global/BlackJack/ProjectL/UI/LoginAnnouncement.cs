﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginAnnouncement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class LoginAnnouncement
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public LoginAnnouncement(LoginAnnouncement.AnnounceType type, string title, string content)
    {
    }

    public string Title { get; set; }

    public string Content { get; set; }

    public LoginAnnouncement.AnnounceType CurrentType { get; set; }

    public enum AnnounceType
    {
      Notice = 1,
      Activity = 2,
      None = 3,
    }
  }
}
