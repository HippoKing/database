﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailJobUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroDetailJobUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./InfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelStateCtrl;
    [AutoBind("./InfoPanel/JobDetail/JobNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobNameText;
    [AutoBind("./InfoPanel/JobDetail/JobDescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobDescText;
    [AutoBind("./InfoPanel/JobDetail/ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobArmyImage;
    [AutoBind("./InfoPanel/JobUpgrade", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobUpgradeGo;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/CurLv/LvImgs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobLv;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/CurLv/LvImgsBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobLvBg;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/NextLv", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextJobLvObj;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/NextLv/LvImgs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextJobLv;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/NextLv/LvImgsBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextJobLvBg;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/Master", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMasterImg;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvArrowObj;
    [AutoBind("./InfoPanel/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpGameObject;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Skill", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLearnedSkill;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Skill/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLearnedSkillIconImage;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Skill/SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobLearnedSkillNameText;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLearnedSoldier;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Soldier/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLearnedSoldierIconImage;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Soldier/SoldierNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobLearnedSoldierNameText;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Master", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLearnedMaster;
    [AutoBind("./InfoPanel/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobDescContent;
    [AutoBind("./JobEffectGroup/MasterRewardPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterRewardPanel;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/JobIconGroup/JobIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_masterRewardPanelJobIcon;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/JobIconGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_masterRewardPanelNameText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propHPAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propATAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propDFAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propMagicAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propMagicDFAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propDEXAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propHPAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propATAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propDFAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propMagicAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propMagicDFAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DEX", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propDEXAddObj;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj1;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj2;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj3;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobLvUpgradeButton;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/LvLimit", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_jobMaterialLvLimitCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/LvLimit/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobMaterialLvLimitText;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/UpgradeButton/ArtEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeBtnArtEffect;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeFrameEffect;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeFlyEffect;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect1/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon1;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect2/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon2;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect3/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon3;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect4/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon4;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/StarUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeStarUpEffect;
    [AutoBind("./JobEffectGroup/MasterRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMasterRewardGroupGo;
    [AutoBind("./JobEffectGroup/MasterRewardGroup/ReturnBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobMasterRewardGroupGoReturnButton;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpgradeGroupGo;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/ContinueBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobUpgradeGroupGoContinueBGButton;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Hp/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelHpText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Hp/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelHpAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/AT/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelATText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/AT/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelATAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Magic/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Magic/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/DF/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDFText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/DF/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDFAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/MagicDF/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicDFText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/MagicDF/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicDFAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Dex/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDexText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Dex/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDexAddText;
    [AutoBind("./LearnedPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanel;
    [AutoBind("./LearnedPanel/CloseImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_learnedPanelCloseBtn;
    [AutoBind("./LearnedPanel/Skill/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_learnedPanelSkillCloseBtn;
    [AutoBind("./LearnedPanel/Skill", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSkillPanel;
    [AutoBind("./LearnedPanel/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSoldierPanel;
    [AutoBind("./LearnedPanel/Skill/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_learnedPanelSkillIcon;
    [AutoBind("./LearnedPanel/Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSoldierGraphicObj;
    [AutoBind("./LearnedPanel/Skill/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_learnedPanelSkillName;
    [AutoBind("./LearnedPanel/Soldier/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_learnedPanelSoldierName;
    private Hero m_hero;
    private List<GameObject> m_jobMaterialObjList;
    private UISpineGraphic m_learndSoldierGraphic;
    private ConfigDataSkillInfo m_learnSkillInfo;
    private ConfigDataSoldierInfo m_learnSoliderInfo;
    private List<Goods> m_curjobNeedMaterials;
    private bool m_isCloseJobUpgradePanel;
    private bool m_isCloseJobMasterRewardPanel;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInJobState(Hero hero)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowJobDetailPanel()
    {
    }

    private void ShowJobUpgradePanel()
    {
      this.SetUpperLimitBreak();
      this.SetPropertyLearned();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUpperLimitBreak()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropertyLearned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLearnedSkillClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLearnedSoldierClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLearnedMasterClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetJobLvMasterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMasterRewardProperty(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowJobMaterialPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialClick(JobMaterialEquipedUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLvUpgradeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnJobLvUpgradeSucceed()
    {
      this.StartCoroutine(this.JobLVUpgradeEffect());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator JobLVUpgradeEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetJobUpgradeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowLearnedSkillAndSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseLearnedSkillPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobUpgradeGroupGoContinueBGButtonClick()
    {
    }

    private void OnJobMasterRewardGroupGoReturnButtonClick()
    {
      this.m_isCloseJobMasterRewardPanel = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseLearnedPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsJobLvUpgradeFinished()
    {
      return this.m_jobLvUpgradeButton.interactable;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAndCloseUIEffect()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int, Action> EventOnJobLvUpgrade
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<GoodsType, int, int> EventOnShowGetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnUpdateView
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
