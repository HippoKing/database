﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftChapterUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftChapterUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./WorldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_worldButton;
    [AutoBind("./Chapters/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_chaptersScrollRect;
    [AutoBind("./UnlockCondition", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockConditionUIStateCtrl;
    [AutoBind("./UnlockCondition", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlockConditionGameObject;
    [AutoBind("./UnlockCondition/Panel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlockConditionGroup;
    [AutoBind("./UnlockCondition/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockConditionBGButton;
    [AutoBind("./Margin/Progress", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressGameObject;
    [AutoBind("./Margin/Progress/BGImages/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressTitleText;
    [AutoBind("./Margin/Progress/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressNameText;
    [AutoBind("./Margin/Progress/PlaceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressPlaceText;
    [AutoBind("./Margin/Progress/Normal/StarText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressNormalStarText;
    [AutoBind("./Margin/Progress/Normal/CupText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressNormalAchievementText;
    [AutoBind("./Margin/Progress/Normal/ChestText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressNormalTreasureText;
    [AutoBind("./Margin/Progress/Elite/StarText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressHardStarText;
    [AutoBind("./Margin/Progress/Elite/CupText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressHardAchievementText;
    [AutoBind("./Margin/Progress/Elite/ChestText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressHardTreasureText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RiftChapterButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_riftChapterButtonPrefab;
    [AutoBind("./Prefabs/RiftChapterUnlockConditionItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_riftChapterUnlockConditionItemPrefab;
    private List<RiftChapterButton> m_chapterButtons;
    private List<RiftChapterUnlockConditionItemUIController> m_riftChapterUnlockConditionItems;
    private int m_curChapterIndex;
    private ScrollSnapCenter m_chaptersScrollSnapCenter;

    [MethodImpl((MethodImplOptions) 32768)]
    private RiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearChapterButtons()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChapterButton(
      ConfigDataRiftChapterInfo chapterInfo,
      bool locked,
      bool newChapter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChapterProgress(
      ConfigDataRiftChapterInfo chapterInfo,
      int normalStarCount,
      int normalStarCountMax,
      int normalAchivementCount,
      int normalAchievementCountMax,
      int normalTreasureCount,
      int normalTreasureCountMax,
      int hardStarCount,
      int hardStarCountMax,
      int hardAchivementCount,
      int hardAchievementCountMax,
      int hardTreasureCount,
      int hardTreasureCountMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockConditionBGButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapterUnlockConditionClose()
    {
    }

    public void HideChapterProgress()
    {
      this.m_progressGameObject.SetActive(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChapterUnlockConditions(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRiftLevelUnlockContitionItems()
    {
      // ISSUE: unable to decompile the method.
    }

    private void HideChapterUnlockConditions()
    {
      this.m_unlockConditionGameObject.SetActive(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWorldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChapterButtonClick(RiftChapterButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturnToWorld
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftChapterInfo> EventOnSelectChapter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ConfigDataRiftChapterInfo> EventOnSwitchChapter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
