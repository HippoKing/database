﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RafflePool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RafflePool
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool(int poolId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRafflePool> RafflePools2PbActivityRafflePools(
      List<RafflePool> rafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ProRafflePool RafflePool2PbRafflePool(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RafflePool PBRafflePoolToRafflePool(ProRafflePool pbRafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RafflePool> PBRafflePoolsToRafflePools(
      List<ProRafflePool> pbRafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Drawed(int darwedRaffleId)
    {
      this.DrawedRaffleIds.Add(darwedRaffleId);
    }

    public ulong ActivityInstanceId { get; set; }

    public int PoolId { get; set; }

    public HashSet<int> DrawedRaffleIds { get; set; }

    public int DrawedCount { get; set; }

    public int FreeDrawedCount { get; set; }

    public ConfigDataRafflePoolInfo Config { get; set; }
  }
}
