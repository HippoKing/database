﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RefluxComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RefluxComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected NoviceComponentCommon m_novice;
    protected MissionComponentCommon m_mission;
    protected TimeSpan m_RefluxMissionDuration;
    protected List<ConfigDataRefluxRewardInfo> m_RefluxPointsRewards;
    protected DataSectionReflux m_RefluxDS;
    public long maxDeactivateTime;
    public long minActivateTime;
    private IConfigDataLoader _configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public RefluxComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Reflux";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ClaimReward(int Slot, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimReward(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetMissionPoints()
    {
      return this.m_RefluxDS.MissionPoints;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardClaimed(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDaysAfterOpening()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddMissionPoints(int Delta)
    {
      this.m_RefluxDS.AddMissionPoints(Delta);
    }

    public List<Mission> GetProcessingMissions()
    {
      return this.m_mission.GetAllProcessingRefluxMissionList();
    }

    public List<Mission> GetFinishedMissions()
    {
      return this.m_mission.GetAllFinishedRefluxMissionList();
    }

    public List<ConfigDataRefluxRewardInfo> GetRefluxPointsRewardsConfig()
    {
      return this.m_RefluxPointsRewards;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetMissionDay(ConfigDataMissionInfo Mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<long, List<long>> GetMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetMissionsEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      get
      {
        return this._configDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
