﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CapsuleCollisionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "CapsuleCollisionInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class CapsuleCollisionInfo : IExtensible
  {
    private PVector3D _pointA;
    private PVector3D _pointB;
    private float _radius;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CapsuleCollisionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "pointA")]
    public PVector3D PointA
    {
      get
      {
        return this._pointA;
      }
      set
      {
        this._pointA = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "pointB")]
    public PVector3D PointB
    {
      get
      {
        return this._pointB;
      }
      set
      {
        this._pointB = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "radius")]
    public float Radius
    {
      get
      {
        return this._radius;
      }
      set
      {
        this._radius = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
