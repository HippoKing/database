﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroDungeonComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class HeroDungeonComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected DataSectionHeroDungeon m_heroDungeonDS;
    protected IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroDungeonComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_OnFlushLevelChallengeNumsEvent_hotfix;
    private LuaFunction m_HasGotAchievementRelationIdInt32_hotfix;
    private LuaFunction m_IsLevelFirstPassInt32_hotfix;
    private LuaFunction m_IsFinishedLevelInt32_hotfix;
    private LuaFunction m_IsEnoughAttackNumsConfigDataHeroDungeonLevelInfoInt32_hotfix;
    private LuaFunction m_GetLevelCanChallengeMaxNumsConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_GetDailyChallengeMaxNums_hotfix;
    private LuaFunction m_InitLevelInt32Int32Int32Int32_hotfix;
    private LuaFunction m_SetLevelInt32Int32Int32Int32_hotfix;
    private LuaFunction m_FindLevelInt32Int32_hotfix;
    private LuaFunction m_AttackHeroDungeonLevelInt32_hotfix;
    private LuaFunction m_SetSuccessHeroDungeonLevelConfigDataHeroDungeonLevelInfoList`1Int32List`1_hotfix;
    private LuaFunction m_SetRaidSuccessHeroDungeonLevelConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_CanAttackHeroDungeonLevelInt32_hotfix;
    private LuaFunction m_CanRaidHeroDungeonLevelInt32_hotfix;
    private LuaFunction m_CanAttackLevelByEnergyAndSoOnConfigDataHeroDungeonLevelInfoBoolean_hotfix;
    private LuaFunction m_CanAttackLevelByRewardsConfigDataHeroDungeonLevelInfoBoolean_hotfix;
    private LuaFunction m_CanUnlockHeroDungeonLevelInt32_hotfix;
    private LuaFunction m_GetHeroDungeonChapterStarInt32_hotfix;
    private LuaFunction m_HasGotChapterStarRewardInt32Int32_hotfix;
    private LuaFunction m_CanGainHeroDungeonChapterStarRewardsInt32Int32_hotfix;
    private LuaFunction m_GetHeroDungeonChapterStarRewardsConfigDataHeroInformationInfoInt32_hotfix;
    private LuaFunction m_GainHeroDungeonChapterStarRewardsInt32Int32Boolean_hotfix;
    private LuaFunction m_GenerateHeroDungeonChapterStarRewardsInt32Int32_hotfix;
    private LuaFunction m_get_HasRewardAddRelativeOperationalActivity_hotfix;
    private LuaFunction m_set_HasRewardAddRelativeOperationalActivityBoolean_hotfix;
    private LuaFunction m_get_OperationalActivityChanllengenumsAdd_hotfix;
    private LuaFunction m_set_OperationalActivityChanllengenumsAddInt32_hotfix;
    private LuaFunction m_add_CompleteHeroDungeonLevelMissionEventAction`3_hotfix;
    private LuaFunction m_remove_CompleteHeroDungeonLevelMissionEventAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushLevelChallengeNumsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotAchievementRelationId(int achievementRelationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFirstPass(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinishedLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEnoughAttackNums(ConfigDataHeroDungeonLevelInfo levelInfo, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelCanChallengeMaxNums(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengeMaxNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLevel(int chapterId, int levelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetLevel(int chapterId, int LevelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevel FindLevel(int chapterId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetSuccessHeroDungeonLevel(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      List<int> newGotAchievementRelationInds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaidSuccessHeroDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByRewards(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonChapterStar(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Goods> GetHeroDungeonChapterStarRewards(
      ConfigDataHeroInformationInfo chapterInfo,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainHeroDungeonChapterStarRewards(int chapterId, int index, bool check = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasRewardAddRelativeOperationalActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OperationalActivityChanllengenumsAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType, int, List<int>> CompleteHeroDungeonLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDungeonComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteHeroDungeonLevelMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteHeroDungeonLevelMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      this.CompleteHeroDungeonLevelMissionEvent = (Action<BattleType, int, List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDungeonComponentCommon m_owner;

      public LuaExportHelper(HeroDungeonComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteHeroDungeonLevelMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__callDele_CompleteHeroDungeonLevelMissionEvent(arg1, arg2, arg3);
      }

      public void __clearDele_CompleteHeroDungeonLevelMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__clearDele_CompleteHeroDungeonLevelMissionEvent(arg1, arg2, arg3);
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public DataSectionHeroDungeon m_heroDungeonDS
      {
        get
        {
          return this.m_owner.m_heroDungeonDS;
        }
        set
        {
          this.m_owner.m_heroDungeonDS = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnFlushLevelChallengeNumsEvent()
      {
        this.m_owner.OnFlushLevelChallengeNumsEvent();
      }

      public bool IsEnoughAttackNums(ConfigDataHeroDungeonLevelInfo levelInfo, int nums)
      {
        return this.m_owner.IsEnoughAttackNums(levelInfo, nums);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void InitLevel(int chapterId, int levelId, int stars, int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetLevel(int chapterId, int LevelId, int stars, int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetSuccessHeroDungeonLevel(
        ConfigDataHeroDungeonLevelInfo levelInfo,
        List<int> newGotAchievementRelationInds,
        int stars,
        List<int> battleTreasures)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetRaidSuccessHeroDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
      {
        this.m_owner.SetRaidSuccessHeroDungeonLevel(levelInfo);
      }

      public int CanAttackLevelByEnergyAndSoOn(
        ConfigDataHeroDungeonLevelInfo levelInfo,
        bool isRaid)
      {
        return this.m_owner.CanAttackLevelByEnergyAndSoOn(levelInfo, isRaid);
      }

      public int CanAttackLevelByRewards(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
      {
        return this.m_owner.CanAttackLevelByRewards(levelInfo, isRaid);
      }

      public List<Goods> GetHeroDungeonChapterStarRewards(
        ConfigDataHeroInformationInfo chapterInfo,
        int index)
      {
        return this.m_owner.GetHeroDungeonChapterStarRewards(chapterInfo, index);
      }

      public void GenerateHeroDungeonChapterStarRewards(int chapterId, int index)
      {
        this.m_owner.GenerateHeroDungeonChapterStarRewards(chapterId, index);
      }
    }
  }
}
