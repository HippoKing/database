﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.EquipmentBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class EquipmentBagItem : BagItemBase
  {
    public List<CommonBattleProperty> EnchantProperties;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentBagItem(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEnchant()
    {
      return this.ResonanceId != 0;
    }

    public int Level { get; set; }

    public int Exp { get; set; }

    public int StarLevel { get; set; }

    public bool Locked { get; set; }

    public int ResonanceId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentEnhanced()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEquipmentLevelUped(int bornStarLevel)
    {
      return this.StarLevel > bornStarLevel;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroEquipment ToBattleHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ProGoods ToPBGoods()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
