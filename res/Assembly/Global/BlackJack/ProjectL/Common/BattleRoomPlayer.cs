﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleRoomPlayer
  {
    public List<BattleHero> Heroes;
    public List<TrainingTech> Techs;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer()
    {
    }

    public string UserId { get; set; }

    public ulong SessionId { get; set; }

    public int ChannelId { get; set; }

    public string Name { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    public RealTimePVPInfo RTPVPInfo { get; set; }

    public bool IsOffline { get; set; }

    public PlayerBattleStatus PlayerBattleStatus { get; set; }

    public bool Blessing { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero FindBattleHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleRoomPlayer BattleRoomPlayerToPbBattleRoomPlayer(
      BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleRoomPlayer PbBattleRoomPlayerToBattleRoomPlayer(
      ProBattleRoomPlayer pbPlayer)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
