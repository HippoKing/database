﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBStargateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "GDBStargateInfo")]
  [Serializable]
  public class GDBStargateInfo : IExtensible
  {
    private int _Id;
    private int _ConfTempleteId;
    private double _locationX;
    private double _locationZ;
    private uint _rotationX;
    private uint _rotationY;
    private int _destSolarSystemId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBStargateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConfTempleteId")]
    public int ConfTempleteId
    {
      get
      {
        return this._ConfTempleteId;
      }
      set
      {
        this._ConfTempleteId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "locationX")]
    public double LocationX
    {
      get
      {
        return this._locationX;
      }
      set
      {
        this._locationX = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "locationZ")]
    public double LocationZ
    {
      get
      {
        return this._locationZ;
      }
      set
      {
        this._locationZ = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "rotationX")]
    public uint RotationX
    {
      get
      {
        return this._rotationX;
      }
      set
      {
        this._rotationX = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "rotationY")]
    public uint RotationY
    {
      get
      {
        return this._rotationY;
      }
      set
      {
        this._rotationY = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "destSolarSystemId")]
    public int DestSolarSystemId
    {
      get
      {
        return this._destSolarSystemId;
      }
      set
      {
        this._destSolarSystemId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
