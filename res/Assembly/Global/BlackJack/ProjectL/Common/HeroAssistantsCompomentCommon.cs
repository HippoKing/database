﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAssistantsCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class HeroAssistantsCompomentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected List<BagItemBase> m_changedGoods;
    protected DataSectionHeroAssistants m_heroAssistantsDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected RiftComponentCommon m_rift;
    [DoNotToLua]
    private HeroAssistantsCompomentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IsHeroAssignedInt32_hotfix;
    private LuaFunction m_CanAssignHeroList`1Int32Int32Int32_hotfix;
    private LuaFunction m_AssignHeroList`1Int32Int32Int32Boolean_hotfix;
    private LuaFunction m_CanCancelTaskInt32Int32_hotfix;
    private LuaFunction m_CancelTaskInt32Int32Boolean_hotfix;
    private LuaFunction m_CanClaimRewardsInt32Int32_hotfix;
    private LuaFunction m_ClaimRewardsInt32Int32Boolean_hotfix;
    private LuaFunction m_GetTaskRemainingTimeInt32Int32_hotfix;
    private LuaFunction m_GetDropIdByTaskCompleteRateInt32Int32_hotfix;
    private LuaFunction m_GetDropCountByTaskWorkSecondsInt32Int32_hotfix;
    private LuaFunction m_get_m_configDataLoader_hotfix;
    private LuaFunction m_set_m_configDataLoaderIConfigDataLoader_hotfix;
    private LuaFunction m_add_AssignHeroToTaskMissionEventAction_hotfix;
    private LuaFunction m_remove_AssignHeroToTaskMissionEventAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantsCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroAssigned(int HeroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAssignHero(List<int> HeroIds, int TaskId, int Slot, int WorkSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AssignHero(
      List<int> HeroIds,
      int TaskId,
      int Slot,
      int WorkSeconds,
      bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCancelTask(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CancelTask(int TaskId, int Slot, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimRewards(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ClaimRewards(int TaskId, int Slot, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetTaskRemainingTime(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDropIdByTaskCompleteRate(int TaskId, int CompleteRate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDropCountByTaskWorkSeconds(int TaskId, int WorkSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action AssignHeroToTaskMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroAssistantsCompomentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AssignHeroToTaskMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_AssignHeroToTaskMissionEvent()
    {
      this.AssignHeroToTaskMissionEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAssistantsCompomentCommon m_owner;

      public LuaExportHelper(HeroAssistantsCompomentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_AssignHeroToTaskMissionEvent()
      {
        this.m_owner.__callDele_AssignHeroToTaskMissionEvent();
      }

      public void __clearDele_AssignHeroToTaskMissionEvent()
      {
        this.m_owner.__clearDele_AssignHeroToTaskMissionEvent();
      }

      public IConfigDataLoader _configDataLoader
      {
        get
        {
          return this.m_owner._configDataLoader;
        }
        set
        {
          this.m_owner._configDataLoader = value;
        }
      }

      public List<BagItemBase> m_changedGoods
      {
        get
        {
          return this.m_owner.m_changedGoods;
        }
        set
        {
          this.m_owner.m_changedGoods = value;
        }
      }

      public DataSectionHeroAssistants m_heroAssistantsDS
      {
        get
        {
          return this.m_owner.m_heroAssistantsDS;
        }
        set
        {
          this.m_owner.m_heroAssistantsDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }
    }
  }
}
