﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TeamRoomPlayer
  {
    public string UserId { get; set; }

    public ulong SessionId { get; set; }

    public int ChannelId { get; set; }

    public string Name { get; set; }

    public int HeadIcon { get; set; }

    public int ActiveHeroJobRelatedId { get; set; }

    public int Level { get; set; }

    public int Position { get; set; }

    public int ModenSkinId { get; set; }

    public bool Blessing { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoomPlayer TeamRoomPlayerToPbTeamRoomPlayer(
      TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoomPlayer PbTeamRoomPlayerToTeamRoomPlayer(
      ProTeamRoomPlayer pbPlayer)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
