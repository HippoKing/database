﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CooperateBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CooperateBattle
  {
    public CooperateBattleCollections WhichBattleCollections;
    public List<CooperateBattleLevel> Levels;
    private IConfigDataLoader _ConfigDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattle()
    {
    }

    public int ID { get; set; }

    public string Name
    {
      get
      {
        return this.Config.Name;
      }
    }

    public string Description
    {
      get
      {
        return this.Config.Desc;
      }
    }

    public DateTime OpenDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime CloseDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime OpenDateTime2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime CloseDateTime2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<DayOfWeek> ShowWeekDays
    {
      get
      {
        return this.Config.OpenDaysOfWeek;
      }
    }

    public int ChallengedNums { get; set; }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForDisplay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCooperateBattleInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      get
      {
        return this._ConfigDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
