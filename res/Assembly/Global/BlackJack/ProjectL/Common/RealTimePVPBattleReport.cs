﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RealTimePVPBattleReport : BattleReport
  {
    public RealTimePVPBattleReportPlayerData[] PlayerDatas;
    public bool Win;
    public bool IsCancel;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport(BattleReport battleReport)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport DeepCopy()
    {
      // ISSUE: unable to decompile the method.
    }

    public RealTimePVPBattleReportType ReportType { get; set; }

    public BattleRoomBPRule BPRule { get; set; }

    public DateTime CreateTime { get; set; }
  }
}
