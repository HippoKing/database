﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RiftComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RiftComponentCommon : IComponentBase
  {
    public Action<int> RiftLevelCompleteEvent;
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected LevelComponentCommon m_level;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected BattleComponentCommon m_battle;
    protected DataSectionRift m_riftDS;
    public RiftLevelUnLockInfo m_unLockInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftComponentCommon()
    {
    }

    public string GetName()
    {
      return "Rift";
    }

    public virtual void Init()
    {
      this.OperationalActivityChanllengenumsAdd = 0;
      this.HasRewardAddRelativeOperationalActivity = false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    private void OnFlushChallengeCountEvent()
    {
      this.ResetLevelChallengeNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChallengeNums(ConfigDataRiftLevelInfo levelInfo, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ResetLevelChallengeNums()
    {
      this.m_riftDS.ResetLevelChallengeNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetAllRiftLevelAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetLevel(int chapterId, int levelId, int nums, int stars, bool needAddStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddChapterTotalStar(RiftChapter chapter, int addStar)
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsGameFunctionOpened()
    {
      return this.m_basicInfo.IsGameFunctionOpened(GameFunctionType.GameFunctionType_Rift);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstPassLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(ConfigDataRiftLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAddRandomDropRewardExpectedSize(int randomDropId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnLockLevel(int riftLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEnoughAttackNums(ConfigDataRiftLevelInfo levelInfo, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetRiftLevelCanChallengeMaxNums(ConfigDataRiftLevelInfo levelInfo)
    {
      return levelInfo.ChallengeCount + this.OperationalActivityChanllengenumsAdd;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidLevel(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void CompleteAchievement(int achievementRelationId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddRiftLevelBasicRewards(ConfigDataRiftLevelInfo riftLevelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetSuccessRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo,
      List<int> newAchievementIds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaidSuccessRiftLevel(ConfigDataRiftLevelInfo levelInfo)
    {
    }

    public bool HasGotAchievementRelationId(int achievementId)
    {
      return this.m_riftDS.HasGotAchievementRelationId(achievementId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Goods> GetChapterRewards(
      ConfigDataRiftChapterInfo chapterInfo,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanGainChapterReward(
      ConfigDataRiftChapterInfo chapterInfo,
      int index,
      List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetChapterTotalStars(int chapterId)
    {
      return this.m_riftDS.GetChapterTotalStars(chapterId);
    }

    public int GetAllRiftLevelStars()
    {
      return this.m_riftDS.GetAllRiftLevelStars();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ComplteRiftLevel(int riftLevelId)
    {
    }

    public bool HasRewardAddRelativeOperationalActivity { get; set; }

    protected virtual void OnAllRiftLevelStarAdd(int addStar)
    {
    }

    protected virtual void OnRiftAchivementAdd(int addAchivement)
    {
    }

    public event Action<BattleType, int, List<int>> CompleteRiftLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> GetRiftLevelAchievementMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> GetRiftLevelFightStarMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public int OperationalActivityChanllengenumsAdd { get; set; }
  }
}
