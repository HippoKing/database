﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnThreadSafeLRUCacheObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class UnThreadSafeLRUCacheObject : CacheObject, ILRUCacheObject
  {
    private long m_lastReadTime;
    private long m_lastUpdateTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastUpdateTime()
    {
    }

    public long GetLastUpdateTime()
    {
      return this.m_lastUpdateTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastReadTime()
    {
    }

    public long GetLastReadTime()
    {
      return this.m_lastReadTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetNewestTime()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
