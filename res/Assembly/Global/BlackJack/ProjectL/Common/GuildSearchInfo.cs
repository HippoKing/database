﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildSearchInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildSearchInfo
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public int MemberCount { get; set; }

    public int JoinLevel { get; set; }

    public int LastWeekActivities { get; set; }

    public string HiringDeclaration { get; set; }

    public GuildMember PresidentMemberInfo { get; set; }

    public bool HaveSendJoinReq { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildSearchInfo ToPBGuildSearchInfo(GuildSearchInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildSearchInfo ToGuildSearchInfo(ProGuildSearchInfo proInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
