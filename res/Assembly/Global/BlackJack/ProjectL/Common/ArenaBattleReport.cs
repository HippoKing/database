﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaBattleReport : BattleReport
  {
    public List<BattleHero> DefenderHeroes;
    public List<TrainingTech> DefenderTechs;
    public List<BattleHero> AttackerHeroes;
    public List<TrainingTech> AttackerTechs;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport(BattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport(ArenaBattleReport other)
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaBattleReportStatus Status { get; set; }

    public int ArenaDefenderRuleId { get; set; }

    public string DefenderUserId { get; set; }

    public string DefenderName { get; set; }

    public int DefenderLevel { get; set; }

    public string AttackerUserId { get; set; }

    public string AttackerName { get; set; }

    public int AttackerLevel { get; set; }

    public int AttackerGotArenaPoints { get; set; }

    public int DefenderGotArenaPoints { get; set; }

    public long CreateTime { get; set; }

    public int OpponentHeadIcon { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaBattleReport PBArenaBattleReportToArenaBattleReport(
      ProArenaBattleReport pbArenaBattleReport,
      bool isBattleData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaBattleReport ArenaBattleReportToPBArenaBattleReport(
      ArenaBattleReport arenaBattleReport,
      bool isBattleData)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
