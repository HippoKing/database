﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UpdateCache`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UpdateCache<T> : CacheObject where T : class
  {
    public UpdateCache(T data)
    {
      this.Data = data;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsValid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetInvalid()
    {
      // ISSUE: unable to decompile the method.
    }

    public T Data { get; set; }
  }
}
