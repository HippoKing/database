﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Mail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class Mail
  {
    public List<Goods> Attachments;
    public List<Goods> TemplateAttachments;

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail(Mail sourceMail)
    {
      // ISSUE: unable to decompile the method.
    }

    public int TemplateId { get; set; }

    public MailType MailTypeId { get; set; }

    public ulong InstanceId { get; set; }

    public int Status { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    public DateTime SendTime { get; set; }

    public DateTime ReadedOrGotAttachmentTime { get; set; }

    public uint ExpiredTime { get; set; }

    public int ReadedExpiredTime { get; set; }

    public bool GotDeleted { get; set; }

    public ConfigDataMailInfo TemplateMailConfig { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProMail MailToPBMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProMail> MailsToPBMails(List<Mail> mails)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Mail PBMailToMail(ProMail pbMail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<Mail> PBMailsToMails(List<ProMail> pbMails)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
