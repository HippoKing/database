﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UseableBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;

namespace BlackJack.ProjectL.Common
{
  public abstract class UseableBagItem : BagItemBase
  {
    public UseableBagItem(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
      : base(goodsTypeId, contentId, nums, instanceId)
    {
    }

    public abstract int HaveEffect(IComponentOwner owner, params object[] param);
  }
}
