﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionFixedStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionFixedStore : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionFixedStore()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.Stores.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStores(List<FixedStore> stores)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStores(List<FixedStore> stores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStore(FixedStore store)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStore(FixedStore store)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetStore(FixedStore newStore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore FindStore(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyStoreItem(FixedStoreItem storeItem, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsFirstBuy(FixedStoreItem storeItem)
    {
      return storeItem.IsFirstBuy;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreItem GetStoreItem(FixedStore store, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, FixedStore> Stores { get; set; }
  }
}
