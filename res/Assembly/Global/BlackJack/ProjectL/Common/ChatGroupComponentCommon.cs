﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatGroupComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ChatGroupComponentCommon : IComponentBase
  {
    public string GetName()
    {
      return "ChatGroup";
    }

    public virtual void Init()
    {
    }

    public virtual void PostInit()
    {
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }
  }
}
