﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionCollection : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionCollection()
    {
    }

    public override void ClearInitedData()
    {
      this.CollectionActivities.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity FindCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionActivity(CollectionActivity activity)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChallengeLevelId(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLootLevelId(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsChallengeLevelFinished(CollectionActivity acitivity, int levelId)
    {
      return acitivity.FinishedChallengeLevelIds.Contains(levelId);
    }

    public bool IsLootLevelFinished(CollectionActivity acitivity, int levelId)
    {
      return acitivity.FinishedLootLevelIds.Contains(levelId);
    }

    public void SetCurrentWayPointId(CollectionActivity acitivity, int waypointId)
    {
      acitivity.CurrentWayPointId = waypointId;
      this.SetDirty(true);
    }

    public void SetScenarioId(CollectionActivity acitivity, int id)
    {
      acitivity.LastFinishedScenarioId = id;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddExchangeCount(CollectionActivity activity, int exchangeID, int delta)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetExchangeCount(CollectionActivity activity, int exchangeID)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<CollectionActivityPlayerExchangeInfo> GetExchangeInfoList(
      CollectionActivity activity)
    {
      return activity.ExchangeInfoList;
    }

    public List<CollectionActivity> CollectionActivities { get; set; }
  }
}
