﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.Common
{
  public enum BattleType
  {
    None,
    WayPoint,
    Rift,
    ThearchyTrial,
    AnikiGym,
    ArenaAttack,
    HeroDungeon,
    Scenario,
    TreasureMap,
    MemoryCorridor,
    HeroTrainning,
    PVP,
    HeroPhantom,
    CooperateBattle,
    RealTimePVP,
    UnchartedScore_ChallengeLevel,
    UnchartedScore_ScoreLevel,
    ClimbTower,
    GuildMassiveCombat,
    TeamOnePerson,
    TeamTwoPerson,
    TeamThreePerson,
    EternalShrine,
    TeamFourPerson,
    TeamFivePerson,
    CollectionActivityScenario,
    CollectionActivityChallenge,
    CollectionActivityLoot,
  }
}
