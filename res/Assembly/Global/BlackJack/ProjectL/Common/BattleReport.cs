﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleReport
  {
    public List<BattleCommand> Commands;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReport()
    {
    }

    public int Version { get; set; }

    public ulong InstanceId { get; set; }

    public BattleType BattleType { get; set; }

    public int BattleId { get; set; }

    public int RandomSeed { get; set; }
  }
}
