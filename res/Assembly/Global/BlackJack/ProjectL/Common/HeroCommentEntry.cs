﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroCommentEntry
  {
    public HeroCommentEntry()
    {
      this.CommentTime = 0L;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentEntry(HeroCommentEntry other)
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId { get; set; }

    public string Content { get; set; }

    public string CommenterUserId { get; set; }

    public string CommenterName { get; set; }

    public int CommenterLevel { get; set; }

    public int PraiseNums { get; set; }

    public long CommentTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroCommentEntry HeroCommentEntryToPBHeroCommentEntry(
      HeroCommentEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroCommentEntry PBHeroCommentEntryToHeroCommentEntry(
      ProHeroCommentEntry pbEntry)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
