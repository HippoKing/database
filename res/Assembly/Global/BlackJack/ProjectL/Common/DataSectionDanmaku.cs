﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionDanmaku
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionDanmaku : DataSection
  {
    public DateTime m_bannedTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionDanmaku()
    {
    }

    public override void ClearInitedData()
    {
      this.m_bannedTime = DateTime.MinValue;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Ban(DateTime bannedTime)
    {
      this.m_bannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
    }
  }
}
