﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class CollectionComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionCollection m_collectionDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    [DoNotToLua]
    private CollectionComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IterateAvailableExchangeItems_hotfix;
    private LuaFunction m_CanExchangeItemUInt64Int32_hotfix;
    private LuaFunction m_ExchangeItemUInt64Int32_hotfix;
    private LuaFunction m_GetCollectionActivityInfoUInt64_hotfix;
    private LuaFunction m_GetActivityInstanceIdGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetCollectionActivityInfoGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanMoveToWayPointInt32_hotfix;
    private LuaFunction m_MoveToWayPointInt32_hotfix;
    private LuaFunction m_CanHandleScenarioInt32_hotfix;
    private LuaFunction m_IsNextScenarioIdUInt64Int32_hotfix;
    private LuaFunction m_GetNextScenarioIdUInt64_hotfix;
    private LuaFunction m_CanAttackLevelGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanAttackScenarioLevelCollectionActivityInt32Int32_hotfix;
    private LuaFunction m_CanAttackChallengeLevelCollectionActivityInt32Int32_hotfix;
    private LuaFunction m_CanAttackLootLevelCollectionActivityInt32Int32_hotfix;
    private LuaFunction m__IsLootLevelUnlockCollectionActivityConfigDataCollectionActivityLootLevelInfoInt32_hotfix;
    private LuaFunction m_IsPrevLevelUnlockCollectionActivityList`1_hotfix;
    private LuaFunction m_IsScenarioFinishedCollectionActivityInt32_hotfix;
    private LuaFunction m_AttackLevelGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetAllUnlockedLootLevels_hotfix;
    private LuaFunction m_IsLootLevelUnlockInt32_hotfix;
    private LuaFunction m_AddFinishedLootLevelCollectionActivityInt32_hotfix;
    private LuaFunction m_SetCommonSuccessLevelCollectionActivityGameFunctionTypeInt32List`1Boolean_hotfix;
    private LuaFunction m_FindCollectionActivityUInt64_hotfix;
    private LuaFunction m_RemoveCollectionActivityOperationalActivityBase_hotfix;
    private LuaFunction m_AddCollectionActivityOperationalActivityBase_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> IterateAvailableExchangeItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo(
      ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetActivityInstanceId(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo(
      GameFunctionType typeId,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanMoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanHandleScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsNextScenarioId(ulong activityInstanceId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextScenarioId(ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackLevel(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackScenarioLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackChallengeLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLootLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int _IsLootLevelUnlock(
      CollectionActivity collectionActivity,
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPrevLevelUnlock(CollectionActivity activity, List<LevelInfo> PrevLevels)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsScenarioFinished(CollectionActivity collectionActivity, int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackLevel(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLootLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelUnlock(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFinishedLootLevel(CollectionActivity collectionActivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessLevel(
      CollectionActivity collectionActivity,
      GameFunctionType typeId,
      int levelId,
      List<int> heroes,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity FindCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CollectionComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionComponentCommon m_owner;

      public LuaExportHelper(CollectionComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionCollection m_collectionDS
      {
        get
        {
          return this.m_owner.m_collectionDS;
        }
        set
        {
          this.m_owner.m_collectionDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public OperationalActivityCompomentCommon m_operationalActivity
      {
        get
        {
          return this.m_owner.m_operationalActivity;
        }
        set
        {
          this.m_owner.m_operationalActivity = value;
        }
      }

      public int CanAttackScenarioLevel(
        CollectionActivity collectionActivity,
        int levelId,
        int deltaDays)
      {
        return this.m_owner.CanAttackScenarioLevel(collectionActivity, levelId, deltaDays);
      }

      public int CanAttackChallengeLevel(
        CollectionActivity collectionActivity,
        int levelId,
        int deltaDays)
      {
        return this.m_owner.CanAttackChallengeLevel(collectionActivity, levelId, deltaDays);
      }

      public int CanAttackLootLevel(
        CollectionActivity collectionActivity,
        int levelId,
        int deltaDays)
      {
        return this.m_owner.CanAttackLootLevel(collectionActivity, levelId, deltaDays);
      }

      public int _IsLootLevelUnlock(
        CollectionActivity collectionActivity,
        ConfigDataCollectionActivityLootLevelInfo levelInfo,
        int deltaDays)
      {
        return this.m_owner._IsLootLevelUnlock(collectionActivity, levelInfo, deltaDays);
      }

      public bool IsPrevLevelUnlock(CollectionActivity activity, List<LevelInfo> PrevLevels)
      {
        return this.m_owner.IsPrevLevelUnlock(activity, PrevLevels);
      }

      public bool IsScenarioFinished(CollectionActivity collectionActivity, int scenarioId)
      {
        return this.m_owner.IsScenarioFinished(collectionActivity, scenarioId);
      }
    }
  }
}
