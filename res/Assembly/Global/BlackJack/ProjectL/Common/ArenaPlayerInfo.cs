﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaPlayerInfo
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerDefensiveTeam DefensiveTeam { get; set; }

    public List<ArenaOpponent> Opponents { get; set; }

    public int ArenaLevelId { get; set; }

    public ushort ArenaPoints { get; set; }

    public bool AttackedOpponent { get; set; }

    public DateTime WeekLastFlushTime { get; set; }

    public int VictoryPoints { get; set; }

    public List<int> ReceivedVictoryPointsRewardIndexs { get; set; }

    public List<int> ThisWeekBattleIds { get; set; }

    public int ThisWeekTotalBattleNums { get; set; }

    public int ThisWeekVictoryNums { get; set; }

    public bool IsAutoBattle { get; set; }

    public ArenaOpponent RevengeOpponent { get; set; }

    public ulong RevengeBattleReportInstanceId { get; set; }

    public ArenaOpponentDefensiveBattleInfo OpponentDefensiveBattleInfo { get; set; }

    public int MineRank { get; set; }

    public int ConsecutiveVictoryNums { get; set; }

    public long NextFlushOpponentTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerInfo PBArenaPlayerInfoToArenaPlayerInfo(
      ProArenaPlayerInfo pbArenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerInfo ArenaPlayerInfoToPBArenaPlayerInfo(
      ArenaPlayerInfo arenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
