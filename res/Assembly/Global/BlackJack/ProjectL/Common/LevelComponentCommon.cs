﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.LevelComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class LevelComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    protected DataSectionLevel m_levelDS;
    [DoNotToLua]
    private LevelComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IsWayPointArrivedInt32_hotfix;
    private LuaFunction m_OutPutRandomEventOperateLogInt32RandomEventStatusList`1_hotfix;
    private LuaFunction m_InitLevelInfoList`1_hotfix;
    private LuaFunction m_SetArrivedWayPointInt32_hotfix;
    private LuaFunction m_ArriveWayPointInt32_hotfix;
    private LuaFunction m_HasFirstWayPointWithScenario_hotfix;
    private LuaFunction m_AddCanMovePublicWayPointInt32_hotfix;
    private LuaFunction m_SetFinishedScenarioConfigDataScenarioInfo_hotfix;
    private LuaFunction m_GetLastFinishedScenarioId_hotfix;
    private LuaFunction m_IsScenarioCompletedInt32_hotfix;
    private LuaFunction m_GetScenarioId_hotfix;
    private LuaFunction m_SetBattleWayPointSuccessfulConfigDataWaypointInfoList`1_hotfix;
    private LuaFunction m_IsExistRandomEvent_hotfix;
    private LuaFunction m_IsSetRandomEventInt32_hotfix;
    private LuaFunction m_MoveToWayPointInt32_hotfix;
    private LuaFunction m_HandleWayPointConfigDataWaypointInfo_hotfix;
    private LuaFunction m_InitRandomEventsList`1_hotfix;
    private LuaFunction m_PreInitRandomEventRandomEvent_hotfix;
    private LuaFunction m_GetEventExpiredTimeInt32_hotfix;
    private LuaFunction m_GetEventLivesInt32_hotfix;
    private LuaFunction m_IsRandomEventTimeOutRandomEvent_hotfix;
    private LuaFunction m_IsRandomEventDeadRandomEvent_hotfix;
    private LuaFunction m_RemoveRandomEventRandomEvent_hotfix;
    private LuaFunction m_AddRandomEventRandomEvent_hotfix;
    private LuaFunction m_CanUnLockScenarioConfigDataScenarioInfo_hotfix;
    private LuaFunction m_HandleScenarioInt32Boolean_hotfix;
    private LuaFunction m_IsScenarioFinishedConfigDataScenarioInfo_hotfix;
    private LuaFunction m_IsScenarioFinishedInt32_hotfix;
    private LuaFunction m_IsScenarioInWaypointInt32_hotfix;
    private LuaFunction m_GetRandomEventInt32_hotfix;
    private LuaFunction m_GetEventIdConfigDataWaypointInfo_hotfix;
    private LuaFunction m_CheckGameFunctionOpenByGMGameFunctionType_hotfix;
    private LuaFunction m_HandleEventWayPointConfigDataWaypointInfo_hotfix;
    private LuaFunction m_OnEventCompleteInt32RandomEvent_hotfix;
    private LuaFunction m_HandleDialogEventConfigDataWaypointInfoList`1Int32Int32Int32Int32_hotfix;
    private LuaFunction m_HandleTresureEventInt32ConfigDataEventInfo_hotfix;
    private LuaFunction m_GetEventRewardsConfigDataEventInfo_hotfix;
    private LuaFunction m_AttackScenarioConfigDataScenarioInfoBooleanBoolean_hotfix;
    private LuaFunction m_CanAttackScenarioConfigDataScenarioInfoBooleanBoolean_hotfix;
    private LuaFunction m_HandleAttackWayPointEventInt32ConfigDataEventInfo_hotfix;
    private LuaFunction m_AttackEventWayPointInt32ConfigDataEventInfo_hotfix;
    private LuaFunction m_CanAttackEventWayPointInt32ConfigDataEventInfo_hotfix;
    private LuaFunction m_CanMoveToWayPointInt32_hotfix;
    private LuaFunction m_CanMoveToWayPointExistRandomEventInt32_hotfix;
    private LuaFunction m_OnCompleteWayPointEvent_hotfix;
    private LuaFunction m_IsRegionOpenConfigDataRegionInfo_hotfix;
    private LuaFunction m_OnOpenRegionConfigDataRegionInfo_hotfix;
    private LuaFunction m_GetWayPointStatusInt32_hotfix;
    private LuaFunction m_add_CompleteEventMissionEventAction`1_hotfix;
    private LuaFunction m_remove_CompleteEventMissionEventAction`1_hotfix;
    private LuaFunction m_add_CompleteScenarioMissionEventAction`1_hotfix;
    private LuaFunction m_remove_CompleteScenarioMissionEventAction`1_hotfix;
    private LuaFunction m_add_CompleteNewScenarioMissionEventAction`1_hotfix;
    private LuaFunction m_remove_CompleteNewScenarioMissionEventAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWayPointArrived(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutRandomEventOperateLog(
      int eventId,
      RandomEventStatus status,
      List<Goods> rewards = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLevelInfo(List<int> arrivedWayPoints)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetArrivedWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ArriveWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool HasFirstWayPointWithScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCanMovePublicWayPoint(int newId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetFinishedScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastFinishedScenarioId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioCompleted(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScenarioId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetBattleWayPointSuccessful(
      ConfigDataWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsExistRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitRandomEvents(List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreInitRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected long GetEventExpiredTime(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEventLives(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRandomEventTimeOut(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRandomEventDead(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void RemoveRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnLockScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleScenario(int scenarioId, bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsScenarioFinished(ConfigDataScenarioInfo secenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioInWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent GetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEventId(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int CheckGameFunctionOpenByGM(GameFunctionType gameFunctionTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HandleEventWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventComplete(int wayPointId, RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleDialogEvent(
      ConfigDataWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int expReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AttackScenario(
      ConfigDataScenarioInfo secenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackScenario(
      ConfigDataScenarioInfo scenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HandleAttackWayPointEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AttackEventWayPoint(int wayPointId, ConfigDataEventInfo eventyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWayPoint(int wayPointdId, ConfigDataEventInfo eventyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanMoveToWayPoint(int destWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanMoveToWayPointExistRandomEvent(int destWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCompleteWayPointEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRegionOpen(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnOpenRegion(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWayPointStatus(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> CompleteEventMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteScenarioMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteNewScenarioMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public LevelComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteEventMissionEvent(bool obj)
    {
    }

    private void __clearDele_CompleteEventMissionEvent(bool obj)
    {
      this.CompleteEventMissionEvent = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteScenarioMissionEvent(int obj)
    {
    }

    private void __clearDele_CompleteScenarioMissionEvent(int obj)
    {
      this.CompleteScenarioMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteNewScenarioMissionEvent(int obj)
    {
    }

    private void __clearDele_CompleteNewScenarioMissionEvent(int obj)
    {
      this.CompleteNewScenarioMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private LevelComponentCommon m_owner;

      public LuaExportHelper(LevelComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteEventMissionEvent(bool obj)
      {
        this.m_owner.__callDele_CompleteEventMissionEvent(obj);
      }

      public void __clearDele_CompleteEventMissionEvent(bool obj)
      {
        this.m_owner.__clearDele_CompleteEventMissionEvent(obj);
      }

      public void __callDele_CompleteScenarioMissionEvent(int obj)
      {
        this.m_owner.__callDele_CompleteScenarioMissionEvent(obj);
      }

      public void __clearDele_CompleteScenarioMissionEvent(int obj)
      {
        this.m_owner.__clearDele_CompleteScenarioMissionEvent(obj);
      }

      public void __callDele_CompleteNewScenarioMissionEvent(int obj)
      {
        this.m_owner.__callDele_CompleteNewScenarioMissionEvent(obj);
      }

      public void __clearDele_CompleteNewScenarioMissionEvent(int obj)
      {
        this.m_owner.__clearDele_CompleteNewScenarioMissionEvent(obj);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public DataSectionLevel m_levelDS
      {
        get
        {
          return this.m_owner.m_levelDS;
        }
        set
        {
          this.m_owner.m_levelDS = value;
        }
      }

      public void OutPutRandomEventOperateLog(
        int eventId,
        RandomEventStatus status,
        List<Goods> rewards)
      {
        this.m_owner.OutPutRandomEventOperateLog(eventId, status, rewards);
      }

      public void InitLevelInfo(List<int> arrivedWayPoints)
      {
        this.m_owner.InitLevelInfo(arrivedWayPoints);
      }

      public void SetArrivedWayPoint(int wayPointId)
      {
        this.m_owner.SetArrivedWayPoint(wayPointId);
      }

      public bool HasFirstWayPointWithScenario()
      {
        return this.m_owner.HasFirstWayPointWithScenario();
      }

      public bool IsExistRandomEvent()
      {
        return this.m_owner.IsExistRandomEvent();
      }

      public void InitRandomEvents(List<RandomEvent> randomEvents)
      {
        this.m_owner.InitRandomEvents(randomEvents);
      }

      public void PreInitRandomEvent(RandomEvent randomEvent)
      {
        this.m_owner.PreInitRandomEvent(randomEvent);
      }

      public long GetEventExpiredTime(int eventId)
      {
        return this.m_owner.GetEventExpiredTime(eventId);
      }

      public int GetEventLives(int eventId)
      {
        return this.m_owner.GetEventLives(eventId);
      }

      public bool IsScenarioFinished(ConfigDataScenarioInfo secenarioInfo)
      {
        return this.m_owner.IsScenarioFinished(secenarioInfo);
      }

      public int CheckGameFunctionOpenByGM(GameFunctionType gameFunctionTypeId)
      {
        return this.m_owner.CheckGameFunctionOpenByGM(gameFunctionTypeId);
      }

      public int HandleEventWayPoint(ConfigDataWaypointInfo wayPointInfo)
      {
        return this.m_owner.HandleEventWayPoint(wayPointInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int HandleDialogEvent(
        ConfigDataWaypointInfo wayPointInfo,
        List<Goods> itemRewards,
        int expReward,
        int goldReward,
        int energyCost,
        int scenarioId)
      {
        // ISSUE: unable to decompile the method.
      }

      public int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.HandleTresureEvent(wayPointId, eventInfo);
      }

      public List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.GetEventRewards(eventInfo);
      }

      public int AttackScenario(
        ConfigDataScenarioInfo secenarioInfo,
        bool scenarioFinished,
        bool checkBag)
      {
        return this.m_owner.AttackScenario(secenarioInfo, scenarioFinished, checkBag);
      }

      public int HandleAttackWayPointEvent(int wayPointId, ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.HandleAttackWayPointEvent(wayPointId, eventInfo);
      }

      public int AttackEventWayPoint(int wayPointId, ConfigDataEventInfo eventyInfo)
      {
        return this.m_owner.AttackEventWayPoint(wayPointId, eventyInfo);
      }

      public int CanMoveToWayPoint(int destWayPointId)
      {
        return this.m_owner.CanMoveToWayPoint(destWayPointId);
      }

      public int CanMoveToWayPointExistRandomEvent(int destWayPointId)
      {
        return this.m_owner.CanMoveToWayPointExistRandomEvent(destWayPointId);
      }

      public void OnOpenRegion(ConfigDataRegionInfo regionInfo)
      {
        this.m_owner.OnOpenRegion(regionInfo);
      }
    }
  }
}
