﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.OperationalActivityCompoment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class OperationalActivityCompoment : OperationalActivityCompomentCommon
  {
    [DoNotToLua]
    private OperationalActivityCompoment.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_ExchangeItemGroupUInt64Int32_hotfix;
    private LuaFunction m_GainRewardUInt64Int32_hotfix;
    private LuaFunction m_DeserializeDSOperationalActivityNtf_hotfix;
    private LuaFunction m_DeserializeDSAnnouncementNtf_hotfix;
    private LuaFunction m_DeserializeAdvertisementFlowLayoutUpdateNtf_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_ResetAllEffectFlag_hotfix;
    private LuaFunction m_DeserializeWebControlInfoUpdateNtf_hotfix;
    private LuaFunction m_GetWebInfo_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_GetAnnouncementDSVersion_hotfix;
    private LuaFunction m_AddNewOperationalActivityProOperationalActivityBasicInfo_hotfix;
    private LuaFunction m_AddRedeemActivityInt32RedeemInfoAck_hotfix;
    private LuaFunction m_AddFansRewardFromPBTCBTActivityInt32Int64Int64Boolean_hotfix;
    private LuaFunction m_AddAnnouncementProAnnouncement_hotfix;
    private LuaFunction m_UpdateOperationalActivityProOperationalActivityBasicInfo_hotfix;
    private LuaFunction m_GetValidAnnouncements_hotfix;
    private LuaFunction m_RemoveGlobalAnnouncementUInt64_hotfix;
    private LuaFunction m_RemoveAllExpiredAnnouncements_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityCompoment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ExchangeItemGroup(ulong operationalActivityInstanceId, int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainReward(ulong operationalActivityInstanceId, int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSAnnouncementNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(AdvertisementFlowLayoutUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetAllEffectFlag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(WebControlInfoUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WebInfoControl GetWebInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetAnnouncementDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddNewOperationalActivity(
      ProOperationalActivityBasicInfo operationalActivityBasicInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddRedeemActivity(int activityId, RedeemInfoAck info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddFansRewardFromPBTCBTActivity(
      int activityID,
      long startDate,
      long endDate,
      bool isClaimed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnnouncement(ProAnnouncement pbAnnouncement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOperationalActivity(
      ProOperationalActivityBasicInfo opertioanlActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> GetValidAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveGlobalAnnouncement(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllExpiredAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public OperationalActivityCompoment.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      base.PostDeSerialize();
    }

    private void __callBase_OnFlushLoginDaysEvent()
    {
      this.OnFlushLoginDaysEvent();
    }

    private void __callBase_OnFlushSuccessCallBack(OperationalActivityBase activity)
    {
      this.OnFlushSuccessCallBack(activity);
    }

    private void __callBase_OnPlayerLevelUpEvent(int palyerLevel)
    {
      this.OnPlayerLevelUpEvent(palyerLevel);
    }

    private void __callBase_AddNewOperationalActivity(OperationalActivityBase operationalActivity)
    {
      this.AddNewOperationalActivity(operationalActivity);
    }

    private void __callBase_InitNewNewOperationalActivityInfo(
      OperationalActivityBase operationalActivityInfo)
    {
      this.InitNewNewOperationalActivityInfo(operationalActivityInfo);
    }

    private void __callBase_OnAddNewActivityCallBack(OperationalActivityBase activity)
    {
      this.OnAddNewActivityCallBack(activity);
    }

    private void __callBase_OnActivityInitCallBack(OperationalActivityBase activity)
    {
      this.OnActivityInitCallBack(activity);
    }

    private void __callBase_RemoveAllExpiredOperationalActivities()
    {
      this.RemoveAllExpiredOperationalActivities();
    }

    private int __callBase_CanExchangeItemGroup(
      LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationalActivity,
      int itemGroupIndex)
    {
      return this.CanExchangeItemGroup(limitedTimeExchangeOperationalActivity, itemGroupIndex);
    }

    private int __callBase_CanGainOperactionalActivityReward(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      return this.CanGainOperactionalActivityReward(operationalActivityBase, rewardIndex);
    }

    private List<OperationalActivityBase> __callBase_GetAllOperationalActivities()
    {
      return this.GetAllOperationalActivities();
    }

    private List<OperationalActivityBase> __callBase_GetAllValidOperationalActivities()
    {
      return this.GetAllValidOperationalActivities();
    }

    private int __callBase_GetAwardOperationActivityRewardItemGroupIdByIndex(
      OperationalActivityBase operationalActivity,
      int rewardIndex)
    {
      return this.GetAwardOperationActivityRewardItemGroupIdByIndex(operationalActivity, rewardIndex);
    }

    private void __callBase_EffectOperationActivityGenerateEffect(
      OperationalActivityBase operationalActivity,
      bool isPositive)
    {
      this.EffectOperationActivityGenerateEffect(operationalActivity, isPositive);
    }

    private OperationalActivityBase __callBase_FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      return this.FindOperationalActivityById(operationalActivityInstanceId);
    }

    private List<OperationalActivityBase> __callBase_FindOperationalActivitiesByType(
      OperationalActivityType activityType)
    {
      return this.FindOperationalActivitiesByType(activityType);
    }

    private OperationalActivityBase __callBase_FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      return this.FindOperationalActivityByActivityCardPoolId(activityCardPoolId);
    }

    private OperationalActivityBase __callBase_FindOperationalActivityByRafflePoolId(
      int rafflePoolId)
    {
      return this.FindOperationalActivityByRafflePoolId(rafflePoolId);
    }

    private OperationalActivityBase __callBase_FindOperationalActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      return this.FindOperationalActivityByUnchartedScoreId(UnchartedScoreId);
    }

    private OperationalActivityBase __callBase_FindShowActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      return this.FindShowActivityByUnchartedScoreId(UnchartedScoreId);
    }

    private OperationalActivityBase __callBase_FindOperationalActivityByCollectionActivityId(
      int collectionActivityId)
    {
      return this.FindOperationalActivityByCollectionActivityId(collectionActivityId);
    }

    private OperationalActivityBase __callBase_FindExsitOperationalActivityByCollectionActivityId(
      int collectionActivityId)
    {
      return this.FindExsitOperationalActivityByCollectionActivityId(collectionActivityId);
    }

    private OperationalActivityBase __callBase_FindShowActivityByCollectionActivityId(
      int collectionActivityId)
    {
      return this.FindShowActivityByCollectionActivityId(collectionActivityId);
    }

    private OperationalActivityBase __callBase_FindGainRewardActivityByCollectionActivityId(
      int collectionActivityId)
    {
      return this.FindGainRewardActivityByCollectionActivityId(collectionActivityId);
    }

    private List<AdvertisementFlowLayout> __callBase_GetAllAdvertisementFlowLayouts()
    {
      return this.GetAllAdvertisementFlowLayouts();
    }

    private void __callBase_OnBuyStoreItemCallBack(int storeId, int goodsId)
    {
      this.OnBuyStoreItemCallBack(storeId, goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private OperationalActivityCompoment m_owner;

      public LuaExportHelper(OperationalActivityCompoment owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public void __callBase_OnFlushLoginDaysEvent()
      {
        this.m_owner.__callBase_OnFlushLoginDaysEvent();
      }

      public void __callBase_OnFlushSuccessCallBack(OperationalActivityBase activity)
      {
        this.m_owner.__callBase_OnFlushSuccessCallBack(activity);
      }

      public void __callBase_OnPlayerLevelUpEvent(int palyerLevel)
      {
        this.m_owner.__callBase_OnPlayerLevelUpEvent(palyerLevel);
      }

      public void __callBase_AddNewOperationalActivity(OperationalActivityBase operationalActivity)
      {
        this.m_owner.__callBase_AddNewOperationalActivity(operationalActivity);
      }

      public void __callBase_InitNewNewOperationalActivityInfo(
        OperationalActivityBase operationalActivityInfo)
      {
        this.m_owner.__callBase_InitNewNewOperationalActivityInfo(operationalActivityInfo);
      }

      public void __callBase_OnAddNewActivityCallBack(OperationalActivityBase activity)
      {
        this.m_owner.__callBase_OnAddNewActivityCallBack(activity);
      }

      public void __callBase_OnActivityInitCallBack(OperationalActivityBase activity)
      {
        this.m_owner.__callBase_OnActivityInitCallBack(activity);
      }

      public void __callBase_RemoveAllExpiredOperationalActivities()
      {
        this.m_owner.__callBase_RemoveAllExpiredOperationalActivities();
      }

      public int __callBase_CanExchangeItemGroup(
        LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationalActivity,
        int itemGroupIndex)
      {
        return this.m_owner.__callBase_CanExchangeItemGroup(limitedTimeExchangeOperationalActivity, itemGroupIndex);
      }

      public int __callBase_CanGainOperactionalActivityReward(
        OperationalActivityBase operationalActivityBase,
        int rewardIndex)
      {
        return this.m_owner.__callBase_CanGainOperactionalActivityReward(operationalActivityBase, rewardIndex);
      }

      public List<OperationalActivityBase> __callBase_GetAllOperationalActivities()
      {
        return this.m_owner.__callBase_GetAllOperationalActivities();
      }

      public List<OperationalActivityBase> __callBase_GetAllValidOperationalActivities()
      {
        return this.m_owner.__callBase_GetAllValidOperationalActivities();
      }

      public int __callBase_GetAwardOperationActivityRewardItemGroupIdByIndex(
        OperationalActivityBase operationalActivity,
        int rewardIndex)
      {
        return this.m_owner.__callBase_GetAwardOperationActivityRewardItemGroupIdByIndex(operationalActivity, rewardIndex);
      }

      public void __callBase_EffectOperationActivityGenerateEffect(
        OperationalActivityBase operationalActivity,
        bool isPositive)
      {
        this.m_owner.__callBase_EffectOperationActivityGenerateEffect(operationalActivity, isPositive);
      }

      public OperationalActivityBase __callBase_FindOperationalActivityById(
        ulong operationalActivityInstanceId)
      {
        return this.m_owner.__callBase_FindOperationalActivityById(operationalActivityInstanceId);
      }

      public List<OperationalActivityBase> __callBase_FindOperationalActivitiesByType(
        OperationalActivityType activityType)
      {
        return this.m_owner.__callBase_FindOperationalActivitiesByType(activityType);
      }

      public OperationalActivityBase __callBase_FindOperationalActivityByActivityCardPoolId(
        int activityCardPoolId)
      {
        return this.m_owner.__callBase_FindOperationalActivityByActivityCardPoolId(activityCardPoolId);
      }

      public OperationalActivityBase __callBase_FindOperationalActivityByRafflePoolId(
        int rafflePoolId)
      {
        return this.m_owner.__callBase_FindOperationalActivityByRafflePoolId(rafflePoolId);
      }

      public OperationalActivityBase __callBase_FindOperationalActivityByUnchartedScoreId(
        int UnchartedScoreId)
      {
        return this.m_owner.__callBase_FindOperationalActivityByUnchartedScoreId(UnchartedScoreId);
      }

      public OperationalActivityBase __callBase_FindShowActivityByUnchartedScoreId(
        int UnchartedScoreId)
      {
        return this.m_owner.__callBase_FindShowActivityByUnchartedScoreId(UnchartedScoreId);
      }

      public OperationalActivityBase __callBase_FindOperationalActivityByCollectionActivityId(
        int collectionActivityId)
      {
        return this.m_owner.__callBase_FindOperationalActivityByCollectionActivityId(collectionActivityId);
      }

      public OperationalActivityBase __callBase_FindExsitOperationalActivityByCollectionActivityId(
        int collectionActivityId)
      {
        return this.m_owner.__callBase_FindExsitOperationalActivityByCollectionActivityId(collectionActivityId);
      }

      public OperationalActivityBase __callBase_FindShowActivityByCollectionActivityId(
        int collectionActivityId)
      {
        return this.m_owner.__callBase_FindShowActivityByCollectionActivityId(collectionActivityId);
      }

      public OperationalActivityBase __callBase_FindGainRewardActivityByCollectionActivityId(
        int collectionActivityId)
      {
        return this.m_owner.__callBase_FindGainRewardActivityByCollectionActivityId(collectionActivityId);
      }

      public List<AdvertisementFlowLayout> __callBase_GetAllAdvertisementFlowLayouts()
      {
        return this.m_owner.__callBase_GetAllAdvertisementFlowLayouts();
      }

      public void __callBase_OnBuyStoreItemCallBack(int storeId, int goodsId)
      {
        this.m_owner.__callBase_OnBuyStoreItemCallBack(storeId, goodsId);
      }

      public void ResetAllEffectFlag()
      {
        this.m_owner.ResetAllEffectFlag();
      }
    }
  }
}
