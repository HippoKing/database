﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CurrentBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class CurrentBattle
  {
    public BattleType BattleType;
    public ConfigDataBattleInfo BattleInfo;
    public ConfigDataPVPBattleInfo PVPBattleInfo;
    public ConfigDataRealTimePVPBattleInfo RealTimePVPBattleInfo;
    public RealTimePVPBattleReport RealTimePVPBattleReport;
    public ConfigDataArenaBattleInfo ArenaBattleInfo;
    public ConfigDataArenaDefendRuleInfo ArenaDefendRuleInfo;
    public int ArenaDefenderPlayerLevel;
    public List<BattleHero> ArenaDefenderHeros;
    public List<TrainingTech> ArenaDefenderTrainTechs;
    public ArenaBattleReport ArenaBattleReport;
    public bool IsArenaRevenge;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
