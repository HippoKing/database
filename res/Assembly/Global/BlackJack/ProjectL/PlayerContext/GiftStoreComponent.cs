﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GiftStoreComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class GiftStoreComponent : GiftStoreComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    public ushort GetDSVersion()
    {
      return this.m_giftStoreDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGiftStoreItemSellOut(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSGiftStoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(GiftStoreOperationalGoodsUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyGoods(int goodsId, string registerId, int boughtNums, long nextFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem GetBoughtItem(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
