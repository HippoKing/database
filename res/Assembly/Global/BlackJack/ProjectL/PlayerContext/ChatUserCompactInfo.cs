﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatUserCompactInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatUserCompactInfo
  {
    public string UserId;
    public string UserName;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUserCompactInfo(ProChatUserCompactInfo userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeadIcon { get; set; }

    public bool IsOnline { get; set; }
  }
}
