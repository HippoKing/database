﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.EmojiParseDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [Serializable]
  public class EmojiParseDesc : MonoBehaviour
  {
    [Header("表情的大小(正方形，x,y大小一致):")]
    public Vector2 m_emojiSize;
    [Header("Emoji所占位置大小(和表情自身大小无关):")]
    public int m_emSpace;
    [Header("表情坐标在X轴的偏移:")]
    public float m_offsetX;
    [Header("表情坐标在Y轴的偏移:")]
    public float m_offsetY;
  }
}
