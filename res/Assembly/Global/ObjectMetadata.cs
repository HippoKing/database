﻿// Decompiled with JetBrains decompiler
// Type: ObjectMetadata
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal struct ObjectMetadata
{
  private System.Type element_type;
  private bool is_dictionary;
  private IDictionary<string, PropertyMetadata> properties;

  public System.Type ElementType
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    set
    {
      this.element_type = value;
    }
  }

  public bool IsDictionary
  {
    get
    {
      return this.is_dictionary;
    }
    set
    {
      this.is_dictionary = value;
    }
  }

  public IDictionary<string, PropertyMetadata> Properties
  {
    get
    {
      return this.properties;
    }
    set
    {
      this.properties = value;
    }
  }
}
