﻿// Decompiled with JetBrains decompiler
// Type: EventTriggerListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerListener : EventTrigger
{
  public EventTriggerListener.VoidDelegate onClick;
  public EventTriggerListener.VoidDelegate onDown;
  public EventTriggerListener.VoidDelegate onUp;
  public EventTriggerListener.VoidDelegate onEnter;
  public EventTriggerListener.VoidDelegate onExit;
  public EventTriggerListener.VoidDelegate onSelect;
  public EventTriggerListener.VoidDelegate onDeselect;
  public EventTriggerListener.VoidDelegate onUpdateSelect;
  public EventTriggerListener.VoidDelegate onBeginDrag;
  public EventTriggerListener.VoidDelegate onEndDrag;

  [MethodImpl((MethodImplOptions) 32768)]
  public static EventTriggerListener Get(GameObject go)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static EventTriggerListener Get(Transform transform)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerClick(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerUp(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerDown(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerEnter(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerExit(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnSelect(BaseEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnDeselect(BaseEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnBeginDrag(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnEndDrag(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnUpdateSelected(BaseEventData eventData)
  {
  }

  public delegate void VoidDelegate(GameObject go);
}
