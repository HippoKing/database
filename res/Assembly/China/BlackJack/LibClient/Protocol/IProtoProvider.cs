﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.Protocol.IProtoProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;

namespace BlackJack.LibClient.Protocol
{
  public interface IProtoProvider
  {
    Type GetTypeById(int vId);

    int GetIdByType(Type vType);
  }
}
