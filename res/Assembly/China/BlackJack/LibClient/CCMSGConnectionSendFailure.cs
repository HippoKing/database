﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.CCMSGConnectionSendFailure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.LibClient
{
  [ProtoContract(Name = "CCMSGConnectionSendFailure")]
  [Serializable]
  public class CCMSGConnectionSendFailure
  {
    private int _MessageId;

    [MethodImpl((MethodImplOptions) 32768)]
    public CCMSGConnectionSendFailure()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "MessageId")]
    public int MessageId
    {
      get
      {
        return this._MessageId;
      }
      set
      {
        this._MessageId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ExceptionInfo")]
    public string ExceptionInfo { get; set; }
  }
}
