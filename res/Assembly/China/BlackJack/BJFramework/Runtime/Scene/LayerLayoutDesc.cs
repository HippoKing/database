﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.LayerLayoutDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Scene
{
  [CustomLuaClass]
  public class LayerLayoutDesc : MonoBehaviour
  {
    public bool Opaque;
    public bool FullScreen;
    public bool StayOnTop;
  }
}
