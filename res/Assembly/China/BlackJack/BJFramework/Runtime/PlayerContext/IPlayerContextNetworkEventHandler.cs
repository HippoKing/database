﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.IPlayerContextNetworkEventHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  public interface IPlayerContextNetworkEventHandler
  {
    void OnGameServerConnected();

    void OnGameServerDisconnected();

    void OnGameServerError(int err, string excepionInfo = null);

    void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken);

    void OnLoginBySessionTokenAck(int result);

    void OnGameServerMessage(object msg, int msgId);
  }
}
