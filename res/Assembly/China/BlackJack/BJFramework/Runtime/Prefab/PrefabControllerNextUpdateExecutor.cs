﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.PrefabControllerNextUpdateExecutor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  public class PrefabControllerNextUpdateExecutor
  {
    private List<Action> m_nextUpdateExecutionList = new List<Action>();

    [MethodImpl((MethodImplOptions) 32768)]
    public PrefabControllerNextUpdateExecutor()
    {
    }

    public void AddNextUpdateExecution(Action action)
    {
      this.m_nextUpdateExecutionList.Add(action);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
