﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TypeDNName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  [Serializable]
  public class TypeDNName
  {
    public string m_assemblyName;
    public string m_typeFullName;

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDNName(string typeDNName)
    {
      int length = typeDNName.IndexOf('@');
      if (length == -1)
      {
        this.m_assemblyName = "Assembly-CSharp";
        this.m_typeFullName = typeDNName;
      }
      else
      {
        this.m_assemblyName = typeDNName.Substring(0, length);
        this.m_typeFullName = typeDNName.Substring(length + 1);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
