﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateColorDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateColorDesc
  {
    public Color TargetColor;
    public GameObject ChangeColorGo;
    [HideInInspector]
    public List<Image> ChangeColorImageList;
    [HideInInspector]
    public List<Text> ChangeColorTextList;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateColorDesc()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
