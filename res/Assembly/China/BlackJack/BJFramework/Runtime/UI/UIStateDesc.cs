﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateDesc
  {
    public int StateColorSetIndex = -1;
    public List<GameObject> SetToShowGameObjectList = new List<GameObject>();
    public List<TweenMain> TweenAnimationList = new List<TweenMain>();
    public List<UIStateGradientColorDesc> GradientDescList = new List<UIStateGradientColorDesc>();
    public string StateName;
    [HideInInspector]
    private bool m_isTweenFinished;
    [HideInInspector]
    private bool m_isAnimationFinished;
    [HideInInspector]
    public Action m_eventFinished;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateDesc()
    {
    }

    [HideInInspector]
    public bool AnimationFinished
    {
      get
      {
        return this.m_isAnimationFinished;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        if (this.m_isAnimationFinished == value)
          return;
        this.m_isAnimationFinished = value;
        if (this.m_eventFinished == null || !this.m_isAnimationFinished || !this.m_isTweenFinished)
          return;
        this.m_eventFinished();
      }
    }

    [HideInInspector]
    public bool TweenFinished
    {
      get
      {
        return this.m_isTweenFinished;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        if (this.m_isTweenFinished == value)
          return;
        this.m_isTweenFinished = value;
        if (this.m_eventFinished == null || !this.m_isAnimationFinished || !this.m_isTweenFinished)
          return;
        this.m_eventFinished();
      }
    }
  }
}
