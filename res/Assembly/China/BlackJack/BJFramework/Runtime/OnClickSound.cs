﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.OnClickSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Scene;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  public class OnClickSound : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
  {
    public string m_clickSoundName = string.Empty;
    private bool m_isClicked;
    private bool m_shouldLatePlay;

    [MethodImpl((MethodImplOptions) 32768)]
    public OnClickSound()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      if (string.IsNullOrEmpty(this.m_clickSoundName))
      {
        Debug.LogError(string.Format("The OnClickSound at [{1}] has no click sound name.", (object) this.m_clickSoundName, (object) SceneManager.GetObjectPath(this.gameObject)));
      }
      else
      {
        Button component1 = this.gameObject.GetComponent<Button>();
        if ((Object) component1 != (Object) null)
        {
          component1.onClick.AddListener(new UnityAction(this.OnButtonClick));
        }
        else
        {
          Toggle component2 = this.gameObject.GetComponent<Toggle>();
          if ((Object) component2 != (Object) null)
            component2.onValueChanged.AddListener(new UnityAction<bool>(this.OnToggleValueChanged));
          else
            Debug.LogError(string.Format("The OnClickSound with [{0}] at [{1}] has no Button and no Toggle component.", (object) this.m_clickSoundName, (object) SceneManager.GetObjectPath(this.gameObject)));
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnToggleValueChanged(bool val)
    {
      if (!val)
        return;
      this.m_shouldLatePlay = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      if (!this.m_shouldLatePlay)
        return;
      this.m_shouldLatePlay = false;
      if (!this.m_isClicked)
        return;
      this.m_isClicked = false;
      if (GameManager.Instance == null || GameManager.Instance.AudioManager == null)
        return;
      GameManager.Instance.AudioManager.PlaySound(this.m_clickSoundName, 1f);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      this.m_isClicked = true;
    }
  }
}
