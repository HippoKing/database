﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleTreasureInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleTreasureInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataBattleTreasureInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private List<ParamPosition> _ModelPosition;
    private List<ParamPosition> _TriggerPositions;
    private List<Goods> _Reward;
    private string _Model;
    private int _ModelScale;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleTreasureInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "ModelPosition")]
    public List<ParamPosition> ModelPosition
    {
      get
      {
        return this._ModelPosition;
      }
      set
      {
        this._ModelPosition = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "TriggerPositions")]
    public List<ParamPosition> TriggerPositions
    {
      get
      {
        return this._TriggerPositions;
      }
      set
      {
        this._TriggerPositions = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModelScale")]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
