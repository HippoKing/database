﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityRewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityRewardType")]
  public enum CollectionActivityRewardType
  {
    [ProtoEnum(Name = "CollectionActivityRewardType_None", Value = 0)] CollectionActivityRewardType_None,
    [ProtoEnum(Name = "CollectionActivityRewardType_Exchange", Value = 1)] CollectionActivityRewardType_Exchange,
    [ProtoEnum(Name = "CollectionActivityRewardType_Score", Value = 2)] CollectionActivityRewardType_Score,
  }
}
