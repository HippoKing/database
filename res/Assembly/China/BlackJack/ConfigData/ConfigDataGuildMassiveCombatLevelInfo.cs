﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGuildMassiveCombatLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataGuildMassiveCombatLevelInfo")]
  [Serializable]
  public class ConfigDataGuildMassiveCombatLevelInfo : IExtensible
  {
    private int _ID;
    private int _BattleId;
    private string _MiniMapResources;
    private string _Strategy;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataGuildMassiveCombatStrongholdInfo m_strongholdInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "MiniMapResources")]
    public string MiniMapResources
    {
      get
      {
        return this._MiniMapResources;
      }
      set
      {
        this._MiniMapResources = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strategy")]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
