﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataConfessionDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataConfessionDialogInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataConfessionDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextWordID;
    private int _FrameType;
    private string _Animation;
    private string _Word;
    private string _Voice;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfessionDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextWordID")]
    public int NextWordID
    {
      get
      {
        return this._NextWordID;
      }
      set
      {
        this._NextWordID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FrameType")]
    public int FrameType
    {
      get
      {
        return this._FrameType;
      }
      set
      {
        this._FrameType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Animation")]
    public string Animation
    {
      get
      {
        return this._Animation;
      }
      set
      {
        this._Animation = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Word")]
    public string Word
    {
      get
      {
        return this._Word;
      }
      set
      {
        this._Word = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "Voice")]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
