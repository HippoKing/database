﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionPeriodType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionPeriodType")]
  public enum MissionPeriodType
  {
    [ProtoEnum(Name = "MissionPeriodType_Everyday", Value = 1)] MissionPeriodType_Everyday = 1,
    [ProtoEnum(Name = "MissionPeriodType_OneOff", Value = 2)] MissionPeriodType_OneOff = 2,
  }
}
