﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventFuncType")]
  public enum EventFuncType
  {
    [ProtoEnum(Name = "EventFuncType_None", Value = 0)] EventFuncType_None,
    [ProtoEnum(Name = "EventFuncType_Monster", Value = 1)] EventFuncType_Monster,
    [ProtoEnum(Name = "EventFuncType_Mission", Value = 2)] EventFuncType_Mission,
    [ProtoEnum(Name = "EventFuncType_Dialog", Value = 3)] EventFuncType_Dialog,
    [ProtoEnum(Name = "EventFuncType_Treasure", Value = 4)] EventFuncType_Treasure,
    [ProtoEnum(Name = "EventFuncType_Shop", Value = 5)] EventFuncType_Shop,
  }
}
