﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MoveType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MoveType")]
  public enum MoveType
  {
    [ProtoEnum(Name = "MoveType_None", Value = 0)] MoveType_None,
    [ProtoEnum(Name = "MoveType_Ride", Value = 1)] MoveType_Ride,
    [ProtoEnum(Name = "MoveType_Walk", Value = 2)] MoveType_Walk,
    [ProtoEnum(Name = "MoveType_Water", Value = 3)] MoveType_Water,
    [ProtoEnum(Name = "MoveType_Fly", Value = 4)] MoveType_Fly,
    [ProtoEnum(Name = "MoveType_FieldArmy", Value = 5)] MoveType_FieldArmy,
  }
}
