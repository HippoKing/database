﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScenarioUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScenarioUnlockConditionType")]
  public enum ScenarioUnlockConditionType
  {
    [ProtoEnum(Name = "ScenarioUnlockConditionType_None", Value = 0)] ScenarioUnlockConditionType_None,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_PlayerLevel", Value = 1)] ScenarioUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_RiftLevel", Value = 2)] ScenarioUnlockConditionType_RiftLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_Count", Value = 3)] ScenarioUnlockConditionType_Count,
  }
}
