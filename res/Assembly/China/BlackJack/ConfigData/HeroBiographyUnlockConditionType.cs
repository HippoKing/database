﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroBiographyUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroBiographyUnlockConditionType")]
  public enum HeroBiographyUnlockConditionType
  {
    [ProtoEnum(Name = "HeroBiographyUnlockConditionType_None", Value = 0)] HeroBiographyUnlockConditionType_None,
    [ProtoEnum(Name = "HeroBiographyUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroBiographyUnlockConditionType_HeroFavorabilityLevel,
  }
}
