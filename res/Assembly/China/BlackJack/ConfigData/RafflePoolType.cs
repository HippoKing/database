﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RafflePoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RafflePoolType")]
  public enum RafflePoolType
  {
    [ProtoEnum(Name = "RafflePoolType_None", Value = 0)] RafflePoolType_None,
    [ProtoEnum(Name = "RafflePoolType_Raffle", Value = 1)] RafflePoolType_Raffle,
    [ProtoEnum(Name = "RafflePoolType_Tarot", Value = 2)] RafflePoolType_Tarot,
  }
}
