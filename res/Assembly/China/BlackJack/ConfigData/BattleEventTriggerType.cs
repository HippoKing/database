﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleEventTriggerType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleEventTriggerType")]
  public enum BattleEventTriggerType
  {
    [ProtoEnum(Name = "BattleEventTriggerType_None", Value = 0)] BattleEventTriggerType_None,
    [ProtoEnum(Name = "BattleEventTriggerType_TurnCount", Value = 1)] BattleEventTriggerType_TurnCount,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorDie", Value = 2)] BattleEventTriggerType_ActorDie,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorNeighbor", Value = 3)] BattleEventTriggerType_ActorNeighbor,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorReachPosition", Value = 4)] BattleEventTriggerType_ActorReachPosition,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorOverPosition", Value = 5)] BattleEventTriggerType_ActorOverPosition,
    [ProtoEnum(Name = "BattleEventTriggerType_Achievement", Value = 6)] BattleEventTriggerType_Achievement,
    [ProtoEnum(Name = "BattleEventTriggerType_Win", Value = 7)] BattleEventTriggerType_Win,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorHpLess", Value = 8)] BattleEventTriggerType_ActorHpLess,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorFirstAction", Value = 9)] BattleEventTriggerType_ActorFirstAction,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorFirstAttack", Value = 10)] BattleEventTriggerType_ActorFirstAttack,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorFirstAttackAfter", Value = 11)] BattleEventTriggerType_ActorFirstAttackAfter,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorFirstSkill", Value = 12)] BattleEventTriggerType_ActorFirstSkill,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorFirstBeAttack", Value = 13)] BattleEventTriggerType_ActorFirstBeAttack,
    [ProtoEnum(Name = "BattleEventTriggerType_KillCount", Value = 14)] BattleEventTriggerType_KillCount,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorCountLess", Value = 15)] BattleEventTriggerType_ActorCountLess,
    [ProtoEnum(Name = "BattleEventTriggerType_GroupDie", Value = 16)] BattleEventTriggerType_GroupDie,
    [ProtoEnum(Name = "BattleEventTriggerType_MultiTrigger", Value = 17)] BattleEventTriggerType_MultiTrigger,
    [ProtoEnum(Name = "BattleEventTriggerType_NotTrigger", Value = 18)] BattleEventTriggerType_NotTrigger,
    [ProtoEnum(Name = "BattleEventTriggerType_TriggerTurnCount", Value = 19)] BattleEventTriggerType_TriggerTurnCount,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorAttackTarget", Value = 20)] BattleEventTriggerType_ActorAttackTarget,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorAttackTargetAfter", Value = 21)] BattleEventTriggerType_ActorAttackTargetAfter,
    [ProtoEnum(Name = "BattleEventTriggerType_TurnCountMulti", Value = 22)] BattleEventTriggerType_TurnCountMulti,
    [ProtoEnum(Name = "BattleEventTriggerType_TurnEndNotDamage", Value = 23)] BattleEventTriggerType_TurnEndNotDamage,
    [ProtoEnum(Name = "BattleEventTriggerType_ActorKillActor", Value = 24)] BattleEventTriggerType_ActorKillActor,
    [ProtoEnum(Name = "BattleEventTriggerType_VariableGreater", Value = 25)] BattleEventTriggerType_VariableGreater,
    [ProtoEnum(Name = "BattleEventTriggerType_VariableLess", Value = 26)] BattleEventTriggerType_VariableLess,
    [ProtoEnum(Name = "BattleEventTriggerType_VariableEqual", Value = 27)] BattleEventTriggerType_VariableEqual,
  }
}
