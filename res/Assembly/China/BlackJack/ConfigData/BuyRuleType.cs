﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuyRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuyRuleType")]
  public enum BuyRuleType
  {
    [ProtoEnum(Name = "BuyRuleType_FixedTime", Value = 1)] BuyRuleType_FixedTime = 1,
    [ProtoEnum(Name = "BuyRuleType_WeekTime", Value = 2)] BuyRuleType_WeekTime = 2,
    [ProtoEnum(Name = "BuyRuleType_MonthTime", Value = 3)] BuyRuleType_MonthTime = 3,
    [ProtoEnum(Name = "BuyRuleType_Forever", Value = 4)] BuyRuleType_Forever = 4,
  }
}
