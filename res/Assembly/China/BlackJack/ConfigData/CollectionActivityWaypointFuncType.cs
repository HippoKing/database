﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointFuncType")]
  public enum CollectionActivityWaypointFuncType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_None", Value = 0)] CollectionActivityWaypointFuncType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Default", Value = 1)] CollectionActivityWaypointFuncType_Default,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Empty", Value = 2)] CollectionActivityWaypointFuncType_Empty,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Event", Value = 3)] CollectionActivityWaypointFuncType_Event,
  }
}
