﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SelectSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SelectSkill")]
  public enum SelectSkill
  {
    [ProtoEnum(Name = "SelectSkill_DefaultSelection", Value = 1)] SelectSkill_DefaultSelection = 1,
    [ProtoEnum(Name = "SelectSkill_DirectReachTargetSkill", Value = 2)] SelectSkill_DirectReachTargetSkill = 2,
    [ProtoEnum(Name = "SelectSkill_ExcludeSkillID", Value = 3)] SelectSkill_ExcludeSkillID = 3,
    [ProtoEnum(Name = "SelectSkill_IncludeSkillID", Value = 4)] SelectSkill_IncludeSkillID = 4,
  }
}
