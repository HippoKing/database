﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaDanInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaDanInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataPeakArenaDanInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _DisplayDan;
    private int _DanGroup;
    private int _BaselineScore;
    private int _NextSeasonDan;
    private List<PeakArenaWinScoreDiff> _WinScoreDiffBonus;
    private List<PeakArenaLossScoreDiff> _LossScoreDiffBonus;
    private List<PeakArenaConsecutiveWins> _ConsecutiveWinScoreBonus;
    private int _WinBasicScore;
    private int _LossBasicScore;
    private int _DanDiffUpperBound;
    private int _DanDiffLowerBound;
    private List<WaitingTimeInfo> _WaitingTimeAdjustment;
    private List<PeakArenaConsecutiveWinsMatchmakingScoreInfo> _ConsecutiveWinsMatchmakingScoreAdjustment;
    private List<PeakArenaConsecutiveLossesMatchmakingInfo> _ConsecutiveLossesMatchmakingAdjustment;
    private List<PeakArenaMatchmakingFailInfo> _MatchmakingFailAdjustment;
    private string _Icon;
    private List<FriendlyWaitingTimeInfo> _FriendlyWaitingTimeAdjustment;
    private int _DanRewardMailTemplateId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayDan")]
    public int DisplayDan
    {
      get
      {
        return this._DisplayDan;
      }
      set
      {
        this._DisplayDan = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanGroup")]
    public int DanGroup
    {
      get
      {
        return this._DanGroup;
      }
      set
      {
        this._DanGroup = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BaselineScore")]
    public int BaselineScore
    {
      get
      {
        return this._BaselineScore;
      }
      set
      {
        this._BaselineScore = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextSeasonDan")]
    public int NextSeasonDan
    {
      get
      {
        return this._NextSeasonDan;
      }
      set
      {
        this._NextSeasonDan = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "WinScoreDiffBonus")]
    public List<PeakArenaWinScoreDiff> WinScoreDiffBonus
    {
      get
      {
        return this._WinScoreDiffBonus;
      }
      set
      {
        this._WinScoreDiffBonus = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "LossScoreDiffBonus")]
    public List<PeakArenaLossScoreDiff> LossScoreDiffBonus
    {
      get
      {
        return this._LossScoreDiffBonus;
      }
      set
      {
        this._LossScoreDiffBonus = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "ConsecutiveWinScoreBonus")]
    public List<PeakArenaConsecutiveWins> ConsecutiveWinScoreBonus
    {
      get
      {
        return this._ConsecutiveWinScoreBonus;
      }
      set
      {
        this._ConsecutiveWinScoreBonus = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinBasicScore")]
    public int WinBasicScore
    {
      get
      {
        return this._WinBasicScore;
      }
      set
      {
        this._WinBasicScore = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LossBasicScore")]
    public int LossBasicScore
    {
      get
      {
        return this._LossBasicScore;
      }
      set
      {
        this._LossBasicScore = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiffUpperBound")]
    public int DanDiffUpperBound
    {
      get
      {
        return this._DanDiffUpperBound;
      }
      set
      {
        this._DanDiffUpperBound = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiffLowerBound")]
    public int DanDiffLowerBound
    {
      get
      {
        return this._DanDiffLowerBound;
      }
      set
      {
        this._DanDiffLowerBound = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "WaitingTimeAdjustment")]
    public List<WaitingTimeInfo> WaitingTimeAdjustment
    {
      get
      {
        return this._WaitingTimeAdjustment;
      }
      set
      {
        this._WaitingTimeAdjustment = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, Name = "ConsecutiveWinsMatchmakingScoreAdjustment")]
    public List<PeakArenaConsecutiveWinsMatchmakingScoreInfo> ConsecutiveWinsMatchmakingScoreAdjustment
    {
      get
      {
        return this._ConsecutiveWinsMatchmakingScoreAdjustment;
      }
      set
      {
        this._ConsecutiveWinsMatchmakingScoreAdjustment = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "ConsecutiveLossesMatchmakingAdjustment")]
    public List<PeakArenaConsecutiveLossesMatchmakingInfo> ConsecutiveLossesMatchmakingAdjustment
    {
      get
      {
        return this._ConsecutiveLossesMatchmakingAdjustment;
      }
      set
      {
        this._ConsecutiveLossesMatchmakingAdjustment = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, Name = "MatchmakingFailAdjustment")]
    public List<PeakArenaMatchmakingFailInfo> MatchmakingFailAdjustment
    {
      get
      {
        return this._MatchmakingFailAdjustment;
      }
      set
      {
        this._MatchmakingFailAdjustment = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, Name = "FriendlyWaitingTimeAdjustment")]
    public List<FriendlyWaitingTimeInfo> FriendlyWaitingTimeAdjustment
    {
      get
      {
        return this._FriendlyWaitingTimeAdjustment;
      }
      set
      {
        this._FriendlyWaitingTimeAdjustment = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanRewardMailTemplateId")]
    public int DanRewardMailTemplateId
    {
      get
      {
        return this._DanRewardMailTemplateId;
      }
      set
      {
        this._DanRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
