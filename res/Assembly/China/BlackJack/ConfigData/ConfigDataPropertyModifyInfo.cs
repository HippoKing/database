﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPropertyModifyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPropertyModifyInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataPropertyModifyInfo : IExtensible
  {
    private int _ID;
    private PropertyModifyType _PropertyModifyType;
    private string _Name;
    private bool _IsAddType;
    private bool _IsDynamic;
    private PropertyType _PropertyType;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPropertyModifyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PropertyModifyType")]
    public PropertyModifyType PropertyModifyType
    {
      get
      {
        return this._PropertyModifyType;
      }
      set
      {
        this._PropertyModifyType = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAddType")]
    public bool IsAddType
    {
      get
      {
        return this._IsAddType;
      }
      set
      {
        this._IsAddType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsDynamic")]
    public bool IsDynamic
    {
      get
      {
        return this._IsDynamic;
      }
      set
      {
        this._IsDynamic = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PropertyType")]
    public PropertyType PropertyType
    {
      get
      {
        return this._PropertyType;
      }
      set
      {
        this._PropertyType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
