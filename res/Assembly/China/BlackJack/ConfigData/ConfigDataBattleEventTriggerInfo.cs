﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleEventTriggerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataBattleEventTriggerInfo")]
  [Serializable]
  public class ConfigDataBattleEventTriggerInfo : IExtensible
  {
    private int _ID;
    private BattleEventTriggerType _TriggerType;
    private List<int> _Param1;
    private List<int> _Param2;
    private List<ParamPosition> _Param3;
    private List<int> _Actions_ID;
    private int _TriggerCountMax;
    private IExtension extensionObject;
    public ConfigDataBattleEventActionInfo[] m_battleEventActionInfos;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventTriggerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TriggerType")]
    public BattleEventTriggerType TriggerType
    {
      get
      {
        return this._TriggerType;
      }
      set
      {
        this._TriggerType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "Param1")]
    public List<int> Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "Param2")]
    public List<int> Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Param3")]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Actions_ID")]
    public List<int> Actions_ID
    {
      get
      {
        return this._Actions_ID;
      }
      set
      {
        this._Actions_ID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TriggerCountMax")]
    public int TriggerCountMax
    {
      get
      {
        return this._TriggerCountMax;
      }
      set
      {
        this._TriggerCountMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
