﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.MultiLanguageManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class MultiLanguageManager
  {
    public static string CurrentLanguage = string.Empty;
    public static string DefaultLanguage = string.Empty;
    private static Dictionary<string, string> m_LanguageDict;
    private const string MULTI_LANGUAGE_KEY = "USER_CURRENT_LANGUAGE";

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool InitMultiLanguageSetting()
    {
      List<string> stringList = new List<string>();
      Dictionary<string, string> dictionary = (Dictionary<string, string>) null;
      if (MultiLanguageManager.LoadSetting())
        dictionary = MultiLanguageManager.GetLanguageDict();
      if (dictionary != null && dictionary.Count > 0)
        stringList = new List<string>((IEnumerable<string>) dictionary.Keys);
      if (stringList.Count <= 0)
        return false;
      MultiLanguageManager.CurrentLanguage = string.Empty;
      if (!PlayerPrefs.HasKey("USER_CURRENT_LANGUAGE"))
      {
        MultiLanguageManager.CurrentLanguage = stringList[0];
        PlayerPrefs.SetString("USER_CURRENT_LANGUAGE", MultiLanguageManager.CurrentLanguage);
        PlayerPrefs.Save();
      }
      else
        MultiLanguageManager.CurrentLanguage = PlayerPrefs.GetString("USER_CURRENT_LANGUAGE");
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetMultiLanguageConfigDataPath(ConfigDataSettings configSetting)
    {
      if (MultiLanguageManager.IsDefaultLanguage())
        return;
      configSetting.RuntimeConfigDataAssetTargetPath = configSetting.ConfigDataAssetTargetPath.Replace("ConfigData_ABS", "ConfigData_" + MultiLanguageManager.CurrentLanguage + "_ABS");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ChangeLanguage(string nextLanguage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool LoadSetting()
    {
      MultiLanguageManager.m_LanguageDict = new Dictionary<string, string>();
      TextAsset textAsset = Resources.Load<TextAsset>("MultiLanguage");
      if ((UnityEngine.Object) textAsset == (UnityEngine.Object) null)
        return false;
      string[] strArray1 = textAsset.text.Split(new char[1]
      {
        '\n'
      }, StringSplitOptions.RemoveEmptyEntries);
      string empty = string.Empty;
      foreach (string str in strArray1)
      {
        string[] separator = new string[1]{ "=" };
        string[] strArray2 = str.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        if (strArray2.Length == 2 && !string.IsNullOrEmpty(strArray2[0]) && !string.IsNullOrEmpty(strArray2[1]))
        {
          if (MultiLanguageManager.m_LanguageDict.TryGetValue(strArray2[0], out empty))
          {
            Debug.LogError(string.Format("MultiLanguage.Load() key {0} repeated.", (object) strArray2[0]));
            return false;
          }
          MultiLanguageManager.m_LanguageDict.Add(strArray2[0], strArray2[1].Replace("\\n", "\n"));
          if (MultiLanguageManager.DefaultLanguage == string.Empty)
            MultiLanguageManager.DefaultLanguage = strArray2[0];
          Debug.Log("m_LanguageDict.Add key=" + strArray2[0] + " value=" + strArray2[1]);
        }
      }
      return true;
    }

    public static Dictionary<string, string> GetLanguageDict()
    {
      return MultiLanguageManager.m_LanguageDict;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsDefaultLanguage()
    {
      if (!(MultiLanguageManager.CurrentLanguage == string.Empty))
        return MultiLanguageManager.CurrentLanguage == MultiLanguageManager.DefaultLanguage;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static MultiLanguageManager()
    {
    }
  }
}
