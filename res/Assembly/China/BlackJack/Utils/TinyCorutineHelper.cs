﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.Utils.TinyCorutineHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.Utils
{
  [CustomLuaClass]
  public class TinyCorutineHelper
  {
    private LinkedList<IEnumerator> m_corutineList = new LinkedList<IEnumerator>();
    private bool m_cancel;

    [MethodImpl((MethodImplOptions) 32768)]
    public TinyCorutineHelper()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCorutine(Func<IEnumerator> corutine)
    {
      this.m_corutineList.AddLast(corutine());
    }

    public void StartCorutine(IEnumerator corutine)
    {
      this.m_corutineList.AddLast(corutine);
    }

    public void StartLuaCoroutine(LuaFunction func)
    {
      this.StartCorutine(this.LuaCoroutine(func));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    internal IEnumerator LuaCoroutine(LuaFunction func)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CancelAll()
    {
      this.m_cancel = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      if (this.m_corutineList.Count == 0)
        return;
      LinkedListNode<IEnumerator> next;
      for (LinkedListNode<IEnumerator> node = this.m_corutineList.First; node != null && !this.m_cancel; node = next)
      {
        bool flag = TinyCorutineHelper.MoveNext(node.Value);
        next = node.Next;
        if (!flag)
          this.m_corutineList.Remove(node);
      }
      if (!this.m_cancel)
        return;
      this.m_corutineList.Clear();
      this.m_cancel = false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MoveNext(IEnumerator iter)
    {
      if (iter == null)
        return false;
      if (iter.Current != null && TinyCorutineHelper.MoveNext(iter.Current as IEnumerator))
        return true;
      return iter.MoveNext();
    }
  }
}
