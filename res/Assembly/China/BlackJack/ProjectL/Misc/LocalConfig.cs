﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalConfig
  {
    private static LocalConfig s_instance;
    private static string m_fileName;
    private LocalConfigData m_data;
    private const int m_maxRecentLoginServerCount = 2;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFileName(string name)
    {
      LocalConfig.m_fileName = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Save()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyVolume(string category, bool isInitialize = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Apply(bool isInitialize = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRecentLoginServerID(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetAutoBattleId(int battleId)
    {
      this.m_data.AutoBattleId = battleId;
    }

    public int GetAutoBattleId()
    {
      return this.m_data.AutoBattleId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestUIId(int testType, int testId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestUIId(int testType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleDanmakuState(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListSortType(int type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsOnlyShowCurJobSkin(bool onlyShowCurJobSkin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsSetSkinToAllSoldier(bool setSkinToAllSoldier)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGMUser(bool isGMUser)
    {
      // ISSUE: unable to decompile the method.
    }

    public LocalConfigData Data
    {
      get
      {
        return this.m_data;
      }
    }

    public static LocalConfig Instance
    {
      set
      {
        LocalConfig.s_instance = value;
      }
      get
      {
        return LocalConfig.s_instance;
      }
    }
  }
}
