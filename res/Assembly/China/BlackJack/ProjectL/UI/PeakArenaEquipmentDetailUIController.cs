﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaEquipmentDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaEquipmentDetailUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipItemDescStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipBGButton;
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescTitleText;
    [AutoBind("./Detail/Lay/FrameImage/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescLvText;
    [AutoBind("./Detail/Lay/FrameImage/Top/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescExpText;
    [AutoBind("./Detail/Lay/FrameImage/Top/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconImage;
    [AutoBind("./Detail/Lay/FrameImage/Top/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconBg;
    [AutoBind("./Detail/Lay/FrameImage/Top/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSSREffect;
    [AutoBind("./Detail/Lay/FrameImage/Top/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescProgressImage;
    [AutoBind("./Detail/Lay/FrameImage/Top/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescStarGroup;
    [AutoBind("./Detail/Lay/FrameImage/Top/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescEquipLimitContent;
    [AutoBind("./Detail/Lay/FrameImage/Top/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropContent;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropATGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropATValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDFGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDFValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropHPGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropHPValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagiccGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagicDFGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicDFValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDexGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDexValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillContent;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillNameText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillLvText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillOwnerGameObject;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillOwnerText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillDescText;
    [AutoBind("./Detail/Lay/FrameImage/Button/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescNotEquipSkillTip;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private PeakArenaEquipmentDetailUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_PlayShowAnimation_hotfix;
    private LuaFunction m_SetEquipmentItemDescEquipmentBagItemHeroList`1_hotfix;
    private LuaFunction m_SetPropItemsPropertyModifyTypeInt32Int32Int32_hotfix;
    private LuaFunction m_SetPropEnchantmentEquipmentBagItem_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayShowAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEquipmentItemDesc(
      EquipmentBagItem equipment,
      Hero hero,
      List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropEnchantment(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaEquipmentDetailUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaEquipmentDetailUIController m_owner;

      public LuaExportHelper(PeakArenaEquipmentDetailUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_equipItemDescStateController
      {
        get
        {
          return this.m_owner.m_equipItemDescStateController;
        }
        set
        {
          this.m_owner.m_equipItemDescStateController = value;
        }
      }

      public Button m_equipBGButton
      {
        get
        {
          return this.m_owner.m_equipBGButton;
        }
        set
        {
          this.m_owner.m_equipBGButton = value;
        }
      }

      public Text m_equipItemDescTitleText
      {
        get
        {
          return this.m_owner.m_equipItemDescTitleText;
        }
        set
        {
          this.m_owner.m_equipItemDescTitleText = value;
        }
      }

      public Text m_equipItemDescLvText
      {
        get
        {
          return this.m_owner.m_equipItemDescLvText;
        }
        set
        {
          this.m_owner.m_equipItemDescLvText = value;
        }
      }

      public Text m_equipItemDescExpText
      {
        get
        {
          return this.m_owner.m_equipItemDescExpText;
        }
        set
        {
          this.m_owner.m_equipItemDescExpText = value;
        }
      }

      public Image m_equipItemDescIconImage
      {
        get
        {
          return this.m_owner.m_equipItemDescIconImage;
        }
        set
        {
          this.m_owner.m_equipItemDescIconImage = value;
        }
      }

      public Image m_equipItemDescIconBg
      {
        get
        {
          return this.m_owner.m_equipItemDescIconBg;
        }
        set
        {
          this.m_owner.m_equipItemDescIconBg = value;
        }
      }

      public GameObject m_equipItemDescSSREffect
      {
        get
        {
          return this.m_owner.m_equipItemDescSSREffect;
        }
        set
        {
          this.m_owner.m_equipItemDescSSREffect = value;
        }
      }

      public Image m_equipItemDescProgressImage
      {
        get
        {
          return this.m_owner.m_equipItemDescProgressImage;
        }
        set
        {
          this.m_owner.m_equipItemDescProgressImage = value;
        }
      }

      public GameObject m_equipItemDescStarGroup
      {
        get
        {
          return this.m_owner.m_equipItemDescStarGroup;
        }
        set
        {
          this.m_owner.m_equipItemDescStarGroup = value;
        }
      }

      public GameObject m_equipItemDescEquipLimitContent
      {
        get
        {
          return this.m_owner.m_equipItemDescEquipLimitContent;
        }
        set
        {
          this.m_owner.m_equipItemDescEquipLimitContent = value;
        }
      }

      public Text m_descEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_descEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_descEquipUnlimitText = value;
        }
      }

      public GameObject m_equipItemDescPropContent
      {
        get
        {
          return this.m_owner.m_equipItemDescPropContent;
        }
        set
        {
          this.m_owner.m_equipItemDescPropContent = value;
        }
      }

      public GameObject m_equipItemDescPropATGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropATGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropATGo = value;
        }
      }

      public Text m_equipItemDescPropATValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropATValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropATValueText = value;
        }
      }

      public GameObject m_equipItemDescPropDFGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDFGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDFGo = value;
        }
      }

      public Text m_equipItemDescPropDFValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDFValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDFValueText = value;
        }
      }

      public GameObject m_equipItemDescPropHPGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropHPGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropHPGo = value;
        }
      }

      public Text m_equipItemDescPropHPValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropHPValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropHPValueText = value;
        }
      }

      public GameObject m_equipItemDescPropMagiccGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagiccGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagiccGo = value;
        }
      }

      public Text m_equipItemDescPropMagicValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagicValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagicValueText = value;
        }
      }

      public GameObject m_equipItemDescPropMagicDFGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagicDFGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagicDFGo = value;
        }
      }

      public Text m_equipItemDescPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagicDFValueText = value;
        }
      }

      public GameObject m_equipItemDescPropDexGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDexGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDexGo = value;
        }
      }

      public Text m_equipItemDescPropDexValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDexValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDexValueText = value;
        }
      }

      public CommonUIStateController m_descPropGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropGroupStateCtrl = value;
        }
      }

      public GameObject m_descPropEnchantmentGroup
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroup;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroup = value;
        }
      }

      public CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl = value;
        }
      }

      public Image m_descPropEnchantmentGroupRuneIconImage
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneIconImage;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneIconImage = value;
        }
      }

      public Text m_descPropEnchantmentGroupRuneNameText
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneNameText;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneNameText = value;
        }
      }

      public GameObject m_equipItemDescSkillContent
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillContent;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillContent = value;
        }
      }

      public Text m_equipItemDescSkillNameText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillNameText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillNameText = value;
        }
      }

      public Text m_equipItemDescSkillLvText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillLvText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillLvText = value;
        }
      }

      public GameObject m_equipItemDescSkillOwnerGameObject
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillOwnerGameObject;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillOwnerGameObject = value;
        }
      }

      public Text m_equipItemDescSkillOwnerText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillOwnerText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillOwnerText = value;
        }
      }

      public Text m_equipItemDescSkillDescText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillDescText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillDescText = value;
        }
      }

      public GameObject m_equipItemDescNotEquipSkillTip
      {
        get
        {
          return this.m_owner.m_equipItemDescNotEquipSkillTip;
        }
        set
        {
          this.m_owner.m_equipItemDescNotEquipSkillTip = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetPropEnchantment(EquipmentBagItem equipment)
      {
        this.m_owner.SetPropEnchantment(equipment);
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }
    }
  }
}
