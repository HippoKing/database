﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaDefendUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ArenaDefendUIController m_arenaDefendUIController;
    private ArenaDefendMapUIController m_arenaDefendMapUIController;
    private ArenaDefendSceneUIController m_arenaDefendSceneUIController;
    private BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController;
    private ArenaDefendBattle m_arenaDefendBattle;
    private List<GridPosition> m_defendPositions;
    private List<GridPosition> m_attackPositions;
    private List<ConfigDataArenaBattleInfo> m_arenaBattleInfos;
    private List<ConfigDataArenaDefendRuleInfo> m_defendRuleInfos;
    private List<BattleHero> m_playerBattleHeros;
    private List<BattleHero> m_defendStageHeros;
    private List<TrainingTech> m_trainingTechs;
    private int m_curBattleIndex;
    private int m_curDefendRuleIndex;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAndSoldierModelAssets(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitArenaDefendUIController();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapAndActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnSave()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowActionOrderPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageHeroActionValues()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowMapPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowDefendRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmMap(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmDefendRule(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowMyActorInfo(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ArenaDefendUIController_OnHideActorInfo()
    {
      this.m_battlePrepareActorInfoUIController.Close();
    }

    private void ArenaDefendUIController_OnStageActorChange()
    {
      this.UpdateBattlePower();
      this.ShowStagePositions();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnHeroOnStage(BattleHero hero, GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ArenaDefendUIController_OnActorOffStage(ArenaDefendActor sa)
    {
      this.m_arenaDefendUIController.ActorOffStage(sa);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorMove(ArenaDefendActor sa, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorSwap(
      ArenaDefendActor sa1,
      ArenaDefendActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Hero GetHeroFromBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSkillPanel(BattleHero battleHero)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSkill(
      BattleHero battleHero,
      List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSoldier(
      BattleHero battleHero,
      int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ArenaDefendSceneUIController_OnPointerDown(PointerEventData eventData)
    {
      this.m_arenaDefendUIController.OnScenePointerDown(eventData);
    }

    private void ArenaDefendSceneUIController_OnPointerUp(PointerEventData eventData)
    {
      this.m_arenaDefendUIController.OnScenePointerUp(eventData);
    }

    private void ArenaDefendSceneUIController_OnPointerClick(PointerEventData eventData)
    {
      this.m_arenaDefendUIController.OnScenePointerClick(eventData);
    }

    private void ArenaDefendSceneUIController_OnBeginDrag(PointerEventData eventData)
    {
      this.m_arenaDefendUIController.OnSceneBeginDrag(eventData);
    }

    private void ArenaDefendSceneUIController_OnEndDrag(PointerEventData eventData)
    {
      this.m_arenaDefendUIController.OnSceneEndDrag(eventData);
    }

    private void ArenaDefendSceneUIController_OnDrag(PointerEventData eventData)
    {
      this.m_arenaDefendUIController.OnSceneDrag(eventData);
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
