﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleActorInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleActorInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Hero/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Hero/Army/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroArmyText;
    [AutoBind("./HeroPanelGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroPanelGroupCtrl;
    [AutoBind("./HeroPanelGroup/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIcon;
    [AutoBind("./Hero/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroArmyIcon;
    [AutoBind("./Hero/Job/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroJobText;
    [AutoBind("./Hero/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLevelText;
    [AutoBind("./Hero/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroHPText;
    [AutoBind("./Hero/DEX/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDEXText;
    [AutoBind("./Hero/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHPImage;
    [AutoBind("./Hero/Attack/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroAttackText;
    [AutoBind("./Hero/Defense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDefenseText;
    [AutoBind("./Hero/Magic/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicText;
    [AutoBind("./Hero/MagicDefense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicDefenseText;
    [AutoBind("./Hero/Range/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroRangeText;
    [AutoBind("./Hero/Move/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMoveText;
    [AutoBind("./Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGameObject;
    [AutoBind("./Soldier/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./Soldier/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierArmyIcon;
    [AutoBind("./Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicGameObject;
    [AutoBind("./Soldier/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierHPImage;
    [AutoBind("./Soldier/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPText;
    [AutoBind("./Soldier/Prop/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAttackText;
    [AutoBind("./Soldier/Prop/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDefenseText;
    [AutoBind("./Soldier/Prop/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFText;
    [AutoBind("./Soldier/Prop/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAttackAddText;
    [AutoBind("./Soldier/Prop/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDefenseAddText;
    [AutoBind("./Soldier/Prop/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddText;
    [AutoBind("./Hero/SkillInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillInfoButtonGo;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/TalentSkill", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillInfoGroupTalentSkillImage;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo2;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo3;
    [AutoBind("./SkillDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailPanel;
    [AutoBind("./SkillDetailPanel/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailListGameObject;
    [AutoBind("./SkillDetailPanel/List/Content/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillTalentGameObject;
    [AutoBind("./SkillDetailPanel/List/Content/Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillTalentIcon;
    [AutoBind("./SkillDetailPanel/List/Content/Talent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillTalentNameText;
    [AutoBind("./SkillDetailPanel/List/Content/Talent/DescTextScrollRect/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillTalentDescText;
    [AutoBind("./SkillDetailPanel/List/Content/LineImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillLineImage;
    [AutoBind("./SkillDetailPanel/List/Content/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListGameObject;
    [AutoBind("./SkillDetailPanel/List/NoSkills", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noSkillsAndTalentGameObject;
    [AutoBind("./SkillDetailPanel/List/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentGameObject;
    [AutoBind("./DebugBuffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_debugBuffToggle;
    [AutoBind("./BuffList", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buffsListGameObject;
    [AutoBind("./BuffsDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffDescDetailGameObject;
    [AutoBind("./BuffsDetailPanel/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffsDescDetailListGameObject;
    [AutoBind("./BuffsDetailPanel/List/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffsDescContentGameObject;
    [AutoBind("./BuffsDetailPanel/List/NoBuff", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noBuffsGameObject;
    [AutoBind("./Soldier/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierDetailButton;
    [AutoBind("./SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelUIStateController;
    [AutoBind("./SoldierDetailPanel/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicObj;
    [AutoBind("./SoldierDetailPanel/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./SoldierDetailPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierTitleText;
    [AutoBind("./SoldierDetailPanel/Desc/DescTextScroll/Mask/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./SoldierDetailPanel/Faction/Melee", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDescMeleeGo;
    [AutoBind("./SoldierDetailPanel/Faction/NotMelee", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDescNotMeleeGo;
    [AutoBind("./SoldierDetailPanel/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescRangeValueText;
    [AutoBind("./SoldierDetailPanel/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescMoveValueText;
    [AutoBind("./SoldierDetailPanel/Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescRestrainValueText;
    [AutoBind("./SoldierDetailPanel/Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescWeakValueText;
    [AutoBind("./SoldierDetailPanel/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPValueText;
    [AutoBind("./SoldierDetailPanel/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFValueText;
    [AutoBind("./SoldierDetailPanel/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATValueText;
    [AutoBind("./SoldierDetailPanel/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFValueText;
    [AutoBind("./SoldierDetailPanel/HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPAddText;
    [AutoBind("./SoldierDetailPanel/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFAddText;
    [AutoBind("./SoldierDetailPanel/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATAddText;
    [AutoBind("./SoldierDetailPanel/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFAddText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/BuffDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffDescPrefab;
    [AutoBind("./Prefabs/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDescPrefab;
    [AutoBind("./Prefabs/BuffIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffIconPrefab;
    private GameObjectPool<SkillDesc> m_skillDescPool;
    private GameObjectPool<BuffDesc> m_buffDescPool;
    private UISpineGraphic m_soldierGraphic;
    private UISpineGraphic m_soldierDetailGraphic;
    private bool m_isOpened;
    private BattleActor m_actor;
    private List<TrainingTech> m_trainTechs;
    private int m_myPlayerTeam;
    [DoNotToLua]
    private BattleActorInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_IsOpened_hotfix;
    private LuaFunction m_SetActorInfoBattleActorInt32List`1_hotfix;
    private LuaFunction m_SetSoldierDetailInfoBattleActorConfigDataSoldierInfoInt32_hotfix;
    private LuaFunction m_CalcPropValueInt32Int32Boolean_hotfix;
    private LuaFunction m_CreateSpineGraphicStringSingleVector2GameObjectList`1_hotfix;
    private LuaFunction m_DestroySpineGraphicUISpineGraphic_hotfix;
    private LuaFunction m_DebugBuffToggle_OnValueChangedBoolean_hotfix;

    private BattleActorInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorInfo(BattleActor a, int myPlayerTeam, List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailInfo(
      BattleActor a,
      ConfigDataSoldierInfo soldierInfo,
      int myPlayerTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropValue(int v0, int v1, bool isAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DebugBuffToggle_OnValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleActorInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleActorInfoUIController m_owner;

      public LuaExportHelper(BattleActorInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Text m_heroNameText
      {
        get
        {
          return this.m_owner.m_heroNameText;
        }
        set
        {
          this.m_owner.m_heroNameText = value;
        }
      }

      public Text m_heroArmyText
      {
        get
        {
          return this.m_owner.m_heroArmyText;
        }
        set
        {
          this.m_owner.m_heroArmyText = value;
        }
      }

      public CommonUIStateController m_heroPanelGroupCtrl
      {
        get
        {
          return this.m_owner.m_heroPanelGroupCtrl;
        }
        set
        {
          this.m_owner.m_heroPanelGroupCtrl = value;
        }
      }

      public Image m_heroIcon
      {
        get
        {
          return this.m_owner.m_heroIcon;
        }
        set
        {
          this.m_owner.m_heroIcon = value;
        }
      }

      public Image m_heroArmyIcon
      {
        get
        {
          return this.m_owner.m_heroArmyIcon;
        }
        set
        {
          this.m_owner.m_heroArmyIcon = value;
        }
      }

      public Text m_heroJobText
      {
        get
        {
          return this.m_owner.m_heroJobText;
        }
        set
        {
          this.m_owner.m_heroJobText = value;
        }
      }

      public Text m_heroLevelText
      {
        get
        {
          return this.m_owner.m_heroLevelText;
        }
        set
        {
          this.m_owner.m_heroLevelText = value;
        }
      }

      public Text m_heroHPText
      {
        get
        {
          return this.m_owner.m_heroHPText;
        }
        set
        {
          this.m_owner.m_heroHPText = value;
        }
      }

      public Text m_heroDEXText
      {
        get
        {
          return this.m_owner.m_heroDEXText;
        }
        set
        {
          this.m_owner.m_heroDEXText = value;
        }
      }

      public Image m_heroHPImage
      {
        get
        {
          return this.m_owner.m_heroHPImage;
        }
        set
        {
          this.m_owner.m_heroHPImage = value;
        }
      }

      public Text m_heroAttackText
      {
        get
        {
          return this.m_owner.m_heroAttackText;
        }
        set
        {
          this.m_owner.m_heroAttackText = value;
        }
      }

      public Text m_heroDefenseText
      {
        get
        {
          return this.m_owner.m_heroDefenseText;
        }
        set
        {
          this.m_owner.m_heroDefenseText = value;
        }
      }

      public Text m_heroMagicText
      {
        get
        {
          return this.m_owner.m_heroMagicText;
        }
        set
        {
          this.m_owner.m_heroMagicText = value;
        }
      }

      public Text m_heroMagicDefenseText
      {
        get
        {
          return this.m_owner.m_heroMagicDefenseText;
        }
        set
        {
          this.m_owner.m_heroMagicDefenseText = value;
        }
      }

      public Text m_heroRangeText
      {
        get
        {
          return this.m_owner.m_heroRangeText;
        }
        set
        {
          this.m_owner.m_heroRangeText = value;
        }
      }

      public Text m_heroMoveText
      {
        get
        {
          return this.m_owner.m_heroMoveText;
        }
        set
        {
          this.m_owner.m_heroMoveText = value;
        }
      }

      public GameObject m_soldierGameObject
      {
        get
        {
          return this.m_owner.m_soldierGameObject;
        }
        set
        {
          this.m_owner.m_soldierGameObject = value;
        }
      }

      public Text m_soldierNameText
      {
        get
        {
          return this.m_owner.m_soldierNameText;
        }
        set
        {
          this.m_owner.m_soldierNameText = value;
        }
      }

      public Image m_soldierArmyIcon
      {
        get
        {
          return this.m_owner.m_soldierArmyIcon;
        }
        set
        {
          this.m_owner.m_soldierArmyIcon = value;
        }
      }

      public GameObject m_soldierGraphicGameObject
      {
        get
        {
          return this.m_owner.m_soldierGraphicGameObject;
        }
        set
        {
          this.m_owner.m_soldierGraphicGameObject = value;
        }
      }

      public Image m_soldierHPImage
      {
        get
        {
          return this.m_owner.m_soldierHPImage;
        }
        set
        {
          this.m_owner.m_soldierHPImage = value;
        }
      }

      public Text m_soldierHPText
      {
        get
        {
          return this.m_owner.m_soldierHPText;
        }
        set
        {
          this.m_owner.m_soldierHPText = value;
        }
      }

      public Text m_soldierAttackText
      {
        get
        {
          return this.m_owner.m_soldierAttackText;
        }
        set
        {
          this.m_owner.m_soldierAttackText = value;
        }
      }

      public Text m_soldierDefenseText
      {
        get
        {
          return this.m_owner.m_soldierDefenseText;
        }
        set
        {
          this.m_owner.m_soldierDefenseText = value;
        }
      }

      public Text m_soldierMagicDFText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFText = value;
        }
      }

      public Text m_soldierAttackAddText
      {
        get
        {
          return this.m_owner.m_soldierAttackAddText;
        }
        set
        {
          this.m_owner.m_soldierAttackAddText = value;
        }
      }

      public Text m_soldierDefenseAddText
      {
        get
        {
          return this.m_owner.m_soldierDefenseAddText;
        }
        set
        {
          this.m_owner.m_soldierDefenseAddText = value;
        }
      }

      public Text m_soldierMagicDFAddText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFAddText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFAddText = value;
        }
      }

      public Button m_skillInfoButtonGo
      {
        get
        {
          return this.m_owner.m_skillInfoButtonGo;
        }
        set
        {
          this.m_owner.m_skillInfoButtonGo = value;
        }
      }

      public Image m_skillInfoGroupTalentSkillImage
      {
        get
        {
          return this.m_owner.m_skillInfoGroupTalentSkillImage;
        }
        set
        {
          this.m_owner.m_skillInfoGroupTalentSkillImage = value;
        }
      }

      public GameObject m_skillInfoGroupSkillGo
      {
        get
        {
          return this.m_owner.m_skillInfoGroupSkillGo;
        }
        set
        {
          this.m_owner.m_skillInfoGroupSkillGo = value;
        }
      }

      public GameObject m_skillInfoGroupSkillGo2
      {
        get
        {
          return this.m_owner.m_skillInfoGroupSkillGo2;
        }
        set
        {
          this.m_owner.m_skillInfoGroupSkillGo2 = value;
        }
      }

      public GameObject m_skillInfoGroupSkillGo3
      {
        get
        {
          return this.m_owner.m_skillInfoGroupSkillGo3;
        }
        set
        {
          this.m_owner.m_skillInfoGroupSkillGo3 = value;
        }
      }

      public GameObject m_skillDetailPanel
      {
        get
        {
          return this.m_owner.m_skillDetailPanel;
        }
        set
        {
          this.m_owner.m_skillDetailPanel = value;
        }
      }

      public GameObject m_skillDetailListGameObject
      {
        get
        {
          return this.m_owner.m_skillDetailListGameObject;
        }
        set
        {
          this.m_owner.m_skillDetailListGameObject = value;
        }
      }

      public GameObject m_skillTalentGameObject
      {
        get
        {
          return this.m_owner.m_skillTalentGameObject;
        }
        set
        {
          this.m_owner.m_skillTalentGameObject = value;
        }
      }

      public Image m_skillTalentIcon
      {
        get
        {
          return this.m_owner.m_skillTalentIcon;
        }
        set
        {
          this.m_owner.m_skillTalentIcon = value;
        }
      }

      public Text m_skillTalentNameText
      {
        get
        {
          return this.m_owner.m_skillTalentNameText;
        }
        set
        {
          this.m_owner.m_skillTalentNameText = value;
        }
      }

      public Text m_skillTalentDescText
      {
        get
        {
          return this.m_owner.m_skillTalentDescText;
        }
        set
        {
          this.m_owner.m_skillTalentDescText = value;
        }
      }

      public Image m_skillLineImage
      {
        get
        {
          return this.m_owner.m_skillLineImage;
        }
        set
        {
          this.m_owner.m_skillLineImage = value;
        }
      }

      public GameObject m_skillListGameObject
      {
        get
        {
          return this.m_owner.m_skillListGameObject;
        }
        set
        {
          this.m_owner.m_skillListGameObject = value;
        }
      }

      public GameObject m_noSkillsAndTalentGameObject
      {
        get
        {
          return this.m_owner.m_noSkillsAndTalentGameObject;
        }
        set
        {
          this.m_owner.m_noSkillsAndTalentGameObject = value;
        }
      }

      public GameObject m_skillListContentGameObject
      {
        get
        {
          return this.m_owner.m_skillListContentGameObject;
        }
        set
        {
          this.m_owner.m_skillListContentGameObject = value;
        }
      }

      public Toggle m_debugBuffToggle
      {
        get
        {
          return this.m_owner.m_debugBuffToggle;
        }
        set
        {
          this.m_owner.m_debugBuffToggle = value;
        }
      }

      public Button m_buffsListGameObject
      {
        get
        {
          return this.m_owner.m_buffsListGameObject;
        }
        set
        {
          this.m_owner.m_buffsListGameObject = value;
        }
      }

      public GameObject m_buffDescDetailGameObject
      {
        get
        {
          return this.m_owner.m_buffDescDetailGameObject;
        }
        set
        {
          this.m_owner.m_buffDescDetailGameObject = value;
        }
      }

      public GameObject m_buffsDescDetailListGameObject
      {
        get
        {
          return this.m_owner.m_buffsDescDetailListGameObject;
        }
        set
        {
          this.m_owner.m_buffsDescDetailListGameObject = value;
        }
      }

      public GameObject m_buffsDescContentGameObject
      {
        get
        {
          return this.m_owner.m_buffsDescContentGameObject;
        }
        set
        {
          this.m_owner.m_buffsDescContentGameObject = value;
        }
      }

      public GameObject m_noBuffsGameObject
      {
        get
        {
          return this.m_owner.m_noBuffsGameObject;
        }
        set
        {
          this.m_owner.m_noBuffsGameObject = value;
        }
      }

      public Button m_soldierDetailButton
      {
        get
        {
          return this.m_owner.m_soldierDetailButton;
        }
        set
        {
          this.m_owner.m_soldierDetailButton = value;
        }
      }

      public CommonUIStateController m_soldierDetailPanelUIStateController
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelUIStateController;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelUIStateController = value;
        }
      }

      public GameObject m_soldierGraphicObj
      {
        get
        {
          return this.m_owner.m_soldierGraphicObj;
        }
        set
        {
          this.m_owner.m_soldierGraphicObj = value;
        }
      }

      public Image m_soldierIconImg
      {
        get
        {
          return this.m_owner.m_soldierIconImg;
        }
        set
        {
          this.m_owner.m_soldierIconImg = value;
        }
      }

      public Text m_soldierTitleText
      {
        get
        {
          return this.m_owner.m_soldierTitleText;
        }
        set
        {
          this.m_owner.m_soldierTitleText = value;
        }
      }

      public Text m_soldierDescText
      {
        get
        {
          return this.m_owner.m_soldierDescText;
        }
        set
        {
          this.m_owner.m_soldierDescText = value;
        }
      }

      public GameObject m_soldierDescMeleeGo
      {
        get
        {
          return this.m_owner.m_soldierDescMeleeGo;
        }
        set
        {
          this.m_owner.m_soldierDescMeleeGo = value;
        }
      }

      public GameObject m_soldierDescNotMeleeGo
      {
        get
        {
          return this.m_owner.m_soldierDescNotMeleeGo;
        }
        set
        {
          this.m_owner.m_soldierDescNotMeleeGo = value;
        }
      }

      public Text m_soldierDescRangeValueText
      {
        get
        {
          return this.m_owner.m_soldierDescRangeValueText;
        }
        set
        {
          this.m_owner.m_soldierDescRangeValueText = value;
        }
      }

      public Text m_soldierDescMoveValueText
      {
        get
        {
          return this.m_owner.m_soldierDescMoveValueText;
        }
        set
        {
          this.m_owner.m_soldierDescMoveValueText = value;
        }
      }

      public Text m_soldierDescRestrainValueText
      {
        get
        {
          return this.m_owner.m_soldierDescRestrainValueText;
        }
        set
        {
          this.m_owner.m_soldierDescRestrainValueText = value;
        }
      }

      public Text m_soldierDescWeakValueText
      {
        get
        {
          return this.m_owner.m_soldierDescWeakValueText;
        }
        set
        {
          this.m_owner.m_soldierDescWeakValueText = value;
        }
      }

      public Text m_soldierPropHPValueText
      {
        get
        {
          return this.m_owner.m_soldierPropHPValueText;
        }
        set
        {
          this.m_owner.m_soldierPropHPValueText = value;
        }
      }

      public Text m_soldierPropDFValueText
      {
        get
        {
          return this.m_owner.m_soldierPropDFValueText;
        }
        set
        {
          this.m_owner.m_soldierPropDFValueText = value;
        }
      }

      public Text m_soldierPropATValueText
      {
        get
        {
          return this.m_owner.m_soldierPropATValueText;
        }
        set
        {
          this.m_owner.m_soldierPropATValueText = value;
        }
      }

      public Text m_soldierPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_soldierPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_soldierPropMagicDFValueText = value;
        }
      }

      public Text m_soldierPropHPAddText
      {
        get
        {
          return this.m_owner.m_soldierPropHPAddText;
        }
        set
        {
          this.m_owner.m_soldierPropHPAddText = value;
        }
      }

      public Text m_soldierPropDFAddText
      {
        get
        {
          return this.m_owner.m_soldierPropDFAddText;
        }
        set
        {
          this.m_owner.m_soldierPropDFAddText = value;
        }
      }

      public Text m_soldierPropATAddText
      {
        get
        {
          return this.m_owner.m_soldierPropATAddText;
        }
        set
        {
          this.m_owner.m_soldierPropATAddText = value;
        }
      }

      public Text m_soldierPropMagicDFAddText
      {
        get
        {
          return this.m_owner.m_soldierPropMagicDFAddText;
        }
        set
        {
          this.m_owner.m_soldierPropMagicDFAddText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_buffDescPrefab
      {
        get
        {
          return this.m_owner.m_buffDescPrefab;
        }
        set
        {
          this.m_owner.m_buffDescPrefab = value;
        }
      }

      public GameObject m_skillDescPrefab
      {
        get
        {
          return this.m_owner.m_skillDescPrefab;
        }
        set
        {
          this.m_owner.m_skillDescPrefab = value;
        }
      }

      public GameObject m_buffIconPrefab
      {
        get
        {
          return this.m_owner.m_buffIconPrefab;
        }
        set
        {
          this.m_owner.m_buffIconPrefab = value;
        }
      }

      public GameObjectPool<SkillDesc> m_skillDescPool
      {
        get
        {
          return this.m_owner.m_skillDescPool;
        }
        set
        {
          this.m_owner.m_skillDescPool = value;
        }
      }

      public GameObjectPool<BuffDesc> m_buffDescPool
      {
        get
        {
          return this.m_owner.m_buffDescPool;
        }
        set
        {
          this.m_owner.m_buffDescPool = value;
        }
      }

      public UISpineGraphic m_soldierGraphic
      {
        get
        {
          return this.m_owner.m_soldierGraphic;
        }
        set
        {
          this.m_owner.m_soldierGraphic = value;
        }
      }

      public UISpineGraphic m_soldierDetailGraphic
      {
        get
        {
          return this.m_owner.m_soldierDetailGraphic;
        }
        set
        {
          this.m_owner.m_soldierDetailGraphic = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public BattleActor m_actor
      {
        get
        {
          return this.m_owner.m_actor;
        }
        set
        {
          this.m_owner.m_actor = value;
        }
      }

      public List<TrainingTech> m_trainTechs
      {
        get
        {
          return this.m_owner.m_trainTechs;
        }
        set
        {
          this.m_owner.m_trainTechs = value;
        }
      }

      public int m_myPlayerTeam
      {
        get
        {
          return this.m_owner.m_myPlayerTeam;
        }
        set
        {
          this.m_owner.m_myPlayerTeam = value;
        }
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetSoldierDetailInfo(
        BattleActor a,
        ConfigDataSoldierInfo soldierInfo,
        int myPlayerTeam)
      {
        this.m_owner.SetSoldierDetailInfo(a, soldierInfo, myPlayerTeam);
      }

      public string CalcPropValue(int v0, int v1, bool isAdd)
      {
        return this.m_owner.CalcPropValue(v0, v1, isAdd);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UISpineGraphic CreateSpineGraphic(
        string assetName,
        float scale,
        Vector2 offset,
        GameObject parent,
        List<ReplaceAnim> replaceAnims)
      {
        // ISSUE: unable to decompile the method.
      }

      public void DestroySpineGraphic(UISpineGraphic g)
      {
        this.m_owner.DestroySpineGraphic(g);
      }

      public void DebugBuffToggle_OnValueChanged(bool isOn)
      {
        this.m_owner.DebugBuffToggle_OnValueChanged(isOn);
      }
    }
  }
}
