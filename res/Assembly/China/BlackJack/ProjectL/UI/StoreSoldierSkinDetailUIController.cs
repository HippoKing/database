﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreSoldierSkinDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class StoreSoldierSkinDetailUIController : UIControllerBase
  {
    public List<SoldierShowItemController> m_soldierDefaultSkinItemCtrlList;
    public List<SoldierShowItemController> m_soldierBuySkinItemCtrlList;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_panelBgButton;
    [AutoBind("./LayoutRoot/BuyAndGot/SureBtn", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_buyButton;
    [AutoBind("./LayoutRoot/BuyAndGot/SureBtn/SureText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_priceText;
    [AutoBind("./LayoutRoot/BuyAndGot/SureBtn/SureIcon/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_currencyImage;
    [AutoBind("./LayoutRoot/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_nameText;
    [AutoBind("./LayoutRoot/BuyAndGot", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_buyStateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_panelStateUICtrl;
    [AutoBind("./LayoutRoot/SoldierRight/SoldierGroup/Soldier1", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierDefaultSkinItem1;
    [AutoBind("./LayoutRoot/SoldierRight/SoldierGroup/Soldier2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierDefaultSkinItem2;
    [AutoBind("./LayoutRoot/SoldierLeft/SoldierGroup/Soldier1", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierBuySkinItem1;
    [AutoBind("./LayoutRoot/SoldierLeft/SoldierGroup/Soldier2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierBuySkinItem2;
    [DoNotToLua]
    private StoreSoldierSkinDetailUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateSoldierSkinDetailPanelByFixedStoreItemConfigInfoConfigDataFixedStoreItemInfoBoolean_hotfix;
    private LuaFunction m_UpdateSoldierSkinDetailPanelBySoldierSkinConfigInfoConfigDataSoldierSkinInfoBoolean_hotfix;
    private LuaFunction m_PlayPanelOpenAnim_hotfix;
    private LuaFunction m_PlayPanelCloseAnimAction_hotfix;
    private LuaFunction m_SetBuyButtonToHasBuyMode_hotfix;
    private LuaFunction m_SetPanelBuyButtonStateBoolean_hotfix;
    private LuaFunction m_SetSoldierDefaultAndSkinItemInfoStringList`1_hotfix;
    private LuaFunction m_SetSpineAnimStringUISpineGraphic_GameObjectConfigDataSoldierInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreSoldierSkinDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSoldierSkinDetailPanelByFixedStoreItemConfigInfo(
      ConfigDataFixedStoreItemInfo fixedStoreItemConfig,
      bool isNeedBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSoldierSkinDetailPanelBySoldierSkinConfigInfo(
      ConfigDataSoldierSkinInfo skinConfigInfo,
      bool isNeedBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPanelOpenAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPanelCloseAnim(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuyButtonToHasBuyMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetPanelBuyButtonState(bool isNeedBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSoldierDefaultAndSkinItemInfo(
      string skinName,
      List<Soldier2SkinResource> soldier2SkinResList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSpineAnim(
      string modelPath,
      ref UISpineGraphic graphic,
      GameObject go,
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public StoreSoldierSkinDetailUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private StoreSoldierSkinDetailUIController m_owner;

      public LuaExportHelper(StoreSoldierSkinDetailUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetPanelBuyButtonState(bool isNeedBuy)
      {
        this.m_owner.SetPanelBuyButtonState(isNeedBuy);
      }

      public void SetSoldierDefaultAndSkinItemInfo(
        string skinName,
        List<Soldier2SkinResource> soldier2SkinResList)
      {
        this.m_owner.SetSoldierDefaultAndSkinItemInfo(skinName, soldier2SkinResList);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetSpineAnim(
        string modelPath,
        ref UISpineGraphic graphic,
        GameObject go,
        ConfigDataSoldierInfo soldierInfo)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
