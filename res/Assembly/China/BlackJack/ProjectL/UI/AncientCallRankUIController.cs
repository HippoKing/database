﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRankUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AncientCallRankUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankState;
    [AutoBind("./Detail/Filter/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectRankButton;
    [AutoBind("./Detail/Filter/Button/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterNameText;
    [AutoBind("./Detail/ToggleGroup/ALLToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_allToggle;
    [AutoBind("./Detail/ToggleGroup/SocialToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_socialToggle;
    [AutoBind("./Detail/Filter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sortTypeState;
    [AutoBind("./Detail/Filter/SortTypes/ScrollView/Viewport/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_filterContent;
    [AutoBind("./Detail/TotalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_totalLoopScrollRect;
    [AutoBind("./Detail/TotalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_totalObjectPool;
    [AutoBind("./Detail/SingleBossScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_bossLoopScrollRect;
    [AutoBind("./Detail/SingleBossScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_bossObjectPool;
    [AutoBind("./Detail/BossRank", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleBossRankGameObject;
    [AutoBind("./Detail/AllRank", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allRankGameObject;
    [AutoBind("./TeamDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamDetailPanelStateCtrl;
    [AutoBind("./TeamDetailPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamDetailPanelBackgroundButton;
    [AutoBind("./TeamDetailPanel/Panel/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_teamDetailPanelHeroGroup;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teamDetailPanelPlayerIconImage;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_teamDetailPanelHeadFrameDummy;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamDetailPanelLevelText;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamDetailPanelPlayerNameText;
    [AutoBind("./TeamDetailPanel/Panel/HeroPower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamDetailPanelHeroPowerText;
    [AutoBind("./Prefab/BossName", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_togglePrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private AncientCallRankingListUITask m_ancientCallRankingListUITask;
    private AncientCallSelfRankAllUIController m_selfRankAllUIController;
    private AncientCallSelfRankSingleBossUIController m_selfRankSingleBossUIController;
    private GameObjectPool m_filterTogglePool;
    private Dictionary<int, Toggle> m_filterToggleDic;
    private int m_selectBossId;
    private RankingListInfo m_rankingListInfo;
    private Toggle m_selectSocialToggle;
    private const int AllBoss = 0;
    [DoNotToLua]
    private AncientCallRankUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnTotalBossPoolObjCreateStringGameObject_hotfix;
    private LuaFunction m_OnSingleBossPoolObjCreateStringGameObject_hotfix;
    private LuaFunction m_CloseTeamDetailPanel_hotfix;
    private LuaFunction m_OpenList`1_hotfix;
    private LuaFunction m_SetRankInt32_hotfix;
    private LuaFunction m_UpdateVeiwRank_hotfix;
    private LuaFunction m_SendAncientCallRankReq_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnTotalBossListItemFillUIControllerBase_hotfix;
    private LuaFunction m_OnSingleBossListItemFillUIControllerBase_hotfix;
    private LuaFunction m_OnBattleArraryClickRankingTargetPlayerInfoInt32_hotfix;
    private LuaFunction m_OnSelfBattleArrayClickInt32_hotfix;
    private LuaFunction m_OnSelectRankClick_hotfix;
    private LuaFunction m_OnSelectSocialRelationClickToggle_hotfix;
    private LuaFunction m_OnSelectRankToggleInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallRankUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnTotalBossPoolObjCreate(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSingleBossPoolObjCreate(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseTeamDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(List<int> bossIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRank(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateVeiwRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SendAncientCallRankReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTotalBossListItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSingleBossListItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleArraryClick(RankingTargetPlayerInfo info, int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelfBattleArrayClick(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectRankClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectSocialRelationClick(Toggle selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectRankToggle(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public AncientCallRankUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AncientCallRankUIController m_owner;

      public LuaExportHelper(AncientCallRankUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiState
      {
        get
        {
          return this.m_owner.m_uiState;
        }
        set
        {
          this.m_owner.m_uiState = value;
        }
      }

      public Button m_bgButton
      {
        get
        {
          return this.m_owner.m_bgButton;
        }
        set
        {
          this.m_owner.m_bgButton = value;
        }
      }

      public CommonUIStateController m_rankState
      {
        get
        {
          return this.m_owner.m_rankState;
        }
        set
        {
          this.m_owner.m_rankState = value;
        }
      }

      public Button m_selectRankButton
      {
        get
        {
          return this.m_owner.m_selectRankButton;
        }
        set
        {
          this.m_owner.m_selectRankButton = value;
        }
      }

      public Text m_filterNameText
      {
        get
        {
          return this.m_owner.m_filterNameText;
        }
        set
        {
          this.m_owner.m_filterNameText = value;
        }
      }

      public Toggle m_allToggle
      {
        get
        {
          return this.m_owner.m_allToggle;
        }
        set
        {
          this.m_owner.m_allToggle = value;
        }
      }

      public Toggle m_socialToggle
      {
        get
        {
          return this.m_owner.m_socialToggle;
        }
        set
        {
          this.m_owner.m_socialToggle = value;
        }
      }

      public CommonUIStateController m_sortTypeState
      {
        get
        {
          return this.m_owner.m_sortTypeState;
        }
        set
        {
          this.m_owner.m_sortTypeState = value;
        }
      }

      public Transform m_filterContent
      {
        get
        {
          return this.m_owner.m_filterContent;
        }
        set
        {
          this.m_owner.m_filterContent = value;
        }
      }

      public LoopVerticalScrollRect m_totalLoopScrollRect
      {
        get
        {
          return this.m_owner.m_totalLoopScrollRect;
        }
        set
        {
          this.m_owner.m_totalLoopScrollRect = value;
        }
      }

      public EasyObjectPool m_totalObjectPool
      {
        get
        {
          return this.m_owner.m_totalObjectPool;
        }
        set
        {
          this.m_owner.m_totalObjectPool = value;
        }
      }

      public LoopVerticalScrollRect m_bossLoopScrollRect
      {
        get
        {
          return this.m_owner.m_bossLoopScrollRect;
        }
        set
        {
          this.m_owner.m_bossLoopScrollRect = value;
        }
      }

      public EasyObjectPool m_bossObjectPool
      {
        get
        {
          return this.m_owner.m_bossObjectPool;
        }
        set
        {
          this.m_owner.m_bossObjectPool = value;
        }
      }

      public GameObject m_singleBossRankGameObject
      {
        get
        {
          return this.m_owner.m_singleBossRankGameObject;
        }
        set
        {
          this.m_owner.m_singleBossRankGameObject = value;
        }
      }

      public GameObject m_allRankGameObject
      {
        get
        {
          return this.m_owner.m_allRankGameObject;
        }
        set
        {
          this.m_owner.m_allRankGameObject = value;
        }
      }

      public CommonUIStateController m_teamDetailPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_teamDetailPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_teamDetailPanelStateCtrl = value;
        }
      }

      public Button m_teamDetailPanelBackgroundButton
      {
        get
        {
          return this.m_owner.m_teamDetailPanelBackgroundButton;
        }
        set
        {
          this.m_owner.m_teamDetailPanelBackgroundButton = value;
        }
      }

      public Transform m_teamDetailPanelHeroGroup
      {
        get
        {
          return this.m_owner.m_teamDetailPanelHeroGroup;
        }
        set
        {
          this.m_owner.m_teamDetailPanelHeroGroup = value;
        }
      }

      public Image m_teamDetailPanelPlayerIconImage
      {
        get
        {
          return this.m_owner.m_teamDetailPanelPlayerIconImage;
        }
        set
        {
          this.m_owner.m_teamDetailPanelPlayerIconImage = value;
        }
      }

      public Transform m_teamDetailPanelHeadFrameDummy
      {
        get
        {
          return this.m_owner.m_teamDetailPanelHeadFrameDummy;
        }
        set
        {
          this.m_owner.m_teamDetailPanelHeadFrameDummy = value;
        }
      }

      public Text m_teamDetailPanelLevelText
      {
        get
        {
          return this.m_owner.m_teamDetailPanelLevelText;
        }
        set
        {
          this.m_owner.m_teamDetailPanelLevelText = value;
        }
      }

      public Text m_teamDetailPanelPlayerNameText
      {
        get
        {
          return this.m_owner.m_teamDetailPanelPlayerNameText;
        }
        set
        {
          this.m_owner.m_teamDetailPanelPlayerNameText = value;
        }
      }

      public Text m_teamDetailPanelHeroPowerText
      {
        get
        {
          return this.m_owner.m_teamDetailPanelHeroPowerText;
        }
        set
        {
          this.m_owner.m_teamDetailPanelHeroPowerText = value;
        }
      }

      public GameObject m_togglePrefab
      {
        get
        {
          return this.m_owner.m_togglePrefab;
        }
        set
        {
          this.m_owner.m_togglePrefab = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public AncientCallRankingListUITask m_ancientCallRankingListUITask
      {
        get
        {
          return this.m_owner.m_ancientCallRankingListUITask;
        }
        set
        {
          this.m_owner.m_ancientCallRankingListUITask = value;
        }
      }

      public AncientCallSelfRankAllUIController m_selfRankAllUIController
      {
        get
        {
          return this.m_owner.m_selfRankAllUIController;
        }
        set
        {
          this.m_owner.m_selfRankAllUIController = value;
        }
      }

      public AncientCallSelfRankSingleBossUIController m_selfRankSingleBossUIController
      {
        get
        {
          return this.m_owner.m_selfRankSingleBossUIController;
        }
        set
        {
          this.m_owner.m_selfRankSingleBossUIController = value;
        }
      }

      public GameObjectPool m_filterTogglePool
      {
        get
        {
          return this.m_owner.m_filterTogglePool;
        }
        set
        {
          this.m_owner.m_filterTogglePool = value;
        }
      }

      public Dictionary<int, Toggle> m_filterToggleDic
      {
        get
        {
          return this.m_owner.m_filterToggleDic;
        }
        set
        {
          this.m_owner.m_filterToggleDic = value;
        }
      }

      public int m_selectBossId
      {
        get
        {
          return this.m_owner.m_selectBossId;
        }
        set
        {
          this.m_owner.m_selectBossId = value;
        }
      }

      public RankingListInfo m_rankingListInfo
      {
        get
        {
          return this.m_owner.m_rankingListInfo;
        }
        set
        {
          this.m_owner.m_rankingListInfo = value;
        }
      }

      public Toggle m_selectSocialToggle
      {
        get
        {
          return this.m_owner.m_selectSocialToggle;
        }
        set
        {
          this.m_owner.m_selectSocialToggle = value;
        }
      }

      public static int AllBoss
      {
        get
        {
          return 0;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void CloseTeamDetailPanel()
      {
        this.m_owner.CloseTeamDetailPanel();
      }

      public void UpdateVeiwRank()
      {
        this.m_owner.UpdateVeiwRank();
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnTotalBossListItemFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnTotalBossListItemFill(itemCtrl);
      }

      public void OnSingleBossListItemFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnSingleBossListItemFill(itemCtrl);
      }

      public void OnBattleArraryClick(RankingTargetPlayerInfo info, int bossId)
      {
        this.m_owner.OnBattleArraryClick(info, bossId);
      }

      public void OnSelfBattleArrayClick(int bossId)
      {
        this.m_owner.OnSelfBattleArrayClick(bossId);
      }

      public void OnSelectRankClick()
      {
        this.m_owner.OnSelectRankClick();
      }

      public void OnSelectSocialRelationClick(Toggle selectToggle)
      {
        this.m_owner.OnSelectSocialRelationClick(selectToggle);
      }

      public void OnSelectRankToggle(int bossId)
      {
        this.m_owner.OnSelectRankToggle(bossId);
      }
    }
  }
}
