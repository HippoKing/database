﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SoldierDescUIController : UIControllerBase
  {
    [AutoBind("./Faction/JobImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailGraphic;
    [AutoBind("./Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierRangeText;
    [AutoBind("./Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMoveText;
    [AutoBind("./Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImg;
    [AutoBind("./Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImg2;
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierTitleText;
    [AutoBind("./Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierWeakText;
    [AutoBind("./Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierStrongText;
    [AutoBind("./HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPValueText;
    [AutoBind("./DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFValueText;
    [AutoBind("./AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATValueText;
    [AutoBind("./MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFValueText;
    [AutoBind("./HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPAddText;
    [AutoBind("./DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFAddText;
    [AutoBind("./AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATAddText;
    [AutoBind("./MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFAddText;
    private UISpineGraphic m_soldierGraphic;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierDesc(ConfigDataSoldierInfo soldierInfo, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierDesc(
      ConfigDataSoldierInfo soldierInfo,
      BattleHero hero,
      List<TrainingTech> techs,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel(
      ConfigDataSoldierInfo soldierInfo,
      HeroPropertyComputer computer,
      string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropValue(int v0, int v1, bool isAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      int team,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    private void DestroySpineGraphic(UISpineGraphic g)
    {
      g?.Destroy();
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
