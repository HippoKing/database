﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceRecordHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class VoiceRecordHelper
  {
    private int m_recordIdSeed;
    private bool m_isRecording;
    private bool m_isCancelRecord;
    private int m_preRecordPos;
    private AudioClip m_recordClip;
    private int m_frequency;
    private DateTime m_startTime;
    private DateTime m_endTime;
    private string m_deviceName;
    private DateTime m_recordNextTickTime;
    private ChatChannel? m_currRecordChannel;
    private int VoiceMaxLength;
    private int VoiceMinLength;
    private int EncodeAndDecodeFrameSize;
    private bool m_isXFRecognizing;
    private LinkedList<string> m_xfWaitForRecognizeFilePathList;
    private LinkedList<string> m_xfRecognizedResultList;
    private XunfeiSDKWrapper m_xfWrapper;
    private static VoiceRecordHelper m_instance;
    private bool m_isRunningStartRecord;

    [MethodImpl((MethodImplOptions) 32768)]
    private VoiceRecordHelper(string deviceName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitXFSDKManager(XunfeiSDKWrapper wrapper)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartRecord(ChatChannel channel, Action onMicrophoneStarted)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMicrophoneAvailable()
    {
      return Microphone.devices.Length != 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStopRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRecording()
    {
      return this.m_isRecording;
    }

    public void Tick()
    {
      this.TickForRecordVoice();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArrivalMaxLength()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTooShort()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetRecordLength()
    {
      // ISSUE: unable to decompile the method.
    }

    public void CancelRecord()
    {
      this.m_isCancelRecord = true;
      this.StopRecord();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSaveAudioFullPath(int voiceRecordId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForRecordVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartXFRecognize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnXFSpeechRecognizedSuccess(string returnTxt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnXFSpeechRecognizeFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeleteAudioTempFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AllocRecordId()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ChatChannel> EventOnVoiceRecordTimeout
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public LinkedList<string> XFRecognizedResultList
    {
      get
      {
        return this.m_xfRecognizedResultList;
      }
    }

    private IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static VoiceRecordHelper Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action m_eventStopRecord
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
