﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallSelfRankSingleBossUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AncientCallSelfRankSingleBossUIController : UIControllerBase
  {
    [AutoBind("./PlayerInfo/PlayerIcon/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerInfo/DamageValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damageValueText;
    [AutoBind("./PlayerInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PlayerInfo/PlayerIcon/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText;
    [AutoBind("./PlayerInfo/PlayerIcon/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankImage;
    [AutoBind("./PlayerInfo/PlayerIcon/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./PlayerInfo/PlayerIcon/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./PlayerInfo/BattleArrayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleArrayButton;
    private ProjectLPlayerContext m_playerContext;
    private ClientConfigDataLoader m_configDataLoader;
    private RankingListInfo m_rankingListInfo;
    private bool m_isSocialRanking;
    private int m_bossId;
    [DoNotToLua]
    private AncientCallSelfRankSingleBossUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetRankPlayerInfoRankingListInfoInt32Boolean_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_GetRankLevelSpriteInt32_hotfix;
    private LuaFunction m_OnBattleArraryClick_hotfix;
    private LuaFunction m_add_EventOnBattleArraryClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnBattleArraryClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRankPlayerInfo(
      RankingListInfo rankingListInfo,
      int bossId,
      bool isSocialRanking = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleArraryClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnBattleArraryClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public AncientCallSelfRankSingleBossUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleArraryClick(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleArraryClick(int obj)
    {
      this.EventOnBattleArraryClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AncientCallSelfRankSingleBossUIController m_owner;

      public LuaExportHelper(AncientCallSelfRankSingleBossUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnBattleArraryClick(int obj)
      {
        this.m_owner.__callDele_EventOnBattleArraryClick(obj);
      }

      public void __clearDele_EventOnBattleArraryClick(int obj)
      {
        this.m_owner.__clearDele_EventOnBattleArraryClick(obj);
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Text m_damageValueText
      {
        get
        {
          return this.m_owner.m_damageValueText;
        }
        set
        {
          this.m_owner.m_damageValueText = value;
        }
      }

      public Text m_scoreText
      {
        get
        {
          return this.m_owner.m_scoreText;
        }
        set
        {
          this.m_owner.m_scoreText = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_rankText
      {
        get
        {
          return this.m_owner.m_rankText;
        }
        set
        {
          this.m_owner.m_rankText = value;
        }
      }

      public Image m_rankImage
      {
        get
        {
          return this.m_owner.m_rankImage;
        }
        set
        {
          this.m_owner.m_rankImage = value;
        }
      }

      public Image m_playerIconImage
      {
        get
        {
          return this.m_owner.m_playerIconImage;
        }
        set
        {
          this.m_owner.m_playerIconImage = value;
        }
      }

      public Transform m_headFrameTransform
      {
        get
        {
          return this.m_owner.m_headFrameTransform;
        }
        set
        {
          this.m_owner.m_headFrameTransform = value;
        }
      }

      public Button m_battleArrayButton
      {
        get
        {
          return this.m_owner.m_battleArrayButton;
        }
        set
        {
          this.m_owner.m_battleArrayButton = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public ClientConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public RankingListInfo m_rankingListInfo
      {
        get
        {
          return this.m_owner.m_rankingListInfo;
        }
        set
        {
          this.m_owner.m_rankingListInfo = value;
        }
      }

      public bool m_isSocialRanking
      {
        get
        {
          return this.m_owner.m_isSocialRanking;
        }
        set
        {
          this.m_owner.m_isSocialRanking = value;
        }
      }

      public int m_bossId
      {
        get
        {
          return this.m_owner.m_bossId;
        }
        set
        {
          this.m_owner.m_bossId = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public Sprite GetRankLevelSprite(int rankLevel)
      {
        return this.m_owner.GetRankLevelSprite(rankLevel);
      }

      public void OnBattleArraryClick()
      {
        this.m_owner.OnBattleArraryClick();
      }
    }
  }
}
