﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private RiftBackgroundUIController m_riftBackgroundUIController;
    private RiftChapterUIController m_riftChapterUIController;
    private RiftLevelUIController m_riftLevelUIController;
    private PlayerResourceUIController m_riftChapterPlayerResourceUIController;
    private PlayerResourceUIController m_riftLevelPlayerResourceUIController;
    private ConfigDataRiftChapterInfo m_chapterInfo;
    private bool m_needReturnToChapter;
    private bool m_needPlayRiftLevelOpenAnimation;
    private BattleLevelInfoUITask m_battleLevelInfoUITask;
    private RiftUITask.ViewState m_viewState;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftBackgroundUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitRiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChapters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowChapterProgress(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    private void HideChapterProgress(ConfigDataRiftChapterInfo chapterInfo)
    {
      this.m_riftChapterUIController.HideChapterProgress();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectChapter(ConfigDataRiftChapterInfo chapterInfo, bool playOpenAnim)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void RiftChapterUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Rift);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void RiftChapterUIController_OnSelectChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      this.SelectChapter(chapterInfo, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnSwitchChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnGoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitRiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRiftLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStarReward(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetStarReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenRiftChapterHard(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleLevelInfoUITask(
      ConfigDataRiftLevelInfo riftLevelInfo,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void RiftLevelUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Rift);
    }

    private void RiftLevelUIController_OnReturnToWorld()
    {
      this.RiftChapterUIController_OnReturnToWorld();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnChangeHard()
    {
      // ISSUE: unable to decompile the method.
    }

    private void RiftLevelUIController_OnSelectRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo,
      NeedGoods needGoods = null)
    {
      this.StartBattleLevelInfoUITask(riftLevelInfo, needGoods);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnGetStarReward(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnGoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void BattleLevelInfoUITask_OnRiftRaidComplete(int levelId)
    {
      this.m_riftLevelPlayerResourceUIController.UpdatePlayerResource();
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum ViewState
    {
      None,
      Chapter,
      RiftLevel,
    }
  }
}
