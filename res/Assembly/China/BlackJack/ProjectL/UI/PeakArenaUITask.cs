﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private PeakArenaUIController m_peakArenaUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    public PeakArenaKnockoutTimeUIController m_knockoutTimeUIController;
    public PeakArenaBuyJettonUIController m_buyJettonUIController;
    public PeakArenaUseJettonUIController m_useJettonUIController;
    private int m_nowSeconds;
    private int m_knockoutGroupId;
    private PeakArenaPanelType m_peakPanelType;
    private DateTime m_matchingUIBeginTime;
    private DateTime m_matchingReqSendTime;
    private RealTimePVPMode m_matchingBattleMode;
    private bool m_canFlushOfflineTopRankPlayers;
    private PeakArenaPlayOffInfoGetNetTask m_curNetTask;
    private float m_lastUpdateTime;
    public const string ParamsKey_knockoutGroupId = "KnockoutGroupId";
    [DoNotToLua]
    private PeakArenaUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_InitPeakArenaUIController_hotfix;
    private LuaFunction m_UninitPeakArenaUIController_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_SendSeasonGetNetTaskInClashState_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_Refresh_hotfix;
    private LuaFunction m_LoadResList`1Action_hotfix;
    private LuaFunction m_UpdatePeakArenaPopWindow_hotfix;
    private LuaFunction m_UpdatePeakMatchingTime_hotfix;
    private LuaFunction m_StartRealTimePVPWaitingForOpponentNetTaskRealTimePVPMode_hotfix;
    private LuaFunction m_StartMatchingRealTimePVPMode_hotfix;
    private LuaFunction m_PlayerContext_OnRealTimePVPMatchupNtfInt32RealTimePVPMode_hotfix;
    private LuaFunction m_PlayerResourceUIController_OnAddCrystal_hotfix;
    private LuaFunction m_PlayerResourceUIController_OnAddGold_hotfix;
    private LuaFunction m_OnAddJettonTicketClick_hotfix;
    private LuaFunction m_OnBuyJettonSuccess_hotfix;
    private LuaFunction m_PeakArenaUIController_OnReturn_hotfix;
    private LuaFunction m_PeakArenaUIController_OnShowHelp_hotfix;
    private LuaFunction m_PeakArenaUIController_OnManagementTeam_hotfix;
    private LuaFunction m_PeakArenaUIController_EventOnShowPeakPanelPeakArenaPanelType_hotfix;
    private LuaFunction m_GetPeakArenaWeekFirstWinRewardsAndShow_hotfix;
    private LuaFunction m_OnGetRewardGoodsUITaskClose_hotfix;
    private LuaFunction m_FlushOfflineTopRankPlayers_hotfix;
    private LuaFunction m_GetSeasonKnockInfoInfoAction_hotfix;
    private LuaFunction m_GetPeakArenaBattleReports_hotfix;
    private LuaFunction m_PeakArenaUIController_OnPeakClashCasualChallengeButtonClick_hotfix;
    private LuaFunction m_PeakArenaUIController_OnPeakClashLadderChallengeButtonClick_hotfix;
    private LuaFunction m_StartPeakArenaPlayOffBattleSetReadyNetTask_hotfix;
    private LuaFunction m_PeakArenaUIController_OnMatchingCancel_hotfix;
    private LuaFunction m_PeakArenaUIController_OnShareBattleReportPeakArenaBattleReportTypeUInt64_hotfix;
    private LuaFunction m_PeakArenaUIController_OnReportNameChangeUInt64_hotfix;
    private LuaFunction m_PeakArenaUIController_OnReportReplyUInt64_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(Action<PeakArenaUITask> OnFinish = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartUITaskInteral(Action<PeakArenaUITask> OnFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void SendGetSeasonInfoNetTask(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPeakArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitPeakArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendSeasonGetNetTaskInClashState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadRes(List<string> assetList, Action onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaPopWindow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakMatchingTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPWaitingForOpponentNetTask(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMatching(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnRealTimePVPMatchupNtf(int result, RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddJettonTicketClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyJettonSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnManagementTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_EventOnShowPeakPanel(PeakArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetPeakArenaWeekFirstWinRewardsAndShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRewardGoodsUITaskClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushOfflineTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetSeasonKnockInfoInfo(Action onFinish = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetPeakArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnPeakClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnPeakClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaPlayOffBattleSetReadyNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnMatchingCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnShareBattleReport(
      PeakArenaBattleReportType type,
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnReportNameChange(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnReportReply(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaUITask m_owner;

      public LuaExportHelper(PeakArenaUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public PeakArenaUIController m_peakArenaUIController
      {
        get
        {
          return this.m_owner.m_peakArenaUIController;
        }
        set
        {
          this.m_owner.m_peakArenaUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public int m_knockoutGroupId
      {
        get
        {
          return this.m_owner.m_knockoutGroupId;
        }
        set
        {
          this.m_owner.m_knockoutGroupId = value;
        }
      }

      public PeakArenaPanelType m_peakPanelType
      {
        get
        {
          return this.m_owner.m_peakPanelType;
        }
        set
        {
          this.m_owner.m_peakPanelType = value;
        }
      }

      public DateTime m_matchingUIBeginTime
      {
        get
        {
          return this.m_owner.m_matchingUIBeginTime;
        }
        set
        {
          this.m_owner.m_matchingUIBeginTime = value;
        }
      }

      public DateTime m_matchingReqSendTime
      {
        get
        {
          return this.m_owner.m_matchingReqSendTime;
        }
        set
        {
          this.m_owner.m_matchingReqSendTime = value;
        }
      }

      public RealTimePVPMode m_matchingBattleMode
      {
        get
        {
          return this.m_owner.m_matchingBattleMode;
        }
        set
        {
          this.m_owner.m_matchingBattleMode = value;
        }
      }

      public bool m_canFlushOfflineTopRankPlayers
      {
        get
        {
          return this.m_owner.m_canFlushOfflineTopRankPlayers;
        }
        set
        {
          this.m_owner.m_canFlushOfflineTopRankPlayers = value;
        }
      }

      public PeakArenaPlayOffInfoGetNetTask m_curNetTask
      {
        get
        {
          return this.m_owner.m_curNetTask;
        }
        set
        {
          this.m_owner.m_curNetTask = value;
        }
      }

      public float m_lastUpdateTime
      {
        get
        {
          return this.m_owner.m_lastUpdateTime;
        }
        set
        {
          this.m_owner.m_lastUpdateTime = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public static void StartUITaskInteral(Action<PeakArenaUITask> OnFinish)
      {
        PeakArenaUITask.StartUITaskInteral(OnFinish);
      }

      public static void SendGetSeasonInfoNetTask(Action onEnd)
      {
        PeakArenaUITask.SendGetSeasonInfoNetTask(onEnd);
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void InitPeakArenaUIController()
      {
        this.m_owner.InitPeakArenaUIController();
      }

      public void UninitPeakArenaUIController()
      {
        this.m_owner.UninitPeakArenaUIController();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void SendSeasonGetNetTaskInClashState()
      {
        this.m_owner.SendSeasonGetNetTaskInClashState();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void UpdatePeakArenaPopWindow()
      {
        this.m_owner.UpdatePeakArenaPopWindow();
      }

      public void UpdatePeakMatchingTime()
      {
        this.m_owner.UpdatePeakMatchingTime();
      }

      public void StartRealTimePVPWaitingForOpponentNetTask(RealTimePVPMode mode)
      {
        this.m_owner.StartRealTimePVPWaitingForOpponentNetTask(mode);
      }

      public void StartMatching(RealTimePVPMode mode)
      {
        this.m_owner.StartMatching(mode);
      }

      public void PlayerContext_OnRealTimePVPMatchupNtf(int result, RealTimePVPMode mode)
      {
        this.m_owner.PlayerContext_OnRealTimePVPMatchupNtf(result, mode);
      }

      public void PlayerResourceUIController_OnAddCrystal()
      {
        this.m_owner.PlayerResourceUIController_OnAddCrystal();
      }

      public void PlayerResourceUIController_OnAddGold()
      {
        this.m_owner.PlayerResourceUIController_OnAddGold();
      }

      public void OnAddJettonTicketClick()
      {
        this.m_owner.OnAddJettonTicketClick();
      }

      public void OnBuyJettonSuccess()
      {
        this.m_owner.OnBuyJettonSuccess();
      }

      public void PeakArenaUIController_OnReturn()
      {
        this.m_owner.PeakArenaUIController_OnReturn();
      }

      public void PeakArenaUIController_OnShowHelp()
      {
        this.m_owner.PeakArenaUIController_OnShowHelp();
      }

      public void PeakArenaUIController_OnManagementTeam()
      {
        this.m_owner.PeakArenaUIController_OnManagementTeam();
      }

      public void PeakArenaUIController_EventOnShowPeakPanel(PeakArenaPanelType panelType)
      {
        this.m_owner.PeakArenaUIController_EventOnShowPeakPanel(panelType);
      }

      public void GetPeakArenaWeekFirstWinRewardsAndShow()
      {
        this.m_owner.GetPeakArenaWeekFirstWinRewardsAndShow();
      }

      public void OnGetRewardGoodsUITaskClose()
      {
        this.m_owner.OnGetRewardGoodsUITaskClose();
      }

      public void FlushOfflineTopRankPlayers()
      {
        this.m_owner.FlushOfflineTopRankPlayers();
      }

      public void GetSeasonKnockInfoInfo(Action onFinish)
      {
        this.m_owner.GetSeasonKnockInfoInfo(onFinish);
      }

      public void GetPeakArenaBattleReports()
      {
        this.m_owner.GetPeakArenaBattleReports();
      }

      public void PeakArenaUIController_OnPeakClashCasualChallengeButtonClick()
      {
        this.m_owner.PeakArenaUIController_OnPeakClashCasualChallengeButtonClick();
      }

      public void PeakArenaUIController_OnPeakClashLadderChallengeButtonClick()
      {
        this.m_owner.PeakArenaUIController_OnPeakClashLadderChallengeButtonClick();
      }

      public void StartPeakArenaPlayOffBattleSetReadyNetTask()
      {
        this.m_owner.StartPeakArenaPlayOffBattleSetReadyNetTask();
      }

      public void PeakArenaUIController_OnMatchingCancel()
      {
        this.m_owner.PeakArenaUIController_OnMatchingCancel();
      }

      public void PeakArenaUIController_OnShareBattleReport(
        PeakArenaBattleReportType type,
        ulong battleReportInstanceId)
      {
        this.m_owner.PeakArenaUIController_OnShareBattleReport(type, battleReportInstanceId);
      }

      public void PeakArenaUIController_OnReportNameChange(ulong reportId)
      {
        this.m_owner.PeakArenaUIController_OnReportNameChange(reportId);
      }

      public void PeakArenaUIController_OnReportReply(ulong reportId)
      {
        this.m_owner.PeakArenaUIController_OnReportReply(reportId);
      }
    }
  }
}
