﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AncientCallUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./TopGroup/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./TopGroup/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/LeftPanel/DetailInfoToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankButton;
    [AutoBind("./Margin/LeftPanel/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardButton;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerGroup;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup/PlayerInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_playerInfoPrefab;
    [AutoBind("./BossListSelect", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_bossListScrollRect;
    [AutoBind("./BossListSelect/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private VerticalLayoutGroup m_bossListVerticalLayoutGroup;
    [AutoBind("./BossListSelect/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_bossListContent;
    [AutoBind("./BossListSelect/Viewport/Content/BossListSelectIcon", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_bossItemPrefab;
    [AutoBind("./Margin/TimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./RankingReward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankRewardPanel;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private AncientCallUITask m_ancientCallUITask;
    private AncientCallRankRewardUIController m_rewardUIController;
    private GameObjectPool<AncientCallBossItemUIController> bossItemPool;
    private List<int> m_bossIdList;
    private List<AncientCallPlayerInfoItemUIController> m_rankPlayerItemUIControllerList;
    private bool isFirstUpdateView;
    [DoNotToLua]
    private AncientCallUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetBossListScrollPos_hotfix;
    private LuaFunction m_UpdateData_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_OnAncientCallRankClickInt32_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnHelpClick_hotfix;
    private LuaFunction m_OnRewardClick_hotfix;
    private LuaFunction m_OnRankClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SetBossListScrollPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAncientCallRankClick(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public AncientCallUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AncientCallUIController m_owner;

      public LuaExportHelper(AncientCallUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiState
      {
        get
        {
          return this.m_owner.m_uiState;
        }
        set
        {
          this.m_owner.m_uiState = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_rankButton
      {
        get
        {
          return this.m_owner.m_rankButton;
        }
        set
        {
          this.m_owner.m_rankButton = value;
        }
      }

      public Button m_rewardButton
      {
        get
        {
          return this.m_owner.m_rewardButton;
        }
        set
        {
          this.m_owner.m_rewardButton = value;
        }
      }

      public Transform m_playerGroup
      {
        get
        {
          return this.m_owner.m_playerGroup;
        }
        set
        {
          this.m_owner.m_playerGroup = value;
        }
      }

      public GameObject m_playerInfoPrefab
      {
        get
        {
          return this.m_owner.m_playerInfoPrefab;
        }
        set
        {
          this.m_owner.m_playerInfoPrefab = value;
        }
      }

      public ScrollRect m_bossListScrollRect
      {
        get
        {
          return this.m_owner.m_bossListScrollRect;
        }
        set
        {
          this.m_owner.m_bossListScrollRect = value;
        }
      }

      public VerticalLayoutGroup m_bossListVerticalLayoutGroup
      {
        get
        {
          return this.m_owner.m_bossListVerticalLayoutGroup;
        }
        set
        {
          this.m_owner.m_bossListVerticalLayoutGroup = value;
        }
      }

      public Transform m_bossListContent
      {
        get
        {
          return this.m_owner.m_bossListContent;
        }
        set
        {
          this.m_owner.m_bossListContent = value;
        }
      }

      public GameObject m_bossItemPrefab
      {
        get
        {
          return this.m_owner.m_bossItemPrefab;
        }
        set
        {
          this.m_owner.m_bossItemPrefab = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public GameObject m_rankRewardPanel
      {
        get
        {
          return this.m_owner.m_rankRewardPanel;
        }
        set
        {
          this.m_owner.m_rankRewardPanel = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public AncientCallUITask m_ancientCallUITask
      {
        get
        {
          return this.m_owner.m_ancientCallUITask;
        }
        set
        {
          this.m_owner.m_ancientCallUITask = value;
        }
      }

      public AncientCallRankRewardUIController m_rewardUIController
      {
        get
        {
          return this.m_owner.m_rewardUIController;
        }
        set
        {
          this.m_owner.m_rewardUIController = value;
        }
      }

      public GameObjectPool<AncientCallBossItemUIController> bossItemPool
      {
        get
        {
          return this.m_owner.bossItemPool;
        }
        set
        {
          this.m_owner.bossItemPool = value;
        }
      }

      public List<int> m_bossIdList
      {
        get
        {
          return this.m_owner.m_bossIdList;
        }
        set
        {
          this.m_owner.m_bossIdList = value;
        }
      }

      public List<AncientCallPlayerInfoItemUIController> m_rankPlayerItemUIControllerList
      {
        get
        {
          return this.m_owner.m_rankPlayerItemUIControllerList;
        }
        set
        {
          this.m_owner.m_rankPlayerItemUIControllerList = value;
        }
      }

      public bool isFirstUpdateView
      {
        get
        {
          return this.m_owner.isFirstUpdateView;
        }
        set
        {
          this.m_owner.isFirstUpdateView = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public IEnumerator SetBossListScrollPos()
      {
        return this.m_owner.SetBossListScrollPos();
      }

      public void UpdateData()
      {
        this.m_owner.UpdateData();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void OnAncientCallRankClick(int bossId)
      {
        this.m_owner.OnAncientCallRankClick(bossId);
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnHelpClick()
      {
        this.m_owner.OnHelpClick();
      }

      public void OnRewardClick()
      {
        this.m_owner.OnRewardClick();
      }

      public void OnRankClick()
      {
        this.m_owner.OnRankClick();
      }
    }
  }
}
