﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRecordRewardListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class AncientCallRecordRewardListItemUIController : UIControllerBase
  {
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_targetDamageText;
    [AutoBind("./CommonRewardItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    public Transform m_rewardGroup;
    [AutoBind("./EvaluateGroupContent/EvaluateLetterItemBig", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_evaluateImage;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAncientCallRecordReward(
      ConfigDataAncientCallRecordRewardInfo rewardInfo,
      GameObject rewardGoodsPrefab)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
