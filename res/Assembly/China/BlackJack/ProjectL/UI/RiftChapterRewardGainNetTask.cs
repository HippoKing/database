﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftChapterRewardGainNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftChapterRewardGainNetTask : UINetTask
  {
    private int m_chapterId;
    private int m_index;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapterRewardGainNetTask(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRiftChapterRewardGainAck(int result, BattleReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleReward Reward { private set; get; }
  }
}
