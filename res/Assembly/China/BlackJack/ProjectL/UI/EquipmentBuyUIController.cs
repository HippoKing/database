﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentBuyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EquipmentBuyUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentBuyUIAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/EquipType", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipTypeAnimation;
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./Detail/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvText;
    [AutoBind("./Detail/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_expText;
    [AutoBind("./Detail/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Detail/Item/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Detail/Item/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Detail/Item/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Detail/Item/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Detail/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipLimitAnimation;
    [AutoBind("./Detail/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipLimitContent;
    [AutoBind("./Detail/EquipGroup/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Detail/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HPGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_HPText;
    [AutoBind("./Detail/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ATGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ATText;
    [AutoBind("./Detail/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DFGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DFText;
    [AutoBind("./Detail/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicDFGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicDFText;
    [AutoBind("./Detail/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicText;
    [AutoBind("./Detail/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DexGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DexText;
    [AutoBind("./Detail/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDescAnimation;
    [AutoBind("./Detail/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_notEquipSkillGameObject;
    [AutoBind("./Detail/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillNameText;
    [AutoBind("./Detail/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDescText;
    [AutoBind("./Detail/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillUnlockConditionText;
    [AutoBind("./Detail/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillBelongBGText;
    [AutoBind("./Detail/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillBelongText;
    [AutoBind("./Detail/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyButton;
    [AutoBind("./Detail/BuyButton/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currencyImage;
    [AutoBind("./Detail/BuyButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currencyCount;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private EquipmentBuyUITask m_equipmentBuyUITask;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    [DoNotToLua]
    private EquipmentBuyUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitStoreIdInt32_hotfix;
    private LuaFunction m_PlayCloseAnimationAction_hotfix;
    private LuaFunction m_GetEquipmentTypeAnimationNameEquipmentType_hotfix;
    private LuaFunction m_SetSkillDescriptionConfigDataEquipmentInfo_hotfix;
    private LuaFunction m_SetEquipConditionConfigDataEquipmentInfo_hotfix;
    private LuaFunction m_ClosePropDisplay_hotfix;
    private LuaFunction m_SetEquipmentPropItemPropertyModifyTypeInt32_hotfix;
    private LuaFunction m_OnBGClick_hotfix;
    private LuaFunction m_OnBuyClick_hotfix;
    private LuaFunction m_add_EventOnCloseClickAction_hotfix;
    private LuaFunction m_remove_EventOnCloseClickAction_hotfix;
    private LuaFunction m_add_EventOnBuyClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnBuyClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayCloseAnimation(Action onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetEquipmentTypeAnimationName(EquipmentType equipmentType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillDescription(ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipCondition(ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePropDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropItem(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnCloseClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Goods> EventOnBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public EquipmentBuyUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCloseClick()
    {
      this.EventOnCloseClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuyClick(Goods obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuyClick(Goods obj)
    {
      this.EventOnBuyClick = (Action<Goods>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private EquipmentBuyUIController m_owner;

      public LuaExportHelper(EquipmentBuyUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnCloseClick()
      {
        this.m_owner.__callDele_EventOnCloseClick();
      }

      public void __clearDele_EventOnCloseClick()
      {
        this.m_owner.__clearDele_EventOnCloseClick();
      }

      public void __callDele_EventOnBuyClick(Goods obj)
      {
        this.m_owner.__callDele_EventOnBuyClick(obj);
      }

      public void __clearDele_EventOnBuyClick(Goods obj)
      {
        this.m_owner.__clearDele_EventOnBuyClick(obj);
      }

      public CommonUIStateController m_equipmentBuyUIAnimation
      {
        get
        {
          return this.m_owner.m_equipmentBuyUIAnimation;
        }
        set
        {
          this.m_owner.m_equipmentBuyUIAnimation = value;
        }
      }

      public Button m_bgButton
      {
        get
        {
          return this.m_owner.m_bgButton;
        }
        set
        {
          this.m_owner.m_bgButton = value;
        }
      }

      public CommonUIStateController m_equipTypeAnimation
      {
        get
        {
          return this.m_owner.m_equipTypeAnimation;
        }
        set
        {
          this.m_owner.m_equipTypeAnimation = value;
        }
      }

      public Text m_titleText
      {
        get
        {
          return this.m_owner.m_titleText;
        }
        set
        {
          this.m_owner.m_titleText = value;
        }
      }

      public Text m_lvText
      {
        get
        {
          return this.m_owner.m_lvText;
        }
        set
        {
          this.m_owner.m_lvText = value;
        }
      }

      public Text m_expText
      {
        get
        {
          return this.m_owner.m_expText;
        }
        set
        {
          this.m_owner.m_expText = value;
        }
      }

      public Image m_descProgressImage
      {
        get
        {
          return this.m_owner.m_descProgressImage;
        }
        set
        {
          this.m_owner.m_descProgressImage = value;
        }
      }

      public Image m_descIcon
      {
        get
        {
          return this.m_owner.m_descIcon;
        }
        set
        {
          this.m_owner.m_descIcon = value;
        }
      }

      public Image m_descIconBg
      {
        get
        {
          return this.m_owner.m_descIconBg;
        }
        set
        {
          this.m_owner.m_descIconBg = value;
        }
      }

      public GameObject m_descSSREffect
      {
        get
        {
          return this.m_owner.m_descSSREffect;
        }
        set
        {
          this.m_owner.m_descSSREffect = value;
        }
      }

      public GameObject m_descStarGroup
      {
        get
        {
          return this.m_owner.m_descStarGroup;
        }
        set
        {
          this.m_owner.m_descStarGroup = value;
        }
      }

      public CommonUIStateController m_equipLimitAnimation
      {
        get
        {
          return this.m_owner.m_equipLimitAnimation;
        }
        set
        {
          this.m_owner.m_equipLimitAnimation = value;
        }
      }

      public GameObject m_equipLimitContent
      {
        get
        {
          return this.m_owner.m_equipLimitContent;
        }
        set
        {
          this.m_owner.m_equipLimitContent = value;
        }
      }

      public Text m_descEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_descEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_descEquipUnlimitText = value;
        }
      }

      public GameObject m_HPGameObject
      {
        get
        {
          return this.m_owner.m_HPGameObject;
        }
        set
        {
          this.m_owner.m_HPGameObject = value;
        }
      }

      public Text m_HPText
      {
        get
        {
          return this.m_owner.m_HPText;
        }
        set
        {
          this.m_owner.m_HPText = value;
        }
      }

      public GameObject m_ATGameObject
      {
        get
        {
          return this.m_owner.m_ATGameObject;
        }
        set
        {
          this.m_owner.m_ATGameObject = value;
        }
      }

      public Text m_ATText
      {
        get
        {
          return this.m_owner.m_ATText;
        }
        set
        {
          this.m_owner.m_ATText = value;
        }
      }

      public GameObject m_DFGameObject
      {
        get
        {
          return this.m_owner.m_DFGameObject;
        }
        set
        {
          this.m_owner.m_DFGameObject = value;
        }
      }

      public Text m_DFText
      {
        get
        {
          return this.m_owner.m_DFText;
        }
        set
        {
          this.m_owner.m_DFText = value;
        }
      }

      public GameObject m_MagicDFGameObject
      {
        get
        {
          return this.m_owner.m_MagicDFGameObject;
        }
        set
        {
          this.m_owner.m_MagicDFGameObject = value;
        }
      }

      public Text m_MagicDFText
      {
        get
        {
          return this.m_owner.m_MagicDFText;
        }
        set
        {
          this.m_owner.m_MagicDFText = value;
        }
      }

      public GameObject m_MagicGameObject
      {
        get
        {
          return this.m_owner.m_MagicGameObject;
        }
        set
        {
          this.m_owner.m_MagicGameObject = value;
        }
      }

      public Text m_MagicText
      {
        get
        {
          return this.m_owner.m_MagicText;
        }
        set
        {
          this.m_owner.m_MagicText = value;
        }
      }

      public GameObject m_DexGameObject
      {
        get
        {
          return this.m_owner.m_DexGameObject;
        }
        set
        {
          this.m_owner.m_DexGameObject = value;
        }
      }

      public Text m_DexText
      {
        get
        {
          return this.m_owner.m_DexText;
        }
        set
        {
          this.m_owner.m_DexText = value;
        }
      }

      public CommonUIStateController m_skillDescAnimation
      {
        get
        {
          return this.m_owner.m_skillDescAnimation;
        }
        set
        {
          this.m_owner.m_skillDescAnimation = value;
        }
      }

      public GameObject m_notEquipSkillGameObject
      {
        get
        {
          return this.m_owner.m_notEquipSkillGameObject;
        }
        set
        {
          this.m_owner.m_notEquipSkillGameObject = value;
        }
      }

      public Text m_skillNameText
      {
        get
        {
          return this.m_owner.m_skillNameText;
        }
        set
        {
          this.m_owner.m_skillNameText = value;
        }
      }

      public Text m_skillDescText
      {
        get
        {
          return this.m_owner.m_skillDescText;
        }
        set
        {
          this.m_owner.m_skillDescText = value;
        }
      }

      public Text m_skillUnlockConditionText
      {
        get
        {
          return this.m_owner.m_skillUnlockConditionText;
        }
        set
        {
          this.m_owner.m_skillUnlockConditionText = value;
        }
      }

      public GameObject m_skillBelongBGText
      {
        get
        {
          return this.m_owner.m_skillBelongBGText;
        }
        set
        {
          this.m_owner.m_skillBelongBGText = value;
        }
      }

      public Text m_skillBelongText
      {
        get
        {
          return this.m_owner.m_skillBelongText;
        }
        set
        {
          this.m_owner.m_skillBelongText = value;
        }
      }

      public Button m_buyButton
      {
        get
        {
          return this.m_owner.m_buyButton;
        }
        set
        {
          this.m_owner.m_buyButton = value;
        }
      }

      public Image m_currencyImage
      {
        get
        {
          return this.m_owner.m_currencyImage;
        }
        set
        {
          this.m_owner.m_currencyImage = value;
        }
      }

      public Text m_currencyCount
      {
        get
        {
          return this.m_owner.m_currencyCount;
        }
        set
        {
          this.m_owner.m_currencyCount = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public EquipmentBuyUITask m_equipmentBuyUITask
      {
        get
        {
          return this.m_owner.m_equipmentBuyUITask;
        }
        set
        {
          this.m_owner.m_equipmentBuyUITask = value;
        }
      }

      public StoreId m_storeId
      {
        get
        {
          return this.m_owner.m_storeId;
        }
        set
        {
          this.m_owner.m_storeId = value;
        }
      }

      public int m_fixedStoreItemId
      {
        get
        {
          return this.m_owner.m_fixedStoreItemId;
        }
        set
        {
          this.m_owner.m_fixedStoreItemId = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public string GetEquipmentTypeAnimationName(EquipmentType equipmentType)
      {
        return this.m_owner.GetEquipmentTypeAnimationName(equipmentType);
      }

      public void SetSkillDescription(ConfigDataEquipmentInfo equipmentInfo)
      {
        this.m_owner.SetSkillDescription(equipmentInfo);
      }

      public void SetEquipCondition(ConfigDataEquipmentInfo equipmentInfo)
      {
        this.m_owner.SetEquipCondition(equipmentInfo);
      }

      public void ClosePropDisplay()
      {
        this.m_owner.ClosePropDisplay();
      }

      public void SetEquipmentPropItem(PropertyModifyType type, int value)
      {
        this.m_owner.SetEquipmentPropItem(type, value);
      }

      public void OnBGClick()
      {
        this.m_owner.OnBGClick();
      }

      public void OnBuyClick()
      {
        this.m_owner.OnBuyClick();
      }
    }
  }
}
