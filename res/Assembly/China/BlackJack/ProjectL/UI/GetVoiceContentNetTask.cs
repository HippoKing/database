﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GetVoiceContentNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GetVoiceContentNetTask : UINetTask
  {
    private ulong m_instanceId;
    private ChatChannel m_channel;
    private ChatVoiceMessage m_voiceContent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GetVoiceContentNetTask(ChatChannel channel, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnGetVoiceContentAck(ChatVoiceMessage voiceContent)
    {
      this.m_voiceContent = voiceContent;
      this.Stop();
    }

    public ChatVoiceMessage VoiceContent
    {
      get
      {
        return this.m_voiceContent;
      }
    }
  }
}
