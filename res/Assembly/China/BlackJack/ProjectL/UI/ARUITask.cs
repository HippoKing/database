﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ARUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_iosLayerDescArray;
    private UITaskBase.LayerDesc[] m_hwLayerDescArray;
    private static readonly UITaskBase.LayerDesc m_uiLayerDesc;
    private static readonly UITaskBase.LayerDesc m_iosSceneLayerDesc;
    private static readonly UITaskBase.LayerDesc m_hwSceneLayerDesc;
    private UITaskBase.UIControllerDesc[] m_iosCtrlDescArray;
    private UITaskBase.UIControllerDesc[] m_hwCtrlDescArray;
    private static readonly UITaskBase.UIControllerDesc m_uiCtrlDesc;
    private static readonly UITaskBase.UIControllerDesc m_iosSceneCtrlDesc;
    private static readonly UITaskBase.UIControllerDesc m_hwSceneCtrlDesc;
    private ARShowUIController m_arShowUIController;
    private ARShowSceneController m_arShowSceneController;

    [MethodImpl((MethodImplOptions) 32768)]
    public ARUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void UpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CustomLoadAsset(Action collectionCallback, Action finishCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CollectAssetWrap(string assetName)
    {
      this.CollectAsset(assetName);
    }

    public void ReturnLastTask()
    {
      this.ReturnPrevUITask();
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_iosLayerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_iosCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static ARUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
