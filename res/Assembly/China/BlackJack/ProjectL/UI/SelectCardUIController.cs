﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelectCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SelectCardUIController : UIControllerBase
  {
    [AutoBind("./MainUI/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./MainUI", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mainUIGo;
    [AutoBind("./MainUI/Currencys/NormalCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_normalCardNumText;
    [AutoBind("./MainUI/Currencys/DiamondCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_diamondsCardNumText;
    [AutoBind("./MainUI/Currencys/Diamond/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_diamondsAddBtn;
    [AutoBind("./MainUI/Currencys/Diamond/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_diamondsNumText;
    [AutoBind("./MainUI/SummonPanel/Ads/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_adsImage;
    [AutoBind("./MainUI/SummonPanel/Ads/Image/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_adsTimeText;
    [AutoBind("./MainUI/SummonPanel/Ads/Image/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_activityDetailButton;
    [AutoBind("./MainUI/SummonPanel/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_cardPoolToggleGroupGo;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools/TicketSummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_ticketSummonToggle;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools/MagicStoneSummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_magicStoneSummonToggle;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools/EquipSummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipSummonToggle;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_singleSelectBtn;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton/DetailGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectBtnIconImg;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton/DetailGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_singleSelectBtnNumText;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton/DetailGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_singleSelectBtnTitleText;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tenSelectBtn;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/DetailGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tenSelectBtnIconImg;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/DetailGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tenSelectBtnNumText;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/DetailGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tenSelectBtnTitleText;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/U_TenSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenSelectBtn3DEffect;
    [AutoBind("./MainUI/BuyDiamondButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyDiamondButton;
    [AutoBind("./Prefab/SummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_summonToggleItemPrefab;
    [AutoBind("./AfterSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_afterSelectPanel;
    [AutoBind("./AfterSelectPanel/ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectArrowButton;
    [AutoBind("./AfterSelectPanel/OpenNoticePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectOpenNoticePanelGo;
    [AutoBind("./AfterSelectPanel/OpenNoticePanel/ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectOpenNoticePanelImage;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectPanel;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleHeroWordPanel;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/ContinueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_singleSelectContinueImage;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/NewText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectNewTextObj;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/GetText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectGetTextObj;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectStarGroupObj;
    [AutoBind("./AfterSelectPanel/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectSkipButton;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/ItemImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectItemImage;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/FragmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectFragmentIconImg;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/FragmentIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_singleSelectFragmentCount;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/SSR", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectFragmentSSRImg;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/SR", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectFragmentSRImg;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleShareButtonDummy;
    [AutoBind("./AfterSelectPanel/SharePhotoDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./AfterSelectPanel/TenSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenSelectPanel;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/ItemShowPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenSelectContentObj;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_afterSelectConfirmButton;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectAgainButton;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/Single", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectAgainButtonSingleBgObj;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/Ten", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectAgainButtonTenBgObj;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/DetailGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectAgainBtnIconImg;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/DetailGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectAgainBtnNumText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/DetailGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectAgainBtnTitleText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenShareButtonDummy;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/Diamond/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_diamondsAddBtn2;
    [AutoBind("./ChangeCrystalToTicketPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeCrystalToTicketPanel;
    [AutoBind("./ChangeCrystalToTicketPanel/Detail/Tip", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_changeCrystalToTicketPanelTip;
    [AutoBind("./ChangeCrystalToTicketPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeCrystalToTicketPanelCancelButton;
    [AutoBind("./ChangeCrystalToTicketPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeCrystalToTicketPanelConfirmButton;
    [AutoBind("./CrystalAndTicketNotEnoughPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_crystalAndTicketNotEnoughPanel;
    [AutoBind("./CrystalAndTicketNotEnoughPanel/Detail/Tip", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_crystalAndTicketNotEnoughPanelTip;
    [AutoBind("./CrystalAndTicketNotEnoughPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_crystalAndTicketNotEnoughPanelCancelButton;
    [AutoBind("./CrystalAndTicketNotEnoughPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_crystalAndTicketNotEnoughPanelConfirmButton;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/NormalCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_afterSelectPanelNormalCardNumText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/DiamondCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_afterSelectPanelDiamondsCardNumText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/Diamond/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_afterSelectPanelDiamondsNumText;
    [AutoBind("./MainUI/SummonPanel/Margin/ArchiveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_archiveButton;
    [AutoBind("/MainUI/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    [AutoBind("/DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailPanelObj;
    [AutoBind("/DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_detailPanelStateCtrl;
    [AutoBind("/DetailPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailCloseButton;
    [AutoBind("/DetailPanel/BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailReturnBgButton;
    [AutoBind("/DetailPanel/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_detailScrollRect;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailWeightTitleText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/QualityGroup/SSR/WeightText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailSSRWeightText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/QualityGroup/SR/WeightText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailSRWeightText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/QualityGroup/R/WeightText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailRWeightText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/InfoGroup/Title/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailScrollViewContentTitleText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/InfoGroup/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailScrollViewContent;
    [AutoBind("/Prefab/PoolItemWithRank", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_poolItemWithRankPrefab;
    private Button m_weiBoShareOneButton;
    private Button m_weChatShareOneButton;
    private Button m_weiBoShareTenButton;
    private Button m_weChatShareTenButton;
    private Text m_playerNameText;
    private Text m_playerLvText;
    private Text m_serverNameText;
    private Text m_heroNameText;
    private int m_nToShareHeroId;
    private int m_cardPoolId;
    private CardPool m_cardPool;
    private bool m_isClockHold;
    private bool m_isSingleSelect;
    private bool m_isContinueButtonClick;
    private List<Goods> m_selectRewards;
    private const string PoolItemNameText = "NameText";
    private const int NormalCardId = 4000;
    private const int CrystalCardId = 4001;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private HeroCharUIController m_heroCharUIController;
    private SelectCardBackground3DController m_selectCardBackground3DController;
    private ShareTenSelectCardUIController m_shareTenSelectCardUIController;
    private bool m_hasShowClockEffect;
    private List<SummonToggleUIController> m_toggleCtrlList;
    private List<Goods> m_extraRewardsCache;
    [DoNotToLua]
    private SelectCardUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateViewInSelectCardInt32_hotfix;
    private LuaFunction m_UpdateSharePlayerInfo_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_SetShareTenSelectCardUIControllerShareTenSelectCardUIController_hotfix;
    private LuaFunction m_ShowSelectCardRewardsList`1List`1SelectCardBackground3DController_hotfix;
    private LuaFunction m_PlayRewardsEffectList`1_hotfix;
    private LuaFunction m_PlaySingleRewardEffectGoodsBooleanInt32Int32Int32_hotfix;
    private LuaFunction m_StopClockEffectSoundOnSkipButtonClick_hotfix;
    private LuaFunction m_AfterShowClockEffectFinishedGoodsInt32Int32_hotfix;
    private LuaFunction m_OnClockButtonDragGameObject_hotfix;
    private LuaFunction m_OnContinueButtonClick_hotfix;
    private LuaFunction m_PlayTotalRewardsEffect_hotfix;
    private LuaFunction m_SetAfterSelectCurrencyPanel_hotfix;
    private LuaFunction m_SetSingleSelectRewardInfoGoodsInt32Int32_hotfix;
    private LuaFunction m_SetTenSelectRewardsInfoList`1_hotfix;
    private LuaFunction m_CreateCardPoolTabs_hotfix;
    private LuaFunction m_SummonToggleCtrl_EventOnToggleClickSummonToggleUIController_hotfix;
    private LuaFunction m_SetValuesAfterToggleChanged_hotfix;
    private LuaFunction m_SetSelectButtonImageTextTextBoolean_hotfix;
    private LuaFunction m_OnActivityDetailButtonClick_hotfix;
    private LuaFunction m_OnSingleSelectButtonClick_hotfix;
    private LuaFunction m_OnTenSelectButtonClick_hotfix;
    private LuaFunction m_OnSelectAgainButtonClick_hotfix;
    private LuaFunction m_CloseTenSelectPanelAction_hotfix;
    private LuaFunction m_ShowChangeCrystalToTicketPanel_hotfix;
    private LuaFunction m_ChangeCrystalToTicketPanelCancelButtonClick_hotfix;
    private LuaFunction m_CloseChangeCrystalToTicketPanel_hotfix;
    private LuaFunction m_ChangeCrystalToTicketPanelConfirmButtonClick_hotfix;
    private LuaFunction m_ShowCrystalAndTicketNotEnoughPanel_hotfix;
    private LuaFunction m_CrystalAndTicketNotEnoughPanelCancelButtonClick_hotfix;
    private LuaFunction m_CloseCrystalAndTicketNotEnoughPanel_hotfix;
    private LuaFunction m_CrystalAndTicketNotEnoughPanelConfirmButtonClick_hotfix;
    private LuaFunction m_OnBuyDiamondButtonClick_hotfix;
    private LuaFunction m_OnAddCrystalBtnClick_hotfix;
    private LuaFunction m_OnAddCrystal_hotfix;
    private LuaFunction m_SkipShowRewardEffect_hotfix;
    private LuaFunction m_Close3DUIEffect_hotfix;
    private LuaFunction m_CloseShowSelectRewardsPanel_hotfix;
    private LuaFunction m_IsShowSelectCardResult_hotfix;
    private LuaFunction m_ShowDetailScrollView_hotfix;
    private LuaFunction m_CreatePoolItemPrefabPoolContentItemData_hotfix;
    private LuaFunction m_OnDetailButtonClick_hotfix;
    private LuaFunction m_OnCloseDetailPanelButtonClick_hotfix;
    private LuaFunction m_OnArchiveButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnWeiBoShareOnceClick_hotfix;
    private LuaFunction m_OnWeChatShareOnceClick_hotfix;
    private LuaFunction m_OnWeiBoShareTenClick_hotfix;
    private LuaFunction m_OnWeChatShareTenClick_hotfix;
    private LuaFunction m_CaptureOnceFrameInt32_hotfix;
    private LuaFunction m_CaptureTenFrameInt32_hotfix;
    private LuaFunction m_add_EventOnWeiBoShareTenAction_hotfix;
    private LuaFunction m_remove_EventOnWeiBoShareTenAction_hotfix;
    private LuaFunction m_add_EventOnWeChatShareTenAction_hotfix;
    private LuaFunction m_remove_EventOnWeChatShareTenAction_hotfix;
    private LuaFunction m_add_EventOnArchiveAction_hotfix;
    private LuaFunction m_remove_EventOnArchiveAction_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnSelectCardAction`2_hotfix;
    private LuaFunction m_remove_EventOnSelectCardAction`2_hotfix;
    private LuaFunction m_add_EventOnShowSelectCardHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowSelectCardHelpAction_hotfix;
    private LuaFunction m_add_EventOnShowActivityDetailAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowActivityDetailAction`1_hotfix;
    private LuaFunction m_add_EventOnBagCapcityNotEnoughAction_hotfix;
    private LuaFunction m_remove_EventOnBagCapcityNotEnoughAction_hotfix;
    private LuaFunction m_add_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_remove_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_add_EventOnAddCrystalAction_hotfix;
    private LuaFunction m_remove_EventOnAddCrystalAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInSelectCard(int cardPoolId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSharePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetShareTenSelectCardUIController(ShareTenSelectCardUIController controller)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectCardRewards(
      List<Goods> rewards,
      List<Goods> extraRewards,
      SelectCardBackground3DController selectCardBackground3DController)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayRewardsEffect(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlaySingleRewardEffect(
      Goods goods,
      bool isNeedClockEffect,
      int rank,
      int maxRank,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopClockEffectSoundOnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AfterShowClockEffectFinished(Goods goods, int index, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClockButtonDrag(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayTotalRewardsEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAfterSelectCurrencyPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSingleSelectRewardInfo(Goods goods, int index, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTenSelectRewardsInfo(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCardPoolTabs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SummonToggleCtrl_EventOnToggleClick(SummonToggleUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetValuesAfterToggleChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSelectButton(Image iconImage, Text titleText, Text numText, bool isSingle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActivityDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSingleSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTenSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectAgainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseTenSelectPanel(Action action = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowChangeCrystalToTicketPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeCrystalToTicketPanelCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseChangeCrystalToTicketPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeCrystalToTicketPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCrystalAndTicketNotEnoughPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CrystalAndTicketNotEnoughPanelCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseCrystalAndTicketNotEnoughPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CrystalAndTicketNotEnoughPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyDiamondButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddCrystalBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkipShowRewardEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Close3DUIEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseShowSelectRewardsPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowSelectCardResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDetailScrollView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreatePoolItemPrefab(PoolContentItemData item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseDetailPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArchiveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureOnceFrame(int sharePlatform)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureTenFrame(int sharePlatform)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnWeiBoShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnWeChatShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnArchive
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool> EventOnSelectCard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSelectCardHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CardPool> EventOnShowActivityDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBagCapcityNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public SelectCardUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWeiBoShareTen()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWeiBoShareTen()
    {
      this.EventOnWeiBoShareTen = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWeChatShareTen()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWeChatShareTen()
    {
      this.EventOnWeChatShareTen = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnArchive()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnArchive()
    {
      this.EventOnArchive = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectCard(int arg1, bool arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectCard(int arg1, bool arg2)
    {
      this.EventOnSelectCard = (Action<int, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowSelectCardHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowSelectCardHelp()
    {
      this.EventOnShowSelectCardHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowActivityDetail(CardPool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowActivityDetail(CardPool obj)
    {
      this.EventOnShowActivityDetail = (Action<CardPool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBagCapcityNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBagCapcityNotEnough()
    {
      this.EventOnBagCapcityNotEnough = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCrystalNotEnough()
    {
      this.EventOnCrystalNotEnough = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddCrystal()
    {
      this.EventOnAddCrystal = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SelectCardUIController m_owner;

      public LuaExportHelper(SelectCardUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnWeiBoShareTen()
      {
        this.m_owner.__callDele_EventOnWeiBoShareTen();
      }

      public void __clearDele_EventOnWeiBoShareTen()
      {
        this.m_owner.__clearDele_EventOnWeiBoShareTen();
      }

      public void __callDele_EventOnWeChatShareTen()
      {
        this.m_owner.__callDele_EventOnWeChatShareTen();
      }

      public void __clearDele_EventOnWeChatShareTen()
      {
        this.m_owner.__clearDele_EventOnWeChatShareTen();
      }

      public void __callDele_EventOnArchive()
      {
        this.m_owner.__callDele_EventOnArchive();
      }

      public void __clearDele_EventOnArchive()
      {
        this.m_owner.__clearDele_EventOnArchive();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnSelectCard(int arg1, bool arg2)
      {
        this.m_owner.__callDele_EventOnSelectCard(arg1, arg2);
      }

      public void __clearDele_EventOnSelectCard(int arg1, bool arg2)
      {
        this.m_owner.__clearDele_EventOnSelectCard(arg1, arg2);
      }

      public void __callDele_EventOnShowSelectCardHelp()
      {
        this.m_owner.__callDele_EventOnShowSelectCardHelp();
      }

      public void __clearDele_EventOnShowSelectCardHelp()
      {
        this.m_owner.__clearDele_EventOnShowSelectCardHelp();
      }

      public void __callDele_EventOnShowActivityDetail(CardPool obj)
      {
        this.m_owner.__callDele_EventOnShowActivityDetail(obj);
      }

      public void __clearDele_EventOnShowActivityDetail(CardPool obj)
      {
        this.m_owner.__clearDele_EventOnShowActivityDetail(obj);
      }

      public void __callDele_EventOnBagCapcityNotEnough()
      {
        this.m_owner.__callDele_EventOnBagCapcityNotEnough();
      }

      public void __clearDele_EventOnBagCapcityNotEnough()
      {
        this.m_owner.__clearDele_EventOnBagCapcityNotEnough();
      }

      public void __callDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__callDele_EventOnCrystalNotEnough();
      }

      public void __clearDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__clearDele_EventOnCrystalNotEnough();
      }

      public void __callDele_EventOnAddCrystal()
      {
        this.m_owner.__callDele_EventOnAddCrystal();
      }

      public void __clearDele_EventOnAddCrystal()
      {
        this.m_owner.__clearDele_EventOnAddCrystal();
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public GameObject m_mainUIGo
      {
        get
        {
          return this.m_owner.m_mainUIGo;
        }
        set
        {
          this.m_owner.m_mainUIGo = value;
        }
      }

      public Text m_normalCardNumText
      {
        get
        {
          return this.m_owner.m_normalCardNumText;
        }
        set
        {
          this.m_owner.m_normalCardNumText = value;
        }
      }

      public Text m_diamondsCardNumText
      {
        get
        {
          return this.m_owner.m_diamondsCardNumText;
        }
        set
        {
          this.m_owner.m_diamondsCardNumText = value;
        }
      }

      public Button m_diamondsAddBtn
      {
        get
        {
          return this.m_owner.m_diamondsAddBtn;
        }
        set
        {
          this.m_owner.m_diamondsAddBtn = value;
        }
      }

      public Text m_diamondsNumText
      {
        get
        {
          return this.m_owner.m_diamondsNumText;
        }
        set
        {
          this.m_owner.m_diamondsNumText = value;
        }
      }

      public Image m_adsImage
      {
        get
        {
          return this.m_owner.m_adsImage;
        }
        set
        {
          this.m_owner.m_adsImage = value;
        }
      }

      public Text m_adsTimeText
      {
        get
        {
          return this.m_owner.m_adsTimeText;
        }
        set
        {
          this.m_owner.m_adsTimeText = value;
        }
      }

      public Button m_activityDetailButton
      {
        get
        {
          return this.m_owner.m_activityDetailButton;
        }
        set
        {
          this.m_owner.m_activityDetailButton = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public GameObject m_cardPoolToggleGroupGo
      {
        get
        {
          return this.m_owner.m_cardPoolToggleGroupGo;
        }
        set
        {
          this.m_owner.m_cardPoolToggleGroupGo = value;
        }
      }

      public Toggle m_ticketSummonToggle
      {
        get
        {
          return this.m_owner.m_ticketSummonToggle;
        }
        set
        {
          this.m_owner.m_ticketSummonToggle = value;
        }
      }

      public Toggle m_magicStoneSummonToggle
      {
        get
        {
          return this.m_owner.m_magicStoneSummonToggle;
        }
        set
        {
          this.m_owner.m_magicStoneSummonToggle = value;
        }
      }

      public Toggle m_equipSummonToggle
      {
        get
        {
          return this.m_owner.m_equipSummonToggle;
        }
        set
        {
          this.m_owner.m_equipSummonToggle = value;
        }
      }

      public Button m_singleSelectBtn
      {
        get
        {
          return this.m_owner.m_singleSelectBtn;
        }
        set
        {
          this.m_owner.m_singleSelectBtn = value;
        }
      }

      public Image m_singleSelectBtnIconImg
      {
        get
        {
          return this.m_owner.m_singleSelectBtnIconImg;
        }
        set
        {
          this.m_owner.m_singleSelectBtnIconImg = value;
        }
      }

      public Text m_singleSelectBtnNumText
      {
        get
        {
          return this.m_owner.m_singleSelectBtnNumText;
        }
        set
        {
          this.m_owner.m_singleSelectBtnNumText = value;
        }
      }

      public Text m_singleSelectBtnTitleText
      {
        get
        {
          return this.m_owner.m_singleSelectBtnTitleText;
        }
        set
        {
          this.m_owner.m_singleSelectBtnTitleText = value;
        }
      }

      public Button m_tenSelectBtn
      {
        get
        {
          return this.m_owner.m_tenSelectBtn;
        }
        set
        {
          this.m_owner.m_tenSelectBtn = value;
        }
      }

      public Image m_tenSelectBtnIconImg
      {
        get
        {
          return this.m_owner.m_tenSelectBtnIconImg;
        }
        set
        {
          this.m_owner.m_tenSelectBtnIconImg = value;
        }
      }

      public Text m_tenSelectBtnNumText
      {
        get
        {
          return this.m_owner.m_tenSelectBtnNumText;
        }
        set
        {
          this.m_owner.m_tenSelectBtnNumText = value;
        }
      }

      public Text m_tenSelectBtnTitleText
      {
        get
        {
          return this.m_owner.m_tenSelectBtnTitleText;
        }
        set
        {
          this.m_owner.m_tenSelectBtnTitleText = value;
        }
      }

      public GameObject m_tenSelectBtn3DEffect
      {
        get
        {
          return this.m_owner.m_tenSelectBtn3DEffect;
        }
        set
        {
          this.m_owner.m_tenSelectBtn3DEffect = value;
        }
      }

      public Button m_buyDiamondButton
      {
        get
        {
          return this.m_owner.m_buyDiamondButton;
        }
        set
        {
          this.m_owner.m_buyDiamondButton = value;
        }
      }

      public GameObject m_summonToggleItemPrefab
      {
        get
        {
          return this.m_owner.m_summonToggleItemPrefab;
        }
        set
        {
          this.m_owner.m_summonToggleItemPrefab = value;
        }
      }

      public GameObject m_afterSelectPanel
      {
        get
        {
          return this.m_owner.m_afterSelectPanel;
        }
        set
        {
          this.m_owner.m_afterSelectPanel = value;
        }
      }

      public GameObject m_singleSelectArrowButton
      {
        get
        {
          return this.m_owner.m_singleSelectArrowButton;
        }
        set
        {
          this.m_owner.m_singleSelectArrowButton = value;
        }
      }

      public GameObject m_singleSelectOpenNoticePanelGo
      {
        get
        {
          return this.m_owner.m_singleSelectOpenNoticePanelGo;
        }
        set
        {
          this.m_owner.m_singleSelectOpenNoticePanelGo = value;
        }
      }

      public GameObject m_singleSelectOpenNoticePanelImage
      {
        get
        {
          return this.m_owner.m_singleSelectOpenNoticePanelImage;
        }
        set
        {
          this.m_owner.m_singleSelectOpenNoticePanelImage = value;
        }
      }

      public GameObject m_singleSelectPanel
      {
        get
        {
          return this.m_owner.m_singleSelectPanel;
        }
        set
        {
          this.m_owner.m_singleSelectPanel = value;
        }
      }

      public GameObject m_singleHeroWordPanel
      {
        get
        {
          return this.m_owner.m_singleHeroWordPanel;
        }
        set
        {
          this.m_owner.m_singleHeroWordPanel = value;
        }
      }

      public Button m_singleSelectContinueImage
      {
        get
        {
          return this.m_owner.m_singleSelectContinueImage;
        }
        set
        {
          this.m_owner.m_singleSelectContinueImage = value;
        }
      }

      public GameObject m_singleSelectNewTextObj
      {
        get
        {
          return this.m_owner.m_singleSelectNewTextObj;
        }
        set
        {
          this.m_owner.m_singleSelectNewTextObj = value;
        }
      }

      public GameObject m_singleSelectGetTextObj
      {
        get
        {
          return this.m_owner.m_singleSelectGetTextObj;
        }
        set
        {
          this.m_owner.m_singleSelectGetTextObj = value;
        }
      }

      public GameObject m_singleSelectStarGroupObj
      {
        get
        {
          return this.m_owner.m_singleSelectStarGroupObj;
        }
        set
        {
          this.m_owner.m_singleSelectStarGroupObj = value;
        }
      }

      public Button m_selectSkipButton
      {
        get
        {
          return this.m_owner.m_selectSkipButton;
        }
        set
        {
          this.m_owner.m_selectSkipButton = value;
        }
      }

      public Image m_singleSelectItemImage
      {
        get
        {
          return this.m_owner.m_singleSelectItemImage;
        }
        set
        {
          this.m_owner.m_singleSelectItemImage = value;
        }
      }

      public Image m_singleSelectFragmentIconImg
      {
        get
        {
          return this.m_owner.m_singleSelectFragmentIconImg;
        }
        set
        {
          this.m_owner.m_singleSelectFragmentIconImg = value;
        }
      }

      public Text m_singleSelectFragmentCount
      {
        get
        {
          return this.m_owner.m_singleSelectFragmentCount;
        }
        set
        {
          this.m_owner.m_singleSelectFragmentCount = value;
        }
      }

      public Image m_singleSelectFragmentSSRImg
      {
        get
        {
          return this.m_owner.m_singleSelectFragmentSSRImg;
        }
        set
        {
          this.m_owner.m_singleSelectFragmentSSRImg = value;
        }
      }

      public Image m_singleSelectFragmentSRImg
      {
        get
        {
          return this.m_owner.m_singleSelectFragmentSRImg;
        }
        set
        {
          this.m_owner.m_singleSelectFragmentSRImg = value;
        }
      }

      public GameObject m_singleShareButtonDummy
      {
        get
        {
          return this.m_owner.m_singleShareButtonDummy;
        }
        set
        {
          this.m_owner.m_singleShareButtonDummy = value;
        }
      }

      public GameObject m_sharePhotoDummy
      {
        get
        {
          return this.m_owner.m_sharePhotoDummy;
        }
        set
        {
          this.m_owner.m_sharePhotoDummy = value;
        }
      }

      public GameObject m_tenSelectPanel
      {
        get
        {
          return this.m_owner.m_tenSelectPanel;
        }
        set
        {
          this.m_owner.m_tenSelectPanel = value;
        }
      }

      public GameObject m_tenSelectContentObj
      {
        get
        {
          return this.m_owner.m_tenSelectContentObj;
        }
        set
        {
          this.m_owner.m_tenSelectContentObj = value;
        }
      }

      public Button m_afterSelectConfirmButton
      {
        get
        {
          return this.m_owner.m_afterSelectConfirmButton;
        }
        set
        {
          this.m_owner.m_afterSelectConfirmButton = value;
        }
      }

      public Button m_selectAgainButton
      {
        get
        {
          return this.m_owner.m_selectAgainButton;
        }
        set
        {
          this.m_owner.m_selectAgainButton = value;
        }
      }

      public GameObject m_selectAgainButtonSingleBgObj
      {
        get
        {
          return this.m_owner.m_selectAgainButtonSingleBgObj;
        }
        set
        {
          this.m_owner.m_selectAgainButtonSingleBgObj = value;
        }
      }

      public GameObject m_selectAgainButtonTenBgObj
      {
        get
        {
          return this.m_owner.m_selectAgainButtonTenBgObj;
        }
        set
        {
          this.m_owner.m_selectAgainButtonTenBgObj = value;
        }
      }

      public Image m_selectAgainBtnIconImg
      {
        get
        {
          return this.m_owner.m_selectAgainBtnIconImg;
        }
        set
        {
          this.m_owner.m_selectAgainBtnIconImg = value;
        }
      }

      public Text m_selectAgainBtnNumText
      {
        get
        {
          return this.m_owner.m_selectAgainBtnNumText;
        }
        set
        {
          this.m_owner.m_selectAgainBtnNumText = value;
        }
      }

      public Text m_selectAgainBtnTitleText
      {
        get
        {
          return this.m_owner.m_selectAgainBtnTitleText;
        }
        set
        {
          this.m_owner.m_selectAgainBtnTitleText = value;
        }
      }

      public GameObject m_tenShareButtonDummy
      {
        get
        {
          return this.m_owner.m_tenShareButtonDummy;
        }
        set
        {
          this.m_owner.m_tenShareButtonDummy = value;
        }
      }

      public Button m_diamondsAddBtn2
      {
        get
        {
          return this.m_owner.m_diamondsAddBtn2;
        }
        set
        {
          this.m_owner.m_diamondsAddBtn2 = value;
        }
      }

      public GameObject m_changeCrystalToTicketPanel
      {
        get
        {
          return this.m_owner.m_changeCrystalToTicketPanel;
        }
        set
        {
          this.m_owner.m_changeCrystalToTicketPanel = value;
        }
      }

      public Text m_changeCrystalToTicketPanelTip
      {
        get
        {
          return this.m_owner.m_changeCrystalToTicketPanelTip;
        }
        set
        {
          this.m_owner.m_changeCrystalToTicketPanelTip = value;
        }
      }

      public Button m_changeCrystalToTicketPanelCancelButton
      {
        get
        {
          return this.m_owner.m_changeCrystalToTicketPanelCancelButton;
        }
        set
        {
          this.m_owner.m_changeCrystalToTicketPanelCancelButton = value;
        }
      }

      public Button m_changeCrystalToTicketPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_changeCrystalToTicketPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_changeCrystalToTicketPanelConfirmButton = value;
        }
      }

      public GameObject m_crystalAndTicketNotEnoughPanel
      {
        get
        {
          return this.m_owner.m_crystalAndTicketNotEnoughPanel;
        }
        set
        {
          this.m_owner.m_crystalAndTicketNotEnoughPanel = value;
        }
      }

      public Text m_crystalAndTicketNotEnoughPanelTip
      {
        get
        {
          return this.m_owner.m_crystalAndTicketNotEnoughPanelTip;
        }
        set
        {
          this.m_owner.m_crystalAndTicketNotEnoughPanelTip = value;
        }
      }

      public Button m_crystalAndTicketNotEnoughPanelCancelButton
      {
        get
        {
          return this.m_owner.m_crystalAndTicketNotEnoughPanelCancelButton;
        }
        set
        {
          this.m_owner.m_crystalAndTicketNotEnoughPanelCancelButton = value;
        }
      }

      public Button m_crystalAndTicketNotEnoughPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_crystalAndTicketNotEnoughPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_crystalAndTicketNotEnoughPanelConfirmButton = value;
        }
      }

      public Text m_afterSelectPanelNormalCardNumText
      {
        get
        {
          return this.m_owner.m_afterSelectPanelNormalCardNumText;
        }
        set
        {
          this.m_owner.m_afterSelectPanelNormalCardNumText = value;
        }
      }

      public Text m_afterSelectPanelDiamondsCardNumText
      {
        get
        {
          return this.m_owner.m_afterSelectPanelDiamondsCardNumText;
        }
        set
        {
          this.m_owner.m_afterSelectPanelDiamondsCardNumText = value;
        }
      }

      public Text m_afterSelectPanelDiamondsNumText
      {
        get
        {
          return this.m_owner.m_afterSelectPanelDiamondsNumText;
        }
        set
        {
          this.m_owner.m_afterSelectPanelDiamondsNumText = value;
        }
      }

      public Button m_archiveButton
      {
        get
        {
          return this.m_owner.m_archiveButton;
        }
        set
        {
          this.m_owner.m_archiveButton = value;
        }
      }

      public Button m_detailButton
      {
        get
        {
          return this.m_owner.m_detailButton;
        }
        set
        {
          this.m_owner.m_detailButton = value;
        }
      }

      public GameObject m_detailPanelObj
      {
        get
        {
          return this.m_owner.m_detailPanelObj;
        }
        set
        {
          this.m_owner.m_detailPanelObj = value;
        }
      }

      public CommonUIStateController m_detailPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_detailPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_detailPanelStateCtrl = value;
        }
      }

      public Button m_detailCloseButton
      {
        get
        {
          return this.m_owner.m_detailCloseButton;
        }
        set
        {
          this.m_owner.m_detailCloseButton = value;
        }
      }

      public Button m_detailReturnBgButton
      {
        get
        {
          return this.m_owner.m_detailReturnBgButton;
        }
        set
        {
          this.m_owner.m_detailReturnBgButton = value;
        }
      }

      public ScrollRect m_detailScrollRect
      {
        get
        {
          return this.m_owner.m_detailScrollRect;
        }
        set
        {
          this.m_owner.m_detailScrollRect = value;
        }
      }

      public Text m_detailWeightTitleText
      {
        get
        {
          return this.m_owner.m_detailWeightTitleText;
        }
        set
        {
          this.m_owner.m_detailWeightTitleText = value;
        }
      }

      public Text m_detailSSRWeightText
      {
        get
        {
          return this.m_owner.m_detailSSRWeightText;
        }
        set
        {
          this.m_owner.m_detailSSRWeightText = value;
        }
      }

      public Text m_detailSRWeightText
      {
        get
        {
          return this.m_owner.m_detailSRWeightText;
        }
        set
        {
          this.m_owner.m_detailSRWeightText = value;
        }
      }

      public Text m_detailRWeightText
      {
        get
        {
          return this.m_owner.m_detailRWeightText;
        }
        set
        {
          this.m_owner.m_detailRWeightText = value;
        }
      }

      public Text m_detailScrollViewContentTitleText
      {
        get
        {
          return this.m_owner.m_detailScrollViewContentTitleText;
        }
        set
        {
          this.m_owner.m_detailScrollViewContentTitleText = value;
        }
      }

      public GameObject m_detailScrollViewContent
      {
        get
        {
          return this.m_owner.m_detailScrollViewContent;
        }
        set
        {
          this.m_owner.m_detailScrollViewContent = value;
        }
      }

      public GameObject m_poolItemWithRankPrefab
      {
        get
        {
          return this.m_owner.m_poolItemWithRankPrefab;
        }
        set
        {
          this.m_owner.m_poolItemWithRankPrefab = value;
        }
      }

      public Button m_weiBoShareOneButton
      {
        get
        {
          return this.m_owner.m_weiBoShareOneButton;
        }
        set
        {
          this.m_owner.m_weiBoShareOneButton = value;
        }
      }

      public Button m_weChatShareOneButton
      {
        get
        {
          return this.m_owner.m_weChatShareOneButton;
        }
        set
        {
          this.m_owner.m_weChatShareOneButton = value;
        }
      }

      public Button m_weiBoShareTenButton
      {
        get
        {
          return this.m_owner.m_weiBoShareTenButton;
        }
        set
        {
          this.m_owner.m_weiBoShareTenButton = value;
        }
      }

      public Button m_weChatShareTenButton
      {
        get
        {
          return this.m_owner.m_weChatShareTenButton;
        }
        set
        {
          this.m_owner.m_weChatShareTenButton = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Text m_playerLvText
      {
        get
        {
          return this.m_owner.m_playerLvText;
        }
        set
        {
          this.m_owner.m_playerLvText = value;
        }
      }

      public Text m_serverNameText
      {
        get
        {
          return this.m_owner.m_serverNameText;
        }
        set
        {
          this.m_owner.m_serverNameText = value;
        }
      }

      public Text m_heroNameText
      {
        get
        {
          return this.m_owner.m_heroNameText;
        }
        set
        {
          this.m_owner.m_heroNameText = value;
        }
      }

      public int m_nToShareHeroId
      {
        get
        {
          return this.m_owner.m_nToShareHeroId;
        }
        set
        {
          this.m_owner.m_nToShareHeroId = value;
        }
      }

      public int m_cardPoolId
      {
        get
        {
          return this.m_owner.m_cardPoolId;
        }
        set
        {
          this.m_owner.m_cardPoolId = value;
        }
      }

      public CardPool m_cardPool
      {
        get
        {
          return this.m_owner.m_cardPool;
        }
        set
        {
          this.m_owner.m_cardPool = value;
        }
      }

      public bool m_isClockHold
      {
        get
        {
          return this.m_owner.m_isClockHold;
        }
        set
        {
          this.m_owner.m_isClockHold = value;
        }
      }

      public bool m_isSingleSelect
      {
        get
        {
          return this.m_owner.m_isSingleSelect;
        }
        set
        {
          this.m_owner.m_isSingleSelect = value;
        }
      }

      public bool m_isContinueButtonClick
      {
        get
        {
          return this.m_owner.m_isContinueButtonClick;
        }
        set
        {
          this.m_owner.m_isContinueButtonClick = value;
        }
      }

      public List<Goods> m_selectRewards
      {
        get
        {
          return this.m_owner.m_selectRewards;
        }
        set
        {
          this.m_owner.m_selectRewards = value;
        }
      }

      public static string PoolItemNameText
      {
        get
        {
          return "NameText";
        }
      }

      public static int NormalCardId
      {
        get
        {
          return 4000;
        }
      }

      public static int CrystalCardId
      {
        get
        {
          return 4001;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public SelectCardBackground3DController m_selectCardBackground3DController
      {
        get
        {
          return this.m_owner.m_selectCardBackground3DController;
        }
        set
        {
          this.m_owner.m_selectCardBackground3DController = value;
        }
      }

      public ShareTenSelectCardUIController m_shareTenSelectCardUIController
      {
        get
        {
          return this.m_owner.m_shareTenSelectCardUIController;
        }
        set
        {
          this.m_owner.m_shareTenSelectCardUIController = value;
        }
      }

      public bool m_hasShowClockEffect
      {
        get
        {
          return this.m_owner.m_hasShowClockEffect;
        }
        set
        {
          this.m_owner.m_hasShowClockEffect = value;
        }
      }

      public List<SummonToggleUIController> m_toggleCtrlList
      {
        get
        {
          return this.m_owner.m_toggleCtrlList;
        }
        set
        {
          this.m_owner.m_toggleCtrlList = value;
        }
      }

      public List<Goods> m_extraRewardsCache
      {
        get
        {
          return this.m_owner.m_extraRewardsCache;
        }
        set
        {
          this.m_owner.m_extraRewardsCache = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public IEnumerator PlayRewardsEffect(List<Goods> rewards)
      {
        return this.m_owner.PlayRewardsEffect(rewards);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator PlaySingleRewardEffect(
        Goods goods,
        bool isNeedClockEffect,
        int rank,
        int maxRank,
        int index)
      {
        // ISSUE: unable to decompile the method.
      }

      public void StopClockEffectSoundOnSkipButtonClick()
      {
        this.m_owner.StopClockEffectSoundOnSkipButtonClick();
      }

      public IEnumerator AfterShowClockEffectFinished(Goods goods, int index, int rank)
      {
        return this.m_owner.AfterShowClockEffectFinished(goods, index, rank);
      }

      public void OnClockButtonDrag(GameObject go)
      {
        this.m_owner.OnClockButtonDrag(go);
      }

      public void OnContinueButtonClick()
      {
        this.m_owner.OnContinueButtonClick();
      }

      public IEnumerator PlayTotalRewardsEffect()
      {
        return this.m_owner.PlayTotalRewardsEffect();
      }

      public void SetSingleSelectRewardInfo(Goods goods, int index, int rank)
      {
        this.m_owner.SetSingleSelectRewardInfo(goods, index, rank);
      }

      public void SetTenSelectRewardsInfo(List<Goods> goodsList)
      {
        this.m_owner.SetTenSelectRewardsInfo(goodsList);
      }

      public void CreateCardPoolTabs()
      {
        this.m_owner.CreateCardPoolTabs();
      }

      public void SummonToggleCtrl_EventOnToggleClick(SummonToggleUIController ctrl)
      {
        this.m_owner.SummonToggleCtrl_EventOnToggleClick(ctrl);
      }

      public void SetValuesAfterToggleChanged()
      {
        this.m_owner.SetValuesAfterToggleChanged();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetSelectButton(Image iconImage, Text titleText, Text numText, bool isSingle)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnActivityDetailButtonClick()
      {
        this.m_owner.OnActivityDetailButtonClick();
      }

      public void OnSingleSelectButtonClick()
      {
        this.m_owner.OnSingleSelectButtonClick();
      }

      public void OnTenSelectButtonClick()
      {
        this.m_owner.OnTenSelectButtonClick();
      }

      public void OnSelectAgainButtonClick()
      {
        this.m_owner.OnSelectAgainButtonClick();
      }

      public void ShowChangeCrystalToTicketPanel()
      {
        this.m_owner.ShowChangeCrystalToTicketPanel();
      }

      public void ChangeCrystalToTicketPanelCancelButtonClick()
      {
        this.m_owner.ChangeCrystalToTicketPanelCancelButtonClick();
      }

      public void CloseChangeCrystalToTicketPanel()
      {
        this.m_owner.CloseChangeCrystalToTicketPanel();
      }

      public void ChangeCrystalToTicketPanelConfirmButtonClick()
      {
        this.m_owner.ChangeCrystalToTicketPanelConfirmButtonClick();
      }

      public void ShowCrystalAndTicketNotEnoughPanel()
      {
        this.m_owner.ShowCrystalAndTicketNotEnoughPanel();
      }

      public void CrystalAndTicketNotEnoughPanelCancelButtonClick()
      {
        this.m_owner.CrystalAndTicketNotEnoughPanelCancelButtonClick();
      }

      public void CloseCrystalAndTicketNotEnoughPanel()
      {
        this.m_owner.CloseCrystalAndTicketNotEnoughPanel();
      }

      public void CrystalAndTicketNotEnoughPanelConfirmButtonClick()
      {
        this.m_owner.CrystalAndTicketNotEnoughPanelConfirmButtonClick();
      }

      public void OnBuyDiamondButtonClick()
      {
        this.m_owner.OnBuyDiamondButtonClick();
      }

      public void OnAddCrystalBtnClick()
      {
        this.m_owner.OnAddCrystalBtnClick();
      }

      public void OnAddCrystal()
      {
        this.m_owner.OnAddCrystal();
      }

      public void SkipShowRewardEffect()
      {
        this.m_owner.SkipShowRewardEffect();
      }

      public void Close3DUIEffect()
      {
        this.m_owner.Close3DUIEffect();
      }

      public void CloseShowSelectRewardsPanel()
      {
        this.m_owner.CloseShowSelectRewardsPanel();
      }

      public void ShowDetailScrollView()
      {
        this.m_owner.ShowDetailScrollView();
      }

      public void CreatePoolItemPrefab(PoolContentItemData item)
      {
        this.m_owner.CreatePoolItemPrefab(item);
      }

      public void OnDetailButtonClick()
      {
        this.m_owner.OnDetailButtonClick();
      }

      public void OnCloseDetailPanelButtonClick()
      {
        this.m_owner.OnCloseDetailPanelButtonClick();
      }

      public void OnArchiveButtonClick()
      {
        this.m_owner.OnArchiveButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnWeiBoShareOnceClick()
      {
        this.m_owner.OnWeiBoShareOnceClick();
      }

      public void OnWeChatShareOnceClick()
      {
        this.m_owner.OnWeChatShareOnceClick();
      }

      public void OnWeiBoShareTenClick()
      {
        this.m_owner.OnWeiBoShareTenClick();
      }

      public void OnWeChatShareTenClick()
      {
        this.m_owner.OnWeChatShareTenClick();
      }

      public IEnumerator CaptureOnceFrame(int sharePlatform)
      {
        return this.m_owner.CaptureOnceFrame(sharePlatform);
      }

      public IEnumerator CaptureTenFrame(int sharePlatform)
      {
        return this.m_owner.CaptureTenFrame(sharePlatform);
      }
    }
  }
}
