﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RankingUIController : UIControllerBase
  {
    protected bool m_isDuringUpdate;
    protected bool m_isSubMenuPanelPowerShow;
    protected bool m_isSubMenuPanelRiftShow;
    protected bool m_isSubMenuPanelUnchartedShow;
    protected bool m_isSubMenuPanelPeakArenaShow;
    protected RankingUIController.MainMenuSelectState m_mainMenuSelectState;
    protected RankingListType m_currRankingType;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ReturnButton;
    [AutoBind("./SubMenumGroup/NoneSubMenuArea", AutoBindAttribute.InitState.NotInit, false)]
    public RankingSubMenuUIController SubMenuUICtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Power", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuPowerButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Power", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuPowerStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/TopHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuTopHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/TopHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuTopHeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/Top15Hero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuTop15HeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/Top15Hero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuTop15HeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/AllHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuAllHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/AllHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuAllHeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/ChampionHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuChampionHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/ChampionHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuChampionHeroStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Rift", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuRiftButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Rift", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuRiftStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftChapterStar", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuRiftChapterStarButton;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftChapterStar", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuRiftChapterStarStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftAchievement", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuRiftAchievementButton;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftAchievement", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuRiftAchievementStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Uncharted", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuUnchartedButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Uncharted", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuUnchartedStateCtrl;
    [AutoBind("./SubMenumGroup/UnchartedMenuList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuClimbTowerButton;
    [AutoBind("./SubMenumGroup/UnchartedMenuList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuClimbTowerStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Stage", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuStageButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Stage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuStageStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/PeakArena", AutoBindAttribute.InitState.Inactive, false)]
    public ButtonEx MainMenuPeakArenaButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/PeakArena", AutoBindAttribute.InitState.Inactive, false)]
    public CommonUIStateController MainMenuPeakArenaStateCtrl;
    [AutoBind("./SubMenumGroup/PeakArenaListList/BGImage/BGImage1/Content", AutoBindAttribute.InitState.NotInit, false)]
    public Transform SubMenuPeakArenaSubMemuTransform;
    [AutoBind("./SubMenumGroup/PeakArenaListList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SubMenuPeakArenaItemPrefab;
    [AutoBind("./SubMenumGroup/PowerMenulList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelPowerStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelRiftStateCtrl;
    [AutoBind("./SubMenumGroup/UnchartedMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelUnchartedStateCtrl;
    [AutoBind("./SubMenumGroup/StageMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelStageStateCtrl;
    [AutoBind("./SubMenumGroup/PeakArenaListList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelPeakArenaStateCtrl;
    private List<CommonUIStateController> PeakArenaSubMenuItemStateCtrlList;
    private int m_curSelectPeakArenaSeasonID;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingUICtrl(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected RankingUIController.MainMenuSelectState GetMainMenuSelectStateFromRankingType(
      RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetMainMenuSelectState(RankingUIController.MainMenuSelectState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetAllMainMenuUnselect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSubMenuSelectState(RankingListType rankType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetAllSubMenuToggleUnselect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowPowerSubMenuPanel(bool isshow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowRiftSubMenuPanel(bool isshow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowUnchartedSubMenuPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowPeakArenaSubMenuPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void HideAllSubMenuPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CreatePeakArenaSeasonSubItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNoneSubMenuAreaClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuPowerClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuRiftClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuUnchartedClick()
    {
    }

    protected void OnMainMenuStageClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMainMenuPeakArenaClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankingTypeMenuClick(RankingListType rankListType)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnSubMenuTop15HeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.TopFifteenHero);
    }

    protected void OnSubMenuTopHeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.TopHero);
    }

    protected void OnSubMenuAllHeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.AllHero);
    }

    protected void OnSubMenuChampionHeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.ChampionHero);
    }

    protected void OnSubMenuRiftChapterStarClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.RiftChapterStar);
    }

    protected void OnSubMenuRiftAchievementClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.RiftAchievement);
    }

    protected void OnSubMenuClimbTowerClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.TowerFloor);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuPeakArenaClick(int seasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RankingListType, int> EventOnRankingTypeMenuClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnResetRankingType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRetunButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum MainMenuSelectState
    {
      None,
      Power,
      Rift,
      Uncharted,
      Stage,
      PeakArena,
    }
  }
}
