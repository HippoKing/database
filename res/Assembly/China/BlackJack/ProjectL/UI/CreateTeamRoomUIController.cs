﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CreateTeamRoomUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class CreateTeamRoomUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/CreateButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createButton;
    [AutoBind("./Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Panel/AuthorityToggle/AllPeopleToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_authorityAllToggle;
    [AutoBind("./Panel/AuthorityToggle/FriendAndGuildToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_authorityFriendToggle;
    [AutoBind("./Panel/AuthorityToggle/NotpublicToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_authorityNonPublicToggle;
    [AutoBind("./Panel/Info/GameFunctionType/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_gameFunctionTypeScrollRect;
    [AutoBind("./Panel/Info/Location/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_locationScrollRect;
    [AutoBind("./Panel/Info/PlayerLevel/LeftScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_playerLevelMinScrollRect;
    [AutoBind("./Panel/Info/PlayerLevel/RightScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_playerLevelMaxScrollRect;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RoomInfoListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_roomInfoListItemPrefab;
    private ScrollSnapCenter m_gameFunctionTypeScrollSnapCenter;
    private ScrollSnapCenter m_locationScrollSnapCenter;
    private ScrollSnapCenter m_playerLevelMinScrollSnapCenter;
    private ScrollSnapCenter m_playerLevelMaxScrollSnapCenter;
    private List<TeamRoomInfoListItemUIController> m_gameFunctionTypeListItems;
    private List<TeamRoomInfoListItemUIController> m_locationListItems;
    private List<TeamRoomInfoListItemUIController> m_playerLevelMinListItems;
    private List<TeamRoomInfoListItemUIController> m_playerLevelMaxListItems;

    [MethodImpl((MethodImplOptions) 32768)]
    private CreateTeamRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenCollectionActivity(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCollectionActivityListItems(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItem(
      string name,
      GameFunctionType gameFunctionType,
      int chapterId,
      bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItems(GameFunctionType gameFunctionType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearLocationListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItem(string name, int id, bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayerLevelMinListItem(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayerLevelMinListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayerLevelMaxListItem(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayerLevelMaxListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetRoomInfoListItemIndexByValue(
      List<TeamRoomInfoListItemUIController> list,
      int value,
      int value2 = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameFunctionType(GameFunctionType gameFunctionType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameFunctionType GetGameFunctionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocation(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLocationId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerLevelMin(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPlayerLevelMin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerLevelMax(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPlayerLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomAuthority GetAuthority()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveTeamRoomSetting(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnBackgroundButtonClick()
    {
      this.OnCancelButtonClick();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GameFunctionTypeListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocationListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMinListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMaxListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GameFunctionTypeScrollSnapCenter_OnCenterItemChanged(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GameFunctionTypeScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocationScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMinScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMaxScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ScaleListItem(
      ScrollRect scrollRect,
      List<TeamRoomInfoListItemUIController> listItems)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TeamRoomSetting> EventOnCreateTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
