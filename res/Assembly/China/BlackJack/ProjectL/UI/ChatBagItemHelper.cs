﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatBagItemHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ChatBagItemHelper
  {
    private const string m_shareNameMark = "[{0}]";
    private static readonly Regex s_nameRegex;
    private static readonly StringBuilder s_TextBuilder;

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ChatTextToChatLinkText(
      string chatText,
      List<ChatBagItemMessage.ChatBagItemData> chatBagItemDataList,
      int linkCount)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetBagItemChatName(string name)
    {
      return string.Format("[{0}]", (object) name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetChatBagItemDataWithInstanceId(
      ulong instanceId,
      List<ChatBagItemMessage.ChatBagItemData> chatBagItemDataList,
      out ChatBagItemMessage.ChatBagItemData chatBagItemData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryGetBagItemNameChatLinkText(
      string name,
      string itemChatName,
      ChatBagItemMessage.ChatBagItemData item,
      out string linkName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static ChatBagItemHelper()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
