﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroListUIController : UIControllerBase
  {
    [AutoBind("./HeroListPanel/HeroListScrollView/Viewport/Content/UnlockedHeroContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewUnlockedHeroContent;
    [AutoBind("./HeroListPanel/HeroListScrollView/Viewport/Content/LockedHeroContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewLockedHeroContent;
    [AutoBind("./HeroListPanel/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scrollView;
    [AutoBind("./HeroInfoPanel/CVPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cvText;
    [AutoBind("./HeroInfoPanel/CommentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_commentButton;
    [AutoBind("./HeroInfoPanel/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    [AutoBind("./HeroInfoPanel/DetailButton/RedPointImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailButtonRedPointImage;
    [AutoBind("./HeroInfoPanel/SummonPanel/SummonButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_summonButton;
    [AutoBind("./HeroInfoPanel/SummonPanel/SummonButton/U_SummonButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_summonButtonPressEffect;
    [AutoBind("./HeroInfoPanel/SummonPanel/SummonButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_summonButtonReadyEffect;
    [AutoBind("./HeroListPanel/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterButton;
    [AutoBind("./HeroListPanel/AddHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addheroButton;
    [AutoBind("./HeroListPanel/SpeedUpHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_speedUpHeroButton;
    [AutoBind("./HeroListPanel/ResetHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_resetHeroButton;
    [AutoBind("./HeroListPanel/HeroIdInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_heroIdInputField;
    [AutoBind("./ComposeConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_composeConfirmPanel;
    [AutoBind("./ComposeConfirmPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_composeCostText;
    [AutoBind("./ComposeConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_composeCancelButton;
    [AutoBind("./ComposeConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_composeConfirmButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HeroInfoPanel/HeroName/NameTextCh", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameTextCh;
    [AutoBind("./HeroInfoPanel/HeroName/NameTextCh", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroNameStateCtrl;
    [AutoBind("./HeroInfoPanel/HeroName/NameTextEn", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameTextEn;
    [AutoBind("./HeroInfoPanel/HeroName/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroRankImage;
    [AutoBind("./HeroInfoPanel/HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./HeroInfoPanel/Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charWordGameObject;
    [AutoBind("./HeroInfoPanel/Char/U_Summonout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charSummonEffect;
    [AutoBind("./HeroInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoPanel;
    [AutoBind("./HeroInfoPanel/SkinChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroCharSkinChangeButton;
    [AutoBind("./HeroListPanel/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroJobUpButton;
    [AutoBind("./HeroInfoPanel/Equipments", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroEquipmentsButton;
    [AutoBind("./HeroInfoPanel/Equipments/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroEquipmentsContent;
    [AutoBind("./HeroInfoPanel/Equipments/RedPointImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroEquipmentsRedPointImage;
    [AutoBind("./HeroInfoPanel/PowerValue", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_powerValueStateCtrl;
    [AutoBind("./HeroInfoPanel/PowerValue/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_powerValueText;
    [AutoBind("./HeroInfoPanel/PowerValue/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_powerArrowGo;
    [AutoBind("./HeroInfoPanel/BreakPanel/BreakButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakButton;
    [AutoBind("./HeroInfoPanel/BreakPanel/BreakButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakButtonReadyEffect;
    [AutoBind("./HeroInfoPanel/BreakPanel/BreakButton/U_BreakButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakButtonPressEffect;
    [AutoBind("./HeroInfoPanel/BreakPanel/Fragment/AddImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakAddBtn;
    [AutoBind("./HeroInfoPanel/BreakPanel/Fragment/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakNumText;
    [AutoBind("./HeroInfoPanel/BreakPanel/Fragment/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakIconImage;
    [AutoBind("./HeroInfoPanel/BreakPanel/ExtractionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_extractionButton;
    [AutoBind("./HeroInfoPanel/BreakPanel/ExtractionTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_extractionTipsStateCtrl;
    [AutoBind("./HeroListPanel/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortButton;
    [AutoBind("./HeroListPanel/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sortButtonStateCtrl;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sortTypesPanelStateCtrl;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/BGImages/ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortTypesReturnBgImage;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Level", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesLevelToggle;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Star", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesStarToggle;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Rarity", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesRarityToggle;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Power", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesPowerToggle;
    public const string LevelType = "Level";
    public const string StarLevelType = "StarLevel";
    public const string RankType = "Rank";
    public const string PowerType = "Power";
    public bool m_isUnLockHero;
    private int m_heroPowerValue;
    private int m_curHeroNeedFragmentCount;
    public int m_curSelectedHeroIndex;
    public GameObject m_curSeletedHeroList;
    private HeroCharUIController m_heroCharUIController;
    public List<Hero> m_unLockedHeroList;
    public List<Hero> m_lockedHeroList;
    public List<HeroItemUIController> m_unLockedHeroCtrlList;
    public List<HeroItemUIController> m_lockedHeroCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroListUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_UpdateViewInHeroListBoolean_hotfix;
    private LuaFunction m_UpdateHeroList_hotfix;
    private LuaFunction m_UpdateRightInfoPanel_hotfix;
    private LuaFunction m_SetHeroPowerValue_hotfix;
    private LuaFunction m_ResetHeroPowerValue_hotfix;
    private LuaFunction m_StopAllCoroutineInHeroList_hotfix;
    private LuaFunction m_SetHeroCharSkinPreviewStringInt32_hotfix;
    private LuaFunction m_Co_SetPowerValueSingleSingle_hotfix;
    private LuaFunction m_SetCurHeroListList`1List`1_hotfix;
    private LuaFunction m_SetToInitState_hotfix;
    private LuaFunction m_SetToAtLeftState_hotfix;
    private LuaFunction m_OnHeroItemClickHeroItemUIController_hotfix;
    private LuaFunction m_OnHeroFragmentSearchButtonClick_hotfix;
    private LuaFunction m_OnBreakButtonClick_hotfix;
    private LuaFunction m_PlayBreakButtonClickEffectInt32_hotfix;
    private LuaFunction m_ComposeLockedHeroHero_hotfix;
    private LuaFunction m_CloseComposeHeroPanel_hotfix;
    private LuaFunction m_ConfirmComposeHero_hotfix;
    private LuaFunction m_OnComposeHeroSucceed_hotfix;
    private LuaFunction m_PlayHeroGetEffect_hotfix;
    private LuaFunction m_OnExtractionButtonClick_hotfix;
    private LuaFunction m_OnEquipmentsButtonClick_hotfix;
    private LuaFunction m_OnHeroJobUpButtonClick_hotfix;
    private LuaFunction m_OnHeroCharSkinChangeButton_hotfix;
    private LuaFunction m_HeroCharUIController_OnClick_hotfix;
    private LuaFunction m_ClickHeroByIdForUserGuideInt32_hotfix;
    private LuaFunction m_ClickLowLevelHeroForUserGuide_hotfix;
    private LuaFunction m_ClickLockedHeroForUserGuide_hotfix;
    private LuaFunction m_GetHeroItemPosInListGameObjectList`1_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_SetSortTypeButtonAndToggles_hotfix;
    private LuaFunction m_ResetData_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_OnFilterButtonClick_hotfix;
    private LuaFunction m_ResetScrollViewPosition_hotfix;
    private LuaFunction m_OnDetailButtonClick_hotfix;
    private LuaFunction m_OnSummonButtonClick_hotfix;
    private LuaFunction m_PlaySummonButtonClickEffect_hotfix;
    private LuaFunction m_ShowButtonGameObjectBoolean_hotfix;
    private LuaFunction m_OnCommentButtonClick_hotfix;
    private LuaFunction m_OnAddHeroButtonClick_hotfix;
    private LuaFunction m_OnSpeedUpHeroButtonClick_hotfix;
    private LuaFunction m_OnResetHeroButtonClick_hotfix;
    private LuaFunction m_ResetPanelPos_hotfix;
    private LuaFunction m_InactiveCharWordPanel_hotfix;
    private LuaFunction m_GetCurHeroList_hotfix;
    private LuaFunction m_HaveHeroCanCompose_hotfix;
    private LuaFunction m_PlayHeroPerformanceInt32_hotfix;
    private LuaFunction m_OnLevelToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnStarLevelToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnRankToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnPowerToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnSortButtonClick_hotfix;
    private LuaFunction m_CloseSortTypesPanel_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnFilterAction_hotfix;
    private LuaFunction m_remove_EventOnFilterAction_hotfix;
    private LuaFunction m_add_EventOnDetailAction_hotfix;
    private LuaFunction m_remove_EventOnDetailAction_hotfix;
    private LuaFunction m_add_EventOnCommentAction_hotfix;
    private LuaFunction m_remove_EventOnCommentAction_hotfix;
    private LuaFunction m_add_EventOnEquipmentAction_hotfix;
    private LuaFunction m_remove_EventOnEquipmentAction_hotfix;
    private LuaFunction m_add_EventOnJobAction_hotfix;
    private LuaFunction m_remove_EventOnJobAction_hotfix;
    private LuaFunction m_add_EventOnHeroCharClickAction_hotfix;
    private LuaFunction m_remove_EventOnHeroCharClickAction_hotfix;
    private LuaFunction m_add_EventOnHeroBreakAction`1_hotfix;
    private LuaFunction m_remove_EventOnHeroBreakAction`1_hotfix;
    private LuaFunction m_add_EventOnAddHeroAction`1_hotfix;
    private LuaFunction m_remove_EventOnAddHeroAction`1_hotfix;
    private LuaFunction m_add_EventOnComposeHeroAction`2_hotfix;
    private LuaFunction m_remove_EventOnComposeHeroAction`2_hotfix;
    private LuaFunction m_add_EventOnSortToggleClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSortToggleClickAction`1_hotfix;
    private LuaFunction m_add_EventOnShowGetPathAction`3_hotfix;
    private LuaFunction m_remove_EventOnShowGetPathAction`3_hotfix;
    private LuaFunction m_add_EventOnInitHeroListAction`2_hotfix;
    private LuaFunction m_remove_EventOnInitHeroListAction`2_hotfix;
    private LuaFunction m_add_EventOnUpdateViewInListAndDetailAction`5_hotfix;
    private LuaFunction m_remove_EventOnUpdateViewInListAndDetailAction`5_hotfix;
    private LuaFunction m_add_EventOnHeroCharSkinChangeButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnHeroCharSkinChangeButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnGoToMemoryExtractionStoreAction_hotfix;
    private LuaFunction m_remove_EventOnGoToMemoryExtractionStoreAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroList(bool isNeedRebuildHeroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRightInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroPowerValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetHeroPowerValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopAllCoroutineInHeroList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCharSkinPreview(string spinePath, int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetPowerValue(float newValue, float oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurHeroList(List<Hero> unlockedList, List<Hero> lockedList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToInitState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToAtLeftState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick(HeroItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroFragmentSearchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayBreakButtonClickEffect(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComposeLockedHero(Hero h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseComposeHeroPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ConfirmComposeHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnComposeHeroSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayHeroGetEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExtractionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentsButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroCharSkinChangeButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharUIController_OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickHeroByIdForUserGuide(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickLowLevelHeroForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ClickLockedHeroForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetHeroItemPosInList(GameObject child, List<HeroItemUIController> parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSortTypeButtonAndToggles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSummonButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlaySummonButtonClickEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowButtonGameObject(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCommentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpeedUpHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnResetHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetPanelPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InactiveCharWordPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetCurHeroList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveHeroCanCompose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarLevelToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPowerToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSortTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFilter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnComment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEquipment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJob
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroCharClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroBreak
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnComposeHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroListUIController.HeroSortType> EventOnSortToggleClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnShowGetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<Hero>, List<Hero>> EventOnInitHeroList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event HeroListUIController.Action<int, bool, bool, int, bool> EventOnUpdateViewInListAndDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroCharSkinChangeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGoToMemoryExtractionStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroListUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFilter()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFilter()
    {
      this.EventOnFilter = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDetail()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnDetail()
    {
      this.EventOnDetail = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnComment()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnComment()
    {
      this.EventOnComment = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEquipment()
    {
      this.EventOnEquipment = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnJob()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnJob()
    {
      this.EventOnJob = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroCharClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroCharClick()
    {
      this.EventOnHeroCharClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroBreak(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroBreak(int obj)
    {
      this.EventOnHeroBreak = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddHero(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddHero(string obj)
    {
      this.EventOnAddHero = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnComposeHero(int arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnComposeHero(int arg1, Action arg2)
    {
      this.EventOnComposeHero = (Action<int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSortToggleClick(HeroListUIController.HeroSortType obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSortToggleClick(HeroListUIController.HeroSortType obj)
    {
      this.EventOnSortToggleClick = (Action<HeroListUIController.HeroSortType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowGetPath(GoodsType arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowGetPath(GoodsType arg1, int arg2, int arg3)
    {
      this.EventOnShowGetPath = (Action<GoodsType, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnInitHeroList(List<Hero> arg1, List<Hero> arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnInitHeroList(List<Hero> arg1, List<Hero> arg2)
    {
      this.EventOnInitHeroList = (Action<List<Hero>, List<Hero>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUpdateViewInListAndDetail(
      int arg1,
      bool arg2,
      bool arg3,
      int arg4,
      bool arg5)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUpdateViewInListAndDetail(
      int arg1,
      bool arg2,
      bool arg3,
      int arg4,
      bool arg5)
    {
      this.EventOnUpdateViewInListAndDetail = (HeroListUIController.Action<int, bool, bool, int, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroCharSkinChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroCharSkinChangeButtonClick()
    {
      this.EventOnHeroCharSkinChangeButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoToMemoryExtractionStore()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGoToMemoryExtractionStore()
    {
      this.EventOnGoToMemoryExtractionStore = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public enum HeroSortType
    {
      Level,
      StarLevel,
      Rank,
      Power,
    }

    public class LuaExportHelper
    {
      private HeroListUIController m_owner;

      public LuaExportHelper(HeroListUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnFilter()
      {
        this.m_owner.__callDele_EventOnFilter();
      }

      public void __clearDele_EventOnFilter()
      {
        this.m_owner.__clearDele_EventOnFilter();
      }

      public void __callDele_EventOnDetail()
      {
        this.m_owner.__callDele_EventOnDetail();
      }

      public void __clearDele_EventOnDetail()
      {
        this.m_owner.__clearDele_EventOnDetail();
      }

      public void __callDele_EventOnComment()
      {
        this.m_owner.__callDele_EventOnComment();
      }

      public void __clearDele_EventOnComment()
      {
        this.m_owner.__clearDele_EventOnComment();
      }

      public void __callDele_EventOnEquipment()
      {
        this.m_owner.__callDele_EventOnEquipment();
      }

      public void __clearDele_EventOnEquipment()
      {
        this.m_owner.__clearDele_EventOnEquipment();
      }

      public void __callDele_EventOnJob()
      {
        this.m_owner.__callDele_EventOnJob();
      }

      public void __clearDele_EventOnJob()
      {
        this.m_owner.__clearDele_EventOnJob();
      }

      public void __callDele_EventOnHeroCharClick()
      {
        this.m_owner.__callDele_EventOnHeroCharClick();
      }

      public void __clearDele_EventOnHeroCharClick()
      {
        this.m_owner.__clearDele_EventOnHeroCharClick();
      }

      public void __callDele_EventOnHeroBreak(int obj)
      {
        this.m_owner.__callDele_EventOnHeroBreak(obj);
      }

      public void __clearDele_EventOnHeroBreak(int obj)
      {
        this.m_owner.__clearDele_EventOnHeroBreak(obj);
      }

      public void __callDele_EventOnAddHero(string obj)
      {
        this.m_owner.__callDele_EventOnAddHero(obj);
      }

      public void __clearDele_EventOnAddHero(string obj)
      {
        this.m_owner.__clearDele_EventOnAddHero(obj);
      }

      public void __callDele_EventOnComposeHero(int arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnComposeHero(arg1, arg2);
      }

      public void __clearDele_EventOnComposeHero(int arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnComposeHero(arg1, arg2);
      }

      public void __callDele_EventOnSortToggleClick(HeroListUIController.HeroSortType obj)
      {
        this.m_owner.__callDele_EventOnSortToggleClick(obj);
      }

      public void __clearDele_EventOnSortToggleClick(HeroListUIController.HeroSortType obj)
      {
        this.m_owner.__clearDele_EventOnSortToggleClick(obj);
      }

      public void __callDele_EventOnShowGetPath(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnShowGetPath(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnShowGetPath(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnShowGetPath(arg1, arg2, arg3);
      }

      public void __callDele_EventOnInitHeroList(List<Hero> arg1, List<Hero> arg2)
      {
        this.m_owner.__callDele_EventOnInitHeroList(arg1, arg2);
      }

      public void __clearDele_EventOnInitHeroList(List<Hero> arg1, List<Hero> arg2)
      {
        this.m_owner.__clearDele_EventOnInitHeroList(arg1, arg2);
      }

      public void __callDele_EventOnUpdateViewInListAndDetail(
        int arg1,
        bool arg2,
        bool arg3,
        int arg4,
        bool arg5)
      {
        this.m_owner.__callDele_EventOnUpdateViewInListAndDetail(arg1, arg2, arg3, arg4, arg5);
      }

      public void __clearDele_EventOnUpdateViewInListAndDetail(
        int arg1,
        bool arg2,
        bool arg3,
        int arg4,
        bool arg5)
      {
        this.m_owner.__clearDele_EventOnUpdateViewInListAndDetail(arg1, arg2, arg3, arg4, arg5);
      }

      public void __callDele_EventOnHeroCharSkinChangeButtonClick()
      {
        this.m_owner.__callDele_EventOnHeroCharSkinChangeButtonClick();
      }

      public void __clearDele_EventOnHeroCharSkinChangeButtonClick()
      {
        this.m_owner.__clearDele_EventOnHeroCharSkinChangeButtonClick();
      }

      public void __callDele_EventOnGoToMemoryExtractionStore()
      {
        this.m_owner.__callDele_EventOnGoToMemoryExtractionStore();
      }

      public void __clearDele_EventOnGoToMemoryExtractionStore()
      {
        this.m_owner.__clearDele_EventOnGoToMemoryExtractionStore();
      }

      public GameObject m_scrollViewUnlockedHeroContent
      {
        get
        {
          return this.m_owner.m_scrollViewUnlockedHeroContent;
        }
        set
        {
          this.m_owner.m_scrollViewUnlockedHeroContent = value;
        }
      }

      public GameObject m_scrollViewLockedHeroContent
      {
        get
        {
          return this.m_owner.m_scrollViewLockedHeroContent;
        }
        set
        {
          this.m_owner.m_scrollViewLockedHeroContent = value;
        }
      }

      public ScrollRect m_scrollView
      {
        get
        {
          return this.m_owner.m_scrollView;
        }
        set
        {
          this.m_owner.m_scrollView = value;
        }
      }

      public Text m_cvText
      {
        get
        {
          return this.m_owner.m_cvText;
        }
        set
        {
          this.m_owner.m_cvText = value;
        }
      }

      public Button m_commentButton
      {
        get
        {
          return this.m_owner.m_commentButton;
        }
        set
        {
          this.m_owner.m_commentButton = value;
        }
      }

      public Button m_detailButton
      {
        get
        {
          return this.m_owner.m_detailButton;
        }
        set
        {
          this.m_owner.m_detailButton = value;
        }
      }

      public GameObject m_detailButtonRedPointImage
      {
        get
        {
          return this.m_owner.m_detailButtonRedPointImage;
        }
        set
        {
          this.m_owner.m_detailButtonRedPointImage = value;
        }
      }

      public Button m_summonButton
      {
        get
        {
          return this.m_owner.m_summonButton;
        }
        set
        {
          this.m_owner.m_summonButton = value;
        }
      }

      public GameObject m_summonButtonPressEffect
      {
        get
        {
          return this.m_owner.m_summonButtonPressEffect;
        }
        set
        {
          this.m_owner.m_summonButtonPressEffect = value;
        }
      }

      public GameObject m_summonButtonReadyEffect
      {
        get
        {
          return this.m_owner.m_summonButtonReadyEffect;
        }
        set
        {
          this.m_owner.m_summonButtonReadyEffect = value;
        }
      }

      public Button m_filterButton
      {
        get
        {
          return this.m_owner.m_filterButton;
        }
        set
        {
          this.m_owner.m_filterButton = value;
        }
      }

      public Button m_addheroButton
      {
        get
        {
          return this.m_owner.m_addheroButton;
        }
        set
        {
          this.m_owner.m_addheroButton = value;
        }
      }

      public Button m_speedUpHeroButton
      {
        get
        {
          return this.m_owner.m_speedUpHeroButton;
        }
        set
        {
          this.m_owner.m_speedUpHeroButton = value;
        }
      }

      public Button m_resetHeroButton
      {
        get
        {
          return this.m_owner.m_resetHeroButton;
        }
        set
        {
          this.m_owner.m_resetHeroButton = value;
        }
      }

      public InputField m_heroIdInputField
      {
        get
        {
          return this.m_owner.m_heroIdInputField;
        }
        set
        {
          this.m_owner.m_heroIdInputField = value;
        }
      }

      public GameObject m_composeConfirmPanel
      {
        get
        {
          return this.m_owner.m_composeConfirmPanel;
        }
        set
        {
          this.m_owner.m_composeConfirmPanel = value;
        }
      }

      public Text m_composeCostText
      {
        get
        {
          return this.m_owner.m_composeCostText;
        }
        set
        {
          this.m_owner.m_composeCostText = value;
        }
      }

      public Button m_composeCancelButton
      {
        get
        {
          return this.m_owner.m_composeCancelButton;
        }
        set
        {
          this.m_owner.m_composeCancelButton = value;
        }
      }

      public Button m_composeConfirmButton
      {
        get
        {
          return this.m_owner.m_composeConfirmButton;
        }
        set
        {
          this.m_owner.m_composeConfirmButton = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Text m_heroNameTextCh
      {
        get
        {
          return this.m_owner.m_heroNameTextCh;
        }
        set
        {
          this.m_owner.m_heroNameTextCh = value;
        }
      }

      public CommonUIStateController m_heroNameStateCtrl
      {
        get
        {
          return this.m_owner.m_heroNameStateCtrl;
        }
        set
        {
          this.m_owner.m_heroNameStateCtrl = value;
        }
      }

      public Text m_heroNameTextEn
      {
        get
        {
          return this.m_owner.m_heroNameTextEn;
        }
        set
        {
          this.m_owner.m_heroNameTextEn = value;
        }
      }

      public Image m_heroRankImage
      {
        get
        {
          return this.m_owner.m_heroRankImage;
        }
        set
        {
          this.m_owner.m_heroRankImage = value;
        }
      }

      public GameObject m_heroStar
      {
        get
        {
          return this.m_owner.m_heroStar;
        }
        set
        {
          this.m_owner.m_heroStar = value;
        }
      }

      public GameObject m_charGameObjectRoot
      {
        get
        {
          return this.m_owner.m_charGameObjectRoot;
        }
        set
        {
          this.m_owner.m_charGameObjectRoot = value;
        }
      }

      public GameObject m_charWordGameObject
      {
        get
        {
          return this.m_owner.m_charWordGameObject;
        }
        set
        {
          this.m_owner.m_charWordGameObject = value;
        }
      }

      public GameObject m_charSummonEffect
      {
        get
        {
          return this.m_owner.m_charSummonEffect;
        }
        set
        {
          this.m_owner.m_charSummonEffect = value;
        }
      }

      public GameObject m_heroInfoPanel
      {
        get
        {
          return this.m_owner.m_heroInfoPanel;
        }
        set
        {
          this.m_owner.m_heroInfoPanel = value;
        }
      }

      public Button m_heroCharSkinChangeButton
      {
        get
        {
          return this.m_owner.m_heroCharSkinChangeButton;
        }
        set
        {
          this.m_owner.m_heroCharSkinChangeButton = value;
        }
      }

      public Button m_heroJobUpButton
      {
        get
        {
          return this.m_owner.m_heroJobUpButton;
        }
        set
        {
          this.m_owner.m_heroJobUpButton = value;
        }
      }

      public Button m_heroEquipmentsButton
      {
        get
        {
          return this.m_owner.m_heroEquipmentsButton;
        }
        set
        {
          this.m_owner.m_heroEquipmentsButton = value;
        }
      }

      public GameObject m_heroEquipmentsContent
      {
        get
        {
          return this.m_owner.m_heroEquipmentsContent;
        }
        set
        {
          this.m_owner.m_heroEquipmentsContent = value;
        }
      }

      public GameObject m_heroEquipmentsRedPointImage
      {
        get
        {
          return this.m_owner.m_heroEquipmentsRedPointImage;
        }
        set
        {
          this.m_owner.m_heroEquipmentsRedPointImage = value;
        }
      }

      public CommonUIStateController m_powerValueStateCtrl
      {
        get
        {
          return this.m_owner.m_powerValueStateCtrl;
        }
        set
        {
          this.m_owner.m_powerValueStateCtrl = value;
        }
      }

      public Text m_powerValueText
      {
        get
        {
          return this.m_owner.m_powerValueText;
        }
        set
        {
          this.m_owner.m_powerValueText = value;
        }
      }

      public GameObject m_powerArrowGo
      {
        get
        {
          return this.m_owner.m_powerArrowGo;
        }
        set
        {
          this.m_owner.m_powerArrowGo = value;
        }
      }

      public Button m_breakButton
      {
        get
        {
          return this.m_owner.m_breakButton;
        }
        set
        {
          this.m_owner.m_breakButton = value;
        }
      }

      public GameObject m_breakButtonReadyEffect
      {
        get
        {
          return this.m_owner.m_breakButtonReadyEffect;
        }
        set
        {
          this.m_owner.m_breakButtonReadyEffect = value;
        }
      }

      public GameObject m_breakButtonPressEffect
      {
        get
        {
          return this.m_owner.m_breakButtonPressEffect;
        }
        set
        {
          this.m_owner.m_breakButtonPressEffect = value;
        }
      }

      public Button m_breakAddBtn
      {
        get
        {
          return this.m_owner.m_breakAddBtn;
        }
        set
        {
          this.m_owner.m_breakAddBtn = value;
        }
      }

      public Text m_breakNumText
      {
        get
        {
          return this.m_owner.m_breakNumText;
        }
        set
        {
          this.m_owner.m_breakNumText = value;
        }
      }

      public Image m_breakIconImage
      {
        get
        {
          return this.m_owner.m_breakIconImage;
        }
        set
        {
          this.m_owner.m_breakIconImage = value;
        }
      }

      public Button m_extractionButton
      {
        get
        {
          return this.m_owner.m_extractionButton;
        }
        set
        {
          this.m_owner.m_extractionButton = value;
        }
      }

      public CommonUIStateController m_extractionTipsStateCtrl
      {
        get
        {
          return this.m_owner.m_extractionTipsStateCtrl;
        }
        set
        {
          this.m_owner.m_extractionTipsStateCtrl = value;
        }
      }

      public Button m_sortButton
      {
        get
        {
          return this.m_owner.m_sortButton;
        }
        set
        {
          this.m_owner.m_sortButton = value;
        }
      }

      public CommonUIStateController m_sortButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_sortButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_sortButtonStateCtrl = value;
        }
      }

      public CommonUIStateController m_sortTypesPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_sortTypesPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_sortTypesPanelStateCtrl = value;
        }
      }

      public Button m_sortTypesReturnBgImage
      {
        get
        {
          return this.m_owner.m_sortTypesReturnBgImage;
        }
        set
        {
          this.m_owner.m_sortTypesReturnBgImage = value;
        }
      }

      public Toggle m_sortTypesLevelToggle
      {
        get
        {
          return this.m_owner.m_sortTypesLevelToggle;
        }
        set
        {
          this.m_owner.m_sortTypesLevelToggle = value;
        }
      }

      public Toggle m_sortTypesStarToggle
      {
        get
        {
          return this.m_owner.m_sortTypesStarToggle;
        }
        set
        {
          this.m_owner.m_sortTypesStarToggle = value;
        }
      }

      public Toggle m_sortTypesRarityToggle
      {
        get
        {
          return this.m_owner.m_sortTypesRarityToggle;
        }
        set
        {
          this.m_owner.m_sortTypesRarityToggle = value;
        }
      }

      public Toggle m_sortTypesPowerToggle
      {
        get
        {
          return this.m_owner.m_sortTypesPowerToggle;
        }
        set
        {
          this.m_owner.m_sortTypesPowerToggle = value;
        }
      }

      public int m_heroPowerValue
      {
        get
        {
          return this.m_owner.m_heroPowerValue;
        }
        set
        {
          this.m_owner.m_heroPowerValue = value;
        }
      }

      public int m_curHeroNeedFragmentCount
      {
        get
        {
          return this.m_owner.m_curHeroNeedFragmentCount;
        }
        set
        {
          this.m_owner.m_curHeroNeedFragmentCount = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void UpdateHeroList()
      {
        this.m_owner.UpdateHeroList();
      }

      public void UpdateRightInfoPanel()
      {
        this.m_owner.UpdateRightInfoPanel();
      }

      public IEnumerator Co_SetPowerValue(float newValue, float oldValue)
      {
        return this.m_owner.Co_SetPowerValue(newValue, oldValue);
      }

      public void OnHeroItemClick(HeroItemUIController goCtrl)
      {
        this.m_owner.OnHeroItemClick(goCtrl);
      }

      public void OnHeroFragmentSearchButtonClick()
      {
        this.m_owner.OnHeroFragmentSearchButtonClick();
      }

      public void OnBreakButtonClick()
      {
        this.m_owner.OnBreakButtonClick();
      }

      public IEnumerator PlayBreakButtonClickEffect(int heroId)
      {
        return this.m_owner.PlayBreakButtonClickEffect(heroId);
      }

      public void ComposeLockedHero(Hero h)
      {
        this.m_owner.ComposeLockedHero(h);
      }

      public void CloseComposeHeroPanel()
      {
        this.m_owner.CloseComposeHeroPanel();
      }

      public void ConfirmComposeHero()
      {
        this.m_owner.ConfirmComposeHero();
      }

      public void OnComposeHeroSucceed()
      {
        this.m_owner.OnComposeHeroSucceed();
      }

      public IEnumerator PlayHeroGetEffect()
      {
        return this.m_owner.PlayHeroGetEffect();
      }

      public void OnExtractionButtonClick()
      {
        this.m_owner.OnExtractionButtonClick();
      }

      public void OnEquipmentsButtonClick()
      {
        this.m_owner.OnEquipmentsButtonClick();
      }

      public void OnHeroJobUpButtonClick()
      {
        this.m_owner.OnHeroJobUpButtonClick();
      }

      public void OnHeroCharSkinChangeButton()
      {
        this.m_owner.OnHeroCharSkinChangeButton();
      }

      public void HeroCharUIController_OnClick()
      {
        this.m_owner.HeroCharUIController_OnClick();
      }

      public int GetHeroItemPosInList(GameObject child, List<HeroItemUIController> parent)
      {
        return this.m_owner.GetHeroItemPosInList(child, parent);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void SetSortTypeButtonAndToggles()
      {
        this.m_owner.SetSortTypeButtonAndToggles();
      }

      public void ResetData()
      {
        this.m_owner.ResetData();
      }

      public void OnFilterButtonClick()
      {
        this.m_owner.OnFilterButtonClick();
      }

      public void ResetScrollViewPosition()
      {
        this.m_owner.ResetScrollViewPosition();
      }

      public void OnDetailButtonClick()
      {
        this.m_owner.OnDetailButtonClick();
      }

      public void OnSummonButtonClick()
      {
        this.m_owner.OnSummonButtonClick();
      }

      public IEnumerator PlaySummonButtonClickEffect()
      {
        return this.m_owner.PlaySummonButtonClickEffect();
      }

      public void OnCommentButtonClick()
      {
        this.m_owner.OnCommentButtonClick();
      }

      public void OnAddHeroButtonClick()
      {
        this.m_owner.OnAddHeroButtonClick();
      }

      public void OnSpeedUpHeroButtonClick()
      {
        this.m_owner.OnSpeedUpHeroButtonClick();
      }

      public void OnResetHeroButtonClick()
      {
        this.m_owner.OnResetHeroButtonClick();
      }

      public void OnLevelToggleValueChanged(bool isOn)
      {
        this.m_owner.OnLevelToggleValueChanged(isOn);
      }

      public void OnStarLevelToggleValueChanged(bool isOn)
      {
        this.m_owner.OnStarLevelToggleValueChanged(isOn);
      }

      public void OnRankToggleValueChanged(bool isOn)
      {
        this.m_owner.OnRankToggleValueChanged(isOn);
      }

      public void OnPowerToggleValueChanged(bool isOn)
      {
        this.m_owner.OnPowerToggleValueChanged(isOn);
      }

      public void OnSortButtonClick()
      {
        this.m_owner.OnSortButtonClick();
      }

      public void CloseSortTypesPanel()
      {
        this.m_owner.CloseSortTypesPanel();
      }
    }
  }
}
