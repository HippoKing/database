﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceRecordUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class VoiceRecordUIController : UIControllerBase
  {
    [AutoBind("./SoundMessage/Send/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text voiceTimeText;
    [AutoBind("./SoundMessage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController voiceRecordStateUICtrl;
    [AutoBind("./SoundMessage/Send/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image recordTimeProgressBar;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public VoiceRecordUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceCancelTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceShortTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateVoiceRecordTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
