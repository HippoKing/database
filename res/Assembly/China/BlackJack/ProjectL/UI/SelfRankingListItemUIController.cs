﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfRankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SelfRankingListItemUIController : RankingListItemUIController
  {
    [AutoBind("./ComparePanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController ComparePanelUIStateCtrl;
    [AutoBind("./ComparePanel/LastRankLevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text LastRankLevelText;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCompareInfo(int lastRankLevel, int curRankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableComparePanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
