﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UINetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class UINetTask : NetWorkTransactionTask
  {
    protected bool m_isDisableInput;

    [MethodImpl((MethodImplOptions) 32768)]
    public UINetTask(float timeout = 10f, UITaskBase blockedUITask = null, bool autoRetry = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnReLoginSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Result { protected set; get; }

    public bool IsReloginSuccess { protected set; get; }
  }
}
