﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCharSkinItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroCharSkinItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./SkinNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./LimitTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGo;
    [AutoBind("./LimitTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeValueText;
    [AutoBind("./LimitTime", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_timeStateCtrl;
    private int m_index;
    [DoNotToLua]
    private HeroCharSkinItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetHeroCharSkinItemInt32Int32ConfigDataHeroInfoInt32String_hotfix;
    private LuaFunction m_CreateDefaultItemConfigDataHeroInfo_hotfix;
    private LuaFunction m_ShowSelectImageBoolean_hotfix;
    private LuaFunction m_OnHeroJobCardItemClick_hotfix;
    private LuaFunction m_GetIndex_hotfix;
    private LuaFunction m_SetLimitTime_hotfix;
    private LuaFunction m_add_EventOnItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnItemClickAction`1_hotfix;
    private LuaFunction m_get_IsDefaultSkin_hotfix;
    private LuaFunction m_set_IsDefaultSkinBoolean_hotfix;
    private LuaFunction m_get_HeroInfo_hotfix;
    private LuaFunction m_set_HeroInfoConfigDataHeroInfo_hotfix;
    private LuaFunction m_get_HeroSkinInfo_hotfix;
    private LuaFunction m_set_HeroSkinInfoConfigDataHeroSkinInfo_hotfix;
    private LuaFunction m_get_CharSkinInfo_hotfix;
    private LuaFunction m_set_CharSkinInfoConfigDataCharImageSkinResourceInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCharSkinItem(
      int heroSkinId,
      int index,
      ConfigDataHeroInfo heroInfo,
      int heroCurSkinId,
      string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateDefaultItem(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobCardItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroCharSkinItemUIController> EventOnItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsDefaultSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroSkinInfo HeroSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCharImageSkinResourceInfo CharSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroCharSkinItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnItemClick(HeroCharSkinItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnItemClick(HeroCharSkinItemUIController obj)
    {
      this.EventOnItemClick = (Action<HeroCharSkinItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroCharSkinItemUIController m_owner;

      public LuaExportHelper(HeroCharSkinItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnItemClick(HeroCharSkinItemUIController obj)
      {
        this.m_owner.__callDele_EventOnItemClick(obj);
      }

      public void __clearDele_EventOnItemClick(HeroCharSkinItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnItemClick(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public GameObject m_selectImage
      {
        get
        {
          return this.m_owner.m_selectImage;
        }
        set
        {
          this.m_owner.m_selectImage = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public GameObject m_timeGo
      {
        get
        {
          return this.m_owner.m_timeGo;
        }
        set
        {
          this.m_owner.m_timeGo = value;
        }
      }

      public Text m_timeValueText
      {
        get
        {
          return this.m_owner.m_timeValueText;
        }
        set
        {
          this.m_owner.m_timeValueText = value;
        }
      }

      public CommonUIStateController m_timeStateCtrl
      {
        get
        {
          return this.m_owner.m_timeStateCtrl;
        }
        set
        {
          this.m_owner.m_timeStateCtrl = value;
        }
      }

      public int m_index
      {
        get
        {
          return this.m_owner.m_index;
        }
        set
        {
          this.m_owner.m_index = value;
        }
      }

      public bool IsDefaultSkin
      {
        set
        {
          this.m_owner.IsDefaultSkin = value;
        }
      }

      public ConfigDataHeroInfo HeroInfo
      {
        set
        {
          this.m_owner.HeroInfo = value;
        }
      }

      public ConfigDataHeroSkinInfo HeroSkinInfo
      {
        set
        {
          this.m_owner.HeroSkinInfo = value;
        }
      }

      public ConfigDataCharImageSkinResourceInfo CharSkinInfo
      {
        set
        {
          this.m_owner.CharSkinInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnHeroJobCardItemClick()
      {
        this.m_owner.OnHeroJobCardItemClick();
      }
    }
  }
}
