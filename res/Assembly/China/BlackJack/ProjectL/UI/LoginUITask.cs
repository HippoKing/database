﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using PD.SDK;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class LoginUITask : LoginUITaskBase
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Coroutine m_loginAgentCoroutine;
    private LoginUIController m_loginUIController;
    private ServerListUIController m_serverListUIController;
    private CreateCharacterUIController m_createCharaterUIController;
    private LoginCommonUIController m_loginCommonUIController;
    private int m_retryLoginByAuthTokenCount;
    private int m_retryLoginBySessionTokenCount;
    private List<string> m_assets;
    private List<string> m_randomNameHead;
    private List<string> m_randomNameMiddle;
    private List<string> m_randomNameTail;
    private System.Random m_randomNameRandom;
    private ClientConfigDataLoader m_configDataLoader;
    private static bool s_isGlobalInitialized;
    private static string s_localConfigPath;
    private static string m_loadConfigFailedMessage;
    private List<UITaskBase.LayerDesc> m_curLoadingLayers;
    private static string m_serverListText;
    private static List<LoginUITask.ServerInfo> m_serverlist;
    private static bool m_isRecentLoginServerIDInServerlist;
    private static int m_curSelectServerID;
    private static int m_recommendServerIndex;
    private static float m_totalRecommandWeight;
    private List<LoginUITask.ExistCharInfo> m_exsitCharsInfo;
    private bool m_isOpeningUI;
    private string m_curLoginAgnetUrl;
    public const string CustomParams = "menghuanmonizhan";
    public const int m_messageDuration = 3;
    private bool m_ignoreNetworkErrorOnce;
    private Dictionary<string, HashSet<string>> m_gmUserIDs;
    private static bool m_isGMUser;
    private bool m_isGettingSDKPlatformUserID;
    private string m_pdsdkLoginReturnData;
    private string m_pdsdkLoginReturnOpcode;
    private string m_pdsdkLoginReturnChannelID;
    private string m_pdsdkLoginReturnCustomParams;
    private static bool m_isAutoRelogin;
    private static bool m_isReturnToLoginAndSwitchUser;
    private static bool m_isReturnToLoginAndOnLoginSuccess;
    private static LoginSuccessMsg m_onSwitchUserSuccessMsg;
    private bool m_isNewRole;
    private Coroutine m_downloadServerListCoroutine;
    private bool m_isClearSessionFailed;
    [DoNotToLua]
    private LoginUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_Finalize_hotfix;
    private LuaFunction m_ShowDialogBoxStringTableId_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_CollectAllDynamicResForLoad_hotfix;
    private LuaFunction m_PostOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_StartCoroutineKeepUpdatingServerList_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_ServerListUIController_OnServerListClosedInt32_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_StopEntryUITask_hotfix;
    private LuaFunction m_DownloadServerListFile_hotfix;
    private LuaFunction m_DownloadAnnounceFileBoolean_hotfix;
    private LuaFunction m_DownloadGMUserIDs_hotfix;
    private LuaFunction m_ParseGMUserIDsTextString_hotfix;
    private LuaFunction m_UpdateLoginPCClientButton_hotfix;
    private LuaFunction m_IsSdkIDCanLoginPCClientString_hotfix;
    private LuaFunction m_UpdateGMUserFlagServerInfo_hotfix;
    private LuaFunction m_UpdateLocalConfigGMUser_hotfix;
    private LuaFunction m_GetSDKPlatformUserIDServerInfo_hotfix;
    private LuaFunction m_ReqExistChars_hotfix;
    private LuaFunction m_ShowWaitingNetBoolean_hotfix;
    private LuaFunction m_LaunchEnterGameUIWithGameSettingTokenAndServer_hotfix;
    private LuaFunction m_LaunchEnterGameUIWithUIInputAccountAndServer_hotfix;
    private LuaFunction m_DownloadGameServerLoginAnnouncementAction`1_hotfix;
    private LuaFunction m_ShowGameServerLoginAnnouncementUI_hotfix;
    private LuaFunction m_ShowEnterGameUI_hotfix;
    private LuaFunction m_StartLoginAgentLoginAction`3_hotfix;
    private LuaFunction m_OnLoginByAuthTokenAckInt32StringBoolean_hotfix;
    private LuaFunction m_DelayEnableInputBooleanSingle_hotfix;
    private LuaFunction m_ShowMessageStringTableIdInt32Boolean_hotfix;
    private LuaFunction m_OnLoginBySessionTokenAckInt32_hotfix;
    private LuaFunction m_LoginBySessionTokenAgain_hotfix;
    private LuaFunction m_StartPlayerInfoInitReq_hotfix;
    private LuaFunction m_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_PlayerContext_OnGameServerNetworkError_hotfix;
    private LuaFunction m_LauncheMainUI_hotfix;
    private LuaFunction m_OnWaitingMsgAckOutTime_hotfix;
    private LuaFunction m_OnGameServerConnected_hotfix;
    private LuaFunction m_OnGameServerNetworkErrorInt32_hotfix;
    private LuaFunction m_ShowCreateCharacterUI_hotfix;
    private LuaFunction m_StartCreateCharacterReq_hotfix;
    private LuaFunction m_SaveSessionTokenCacheString_hotfix;
    private LuaFunction m_GetSessionTokenCache_hotfix;
    private LuaFunction m_get_SessionTokenCacheFileName_hotfix;
    private LuaFunction m_ClearSessionTokenCache_hotfix;
    private LuaFunction m_IsSessionTokenValidString_hotfix;
    private LuaFunction m_SaveLoginInfo_hotfix;
    private LuaFunction m_CreateRandomName_hotfix;
    private LuaFunction m_EnableInputBoolean_hotfix;
    private LuaFunction m_PDLogin_hotfix;
    private LuaFunction m_LoginUIController_OnLoginPCClient_hotfix;
    private LuaFunction m_PDSDK_OnQRLoginSuccessString_hotfix;
    private LuaFunction m_PDSDK_OnQRLoginFailedString_hotfix;
    private LuaFunction m_PDSDK_OnQRLoginCancelString_hotfix;
    private LuaFunction m_PDSK_OnLoginFailedString_hotfix;
    private LuaFunction m_PDSDK_OnInitFailed_hotfix;
    private LuaFunction m_PDSDK_OnInitSucess_hotfix;
    private LuaFunction m_ClearSDKUserIDOfAllServer_hotfix;
    private LuaFunction m_PDSDK_OnLoginSuccessLoginSuccessMsg_hotfix;
    private LuaFunction m_ParseLoginAgentAckStringString_String_String_String_String__hotfix;
    private LuaFunction m_LoginAgentStringStringStringStringString_hotfix;
    private LuaFunction m_SetPlatformInfoToServerListStringStringString_hotfix;
    private LuaFunction m_LoginUIController_OnOpenUserCenter_hotfix;
    private LuaFunction m_SwitchUserAccount_hotfix;
    private LuaFunction m_LoginUIController_OnAccountTextChangedString_hotfix;
    private LuaFunction m_LoginUIController_OnSaveServerConfig_hotfix;
    private LuaFunction m_LoginUIController_OnCloseAnnouncePanel_hotfix;
    private LuaFunction m_LoginUIController_OnOpenAnnouncePanel_hotfix;
    private LuaFunction m_LoginUIController_OnSelectServerClick_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_CollectAllStaticResDescForLoad_hotfix;
    private LuaFunction m_ShowServerListUIBoolean_hotfix;
    private LuaFunction m_KeepUpdatingServerList_hotfix;
    private LuaFunction m_LoginUIController_OnLogin_hotfix;
    private LuaFunction m_CreateCharacterUIController_OnCreate_hotfix;
    private LuaFunction m_CreateCharacterUIController_OnAutoName_hotfix;
    private LuaFunction m_UpdateServerList_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~LoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(StringTableId strID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Relogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReturnToLoginAndSwitchUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitializeGlobals()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitLocalConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LogSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public static List<LoginUITask.ServerInfo> ServerList
    {
      get
      {
        return LoginUITask.m_serverlist;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LoginUITask.ServerInfo GetServerInfoByBornChannelID(int bornChannelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    private static bool ShouldUsePDSDK
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCoroutineKeepUpdatingServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClearLocalPushNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLocalPushNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static DateTime GetPushNotificationTime(ConfigDataDailyPushNotification cfg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<LoginUITask.ServerInfo> GetRecentLoginServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ServerListUIController_OnServerListClosed(int selectedServerID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DeleteExpiredLogFiles(string logFolder, int days)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator CheckClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopEntryUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int RecommendServerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LoginUITask.ServerInfo GetCurrentSelectServerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadServerListFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadAnnounceFile(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadGMUserIDs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ParseGMUserIDsText(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsGMUser
    {
      get
      {
        return LoginUITask.m_isGMUser;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateLoginPCClientButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSdkIDCanLoginPCClient(string sdkChannelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGMUserFlag(LoginUITask.ServerInfo si = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLocalConfigGMUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetSDKPlatformUserID(LoginUITask.ServerInfo si)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReqExistChars()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitingNet(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LaunchEnterGameUIWithGameSettingTokenAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LaunchEnterGameUIWithUIInputAccountAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ShowGameServerLoginAnnouncementUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ShowEnterGameUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayEnableInput(bool isEnable, float delaySeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, int time = 0, bool isOverride = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnLoginBySessionTokenAck(int ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoginBySessionTokenAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void StartPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetStartGameOrCreateRoleJsonString(string rolenameProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string PDSDKStartGameJsonString
    {
      get
      {
        return LoginUITask.GetStartGameOrCreateRoleJsonString("GameName");
      }
    }

    public static string PDSDKGameRoleJsonString
    {
      get
      {
        return LoginUITask.GetStartGameOrCreateRoleJsonString("RoleName");
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnGameServerNetworkError()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LauncheMainUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartPreloadUITask()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnWaitingMsgAckOutTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnGameServerConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnGameServerNetworkError(int err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCreateCharacterUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCreateCharacterReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveSessionTokenCache(string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSessionTokenCache()
    {
      // ISSUE: unable to decompile the method.
    }

    private string SessionTokenCacheFileName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearSessionTokenCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSessionTokenValid(string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveLoginInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CreateRandomName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableInput(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnLoginPCClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSK_OnLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnInitFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnInitSucess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearSDKUserIDOfAllServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDK_OnLogoutSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDK_OnSwitchUserSuccess(LoginSuccessMsg msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnLoginSuccess(LoginSuccessMsg msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ParseLoginAgentAck(
      string ackText,
      ref string status,
      ref string platformName,
      ref string userId,
      ref string token,
      ref string error)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoginAgent(
      string svrUrl,
      string data,
      string opcode,
      string channel_id,
      string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlatformInfoToServerList(
      string loginAgentUrl,
      string platformName,
      string platformUserID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnOpenUserCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SwitchUserAccount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnAccountTextChanged(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnSaveServerConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnCloseAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnOpenAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnSelectServerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowServerListUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string SessionAccountInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator KeepUpdatingServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharacterUIController_OnCreate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharacterUIController_OnAutoName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckServerMaitainState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_OnReLoginBySession(Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    public static LoginUITask FindAInstance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_ReturnToLoginUI(bool isDataDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_StartLoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ParseServerListText(string serverListText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ParseServerOpenDateTime(string dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_OnEventShowUIWaiting(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public LoginUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private bool __callBase_IsSDKLogin()
    {
      return this.IsSDKLogin();
    }

    private bool __callBase_IsEditorTestLogin()
    {
      return this.IsEditorTestLogin();
    }

    private void __callBase_StartInitSDK()
    {
      this.StartInitSDK();
    }

    private void __callBase_OnSDKInitEnd(bool isSuccess)
    {
      this.OnSDKInitEnd(isSuccess);
    }

    private void __callBase_StartSDKLogin()
    {
      this.StartSDKLogin();
    }

    private void __callBase_OnSDKLoginEnd(bool isSuccess)
    {
      this.OnSDKLoginEnd(isSuccess);
    }

    private void __callBase_PostSDKLogin()
    {
      this.PostSDKLogin();
    }

    private bool __callBase_IsNeedSelectServer()
    {
      return this.IsNeedSelectServer();
    }

    private void __callBase_LaunchServerListUI()
    {
      this.LaunchServerListUI();
    }

    private IEnumerator __callBase_DownloadServerList(Action<bool> onEnd)
    {
      return this.DownloadServerList(onEnd);
    }

    private void __callBase_OnDownloadServerListEnd(bool isSuccess)
    {
      this.OnDownloadServerListEnd(isSuccess);
    }

    private void __callBase_ShowServerListUI()
    {
      this.ShowServerListUI();
    }

    private void __callBase_OnGameServerConfirmed(string serverId)
    {
      this.OnGameServerConfirmed(serverId);
    }

    private void __callBase_InitServerCtxByServerID(string serverId)
    {
      this.InitServerCtxByServerID(serverId);
    }

    private string __callBase_GetLastLoginedServerID()
    {
      return this.GetLastLoginedServerID();
    }

    private void __callBase_LaunchEnterGameUI()
    {
      this.LaunchEnterGameUI();
    }

    private void __callBase_LaunchEnterGameUIWithGameSettingTokenAndServer()
    {
      base.LaunchEnterGameUIWithGameSettingTokenAndServer();
    }

    private void __callBase_LaunchEnterGameUIWithUIInputAccountAndServer()
    {
      base.LaunchEnterGameUIWithUIInputAccountAndServer();
    }

    private bool __callBase_LoadLastLoginWithUIInputInfo(
      out string loginUserId,
      out string authLoginServerAddress,
      out int authLoginServerPort)
    {
      return this.LoadLastLoginWithUIInputInfo(out loginUserId, out authLoginServerAddress, out authLoginServerPort);
    }

    private IEnumerator __callBase_DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
    {
      return base.DownloadGameServerLoginAnnouncement(onEnd);
    }

    private void __callBase_OnDownloadGameServerLoginAnnouncementEnd(bool isSuccess)
    {
      this.OnDownloadGameServerLoginAnnouncementEnd(isSuccess);
    }

    private void __callBase_ShowGameServerLoginAnnouncementUI()
    {
      base.ShowGameServerLoginAnnouncementUI();
    }

    private void __callBase_ShowEnterGameUI()
    {
      base.ShowEnterGameUI();
    }

    private void __callBase_OnEnterGameConfirmed()
    {
      this.OnEnterGameConfirmed();
    }

    private IEnumerator __callBase_StartLoginAgentLogin(Action<int, string, string> onEnd)
    {
      return base.StartLoginAgentLogin(onEnd);
    }

    private void __callBase_LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
    {
      this.LoginAgentLoginEnd(errCode, loginUserId, authToken);
    }

    private void __callBase_StartAuthLogin()
    {
      this.StartAuthLogin();
    }

    private void __callBase_StartSessionLogin()
    {
      this.StartSessionLogin();
    }

    private void __callBase_StartPlayerInfoInitReq()
    {
      base.StartPlayerInfoInitReq();
    }

    private void __callBase_LauncheMainUI()
    {
      base.LauncheMainUI();
    }

    private void __callBase_OnWaitingMsgAckOutTime()
    {
      base.OnWaitingMsgAckOutTime();
    }

    private void __callBase_StartWaitingMsgAck(float waitTime)
    {
      this.StartWaitingMsgAck(waitTime);
    }

    private void __callBase_StopWaitingMsgAck()
    {
      this.StopWaitingMsgAck();
    }

    private void __callBase_ClearGameServerLoginState()
    {
      this.ClearGameServerLoginState();
    }

    private void __callBase_OnTick()
    {
      base.OnTick();
    }

    private void __callBase_OnGameServerConnected()
    {
      base.OnGameServerConnected();
    }

    private void __callBase_OnGameServerDisconnected()
    {
      this.OnGameServerDisconnected();
    }

    private void __callBase_OnGameServerNetworkError(int err)
    {
      base.OnGameServerNetworkError(err);
    }

    private bool __callBase_OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
    {
      return base.OnLoginByAuthTokenAck(ret, sessionToken, needRedirect);
    }

    private bool __callBase_OnLoginBySessionTokenAck(int ret)
    {
      return base.OnLoginBySessionTokenAck(ret);
    }

    private void __callBase_OnConfigDataMD5InfoNtf(
      string fileNameWithErrorMD5,
      string localMD5,
      string serverMD5)
    {
      this.OnConfigDataMD5InfoNtf(fileNameWithErrorMD5, localMD5, serverMD5);
    }

    private void __callBase_OnPlayerInfoInitEnd()
    {
      base.OnPlayerInfoInitEnd();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static LoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [HotFix]
    public class ExistCharInfo
    {
      public string m_roleListURL;
      public int m_channelId;
      public int m_playerLevel;
      public int m_headIcon;
      public int m_lastLoginHours;
      public string m_charName;

      [MethodImpl((MethodImplOptions) 32768)]
      public ExistCharInfo(
        string roleListURL,
        int channelId,
        int playerId,
        int headIcon,
        int lastLoginHours,
        string charName)
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [HotFix]
    public class ServerInfo
    {
      public LoginUITask.ServerInfo.State m_state = LoginUITask.ServerInfo.State.Normal;
      public int m_id;
      public string m_name;
      public bool m_isNew;
      public string m_ip;
      public string m_domain;
      public int m_port;
      public string m_loginAgentUrl;
      public int m_channelId;
      public int m_bornChannelId;
      public float m_recommendWeight;
      public int m_initialIndex;
      public string m_sdkPlatformName;
      public string m_sdkPlatformUserID;
      public string m_roleListURL;
      public int m_realmID;
      public bool m_isAppleReview;
      public string m_serverOpenDateTime;
      public bool m_isRefuseNewPlayer;
      public string m_areaName;

      public ServerInfo(
        int initialIndex,
        int id,
        string name,
        LoginUITask.ServerInfo.State state,
        bool isNew,
        string ip,
        string domain,
        int port,
        string loginAgentUrl,
        int channelId,
        int bornChannelId,
        float recommendWeight,
        string roleListURL,
        int realmID,
        bool isAppleReview,
        string serverOpenDateTime,
        bool isRefuseNewPlayer,
        string areaName)
      {
        this.m_initialIndex = initialIndex;
        this.m_id = id;
        this.m_name = name;
        this.m_state = state;
        this.m_isNew = isNew;
        this.m_ip = ip;
        this.m_domain = domain;
        this.m_port = port;
        this.m_loginAgentUrl = loginAgentUrl;
        this.m_channelId = channelId;
        this.m_bornChannelId = bornChannelId;
        this.m_recommendWeight = recommendWeight;
        this.m_roleListURL = roleListURL.ToLower();
        this.m_realmID = realmID;
        this.m_isAppleReview = isAppleReview;
        this.m_serverOpenDateTime = serverOpenDateTime;
        this.m_isRefuseNewPlayer = isRefuseNewPlayer;
        this.m_areaName = areaName;
      }

      public override string ToString()
      {
        return this.m_ip + (object) this.m_port + this.m_loginAgentUrl + (object) this.m_channelId + (object) this.m_bornChannelId;
      }

      public enum State
      {
        Hot = 1,
        Crowded = 2,
        Maintain = 3,
        Normal = 4,
      }
    }

    public class LuaExportHelper
    {
      private LoginUITask m_owner;

      public LuaExportHelper(LoginUITask owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public bool __callBase_IsSDKLogin()
      {
        return this.m_owner.__callBase_IsSDKLogin();
      }

      public bool __callBase_IsEditorTestLogin()
      {
        return this.m_owner.__callBase_IsEditorTestLogin();
      }

      public void __callBase_StartInitSDK()
      {
        this.m_owner.__callBase_StartInitSDK();
      }

      public void __callBase_OnSDKInitEnd(bool isSuccess)
      {
        this.m_owner.__callBase_OnSDKInitEnd(isSuccess);
      }

      public void __callBase_StartSDKLogin()
      {
        this.m_owner.__callBase_StartSDKLogin();
      }

      public void __callBase_OnSDKLoginEnd(bool isSuccess)
      {
        this.m_owner.__callBase_OnSDKLoginEnd(isSuccess);
      }

      public void __callBase_PostSDKLogin()
      {
        this.m_owner.__callBase_PostSDKLogin();
      }

      public bool __callBase_IsNeedSelectServer()
      {
        return this.m_owner.__callBase_IsNeedSelectServer();
      }

      public void __callBase_LaunchServerListUI()
      {
        this.m_owner.__callBase_LaunchServerListUI();
      }

      public IEnumerator __callBase_DownloadServerList(Action<bool> onEnd)
      {
        return this.m_owner.__callBase_DownloadServerList(onEnd);
      }

      public void __callBase_OnDownloadServerListEnd(bool isSuccess)
      {
        this.m_owner.__callBase_OnDownloadServerListEnd(isSuccess);
      }

      public void __callBase_ShowServerListUI()
      {
        this.m_owner.__callBase_ShowServerListUI();
      }

      public void __callBase_OnGameServerConfirmed(string serverId)
      {
        this.m_owner.__callBase_OnGameServerConfirmed(serverId);
      }

      public void __callBase_InitServerCtxByServerID(string serverId)
      {
        this.m_owner.__callBase_InitServerCtxByServerID(serverId);
      }

      public string __callBase_GetLastLoginedServerID()
      {
        return this.m_owner.__callBase_GetLastLoginedServerID();
      }

      public void __callBase_LaunchEnterGameUI()
      {
        this.m_owner.__callBase_LaunchEnterGameUI();
      }

      public void __callBase_LaunchEnterGameUIWithGameSettingTokenAndServer()
      {
        this.m_owner.__callBase_LaunchEnterGameUIWithGameSettingTokenAndServer();
      }

      public void __callBase_LaunchEnterGameUIWithUIInputAccountAndServer()
      {
        this.m_owner.__callBase_LaunchEnterGameUIWithUIInputAccountAndServer();
      }

      public bool __callBase_LoadLastLoginWithUIInputInfo(
        out string loginUserId,
        out string authLoginServerAddress,
        out int authLoginServerPort)
      {
        return this.m_owner.__callBase_LoadLastLoginWithUIInputInfo(out loginUserId, out authLoginServerAddress, out authLoginServerPort);
      }

      public IEnumerator __callBase_DownloadGameServerLoginAnnouncement(
        Action<bool> onEnd)
      {
        return this.m_owner.__callBase_DownloadGameServerLoginAnnouncement(onEnd);
      }

      public void __callBase_OnDownloadGameServerLoginAnnouncementEnd(bool isSuccess)
      {
        this.m_owner.__callBase_OnDownloadGameServerLoginAnnouncementEnd(isSuccess);
      }

      public void __callBase_ShowGameServerLoginAnnouncementUI()
      {
        this.m_owner.__callBase_ShowGameServerLoginAnnouncementUI();
      }

      public void __callBase_ShowEnterGameUI()
      {
        this.m_owner.__callBase_ShowEnterGameUI();
      }

      public void __callBase_OnEnterGameConfirmed()
      {
        this.m_owner.__callBase_OnEnterGameConfirmed();
      }

      public IEnumerator __callBase_StartLoginAgentLogin(
        Action<int, string, string> onEnd)
      {
        return this.m_owner.__callBase_StartLoginAgentLogin(onEnd);
      }

      public void __callBase_LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
      {
        this.m_owner.__callBase_LoginAgentLoginEnd(errCode, loginUserId, authToken);
      }

      public void __callBase_StartAuthLogin()
      {
        this.m_owner.__callBase_StartAuthLogin();
      }

      public void __callBase_StartSessionLogin()
      {
        this.m_owner.__callBase_StartSessionLogin();
      }

      public void __callBase_StartPlayerInfoInitReq()
      {
        this.m_owner.__callBase_StartPlayerInfoInitReq();
      }

      public void __callBase_LauncheMainUI()
      {
        this.m_owner.__callBase_LauncheMainUI();
      }

      public void __callBase_OnWaitingMsgAckOutTime()
      {
        this.m_owner.__callBase_OnWaitingMsgAckOutTime();
      }

      public void __callBase_StartWaitingMsgAck(float waitTime)
      {
        this.m_owner.__callBase_StartWaitingMsgAck(waitTime);
      }

      public void __callBase_StopWaitingMsgAck()
      {
        this.m_owner.__callBase_StopWaitingMsgAck();
      }

      public void __callBase_ClearGameServerLoginState()
      {
        this.m_owner.__callBase_ClearGameServerLoginState();
      }

      public void __callBase_OnTick()
      {
        this.m_owner.__callBase_OnTick();
      }

      public void __callBase_OnGameServerConnected()
      {
        this.m_owner.__callBase_OnGameServerConnected();
      }

      public void __callBase_OnGameServerDisconnected()
      {
        this.m_owner.__callBase_OnGameServerDisconnected();
      }

      public void __callBase_OnGameServerNetworkError(int err)
      {
        this.m_owner.__callBase_OnGameServerNetworkError(err);
      }

      public bool __callBase_OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
      {
        return this.m_owner.__callBase_OnLoginByAuthTokenAck(ret, sessionToken, needRedirect);
      }

      public bool __callBase_OnLoginBySessionTokenAck(int ret)
      {
        return this.m_owner.__callBase_OnLoginBySessionTokenAck(ret);
      }

      public void __callBase_OnConfigDataMD5InfoNtf(
        string fileNameWithErrorMD5,
        string localMD5,
        string serverMD5)
      {
        this.m_owner.__callBase_OnConfigDataMD5InfoNtf(fileNameWithErrorMD5, localMD5, serverMD5);
      }

      public void __callBase_OnPlayerInfoInitEnd()
      {
        this.m_owner.__callBase_OnPlayerInfoInitEnd();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public Coroutine m_loginAgentCoroutine
      {
        get
        {
          return this.m_owner.m_loginAgentCoroutine;
        }
        set
        {
          this.m_owner.m_loginAgentCoroutine = value;
        }
      }

      public LoginUIController m_loginUIController
      {
        get
        {
          return this.m_owner.m_loginUIController;
        }
        set
        {
          this.m_owner.m_loginUIController = value;
        }
      }

      public ServerListUIController m_serverListUIController
      {
        get
        {
          return this.m_owner.m_serverListUIController;
        }
        set
        {
          this.m_owner.m_serverListUIController = value;
        }
      }

      public CreateCharacterUIController m_createCharaterUIController
      {
        get
        {
          return this.m_owner.m_createCharaterUIController;
        }
        set
        {
          this.m_owner.m_createCharaterUIController = value;
        }
      }

      public LoginCommonUIController m_loginCommonUIController
      {
        get
        {
          return this.m_owner.m_loginCommonUIController;
        }
        set
        {
          this.m_owner.m_loginCommonUIController = value;
        }
      }

      public int m_retryLoginByAuthTokenCount
      {
        get
        {
          return this.m_owner.m_retryLoginByAuthTokenCount;
        }
        set
        {
          this.m_owner.m_retryLoginByAuthTokenCount = value;
        }
      }

      public int m_retryLoginBySessionTokenCount
      {
        get
        {
          return this.m_owner.m_retryLoginBySessionTokenCount;
        }
        set
        {
          this.m_owner.m_retryLoginBySessionTokenCount = value;
        }
      }

      public List<string> m_assets
      {
        get
        {
          return this.m_owner.m_assets;
        }
        set
        {
          this.m_owner.m_assets = value;
        }
      }

      public List<string> m_randomNameHead
      {
        get
        {
          return this.m_owner.m_randomNameHead;
        }
        set
        {
          this.m_owner.m_randomNameHead = value;
        }
      }

      public List<string> m_randomNameMiddle
      {
        get
        {
          return this.m_owner.m_randomNameMiddle;
        }
        set
        {
          this.m_owner.m_randomNameMiddle = value;
        }
      }

      public List<string> m_randomNameTail
      {
        get
        {
          return this.m_owner.m_randomNameTail;
        }
        set
        {
          this.m_owner.m_randomNameTail = value;
        }
      }

      public System.Random m_randomNameRandom
      {
        get
        {
          return this.m_owner.m_randomNameRandom;
        }
        set
        {
          this.m_owner.m_randomNameRandom = value;
        }
      }

      public ClientConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public static bool s_isGlobalInitialized
      {
        get
        {
          return LoginUITask.s_isGlobalInitialized;
        }
        set
        {
          LoginUITask.s_isGlobalInitialized = value;
        }
      }

      public static string s_localConfigPath
      {
        get
        {
          return LoginUITask.s_localConfigPath;
        }
        set
        {
          LoginUITask.s_localConfigPath = value;
        }
      }

      public static string m_loadConfigFailedMessage
      {
        get
        {
          return LoginUITask.m_loadConfigFailedMessage;
        }
        set
        {
          LoginUITask.m_loadConfigFailedMessage = value;
        }
      }

      public List<UITaskBase.LayerDesc> m_curLoadingLayers
      {
        get
        {
          return this.m_owner.m_curLoadingLayers;
        }
        set
        {
          this.m_owner.m_curLoadingLayers = value;
        }
      }

      public static string m_serverListText
      {
        get
        {
          return LoginUITask.m_serverListText;
        }
        set
        {
          LoginUITask.m_serverListText = value;
        }
      }

      public static List<LoginUITask.ServerInfo> m_serverlist
      {
        get
        {
          return LoginUITask.m_serverlist;
        }
        set
        {
          LoginUITask.m_serverlist = value;
        }
      }

      public static bool m_isRecentLoginServerIDInServerlist
      {
        get
        {
          return LoginUITask.m_isRecentLoginServerIDInServerlist;
        }
        set
        {
          LoginUITask.m_isRecentLoginServerIDInServerlist = value;
        }
      }

      public static int m_curSelectServerID
      {
        get
        {
          return LoginUITask.m_curSelectServerID;
        }
        set
        {
          LoginUITask.m_curSelectServerID = value;
        }
      }

      public static int m_recommendServerIndex
      {
        get
        {
          return LoginUITask.m_recommendServerIndex;
        }
        set
        {
          LoginUITask.m_recommendServerIndex = value;
        }
      }

      public static float m_totalRecommandWeight
      {
        get
        {
          return LoginUITask.m_totalRecommandWeight;
        }
        set
        {
          LoginUITask.m_totalRecommandWeight = value;
        }
      }

      public List<LoginUITask.ExistCharInfo> m_exsitCharsInfo
      {
        get
        {
          return this.m_owner.m_exsitCharsInfo;
        }
        set
        {
          this.m_owner.m_exsitCharsInfo = value;
        }
      }

      public bool m_isOpeningUI
      {
        get
        {
          return this.m_owner.m_isOpeningUI;
        }
        set
        {
          this.m_owner.m_isOpeningUI = value;
        }
      }

      public string m_curLoginAgnetUrl
      {
        get
        {
          return this.m_owner.m_curLoginAgnetUrl;
        }
        set
        {
          this.m_owner.m_curLoginAgnetUrl = value;
        }
      }

      public bool m_ignoreNetworkErrorOnce
      {
        get
        {
          return this.m_owner.m_ignoreNetworkErrorOnce;
        }
        set
        {
          this.m_owner.m_ignoreNetworkErrorOnce = value;
        }
      }

      public Dictionary<string, HashSet<string>> m_gmUserIDs
      {
        get
        {
          return this.m_owner.m_gmUserIDs;
        }
        set
        {
          this.m_owner.m_gmUserIDs = value;
        }
      }

      public static bool m_isGMUser
      {
        get
        {
          return LoginUITask.m_isGMUser;
        }
        set
        {
          LoginUITask.m_isGMUser = value;
        }
      }

      public bool m_isGettingSDKPlatformUserID
      {
        get
        {
          return this.m_owner.m_isGettingSDKPlatformUserID;
        }
        set
        {
          this.m_owner.m_isGettingSDKPlatformUserID = value;
        }
      }

      public string m_pdsdkLoginReturnData
      {
        get
        {
          return this.m_owner.m_pdsdkLoginReturnData;
        }
        set
        {
          this.m_owner.m_pdsdkLoginReturnData = value;
        }
      }

      public string m_pdsdkLoginReturnOpcode
      {
        get
        {
          return this.m_owner.m_pdsdkLoginReturnOpcode;
        }
        set
        {
          this.m_owner.m_pdsdkLoginReturnOpcode = value;
        }
      }

      public string m_pdsdkLoginReturnChannelID
      {
        get
        {
          return this.m_owner.m_pdsdkLoginReturnChannelID;
        }
        set
        {
          this.m_owner.m_pdsdkLoginReturnChannelID = value;
        }
      }

      public string m_pdsdkLoginReturnCustomParams
      {
        get
        {
          return this.m_owner.m_pdsdkLoginReturnCustomParams;
        }
        set
        {
          this.m_owner.m_pdsdkLoginReturnCustomParams = value;
        }
      }

      public static bool m_isAutoRelogin
      {
        get
        {
          return LoginUITask.m_isAutoRelogin;
        }
        set
        {
          LoginUITask.m_isAutoRelogin = value;
        }
      }

      public static bool m_isReturnToLoginAndSwitchUser
      {
        get
        {
          return LoginUITask.m_isReturnToLoginAndSwitchUser;
        }
        set
        {
          LoginUITask.m_isReturnToLoginAndSwitchUser = value;
        }
      }

      public static bool m_isReturnToLoginAndOnLoginSuccess
      {
        get
        {
          return LoginUITask.m_isReturnToLoginAndOnLoginSuccess;
        }
        set
        {
          LoginUITask.m_isReturnToLoginAndOnLoginSuccess = value;
        }
      }

      public static LoginSuccessMsg m_onSwitchUserSuccessMsg
      {
        get
        {
          return LoginUITask.m_onSwitchUserSuccessMsg;
        }
        set
        {
          LoginUITask.m_onSwitchUserSuccessMsg = value;
        }
      }

      public bool m_isNewRole
      {
        get
        {
          return this.m_owner.m_isNewRole;
        }
        set
        {
          this.m_owner.m_isNewRole = value;
        }
      }

      public Coroutine m_downloadServerListCoroutine
      {
        get
        {
          return this.m_owner.m_downloadServerListCoroutine;
        }
        set
        {
          this.m_owner.m_downloadServerListCoroutine = value;
        }
      }

      public bool m_isClearSessionFailed
      {
        get
        {
          return this.m_owner.m_isClearSessionFailed;
        }
        set
        {
          this.m_owner.m_isClearSessionFailed = value;
        }
      }

      public static bool ShouldUsePDSDK
      {
        get
        {
          return LoginUITask.ShouldUsePDSDK;
        }
      }

      public string SessionTokenCacheFileName
      {
        get
        {
          return this.m_owner.SessionTokenCacheFileName;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public static void InitializeGlobals()
      {
        LoginUITask.InitializeGlobals();
      }

      public static void InitLocalConfig()
      {
        LoginUITask.InitLocalConfig();
      }

      public static void LogSystemInfo()
      {
        LoginUITask.LogSystemInfo();
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public List<string> CollectAllDynamicResForLoad()
      {
        return this.m_owner.CollectAllDynamicResForLoad();
      }

      public void PostOnLoadAllResCompleted()
      {
        this.m_owner.PostOnLoadAllResCompleted();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void StartCoroutineKeepUpdatingServerList()
      {
        this.m_owner.StartCoroutineKeepUpdatingServerList();
      }

      public static DateTime GetPushNotificationTime(ConfigDataDailyPushNotification cfg)
      {
        return LoginUITask.GetPushNotificationTime(cfg);
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public static List<LoginUITask.ServerInfo> GetRecentLoginServerList()
      {
        return LoginUITask.GetRecentLoginServerList();
      }

      public void ServerListUIController_OnServerListClosed(int selectedServerID)
      {
        this.m_owner.ServerListUIController_OnServerListClosed(selectedServerID);
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void StopEntryUITask()
      {
        this.m_owner.StopEntryUITask();
      }

      public void DownloadServerListFile()
      {
        this.m_owner.DownloadServerListFile();
      }

      public void DownloadAnnounceFile(bool isShow)
      {
        this.m_owner.DownloadAnnounceFile(isShow);
      }

      public void DownloadGMUserIDs()
      {
        this.m_owner.DownloadGMUserIDs();
      }

      public void ParseGMUserIDsText(string text)
      {
        this.m_owner.ParseGMUserIDsText(text);
      }

      public bool IsSdkIDCanLoginPCClient(string sdkChannelID)
      {
        return this.m_owner.IsSdkIDCanLoginPCClient(sdkChannelID);
      }

      public void UpdateLocalConfigGMUser()
      {
        this.m_owner.UpdateLocalConfigGMUser();
      }

      public void ReqExistChars()
      {
        this.m_owner.ReqExistChars();
      }

      public void LaunchEnterGameUIWithGameSettingTokenAndServer()
      {
        this.m_owner.LaunchEnterGameUIWithGameSettingTokenAndServer();
      }

      public void LaunchEnterGameUIWithUIInputAccountAndServer()
      {
        this.m_owner.LaunchEnterGameUIWithUIInputAccountAndServer();
      }

      public IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
      {
        return this.m_owner.DownloadGameServerLoginAnnouncement(onEnd);
      }

      public void ShowGameServerLoginAnnouncementUI()
      {
        this.m_owner.ShowGameServerLoginAnnouncementUI();
      }

      public void ShowEnterGameUI()
      {
        this.m_owner.ShowEnterGameUI();
      }

      public IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
      {
        return this.m_owner.StartLoginAgentLogin(onEnd);
      }

      public bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
      {
        return this.m_owner.OnLoginByAuthTokenAck(ret, sessionToken, needRedirect);
      }

      public IEnumerator DelayEnableInput(bool isEnable, float delaySeconds)
      {
        return this.m_owner.DelayEnableInput(isEnable, delaySeconds);
      }

      public bool OnLoginBySessionTokenAck(int ret)
      {
        return this.m_owner.OnLoginBySessionTokenAck(ret);
      }

      public IEnumerator LoginBySessionTokenAgain()
      {
        return this.m_owner.LoginBySessionTokenAgain();
      }

      public void StartPlayerInfoInitReq()
      {
        this.m_owner.StartPlayerInfoInitReq();
      }

      public void OnPlayerInfoInitEnd()
      {
        this.m_owner.OnPlayerInfoInitEnd();
      }

      public static string GetStartGameOrCreateRoleJsonString(string rolenameProperty)
      {
        return LoginUITask.GetStartGameOrCreateRoleJsonString(rolenameProperty);
      }

      public void PlayerContext_OnGameServerNetworkError()
      {
        this.m_owner.PlayerContext_OnGameServerNetworkError();
      }

      public void LauncheMainUI()
      {
        this.m_owner.LauncheMainUI();
      }

      public static IEnumerator Co_UnloadAssetsAndStartPreloadUITask()
      {
        return LoginUITask.Co_UnloadAssetsAndStartPreloadUITask();
      }

      public void OnWaitingMsgAckOutTime()
      {
        this.m_owner.OnWaitingMsgAckOutTime();
      }

      public void OnGameServerConnected()
      {
        this.m_owner.OnGameServerConnected();
      }

      public void OnGameServerNetworkError(int err)
      {
        this.m_owner.OnGameServerNetworkError(err);
      }

      public void ShowCreateCharacterUI()
      {
        this.m_owner.ShowCreateCharacterUI();
      }

      public void StartCreateCharacterReq()
      {
        this.m_owner.StartCreateCharacterReq();
      }

      public bool IsSessionTokenValid(string sessionToken)
      {
        return this.m_owner.IsSessionTokenValid(sessionToken);
      }

      public void SaveLoginInfo()
      {
        this.m_owner.SaveLoginInfo();
      }

      public string CreateRandomName()
      {
        return this.m_owner.CreateRandomName();
      }

      public void EnableInput(bool isEnable)
      {
        this.m_owner.EnableInput(isEnable);
      }

      public void PDLogin()
      {
        this.m_owner.PDLogin();
      }

      public void LoginUIController_OnLoginPCClient()
      {
        this.m_owner.LoginUIController_OnLoginPCClient();
      }

      public void PDSDK_OnInitFailed()
      {
        this.m_owner.PDSDK_OnInitFailed();
      }

      public void PDSDK_OnInitSucess()
      {
        this.m_owner.PDSDK_OnInitSucess();
      }

      public void ClearSDKUserIDOfAllServer()
      {
        this.m_owner.ClearSDKUserIDOfAllServer();
      }

      public bool ParseLoginAgentAck(
        string ackText,
        ref string status,
        ref string platformName,
        ref string userId,
        ref string token,
        ref string error)
      {
        return this.m_owner.ParseLoginAgentAck(ackText, ref status, ref platformName, ref userId, ref token, ref error);
      }

      public IEnumerator LoginAgent(
        string svrUrl,
        string data,
        string opcode,
        string channel_id,
        string customparams)
      {
        return this.m_owner.LoginAgent(svrUrl, data, opcode, channel_id, customparams);
      }

      public void SetPlatformInfoToServerList(
        string loginAgentUrl,
        string platformName,
        string platformUserID)
      {
        this.m_owner.SetPlatformInfoToServerList(loginAgentUrl, platformName, platformUserID);
      }

      public void LoginUIController_OnOpenUserCenter()
      {
        this.m_owner.LoginUIController_OnOpenUserCenter();
      }

      public IEnumerator SwitchUserAccount()
      {
        return this.m_owner.SwitchUserAccount();
      }

      public void LoginUIController_OnAccountTextChanged(string text)
      {
        this.m_owner.LoginUIController_OnAccountTextChanged(text);
      }

      public void LoginUIController_OnSaveServerConfig()
      {
        this.m_owner.LoginUIController_OnSaveServerConfig();
      }

      public void LoginUIController_OnCloseAnnouncePanel()
      {
        this.m_owner.LoginUIController_OnCloseAnnouncePanel();
      }

      public void LoginUIController_OnOpenAnnouncePanel()
      {
        this.m_owner.LoginUIController_OnOpenAnnouncePanel();
      }

      public void LoginUIController_OnSelectServerClick()
      {
        this.m_owner.LoginUIController_OnSelectServerClick();
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
      {
        return this.m_owner.CollectAllStaticResDescForLoad();
      }

      public void ShowServerListUI(bool isShow)
      {
        this.m_owner.ShowServerListUI(isShow);
      }

      public IEnumerator KeepUpdatingServerList()
      {
        return this.m_owner.KeepUpdatingServerList();
      }

      public void LoginUIController_OnLogin()
      {
        this.m_owner.LoginUIController_OnLogin();
      }

      public void CreateCharacterUIController_OnCreate()
      {
        this.m_owner.CreateCharacterUIController_OnCreate();
      }

      public void CreateCharacterUIController_OnAutoName()
      {
        this.m_owner.CreateCharacterUIController_OnAutoName();
      }

      public static void NetWorkTransactionTask_OnReLoginBySession(Action action)
      {
        LoginUITask.NetWorkTransactionTask_OnReLoginBySession(action);
      }

      public static void NetWorkTransactionTask_ReturnToLoginUI(bool isDataDirty)
      {
        LoginUITask.NetWorkTransactionTask_ReturnToLoginUI(isDataDirty);
      }

      public static IEnumerator Co_StartLoginUITask()
      {
        return LoginUITask.Co_StartLoginUITask();
      }

      public static void ParseServerListText(string serverListText)
      {
        LoginUITask.ParseServerListText(serverListText);
      }

      public void UpdateServerList()
      {
        this.m_owner.UpdateServerList();
      }

      public static string ParseServerOpenDateTime(string dt)
      {
        return LoginUITask.ParseServerOpenDateTime(dt);
      }

      public static void NetWorkTransactionTask_OnEventShowUIWaiting(bool show)
      {
        LoginUITask.NetWorkTransactionTask_OnEventShowUIWaiting(show);
      }
    }
  }
}
