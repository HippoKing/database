﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.IOSARShowSceneController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.ProjectL.AR;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class IOSARShowSceneController : ARShowSceneController
  {
    [AutoBind("./ARCamera", AutoBindAttribute.InitState.NotInit, false)]
    private Camera m_camera;
    [AutoBind("./FocusSquare/FocusSquare", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_focusSquare;
    [AutoBind("./FocusSquare/FocusSquare/Quad", AutoBindAttribute.InitState.NotInit, false)]
    private Renderer m_focusSquareRenderer;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charNode;
    [AutoBind("./CharDraw", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charDrawNode;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGroupNode;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override ARPlaneTrace CreatePlaneTrace()
    {
      return (ARPlaneTrace) new IOSARPlaneTrace();
    }

    protected override GameObject focusSquare
    {
      get
      {
        return this.m_focusSquare;
      }
    }

    protected override GameObject charNode
    {
      get
      {
        return this.m_charNode;
      }
    }

    protected override GameObject charDrawNode
    {
      get
      {
        return this.m_charDrawNode;
      }
    }

    protected override GameObject charGroupNode
    {
      get
      {
        return this.m_charGroupNode;
      }
    }

    protected override Renderer focusSquareRenderer
    {
      get
      {
        return this.m_focusSquareRenderer;
      }
    }
  }
}
