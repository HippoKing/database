﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSoldierSkinUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroSoldierSkinUIController : UIControllerBase
  {
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rootGo;
    [AutoBind("./LayoutRoot/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollContent;
    [AutoBind("./LayoutRoot/ShowTogglePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_showToggleStateCtrl;
    [AutoBind("./LayoutRoot/ShowTogglePanel/HeroCurrentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_showHeroToggle;
    [AutoBind("./LayoutRoot/ShowTogglePanel/SoldierCurrentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_showSoliderToggle;
    [AutoBind("./ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelStateCtrl;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoTitleText;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoDescText;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/HeroInfoPanel/JobScrollView/Viewport/JobGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroJobContent;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/HeroInfoPanel/JobScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_infoHeroJobScrollRect;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/GetOn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSwitchButton;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/Unavailable", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoUnavailableButton;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/Buy", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoBuyButton;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/FromText/Title/DetailText ", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoFromDetailText;
    [AutoBind("./Prefab/Hero&SoldierSkinItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSoldierSkinItemPrefab;
    private Hero m_hero;
    private bool m_isFromHeroTab;
    private ConfigDataSoldierInfo m_soldierInfo;
    private HeroOrSoliderSkinUIController m_curSelectHeroSkinItemCtrl;
    private List<HeroOrSoliderSkinUIController> m_itemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroSoldierSkinUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateViewInHeroSoldierSkinHeroBooleanConfigDataSoldierInfo_hotfix;
    private LuaFunction m_LateUpdate_hotfix;
    private LuaFunction m_ShowHeroSkin_hotfix;
    private LuaFunction m_CreatDefaultHeroSkin_hotfix;
    private LuaFunction m_SetHeroInfoPanelHeroOrSoliderSkinUIController_hotfix;
    private LuaFunction m_SetHeroButtonStateHeroOrSoliderSkinUIController_hotfix;
    private LuaFunction m_ShowSoldierSkin_hotfix;
    private LuaFunction m_CreateDefaultSoldier_hotfix;
    private LuaFunction m_SetSoldierInfoPanelHeroOrSoliderSkinUIController_hotfix;
    private LuaFunction m_OnSkinItemClickHeroOrSoliderSkinUIController_hotfix;
    private LuaFunction m_OnShowHeroToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnShowSoldierToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnInfoUnavailableButtonClick_hotfix;
    private LuaFunction m_OnInfoBuyButtonClick_hotfix;
    private LuaFunction m_OnInfoSwitchButtonClick_hotfix;
    private LuaFunction m_ShowSwitchHeroCharSkinPanel_hotfix;
    private LuaFunction m_ShowSwitchDefaultHeroCharSkinPanel_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction`1_hotfix;
    private LuaFunction m_remove_EventOnReturnAction`1_hotfix;
    private LuaFunction m_add_EventOnBuyHeroSkinAction`1_hotfix;
    private LuaFunction m_remove_EventOnBuyHeroSkinAction`1_hotfix;
    private LuaFunction m_add_EventOnBuySoldierSkinAction`1_hotfix;
    private LuaFunction m_remove_EventOnBuySoldierSkinAction`1_hotfix;
    private LuaFunction m_add_EventOnTakeOffModelSkinAction`3_hotfix;
    private LuaFunction m_remove_EventOnTakeOffModelSkinAction`3_hotfix;
    private LuaFunction m_add_EventOnTakeOffSoldierSkinAction`2_hotfix;
    private LuaFunction m_remove_EventOnTakeOffSoldierSkinAction`2_hotfix;
    private LuaFunction m_add_EventOnWearModelSkinAction`4_hotfix;
    private LuaFunction m_remove_EventOnWearModelSkinAction`4_hotfix;
    private LuaFunction m_add_EventOnWearSoldierSkinAction`3_hotfix;
    private LuaFunction m_remove_EventOnWearSoldierSkinAction`3_hotfix;
    private LuaFunction m_add_EventOnWearCharSkinAction`2_hotfix;
    private LuaFunction m_remove_EventOnWearCharSkinAction`2_hotfix;
    private LuaFunction m_add_EventOnTakeOffCharSkinAction`1_hotfix;
    private LuaFunction m_remove_EventOnTakeOffCharSkinAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSoldierSkinUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroSoldierSkin(
      Hero hero,
      bool isFromHeroTab,
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeroSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreatDefaultHeroSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroInfoPanel(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroButtonState(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSoldierSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDefaultSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierInfoPanel(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinItemClick(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowHeroToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowSoldierToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoUnavailableButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoSwitchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchHeroCharSkinPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchDefaultHeroCharSkinPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuyHeroSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuySoldierSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnTakeOffModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnTakeOffSoldierSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int, Action> EventOnWearModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int> EventOnWearSoldierSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnWearCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTakeOffCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroSoldierSkinUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn(bool obj)
    {
      this.EventOnReturn = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuyHeroSkin(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuyHeroSkin(int obj)
    {
      this.EventOnBuyHeroSkin = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuySoldierSkin(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuySoldierSkin(int obj)
    {
      this.EventOnBuySoldierSkin = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTakeOffModelSkin(int arg1, int arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTakeOffModelSkin(int arg1, int arg2, Action arg3)
    {
      this.EventOnTakeOffModelSkin = (Action<int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTakeOffSoldierSkin(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTakeOffSoldierSkin(int arg1, int arg2)
    {
      this.EventOnTakeOffSoldierSkin = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWearModelSkin(int arg1, int arg2, int arg3, Action arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWearModelSkin(int arg1, int arg2, int arg3, Action arg4)
    {
      this.EventOnWearModelSkin = (Action<int, int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWearSoldierSkin(int arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWearSoldierSkin(int arg1, int arg2, int arg3)
    {
      this.EventOnWearSoldierSkin = (Action<int, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWearCharSkin(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWearCharSkin(int arg1, int arg2)
    {
      this.EventOnWearCharSkin = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTakeOffCharSkin(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTakeOffCharSkin(int obj)
    {
      this.EventOnTakeOffCharSkin = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroSoldierSkinUIController m_owner;

      public LuaExportHelper(HeroSoldierSkinUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn(bool obj)
      {
        this.m_owner.__callDele_EventOnReturn(obj);
      }

      public void __clearDele_EventOnReturn(bool obj)
      {
        this.m_owner.__clearDele_EventOnReturn(obj);
      }

      public void __callDele_EventOnBuyHeroSkin(int obj)
      {
        this.m_owner.__callDele_EventOnBuyHeroSkin(obj);
      }

      public void __clearDele_EventOnBuyHeroSkin(int obj)
      {
        this.m_owner.__clearDele_EventOnBuyHeroSkin(obj);
      }

      public void __callDele_EventOnBuySoldierSkin(int obj)
      {
        this.m_owner.__callDele_EventOnBuySoldierSkin(obj);
      }

      public void __clearDele_EventOnBuySoldierSkin(int obj)
      {
        this.m_owner.__clearDele_EventOnBuySoldierSkin(obj);
      }

      public void __callDele_EventOnTakeOffModelSkin(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnTakeOffModelSkin(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnTakeOffModelSkin(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnTakeOffModelSkin(arg1, arg2, arg3);
      }

      public void __callDele_EventOnTakeOffSoldierSkin(int arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnTakeOffSoldierSkin(arg1, arg2);
      }

      public void __clearDele_EventOnTakeOffSoldierSkin(int arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnTakeOffSoldierSkin(arg1, arg2);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callDele_EventOnWearModelSkin(int arg1, int arg2, int arg3, Action arg4)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __clearDele_EventOnWearModelSkin(int arg1, int arg2, int arg3, Action arg4)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callDele_EventOnWearSoldierSkin(int arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnWearSoldierSkin(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnWearSoldierSkin(int arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnWearSoldierSkin(arg1, arg2, arg3);
      }

      public void __callDele_EventOnWearCharSkin(int arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnWearCharSkin(arg1, arg2);
      }

      public void __clearDele_EventOnWearCharSkin(int arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnWearCharSkin(arg1, arg2);
      }

      public void __callDele_EventOnTakeOffCharSkin(int obj)
      {
        this.m_owner.__callDele_EventOnTakeOffCharSkin(obj);
      }

      public void __clearDele_EventOnTakeOffCharSkin(int obj)
      {
        this.m_owner.__clearDele_EventOnTakeOffCharSkin(obj);
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public GameObject m_rootGo
      {
        get
        {
          return this.m_owner.m_rootGo;
        }
        set
        {
          this.m_owner.m_rootGo = value;
        }
      }

      public GameObject m_scrollContent
      {
        get
        {
          return this.m_owner.m_scrollContent;
        }
        set
        {
          this.m_owner.m_scrollContent = value;
        }
      }

      public CommonUIStateController m_showToggleStateCtrl
      {
        get
        {
          return this.m_owner.m_showToggleStateCtrl;
        }
        set
        {
          this.m_owner.m_showToggleStateCtrl = value;
        }
      }

      public Toggle m_showHeroToggle
      {
        get
        {
          return this.m_owner.m_showHeroToggle;
        }
        set
        {
          this.m_owner.m_showHeroToggle = value;
        }
      }

      public Toggle m_showSoliderToggle
      {
        get
        {
          return this.m_owner.m_showSoliderToggle;
        }
        set
        {
          this.m_owner.m_showSoliderToggle = value;
        }
      }

      public CommonUIStateController m_infoPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_infoPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_infoPanelStateCtrl = value;
        }
      }

      public Text m_infoTitleText
      {
        get
        {
          return this.m_owner.m_infoTitleText;
        }
        set
        {
          this.m_owner.m_infoTitleText = value;
        }
      }

      public Text m_infoDescText
      {
        get
        {
          return this.m_owner.m_infoDescText;
        }
        set
        {
          this.m_owner.m_infoDescText = value;
        }
      }

      public GameObject m_infoHeroJobContent
      {
        get
        {
          return this.m_owner.m_infoHeroJobContent;
        }
        set
        {
          this.m_owner.m_infoHeroJobContent = value;
        }
      }

      public ScrollRect m_infoHeroJobScrollRect
      {
        get
        {
          return this.m_owner.m_infoHeroJobScrollRect;
        }
        set
        {
          this.m_owner.m_infoHeroJobScrollRect = value;
        }
      }

      public Button m_infoSwitchButton
      {
        get
        {
          return this.m_owner.m_infoSwitchButton;
        }
        set
        {
          this.m_owner.m_infoSwitchButton = value;
        }
      }

      public Button m_infoUnavailableButton
      {
        get
        {
          return this.m_owner.m_infoUnavailableButton;
        }
        set
        {
          this.m_owner.m_infoUnavailableButton = value;
        }
      }

      public Button m_infoBuyButton
      {
        get
        {
          return this.m_owner.m_infoBuyButton;
        }
        set
        {
          this.m_owner.m_infoBuyButton = value;
        }
      }

      public Text m_infoFromDetailText
      {
        get
        {
          return this.m_owner.m_infoFromDetailText;
        }
        set
        {
          this.m_owner.m_infoFromDetailText = value;
        }
      }

      public GameObject m_heroSoldierSkinItemPrefab
      {
        get
        {
          return this.m_owner.m_heroSoldierSkinItemPrefab;
        }
        set
        {
          this.m_owner.m_heroSoldierSkinItemPrefab = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public bool m_isFromHeroTab
      {
        get
        {
          return this.m_owner.m_isFromHeroTab;
        }
        set
        {
          this.m_owner.m_isFromHeroTab = value;
        }
      }

      public ConfigDataSoldierInfo m_soldierInfo
      {
        get
        {
          return this.m_owner.m_soldierInfo;
        }
        set
        {
          this.m_owner.m_soldierInfo = value;
        }
      }

      public HeroOrSoliderSkinUIController m_curSelectHeroSkinItemCtrl
      {
        get
        {
          return this.m_owner.m_curSelectHeroSkinItemCtrl;
        }
        set
        {
          this.m_owner.m_curSelectHeroSkinItemCtrl = value;
        }
      }

      public List<HeroOrSoliderSkinUIController> m_itemCtrlList
      {
        get
        {
          return this.m_owner.m_itemCtrlList;
        }
        set
        {
          this.m_owner.m_itemCtrlList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void LateUpdate()
      {
        this.m_owner.LateUpdate();
      }

      public void ShowHeroSkin()
      {
        this.m_owner.ShowHeroSkin();
      }

      public void CreatDefaultHeroSkin()
      {
        this.m_owner.CreatDefaultHeroSkin();
      }

      public void SetHeroInfoPanel(HeroOrSoliderSkinUIController ctrl)
      {
        this.m_owner.SetHeroInfoPanel(ctrl);
      }

      public void SetHeroButtonState(HeroOrSoliderSkinUIController ctrl)
      {
        this.m_owner.SetHeroButtonState(ctrl);
      }

      public void ShowSoldierSkin()
      {
        this.m_owner.ShowSoldierSkin();
      }

      public void CreateDefaultSoldier()
      {
        this.m_owner.CreateDefaultSoldier();
      }

      public void SetSoldierInfoPanel(HeroOrSoliderSkinUIController ctrl)
      {
        this.m_owner.SetSoldierInfoPanel(ctrl);
      }

      public void OnSkinItemClick(HeroOrSoliderSkinUIController ctrl)
      {
        this.m_owner.OnSkinItemClick(ctrl);
      }

      public void OnShowHeroToggleValueChanged(bool isOn)
      {
        this.m_owner.OnShowHeroToggleValueChanged(isOn);
      }

      public void OnShowSoldierToggleValueChanged(bool isOn)
      {
        this.m_owner.OnShowSoldierToggleValueChanged(isOn);
      }

      public void OnInfoUnavailableButtonClick()
      {
        this.m_owner.OnInfoUnavailableButtonClick();
      }

      public void OnInfoBuyButtonClick()
      {
        this.m_owner.OnInfoBuyButtonClick();
      }

      public void OnInfoSwitchButtonClick()
      {
        this.m_owner.OnInfoSwitchButtonClick();
      }

      public void ShowSwitchHeroCharSkinPanel()
      {
        this.m_owner.ShowSwitchHeroCharSkinPanel();
      }

      public void ShowSwitchDefaultHeroCharSkinPanel()
      {
        this.m_owner.ShowSwitchDefaultHeroCharSkinPanel();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }
    }
  }
}
