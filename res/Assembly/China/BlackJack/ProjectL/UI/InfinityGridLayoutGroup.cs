﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.InfinityGridLayoutGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  [RequireComponent(typeof (GridLayoutGroup))]
  [RequireComponent(typeof (ContentSizeFitter))]
  public class InfinityGridLayoutGroup : MonoBehaviour
  {
    [SerializeField]
    private int m_minAmount;
    [SerializeField]
    private int amount;
    private RectTransform rectTransform;
    private GridLayoutGroup gridLayoutGroup;
    private ContentSizeFitter contentSizeFitter;
    private ScrollRect scrollRect;
    private List<RectTransform> children;
    private Vector2 startPosition;
    public InfinityGridLayoutGroup.UpdateChildrenCallbackDelegate updateChildrenCallback;
    private int realIndex;
    private int realIndexUp;
    private bool hasInit;
    private Vector2 gridLayoutSize;
    private Vector2 gridLayoutPos;
    private Dictionary<Transform, Vector2> childsAnchoredPosition;
    private Dictionary<Transform, int> childsSiblingIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    public InfinityGridLayoutGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    public int MinAmount
    {
      get
      {
        return this.m_minAmount;
      }
      set
      {
        this.m_minAmount = value;
      }
    }

    private void Start()
    {
      this.StartCoroutine(this.InitChildren());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator InitChildren()
    {
      // ISSUE: unable to decompile the method.
    }

    private void ScrollCallback(Vector2 data)
    {
      this.UpdateChildren();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildren()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildrenCallback(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAmount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate void UpdateChildrenCallbackDelegate(int index, Transform trans);
  }
}
