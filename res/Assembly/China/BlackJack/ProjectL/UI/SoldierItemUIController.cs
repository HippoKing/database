﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SoldierItemUIController : UIControllerBase
  {
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./Attacking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_attackingObj;
    [AutoBind("./NotGet", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dontGetObj;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGrapgic;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_frameImage;
    [AutoBind("./SkinIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinIconImage;
    private bool m_isSoldierGet;
    private int m_soldierSkinId;
    private UISpineGraphic m_graphic;
    public ConfigDataJobConnectionInfo JobConnectionInfo;
    public ConfigDataSoldierInfo SoldierInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(
      ConfigDataSoldierInfo si,
      ConfigDataJobConnectionInfo jci,
      bool isSoliderGet,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetAttackingPanelActive(bool isShow)
    {
      this.m_attackingObj.SetActive(isShow);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackButtonActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFrameImgActive(bool isShow)
    {
      this.m_frameImage.SetActive(isShow);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataSoldierInfo soldierInfo,
      bool isGet,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SoldierItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataSoldierInfo> EventOnAttackButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
