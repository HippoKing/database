﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class CollectionComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionCollection m_collectionDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    [DoNotToLua]
    private CollectionComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_HandleTresureEventInt32ConfigDataEventInfo_hotfix;
    private LuaFunction m_HandleDialogEventConfigDataCollectionActivityWaypointInfoList`1Int32Int32Int32Int32_hotfix;
    private LuaFunction m_GetEventRewardsConfigDataEventInfo_hotfix;
    private LuaFunction m_RemoveEventList`1UInt64_hotfix;
    private LuaFunction m_IsEventTimeOutCollectionEvent_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IterateAvailableExchangeItems_hotfix;
    private LuaFunction m_CanExchangeItemUInt64Int32_hotfix;
    private LuaFunction m_ExchangeItemUInt64Int32_hotfix;
    private LuaFunction m_GetCollectionActivityInfoUInt64_hotfix;
    private LuaFunction m_GetActivityInstanceIdGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetCollectionActivityInfoGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetEventExpiredTimeInt32_hotfix;
    private LuaFunction m_GetCollectionActivityGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanMoveToWayPointInt32_hotfix;
    private LuaFunction m_MoveToWayPointInt32_hotfix;
    private LuaFunction m_CanHandleScenarioInt32_hotfix;
    private LuaFunction m_IsNextScenarioIdUInt64Int32_hotfix;
    private LuaFunction m_GetNextScenarioIdUInt64_hotfix;
    private LuaFunction m_CanAttackLevelGameFunctionTypeInt32Boolean_hotfix;
    private LuaFunction m_CanAttackScenarioLevelCollectionActivityInt32Int32_hotfix;
    private LuaFunction m_CanAttackChallengeLevelCollectionActivityInt32Int32_hotfix;
    private LuaFunction m_CanAttackLootLevelCollectionActivityInt32Int32Boolean_hotfix;
    private LuaFunction m__IsLootLevelUnlockCollectionActivityConfigDataCollectionActivityLootLevelInfoInt32_hotfix;
    private LuaFunction m_IsPrevLevelUnlockCollectionActivityList`1_hotfix;
    private LuaFunction m_IsScenarioFinishedCollectionActivityInt32_hotfix;
    private LuaFunction m_IsChallengeLevelFinishedCollectionActivityInt32_hotfix;
    private LuaFunction m_AttackLevelGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetAllUnlockedLootLevels_hotfix;
    private LuaFunction m_IsLootLevelUnlockInt32_hotfix;
    private LuaFunction m_AddFinishedLootLevelCollectionActivityInt32_hotfix;
    private LuaFunction m_GetAllEventsByInstanceIdUInt64_hotfix;
    private LuaFunction m_SetCommonSuccessLevelCollectionActivityGameFunctionTypeInt32List`1List`1Boolean_hotfix;
    private LuaFunction m_SendAddEventList`1UInt64_hotfix;
    private LuaFunction m_SendDeleteEventList`1UInt64_hotfix;
    private LuaFunction m_CollectionEventHandleUInt64Int32Int32_hotfix;
    private LuaFunction m_CanHandleCollectionEventUInt64Int32Int32_hotfix;
    private LuaFunction m_HandleCollectionEventUInt64Int32ConfigDataEventInfoConfigDataCollectionActivityWaypointInfo_hotfix;
    private LuaFunction m_AttackEventUInt64Int32Int32_hotfix;
    private LuaFunction m_CanAttackEventUInt64Int32Int32_hotfix;
    private LuaFunction m_CanAttackCollectionEventConfigDataEventInfo_hotfix;
    private LuaFunction m_AddCollectionEventList`1UInt64_hotfix;
    private LuaFunction m_DeleteCollectionEventList`1UInt64_hotfix;
    private LuaFunction m_OnEventCompleteUInt64Int32Int32ConfigDataEventInfo_hotfix;
    private LuaFunction m_SetBattleEventSuccessfulConfigDataCollectionActivityWaypointInfoList`1_hotfix;
    private LuaFunction m_SortByExpiredTimeAscendList`1_hotfix;
    private LuaFunction m_TryDeleteCompleteTimeCollectionActivityInt32Int32_hotfix;
    private LuaFunction m_TryHandleCollectionEventCollectionActivityInt32_hotfix;
    private LuaFunction m_TryDeleteCollectionEventChallengeLevelCollectionActivityInt32_hotfix;
    private LuaFunction m_TryProduceCollectionEventCollectionActivityInt32_hotfix;
    private LuaFunction m_GetCollectionActivityWayPointStateCollectionActivityInt32_hotfix;
    private LuaFunction m_IsNewCollectionActivityOpenedInt32_hotfix;
    private LuaFunction m_GetNewFinishedScenarioLevelInfosCollectionActivity_hotfix;
    private LuaFunction m_TryDeleteCollectionEventScenarioCollectionActivityInt32_hotfix;
    private LuaFunction m_AddScoreCollectionActivityInt32List`1GameFunctionType_hotfix;
    private LuaFunction m_CalculateScoreConfigDataCollectionActivityInfoInt32List`1_hotfix;
    private LuaFunction m_CalculateAllHeroBonusConfigDataCollectionActivityInfoList`1_hotfix;
    private LuaFunction m_FindCollectionActivityUInt64_hotfix;
    private LuaFunction m_RemoveCollectionActivityOperationalActivityBase_hotfix;
    private LuaFunction m_AddCollectionActivityOperationalActivityBase_hotfix;
    private LuaFunction m_add_CompleteCollectionEventMissionEventAction`1_hotfix;
    private LuaFunction m_remove_CompleteCollectionEventMissionEventAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleDialogEvent(
      ConfigDataCollectionActivityWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int expReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void RemoveEvent(List<CollectionEvent> Events, ulong activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventTimeOut(CollectionEvent Event)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> IterateAvailableExchangeItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo(
      ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetActivityInstanceId(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo(
      GameFunctionType typeId,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected long GetEventExpiredTime(int EventinfoId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity GetCollectionActivity(
      GameFunctionType typeId,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanMoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanHandleScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsNextScenarioId(ulong activityInstanceId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextScenarioId(ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackLevel(GameFunctionType typeId, int levelId, bool team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackScenarioLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackChallengeLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLootLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays,
      bool team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int _IsLootLevelUnlock(
      CollectionActivity collectionActivity,
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPrevLevelUnlock(CollectionActivity activity, List<LevelInfo> PrevLevels)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(CollectionActivity collectionActivity, int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelFinished(CollectionActivity collectionActivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackLevel(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLootLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelUnlock(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFinishedLootLevel(CollectionActivity collectionActivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> GetAllEventsByInstanceId(
      ulong ActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessLevel(
      CollectionActivity collectionActivity,
      GameFunctionType typeId,
      int levelId,
      List<int> heroes,
      List<int> allHeroes,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SendAddEvent(List<CollectionEvent> events, ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SendDeleteEvent(List<CollectionEvent> events, ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CollectionEventHandle(ulong activityId, int EventId, int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHandleCollectionEvent(ulong activityId, int wayPointId, int EventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleCollectionEvent(
      ulong activityId,
      int EventId,
      ConfigDataEventInfo eventInfo,
      ConfigDataCollectionActivityWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackEvent(ulong activityId, int wayPointId, int EventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEvent(ulong activityId, int wayPointId, int EventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCollectionEvent(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeleteCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnEventComplete(
      ulong activityId,
      int EventId,
      int wayPointId,
      ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetBattleEventSuccessful(
      ConfigDataCollectionActivityWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SortByExpiredTimeAscend(List<CollectionEvent> events)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryDeleteCompleteTime(
      CollectionActivity collectionActivity,
      int EventId,
      int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryHandleCollectionEvent(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryDeleteCollectionEventChallengeLevel(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryProduceCollectionEvent(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypointStateType GetCollectionActivityWayPointState(
      CollectionActivity collectionActivity,
      int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNewCollectionActivityOpened(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataCollectionActivityScenarioLevelInfo> GetNewFinishedScenarioLevelInfos(
      CollectionActivity collectionActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryDeleteCollectionEventScenario(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddScore(
      CollectionActivity collectionActivity,
      int scoreBase,
      List<int> allHeroes,
      GameFunctionType causeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateScore(
      ConfigDataCollectionActivityInfo info,
      int scoreBase,
      List<int> allHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateAllHeroBonus(ConfigDataCollectionActivityInfo info, List<int> allHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity FindCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> CompleteCollectionEventMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteCollectionEventMissionEvent(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteCollectionEventMissionEvent(bool obj)
    {
      this.CompleteCollectionEventMissionEvent = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionComponentCommon m_owner;

      public LuaExportHelper(CollectionComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteCollectionEventMissionEvent(bool obj)
      {
        this.m_owner.__callDele_CompleteCollectionEventMissionEvent(obj);
      }

      public void __clearDele_CompleteCollectionEventMissionEvent(bool obj)
      {
        this.m_owner.__clearDele_CompleteCollectionEventMissionEvent(obj);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionCollection m_collectionDS
      {
        get
        {
          return this.m_owner.m_collectionDS;
        }
        set
        {
          this.m_owner.m_collectionDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public OperationalActivityCompomentCommon m_operationalActivity
      {
        get
        {
          return this.m_owner.m_operationalActivity;
        }
        set
        {
          this.m_owner.m_operationalActivity = value;
        }
      }

      public int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.HandleTresureEvent(wayPointId, eventInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int HandleDialogEvent(
        ConfigDataCollectionActivityWaypointInfo wayPointInfo,
        List<Goods> itemRewards,
        int expReward,
        int goldReward,
        int energyCost,
        int scenarioId)
      {
        // ISSUE: unable to decompile the method.
      }

      public List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.GetEventRewards(eventInfo);
      }

      public long GetEventExpiredTime(int EventinfoId)
      {
        return this.m_owner.GetEventExpiredTime(EventinfoId);
      }

      public int CanAttackScenarioLevel(
        CollectionActivity collectionActivity,
        int levelId,
        int deltaDays)
      {
        return this.m_owner.CanAttackScenarioLevel(collectionActivity, levelId, deltaDays);
      }

      public int CanAttackChallengeLevel(
        CollectionActivity collectionActivity,
        int levelId,
        int deltaDays)
      {
        return this.m_owner.CanAttackChallengeLevel(collectionActivity, levelId, deltaDays);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int CanAttackLootLevel(
        CollectionActivity collectionActivity,
        int levelId,
        int deltaDays,
        bool team)
      {
        // ISSUE: unable to decompile the method.
      }

      public int _IsLootLevelUnlock(
        CollectionActivity collectionActivity,
        ConfigDataCollectionActivityLootLevelInfo levelInfo,
        int deltaDays)
      {
        return this.m_owner._IsLootLevelUnlock(collectionActivity, levelInfo, deltaDays);
      }

      public bool IsPrevLevelUnlock(CollectionActivity activity, List<LevelInfo> PrevLevels)
      {
        return this.m_owner.IsPrevLevelUnlock(activity, PrevLevels);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddScore(
        CollectionActivity collectionActivity,
        int scoreBase,
        List<int> allHeroes,
        GameFunctionType causeId)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
