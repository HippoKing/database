﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  [Serializable]
  public class ChatMessage
  {
    public ChatChannel ChannelId { get; set; }

    public ChatSrcType ChatSrcType { get; set; }

    public string SrcName { get; set; }

    public int AvatarId { get; set; }

    public ChatContentType ChatContentType { get; set; }

    public int SrcPlayerLevel { get; set; }

    public string SrcGameUserID { get; set; }

    public DateTime SendTime { get; set; }

    public string DestGameUserId { get; set; }

    public string DestChatGroupId { get; set; }

    public int Title { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf ToPbChatMessage()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
