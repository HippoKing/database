﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.SelectCardComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class SelectCardComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    protected DataSectionSelectCard m_selectCardDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "SelectCard";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    public void RemoveCardPool(int cardPool)
    {
      this.m_selectCardDS.RemoveCardPool(cardPool);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitCardPoolConfigs(List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsActivityCardPoolOnActivityTime(int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public OperationalActivityBase FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      return this.m_operationalActivity.FindOperationalActivityByActivityCardPoolId(activityCardPoolId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllOpenActivityCardPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsUsedOutActivityCardPoolSelectOpportunity(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool GetCardPoolById(int cardPoolId, bool ignoreActivityCheck = true)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsTicketsEnough(int ticketId, int ticketCount)
    {
      return this.m_bag.IsBagItemEnough(GoodsType.GoodsType_Item, ticketId, ticketCount);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCrystalCount(
      CardPool cardPool,
      bool isSingleSelect,
      out bool isTenSelectDiscount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectCardUseTickets(CardPool cardPool, bool isSingleSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectCardUseCrystal(CardPool cardPool, bool isSingleSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanSelectCard(CardPool cardPool, bool isSingleSelect, bool isUsingTickets)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsCrystalOrActivityCardPool(CardPoolType cardPoolType)
    {
      if (cardPoolType != CardPoolType.CardPoolType_CrystalCardPool)
        return cardPoolType == CardPoolType.CardPoolType_ActivityCardPool;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int CanUseTicketSelectCard(CardPool cardPool, int ticketNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectCardFinished(int cardPoolId, bool isSingleSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectCardSpendTicketsOrCrystal(
      CardPool cardPool,
      bool isSingleSelect,
      bool isUsingTickets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SelectHeroCard(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> SelectCardMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> SummonHeroMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
