﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionAnnouncement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionAnnouncement : DataSection
  {
    private List<Announcement> m_announcements;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionAnnouncement()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.m_announcements.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitAnnouncements(List<Announcement> announcements)
    {
      this.m_announcements.AddRange((IEnumerable<Announcement>) announcements);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnnouncements(List<Announcement> announcements)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetClientAnnouncementCurrentVersion(uint currentVersion)
    {
      this.ClientCurrentVersion = currentVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerAnnouncementCurrentVersion(int serverCurrentVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAnnouncement(Announcement announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> FindAnnouncementsByCondition(
      Predicate<Announcement> isConditionMatched)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Announcement FindAnnouncementsByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnnouncement(Announcement announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    public uint ClientCurrentVersion { get; set; }
  }
}
