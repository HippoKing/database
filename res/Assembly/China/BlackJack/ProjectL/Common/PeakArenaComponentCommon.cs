﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class PeakArenaComponentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    public DataSectionPeakArena m_peakArenaDS;
    [DoNotToLua]
    private PeakArenaComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_CanBetPeakArenaInt32StringInt32_hotfix;
    private LuaFunction m_BetPeakArenaInt32Int32StringInt32_hotfix;
    private LuaFunction m_SettlePeakArenaMatchupBetRewardInt32_hotfix;
    private LuaFunction m_IsTimeForBettedConfigDataPeakArenaSeasonInfoDateTime_hotfix;
    private LuaFunction m_IsTimeForBuyingJettonDateTime_hotfix;
    private LuaFunction m_GetCurrentSeasonBetInfo_hotfix;
    private LuaFunction m_GetCurrentSeasonConfigInfo_hotfix;
    private LuaFunction m_PeakArenaDanUpdateInt32_hotfix;
    private LuaFunction m_PeakArenaBattleFinishRealTimePVPModeBooleanList`1_hotfix;
    private LuaFunction m_PeakArenaBattleStartRealTimePVPMode_hotfix;
    private LuaFunction m_IsPeakArenaUnlocked_hotfix;
    private LuaFunction m_IsPeakArenaMatchmakingAvailableRealTimePVPMode_hotfix;
    private LuaFunction m_OnFlushPeakArena_hotfix;
    private LuaFunction m_OnFlushPeakArenaBet_hotfix;
    private LuaFunction m_SaveReportBattleReportHeadString_hotfix;
    private LuaFunction m_IsBattleReportFull_hotfix;
    private LuaFunction m_SaveStatisticsInt32Boolean_hotfix;
    private LuaFunction m_GetBattleReport_hotfix;
    private LuaFunction m_CheckAcquireWinsBonusInt32_hotfix;
    private LuaFunction m_AcquireWinsBonusInt32_hotfix;
    private LuaFunction m_GetConsecutiveWinsRealTimePVPMode_hotfix;
    private LuaFunction m_GetConsecutiveLossesRealTimePVPMode_hotfix;
    private LuaFunction m_GetLadderCareerMatchStats_hotfix;
    private LuaFunction m_SetupBattleTeamPeakArenaBattleTeam_hotfix;
    private LuaFunction m_GetBattleTeam_hotfix;
    private LuaFunction m_BanDateTime_hotfix;
    private LuaFunction m_IsBannedDateTime_hotfix;
    private LuaFunction m_GetBattleReportNameRecords_hotfix;
    private LuaFunction m_AddBattleReportNameRecordUInt64String_hotfix;
    private LuaFunction m_ExistBattleReportNameRecordUInt64_hotfix;
    private LuaFunction m_GetBattleReportNameUInt64_hotfix;
    private LuaFunction m_CanChangeBattleReportNameUInt64String_hotfix;
    private LuaFunction m_ChangeBattleReportNameUInt64String_hotfix;
    private LuaFunction m_SelectHeroSkillInPublicPoolsInt32List`1_hotfix;
    private LuaFunction m_SelectHiredHeroSoldierInt32Int32_hotfix;
    private LuaFunction m_IsBattleTeamSetupDone_hotfix;
    private LuaFunction m_IsInPreRankingState_hotfix;
    private LuaFunction m_IsLastPreRankingGameJustFinished_hotfix;
    private LuaFunction m_GetSeasonEndTimeInt32_hotfix;
    private LuaFunction m_GetRegularSeasonEndTimeInt32_hotfix;
    private LuaFunction m_GetDanConfigByScoreInt32_hotfix;
    private LuaFunction m_GetPrevDanConfigByScoreInt32_hotfix;
    private LuaFunction m_GetDisplayDanInt32_hotfix;
    private LuaFunction m_GetNextDisplayDanConfigInt32_hotfix;
    private LuaFunction m_GetMatchmakingDanInt32_hotfix;
    private LuaFunction m_GetScoreThresholdAfterSingleMatchInt32Int32_Int32__hotfix;
    private LuaFunction m_GetAvailableRegularSeasonRewardsInt32Int32_hotfix;
    private LuaFunction m_CanGetRegularSeasonRewardsInt32Int32Int32_hotfix;
    private LuaFunction m_SetLiveRoomIdUInt64_hotfix;
    private LuaFunction m_SetNoticeEnterPlayOffRaceBoolean_hotfix;
    private LuaFunction m_IsOnLiveRoom_hotfix;
    private LuaFunction m_GetLiveRoomId_hotfix;
    private LuaFunction m_get_m_configDataLoader_hotfix;
    private LuaFunction m_set_m_configDataLoaderIConfigDataLoader_hotfix;
    private LuaFunction m_add_PeakArenaBattleStartMissionEventAction`1_hotfix;
    private LuaFunction m_remove_PeakArenaBattleStartMissionEventAction`1_hotfix;
    private LuaFunction m_add_PeakArenaBattleFinishMissionEventAction`2_hotfix;
    private LuaFunction m_remove_PeakArenaBattleFinishMissionEventAction`2_hotfix;
    private LuaFunction m_add_PeakArenaDanUpdateMissionEventAction`1_hotfix;
    private LuaFunction m_remove_PeakArenaDanUpdateMissionEventAction`1_hotfix;
    private LuaFunction m_get_BattleReportInited_hotfix;
    private LuaFunction m_set_BattleReportInitedBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBetPeakArena(int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetPeakArena(int seasonId, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SettlePeakArenaMatchupBetReward(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTimeForBetted(ConfigDataPeakArenaSeasonInfo seaonInfo, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTimeForBuyingJetton(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRoundTimeForBet(
      DateTime currentTime,
      ConfigDataPeakArenaSeasonInfo seaonInfo,
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBetInfo GetCurrentSeasonBetInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo GetCurrentSeasonConfigInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaDanUpdate(int dan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaBattleFinish(RealTimePVPMode mode, bool win, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaBattleStart(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsPeakArenaUnlocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int IsPeakArenaMatchmakingAvailable(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnFlushPeakArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFlushPeakArenaBet()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveReport(BattleReportHead report, string reportName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBattleReportFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveStatistics(int Type, bool Win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleReportHead> GetBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckAcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetConsecutiveWins(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetConsecutiveLosses(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats GetLadderCareerMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SetupBattleTeam(PeakArenaBattleTeam BattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleTeam GetBattleTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaBattleReportNameRecord> GetBattleReportNameRecords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleReportNameRecord(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistBattleReportNameRecord(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportName(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangeBattleReportName(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ChangeBattleReportName(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SelectHeroSkillInPublicPools(int heroId, List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SelectHiredHeroSoldier(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsBattleTeamSetupDone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInPreRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLastPreRankingGameJustFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetRegularSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaDanInfo GetDanConfigByScore(
      int Score,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetDanConfigByScore(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaDanInfo GetPrevDanConfigByScore(
      int Score,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetPrevDanConfigByScore(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int GetDisplayDan(int Score, IConfigDataLoader configDataLoader)
    {
      return PeakArenaComponentCommon.GetDanConfigByScore(Score, configDataLoader).DisplayDan;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDisplayDan(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaDanInfo GetNextDisplayDanConfig(
      int Score,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetNextDisplayDanConfig(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMatchmakingDan(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetScoreThresholdAfterSingleMatch(
      int CurrentScore,
      bool IsPreRankingState,
      IConfigDataLoader configDataLoader,
      out int InclusiveMinScore,
      out int InclusiveMaxScore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetScoreThresholdAfterSingleMatch(
      int CurrentScore,
      out int InclusiveMinScore,
      out int InclusiveMaxScore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAvailableRegularSeasonRewards(int CurrentSeason, int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetRegularSeasonRewards(int CurrentSeason, int Score, int RewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetCurrentPlayOffRound(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetCurrentPlayOffMatchStartTime(
      DateTime dt,
      IConfigDataLoader configDataLoader,
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaSeasonInfo GetLastSeason(
      DateTime dt,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaSeasonInfo GetNextSeason(
      DateTime dt,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaSeasonInfo GetSeason(
      DateTime dt,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRegularSeasonEnd(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInPlayOffMatchPeriod(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInPlayOffMatchDay(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetPlayOffRoundByStage(string stage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLiveRoomId(ulong liveRoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNoticeEnterPlayOffRace(bool notice)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOnLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetLiveRoomId()
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPMode> PeakArenaBattleStartMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPMode, bool> PeakArenaBattleFinishMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> PeakArenaDanUpdateMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool BattleReportInited
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_PeakArenaBattleStartMissionEvent(RealTimePVPMode obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_PeakArenaBattleStartMissionEvent(RealTimePVPMode obj)
    {
      this.PeakArenaBattleStartMissionEvent = (Action<RealTimePVPMode>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_PeakArenaBattleFinishMissionEvent(RealTimePVPMode arg1, bool arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_PeakArenaBattleFinishMissionEvent(RealTimePVPMode arg1, bool arg2)
    {
      this.PeakArenaBattleFinishMissionEvent = (Action<RealTimePVPMode, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_PeakArenaDanUpdateMissionEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_PeakArenaDanUpdateMissionEvent(int obj)
    {
      this.PeakArenaDanUpdateMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaComponentCommon m_owner;

      public LuaExportHelper(PeakArenaComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_PeakArenaBattleStartMissionEvent(RealTimePVPMode obj)
      {
        this.m_owner.__callDele_PeakArenaBattleStartMissionEvent(obj);
      }

      public void __clearDele_PeakArenaBattleStartMissionEvent(RealTimePVPMode obj)
      {
        this.m_owner.__clearDele_PeakArenaBattleStartMissionEvent(obj);
      }

      public void __callDele_PeakArenaBattleFinishMissionEvent(RealTimePVPMode arg1, bool arg2)
      {
        this.m_owner.__callDele_PeakArenaBattleFinishMissionEvent(arg1, arg2);
      }

      public void __clearDele_PeakArenaBattleFinishMissionEvent(RealTimePVPMode arg1, bool arg2)
      {
        this.m_owner.__clearDele_PeakArenaBattleFinishMissionEvent(arg1, arg2);
      }

      public void __callDele_PeakArenaDanUpdateMissionEvent(int obj)
      {
        this.m_owner.__callDele_PeakArenaDanUpdateMissionEvent(obj);
      }

      public void __clearDele_PeakArenaDanUpdateMissionEvent(int obj)
      {
        this.m_owner.__clearDele_PeakArenaDanUpdateMissionEvent(obj);
      }

      public IConfigDataLoader _configDataLoader
      {
        get
        {
          return this.m_owner._configDataLoader;
        }
        set
        {
          this.m_owner._configDataLoader = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnFlushPeakArena()
      {
        this.m_owner.OnFlushPeakArena();
      }

      public void OnFlushPeakArenaBet()
      {
        this.m_owner.OnFlushPeakArenaBet();
      }

      public bool IsBattleReportFull()
      {
        return this.m_owner.IsBattleReportFull();
      }
    }
  }
}
