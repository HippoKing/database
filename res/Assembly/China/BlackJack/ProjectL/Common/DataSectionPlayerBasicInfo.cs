﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionPlayerBasicInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.UtilityTools;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionPlayerBasicInfo : DataSection
  {
    public List<int> RewardedDialogIds;
    public int PlayerLevel;
    public int RechargeCrystal;
    public int Gold;
    public int Crystal;
    public BitArray GuideCompleteFlags;
    public DateTime EnergyFlushTime;
    public DateTime CreateTime;
    public DateTime CreateTimeUtc;
    public DateTime LogoutTime;
    public DateTime LoginTime;
    public DateTime LastSignTime;
    public DateTime RefluxBeginTime;
    public int BuyEngryNums;
    public int BuyArenaTicketsNums;
    public DateTime NextFlushPlayerActionTime;
    public int ArenaTickets;
    public int ArenaHonour;
    public int RealTimePVPHonor;
    public int FriendshipPoints;
    public bool OpenGameRating;
    public int SkinTickets;
    public int MemoryEssence;
    public int MithralStone;
    public int BrillianceMithralStone;
    public int GuildMedal;
    public int ChallengePoint;
    public int FashionPoint;
    public bool MemoryStoreOpen;
    public int BuyJettonNums;
    public int Title;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPlayerBasicInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetRefluxBeginTime(DateTime refluxBeginTime)
    {
      this.RefluxBeginTime = refluxBeginTime;
      this.SetDirty(true);
    }

    public void SetRefluxEndTime(DateTime refluxEndTime)
    {
      this.RefluxBeginTime = refluxEndTime;
      this.SetDirty(true);
    }

    public void SetPlayerExp(int currentExp)
    {
      this.Exp = currentExp;
      this.SetDirty(true);
    }

    public void ChangePlayerName(string newName)
    {
      this.PlayerName = newName;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerLevelUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddRechargedCrystal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long AddRechargeRMB(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddGold(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddCrystal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsUserGuideCompleted(int stepId)
    {
      return this.GuideCompleteFlags.IsBitSet(stepId);
    }

    public void SetEnergy(int energy)
    {
      this.Energy = energy;
      this.SetDirty(true);
    }

    public void InitSignDays(int signDays)
    {
      this.SignDays = signDays;
    }

    public void ResetSignDays()
    {
      this.SignDays = 0;
      this.SetDirty(true);
    }

    public void SetSignDays(int days)
    {
      this.SignDays = days;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSignDays()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetLastSignTime(DateTime time)
    {
      this.LastSignTime = time;
      this.SetDirty(true);
    }

    public void ResetBuyEngryNums()
    {
      this.BuyEngryNums = 0;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyEngry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ResetBuyArenaTicketsNums()
    {
      this.BuyArenaTicketsNums = 0;
      this.SetDirty(true);
    }

    public void SetNextPlayerActionFlushTime(DateTime setTime)
    {
      this.NextFlushPlayerActionTime = setTime;
      this.SetDirty(true);
    }

    public void SetArenaTickets(int nums)
    {
      this.ArenaTickets = nums;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaTickets(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddMemoryEssence(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetMemoryEssence(int nums)
    {
      this.MemoryEssence = nums;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddBrillianceMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddGuildMedal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddArenaHonour(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddRealTimePVPHonor(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddFriendshipPoints(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddChallengePoint(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddFashionPoint(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetEnergyFlushTime(DateTime time)
    {
      this.EnergyFlushTime = time;
      this.SetDirty(true);
    }

    public void SetHeadIcon(int headIcon)
    {
      this.HeadIcon = headIcon;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddSkinTickets(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetMemoryStoreOpenStatus(bool open)
    {
      this.MemoryStoreOpen = open;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuyJetton(int nums = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ResetBuyJettonNums()
    {
      this.BuyJettonNums = 0;
      this.SetDirty(true);
    }

    public void SetTitle(int id)
    {
      this.Title = id;
      this.SetDirty(true);
    }

    public bool IsDialogRewarded(int dialogId)
    {
      return this.RewardedDialogIds.Contains(dialogId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRewardedDialog(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId { get; set; }

    public string PlayerName { get; set; }

    public long RechargeRMB { get; set; }

    public int Energy { get; set; }

    public int Exp { get; set; }

    public int SignDays { get; set; }

    public int HeadIcon { get; set; }
  }
}
