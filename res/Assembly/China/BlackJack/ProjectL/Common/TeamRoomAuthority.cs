﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoomAuthority
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.ProjectL.Common
{
  public enum TeamRoomAuthority
  {
    None = 0,
    AllPublic = 1,
    FriendAndGuild = 2,
    NonPublic = 3,
    GuildMassiveCombat = 4,
    All = 100, // 0x00000064
  }
}
