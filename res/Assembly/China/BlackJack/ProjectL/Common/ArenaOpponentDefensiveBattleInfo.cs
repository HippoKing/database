﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaOpponentDefensiveBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaOpponentDefensiveBattleInfo
  {
    public ArenaPlayerDefensiveTeamSnapshot BattleTeamSnapshot;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaBattleReportStatus Status { get; set; }

    public string OpponentUserId { get; set; }

    public int BattleRandomSeed { get; set; }

    public long BattleExpiredTime { get; set; }

    public int ArenaOpponentPointZoneId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaOpponentDefensiveBattleInfo PBDefensiveBattleInfoToDefensiveBattleInfo(
      ProArenaDefensiveBattleInfo pbDefensiveBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaDefensiveBattleInfo DefensiveBattleInfoToPBDefensiveBattleInfo(
      ArenaOpponentDefensiveBattleInfo defensiveBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
