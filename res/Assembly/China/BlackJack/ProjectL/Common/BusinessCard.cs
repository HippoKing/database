﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BusinessCard
  {
    public List<BattleHero> Heroes;
    public List<TrainingTech> Techs;
    public List<BattleHero> MostSkilledHeroes;
    public List<PeakArenaBattleReportNameRecord> HeroicMomentBattleReportNames;
    public List<BattleReportHead> HeroicMomentBattleReportSummaries;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCard()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId { get; set; }

    public string Name { get; set; }

    public int Level { get; set; }

    public int HeadIcon { get; set; }

    public int ArenaPoints { get; set; }

    public int Likes { get; set; }

    public bool IsOnLine { get; set; }

    public int Title { get; set; }

    public BusinessCardInfoSet SetInfo { get; set; }

    public BusinessCardStatisticalData StatisticalData { get; set; }

    public BusinessCardPeakArenaData PeakArenaData { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCard ToProtocol(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCard FromProtocol(ProBusinessCard pbBusinessCard)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
