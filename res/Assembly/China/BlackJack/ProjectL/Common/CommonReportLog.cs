﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CommonReportLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CommonReportLog
  {
    private static string RandomEventFunctionHeader = "###############RandomEventFunction";

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventLevelZonesReportLog(
      List<RandomEventLevelZone> zones,
      string logHeader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventLevelZoneReportLog(RandomEventLevelZone zone, string zoneName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(RandomEventGroup group)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventsReportLog(List<RandomEvent> randomEvents, string optionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventReportLog(RandomEvent randomEvent, string optionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string IntListReportLog(List<int> list, string listName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string InitDictionary(Dictionary<int, int> dict, string dictName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string StringDictionaryReportLog(Dictionary<string, int> dict)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string WayPointReportLog(
      List<ConfigDataWaypointInfo> wayPointInfos,
      Dictionary<int, WayPointStatus> wayPointStates,
      string functionType,
      string optionType,
      string selectStandard = "NULL")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BattleReport(BattleReportLog report, string owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Message(List<BattleActorSetup> team, string teamName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(BattleActorSetup actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(CommonBattleProperty[] properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(List<BattlePlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(BattlePlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BattleRoomBattle(BattleRoomBattleLog log, string owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Message(List<BattleCommand> commands, string ownerName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Message(BPStageCommand command, string ownerName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(BattleCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(List<int> battleChecksums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GoodsListReportLog(List<Goods> goodsList, string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ProGoodsListReport(List<ProGoods> pbGoodsList, string desc)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
