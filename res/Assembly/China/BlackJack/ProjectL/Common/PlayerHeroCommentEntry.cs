﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerHeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PlayerHeroCommentEntry
  {
    public List<ulong> CommentedEntryInstanceIds;
    public HashSet<ulong> PraisedEntryInstanceIds;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId { get; set; }

    public int CommentedNums { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPlayerHeroCommentEntry PlayerHeroCommentEntryToPBPlayerHeroCommentEntry(
      PlayerHeroCommentEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerHeroCommentEntry PBPlayerHeroCommentEntryToPlayerHeroCommentEntry(
      ProPlayerHeroCommentEntry pbEntry)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
