﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CollectionActivity
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<CollectionEvent> CollectionEvents { get; set; }

    public ulong ActivityInstanceId { get; set; }

    public List<int> FinishedChallengeLevelIds { get; set; }

    public int LastFinishedScenarioId { get; set; }

    public List<int> FinishedLootLevelIds { get; set; }

    public int CurrentWayPointId { get; set; }

    public int Score { get; set; }

    public List<CollectionActivityPlayerExchangeInfo> ExchangeInfoList { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProCollectionActivity ToPb(CollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CollectionActivity FromPb(ProCollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
