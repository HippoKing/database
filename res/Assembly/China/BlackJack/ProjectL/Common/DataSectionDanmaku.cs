﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionDanmaku
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionDanmaku : DataSection
  {
    public DateTime m_bannedTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.m_bannedTime = DateTime.MinValue;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Ban(DateTime bannedTime)
    {
      this.m_bannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
