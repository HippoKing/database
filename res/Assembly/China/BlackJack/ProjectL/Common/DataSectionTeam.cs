﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionTeam : DataSection
  {
    private Dictionary<string, TeamRoomInviteInfo> m_inviteInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.ClearInviteInfos();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearInviteInfos()
    {
      this.m_inviteInfos.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAInviteInfo(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInvited(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomInviteInfo> GetInviteInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string BuildInviteInfoKey(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetTeamRoomInviteInfo(TeamRoomInviteInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    public int RoomId { get; set; }

    public GameFunctionType GameFunctionTypeId { get; set; }

    public int LocationId { get; set; }

    public GameFunctionType WaitingFunctionTypeId { get; set; }

    public int WaitingLocationId { get; set; }

    public DateTime LastInviteSendTime { get; set; }
  }
}
