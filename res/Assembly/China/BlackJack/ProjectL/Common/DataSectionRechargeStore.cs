﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRechargeStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionRechargeStore : DataSection
  {
    private HashSet<int> m_boughtGoodsList;
    public Dictionary<int, DateTime> m_banBuyingGoodsList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRechargeStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBoughtGoodsList(List<int> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBoughtGoodsList()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsGoodsBought(int goodsId)
    {
      return this.m_boughtGoodsList.Contains(goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<int> GetBoughtGoodsList()
    {
      return this.m_boughtGoodsList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsOnBanBuyingPeriod(int goodsId, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanBuyingGoodsTime(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitmBanBuyingGoodsList(Dictionary<int, DateTime> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, DateTime> BanBuyingGoodsList
    {
      get
      {
        return this.m_banBuyingGoodsList;
      }
    }

    public int CurrentId { get; set; }
  }
}
