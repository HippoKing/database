﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.OperationalActivityBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class OperationalActivityBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string ToInfoString()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void ToPBNtf(DSOperationalActivityNtf ntf)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProOperationalActivityBasicInfo ToPBOperationalActivityBasicData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOperationalActivityTime(
      DateTime operationStartTime,
      DateTime operationEndTime,
      DateTime gainRewardEndTime,
      DateTime operationShowTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInOperationPeriod(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInOperationShowPeriod(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInGainRewardPeriod(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsExpiredOperationalActivity(DateTime currentTime)
    {
      return currentTime >= this.GetExpiredTime();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetExpiredTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetInitialTimeInOneDay(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime OperationShowTime { get; set; }

    public DateTime OperationStartTime { get; set; }

    public DateTime OperationEndTime { get; set; }

    public DateTime GainRewardEndTime { get; set; }

    public int DaysAfterPlayerCreated { get; set; }

    public ulong InstanceId { get; set; }

    public int ActivityId { get; set; }

    public OperationalActivityType ActivityType { get; set; }

    public ConfigDataOperationalActivityInfo Config { get; set; }
  }
}
