﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.EquipmentBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class EquipmentBagItem : BagItemBase
  {
    public List<CommonBattleProperty> EnchantProperties;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentBagItem(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEnchant()
    {
      return this.ResonanceId != 0;
    }

    public int Level { get; set; }

    public int Exp { get; set; }

    public int StarLevel { get; set; }

    public bool Locked { get; set; }

    public int ResonanceId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentEnhanced()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEquipmentLevelUped(int bornStarLevel)
    {
      return this.StarLevel > bornStarLevel;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroEquipment ToBattleHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ProGoods ToPBGoods()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
