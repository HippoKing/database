﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class AncientCall
  {
    public List<int> HasGainAchievementRelationIds;
    public AncientCallPeriod PeriodInfo;
    public List<AncientCallBossHistory> BossHistories;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCall()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearPeriodInfo()
    {
      this.PeriodInfo = new AncientCallPeriod();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCall FromPB(ProAncientCall pbCall)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCall ToPB(AncientCall call)
    {
      // ISSUE: unable to decompile the method.
    }

    public void UpdateScore(int score, DateTime updateTime)
    {
      this.PeriodInfo.UpdateScore(score, updateTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateBossDamage(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRealTimePeriodRank(int bossId, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentPeriodExist()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCurrentBoss(int currentBossIndex)
    {
      this.PeriodInfo.SetCurrentBoss(currentBossIndex);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeriodInfo(int periodId, int bossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeriodChanged(int periodId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentBoosChanged(int bossIdIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    public AncientCallBoss FindBossById(int bossId)
    {
      return this.PeriodInfo.FindBossById(bossId);
    }
  }
}
