﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroPhantom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroPhantom
  {
    public List<HeroPhantomLevel> Levels;
    private IConfigDataLoader _ConfigDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantom()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID { get; set; }

    public string Name
    {
      get
      {
        return this.Config.Name;
      }
    }

    public string Description
    {
      get
      {
        return this.Config.Desc;
      }
    }

    public List<ConfigDataHeroPhantomLevelInfo> LevelConfigs
    {
      get
      {
        return this.Config.m_levels;
      }
    }

    public DateTime OpenDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime CloseDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime ShowDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime HideDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroPhantomCollections WhichCollection { get; set; }

    public ConfigDataHeroPhantomInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      get
      {
        return this._ConfigDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForShow
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel GetLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
