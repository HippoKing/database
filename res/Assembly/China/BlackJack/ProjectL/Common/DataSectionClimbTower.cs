﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionClimbTower
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionClimbTower : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionClimbTower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.ClimbTower = new ClimbTower();
    }

    public void SetGlobalClimbTowerInfo(GlobalClimbTowerInfo info)
    {
      this.GlobalClimbTowerInfo = info;
    }

    public ClimbTower ClimbTower { get; set; }

    public GlobalClimbTowerInfo GlobalClimbTowerInfo { get; set; }
  }
}
