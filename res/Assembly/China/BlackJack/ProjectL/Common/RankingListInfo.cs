﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RankingListInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RankingListInfo
  {
    public RankingListType Type { get; set; }

    public int Score { get; set; }

    public int CurrRank { get; set; }

    public int LastRank { get; set; }

    public int ChampionHeroId { get; set; }

    public List<RankingTargetPlayerInfo> PlayerList { get; set; }

    public int TotalTargetCount { get; set; }

    public int BossId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRankingListInfo RankingListToPBRankingList(
      RankingListInfo rankingList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RankingListInfo PBRankingListToRankingList(
      ProRankingListInfo proRankingList)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
