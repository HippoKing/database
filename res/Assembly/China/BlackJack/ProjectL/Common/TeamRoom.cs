﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TeamRoom
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RoomId { get; set; }

    public TeamRoomPlayer Leader { get; set; }

    public List<TeamRoomPlayer> Players { get; set; }

    public DateTime LeaderKickOutTime { get; set; }

    public TeamRoomSetting Setting { get; set; }

    public bool IsLeader(TeamRoomPlayer player)
    {
      return player == this.Leader;
    }

    public bool IsLeaderBySessionId(ulong sessionId)
    {
      return (long) this.Leader.SessionId == (long) sessionId;
    }

    public void JoinTeamRoom(TeamRoomPlayer player)
    {
      this.Players.Add(player);
    }

    public void QuitTeamRoom(TeamRoomPlayer player)
    {
      this.Players.Remove(player);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer FindTeamRoomPlayer(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer FindTeamRoomPlayerAtPosition(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoom TeamRoomToPbTeamRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoom PbTeamRoomToTeamRoom(ProTeamRoom pbRoom)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
