﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class CombatActor : Entity
  {
    private Vector2i m_position;
    private Vector2i m_initPosition;
    private int m_direction;
    private int m_initDirection;
    private int m_formationLine;
    private CombatTeam m_team;
    private bool m_isHero;
    private CombatActorState m_state;
    private int m_stateFrame;
    private bool m_isStateChanged;
    private int m_stateWaitFrame;
    private int m_stateMoveDistance;
    private int m_frameCount;
    private int m_canStopFrame;
    private bool m_isVisible;
    private CombatActor m_targetActor;
    private int m_preAttackHp;
    private ushort[] m_beHitIds;
    private ConfigDataSkillInfo m_skillInfo;
    private CombatSkillState m_curSkillState;
    private List<CombatSkillState> m_skillStates;
    private int m_skillUseCount;
    private bool m_isDoubleAttacked;
    private int m_healthPoint;
    private int m_healthPointMax;
    private int m_radius;
    private int m_height;
    private int m_footHeight;
    private IBattleGraphic m_graphic;
    private Vector2i m_graphicPrevPosition;
    private Fix64 m_graphicMoveDistance;
    private bool m_isGraphicSkillFade;
    private int m_deathAnimType;
    [DoNotToLua]
    private CombatActor.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_InitializeCombatTeamBoolean_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_DoPauseBoolean_hotfix;
    private LuaFunction m_EnterCombat_hotfix;
    private LuaFunction m_StartCombat_hotfix;
    private LuaFunction m_StopCombat_hotfix;
    private LuaFunction m_CanStopCombat_hotfix;
    private LuaFunction m_LogCanNotStopCombat_hotfix;
    private LuaFunction m_SetCanStopFrameInt32_hotfix;
    private LuaFunction m_SetPositionInt32Int32_hotfix;
    private LuaFunction m_SetPositionVector2i_hotfix;
    private LuaFunction m_SetDirectionInt32_hotfix;
    private LuaFunction m_SetFormationLineInt32_hotfix;
    private LuaFunction m_FaceToVector2i_hotfix;
    private LuaFunction m_MoveVector2iFix64Boolean_hotfix;
    private LuaFunction m_MoveXInt32Fix64Boolean_hotfix;
    private LuaFunction m_CreateGraphicStringSingleSingleSingleStringList`1_hotfix;
    private LuaFunction m_SetGraphicSkillFadeBoolean_hotfix;
    private LuaFunction m_PlayAnimationStringBoolean_hotfix;
    private LuaFunction m_PlayDeathAnimationInt32_hotfix;
    private LuaFunction m_PlayFxStringInt32_hotfix;
    private LuaFunction m_PlaySkillFxStringString_hotfix;
    private LuaFunction m_PlaySoundString_hotfix;
    private LuaFunction m_SetGraphicEffectGraphicEffectSingleSingle_hotfix;
    private LuaFunction m_ClearGraphicEffectGraphicEffect_hotfix;
    private LuaFunction m_ClearAttachFxsInt32_hotfix;
    private LuaFunction m_IsDead_hotfix;
    private LuaFunction m_SetVisibleBoolean_hotfix;
    private LuaFunction m_IsVisible_hotfix;
    private LuaFunction m_IsHero_hotfix;
    private LuaFunction m_GetBattleProperty_hotfix;
    private LuaFunction m_GetArmyInfo_hotfix;
    private LuaFunction m_GetMoveSpeed_hotfix;
    private LuaFunction m_GetAttackDistance_hotfix;
    private LuaFunction m_GetMoveType_hotfix;
    private LuaFunction m_GetGraphic_hotfix;
    private LuaFunction m_get_Position_hotfix;
    private LuaFunction m_get_InitPosition_hotfix;
    private LuaFunction m_get_Direction_hotfix;
    private LuaFunction m_get_FormationLine_hotfix;
    private LuaFunction m_get_Radius_hotfix;
    private LuaFunction m_get_Height_hotfix;
    private LuaFunction m_get_FootHeight_hotfix;
    private LuaFunction m_get_HealthPoint_hotfix;
    private LuaFunction m_get_HealthPointMax_hotfix;
    private LuaFunction m_get_State_hotfix;
    private LuaFunction m_get_StateFrame_hotfix;
    private LuaFunction m_get_Team_hotfix;
    private LuaFunction m_get_TeamNumber_hotfix;
    private LuaFunction m_get_Combat_hotfix;
    private LuaFunction m_TickSkillStates_hotfix;
    private LuaFunction m_TickSkillHitsCombatSkillState_hotfix;
    private LuaFunction m_AttackCombatActorConfigDataSkillInfoBoolean_hotfix;
    private LuaFunction m_ShootCombatSkillStateCombatActor_hotfix;
    private LuaFunction m_AttackByCombatActorConfigDataSkillInfoBoolean_hotfix;
    private LuaFunction m_SetHealthPointInt32_hotfix;
    private LuaFunction m_PreAttackCombatActorConfigDataSkillInfoBoolean_hotfix;
    private LuaFunction m_GetPreAttackHealthPoint_hotfix;
    private LuaFunction m_AddBeHitIdUInt16_hotfix;
    private LuaFunction m_HasBeHitIdUInt16_hotfix;
    private LuaFunction m_CreateSkillStateConfigDataSkillInfo_hotfix;
    private LuaFunction m_AddSkillDelayHitCombatSkillStateCombatActorInt32_hotfix;
    private LuaFunction m_AddMagicDamageSkillDelayHitsCombatSkillState_hotfix;
    private LuaFunction m_AddChargeSkillDelayHitsCombatSkillState_hotfix;
    private LuaFunction m_CanBeTarget_hotfix;
    private LuaFunction m_ChangeFightTargetCombatActor_hotfix;
    private LuaFunction m_SearchTarget_hotfix;
    private LuaFunction m_SearchRangeTargetVector2iInt32_hotfix;
    private LuaFunction m_SearchHitTargetInt32UInt16_hotfix;
    private LuaFunction m_SearchRandomHitTargetUInt16Int32_hotfix;
    private LuaFunction m_IsTargetInAttackDistanceCombatActorInt32_hotfix;
    private LuaFunction m_TickState_hotfix;
    private LuaFunction m_ChangeStateCombatActorState_hotfix;
    private LuaFunction m_StateEnter_hotfix;
    private LuaFunction m_StateExit_hotfix;
    private LuaFunction m_StateIdle_hotfix;
    private LuaFunction m_StateFight_hotfix;
    private LuaFunction m_StateFightEnd_hotfix;
    private LuaFunction m_StateFightAgain_hotfix;
    private LuaFunction m_StateSkill_hotfix;
    private LuaFunction m_StateDie_hotfix;
    private LuaFunction m_StateReturn_hotfix;
    private LuaFunction m_StateStop_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(CombatTeam team, bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanStopFrame(int delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(Vector2i p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int d)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFormationLine(int line)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FaceTo(Vector2i pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Move(Vector2i dir, Fix64 distance, bool changeDirection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveX(int dir, Fix64 distance, bool changeDirection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateGraphic(
      string assetName,
      float scale,
      float height,
      float footHeight,
      string name,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayDeathAnimation(int deathType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, int tag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySkillFx(string name, string nameFar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAttachFxs(int tagMask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleProperty GetBattleProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyInfo GetArmyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMoveSpeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MoveType GetMoveType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic GetGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2i Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Vector2i InitPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FormationLine
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Radius
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Height
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FootHeight
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HealthPointMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatActorState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int StateFrame
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatTeam Team
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickSkillStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickSkillHits(CombatSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Attack(CombatActor target, ConfigDataSkillInfo skillInfo, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Shoot(CombatSkillState ss, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBy(CombatActor attacker, ConfigDataSkillInfo skillInfo, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHealthPoint(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PreAttack(CombatActor target, ConfigDataSkillInfo skillInfo, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPreAttackHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBeHitId(ushort id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBeHitId(ushort id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatSkillState CreateSkillState(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSkillDelayHit(CombatSkillState ss, CombatActor target, int delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMagicDamageSkillDelayHits(CombatSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddChargeSkillDelayHits(CombatSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanBeTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeFightTarget(CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchRangeTarget(Vector2i pos, int maxDistance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchHitTarget(int attackDistance, ushort hitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchRandomHitTarget(ushort hitId, int targetType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeTargetScore(Vector2i pos, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTargetInAttackDistance(CombatActor target, int attackDistance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeState(CombatActorState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateEnter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateExit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateIdle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateFight()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateFightEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateFightAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateDie()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CombatActor.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Dispose()
    {
      base.Dispose();
    }

    private void __callBase_Tick()
    {
      base.Tick();
    }

    private void __callBase_TickGraphic(float dt)
    {
      base.TickGraphic(dt);
    }

    private void __callBase_Draw()
    {
      base.Draw();
    }

    private void __callBase_Pause(bool pause)
    {
      this.Pause(pause);
    }

    private void __callBase_DoPause(bool pause)
    {
      base.DoPause(pause);
    }

    private void __callBase_DeleteMe()
    {
      this.DeleteMe();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CombatActor m_owner;

      public LuaExportHelper(CombatActor owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Dispose()
      {
        this.m_owner.__callBase_Dispose();
      }

      public void __callBase_Tick()
      {
        this.m_owner.__callBase_Tick();
      }

      public void __callBase_TickGraphic(float dt)
      {
        this.m_owner.__callBase_TickGraphic(dt);
      }

      public void __callBase_Draw()
      {
        this.m_owner.__callBase_Draw();
      }

      public void __callBase_Pause(bool pause)
      {
        this.m_owner.__callBase_Pause(pause);
      }

      public void __callBase_DoPause(bool pause)
      {
        this.m_owner.__callBase_DoPause(pause);
      }

      public void __callBase_DeleteMe()
      {
        this.m_owner.__callBase_DeleteMe();
      }

      public Vector2i m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public Vector2i m_initPosition
      {
        get
        {
          return this.m_owner.m_initPosition;
        }
        set
        {
          this.m_owner.m_initPosition = value;
        }
      }

      public int m_direction
      {
        get
        {
          return this.m_owner.m_direction;
        }
        set
        {
          this.m_owner.m_direction = value;
        }
      }

      public int m_initDirection
      {
        get
        {
          return this.m_owner.m_initDirection;
        }
        set
        {
          this.m_owner.m_initDirection = value;
        }
      }

      public int m_formationLine
      {
        get
        {
          return this.m_owner.m_formationLine;
        }
        set
        {
          this.m_owner.m_formationLine = value;
        }
      }

      public CombatTeam m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public bool m_isHero
      {
        get
        {
          return this.m_owner.m_isHero;
        }
        set
        {
          this.m_owner.m_isHero = value;
        }
      }

      public CombatActorState m_state
      {
        get
        {
          return this.m_owner.m_state;
        }
        set
        {
          this.m_owner.m_state = value;
        }
      }

      public int m_stateFrame
      {
        get
        {
          return this.m_owner.m_stateFrame;
        }
        set
        {
          this.m_owner.m_stateFrame = value;
        }
      }

      public bool m_isStateChanged
      {
        get
        {
          return this.m_owner.m_isStateChanged;
        }
        set
        {
          this.m_owner.m_isStateChanged = value;
        }
      }

      public int m_stateWaitFrame
      {
        get
        {
          return this.m_owner.m_stateWaitFrame;
        }
        set
        {
          this.m_owner.m_stateWaitFrame = value;
        }
      }

      public int m_stateMoveDistance
      {
        get
        {
          return this.m_owner.m_stateMoveDistance;
        }
        set
        {
          this.m_owner.m_stateMoveDistance = value;
        }
      }

      public int m_frameCount
      {
        get
        {
          return this.m_owner.m_frameCount;
        }
        set
        {
          this.m_owner.m_frameCount = value;
        }
      }

      public int m_canStopFrame
      {
        get
        {
          return this.m_owner.m_canStopFrame;
        }
        set
        {
          this.m_owner.m_canStopFrame = value;
        }
      }

      public bool m_isVisible
      {
        get
        {
          return this.m_owner.m_isVisible;
        }
        set
        {
          this.m_owner.m_isVisible = value;
        }
      }

      public CombatActor m_targetActor
      {
        get
        {
          return this.m_owner.m_targetActor;
        }
        set
        {
          this.m_owner.m_targetActor = value;
        }
      }

      public int m_preAttackHp
      {
        get
        {
          return this.m_owner.m_preAttackHp;
        }
        set
        {
          this.m_owner.m_preAttackHp = value;
        }
      }

      public ushort[] m_beHitIds
      {
        get
        {
          return this.m_owner.m_beHitIds;
        }
        set
        {
          this.m_owner.m_beHitIds = value;
        }
      }

      public ConfigDataSkillInfo m_skillInfo
      {
        get
        {
          return this.m_owner.m_skillInfo;
        }
        set
        {
          this.m_owner.m_skillInfo = value;
        }
      }

      public CombatSkillState m_curSkillState
      {
        get
        {
          return this.m_owner.m_curSkillState;
        }
        set
        {
          this.m_owner.m_curSkillState = value;
        }
      }

      public List<CombatSkillState> m_skillStates
      {
        get
        {
          return this.m_owner.m_skillStates;
        }
        set
        {
          this.m_owner.m_skillStates = value;
        }
      }

      public int m_skillUseCount
      {
        get
        {
          return this.m_owner.m_skillUseCount;
        }
        set
        {
          this.m_owner.m_skillUseCount = value;
        }
      }

      public bool m_isDoubleAttacked
      {
        get
        {
          return this.m_owner.m_isDoubleAttacked;
        }
        set
        {
          this.m_owner.m_isDoubleAttacked = value;
        }
      }

      public int m_healthPoint
      {
        get
        {
          return this.m_owner.m_healthPoint;
        }
        set
        {
          this.m_owner.m_healthPoint = value;
        }
      }

      public int m_healthPointMax
      {
        get
        {
          return this.m_owner.m_healthPointMax;
        }
        set
        {
          this.m_owner.m_healthPointMax = value;
        }
      }

      public int m_radius
      {
        get
        {
          return this.m_owner.m_radius;
        }
        set
        {
          this.m_owner.m_radius = value;
        }
      }

      public int m_height
      {
        get
        {
          return this.m_owner.m_height;
        }
        set
        {
          this.m_owner.m_height = value;
        }
      }

      public int m_footHeight
      {
        get
        {
          return this.m_owner.m_footHeight;
        }
        set
        {
          this.m_owner.m_footHeight = value;
        }
      }

      public IBattleGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public Vector2i m_graphicPrevPosition
      {
        get
        {
          return this.m_owner.m_graphicPrevPosition;
        }
        set
        {
          this.m_owner.m_graphicPrevPosition = value;
        }
      }

      public Fix64 m_graphicMoveDistance
      {
        get
        {
          return this.m_owner.m_graphicMoveDistance;
        }
        set
        {
          this.m_owner.m_graphicMoveDistance = value;
        }
      }

      public bool m_isGraphicSkillFade
      {
        get
        {
          return this.m_owner.m_isGraphicSkillFade;
        }
        set
        {
          this.m_owner.m_isGraphicSkillFade = value;
        }
      }

      public int m_deathAnimType
      {
        get
        {
          return this.m_owner.m_deathAnimType;
        }
        set
        {
          this.m_owner.m_deathAnimType = value;
        }
      }

      public void Move(Vector2i dir, Fix64 distance, bool changeDirection)
      {
        this.m_owner.Move(dir, distance, changeDirection);
      }

      public void MoveX(int dir, Fix64 distance, bool changeDirection)
      {
        this.m_owner.MoveX(dir, distance, changeDirection);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void CreateGraphic(
        string assetName,
        float scale,
        float height,
        float footHeight,
        string name,
        List<ReplaceAnim> replaceAnims)
      {
        // ISSUE: unable to decompile the method.
      }

      public void PlayAnimation(string name, bool loop)
      {
        this.m_owner.PlayAnimation(name, loop);
      }

      public void PlayDeathAnimation(int deathType)
      {
        this.m_owner.PlayDeathAnimation(deathType);
      }

      public void PlayFx(string name, int tag)
      {
        this.m_owner.PlayFx(name, tag);
      }

      public void PlaySkillFx(string name, string nameFar)
      {
        this.m_owner.PlaySkillFx(name, nameFar);
      }

      public void PlaySound(string name)
      {
        this.m_owner.PlaySound(name);
      }

      public void SetGraphicEffect(GraphicEffect e, float param1, float param2)
      {
        this.m_owner.SetGraphicEffect(e, param1, param2);
      }

      public void ClearGraphicEffect(GraphicEffect e)
      {
        this.m_owner.ClearGraphicEffect(e);
      }

      public void ClearAttachFxs(int tagMask)
      {
        this.m_owner.ClearAttachFxs(tagMask);
      }

      public BattleProperty GetBattleProperty()
      {
        return this.m_owner.GetBattleProperty();
      }

      public int GetMoveSpeed()
      {
        return this.m_owner.GetMoveSpeed();
      }

      public MoveType GetMoveType()
      {
        return this.m_owner.GetMoveType();
      }

      public void TickSkillStates()
      {
        this.m_owner.TickSkillStates();
      }

      public void TickSkillHits(CombatSkillState ss)
      {
        this.m_owner.TickSkillHits(ss);
      }

      public bool Attack(CombatActor target, ConfigDataSkillInfo skillInfo, bool isCritical)
      {
        return this.m_owner.Attack(target, skillInfo, isCritical);
      }

      public bool Shoot(CombatSkillState ss, CombatActor target)
      {
        return this.m_owner.Shoot(ss, target);
      }

      public CombatSkillState CreateSkillState(ConfigDataSkillInfo skillInfo)
      {
        return this.m_owner.CreateSkillState(skillInfo);
      }

      public void AddSkillDelayHit(CombatSkillState ss, CombatActor target, int delay)
      {
        this.m_owner.AddSkillDelayHit(ss, target, delay);
      }

      public void AddMagicDamageSkillDelayHits(CombatSkillState ss)
      {
        this.m_owner.AddMagicDamageSkillDelayHits(ss);
      }

      public void AddChargeSkillDelayHits(CombatSkillState ss)
      {
        this.m_owner.AddChargeSkillDelayHits(ss);
      }

      public void ChangeFightTarget(CombatActor target)
      {
        this.m_owner.ChangeFightTarget(target);
      }

      public CombatActor SearchTarget()
      {
        return this.m_owner.SearchTarget();
      }

      public CombatActor SearchRangeTarget(Vector2i pos, int maxDistance)
      {
        return this.m_owner.SearchRangeTarget(pos, maxDistance);
      }

      public CombatActor SearchHitTarget(int attackDistance, ushort hitId)
      {
        return this.m_owner.SearchHitTarget(attackDistance, hitId);
      }

      public CombatActor SearchRandomHitTarget(ushort hitId, int targetType)
      {
        return this.m_owner.SearchRandomHitTarget(hitId, targetType);
      }

      public static int ComputeTargetScore(Vector2i pos, CombatActor target)
      {
        return CombatActor.ComputeTargetScore(pos, target);
      }

      public bool IsTargetInAttackDistance(CombatActor target, int attackDistance)
      {
        return this.m_owner.IsTargetInAttackDistance(target, attackDistance);
      }

      public void TickState()
      {
        this.m_owner.TickState();
      }

      public void ChangeState(CombatActorState state)
      {
        this.m_owner.ChangeState(state);
      }

      public void StateEnter()
      {
        this.m_owner.StateEnter();
      }

      public void StateExit()
      {
        this.m_owner.StateExit();
      }

      public void StateIdle()
      {
        this.m_owner.StateIdle();
      }

      public void StateFight()
      {
        this.m_owner.StateFight();
      }

      public void StateFightEnd()
      {
        this.m_owner.StateFightEnd();
      }

      public void StateFightAgain()
      {
        this.m_owner.StateFightAgain();
      }

      public void StateSkill()
      {
        this.m_owner.StateSkill();
      }

      public void StateDie()
      {
        this.m_owner.StateDie();
      }

      public void StateReturn()
      {
        this.m_owner.StateReturn();
      }

      public void StateStop()
      {
        this.m_owner.StateStop();
      }
    }
  }
}
