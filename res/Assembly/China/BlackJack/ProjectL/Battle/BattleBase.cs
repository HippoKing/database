﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattleBase
  {
    private IConfigDataLoader m_configDataLoader;
    private IBattleListener m_battleListener;
    private RandomNumber m_randomNumber;
    private RandomNumber m_aiRandomNumber1;
    private RandomNumber m_aiRandomNumber2;
    private RandomNumber m_armyRandomNumber;
    private BattleMap m_map;
    private Pathfinder m_pathfinder;
    private Combat m_combat;
    private ConfigDataBattlefieldInfo m_battlefieldInfo;
    private ConfigDataBattleInfo m_battleInfo;
    private ConfigDataArenaBattleInfo m_arenaBattleInfo;
    private ConfigDataPVPBattleInfo m_pvpBattleInfo;
    private ConfigDataRealTimePVPBattleInfo m_realtimePvpBattleInfo;
    private ConfigDataPeakArenaBattleInfo m_peakArenaBattleInfo;
    private BattleType m_battleType;
    private int m_monsterLevel;
    private BattleTeam[] m_teams;
    private BattlePlayer[] m_players;
    private ConfigDataPlayerLevelInfo m_maxPlayerLevelInfo;
    private BattleState m_state;
    private int m_step;
    private int m_turn;
    private int m_turnMax;
    private int m_actionTeam;
    private int m_prevActionPlayerIndex;
    private BattleActor m_actionActor;
    private BattleActor m_execCommandActor;
    private BattleActor m_checkingDieActor;
    private ActionOrderType m_actionOrderType;
    private int m_entityIdCounter;
    private bool m_needCheckBattleStopEventTrigger;
    private bool m_isGiveupBattle;
    private int m_giveupBattlePlayerIndex;
    private int m_combatRandomSeed;
    private BattleActor m_combatActorA;
    private BattleActor m_combatActorB;
    private ConfigDataSkillInfo m_combatSkillInfoA;
    private BattleActor m_beGuardedCombatActor;
    private int m_enterCombatHealthPointPercentA;
    private int m_enterCombatHealthPointPercentB;
    private List<AfterCombatApplyBuff> m_afterCombatApplyBuffList;
    private bool m_isPerformCombat;
    private ConfigDataBattlePerformInfo m_afterComatPerform;
    private BattleEventTriggerState m_afterCombatExecuteTriggerState;
    private int m_afterCombatExecuteTriggerEventActionIndex;
    private bool m_isAfterCombatNextStep;
    private bool m_isAfterCombatNextStepActorActive;
    private ListPool<int> m_tempIntListPool;
    private ListPool<GridPosition> m_tempGridPositionListPool;
    private ListPool<BattleActor> m_tempActorListPool;
    private ListPool<ConfigDataBuffInfo> m_tempBuffInfoListPool;
    private ListPool<BuffState> m_tempBuffStateListPool;
    private BattlePropertyModifier m_tempBattlePropertyModifier;
    private LinkedList<BattleCommand> m_battleCommands;
    private LinkedList<BattleCommand> m_logBattleCommands;
    private LinkedList<BattleCommand> m_tempBattleCommands;
    private List<BattleCommand> m_stepExecutedCommands;
    private bool m_enableLogBattleCommands;
    private bool m_enableTempBattleCommands;
    private bool m_enableDebugLog;
    private bool m_enableErrorLog;
    private bool m_enableChecksum;
    private List<int> m_checksums;
    private List<BattleWinConditionState> m_winConditionStates;
    private List<BattleLoseConditionState> m_loseConditionStates;
    private List<BattleAchievementState> m_achievementStates;
    private List<BattleEventTriggerState> m_eventTriggerStates;
    private List<BattleTreasureState> m_battleTreasureStates;
    private List<BattleActor> m_needPostProcessDieActors;
    private Dictionary<int, int> m_eventVariables;
    private int m_stars;
    private int m_starTurnMax;
    private int m_starDeadMax;
    private int m_myPlayerTeam;
    private int m_cheatPassiveSkillId;
    [DoNotToLua]
    private BattleBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorIConfigDataLoaderIBattleListener_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_SetWinForCheat_hotfix;
    private LuaFunction m_SetCheatPassiveSkillIdInt32_hotfix;
    private LuaFunction m_GetCheatPassiveSkillId_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_GetStarTurnMax_hotfix;
    private LuaFunction m_GetStarDeadMax_hotfix;
    private LuaFunction m_IsPerformOnlyBattle_hotfix;
    private LuaFunction m_Start_hotfix;
    private LuaFunction m_StopBoolean_hotfix;
    private LuaFunction m_FirstStepBoolean_hotfix;
    private LuaFunction m_NextStep_hotfix;
    private LuaFunction m_NextStep_NormalBattleActor_hotfix;
    private LuaFunction m_NextStep_PvpBattleActor_hotfix;
    private LuaFunction m_NextStepActorActive_hotfix;
    private LuaFunction m_NextTurn_hotfix;
    private LuaFunction m_GetDefaultActionActorInt32_hotfix;
    private LuaFunction m_HasNotActionFinishedActor_hotfix;
    private LuaFunction m_GetActionTeam_hotfix;
    private LuaFunction m_GetActionActor_hotfix;
    private LuaFunction m_ChangeActionActorBattleActor_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_ChangeMapTerrainList`1ConfigDataTerrainInfo_hotfix;
    private LuaFunction m_ChangeMapTerrainEffectList`1ConfigDataTerrainEffectInfo_hotfix;
    private LuaFunction m_GetTeamInt32_hotfix;
    private LuaFunction m_GetBattlePlayerInt32_hotfix;
    private LuaFunction m_GetActorByIdInt32_hotfix;
    private LuaFunction m_GetActorByHeroIdInt32_hotfix;
    private LuaFunction m_GetAliveActorByHeroIdInt32_hotfix;
    private LuaFunction m_GetAliveActorsByHeroIdsAndGroupIdsList`1List`1List`1_hotfix;
    private LuaFunction m_GetNextEntityId_hotfix;
    private LuaFunction m_CheckOnActorMoveBattleActor_hotfix;
    private LuaFunction m_CheckOnActorDieBattleActor_hotfix;
    private LuaFunction m_OnActorDieBattleActor_hotfix;
    private LuaFunction m_PostProcessDieActors_hotfix;
    private LuaFunction m_SetProcessingDieActorBattleActor_hotfix;
    private LuaFunction m_GetProcessingDieActor_hotfix;
    private LuaFunction m_OnActorRetreatBattleActorInt32StringBoolean_hotfix;
    private LuaFunction m_OnActorChangeTeamBattleActor_hotfix;
    private LuaFunction m_IsPlay_hotfix;
    private LuaFunction m_IsGiveupBattle_hotfix;
    private LuaFunction m_GetGiveupBattlePlayerIndex_hotfix;
    private LuaFunction m_AllocateTempIntList_hotfix;
    private LuaFunction m_FreeTempIntListList`1_hotfix;
    private LuaFunction m_AllocateTempGridPositionList_hotfix;
    private LuaFunction m_FreeTempGridPositionListList`1_hotfix;
    private LuaFunction m_AllocateTempActorList_hotfix;
    private LuaFunction m_FreeTempActorListList`1_hotfix;
    private LuaFunction m_AllocateTempBuffList_hotfix;
    private LuaFunction m_FreeTempBuffListList`1_hotfix;
    private LuaFunction m_AllocateTempBuffStateListList`1BuffType_hotfix;
    private LuaFunction m_FreeTempBuffStateListList`1_hotfix;
    private LuaFunction m_RandomizeBuffStateListList`1_hotfix;
    private LuaFunction m_GetTempBattlePropertyModifier_hotfix;
    private LuaFunction m_IsProbabilitySatisfiedInt32_hotfix;
    private LuaFunction m_GetRandomBuffListList`1Int32List`1List`1_hotfix;
    private LuaFunction m_ComputeArmyRelationDataBattleActorBooleanBattleActorBooleanBooleanBoolean_hotfix;
    private LuaFunction m_FindPathGridPositionGridPositionInt32MoveTypeFindPathIgnoreTeamTypeInt32Int32SpecialMoveCostTypeInt32List`1_hotfix;
    private LuaFunction m_FindMoveRegionGridPositionInt32MoveTypeFindPathIgnoreTeamTypeInt32Int32SpecialMoveCostTypeInt32List`1_hotfix;
    private LuaFunction m_FindAttackRegionGridPositionInt32Int32List`1_hotfix;
    private LuaFunction m_FindDirectionalAttackRegionGridPositionAttackDirectionInt32Int32Int32List`1_hotfix;
    private LuaFunction m_FindRingRegionGridPositionInt32List`1_hotfix;
    private LuaFunction m_FindFixedDistanceRegionGridPositionInt32List`1_hotfix;
    private LuaFunction m_IsInDangerRegionInt32GridPosition_hotfix;
    private LuaFunction m_GetStars_hotfix;
    private LuaFunction m_IsWin_hotfix;
    private LuaFunction m_GetPlayerActorsInt32_hotfix;
    private LuaFunction m_GetPlayerActorsLogInfosInt32_hotfix;
    private LuaFunction m_GetPlayerKillActorCountInt32Int32_hotfix;
    private LuaFunction m_GetTotalKillActorCountInt32_hotfix;
    private LuaFunction m_GetDeadEnemyActors_hotfix;
    private LuaFunction m_GetBossTotalDamage_hotfix;
    private LuaFunction m_get_AliveActorsInfo_hotfix;
    private LuaFunction m_EnableChecksumBoolean_hotfix;
    private LuaFunction m_GetChecksums_hotfix;
    private LuaFunction m_ComputeStepChecksum_hotfix;
    private LuaFunction m_EnableDebugLogBoolean_hotfix;
    private LuaFunction m_IsEnableDebugLog_hotfix;
    private LuaFunction m_EnableErrorLogBoolean_hotfix;
    private LuaFunction m_IsEnableErrorLog_hotfix;
    private LuaFunction m_get_ConfigDataLoader_hotfix;
    private LuaFunction m_get_Listener_hotfix;
    private LuaFunction m_get_RandomNumber_hotfix;
    private LuaFunction m_get_AIRandomNumber1_hotfix;
    private LuaFunction m_get_AIRandomNumber2_hotfix;
    private LuaFunction m_get_BattleInfo_hotfix;
    private LuaFunction m_get_ArenaBattleInfo_hotfix;
    private LuaFunction m_get_PVPBattleInfo_hotfix;
    private LuaFunction m_get_RealTimePVPBattleInfo_hotfix;
    private LuaFunction m_get_PeakArenaBattleInfo_hotfix;
    private LuaFunction m_get_BattlefieldInfo_hotfix;
    private LuaFunction m_get_BattleType_hotfix;
    private LuaFunction m_get_ActionOrderType_hotfix;
    private LuaFunction m_get_BattlePlayers_hotfix;
    private LuaFunction m_get_MaxPlayerLevelInfo_hotfix;
    private LuaFunction m_get_State_hotfix;
    private LuaFunction m_get_Step_hotfix;
    private LuaFunction m_get_Turn_hotfix;
    private LuaFunction m_get_TurnMax_hotfix;
    private LuaFunction m_get_Map_hotfix;
    private LuaFunction m_get_Combat_hotfix;
    private LuaFunction m_CheckAchievementOnActorMoveBattleActor_hotfix;
    private LuaFunction m_CheckAchievementOnActorDieBattleActor_hotfix;
    private LuaFunction m_CheckAchievementOnNextStep_hotfix;
    private LuaFunction m_CheckAchievementOnTurnEndOrWinBoolean_hotfix;
    private LuaFunction m_CheckAchievementOnWin_hotfix;
    private LuaFunction m_CheckAchievementOnEventTriggerBattleEventTriggerState_hotfix;
    private LuaFunction m_CheckAchievementOnLose_hotfix;
    private LuaFunction m_ChangeAchievementStatusBattleAchievementStateBoolean_hotfix;
    private LuaFunction m_IsActorsAllAliveList`1_hotfix;
    private LuaFunction m_IsActorsAllKillBySkillClassList`1Int32_hotfix;
    private LuaFunction m_IsActorReachPositionsBattleActorList`1Int32_hotfix;
    private LuaFunction m_IsActorReachPositionsBattleActorNpcConditionList`1Int32_hotfix;
    private LuaFunction m_IsActorsAnyReachPositionsList`1List`1Int32_hotfix;
    private LuaFunction m_IsActorsAnyReachPositionsInt32NpcConditionList`1Int32_hotfix;
    private LuaFunction m_GetActorsCountSatisfyConditionInt32NpcConditionInt32_hotfix;
    private LuaFunction m_IsAchievementCompleteInt32_hotfix;
    private LuaFunction m_GetCompleteAchievements_hotfix;
    private LuaFunction m_GetAchievementStatusInt32_hotfix;
    private LuaFunction m_AddBattleCommandBattleCommand_hotfix;
    private LuaFunction m_AddLogBattleCommandBattleCommand_hotfix;
    private LuaFunction m_AddGiveupLogBattleCommandInt32_hotfix;
    private LuaFunction m_AddCancelLogBattleCommandInt32_hotfix;
    private LuaFunction m_AddGiveupOrCancelLogBattleCommandBattleCommandTypeInt32_hotfix;
    private LuaFunction m_AddBattleCommandToListLinkedList`1BattleCommand_hotfix;
    private LuaFunction m_ClearBattleCommandsAndNextStep_hotfix;
    private LuaFunction m_ClearBattleCommands_hotfix;
    private LuaFunction m_GetBattleCommands_hotfix;
    private LuaFunction m_HasBattleCommand_hotfix;
    private LuaFunction m_HasCurrentStepBattleCommand_hotfix;
    private LuaFunction m_HasWrongStepBattleCommand_hotfix;
    private LuaFunction m_ExecuteBattleCommand_hotfix;
    private LuaFunction m_ExecuteGiveupCommandInt32_hotfix;
    private LuaFunction m_RemoveStepCommandsInt32_hotfix;
    private LuaFunction m_RunCurrentStepBattleCommand_hotfix;
    private LuaFunction m_RunCombat_hotfix;
    private LuaFunction m_RunBoolean_hotfix;
    private LuaFunction m_RunAIActors_hotfix;
    private LuaFunction m_RunPlayerBattleCommandsInt32List`1_hotfix;
    private LuaFunction m_AddExecutedCommandBattleCommand_hotfix;
    private LuaFunction m_FixExecutedCommandsBattleActorInt32_hotfix;
    private LuaFunction m_GetStepExecutedCommands_hotfix;
    private LuaFunction m_EnableLogBattleCommandsBoolean_hotfix;
    private LuaFunction m_IsEnableLogBattleCommands_hotfix;
    private LuaFunction m_GetLogBattleCommands_hotfix;
    private LuaFunction m_EnableTempBattleCommandsBoolean_hotfix;
    private LuaFunction m_IsEnableTempBattleCommands_hotfix;
    private LuaFunction m_GetTempBattleCommands_hotfix;
    private LuaFunction m_ClearTempBattleCommands_hotfix;
    private LuaFunction m_StartCombatBattleActorBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_StopCombat_hotfix;
    private LuaFunction m_PostProcessCombatActorsDieBattleActorBattleActorBattleActor_hotfix;
    private LuaFunction m_RestartCombat_hotfix;
    private LuaFunction m_CopyCombatPropertyCombatTeamBattleActor_hotfix;
    private LuaFunction m_CheckWinConditionOnActorMoveBattleActor_hotfix;
    private LuaFunction m_CheckLoseConditionOnActorMoveBattleActor_hotfix;
    private LuaFunction m_CheckWinConditionOnActorDieBattleActor_hotfix;
    private LuaFunction m_CheckLoseConditionOnActorDieBattleActor_hotfix;
    private LuaFunction m_CheckWinConditionOnActorRetreatBattleActor_hotfix;
    private LuaFunction m_CheckLoseConditionOnActorRetreatBattleActor_hotfix;
    private LuaFunction m_CheckWinConditionOnEventTriggerBattleEventTriggerState_hotfix;
    private LuaFunction m_CheckLoseConditionOnEventTriggerBattleEventTriggerState_hotfix;
    private LuaFunction m_CheckWinConditionOnNextStep_hotfix;
    private LuaFunction m_CheckWinConditionOnTurnEnd_hotfix;
    private LuaFunction m_CheckWinLoseConditionOnTurnMax_hotfix;
    private LuaFunction m_ChangeWinConditionStatusBattleWinConditionState_hotfix;
    private LuaFunction m_ChangeLoseConditionStatusBattleLoseConditionState_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorMoveBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorDieBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorActionBeginBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorCombatAttackBeforeBattleActorBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorCombatAttackAfterBattleActorBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorBeCombatAttackBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnActorUseSkillBattleActor_hotfix;
    private LuaFunction m_CheckEventTriggerOnNextStep_hotfix;
    private LuaFunction m_CheckEventTriggerOnNextTeamInt32Boolean_hotfix;
    private LuaFunction m_CheckEventTriggerOnCompleteAchievementInt32_hotfix;
    private LuaFunction m_CheckEventTriggerOnWin_hotfix;
    private LuaFunction m_CheckEventTriggerOnLose_hotfix;
    private LuaFunction m_CheckEventTriggerOnTurnEnd_hotfix;
    private LuaFunction m_CheckBattleStopEventTrigger_hotfix;
    private LuaFunction m_CheckMultiEventTriggerInt32_hotfix;
    private LuaFunction m_CheckVariableEventTriggerInt32Int32_hotfix;
    private LuaFunction m_IsEventTriggerdInt32_hotfix;
    private LuaFunction m_GetEventTriggerStateInt32_hotfix;
    private LuaFunction m_ResetEventTriggerStateInt32_hotfix;
    private LuaFunction m_DisableEventTriggerStateInt32_hotfix;
    private LuaFunction m_TriggerEventBattleEventTriggerState_hotfix;
    private LuaFunction m_ExecuteTriggerEventActionsBattleEventTriggerStateInt32_hotfix;
    private LuaFunction m_ExecuteEventActionConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_ReliefConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_RetreatConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_DialogConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_ChangeTeamConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_AttachBuffConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_ChangeBehaviorConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_MusicConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_ChangeTerrainConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_TerrainEffectConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_PerformConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_RetreatPositionConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_ReplaceConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_RemoveBuffConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_AttachBuffPositionConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_TeleportConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_ResetEventTriggerConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_DisableEventTriggerConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_SetVariableConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_AddVariableConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_ExecuteEventAction_RandomActionConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_IsEmptyPositionGridPosition_hotfix;
    private LuaFunction m_GetRandomBattleArmyActorsList`1_hotfix;
    private LuaFunction m_CreateEventActorGridPositionInt32Int32BooleanInt32Int32Int32Int32Int32StringBattleActorSourceType_hotfix;
    private LuaFunction m_ReplaceEventActorBattleActorInt32Int32String_hotfix;
    private LuaFunction m_SetEventVariableInt32Int32_hotfix;
    private LuaFunction m_GetEventVariableInt32_hotfix;
    private LuaFunction m_FindEmptyFixedDistancePositionGridPositionInt32MoveTypeBoolean_hotfix;
    private LuaFunction m_FindEmptyFixedDistancePositionGridPositionGridPositionInt32_hotfix;
    private LuaFunction m_FindEmptyNearPositionGridPositionMoveTypeBoolean_hotfix;
    private LuaFunction m_CreateMapConfigDataBattlefieldInfo_hotfix;
    private LuaFunction m_ResetMap_hotfix;
    private LuaFunction m_SetMyPlayerTeamNumberInt32_hotfix;
    private LuaFunction m_GetMyPlayerTeamNumber_hotfix;
    private LuaFunction m_InitBattleConfigDataBattleInfoBattleTypeBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32Int32Int32_hotfix;
    private LuaFunction m_InitArenaBattleConfigDataArenaBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32ConfigDataArenaDefendRuleInfo_hotfix;
    private LuaFunction m_InitPVPBattleConfigDataPVPBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32_hotfix;
    private LuaFunction m_InitRealTimePVPBattleConfigDataRealTimePVPBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32_hotfix;
    private LuaFunction m_InitPeakArenaBattleConfigDataPeakArenaBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32_hotfix;
    private LuaFunction m_InitRandomNumbersInt32Int32_hotfix;
    private LuaFunction m_InitPlayersBattlePlayerbe_hotfix;
    private LuaFunction m_InitWinConditionStatesList`1_hotfix;
    private LuaFunction m_InitLoseConditionStatesList`1_hotfix;
    private LuaFunction m_InitEventTriggerStatesList`1_hotfix;
    private LuaFunction m_InitBattleTreasureStatesList`1_hotfix;
    private LuaFunction m_CreateActorsInt32List`1_hotfix;
    private LuaFunction m_InitBattleReportConfigDataBattleInfoBattleTypeBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32Int32Int32List`1_hotfix;
    private LuaFunction m_InitArenaBattleReportConfigDataArenaBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32ConfigDataArenaDefendRuleInfoList`1_hotfix;
    private LuaFunction m_InitPVPBattleReportConfigDataPVPBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32List`1_hotfix;
    private LuaFunction m_InitRealTimePVPBattleReportConfigDataRealTimePVPBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32List`1_hotfix;
    private LuaFunction m_InitPeakArenaBattleReportConfigDataPeakArenaBattleInfoBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32List`1_hotfix;
    private LuaFunction m_InitStarAndAchievementInt32Int32List`1_hotfix;
    private LuaFunction m_InitAchievementList`1_hotfix;
    private LuaFunction m_InitGainBattleTreasuresList`1_hotfix;
    private LuaFunction m_ExecuteBattlePerformsConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteAfterCombatSteps_hotfix;
    private LuaFunction m_ExecuteBattlePerformConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_DialogConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_PlayMusicConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_PlaySoundConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_PlayFxConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_PlayActorFxConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ChangeTerrainConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_TerrainEffectConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_CameraFocusPositionConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_CameraFocusActorConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_CreateActorConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_CreateActorNearConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_RemoveActorConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorMoveConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorMoveNearConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorAttackConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorSkillConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorDirConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorAnimationConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_ActorIdleConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_WaitTimeConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_ExecuteBattlePerform_StopBattleConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_CheckBattleTreasureOnActorMoveBattleActor_hotfix;
    private LuaFunction m_ChangeBattleTeasureStatusBattleTreasureStateBattleActor_hotfix;
    private LuaFunction m_IsGainBattleTreasureInt32_hotfix;
    private LuaFunction m_GetGainBattleTreasures_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleBase(IConfigDataLoader configDataLoader, IBattleListener battleListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWinForCheat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCheatPassiveSkillId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCheatPassiveSkillId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStarTurnMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStarDeadMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPerformOnlyBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Stop(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FirstStep(bool skipPerformCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStep_Normal(BattleActor actionActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStep_Pvp(BattleActor actionActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStepActorActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor GetDefaultActionActor(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasNotActionFinishedActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActionTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActionActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ChangeActionActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrain(List<GridPosition> positions, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam GetTeam(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer GetBattlePlayer(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor GetAliveActorByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetAliveActorsByHeroIdsAndGroupIds(
      List<int> heroIds,
      List<int> groupIds,
      List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostProcessDieActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProcessingDieActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetProcessingDieActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorRetreat(
      BattleActor actor,
      int effectType,
      string fxName,
      bool notifyListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorChangeTeam(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGiveupBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGiveupBattlePlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> AllocateTempIntList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempIntList(List<int> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> AllocateTempGridPositionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempGridPositionList(List<GridPosition> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> AllocateTempActorList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempActorList(List<BattleActor> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBuffInfo> AllocateTempBuffList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempBuffList(List<ConfigDataBuffInfo> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BuffState> AllocateTempBuffStateList(
      List<BuffState> initList = null,
      BuffType initBuffType = BuffType.BuffType_None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempBuffStateList(List<BuffState> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RandomizeBuffStateList(List<BuffState> buffStateList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePropertyModifier GetTempBattlePropertyModifier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProbabilitySatisfied(int rate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetRandomBuffList(
      List<int> buffIds,
      int num,
      List<ConfigDataBuffInfo> buffList,
      List<int> excludeBuffIds = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArmyRelationData ComputeArmyRelationData(
      BattleActor attacker,
      bool attackerIsHero,
      BattleActor target,
      bool targetIsHero,
      bool isMagic,
      bool isUi = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      GridPosition start,
      GridPosition goal,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindMoveRegion(
      GridPosition start,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindAttackRegion(
      GridPosition start,
      int range,
      int shape,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindDirectionalAttackRegion(
      GridPosition start,
      AttackDirection direction,
      int range,
      int shape,
      int distance,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AttackDirection ComputeAttackDirection(
      GridPosition start,
      GridPosition target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindRingRegion(GridPosition start, int range, List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindFixedDistanceRegion(
      GridPosition start,
      int distance,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInRegion(GridPosition start, GridPosition target, int range, int shape)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInDangerRegion(int team, GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStars()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetPlayerActors(int teamId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<BattleActorLogInfo>> GetPlayerActorsLogInfos(
      int teamId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerKillActorCount(int teamId, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalKillActorCount(int teamId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetDeadEnemyActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossTotalDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    public string AliveActorsInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableChecksum(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetChecksums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeStepChecksum()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableDebugLog(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableDebugLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableErrorLog(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableErrorLog()
    {
      // ISSUE: unable to decompile the method.
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IBattleListener Listener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber RandomNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber AIRandomNumber1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber AIRandomNumber2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataBattleInfo BattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArenaBattleInfo ArenaBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataPVPBattleInfo PVPBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRealTimePVPBattleInfo RealTimePVPBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataPeakArenaBattleInfo PeakArenaBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataBattlefieldInfo BattlefieldInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleType BattleType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ActionOrderType ActionOrderType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattlePlayer[] BattlePlayers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataPlayerLevelInfo MaxPlayerLevelInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Step
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Turn
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TurnMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleMap Map
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnTurnEndOrWin(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnEventTrigger(BattleEventTriggerState eventState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnLose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeAchievementStatus(BattleAchievementState bs, bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAllAlive(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAllKillBySkillClass(List<int> heroIds, int skillClass)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorReachPositions(
      BattleActor a,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorReachPositions(
      BattleActor a,
      NpcCondition npcCondition,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAnyReachPositions(
      List<int> heroIds,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAnyReachPositions(
      int team,
      NpcCondition npcCondition,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetActorsCountSatisfyCondition(
      int team,
      NpcCondition npcCondition,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsActorInPositions(BattleActor a, List<ParamPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsSkillClassMatch(ConfigDataSkillInfo skillInfo, int skillClass)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsActorClassMatch(BattleActor actor, int actorClass)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAchievementComplete(int achievementReleatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetCompleteAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleConditionStatus GetAchievementStatus(int achievementReleatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleCommand(BattleCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLogBattleCommand(BattleCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGiveupLogBattleCommand(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCancelLogBattleCommand(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGiveupOrCancelLogBattleCommand(BattleCommandType cmdType, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddBattleCommandToList(LinkedList<BattleCommand> commands, BattleCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleCommandsAndNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> GetBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasCurrentStepBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasWrongStepBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteGiveupCommand(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveStepCommands(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RunCurrentStepBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RunCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Run(bool strictCheckCommand = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RunAIActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RunPlayerBattleCommands(int playerIndex, List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddExecutedCommand(BattleCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FixExecutedCommands(BattleActor actor, int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleCommand> GetStepExecutedCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableLogBattleCommands(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableLogBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> GetLogBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTempBattleCommands(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableTempBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> GetTempBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTempBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat(BattleActor actorA, BattleActor actorB, ConfigDataSkillInfo skillInfoA)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PostProcessCombatActorsDie(
      BattleActor actorA,
      BattleActor actorB,
      BattleActor beGrardActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RestartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CopyCombatProperty(CombatTeam combatTeam, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnActorRetreat(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnActorRetreat(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnEventTrigger(BattleEventTriggerState eventState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnEventTrigger(BattleEventTriggerState eventState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnTurnEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinLoseConditionOnTurnMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeWinConditionStatus(BattleWinConditionState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeLoseConditionStatus(BattleLoseConditionState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorActionBegin(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorCombatAttackBefore(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorCombatAttackAfter(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorBeCombatAttack(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckEventTriggerOnActorUseSkill(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnCompleteAchievement(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnLose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnTurnEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckBattleStopEventTrigger()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckMultiEventTrigger(int subTriggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckVariableEventTrigger(int variableId, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEventTriggerd(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleEventTriggerState GetEventTriggerState(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetEventTriggerState(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DisableEventTriggerState(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TriggerEvent(BattleEventTriggerState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteTriggerEventActions(BattleEventTriggerState state, int fromIdx = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Relief(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Retreat(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Dialog(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ChangeTeam(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AttachBuff(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ChangeBehavior(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Music(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ChangeTerrain(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_TerrainEffect(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Perform(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_RetreatPosition(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Replace(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_RemoveBuff(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AttachBuffPosition(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Teleport(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ResetEventTrigger(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_DisableEventTrigger(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_SetVariable(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AddVariable(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_RandomAction(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEmptyPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<RandomArmyActor> GetRandomBattleArmyActors(
      List<int> randomArmies)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CreateEventActor(
      GridPosition p,
      int dir,
      int team,
      bool isNpc,
      int heroId,
      int heroLevel,
      int behaviorId,
      int groupId,
      int effectType,
      string fxName,
      BattleActorSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ReplaceEventActor(BattleActor a, int heroId, int heroLevel, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEventVariable(int variableId, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetEventVariable(int variableId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindEmptyFixedDistancePosition(
      GridPosition target,
      int distance,
      MoveType moveType,
      bool isRandom = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindEmptyFixedDistancePosition(
      GridPosition start,
      GridPosition target,
      int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition FindEmptyNearPosition(
      GridPosition target,
      MoveType moveType,
      bool isRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CreateMap(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyPlayerTeamNumber(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMyPlayerTeamNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitBattle(
      ConfigDataBattleInfo battleInfo,
      BattleType battleType,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int armyRandomSeed,
      int monsterLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitArenaBattle(
      ConfigDataArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      ConfigDataArenaDefendRuleInfo arenaDefendRuleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPVPBattle(
      ConfigDataPVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitRealTimePVPBattle(
      ConfigDataRealTimePVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPeakArenaBattle(
      ConfigDataPeakArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRandomNumbers(int randomSeed, int armyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayers(BattlePlayer[] players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWinConditionStates(List<int> winConditionIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoseConditionStates(List<int> loseConditionIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitEventTriggerStates(List<int> triggerIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleTreasureStates(List<int> battleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateActors(int team, List<BattleActorSetup> setups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitBattleReport(
      ConfigDataBattleInfo battleInfo,
      BattleType battleType,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int armyRandomSeed,
      int monsterLevel,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitArenaBattleReport(
      ConfigDataArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      ConfigDataArenaDefendRuleInfo arenaDefendRuleInfo,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPVPBattleReport(
      ConfigDataPVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitRealTimePVPBattleReport(
      ConfigDataRealTimePVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPeakArenaBattleReport(
      ConfigDataPeakArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStarAndAchievement(
      int starTurnMax,
      int starDeadMax,
      List<ConfigDataBattleAchievementRelatedInfo> achievementRelatedInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievement(
      List<ConfigDataBattleAchievementRelatedInfo> achievementRelatedInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGainBattleTreasures(List<int> gainBattleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerforms(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteAfterCombatSteps()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_Dialog(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlayMusic(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlaySound(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlayFx(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlayActorFx(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ChangeTerrain(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_TerrainEffect(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CameraFocusPosition(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CameraFocusActor(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CreateActor(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CreateActorNear(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_RemoveActor(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorMove(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorMoveNear(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorAttack(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorSkill(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorDir(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorAnimation(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorIdle(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_WaitTime(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_StopBattle(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckBattleTreasureOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeBattleTeasureStatus(BattleTreasureState state, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGainBattleTreasure(int treasureId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGainBattleTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleBase m_owner;

      public LuaExportHelper(BattleBase owner)
      {
        this.m_owner = owner;
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public IBattleListener m_battleListener
      {
        get
        {
          return this.m_owner.m_battleListener;
        }
        set
        {
          this.m_owner.m_battleListener = value;
        }
      }

      public RandomNumber m_randomNumber
      {
        get
        {
          return this.m_owner.m_randomNumber;
        }
        set
        {
          this.m_owner.m_randomNumber = value;
        }
      }

      public RandomNumber m_aiRandomNumber1
      {
        get
        {
          return this.m_owner.m_aiRandomNumber1;
        }
        set
        {
          this.m_owner.m_aiRandomNumber1 = value;
        }
      }

      public RandomNumber m_aiRandomNumber2
      {
        get
        {
          return this.m_owner.m_aiRandomNumber2;
        }
        set
        {
          this.m_owner.m_aiRandomNumber2 = value;
        }
      }

      public RandomNumber m_armyRandomNumber
      {
        get
        {
          return this.m_owner.m_armyRandomNumber;
        }
        set
        {
          this.m_owner.m_armyRandomNumber = value;
        }
      }

      public BattleMap m_map
      {
        get
        {
          return this.m_owner.m_map;
        }
        set
        {
          this.m_owner.m_map = value;
        }
      }

      public Pathfinder m_pathfinder
      {
        get
        {
          return this.m_owner.m_pathfinder;
        }
        set
        {
          this.m_owner.m_pathfinder = value;
        }
      }

      public Combat m_combat
      {
        get
        {
          return this.m_owner.m_combat;
        }
        set
        {
          this.m_owner.m_combat = value;
        }
      }

      public ConfigDataBattlefieldInfo m_battlefieldInfo
      {
        get
        {
          return this.m_owner.m_battlefieldInfo;
        }
        set
        {
          this.m_owner.m_battlefieldInfo = value;
        }
      }

      public ConfigDataBattleInfo m_battleInfo
      {
        get
        {
          return this.m_owner.m_battleInfo;
        }
        set
        {
          this.m_owner.m_battleInfo = value;
        }
      }

      public ConfigDataArenaBattleInfo m_arenaBattleInfo
      {
        get
        {
          return this.m_owner.m_arenaBattleInfo;
        }
        set
        {
          this.m_owner.m_arenaBattleInfo = value;
        }
      }

      public ConfigDataPVPBattleInfo m_pvpBattleInfo
      {
        get
        {
          return this.m_owner.m_pvpBattleInfo;
        }
        set
        {
          this.m_owner.m_pvpBattleInfo = value;
        }
      }

      public ConfigDataRealTimePVPBattleInfo m_realtimePvpBattleInfo
      {
        get
        {
          return this.m_owner.m_realtimePvpBattleInfo;
        }
        set
        {
          this.m_owner.m_realtimePvpBattleInfo = value;
        }
      }

      public ConfigDataPeakArenaBattleInfo m_peakArenaBattleInfo
      {
        get
        {
          return this.m_owner.m_peakArenaBattleInfo;
        }
        set
        {
          this.m_owner.m_peakArenaBattleInfo = value;
        }
      }

      public BattleType m_battleType
      {
        get
        {
          return this.m_owner.m_battleType;
        }
        set
        {
          this.m_owner.m_battleType = value;
        }
      }

      public int m_monsterLevel
      {
        get
        {
          return this.m_owner.m_monsterLevel;
        }
        set
        {
          this.m_owner.m_monsterLevel = value;
        }
      }

      public BattleTeam[] m_teams
      {
        get
        {
          return this.m_owner.m_teams;
        }
        set
        {
          this.m_owner.m_teams = value;
        }
      }

      public BattlePlayer[] m_players
      {
        get
        {
          return this.m_owner.m_players;
        }
        set
        {
          this.m_owner.m_players = value;
        }
      }

      public ConfigDataPlayerLevelInfo m_maxPlayerLevelInfo
      {
        get
        {
          return this.m_owner.m_maxPlayerLevelInfo;
        }
        set
        {
          this.m_owner.m_maxPlayerLevelInfo = value;
        }
      }

      public BattleState m_state
      {
        get
        {
          return this.m_owner.m_state;
        }
        set
        {
          this.m_owner.m_state = value;
        }
      }

      public int m_step
      {
        get
        {
          return this.m_owner.m_step;
        }
        set
        {
          this.m_owner.m_step = value;
        }
      }

      public int m_turn
      {
        get
        {
          return this.m_owner.m_turn;
        }
        set
        {
          this.m_owner.m_turn = value;
        }
      }

      public int m_turnMax
      {
        get
        {
          return this.m_owner.m_turnMax;
        }
        set
        {
          this.m_owner.m_turnMax = value;
        }
      }

      public int m_actionTeam
      {
        get
        {
          return this.m_owner.m_actionTeam;
        }
        set
        {
          this.m_owner.m_actionTeam = value;
        }
      }

      public int m_prevActionPlayerIndex
      {
        get
        {
          return this.m_owner.m_prevActionPlayerIndex;
        }
        set
        {
          this.m_owner.m_prevActionPlayerIndex = value;
        }
      }

      public BattleActor m_actionActor
      {
        get
        {
          return this.m_owner.m_actionActor;
        }
        set
        {
          this.m_owner.m_actionActor = value;
        }
      }

      public BattleActor m_execCommandActor
      {
        get
        {
          return this.m_owner.m_execCommandActor;
        }
        set
        {
          this.m_owner.m_execCommandActor = value;
        }
      }

      public BattleActor m_checkingDieActor
      {
        get
        {
          return this.m_owner.m_checkingDieActor;
        }
        set
        {
          this.m_owner.m_checkingDieActor = value;
        }
      }

      public ActionOrderType m_actionOrderType
      {
        get
        {
          return this.m_owner.m_actionOrderType;
        }
        set
        {
          this.m_owner.m_actionOrderType = value;
        }
      }

      public int m_entityIdCounter
      {
        get
        {
          return this.m_owner.m_entityIdCounter;
        }
        set
        {
          this.m_owner.m_entityIdCounter = value;
        }
      }

      public bool m_needCheckBattleStopEventTrigger
      {
        get
        {
          return this.m_owner.m_needCheckBattleStopEventTrigger;
        }
        set
        {
          this.m_owner.m_needCheckBattleStopEventTrigger = value;
        }
      }

      public bool m_isGiveupBattle
      {
        get
        {
          return this.m_owner.m_isGiveupBattle;
        }
        set
        {
          this.m_owner.m_isGiveupBattle = value;
        }
      }

      public int m_giveupBattlePlayerIndex
      {
        get
        {
          return this.m_owner.m_giveupBattlePlayerIndex;
        }
        set
        {
          this.m_owner.m_giveupBattlePlayerIndex = value;
        }
      }

      public int m_combatRandomSeed
      {
        get
        {
          return this.m_owner.m_combatRandomSeed;
        }
        set
        {
          this.m_owner.m_combatRandomSeed = value;
        }
      }

      public BattleActor m_combatActorA
      {
        get
        {
          return this.m_owner.m_combatActorA;
        }
        set
        {
          this.m_owner.m_combatActorA = value;
        }
      }

      public BattleActor m_combatActorB
      {
        get
        {
          return this.m_owner.m_combatActorB;
        }
        set
        {
          this.m_owner.m_combatActorB = value;
        }
      }

      public ConfigDataSkillInfo m_combatSkillInfoA
      {
        get
        {
          return this.m_owner.m_combatSkillInfoA;
        }
        set
        {
          this.m_owner.m_combatSkillInfoA = value;
        }
      }

      public BattleActor m_beGuardedCombatActor
      {
        get
        {
          return this.m_owner.m_beGuardedCombatActor;
        }
        set
        {
          this.m_owner.m_beGuardedCombatActor = value;
        }
      }

      public int m_enterCombatHealthPointPercentA
      {
        get
        {
          return this.m_owner.m_enterCombatHealthPointPercentA;
        }
        set
        {
          this.m_owner.m_enterCombatHealthPointPercentA = value;
        }
      }

      public int m_enterCombatHealthPointPercentB
      {
        get
        {
          return this.m_owner.m_enterCombatHealthPointPercentB;
        }
        set
        {
          this.m_owner.m_enterCombatHealthPointPercentB = value;
        }
      }

      public List<AfterCombatApplyBuff> m_afterCombatApplyBuffList
      {
        get
        {
          return this.m_owner.m_afterCombatApplyBuffList;
        }
        set
        {
          this.m_owner.m_afterCombatApplyBuffList = value;
        }
      }

      public bool m_isPerformCombat
      {
        get
        {
          return this.m_owner.m_isPerformCombat;
        }
        set
        {
          this.m_owner.m_isPerformCombat = value;
        }
      }

      public ConfigDataBattlePerformInfo m_afterComatPerform
      {
        get
        {
          return this.m_owner.m_afterComatPerform;
        }
        set
        {
          this.m_owner.m_afterComatPerform = value;
        }
      }

      public BattleEventTriggerState m_afterCombatExecuteTriggerState
      {
        get
        {
          return this.m_owner.m_afterCombatExecuteTriggerState;
        }
        set
        {
          this.m_owner.m_afterCombatExecuteTriggerState = value;
        }
      }

      public int m_afterCombatExecuteTriggerEventActionIndex
      {
        get
        {
          return this.m_owner.m_afterCombatExecuteTriggerEventActionIndex;
        }
        set
        {
          this.m_owner.m_afterCombatExecuteTriggerEventActionIndex = value;
        }
      }

      public bool m_isAfterCombatNextStep
      {
        get
        {
          return this.m_owner.m_isAfterCombatNextStep;
        }
        set
        {
          this.m_owner.m_isAfterCombatNextStep = value;
        }
      }

      public bool m_isAfterCombatNextStepActorActive
      {
        get
        {
          return this.m_owner.m_isAfterCombatNextStepActorActive;
        }
        set
        {
          this.m_owner.m_isAfterCombatNextStepActorActive = value;
        }
      }

      public ListPool<int> m_tempIntListPool
      {
        get
        {
          return this.m_owner.m_tempIntListPool;
        }
        set
        {
          this.m_owner.m_tempIntListPool = value;
        }
      }

      public ListPool<GridPosition> m_tempGridPositionListPool
      {
        get
        {
          return this.m_owner.m_tempGridPositionListPool;
        }
        set
        {
          this.m_owner.m_tempGridPositionListPool = value;
        }
      }

      public ListPool<BattleActor> m_tempActorListPool
      {
        get
        {
          return this.m_owner.m_tempActorListPool;
        }
        set
        {
          this.m_owner.m_tempActorListPool = value;
        }
      }

      public ListPool<ConfigDataBuffInfo> m_tempBuffInfoListPool
      {
        get
        {
          return this.m_owner.m_tempBuffInfoListPool;
        }
        set
        {
          this.m_owner.m_tempBuffInfoListPool = value;
        }
      }

      public ListPool<BuffState> m_tempBuffStateListPool
      {
        get
        {
          return this.m_owner.m_tempBuffStateListPool;
        }
        set
        {
          this.m_owner.m_tempBuffStateListPool = value;
        }
      }

      public BattlePropertyModifier m_tempBattlePropertyModifier
      {
        get
        {
          return this.m_owner.m_tempBattlePropertyModifier;
        }
        set
        {
          this.m_owner.m_tempBattlePropertyModifier = value;
        }
      }

      public LinkedList<BattleCommand> m_battleCommands
      {
        get
        {
          return this.m_owner.m_battleCommands;
        }
        set
        {
          this.m_owner.m_battleCommands = value;
        }
      }

      public LinkedList<BattleCommand> m_logBattleCommands
      {
        get
        {
          return this.m_owner.m_logBattleCommands;
        }
        set
        {
          this.m_owner.m_logBattleCommands = value;
        }
      }

      public LinkedList<BattleCommand> m_tempBattleCommands
      {
        get
        {
          return this.m_owner.m_tempBattleCommands;
        }
        set
        {
          this.m_owner.m_tempBattleCommands = value;
        }
      }

      public List<BattleCommand> m_stepExecutedCommands
      {
        get
        {
          return this.m_owner.m_stepExecutedCommands;
        }
        set
        {
          this.m_owner.m_stepExecutedCommands = value;
        }
      }

      public bool m_enableLogBattleCommands
      {
        get
        {
          return this.m_owner.m_enableLogBattleCommands;
        }
        set
        {
          this.m_owner.m_enableLogBattleCommands = value;
        }
      }

      public bool m_enableTempBattleCommands
      {
        get
        {
          return this.m_owner.m_enableTempBattleCommands;
        }
        set
        {
          this.m_owner.m_enableTempBattleCommands = value;
        }
      }

      public bool m_enableDebugLog
      {
        get
        {
          return this.m_owner.m_enableDebugLog;
        }
        set
        {
          this.m_owner.m_enableDebugLog = value;
        }
      }

      public bool m_enableErrorLog
      {
        get
        {
          return this.m_owner.m_enableErrorLog;
        }
        set
        {
          this.m_owner.m_enableErrorLog = value;
        }
      }

      public bool m_enableChecksum
      {
        get
        {
          return this.m_owner.m_enableChecksum;
        }
        set
        {
          this.m_owner.m_enableChecksum = value;
        }
      }

      public List<int> m_checksums
      {
        get
        {
          return this.m_owner.m_checksums;
        }
        set
        {
          this.m_owner.m_checksums = value;
        }
      }

      public List<BattleWinConditionState> m_winConditionStates
      {
        get
        {
          return this.m_owner.m_winConditionStates;
        }
        set
        {
          this.m_owner.m_winConditionStates = value;
        }
      }

      public List<BattleLoseConditionState> m_loseConditionStates
      {
        get
        {
          return this.m_owner.m_loseConditionStates;
        }
        set
        {
          this.m_owner.m_loseConditionStates = value;
        }
      }

      public List<BattleAchievementState> m_achievementStates
      {
        get
        {
          return this.m_owner.m_achievementStates;
        }
        set
        {
          this.m_owner.m_achievementStates = value;
        }
      }

      public List<BattleEventTriggerState> m_eventTriggerStates
      {
        get
        {
          return this.m_owner.m_eventTriggerStates;
        }
        set
        {
          this.m_owner.m_eventTriggerStates = value;
        }
      }

      public List<BattleTreasureState> m_battleTreasureStates
      {
        get
        {
          return this.m_owner.m_battleTreasureStates;
        }
        set
        {
          this.m_owner.m_battleTreasureStates = value;
        }
      }

      public List<BattleActor> m_needPostProcessDieActors
      {
        get
        {
          return this.m_owner.m_needPostProcessDieActors;
        }
        set
        {
          this.m_owner.m_needPostProcessDieActors = value;
        }
      }

      public Dictionary<int, int> m_eventVariables
      {
        get
        {
          return this.m_owner.m_eventVariables;
        }
        set
        {
          this.m_owner.m_eventVariables = value;
        }
      }

      public int m_stars
      {
        get
        {
          return this.m_owner.m_stars;
        }
        set
        {
          this.m_owner.m_stars = value;
        }
      }

      public int m_starTurnMax
      {
        get
        {
          return this.m_owner.m_starTurnMax;
        }
        set
        {
          this.m_owner.m_starTurnMax = value;
        }
      }

      public int m_starDeadMax
      {
        get
        {
          return this.m_owner.m_starDeadMax;
        }
        set
        {
          this.m_owner.m_starDeadMax = value;
        }
      }

      public int m_myPlayerTeam
      {
        get
        {
          return this.m_owner.m_myPlayerTeam;
        }
        set
        {
          this.m_owner.m_myPlayerTeam = value;
        }
      }

      public int m_cheatPassiveSkillId
      {
        get
        {
          return this.m_owner.m_cheatPassiveSkillId;
        }
        set
        {
          this.m_owner.m_cheatPassiveSkillId = value;
        }
      }

      public void Stop(bool win)
      {
        this.m_owner.Stop(win);
      }

      public void NextStep()
      {
        this.m_owner.NextStep();
      }

      public void NextStep_Normal(BattleActor actionActor)
      {
        this.m_owner.NextStep_Normal(actionActor);
      }

      public void NextStep_Pvp(BattleActor actionActor)
      {
        this.m_owner.NextStep_Pvp(actionActor);
      }

      public void NextStepActorActive()
      {
        this.m_owner.NextStepActorActive();
      }

      public void NextTurn()
      {
        this.m_owner.NextTurn();
      }

      public BattleActor GetDefaultActionActor(int team)
      {
        return this.m_owner.GetDefaultActionActor(team);
      }

      public bool HasNotActionFinishedActor()
      {
        return this.m_owner.HasNotActionFinishedActor();
      }

      public BattleActor GetAliveActorByHeroId(int heroId)
      {
        return this.m_owner.GetAliveActorByHeroId(heroId);
      }

      public void GetAliveActorsByHeroIdsAndGroupIds(
        List<int> heroIds,
        List<int> groupIds,
        List<BattleActor> actors)
      {
        this.m_owner.GetAliveActorsByHeroIdsAndGroupIds(heroIds, groupIds, actors);
      }

      public void ComputeStepChecksum()
      {
        this.m_owner.ComputeStepChecksum();
      }

      public void CheckAchievementOnActorMove(BattleActor actor)
      {
        this.m_owner.CheckAchievementOnActorMove(actor);
      }

      public void CheckAchievementOnActorDie(BattleActor actor)
      {
        this.m_owner.CheckAchievementOnActorDie(actor);
      }

      public void CheckAchievementOnNextStep()
      {
        this.m_owner.CheckAchievementOnNextStep();
      }

      public void CheckAchievementOnTurnEndOrWin(bool isWin)
      {
        this.m_owner.CheckAchievementOnTurnEndOrWin(isWin);
      }

      public void CheckAchievementOnWin()
      {
        this.m_owner.CheckAchievementOnWin();
      }

      public void CheckAchievementOnEventTrigger(BattleEventTriggerState eventState)
      {
        this.m_owner.CheckAchievementOnEventTrigger(eventState);
      }

      public void CheckAchievementOnLose()
      {
        this.m_owner.CheckAchievementOnLose();
      }

      public void ChangeAchievementStatus(BattleAchievementState bs, bool success)
      {
        this.m_owner.ChangeAchievementStatus(bs, success);
      }

      public bool IsActorsAllAlive(List<int> heroIds)
      {
        return this.m_owner.IsActorsAllAlive(heroIds);
      }

      public bool IsActorsAllKillBySkillClass(List<int> heroIds, int skillClass)
      {
        return this.m_owner.IsActorsAllKillBySkillClass(heroIds, skillClass);
      }

      public bool IsActorReachPositions(
        BattleActor a,
        List<ParamPosition> positions,
        int conditionId)
      {
        return this.m_owner.IsActorReachPositions(a, positions, conditionId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool IsActorReachPositions(
        BattleActor a,
        NpcCondition npcCondition,
        List<ParamPosition> positions,
        int conditionId)
      {
        // ISSUE: unable to decompile the method.
      }

      public bool IsActorsAnyReachPositions(
        List<int> heroIds,
        List<ParamPosition> positions,
        int conditionId)
      {
        return this.m_owner.IsActorsAnyReachPositions(heroIds, positions, conditionId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool IsActorsAnyReachPositions(
        int team,
        NpcCondition npcCondition,
        List<ParamPosition> positions,
        int conditionId)
      {
        // ISSUE: unable to decompile the method.
      }

      public int GetActorsCountSatisfyCondition(
        int team,
        NpcCondition npcCondition,
        int conditionId)
      {
        return this.m_owner.GetActorsCountSatisfyCondition(team, npcCondition, conditionId);
      }

      public static bool IsActorInPositions(BattleActor a, List<ParamPosition> positions)
      {
        return BattleBase.IsActorInPositions(a, positions);
      }

      public static bool IsSkillClassMatch(ConfigDataSkillInfo skillInfo, int skillClass)
      {
        return BattleBase.IsSkillClassMatch(skillInfo, skillClass);
      }

      public static bool IsActorClassMatch(BattleActor actor, int actorClass)
      {
        return BattleBase.IsActorClassMatch(actor, actorClass);
      }

      public void AddLogBattleCommand(BattleCommand cmd)
      {
        this.m_owner.AddLogBattleCommand(cmd);
      }

      public void AddGiveupOrCancelLogBattleCommand(BattleCommandType cmdType, int playerIndex)
      {
        this.m_owner.AddGiveupOrCancelLogBattleCommand(cmdType, playerIndex);
      }

      public void AddBattleCommandToList(LinkedList<BattleCommand> commands, BattleCommand command)
      {
        this.m_owner.AddBattleCommandToList(commands, command);
      }

      public bool HasWrongStepBattleCommand()
      {
        return this.m_owner.HasWrongStepBattleCommand();
      }

      public void ExecuteGiveupCommand(int playerIndex)
      {
        this.m_owner.ExecuteGiveupCommand(playerIndex);
      }

      public void RemoveStepCommands(int step)
      {
        this.m_owner.RemoveStepCommands(step);
      }

      public bool RunCurrentStepBattleCommand()
      {
        return this.m_owner.RunCurrentStepBattleCommand();
      }

      public bool RunCombat()
      {
        return this.m_owner.RunCombat();
      }

      public void AddExecutedCommand(BattleCommand cmd)
      {
        this.m_owner.AddExecutedCommand(cmd);
      }

      public void StopCombat()
      {
        this.m_owner.StopCombat();
      }

      public void PostProcessCombatActorsDie(
        BattleActor actorA,
        BattleActor actorB,
        BattleActor beGrardActor)
      {
        this.m_owner.PostProcessCombatActorsDie(actorA, actorB, beGrardActor);
      }

      public void CopyCombatProperty(CombatTeam combatTeam, BattleActor actor)
      {
        this.m_owner.CopyCombatProperty(combatTeam, actor);
      }

      public void CheckWinConditionOnActorMove(BattleActor actor)
      {
        this.m_owner.CheckWinConditionOnActorMove(actor);
      }

      public void CheckLoseConditionOnActorMove(BattleActor actor)
      {
        this.m_owner.CheckLoseConditionOnActorMove(actor);
      }

      public void CheckWinConditionOnActorDie(BattleActor actor)
      {
        this.m_owner.CheckWinConditionOnActorDie(actor);
      }

      public void CheckLoseConditionOnActorDie(BattleActor actor)
      {
        this.m_owner.CheckLoseConditionOnActorDie(actor);
      }

      public void CheckWinConditionOnActorRetreat(BattleActor actor)
      {
        this.m_owner.CheckWinConditionOnActorRetreat(actor);
      }

      public void CheckLoseConditionOnActorRetreat(BattleActor actor)
      {
        this.m_owner.CheckLoseConditionOnActorRetreat(actor);
      }

      public void CheckWinConditionOnEventTrigger(BattleEventTriggerState eventState)
      {
        this.m_owner.CheckWinConditionOnEventTrigger(eventState);
      }

      public void CheckLoseConditionOnEventTrigger(BattleEventTriggerState eventState)
      {
        this.m_owner.CheckLoseConditionOnEventTrigger(eventState);
      }

      public void CheckWinConditionOnNextStep()
      {
        this.m_owner.CheckWinConditionOnNextStep();
      }

      public void CheckWinConditionOnTurnEnd()
      {
        this.m_owner.CheckWinConditionOnTurnEnd();
      }

      public void CheckWinLoseConditionOnTurnMax()
      {
        this.m_owner.CheckWinLoseConditionOnTurnMax();
      }

      public void ChangeWinConditionStatus(BattleWinConditionState state)
      {
        this.m_owner.ChangeWinConditionStatus(state);
      }

      public void ChangeLoseConditionStatus(BattleLoseConditionState state)
      {
        this.m_owner.ChangeLoseConditionStatus(state);
      }

      public void CheckEventTriggerOnActorMove(BattleActor actor)
      {
        this.m_owner.CheckEventTriggerOnActorMove(actor);
      }

      public void CheckEventTriggerOnActorDie(BattleActor actor)
      {
        this.m_owner.CheckEventTriggerOnActorDie(actor);
      }

      public void CheckEventTriggerOnActorActionBegin(BattleActor a)
      {
        this.m_owner.CheckEventTriggerOnActorActionBegin(a);
      }

      public void CheckEventTriggerOnActorCombatAttackBefore(BattleActor a, BattleActor target)
      {
        this.m_owner.CheckEventTriggerOnActorCombatAttackBefore(a, target);
      }

      public void CheckEventTriggerOnActorCombatAttackAfter(BattleActor a, BattleActor target)
      {
        this.m_owner.CheckEventTriggerOnActorCombatAttackAfter(a, target);
      }

      public void CheckEventTriggerOnActorBeCombatAttack(BattleActor a)
      {
        this.m_owner.CheckEventTriggerOnActorBeCombatAttack(a);
      }

      public void CheckEventTriggerOnNextStep()
      {
        this.m_owner.CheckEventTriggerOnNextStep();
      }

      public void CheckEventTriggerOnNextTeam(int team, bool isNpc)
      {
        this.m_owner.CheckEventTriggerOnNextTeam(team, isNpc);
      }

      public void CheckEventTriggerOnCompleteAchievement(int achievementId)
      {
        this.m_owner.CheckEventTriggerOnCompleteAchievement(achievementId);
      }

      public void CheckEventTriggerOnWin()
      {
        this.m_owner.CheckEventTriggerOnWin();
      }

      public void CheckEventTriggerOnLose()
      {
        this.m_owner.CheckEventTriggerOnLose();
      }

      public void CheckEventTriggerOnTurnEnd()
      {
        this.m_owner.CheckEventTriggerOnTurnEnd();
      }

      public void CheckBattleStopEventTrigger()
      {
        this.m_owner.CheckBattleStopEventTrigger();
      }

      public void CheckMultiEventTrigger(int subTriggerId)
      {
        this.m_owner.CheckMultiEventTrigger(subTriggerId);
      }

      public void CheckVariableEventTrigger(int variableId, int value)
      {
        this.m_owner.CheckVariableEventTrigger(variableId, value);
      }

      public bool IsEventTriggerd(int triggerId)
      {
        return this.m_owner.IsEventTriggerd(triggerId);
      }

      public BattleEventTriggerState GetEventTriggerState(int triggerId)
      {
        return this.m_owner.GetEventTriggerState(triggerId);
      }

      public void ResetEventTriggerState(int triggerId)
      {
        this.m_owner.ResetEventTriggerState(triggerId);
      }

      public void DisableEventTriggerState(int triggerId)
      {
        this.m_owner.DisableEventTriggerState(triggerId);
      }

      public void TriggerEvent(BattleEventTriggerState state)
      {
        this.m_owner.TriggerEvent(state);
      }

      public void ExecuteTriggerEventActions(BattleEventTriggerState state, int fromIdx)
      {
        this.m_owner.ExecuteTriggerEventActions(state, fromIdx);
      }

      public void ExecuteEventAction(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction(e);
      }

      public void ExecuteEventAction_Relief(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Relief(e);
      }

      public void ExecuteEventAction_Retreat(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Retreat(e);
      }

      public void ExecuteEventAction_Dialog(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Dialog(e);
      }

      public void ExecuteEventAction_ChangeTeam(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_ChangeTeam(e);
      }

      public void ExecuteEventAction_AttachBuff(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_AttachBuff(e);
      }

      public void ExecuteEventAction_ChangeBehavior(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_ChangeBehavior(e);
      }

      public void ExecuteEventAction_Music(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Music(e);
      }

      public void ExecuteEventAction_ChangeTerrain(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_ChangeTerrain(e);
      }

      public void ExecuteEventAction_TerrainEffect(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_TerrainEffect(e);
      }

      public void ExecuteEventAction_Perform(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Perform(e);
      }

      public void ExecuteEventAction_RetreatPosition(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_RetreatPosition(e);
      }

      public void ExecuteEventAction_Replace(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Replace(e);
      }

      public void ExecuteEventAction_RemoveBuff(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_RemoveBuff(e);
      }

      public void ExecuteEventAction_AttachBuffPosition(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_AttachBuffPosition(e);
      }

      public void ExecuteEventAction_Teleport(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_Teleport(e);
      }

      public void ExecuteEventAction_ResetEventTrigger(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_ResetEventTrigger(e);
      }

      public void ExecuteEventAction_DisableEventTrigger(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_DisableEventTrigger(e);
      }

      public void ExecuteEventAction_SetVariable(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_SetVariable(e);
      }

      public void ExecuteEventAction_AddVariable(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_AddVariable(e);
      }

      public void ExecuteEventAction_RandomAction(ConfigDataBattleEventActionInfo e)
      {
        this.m_owner.ExecuteEventAction_RandomAction(e);
      }

      public bool IsEmptyPosition(GridPosition p)
      {
        return this.m_owner.IsEmptyPosition(p);
      }

      public List<RandomArmyActor> GetRandomBattleArmyActors(
        List<int> randomArmies)
      {
        return this.m_owner.GetRandomBattleArmyActors(randomArmies);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool CreateEventActor(
        GridPosition p,
        int dir,
        int team,
        bool isNpc,
        int heroId,
        int heroLevel,
        int behaviorId,
        int groupId,
        int effectType,
        string fxName,
        BattleActorSourceType sourceType)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool ReplaceEventActor(BattleActor a, int heroId, int heroLevel, string fxName)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetEventVariable(int variableId, int value)
      {
        this.m_owner.SetEventVariable(variableId, value);
      }

      public int GetEventVariable(int variableId)
      {
        return this.m_owner.GetEventVariable(variableId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public GridPosition FindEmptyFixedDistancePosition(
        GridPosition target,
        int distance,
        MoveType moveType,
        bool isRandom)
      {
        // ISSUE: unable to decompile the method.
      }

      public GridPosition FindEmptyFixedDistancePosition(
        GridPosition start,
        GridPosition target,
        int distance)
      {
        return this.m_owner.FindEmptyFixedDistancePosition(start, target, distance);
      }

      public void InitRandomNumbers(int randomSeed, int armyRandomSeed)
      {
        this.m_owner.InitRandomNumbers(randomSeed, armyRandomSeed);
      }

      public void InitPlayers(BattlePlayer[] players)
      {
        this.m_owner.InitPlayers(players);
      }

      public void InitWinConditionStates(List<int> winConditionIds)
      {
        this.m_owner.InitWinConditionStates(winConditionIds);
      }

      public void InitLoseConditionStates(List<int> loseConditionIds)
      {
        this.m_owner.InitLoseConditionStates(loseConditionIds);
      }

      public void InitEventTriggerStates(List<int> triggerIds)
      {
        this.m_owner.InitEventTriggerStates(triggerIds);
      }

      public void InitBattleTreasureStates(List<int> battleTreasureIds)
      {
        this.m_owner.InitBattleTreasureStates(battleTreasureIds);
      }

      public void CreateActors(int team, List<BattleActorSetup> setups)
      {
        this.m_owner.CreateActors(team, setups);
      }

      public void ExecuteBattlePerforms(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerforms(performInfo);
      }

      public void ExecuteAfterCombatSteps()
      {
        this.m_owner.ExecuteAfterCombatSteps();
      }

      public void ExecuteBattlePerform(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform(performInfo);
      }

      public void ExecuteBattlePerform_Dialog(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_Dialog(performInfo);
      }

      public void ExecuteBattlePerform_PlayMusic(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_PlayMusic(performInfo);
      }

      public void ExecuteBattlePerform_PlaySound(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_PlaySound(performInfo);
      }

      public void ExecuteBattlePerform_PlayFx(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_PlayFx(performInfo);
      }

      public void ExecuteBattlePerform_PlayActorFx(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_PlayActorFx(performInfo);
      }

      public void ExecuteBattlePerform_ChangeTerrain(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ChangeTerrain(performInfo);
      }

      public void ExecuteBattlePerform_TerrainEffect(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_TerrainEffect(performInfo);
      }

      public void ExecuteBattlePerform_CameraFocusPosition(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_CameraFocusPosition(performInfo);
      }

      public void ExecuteBattlePerform_CameraFocusActor(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_CameraFocusActor(performInfo);
      }

      public void ExecuteBattlePerform_CreateActor(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_CreateActor(performInfo);
      }

      public void ExecuteBattlePerform_CreateActorNear(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_CreateActorNear(performInfo);
      }

      public void ExecuteBattlePerform_RemoveActor(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_RemoveActor(performInfo);
      }

      public void ExecuteBattlePerform_ActorMove(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorMove(performInfo);
      }

      public void ExecuteBattlePerform_ActorMoveNear(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorMoveNear(performInfo);
      }

      public void ExecuteBattlePerform_ActorAttack(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorAttack(performInfo);
      }

      public void ExecuteBattlePerform_ActorSkill(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorSkill(performInfo);
      }

      public void ExecuteBattlePerform_ActorDir(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorDir(performInfo);
      }

      public void ExecuteBattlePerform_ActorAnimation(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorAnimation(performInfo);
      }

      public void ExecuteBattlePerform_ActorIdle(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_ActorIdle(performInfo);
      }

      public void ExecuteBattlePerform_WaitTime(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_WaitTime(performInfo);
      }

      public void ExecuteBattlePerform_StopBattle(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.ExecuteBattlePerform_StopBattle(performInfo);
      }

      public void CheckBattleTreasureOnActorMove(BattleActor actor)
      {
        this.m_owner.CheckBattleTreasureOnActorMove(actor);
      }

      public void ChangeBattleTeasureStatus(BattleTreasureState state, BattleActor actor)
      {
        this.m_owner.ChangeBattleTeasureStatus(state, actor);
      }
    }
  }
}
