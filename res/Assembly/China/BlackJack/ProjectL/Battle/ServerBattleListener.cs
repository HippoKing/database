﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ServerBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class ServerBattleListener : NullBattleListener
  {
    private BattleTimeEstimator m_battleTimeEstimator;

    [MethodImpl((MethodImplOptions) 32768)]
    public ServerBattleListener()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitTimeEstimator(BattleBase battle)
    {
      this.m_battleTimeEstimator.Init(battle);
    }

    public int GetEstimateTime(bool isFastBattle = true)
    {
      return this.m_battleTimeEstimator.GetTime(isFastBattle);
    }

    public void ClearEstimateTime()
    {
      this.m_battleTimeEstimator.ClearTime();
    }

    public override void OnBattleStart()
    {
      this.m_battleTimeEstimator.OnBattleStart();
    }

    public override void OnBattleNextTurn(int turn)
    {
      this.m_battleTimeEstimator.OnBattleNextTurn(turn);
    }

    public override void OnBattleNextTeam(int team, bool isNpc)
    {
      this.m_battleTimeEstimator.OnBattleNextTeam(team, isNpc);
    }

    public override void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      this.m_battleTimeEstimator.OnBattleNextPlayer(prevPlayerIndex, playerIndex);
    }

    public override void OnBattleActorActive(BattleActor a, bool newStep)
    {
      this.m_battleTimeEstimator.OnBattleActorActive(a, newStep);
    }

    public override void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      this.m_battleTimeEstimator.OnBattleActorMove(a, p, dir);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      this.m_battleTimeEstimator.OnBattleActorPunchMove(a, fxName, isDragExchange);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnBattleActorPlayAnimation(
      BattleActor a,
      string animationName,
      int animationTime)
    {
      this.m_battleTimeEstimator.OnBattleActorPlayAnimation(a, animationName, animationTime);
    }

    public override void OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActors)
    {
      this.m_battleTimeEstimator.OnBattleActorSkill(a, skillInfo, p);
    }

    public override void OnBattleActorImmune(BattleActor a)
    {
      this.m_battleTimeEstimator.OnBattleActorImmune(a);
    }

    public override void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      this.m_battleTimeEstimator.OnBattleActorPassiveSkill(a, target, sourceBuffState);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnBattleActorTeleport(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      this.m_battleTimeEstimator.OnBattleActorTeleport(a, skillInfo, p);
    }

    public override void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      this.m_battleTimeEstimator.OnBattleActorSummon(a, skillInfo);
    }

    public override void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      this.m_battleTimeEstimator.OnBattleActorDie(a, isAfterCombat);
    }

    public override void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      this.m_battleTimeEstimator.OnBattleActorAppear(a, effectType, fxName);
    }

    public override void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      this.m_battleTimeEstimator.OnBattleActorDisappear(a, effectType, fxName);
    }

    public override void OnBattleActorChangeTeam(BattleActor a)
    {
      this.m_battleTimeEstimator.OnBattleActorChangeTeam(a);
    }

    public override void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      this.m_battleTimeEstimator.OnBattleActorReplace(a0, a1, fxName);
    }

    public override void OnBattleActorCameraFocus(BattleActor a)
    {
      this.m_battleTimeEstimator.OnBattleActorCameraFocus(a);
    }

    public override void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      this.m_battleTimeEstimator.OnBattleActorGainBattleTreasure(a, treasureInfo);
    }

    public override void OnStartGuard(BattleActor a, BattleActor target)
    {
      this.m_battleTimeEstimator.OnStartGuard(a, target);
    }

    public override void OnStopGuard(BattleActor a, BattleActor target)
    {
      this.m_battleTimeEstimator.OnStopGuard(a, target);
    }

    public override void OnStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      this.m_battleTimeEstimator.OnStartCombat(a, b, attackerSkillInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnCameraFocus(GridPosition p)
    {
      this.m_battleTimeEstimator.OnCameraFocus(p);
    }

    public override void OnWaitTime(int timeInMs)
    {
      this.m_battleTimeEstimator.OnWaitTime(timeInMs);
    }
  }
}
