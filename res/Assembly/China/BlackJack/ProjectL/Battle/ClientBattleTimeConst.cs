﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ClientBattleTimeConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class ClientBattleTimeConst
  {
    public const int PVPBattleStartCountdownTime = 6500;
    public const int StartCombatEffectTime = 500;
    public const int StopCombatEffectTime = 500;
    public const int SkillFadeTime = 300;
    public const int AnimBattleObjectiveTime = 2000;
    public const int AnimNextTurnTime = 1800;
    public const int AnimNextTeamTime = 2100;
    public const int AnimNextPlayerTime = 1800;
    public const int AnimRoundTime = 1600;
    public const int BattleStopDelay = 500;
    public const int ActorActStartCombatTime = 700;
    public const int ActorActStopCombatTime = 300;
    public const int ActorActImmuneTime = 500;
    public const int ActorActStartGuardDelay = 800;
    public const int ActorActStopGuardDelay = 300;
    public const int ActorActPassiveSkillTime = 1000;
    public const int ActorActPassiveSkillHitTime = 400;
    public const int ActorActSkillMultiHitTime = 400;
    public const int ActorActAppearTime = 500;
    public const int ActorActDisappearTime = 500;
    public const int ActorActTelelportDisappearDelayTime = 500;
    public const int ActorActTelelportAppearDelayTime = 500;
    public const int ActorActSummonDelayTime = 500;
    public const int ActorActChangeTeamTime = 500;
    public const int ActorActPunchMoveTime = 400;
    public const int ActorActExchangeMoveTime = 300;
    public const int ActorActReplaceTime = 500;
    public const int ActorActDieTime = 300;
    public const int ActorActDieHideTime = 200;
    public const int TargetIconTime = 500;
    public const float FastBattleTimeScale = 1.3f;
  }
}
