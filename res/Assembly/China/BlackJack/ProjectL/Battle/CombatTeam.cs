﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class CombatTeam
  {
    private Combat m_combat;
    private int m_teamNumber;
    private BattleActor m_battleActor;
    private List<CombatActor> m_actors;
    private List<CombatFlyObject> m_flyObjects;
    private static Position2i[] s_formationPositions;
    private ConfigDataSkillInfo m_heroSkillInfo;
    private bool m_shouldHeroToHeroCriticalAttack;
    private bool m_shouldHeroToSoldierCriticalAttack;
    private bool m_shouldSoldierToHeroCriticalAttack;
    private bool m_shouldSoldierToSoldierCriticalAttack;
    private bool m_isCastAnyDamageSkill;
    private bool m_isBeCriticalAttack;
    private ConfigDataSkillInfo m_lastDamageBySkillInfo;
    private int m_heroReceiveTotalDamage;
    private int m_soldierReceiveTotalDamage;
    private int m_heroApplyTotalDamage;
    private int m_soldierApplyTotalDamage;
    private int m_reboundPercent;
    private bool m_isTryApplyBuff;
    private ConfigDataSkillInfo m_attachBuffSourceSkillInfo;
    private BuffState m_doubleAttackBuffState;
    private bool m_isStun;
    private int m_fightStartDelay;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static CombatTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      Combat combat,
      int team,
      BattleActor battleActor,
      ConfigDataSkillInfo heroSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeHeroCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeSoldierCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAttackHeroOnly()
    {
      return this.m_battleActor.HasFightTag(FightTag.FightTag_OnlyAttackHero);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveDeleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Colori GetColor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor CreateActor(bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatFlyObject CreateFlyObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorCastSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorAttackBy(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      bool isCritical,
      ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorReboundDamage(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int hpReboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorFightAgain(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Position2i GetFormationPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFormationLine(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<CombatActor> GetActors()
    {
      return this.m_actors;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFirstStrike()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasAliveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetAliveActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAllAliveActorState(
      CombatActorState state1,
      CombatActorState state2,
      CombatActorState state3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam GetTargetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeSoldierTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataSkillInfo GetLastDamageBySkill()
    {
      return this.m_lastDamageBySkillInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReceiveAnyDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataSkillInfo GetAttachBuffSourceSkillInfo()
    {
      return this.m_attachBuffSourceSkillInfo;
    }

    public BuffState GetDoubleAttackBuff()
    {
      return this.m_doubleAttackBuffState;
    }

    public Combat Combat
    {
      get
      {
        return this.m_combat;
      }
    }

    public int TeamNumber
    {
      get
      {
        return this.m_teamNumber;
      }
    }

    public BattleActor BattleActor
    {
      get
      {
        return this.m_battleActor;
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      get
      {
        return this.m_battleActor.HeroInfo;
      }
    }

    public ConfigDataArmyInfo HeroArmyInfo
    {
      get
      {
        return this.m_battleActor.HeroArmyInfo;
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      get
      {
        return this.m_battleActor.JobConnectionInfo;
      }
    }

    public ConfigDataJobInfo JobInfo
    {
      get
      {
        return this.m_battleActor.JobInfo;
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      get
      {
        return this.m_battleActor.SoldierInfo;
      }
    }

    public ConfigDataArmyInfo SoldierArmyInfo
    {
      get
      {
        return this.m_battleActor.SoldierArmyInfo;
      }
    }

    public ConfigDataSkillInfo HeroSkillInfo
    {
      get
      {
        return this.m_heroSkillInfo;
      }
    }

    public int HeroLevel
    {
      get
      {
        return this.m_battleActor.HeroLevel;
      }
    }

    public int HeroStar
    {
      get
      {
        return this.m_battleActor.HeroStar;
      }
    }

    public int JobLevel
    {
      get
      {
        return this.m_battleActor.JobLevel;
      }
    }

    public bool ShouldHeroToHeroCriticalAttack
    {
      get
      {
        return this.m_shouldHeroToHeroCriticalAttack;
      }
    }

    public bool ShouldHeroToSoldierCriticalAttack
    {
      get
      {
        return this.m_shouldHeroToSoldierCriticalAttack;
      }
    }

    public bool ShouldSoldierToHeroCriticalAttack
    {
      get
      {
        return this.m_shouldSoldierToHeroCriticalAttack;
      }
    }

    public bool ShouldSoldierToSoldierCriticalAttack
    {
      get
      {
        return this.m_shouldSoldierToSoldierCriticalAttack;
      }
    }

    public bool IsStun
    {
      get
      {
        return this.m_isStun;
      }
    }

    public int FightStartDelay
    {
      get
      {
        return this.m_fightStartDelay;
      }
    }

    public bool IsCastAnyDamageSkill
    {
      get
      {
        return this.m_isCastAnyDamageSkill;
      }
    }

    public bool IsBeCriticalAttack
    {
      get
      {
        return this.m_isBeCriticalAttack;
      }
    }

    public int HeroReceiveTotalDamage
    {
      get
      {
        return this.m_heroReceiveTotalDamage;
      }
    }

    public int SoldierReceiveTotalDamage
    {
      get
      {
        return this.m_soldierReceiveTotalDamage;
      }
    }

    public int HeroApplyTotalDamage
    {
      get
      {
        return this.m_heroApplyTotalDamage;
      }
    }

    public int SoldierApplyTotalDamage
    {
      get
      {
        return this.m_soldierApplyTotalDamage;
      }
    }

    public int ReboundPercent
    {
      get
      {
        return this.m_reboundPercent;
      }
    }
  }
}
