﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.RankingListInfoQueryAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "RankingListInfoQueryAck")]
  [Serializable]
  public class RankingListInfoQueryAck : IExtensible
  {
    private int _Result;
    private int _RankingListType;
    private int _HeroId;
    private ProRankingListInfo _ListInfo;
    private int _BossId;
    private int _SeasonId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfoQueryAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RankingListType")]
    [DefaultValue(0)]
    public int RankingListType
    {
      get
      {
        return this._RankingListType;
      }
      set
      {
        this._RankingListType = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "HeroId")]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "ListInfo")]
    public ProRankingListInfo ListInfo
    {
      get
      {
        return this._ListInfo;
      }
      set
      {
        this._ListInfo = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BossId")]
    [DefaultValue(0)]
    public int BossId
    {
      get
      {
        return this._BossId;
      }
      set
      {
        this._BossId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "SeasonId")]
    [DefaultValue(0)]
    public int SeasonId
    {
      get
      {
        return this._SeasonId;
      }
      set
      {
        this._SeasonId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
