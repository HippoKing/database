﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSFriendNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSFriendNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSFriendNtf : IExtensible
  {
    private int _Likes;
    private readonly List<string> _LikedUsers;
    private readonly List<string> _FriendshipPointsSent;
    private readonly List<string> _FriendshipPointsReceived;
    private int _FriendshipPointsFromFightWithFriendsToday;
    private int _FriendshipPointsClaimedToday;
    private ProBusinessCardInfoSet _SetInfo;
    private uint _Version;
    private long _BannedTime;
    private readonly List<ulong> _HeroicMomentBattleReports;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSFriendNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Likes")]
    public int Likes
    {
      get
      {
        return this._Likes;
      }
      set
      {
        this._Likes = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "LikedUsers")]
    public List<string> LikedUsers
    {
      get
      {
        return this._LikedUsers;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "FriendshipPointsSent")]
    public List<string> FriendshipPointsSent
    {
      get
      {
        return this._FriendshipPointsSent;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "FriendshipPointsReceived")]
    public List<string> FriendshipPointsReceived
    {
      get
      {
        return this._FriendshipPointsReceived;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPointsFromFightWithFriendsToday")]
    public int FriendshipPointsFromFightWithFriendsToday
    {
      get
      {
        return this._FriendshipPointsFromFightWithFriendsToday;
      }
      set
      {
        this._FriendshipPointsFromFightWithFriendsToday = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPointsClaimedToday")]
    public int FriendshipPointsClaimedToday
    {
      get
      {
        return this._FriendshipPointsClaimedToday;
      }
      set
      {
        this._FriendshipPointsClaimedToday = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "SetInfo")]
    public ProBusinessCardInfoSet SetInfo
    {
      get
      {
        return this._SetInfo;
      }
      set
      {
        this._SetInfo = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BannedTime")]
    [DefaultValue(0)]
    public long BannedTime
    {
      get
      {
        return this._BannedTime;
      }
      set
      {
        this._BannedTime = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "HeroicMomentBattleReports")]
    public List<ulong> HeroicMomentBattleReports
    {
      get
      {
        return this._HeroicMomentBattleReports;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
