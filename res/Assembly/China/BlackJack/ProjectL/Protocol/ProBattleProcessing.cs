﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleProcessing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProBattleProcessing")]
  [Serializable]
  public class ProBattleProcessing : IExtensible
  {
    private int _Type;
    private int _TypeId;
    private int _RandomSeed;
    private int _ArmyRandomSeed;
    private readonly List<int> _Params;
    private readonly List<ulong> _ULongParams;
    private readonly List<string> _StrParams;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleProcessing()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Type")]
    public int Type
    {
      get
      {
        return this._Type;
      }
      set
      {
        this._Type = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TypeId")]
    public int TypeId
    {
      get
      {
        return this._TypeId;
      }
      set
      {
        this._TypeId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomSeed")]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArmyRandomSeed")]
    public int ArmyRandomSeed
    {
      get
      {
        return this._ArmyRandomSeed;
      }
      set
      {
        this._ArmyRandomSeed = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "Params")]
    public List<int> Params
    {
      get
      {
        return this._Params;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "ULongParams")]
    public List<ulong> ULongParams
    {
      get
      {
        return this._ULongParams;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "StrParams")]
    public List<string> StrParams
    {
      get
      {
        return this._StrParams;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
