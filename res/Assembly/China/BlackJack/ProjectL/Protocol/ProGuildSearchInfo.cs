﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuildSearchInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProGuildSearchInfo")]
  [Serializable]
  public class ProGuildSearchInfo : IExtensible
  {
    private string _Id;
    private string _Name;
    private int _Level;
    private int _JoinLevel;
    private int _LastWeekActivities;
    private string _HiringDeclaration;
    private ProGuildMember _PresidentMemberInfo;
    private bool _HaveSendJoinReq;
    private int _MemberCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildSearchInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Id")]
    public string Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JoinLevel")]
    public int JoinLevel
    {
      get
      {
        return this._JoinLevel;
      }
      set
      {
        this._JoinLevel = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastWeekActivities")]
    public int LastWeekActivities
    {
      get
      {
        return this._LastWeekActivities;
      }
      set
      {
        this._LastWeekActivities = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "HiringDeclaration")]
    public string HiringDeclaration
    {
      get
      {
        return this._HiringDeclaration;
      }
      set
      {
        this._HiringDeclaration = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "PresidentMemberInfo")]
    public ProGuildMember PresidentMemberInfo
    {
      get
      {
        return this._PresidentMemberInfo;
      }
      set
      {
        this._PresidentMemberInfo = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "HaveSendJoinReq")]
    public bool HaveSendJoinReq
    {
      get
      {
        return this._HaveSendJoinReq;
      }
      set
      {
        this._HaveSendJoinReq = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MemberCount")]
    public int MemberCount
    {
      get
      {
        return this._MemberCount;
      }
      set
      {
        this._MemberCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
