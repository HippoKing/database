﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomGetAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class BattleRoomGetAck : IExtensible
  {
    private int _Result;
    private readonly List<ProBattleRoomPlayer> _Players;
    private int _BattleId;
    private int _BattleRoomType;
    private int _LeaderIndex;
    private int _GameFunctionTypeId;
    private int _LocationId;
    private int _ArmyRandomSeed;
    private int _RandomSeed;
    private int _BattleRoomStatus;
    private long _ReadyTimeOut;
    private readonly List<ProBattleHeroSetupInfo> _AllSetupInfos;
    private readonly List<ProBattleCommand> _Commands;
    private BattleRoomDataChangeNtf _RealTimePVPRoomData;
    private long _LastPlayerBeginActionTime;
    private readonly List<int> _PreferredHeroTagIds;
    private ulong _InstanceId;
    private ProBattleRoomBPStage _BattleRoomBPStage;
    private long _BattleTurnActionTimeSpan;
    private int _PeakArenaMode;
    private int _Round;
    private int _BORound;
    private int _MatchId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProBattleRoomPlayer> Players
    {
      get
      {
        return this._Players;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleRoomType")]
    public int BattleRoomType
    {
      get
      {
        return this._BattleRoomType;
      }
      set
      {
        this._BattleRoomType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LeaderIndex")]
    [DefaultValue(0)]
    public int LeaderIndex
    {
      get
      {
        return this._LeaderIndex;
      }
      set
      {
        this._LeaderIndex = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      get
      {
        return this._GameFunctionTypeId;
      }
      set
      {
        this._GameFunctionTypeId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LocationId")]
    public int LocationId
    {
      get
      {
        return this._LocationId;
      }
      set
      {
        this._LocationId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArmyRandomSeed")]
    public int ArmyRandomSeed
    {
      get
      {
        return this._ArmyRandomSeed;
      }
      set
      {
        this._ArmyRandomSeed = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RandomSeed")]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleRoomStatus")]
    [DefaultValue(0)]
    public int BattleRoomStatus
    {
      get
      {
        return this._BattleRoomStatus;
      }
      set
      {
        this._BattleRoomStatus = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadyTimeOut")]
    public long ReadyTimeOut
    {
      get
      {
        return this._ReadyTimeOut;
      }
      set
      {
        this._ReadyTimeOut = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "AllSetupInfos")]
    public List<ProBattleHeroSetupInfo> AllSetupInfos
    {
      get
      {
        return this._AllSetupInfos;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, Name = "Commands")]
    public List<ProBattleCommand> Commands
    {
      get
      {
        return this._Commands;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = false, Name = "RealTimePVPRoomData")]
    public BattleRoomDataChangeNtf RealTimePVPRoomData
    {
      get
      {
        return this._RealTimePVPRoomData;
      }
      set
      {
        this._RealTimePVPRoomData = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LastPlayerBeginActionTime")]
    [DefaultValue(0)]
    public long LastPlayerBeginActionTime
    {
      get
      {
        return this._LastPlayerBeginActionTime;
      }
      set
      {
        this._LastPlayerBeginActionTime = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, Name = "PreferredHeroTagIds")]
    public List<int> PreferredHeroTagIds
    {
      get
      {
        return this._PreferredHeroTagIds;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    [DefaultValue(0.0f)]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleRoomBPStage")]
    [DefaultValue(null)]
    public ProBattleRoomBPStage BattleRoomBPStage
    {
      get
      {
        return this._BattleRoomBPStage;
      }
      set
      {
        this._BattleRoomBPStage = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleTurnActionTimeSpan")]
    public long BattleTurnActionTimeSpan
    {
      get
      {
        return this._BattleTurnActionTimeSpan;
      }
      set
      {
        this._BattleTurnActionTimeSpan = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "PeakArenaMode")]
    public int PeakArenaMode
    {
      get
      {
        return this._PeakArenaMode;
      }
      set
      {
        this._PeakArenaMode = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      get
      {
        return this._Round;
      }
      set
      {
        this._Round = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BORound")]
    public int BORound
    {
      get
      {
        return this._BORound;
      }
      set
      {
        this._BORound = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MatchId")]
    public int MatchId
    {
      get
      {
        return this._MatchId;
      }
      set
      {
        this._MatchId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
