﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PeakArenaPlayOffInfoGetReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PeakArenaPlayOffInfoGetReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class PeakArenaPlayOffInfoGetReq : IExtensible
  {
    private int _Season;
    private int _MatchIndex;
    private long _Version;
    private long _LiveRoomVersion;
    private int _ClientSequenceId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffInfoGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Season")]
    public int Season
    {
      get
      {
        return this._Season;
      }
      set
      {
        this._Season = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MatchIndex")]
    public int MatchIndex
    {
      get
      {
        return this._MatchIndex;
      }
      set
      {
        this._MatchIndex = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public long Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LiveRoomVersion")]
    public long LiveRoomVersion
    {
      get
      {
        return this._LiveRoomVersion;
      }
      set
      {
        this._LiveRoomVersion = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ClientSequenceId")]
    public int ClientSequenceId
    {
      get
      {
        return this._ClientSequenceId;
      }
      set
      {
        this._ClientSequenceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
