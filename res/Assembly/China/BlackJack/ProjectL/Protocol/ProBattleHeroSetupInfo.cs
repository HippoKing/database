﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleHeroSetupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProBattleHeroSetupInfo")]
  [Serializable]
  public class ProBattleHeroSetupInfo : IExtensible
  {
    private int _PlayerIndex;
    private int _Position;
    private ProBattleHero _Hero;
    private int _Flag;
    private ProBattleHero _PrevHero;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHeroSetupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerIndex")]
    public int PlayerIndex
    {
      get
      {
        return this._PlayerIndex;
      }
      set
      {
        this._PlayerIndex = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Position")]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "Hero")]
    public ProBattleHero Hero
    {
      get
      {
        return this._Hero;
      }
      set
      {
        this._Hero = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Flag")]
    public int Flag
    {
      get
      {
        return this._Flag;
      }
      set
      {
        this._Flag = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "PrevHero")]
    [DefaultValue(null)]
    public ProBattleHero PrevHero
    {
      get
      {
        return this._PrevHero;
      }
      set
      {
        this._PrevHero = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
