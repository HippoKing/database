﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProGuild")]
  [Serializable]
  public class ProGuild : IExtensible
  {
    private string _Id;
    private string _Name;
    private string _Bulletin;
    private string _HiringDeclaration;
    private bool _AutoJoin;
    private int _TotalActivities;
    private int _LastWeekActivities;
    private int _Activities;
    private readonly List<ProGuildMember> _Members;
    private int _JoinLevel;
    private int _TotalBattlePower;
    private int _CurrentWeekActivities;
    private ProGuildMassiveCombatGeneralInfo _MassiveCombatInfo;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Id")]
    public string Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Bulletin")]
    public string Bulletin
    {
      get
      {
        return this._Bulletin;
      }
      set
      {
        this._Bulletin = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "HiringDeclaration")]
    public string HiringDeclaration
    {
      get
      {
        return this._HiringDeclaration;
      }
      set
      {
        this._HiringDeclaration = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "AutoJoin")]
    public bool AutoJoin
    {
      get
      {
        return this._AutoJoin;
      }
      set
      {
        this._AutoJoin = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalActivities")]
    public int TotalActivities
    {
      get
      {
        return this._TotalActivities;
      }
      set
      {
        this._TotalActivities = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastWeekActivities")]
    public int LastWeekActivities
    {
      get
      {
        return this._LastWeekActivities;
      }
      set
      {
        this._LastWeekActivities = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Activities")]
    public int Activities
    {
      get
      {
        return this._Activities;
      }
      set
      {
        this._Activities = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "Members")]
    public List<ProGuildMember> Members
    {
      get
      {
        return this._Members;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JoinLevel")]
    public int JoinLevel
    {
      get
      {
        return this._JoinLevel;
      }
      set
      {
        this._JoinLevel = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalBattlePower")]
    public int TotalBattlePower
    {
      get
      {
        return this._TotalBattlePower;
      }
      set
      {
        this._TotalBattlePower = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentWeekActivities")]
    public int CurrentWeekActivities
    {
      get
      {
        return this._CurrentWeekActivities;
      }
      set
      {
        this._CurrentWeekActivities = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "MassiveCombatInfo")]
    public ProGuildMassiveCombatGeneralInfo MassiveCombatInfo
    {
      get
      {
        return this._MassiveCombatInfo;
      }
      set
      {
        this._MassiveCombatInfo = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
