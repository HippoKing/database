﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.TeamRoomPlayerQuitNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "TeamRoomPlayerQuitNtf")]
  [Serializable]
  public class TeamRoomPlayerQuitNtf : IExtensible
  {
    private ulong _LeaderSessionId;
    private long _LeaderKickOutTime;
    private ulong _QuitPlayerSessionId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayerQuitNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderSessionId")]
    public ulong LeaderSessionId
    {
      get
      {
        return this._LeaderSessionId;
      }
      set
      {
        this._LeaderSessionId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderKickOutTime")]
    public long LeaderKickOutTime
    {
      get
      {
        return this._LeaderKickOutTime;
      }
      set
      {
        this._LeaderKickOutTime = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "QuitPlayerSessionId")]
    public ulong QuitPlayerSessionId
    {
      get
      {
        return this._QuitPlayerSessionId;
      }
      set
      {
        this._QuitPlayerSessionId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
