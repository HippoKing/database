﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBPStage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBPStage")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBPStage : IExtensible
  {
    private readonly List<ProBPStageHero> _FirstHand;
    private readonly List<ProBPStageHero> _BackHand;
    private readonly List<ProBPStageCommand> _Commands;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBPStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, Name = "FirstHand")]
    public List<ProBPStageHero> FirstHand
    {
      get
      {
        return this._FirstHand;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "BackHand")]
    public List<ProBPStageHero> BackHand
    {
      get
      {
        return this._BackHand;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Commands")]
    public List<ProBPStageCommand> Commands
    {
      get
      {
        return this._Commands;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
