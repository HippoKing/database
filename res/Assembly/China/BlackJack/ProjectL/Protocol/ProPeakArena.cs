﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProPeakArena")]
  [Serializable]
  public class ProPeakArena : IExtensible
  {
    private ProPeakArenaMatchStats _FriendlyMatchStats;
    private ProPeakArenaMatchStats _LadderMatchStats;
    private ProPeakArenaMatchStats _FriendlyCareerMatchStats;
    private ProPeakArenaMatchStats _LadderCareerMatchStats;
    private readonly List<int> _WinsBonusIdAcquired;
    private ProPeakArenaBattleTeam _BattleTeam;
    private readonly List<int> _RegularRewardIndexAcquired;
    private readonly List<ProPeakArenaBattleReportNameRecord> _BattleReportNameRecords;
    private ProPeakArenaBetInfo _BetInfo;
    private int _CurrentSeason;
    private int _FinalRank;
    private ulong _LiveRoomId;
    private int _TodaysQuickLeavingCount;
    private long _BannedTime;
    private bool _NoticeEnterPlayOffRace;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyMatchStats")]
    public ProPeakArenaMatchStats FriendlyMatchStats
    {
      get
      {
        return this._FriendlyMatchStats;
      }
      set
      {
        this._FriendlyMatchStats = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderMatchStats")]
    public ProPeakArenaMatchStats LadderMatchStats
    {
      get
      {
        return this._LadderMatchStats;
      }
      set
      {
        this._LadderMatchStats = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyCareerMatchStats")]
    public ProPeakArenaMatchStats FriendlyCareerMatchStats
    {
      get
      {
        return this._FriendlyCareerMatchStats;
      }
      set
      {
        this._FriendlyCareerMatchStats = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderCareerMatchStats")]
    public ProPeakArenaMatchStats LadderCareerMatchStats
    {
      get
      {
        return this._LadderCareerMatchStats;
      }
      set
      {
        this._LadderCareerMatchStats = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "WinsBonusIdAcquired")]
    public List<int> WinsBonusIdAcquired
    {
      get
      {
        return this._WinsBonusIdAcquired;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleTeam")]
    public ProPeakArenaBattleTeam BattleTeam
    {
      get
      {
        return this._BattleTeam;
      }
      set
      {
        this._BattleTeam = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "RegularRewardIndexAcquired")]
    public List<int> RegularRewardIndexAcquired
    {
      get
      {
        return this._RegularRewardIndexAcquired;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "BattleReportNameRecords")]
    public List<ProPeakArenaBattleReportNameRecord> BattleReportNameRecords
    {
      get
      {
        return this._BattleReportNameRecords;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "BetInfo")]
    public ProPeakArenaBetInfo BetInfo
    {
      get
      {
        return this._BetInfo;
      }
      set
      {
        this._BetInfo = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentSeason")]
    public int CurrentSeason
    {
      get
      {
        return this._CurrentSeason;
      }
      set
      {
        this._CurrentSeason = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FinalRank")]
    public int FinalRank
    {
      get
      {
        return this._FinalRank;
      }
      set
      {
        this._FinalRank = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LiveRoomId")]
    public ulong LiveRoomId
    {
      get
      {
        return this._LiveRoomId;
      }
      set
      {
        this._LiveRoomId = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TodaysQuickLeavingCount")]
    public int TodaysQuickLeavingCount
    {
      get
      {
        return this._TodaysQuickLeavingCount;
      }
      set
      {
        this._TodaysQuickLeavingCount = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BannedTime")]
    public long BannedTime
    {
      get
      {
        return this._BannedTime;
      }
      set
      {
        this._BannedTime = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "NoticeEnterPlayOffRace")]
    public bool NoticeEnterPlayOffRace
    {
      get
      {
        return this._NoticeEnterPlayOffRace;
      }
      set
      {
        this._NoticeEnterPlayOffRace = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
