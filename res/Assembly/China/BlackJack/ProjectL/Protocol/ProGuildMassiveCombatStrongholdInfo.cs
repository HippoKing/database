﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuildMassiveCombatStrongholdInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProGuildMassiveCombatStrongholdInfo")]
  [Serializable]
  public class ProGuildMassiveCombatStrongholdInfo : IExtensible
  {
    private int _ConfigId;
    private int _LevelId;
    private int _EnemyEliminated;
    private long _TakenOverTime;
    private readonly List<int> _PreferredHeroTagIds;
    private int _RandomSeed;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatStrongholdInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConfigId")]
    public int ConfigId
    {
      get
      {
        return this._ConfigId;
      }
      set
      {
        this._ConfigId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelId")]
    public int LevelId
    {
      get
      {
        return this._LevelId;
      }
      set
      {
        this._LevelId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnemyEliminated")]
    public int EnemyEliminated
    {
      get
      {
        return this._EnemyEliminated;
      }
      set
      {
        this._EnemyEliminated = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TakenOverTime")]
    public long TakenOverTime
    {
      get
      {
        return this._TakenOverTime;
      }
      set
      {
        this._TakenOverTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "PreferredHeroTagIds")]
    public List<int> PreferredHeroTagIds
    {
      get
      {
        return this._PreferredHeroTagIds;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomSeed")]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
