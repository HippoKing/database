﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaBattleLiveRoomInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProPeakArenaBattleLiveRoomInfo")]
  [Serializable]
  public class ProPeakArenaBattleLiveRoomInfo : IExtensible
  {
    private long _BORoundStartTime;
    private int _RoomStatus;
    private readonly List<int> _WinPlayerIndexes;
    private readonly List<ProUserSummary> _Players;
    private ProPeakArenaBattleReportBattleInfo _BattleInfo;
    private ProBattleSyncCommand _BattleCommand;
    private ProBPStageSyncCommand _BPCommand;
    private int _Mode;
    private int _Round;
    private int _BORound;
    private readonly List<string> _FirstHandUserIds;
    private long _BattleStartTime;
    private long _BattleStartTurnActionTimeSpan;
    private readonly List<long> _BattleStartPublicTimeSpan;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleLiveRoomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BORoundStartTime")]
    public long BORoundStartTime
    {
      get
      {
        return this._BORoundStartTime;
      }
      set
      {
        this._BORoundStartTime = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomStatus")]
    public int RoomStatus
    {
      get
      {
        return this._RoomStatus;
      }
      set
      {
        this._RoomStatus = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "WinPlayerIndexes")]
    public List<int> WinPlayerIndexes
    {
      get
      {
        return this._WinPlayerIndexes;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProUserSummary> Players
    {
      get
      {
        return this._Players;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleInfo")]
    [DefaultValue(null)]
    public ProPeakArenaBattleReportBattleInfo BattleInfo
    {
      get
      {
        return this._BattleInfo;
      }
      set
      {
        this._BattleInfo = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleCommand")]
    [DefaultValue(null)]
    public ProBattleSyncCommand BattleCommand
    {
      get
      {
        return this._BattleCommand;
      }
      set
      {
        this._BattleCommand = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "BPCommand")]
    [DefaultValue(null)]
    public ProBPStageSyncCommand BPCommand
    {
      get
      {
        return this._BPCommand;
      }
      set
      {
        this._BPCommand = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      get
      {
        return this._Mode;
      }
      set
      {
        this._Mode = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      get
      {
        return this._Round;
      }
      set
      {
        this._Round = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BORound")]
    public int BORound
    {
      get
      {
        return this._BORound;
      }
      set
      {
        this._BORound = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "FirstHandUserIds")]
    public List<string> FirstHandUserIds
    {
      get
      {
        return this._FirstHandUserIds;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleStartTime")]
    public long BattleStartTime
    {
      get
      {
        return this._BattleStartTime;
      }
      set
      {
        this._BattleStartTime = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleStartTurnActionTimeSpan")]
    public long BattleStartTurnActionTimeSpan
    {
      get
      {
        return this._BattleStartTurnActionTimeSpan;
      }
      set
      {
        this._BattleStartTurnActionTimeSpan = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "BattleStartPublicTimeSpan")]
    public List<long> BattleStartPublicTimeSpan
    {
      get
      {
        return this._BattleStartPublicTimeSpan;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
