﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Static.ProjectLGameEntryUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using PD.SDK;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackJack.ProjectL.Static
{
  public class ProjectLGameEntryUIController : UIControllerBase
  {
    private Action m_onShowCompanyEnd;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/Msg", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/ProgressValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressText;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressBar;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/Fill Area", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressBarFill;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/Background", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressBarBackground;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/ProgressValue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressBarValue;
    [AutoBind("EntryUIPrefab/UpdateClient", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_updateClientUI;
    [AutoBind("EntryUIPrefab/Company", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_companyUI;
    [AutoBind("EntryUIPrefab/Company/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeCompanyButton;

    public void SetMesssage(string msg)
    {
      this.m_messageText.text = msg;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessageText(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowUpdateClientUI(bool show)
    {
      this.m_updateClientUI.SetActive(show);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTipActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProgress(float value)
    {
      if ((double) this.m_progressBar.value == (double) value)
        return;
      this.m_progressBar.value = value;
      this.m_progressText.text = string.Format("{0,4:F1}%", (object) (float) ((double) value * 100.0));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddProgress(float value, float threshhold = 1f)
    {
      this.SetProgress(Math.Max(this.m_progressBar.value, Math.Min(threshhold, this.m_progressBar.value + value)));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
      this.m_closeCompanyButton.onClick.AddListener(new UnityAction(this.OnCloseCompanyClicked));
      this.m_progressBarBackground.SetActive(!PDSDK.IsIosReview);
      this.m_progressBarFill.SetActive(!PDSDK.IsIosReview);
      this.m_progressBarValue.SetActive(!PDSDK.IsIosReview);
      this.SetProgress(0.0f);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseCompanyClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCompanyUI(Action onEnd)
    {
      this.m_onShowCompanyEnd = onEnd;
      this.m_companyUI.gameObject.SetActive(true);
      this.m_companyUI.SetActionForUIStateFinshed("Show", (Action) (() =>
      {
        this.m_companyUI.gameObject.SetActive(false);
        if (this.m_onShowCompanyEnd == null)
          return;
        this.m_onShowCompanyEnd();
        this.m_onShowCompanyEnd = (Action) null;
      }));
      this.m_companyUI.SetToUIState("Show", false, true);
    }
  }
}
