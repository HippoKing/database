﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldConst
  {
    public const int TickRate = 30;
    public const float TickTime = 0.03333334f;
    public const float CameraAngle = 20f;
    public const int WorldActorSortingOrder = 2;
    public const int WorldNextScenarioSortingOrder = 5;
    public const int World2DWaypointSortingOrder = 1;
    public const int World2DWaypointUISortingOrder = 2;
    public const int World2DEventActorSortingOrder = 3;
    public const int World2DPlayerActorSortingOrder = 4;
    public const float WaypointZOffset = 0.0f;
    public const float EventActorZOffset = -0.04f;
    public const float PlayerActorZOffset = -0.08f;
    public const float WorldActorUIZOffset = -0.02f;
  }
}
