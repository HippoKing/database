﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldWaypoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldWaypoint : Entity
  {
    private ClientWorld m_clientWorld;
    private ConfigDataWaypointInfo m_waypointInfo;
    private WayPointStatus m_status;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private bool m_isVisible;
    private WorldWaypointUIController m_uiController;
    private CommonUIStateController m_graphicUIStateController;
    private Vector3 m_uiInitScale;
    private float m_graphicInitScale;
    private bool m_isPointerDown;
    private float m_scale;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientWorld world, ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Tick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawArrow(Vector3 p0, Vector3 p1, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(WayPointStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    public WayPointStatus GetStatus()
    {
      return this.m_status;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScenarioState(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataScenarioInfo lastFinishedScenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetClickTransform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVisible()
    {
      return this.m_isVisible;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUISiblingIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIPointerDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIPointerUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2 Position
    {
      get
      {
        return this.m_position;
      }
    }

    public ConfigDataWaypointInfo WaypointInfo
    {
      get
      {
        return this.m_waypointInfo;
      }
    }
  }
}
