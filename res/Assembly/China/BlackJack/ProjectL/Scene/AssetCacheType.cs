﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.AssetCacheType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class AssetCacheType
  {
    public const int Hero0 = 1;
    public const int Hero1 = 2;
    public const int Soldier0 = 3;
    public const int Soldier1 = 4;
    public const int Fx = 5;
    public const int TerrainBackground = 6;
  }
}
