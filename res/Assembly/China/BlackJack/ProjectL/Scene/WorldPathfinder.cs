﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldPathfinder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class WorldPathfinder
  {
    private LinkedList<WorldPathfinder.Node> m_OpenList;
    private List<WorldPathfinder.Node> m_ClosedList;
    private List<WorldPathfinder.Node> m_Successors;
    private WorldPathfinder.SearchState m_State;
    private int m_Steps;
    private WorldPathfinder.Node m_Start;
    private WorldPathfinder.Node m_Goal;
    private WorldPathfinder.Node m_CurrentSolutionNode;
    private bool m_CancelRequest;
    private List<WorldPathfinder.Node> m_NodePool;
    private int m_AllocateNodeCount;
    private List<WorldPathNode> m_PathNodePool;
    private int m_AllocatedPathNodeCount;
    private const int kPreallocatedNodes = 64;
    private ClientWorld m_clientWorld;
    private CollectionActivityWorld m_collectionActivityWorld;
    private bool m_isCheckWaypointStatus;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathfinder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortedAddToOpenList(WorldPathfinder.Node node)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private WorldPathfinder.Node AllocateNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode AllocatePathNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitiatePathfind()
    {
      // ISSUE: unable to decompile the method.
    }

    public void CancelSearch()
    {
      this.m_CancelRequest = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStartAndGoalStates(WorldPathNode start, WorldPathNode goal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathfinder.SearchState SearchStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSuccessor(WorldPathNode state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode GetSolutionStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode GetSolutionNext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeSolutionNodes()
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldPathNode StartNode
    {
      get
      {
        return this.m_Start.m_UserState;
      }
    }

    public WorldPathNode GoalNode
    {
      get
      {
        return this.m_Goal.m_UserState;
      }
    }

    public bool HasStartNode()
    {
      return this.m_Start != null;
    }

    public bool HasGoalNode()
    {
      return this.m_Goal != null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetWaypointNeighbors(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetWaypointPosition(int id, out Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanMoveToWaypoint(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      ClientWorld world,
      int start,
      int goal,
      List<int> path,
      bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      CollectionActivityWorld world,
      int start,
      int goal,
      List<int> path,
      bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum SearchState
    {
      NotInitialized,
      Searching,
      Succeeded,
      Failed,
    }

    [CustomLuaClass]
    public class Node
    {
      public WorldPathfinder.Node parent;
      public WorldPathfinder.Node child;
      public float g;
      public float h;
      public float f;
      public WorldPathNode m_UserState;

      public Node()
      {
        this.Reinitialize();
      }

      public void Reinitialize()
      {
        this.parent = (WorldPathfinder.Node) null;
        this.child = (WorldPathfinder.Node) null;
        this.g = 0.0f;
        this.h = 0.0f;
        this.f = 0.0f;
      }
    }
  }
}
