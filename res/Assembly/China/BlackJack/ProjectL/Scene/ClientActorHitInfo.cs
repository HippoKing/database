﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorHitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientActorHitInfo
  {
    public int m_heroHp;
    public int m_soldierHp;
    public int m_heroHpModify;
    public int m_soldierHpModify;
    public int m_frame;
    public DamageNumberType m_damageNumberType;
    public List<int> m_disabledBuffStateIdList;
  }
}
