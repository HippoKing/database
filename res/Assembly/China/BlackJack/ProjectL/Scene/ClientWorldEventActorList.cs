﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldEventActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldEventActorList
  {
    public static void RemoveAll(List<ClientWorldEventActor> list)
    {
      EntityList.RemoveAll<ClientWorldEventActor>(list);
    }

    public static void RemoveDeleted(List<ClientWorldEventActor> list)
    {
      EntityList.RemoveDeleted<ClientWorldEventActor>(list);
    }

    public static void Tick(List<ClientWorldEventActor> list)
    {
      EntityList.Tick<ClientWorldEventActor>(list);
    }

    public static void TickGraphic(List<ClientWorldEventActor> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldEventActor>(list, dt);
    }

    public static void Draw(List<ClientWorldEventActor> list)
    {
      EntityList.Draw<ClientWorldEventActor>(list);
    }

    public static void Pause(List<ClientWorldEventActor> list, bool pause)
    {
      EntityList.Pause<ClientWorldEventActor>(list, pause);
    }
  }
}
