﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.UI;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class WorldCamera : CameraBase
  {
    private Vector2 m_position;
    private float m_viewHeight;
    private Vector2 m_touchMoveVelocity;
    private Vector2 m_followTargetPosition;
    private Vector2 m_followVelocity;
    private bool m_enableTouchMove;
    private bool m_isFollowing;
    private float m_viewAngle;
    private Vector2 m_smoothMoveStartPosition;
    private Vector2 m_smoothMoveEndPosition;
    private float m_smoothMoveStartHeight;
    private float m_smoothMoveEndHeight;
    private float m_smoothMoveTime;
    private float m_smoothMoveTotalTime;
    private EventSystem m_eventSystem;
    private PointerEventData m_pointData;
    private List<RaycastResult> m_raycastResults;
    private TouchStatus[] m_touchStatus;
    private Vector3 m_initPosition;
    private float m_initFov;
    private Vector2 m_mapSizeHalf;
    private float m_viewHeightMin;
    private float m_viewHeightMax;
    private WorldWaypointUIController m_hitWaypointUIController;
    private WorldEventActorUIController m_hitEventActorUIController;
    private CollectionActivityWaypointUIController m_hitCollectionActivityWaypointUIController;
    private Action m_onSmoothMoveEnd;
    [DoNotToLua]
    private WorldCamera.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeGameObject_hotfix;
    private LuaFunction m_StartConfigDataWorldMapInfoSingle_hotfix;
    private LuaFunction m_StartConfigDataCollectionActivityMapInfoSingle_hotfix;
    private LuaFunction m_EnableTouchMoveBoolean_hotfix;
    private LuaFunction m_UpdateSingle_hotfix;
    private LuaFunction m_IsTouchUIVector2_hotfix;
    private LuaFunction m_UpdateTouchSingle_hotfix;
    private LuaFunction m_ClampCameraPositionVector2_hotfix;
    private LuaFunction m_GroundPositionToCameraPositionVector2_hotfix;
    private LuaFunction m_LookVector2_hotfix;
    private LuaFunction m_SmoothLookVector2ActionSingle_hotfix;
    private LuaFunction m_IsSmoothMoving_hotfix;
    private LuaFunction m_StartFollow_hotfix;
    private LuaFunction m_SetFollowPositionVector2_hotfix;
    private LuaFunction m_GetPosition_hotfix;
    private LuaFunction m_SetHeightSingle_hotfix;
    private LuaFunction m_GetZoomFactor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataWorldMapInfo mapInfo, float viewAngle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataCollectionActivityMapInfo mapInfo, float viewAngle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTouchMove(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Vector2 ScreenPositionToViewPosition(
      Vector2 p,
      float viewWidth,
      float viewHeight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTouchUI(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTouch(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ClampCameraPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GroundPositionToCameraPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Look(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SmoothLook(Vector2 p, Action onEnd = null, float height = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSmoothMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFollow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFollowPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeight(float h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetZoomFactor()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public WorldCamera.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initialize(GameObject cameraGo, GameObject animatorGo)
    {
      this.Initialize(cameraGo, animatorGo);
    }

    private void __callBase_GetViewSize(out float width, out float height)
    {
      this.GetViewSize(out width, out height);
    }

    private void __callBase_PlayAnimation(string name)
    {
      this.PlayAnimation(name);
    }

    private Vector3 __callBase_GetAnimationOffset()
    {
      return this.GetAnimationOffset();
    }

    private bool __callBase_IsCulled(Vector2 p)
    {
      return this.IsCulled(p);
    }

    private bool __callBase_IsCulled(Vector2 bmin, Vector2 bmax)
    {
      return this.IsCulled(bmin, bmax);
    }

    private void __callBase_SetActive(bool a)
    {
      this.SetActive(a);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_onSmoothMoveEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_onSmoothMoveEnd()
    {
      this.m_onSmoothMoveEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private WorldCamera m_owner;

      public LuaExportHelper(WorldCamera owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initialize(GameObject cameraGo, GameObject animatorGo)
      {
        this.m_owner.__callBase_Initialize(cameraGo, animatorGo);
      }

      public void __callBase_GetViewSize(out float width, out float height)
      {
        this.m_owner.__callBase_GetViewSize(out width, out height);
      }

      public void __callBase_PlayAnimation(string name)
      {
        this.m_owner.__callBase_PlayAnimation(name);
      }

      public Vector3 __callBase_GetAnimationOffset()
      {
        return this.m_owner.__callBase_GetAnimationOffset();
      }

      public bool __callBase_IsCulled(Vector2 p)
      {
        return this.m_owner.__callBase_IsCulled(p);
      }

      public bool __callBase_IsCulled(Vector2 bmin, Vector2 bmax)
      {
        return this.m_owner.__callBase_IsCulled(bmin, bmax);
      }

      public void __callBase_SetActive(bool a)
      {
        this.m_owner.__callBase_SetActive(a);
      }

      public void __callDele_m_onSmoothMoveEnd()
      {
        this.m_owner.__callDele_m_onSmoothMoveEnd();
      }

      public void __clearDele_m_onSmoothMoveEnd()
      {
        this.m_owner.__clearDele_m_onSmoothMoveEnd();
      }

      public Vector2 m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public float m_viewHeight
      {
        get
        {
          return this.m_owner.m_viewHeight;
        }
        set
        {
          this.m_owner.m_viewHeight = value;
        }
      }

      public Vector2 m_touchMoveVelocity
      {
        get
        {
          return this.m_owner.m_touchMoveVelocity;
        }
        set
        {
          this.m_owner.m_touchMoveVelocity = value;
        }
      }

      public Vector2 m_followTargetPosition
      {
        get
        {
          return this.m_owner.m_followTargetPosition;
        }
        set
        {
          this.m_owner.m_followTargetPosition = value;
        }
      }

      public Vector2 m_followVelocity
      {
        get
        {
          return this.m_owner.m_followVelocity;
        }
        set
        {
          this.m_owner.m_followVelocity = value;
        }
      }

      public bool m_enableTouchMove
      {
        get
        {
          return this.m_owner.m_enableTouchMove;
        }
        set
        {
          this.m_owner.m_enableTouchMove = value;
        }
      }

      public bool m_isFollowing
      {
        get
        {
          return this.m_owner.m_isFollowing;
        }
        set
        {
          this.m_owner.m_isFollowing = value;
        }
      }

      public float m_viewAngle
      {
        get
        {
          return this.m_owner.m_viewAngle;
        }
        set
        {
          this.m_owner.m_viewAngle = value;
        }
      }

      public Vector2 m_smoothMoveStartPosition
      {
        get
        {
          return this.m_owner.m_smoothMoveStartPosition;
        }
        set
        {
          this.m_owner.m_smoothMoveStartPosition = value;
        }
      }

      public Vector2 m_smoothMoveEndPosition
      {
        get
        {
          return this.m_owner.m_smoothMoveEndPosition;
        }
        set
        {
          this.m_owner.m_smoothMoveEndPosition = value;
        }
      }

      public float m_smoothMoveStartHeight
      {
        get
        {
          return this.m_owner.m_smoothMoveStartHeight;
        }
        set
        {
          this.m_owner.m_smoothMoveStartHeight = value;
        }
      }

      public float m_smoothMoveEndHeight
      {
        get
        {
          return this.m_owner.m_smoothMoveEndHeight;
        }
        set
        {
          this.m_owner.m_smoothMoveEndHeight = value;
        }
      }

      public float m_smoothMoveTime
      {
        get
        {
          return this.m_owner.m_smoothMoveTime;
        }
        set
        {
          this.m_owner.m_smoothMoveTime = value;
        }
      }

      public float m_smoothMoveTotalTime
      {
        get
        {
          return this.m_owner.m_smoothMoveTotalTime;
        }
        set
        {
          this.m_owner.m_smoothMoveTotalTime = value;
        }
      }

      public EventSystem m_eventSystem
      {
        get
        {
          return this.m_owner.m_eventSystem;
        }
        set
        {
          this.m_owner.m_eventSystem = value;
        }
      }

      public PointerEventData m_pointData
      {
        get
        {
          return this.m_owner.m_pointData;
        }
        set
        {
          this.m_owner.m_pointData = value;
        }
      }

      public List<RaycastResult> m_raycastResults
      {
        get
        {
          return this.m_owner.m_raycastResults;
        }
        set
        {
          this.m_owner.m_raycastResults = value;
        }
      }

      public TouchStatus[] m_touchStatus
      {
        get
        {
          return this.m_owner.m_touchStatus;
        }
        set
        {
          this.m_owner.m_touchStatus = value;
        }
      }

      public Vector3 m_initPosition
      {
        get
        {
          return this.m_owner.m_initPosition;
        }
        set
        {
          this.m_owner.m_initPosition = value;
        }
      }

      public float m_initFov
      {
        get
        {
          return this.m_owner.m_initFov;
        }
        set
        {
          this.m_owner.m_initFov = value;
        }
      }

      public Vector2 m_mapSizeHalf
      {
        get
        {
          return this.m_owner.m_mapSizeHalf;
        }
        set
        {
          this.m_owner.m_mapSizeHalf = value;
        }
      }

      public float m_viewHeightMin
      {
        get
        {
          return this.m_owner.m_viewHeightMin;
        }
        set
        {
          this.m_owner.m_viewHeightMin = value;
        }
      }

      public float m_viewHeightMax
      {
        get
        {
          return this.m_owner.m_viewHeightMax;
        }
        set
        {
          this.m_owner.m_viewHeightMax = value;
        }
      }

      public WorldWaypointUIController m_hitWaypointUIController
      {
        get
        {
          return this.m_owner.m_hitWaypointUIController;
        }
        set
        {
          this.m_owner.m_hitWaypointUIController = value;
        }
      }

      public WorldEventActorUIController m_hitEventActorUIController
      {
        get
        {
          return this.m_owner.m_hitEventActorUIController;
        }
        set
        {
          this.m_owner.m_hitEventActorUIController = value;
        }
      }

      public CollectionActivityWaypointUIController m_hitCollectionActivityWaypointUIController
      {
        get
        {
          return this.m_owner.m_hitCollectionActivityWaypointUIController;
        }
        set
        {
          this.m_owner.m_hitCollectionActivityWaypointUIController = value;
        }
      }

      public Action m_onSmoothMoveEnd
      {
        get
        {
          return this.m_owner.m_onSmoothMoveEnd;
        }
        set
        {
          this.m_owner.m_onSmoothMoveEnd = value;
        }
      }

      public static Vector2 ScreenPositionToViewPosition(
        Vector2 p,
        float viewWidth,
        float viewHeight)
      {
        return WorldCamera.ScreenPositionToViewPosition(p, viewWidth, viewHeight);
      }

      public bool IsTouchUI(Vector2 pos)
      {
        return this.m_owner.IsTouchUI(pos);
      }

      public void UpdateTouch(float dt)
      {
        this.m_owner.UpdateTouch(dt);
      }

      public Vector2 ClampCameraPosition(Vector2 p)
      {
        return this.m_owner.ClampCameraPosition(p);
      }

      public Vector2 GroundPositionToCameraPosition(Vector2 p)
      {
        return this.m_owner.GroundPositionToCameraPosition(p);
      }
    }
  }
}
