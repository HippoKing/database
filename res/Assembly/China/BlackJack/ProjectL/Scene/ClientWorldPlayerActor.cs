﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldPlayerActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.UI;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldPlayerActor : Entity
  {
    private ClientWorld m_clientWorld;
    private ConfigDataWaypointInfo m_locateWaypointInfo;
    private List<ClientWorldWaypoint> m_movePath;
    private Action m_onMoveEnd;
    private float m_teleportAppearCountdown;
    private float m_teleportEndActionCountdown;
    private ClientWorldWaypoint m_teleportWaypoint;
    private Action m_onTeleportEnd;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private int m_direction;
    private bool m_isVisible;
    private WorldPlayerActorUIController m_uiController;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientWorld world)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo heroSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Tick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Draw()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(ConfigDataWaypointInfo waypointInfo, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MovePath(List<int> path, Action onMoveEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Teleport(int waypointId, Action onTeleportEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMoving()
    {
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    private void PlaySound(SoundTableId id)
    {
      this.m_clientWorld.PlaySound(id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVisible()
    {
      return this.m_isVisible;
    }

    public Vector2 Position
    {
      get
      {
        return this.m_position;
      }
    }

    public int Direction
    {
      get
      {
        return this.m_direction;
      }
    }

    public ConfigDataWaypointInfo LocateWaypointInfo
    {
      get
      {
        return this.m_locateWaypointInfo;
      }
    }
  }
}
