﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CameraBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class CameraBase
  {
    protected Camera m_camera;
    protected Animator m_animator;
    protected GameObject m_animationGameObject;
    private bool m_isPlayAnimation;

    [MethodImpl((MethodImplOptions) 32768)]
    protected void Initialize(GameObject cameraGo, GameObject animatorGo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GetViewSize(out float width, out float height)
    {
      CameraBase.GetViewSize(this.m_camera, out width, out height);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Vector3 GetAnimationOffset()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool IsCulled(Vector2 p)
    {
      return CameraBase.IsCulled(this.m_camera, p);
    }

    public virtual bool IsCulled(Vector2 bmin, Vector2 bmax)
    {
      return CameraBase.IsCulled(this.m_camera, bmin, bmax);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetViewSize(Camera cam, out float width, out float height)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCulled(Camera cam, Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCulled(Camera cam, Vector2 bmin, Vector2 bmax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float AspectScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActive(bool a)
    {
      // ISSUE: unable to decompile the method.
    }

    public Camera Camera
    {
      get
      {
        return this.m_camera;
      }
    }
  }
}
