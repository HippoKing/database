﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaPlayOffMatchupInfoExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public static class PeakArenaPlayOffMatchupInfoExtension
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaPlayOffMatchupInfo ToPeakArenaPlayOffMatchupInfo(
      this ProPeakArenaPlayOffMatchupInfo Pro)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaPlayOffMatchupInfo> GetMatchupInfosByRound(
      this PeakArenaSeasonInfo info,
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaPlayOffMatchupInfo> GetAllPrevMatchupInfos(
      this PeakArenaSeasonInfo info,
      int currentMatchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaPlayOffMatchupInfo> GetMatchupInfosByGroup(
      this PeakArenaSeasonInfo info,
      int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsMatchupFinished(this PeakArenaPlayOffMatchupInfo info)
    {
      return info.WinnerId != (string) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLeftUserWin(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRightUserWin(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLeftUserForfeited(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRightUserForfeited(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetMatchupInfoBattleScore(
      this PeakArenaPlayOffMatchupInfo info,
      out int leftWins,
      out int rightWins)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
