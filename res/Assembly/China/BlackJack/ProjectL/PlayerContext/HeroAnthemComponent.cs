﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroAnthemComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class HeroAnthemComponent : HeroAnthemComponentCommon
  {
    [DoNotToLua]
    private HeroAnthemComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSHeroAnthemNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_FinishedHeroAnthemLevelInt32List`1BooleanList`1_hotfix;
    private LuaFunction m_SetSuccessHeroAnthemLevelConfigDataHeroAnthemLevelInfoList`1List`1_hotfix;
    private LuaFunction m_GetMaxFinishedLevelIdInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAnthemComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroAnthemNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedHeroAnthemLevel(
      int levelId,
      List<int> gotAchievementIds,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetSuccessHeroAnthemLevel(
      ConfigDataHeroAnthemLevelInfo levelInfo,
      List<int> gotAchievementRelationIds,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroAnthemComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_GetAllHeroAnthemAchievements()
    {
      return this.GetAllHeroAnthemAchievements();
    }

    private HashSet<int> __callBase_GetAllHeroAnthemLevelAchievements()
    {
      return this.GetAllHeroAnthemLevelAchievements();
    }

    private int __callBase_GetChapterHeroAnthemAchievements(int chapterId)
    {
      return this.GetChapterHeroAnthemAchievements(chapterId);
    }

    private int __callBase_GetHeroAnthemLevelAchievements(int levelId)
    {
      return this.GetHeroAnthemLevelAchievements(levelId);
    }

    private bool __callBase_HasGotAchievementRelationId(int achievementId)
    {
      return this.HasGotAchievementRelationId(achievementId);
    }

    private bool __callBase_IsFirstPassLevel(int levelId)
    {
      return this.IsFirstPassLevel(levelId);
    }

    private bool __callBase_IsLevelFinished(int levelId)
    {
      return this.IsLevelFinished(levelId);
    }

    private void __callBase_SetSuccessHeroAnthemLevel(
      ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo,
      List<int> newAchievementIds,
      List<int> battleTreasures)
    {
      base.SetSuccessHeroAnthemLevel(heroAnthemLevelInfo, newAchievementIds, battleTreasures);
    }

    private void __callBase_AddHasCompleteLevel(ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo)
    {
      this.AddHasCompleteLevel(heroAnthemLevelInfo);
    }

    private void __callBase_CompleteAchievement(
      ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo,
      int achievementRelationId)
    {
      this.CompleteAchievement(heroAnthemLevelInfo, achievementRelationId);
    }

    private int __callBase_AttackLevel(int levelId)
    {
      return this.AttackLevel(levelId);
    }

    private int __callBase_CanAttackLevel(int levelId)
    {
      return this.CanAttackLevel(levelId);
    }

    private int __callBase_CanUnLockLevel(int riftLevelId)
    {
      return this.CanUnLockLevel(riftLevelId);
    }

    private void __callBase_OnHeroAnthemAchivementAdd(int addAchivement)
    {
      this.OnHeroAnthemAchivementAdd(addAchivement);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAnthemComponent m_owner;

      public LuaExportHelper(HeroAnthemComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_GetAllHeroAnthemAchievements()
      {
        return this.m_owner.__callBase_GetAllHeroAnthemAchievements();
      }

      public HashSet<int> __callBase_GetAllHeroAnthemLevelAchievements()
      {
        return this.m_owner.__callBase_GetAllHeroAnthemLevelAchievements();
      }

      public int __callBase_GetChapterHeroAnthemAchievements(int chapterId)
      {
        return this.m_owner.__callBase_GetChapterHeroAnthemAchievements(chapterId);
      }

      public int __callBase_GetHeroAnthemLevelAchievements(int levelId)
      {
        return this.m_owner.__callBase_GetHeroAnthemLevelAchievements(levelId);
      }

      public bool __callBase_HasGotAchievementRelationId(int achievementId)
      {
        return this.m_owner.__callBase_HasGotAchievementRelationId(achievementId);
      }

      public bool __callBase_IsFirstPassLevel(int levelId)
      {
        return this.m_owner.__callBase_IsFirstPassLevel(levelId);
      }

      public bool __callBase_IsLevelFinished(int levelId)
      {
        return this.m_owner.__callBase_IsLevelFinished(levelId);
      }

      public void __callBase_SetSuccessHeroAnthemLevel(
        ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo,
        List<int> newAchievementIds,
        List<int> battleTreasures)
      {
        this.m_owner.__callBase_SetSuccessHeroAnthemLevel(heroAnthemLevelInfo, newAchievementIds, battleTreasures);
      }

      public void __callBase_AddHasCompleteLevel(ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo)
      {
        this.m_owner.__callBase_AddHasCompleteLevel(heroAnthemLevelInfo);
      }

      public void __callBase_CompleteAchievement(
        ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo,
        int achievementRelationId)
      {
        this.m_owner.__callBase_CompleteAchievement(heroAnthemLevelInfo, achievementRelationId);
      }

      public int __callBase_AttackLevel(int levelId)
      {
        return this.m_owner.__callBase_AttackLevel(levelId);
      }

      public int __callBase_CanAttackLevel(int levelId)
      {
        return this.m_owner.__callBase_CanAttackLevel(levelId);
      }

      public int __callBase_CanUnLockLevel(int riftLevelId)
      {
        return this.m_owner.__callBase_CanUnLockLevel(riftLevelId);
      }

      public void __callBase_OnHeroAnthemAchivementAdd(int addAchivement)
      {
        this.m_owner.__callBase_OnHeroAnthemAchivementAdd(addAchivement);
      }
    }
  }
}
