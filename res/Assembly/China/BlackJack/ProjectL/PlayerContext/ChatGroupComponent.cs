﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatGroupComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatGroupComponent : ChatGroupComponentCommon
  {
    public DateTime m_dataLastUpdateTime;
    private Dictionary<string, ChatGroupCompactInfo> m_currChatGroupDict;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ChatComponent m_chatComponent;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo GetChatGroupSimpleInfo(string groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyChatGroupInfo(
      List<ProChatGroupCompactInfo> chatGroupInfoList,
      DateTime nowServerTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChatGroupStateChangedNtf(ProChatGroupInfo groupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime DataLastUpdateTime
    {
      get
      {
        return this.m_dataLastUpdateTime;
      }
    }
  }
}
