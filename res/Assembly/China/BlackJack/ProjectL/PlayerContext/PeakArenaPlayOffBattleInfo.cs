﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaPlayOffBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaPlayOffBattleInfo
  {
    public bool PlayerOnLeftReady;
    public bool PlayerOnRightReady;
    public string WinnerId;
    public DateTime BattleEndTime;
  }
}
