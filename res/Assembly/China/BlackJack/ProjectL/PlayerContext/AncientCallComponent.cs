﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.AncientCallComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class AncientCallComponent : AncientCallComponentCommon
  {
    [DoNotToLua]
    private AncientCallComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_DeSerializeDSAncientCallNtf_hotfix;
    private LuaFunction m_FinishedAncientCallBossInt32BooleanInt32List`1_hotfix;
    private LuaFunction m_DeSerializeAncientCallInfoUpdateNtf_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSAncientCallNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedAncientCallBoss(
      int bossId,
      bool isWin,
      int damage,
      List<int> gotAchievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(AncientCallInfoUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public AncientCallComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_IsCurrentPeriodExist()
    {
      return this.IsCurrentPeriodExist();
    }

    private void __callBase_OnFlush()
    {
      this.OnFlush();
    }

    private AncientCallPeriod __callBase_GetCurrentAncientCallPeriod()
    {
      return this.GetCurrentAncientCallPeriod();
    }

    private int __callBase_GetCurrentBossId()
    {
      return this.GetCurrentBossId();
    }

    private bool __callBase_SetCommonBossSuccess(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      List<int> newAchievementRelationIds)
    {
      return this.SetCommonBossSuccess(bossId, damage, teamList, updateTime, newAchievementRelationIds);
    }

    private int __callBase_GetBossCurrentPeriodMaxDamage(int bossId)
    {
      return this.GetBossCurrentPeriodMaxDamage(bossId);
    }

    private int __callBase_GetCurrentPeriodScore()
    {
      return this.GetCurrentPeriodScore();
    }

    private AncientCallBoss __callBase_FindBossById(int bossId)
    {
      return this.FindBossById(bossId);
    }

    private AncientCall __callBase_GetAncientCallInfo()
    {
      return this.GetAncientCallInfo();
    }

    private void __callBase_SetCurrentPeriodInfo(int periodId, int bossIndex)
    {
      this.SetCurrentPeriodInfo(periodId, bossIndex);
    }

    private void __callBase_SetCurrentBoss(int currentBossIndex)
    {
      this.SetCurrentBoss(currentBossIndex);
    }

    private int __callBase_AttackAncientCallBoss(int bossId)
    {
      return this.AttackAncientCallBoss(bossId);
    }

    private int __callBase_CanAttackAncientCallBoss(int bossId)
    {
      return this.CanAttackAncientCallBoss(bossId);
    }

    private bool __callBase_HasGotAchievementRelationId(int achievementId)
    {
      return this.HasGotAchievementRelationId(achievementId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AncientCallComponent m_owner;

      public LuaExportHelper(AncientCallComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_IsCurrentPeriodExist()
      {
        return this.m_owner.__callBase_IsCurrentPeriodExist();
      }

      public void __callBase_OnFlush()
      {
        this.m_owner.__callBase_OnFlush();
      }

      public AncientCallPeriod __callBase_GetCurrentAncientCallPeriod()
      {
        return this.m_owner.__callBase_GetCurrentAncientCallPeriod();
      }

      public int __callBase_GetCurrentBossId()
      {
        return this.m_owner.__callBase_GetCurrentBossId();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool __callBase_SetCommonBossSuccess(
        int bossId,
        int damage,
        List<int> teamList,
        DateTime updateTime,
        List<int> newAchievementRelationIds)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_GetBossCurrentPeriodMaxDamage(int bossId)
      {
        return this.m_owner.__callBase_GetBossCurrentPeriodMaxDamage(bossId);
      }

      public int __callBase_GetCurrentPeriodScore()
      {
        return this.m_owner.__callBase_GetCurrentPeriodScore();
      }

      public AncientCallBoss __callBase_FindBossById(int bossId)
      {
        return this.m_owner.__callBase_FindBossById(bossId);
      }

      public AncientCall __callBase_GetAncientCallInfo()
      {
        return this.m_owner.__callBase_GetAncientCallInfo();
      }

      public void __callBase_SetCurrentPeriodInfo(int periodId, int bossIndex)
      {
        this.m_owner.__callBase_SetCurrentPeriodInfo(periodId, bossIndex);
      }

      public void __callBase_SetCurrentBoss(int currentBossIndex)
      {
        this.m_owner.__callBase_SetCurrentBoss(currentBossIndex);
      }

      public int __callBase_AttackAncientCallBoss(int bossId)
      {
        return this.m_owner.__callBase_AttackAncientCallBoss(bossId);
      }

      public int __callBase_CanAttackAncientCallBoss(int bossId)
      {
        return this.m_owner.__callBase_CanAttackAncientCallBoss(bossId);
      }

      public bool __callBase_HasGotAchievementRelationId(int achievementId)
      {
        return this.m_owner.__callBase_HasGotAchievementRelationId(achievementId);
      }
    }
  }
}
