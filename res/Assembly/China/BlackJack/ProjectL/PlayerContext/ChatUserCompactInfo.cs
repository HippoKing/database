﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatUserCompactInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatUserCompactInfo
  {
    public string UserId;
    public string UserName;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUserCompactInfo(ProChatUserCompactInfo userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeadIcon { get; set; }

    public bool IsOnline { get; set; }
  }
}
