﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class PeakArenaComponent : PeakArenaComponentCommon
  {
    public List<PeakArenaHeroWithEquipments> PeakArenaPublicHeroesPool;
    [DoNotToLua]
    private PeakArenaComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSPeakArenaNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_SetPeakArenaPublicHeroesPoolList`1_hotfix;
    private LuaFunction m_LocalizePeakArenaPublicHeroesPool_hotfix;
    private LuaFunction m_SelectHeroSkillInPublicPoolsInt32List`1_hotfix;
    private LuaFunction m_IsPeakArenaMatchmakingAvailableRealTimePVPMode_hotfix;
    private LuaFunction m_IsPeakArenaMatchmakingAvailableWithOutGameFunctionRealTimePVPMode_hotfix;
    private LuaFunction m_SetPeakArenaBattleReportsList`1_hotfix;
    private LuaFunction m_UpdateDataProPeakArena_hotfix;
    private LuaFunction m_CanBetPeakArenaCare4RoundInt32Int32StringInt32_hotfix;
    private LuaFunction m_UpdateMyPlayOffInfoPeakArenaSeasonInfo_hotfix;
    private LuaFunction m_GetPeakArenaSeasonEndTimeInt32_hotfix;
    private LuaFunction m_get_Dan_hotfix;
    private LuaFunction m_set_DanInt32_hotfix;
    private LuaFunction m_get_Score_hotfix;
    private LuaFunction m_set_ScoreInt32_hotfix;
    private LuaFunction m_get_MyPlayOffGroupId_hotfix;
    private LuaFunction m_set_MyPlayOffGroupIdNullable`1_hotfix;
    private LuaFunction m_get_MyNextPlayoffMatch_hotfix;
    private LuaFunction m_set_MyNextPlayoffMatchPeakArenaPlayOffMatchupInfo_hotfix;
    private LuaFunction m_get_ChampionId_hotfix;
    private LuaFunction m_set_ChampionIdString_hotfix;
    private LuaFunction m_get_LastPlayOffRewardSendDate_hotfix;
    private LuaFunction m_set_LastPlayOffRewardSendDateDateTime_hotfix;
    private LuaFunction m_get_LastPeakArenaSendRewardSeasonID_hotfix;
    private LuaFunction m_set_LastPeakArenaSendRewardSeasonIDInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSPeakArenaNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaPublicHeroesPool(List<PeakArenaHeroWithEquipments> heroesPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LocalizePeakArenaPublicHeroesPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int SelectHeroSkillInPublicPools(int heroId, List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int IsPeakArenaMatchmakingAvailable(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsPeakArenaMatchmakingAvailableWithOutGameFunction(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaBattleReports(List<ProCommonBattleReport> battleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateData(ProPeakArena pro)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBetPeakArenaCare4Round(int round, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMyPlayOffInfo(PeakArenaSeasonInfo seasonInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Dan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Score
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int? MyPlayOffGroupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PeakArenaPlayOffMatchupInfo MyNextPlayoffMatch
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ChampionId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastPlayOffRewardSendDate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastPeakArenaSendRewardSeasonID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_CanBetPeakArena(int matchupId, string userId, int jettonNums)
    {
      return this.CanBetPeakArena(matchupId, userId, jettonNums);
    }

    private void __callBase_BetPeakArena(
      int seasonId,
      int matchupId,
      string userId,
      int jettonNums)
    {
      this.BetPeakArena(seasonId, matchupId, userId, jettonNums);
    }

    private void __callBase_SettlePeakArenaMatchupBetReward(int matchupId)
    {
      this.SettlePeakArenaMatchupBetReward(matchupId);
    }

    private bool __callBase_IsTimeForBetted(
      ConfigDataPeakArenaSeasonInfo seaonInfo,
      DateTime currentTime)
    {
      return this.IsTimeForBetted(seaonInfo, currentTime);
    }

    private bool __callBase_IsTimeForBuyingJetton(DateTime currentTime)
    {
      return this.IsTimeForBuyingJetton(currentTime);
    }

    private PeakArenaBetInfo __callBase_GetCurrentSeasonBetInfo()
    {
      return this.GetCurrentSeasonBetInfo();
    }

    private ConfigDataPeakArenaSeasonInfo __callBase_GetCurrentSeasonConfigInfo()
    {
      return this.GetCurrentSeasonConfigInfo();
    }

    private void __callBase_PeakArenaDanUpdate(int dan)
    {
      this.PeakArenaDanUpdate(dan);
    }

    private void __callBase_PeakArenaBattleFinish(RealTimePVPMode mode, bool win, List<int> heroes)
    {
      this.PeakArenaBattleFinish(mode, win, heroes);
    }

    private void __callBase_PeakArenaBattleStart(RealTimePVPMode mode)
    {
      this.PeakArenaBattleStart(mode);
    }

    private int __callBase_IsPeakArenaUnlocked()
    {
      return this.IsPeakArenaUnlocked();
    }

    private int __callBase_IsPeakArenaMatchmakingAvailable(RealTimePVPMode Mode)
    {
      return base.IsPeakArenaMatchmakingAvailable(Mode);
    }

    private void __callBase_OnFlushPeakArena()
    {
      this.OnFlushPeakArena();
    }

    private void __callBase_OnFlushPeakArenaBet()
    {
      this.OnFlushPeakArenaBet();
    }

    private void __callBase_SaveReport(BattleReportHead report, string reportName)
    {
      this.SaveReport(report, reportName);
    }

    private void __callBase_SaveStatistics(int Type, bool Win)
    {
      this.SaveStatistics(Type, Win);
    }

    private List<BattleReportHead> __callBase_GetBattleReport()
    {
      return this.GetBattleReport();
    }

    private int __callBase_CheckAcquireWinsBonus(int BonusId)
    {
      return this.CheckAcquireWinsBonus(BonusId);
    }

    private int __callBase_AcquireWinsBonus(int BonusId)
    {
      return this.AcquireWinsBonus(BonusId);
    }

    private int __callBase_GetConsecutiveWins(RealTimePVPMode Mode)
    {
      return this.GetConsecutiveWins(Mode);
    }

    private int __callBase_GetConsecutiveLosses(RealTimePVPMode Mode)
    {
      return this.GetConsecutiveLosses(Mode);
    }

    private PeakArenaMatchStats __callBase_GetLadderCareerMatchStats()
    {
      return this.GetLadderCareerMatchStats();
    }

    private int __callBase_SetupBattleTeam(PeakArenaBattleTeam BattleTeam)
    {
      return this.SetupBattleTeam(BattleTeam);
    }

    private PeakArenaBattleTeam __callBase_GetBattleTeam()
    {
      return this.GetBattleTeam();
    }

    private void __callBase_Ban(DateTime banTime)
    {
      this.Ban(banTime);
    }

    private bool __callBase_IsBanned(DateTime currentTime)
    {
      return this.IsBanned(currentTime);
    }

    private List<PeakArenaBattleReportNameRecord> __callBase_GetBattleReportNameRecords()
    {
      return this.GetBattleReportNameRecords();
    }

    private void __callBase_AddBattleReportNameRecord(ulong battleReportId, string name)
    {
      this.AddBattleReportNameRecord(battleReportId, name);
    }

    private bool __callBase_ExistBattleReportNameRecord(ulong battleReportId)
    {
      return this.ExistBattleReportNameRecord(battleReportId);
    }

    private string __callBase_GetBattleReportName(ulong battleReportId)
    {
      return this.GetBattleReportName(battleReportId);
    }

    private int __callBase_CanChangeBattleReportName(ulong battleReportId, string name)
    {
      return this.CanChangeBattleReportName(battleReportId, name);
    }

    private int __callBase_ChangeBattleReportName(ulong battleReportId, string name)
    {
      return this.ChangeBattleReportName(battleReportId, name);
    }

    private int __callBase_SelectHeroSkillInPublicPools(int heroId, List<int> skillIds)
    {
      return base.SelectHeroSkillInPublicPools(heroId, skillIds);
    }

    private int __callBase_SelectHiredHeroSoldier(int heroId, int soldierId)
    {
      return this.SelectHiredHeroSoldier(heroId, soldierId);
    }

    private int __callBase_IsBattleTeamSetupDone()
    {
      return this.IsBattleTeamSetupDone();
    }

    private bool __callBase_IsInPreRankingState()
    {
      return this.IsInPreRankingState();
    }

    private bool __callBase_IsLastPreRankingGameJustFinished()
    {
      return this.IsLastPreRankingGameJustFinished();
    }

    private DateTime __callBase_GetSeasonEndTime(int Season)
    {
      return this.GetSeasonEndTime(Season);
    }

    private DateTime __callBase_GetRegularSeasonEndTime(int Season)
    {
      return this.GetRegularSeasonEndTime(Season);
    }

    private ConfigDataPeakArenaDanInfo __callBase_GetDanConfigByScore(
      int Score)
    {
      return this.GetDanConfigByScore(Score);
    }

    private ConfigDataPeakArenaDanInfo __callBase_GetPrevDanConfigByScore(
      int Score)
    {
      return this.GetPrevDanConfigByScore(Score);
    }

    private int __callBase_GetDisplayDan(int Score)
    {
      return this.GetDisplayDan(Score);
    }

    private ConfigDataPeakArenaDanInfo __callBase_GetNextDisplayDanConfig(
      int Score)
    {
      return this.GetNextDisplayDanConfig(Score);
    }

    private int __callBase_GetMatchmakingDan(int Score)
    {
      return this.GetMatchmakingDan(Score);
    }

    private void __callBase_GetScoreThresholdAfterSingleMatch(
      int CurrentScore,
      out int InclusiveMinScore,
      out int InclusiveMaxScore)
    {
      this.GetScoreThresholdAfterSingleMatch(CurrentScore, out InclusiveMinScore, out InclusiveMaxScore);
    }

    private List<int> __callBase_GetAvailableRegularSeasonRewards(int CurrentSeason, int Score)
    {
      return this.GetAvailableRegularSeasonRewards(CurrentSeason, Score);
    }

    private int __callBase_CanGetRegularSeasonRewards(
      int CurrentSeason,
      int Score,
      int RewardIndex)
    {
      return this.CanGetRegularSeasonRewards(CurrentSeason, Score, RewardIndex);
    }

    private void __callBase_SetLiveRoomId(ulong liveRoomId)
    {
      this.SetLiveRoomId(liveRoomId);
    }

    private void __callBase_SetNoticeEnterPlayOffRace(bool notice)
    {
      this.SetNoticeEnterPlayOffRace(notice);
    }

    private bool __callBase_IsOnLiveRoom()
    {
      return this.IsOnLiveRoom();
    }

    private ulong __callBase_GetLiveRoomId()
    {
      return this.GetLiveRoomId();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaComponent m_owner;

      public LuaExportHelper(PeakArenaComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_CanBetPeakArena(int matchupId, string userId, int jettonNums)
      {
        return this.m_owner.__callBase_CanBetPeakArena(matchupId, userId, jettonNums);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_BetPeakArena(
        int seasonId,
        int matchupId,
        string userId,
        int jettonNums)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_SettlePeakArenaMatchupBetReward(int matchupId)
      {
        this.m_owner.__callBase_SettlePeakArenaMatchupBetReward(matchupId);
      }

      public bool __callBase_IsTimeForBetted(
        ConfigDataPeakArenaSeasonInfo seaonInfo,
        DateTime currentTime)
      {
        return this.m_owner.__callBase_IsTimeForBetted(seaonInfo, currentTime);
      }

      public bool __callBase_IsTimeForBuyingJetton(DateTime currentTime)
      {
        return this.m_owner.__callBase_IsTimeForBuyingJetton(currentTime);
      }

      public PeakArenaBetInfo __callBase_GetCurrentSeasonBetInfo()
      {
        return this.m_owner.__callBase_GetCurrentSeasonBetInfo();
      }

      public ConfigDataPeakArenaSeasonInfo __callBase_GetCurrentSeasonConfigInfo()
      {
        return this.m_owner.__callBase_GetCurrentSeasonConfigInfo();
      }

      public void __callBase_PeakArenaDanUpdate(int dan)
      {
        this.m_owner.__callBase_PeakArenaDanUpdate(dan);
      }

      public void __callBase_PeakArenaBattleFinish(
        RealTimePVPMode mode,
        bool win,
        List<int> heroes)
      {
        this.m_owner.__callBase_PeakArenaBattleFinish(mode, win, heroes);
      }

      public void __callBase_PeakArenaBattleStart(RealTimePVPMode mode)
      {
        this.m_owner.__callBase_PeakArenaBattleStart(mode);
      }

      public int __callBase_IsPeakArenaUnlocked()
      {
        return this.m_owner.__callBase_IsPeakArenaUnlocked();
      }

      public int __callBase_IsPeakArenaMatchmakingAvailable(RealTimePVPMode Mode)
      {
        return this.m_owner.__callBase_IsPeakArenaMatchmakingAvailable(Mode);
      }

      public void __callBase_OnFlushPeakArena()
      {
        this.m_owner.__callBase_OnFlushPeakArena();
      }

      public void __callBase_OnFlushPeakArenaBet()
      {
        this.m_owner.__callBase_OnFlushPeakArenaBet();
      }

      public void __callBase_SaveReport(BattleReportHead report, string reportName)
      {
        this.m_owner.__callBase_SaveReport(report, reportName);
      }

      public void __callBase_SaveStatistics(int Type, bool Win)
      {
        this.m_owner.__callBase_SaveStatistics(Type, Win);
      }

      public List<BattleReportHead> __callBase_GetBattleReport()
      {
        return this.m_owner.__callBase_GetBattleReport();
      }

      public int __callBase_CheckAcquireWinsBonus(int BonusId)
      {
        return this.m_owner.__callBase_CheckAcquireWinsBonus(BonusId);
      }

      public int __callBase_AcquireWinsBonus(int BonusId)
      {
        return this.m_owner.__callBase_AcquireWinsBonus(BonusId);
      }

      public int __callBase_GetConsecutiveWins(RealTimePVPMode Mode)
      {
        return this.m_owner.__callBase_GetConsecutiveWins(Mode);
      }

      public int __callBase_GetConsecutiveLosses(RealTimePVPMode Mode)
      {
        return this.m_owner.__callBase_GetConsecutiveLosses(Mode);
      }

      public PeakArenaMatchStats __callBase_GetLadderCareerMatchStats()
      {
        return this.m_owner.__callBase_GetLadderCareerMatchStats();
      }

      public int __callBase_SetupBattleTeam(PeakArenaBattleTeam BattleTeam)
      {
        return this.m_owner.__callBase_SetupBattleTeam(BattleTeam);
      }

      public PeakArenaBattleTeam __callBase_GetBattleTeam()
      {
        return this.m_owner.__callBase_GetBattleTeam();
      }

      public void __callBase_Ban(DateTime banTime)
      {
        this.m_owner.__callBase_Ban(banTime);
      }

      public bool __callBase_IsBanned(DateTime currentTime)
      {
        return this.m_owner.__callBase_IsBanned(currentTime);
      }

      public List<PeakArenaBattleReportNameRecord> __callBase_GetBattleReportNameRecords()
      {
        return this.m_owner.__callBase_GetBattleReportNameRecords();
      }

      public void __callBase_AddBattleReportNameRecord(ulong battleReportId, string name)
      {
        this.m_owner.__callBase_AddBattleReportNameRecord(battleReportId, name);
      }

      public bool __callBase_ExistBattleReportNameRecord(ulong battleReportId)
      {
        return this.m_owner.__callBase_ExistBattleReportNameRecord(battleReportId);
      }

      public string __callBase_GetBattleReportName(ulong battleReportId)
      {
        return this.m_owner.__callBase_GetBattleReportName(battleReportId);
      }

      public int __callBase_CanChangeBattleReportName(ulong battleReportId, string name)
      {
        return this.m_owner.__callBase_CanChangeBattleReportName(battleReportId, name);
      }

      public int __callBase_ChangeBattleReportName(ulong battleReportId, string name)
      {
        return this.m_owner.__callBase_ChangeBattleReportName(battleReportId, name);
      }

      public int __callBase_SelectHeroSkillInPublicPools(int heroId, List<int> skillIds)
      {
        return this.m_owner.__callBase_SelectHeroSkillInPublicPools(heroId, skillIds);
      }

      public int __callBase_SelectHiredHeroSoldier(int heroId, int soldierId)
      {
        return this.m_owner.__callBase_SelectHiredHeroSoldier(heroId, soldierId);
      }

      public int __callBase_IsBattleTeamSetupDone()
      {
        return this.m_owner.__callBase_IsBattleTeamSetupDone();
      }

      public bool __callBase_IsInPreRankingState()
      {
        return this.m_owner.__callBase_IsInPreRankingState();
      }

      public bool __callBase_IsLastPreRankingGameJustFinished()
      {
        return this.m_owner.__callBase_IsLastPreRankingGameJustFinished();
      }

      public DateTime __callBase_GetSeasonEndTime(int Season)
      {
        return this.m_owner.__callBase_GetSeasonEndTime(Season);
      }

      public DateTime __callBase_GetRegularSeasonEndTime(int Season)
      {
        return this.m_owner.__callBase_GetRegularSeasonEndTime(Season);
      }

      public ConfigDataPeakArenaDanInfo __callBase_GetDanConfigByScore(
        int Score)
      {
        return this.m_owner.__callBase_GetDanConfigByScore(Score);
      }

      public ConfigDataPeakArenaDanInfo __callBase_GetPrevDanConfigByScore(
        int Score)
      {
        return this.m_owner.__callBase_GetPrevDanConfigByScore(Score);
      }

      public int __callBase_GetDisplayDan(int Score)
      {
        return this.m_owner.__callBase_GetDisplayDan(Score);
      }

      public ConfigDataPeakArenaDanInfo __callBase_GetNextDisplayDanConfig(
        int Score)
      {
        return this.m_owner.__callBase_GetNextDisplayDanConfig(Score);
      }

      public int __callBase_GetMatchmakingDan(int Score)
      {
        return this.m_owner.__callBase_GetMatchmakingDan(Score);
      }

      public void __callBase_GetScoreThresholdAfterSingleMatch(
        int CurrentScore,
        out int InclusiveMinScore,
        out int InclusiveMaxScore)
      {
        this.m_owner.__callBase_GetScoreThresholdAfterSingleMatch(CurrentScore, out InclusiveMinScore, out InclusiveMaxScore);
      }

      public List<int> __callBase_GetAvailableRegularSeasonRewards(int CurrentSeason, int Score)
      {
        return this.m_owner.__callBase_GetAvailableRegularSeasonRewards(CurrentSeason, Score);
      }

      public int __callBase_CanGetRegularSeasonRewards(
        int CurrentSeason,
        int Score,
        int RewardIndex)
      {
        return this.m_owner.__callBase_CanGetRegularSeasonRewards(CurrentSeason, Score, RewardIndex);
      }

      public void __callBase_SetLiveRoomId(ulong liveRoomId)
      {
        this.m_owner.__callBase_SetLiveRoomId(liveRoomId);
      }

      public void __callBase_SetNoticeEnterPlayOffRace(bool notice)
      {
        this.m_owner.__callBase_SetNoticeEnterPlayOffRace(notice);
      }

      public bool __callBase_IsOnLiveRoom()
      {
        return this.m_owner.__callBase_IsOnLiveRoom();
      }

      public ulong __callBase_GetLiveRoomId()
      {
        return this.m_owner.__callBase_GetLiveRoomId();
      }
    }
  }
}
