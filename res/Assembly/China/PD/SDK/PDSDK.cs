﻿// Decompiled with JetBrains decompiler
// Type: PD.SDK.PDSDK
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Lua;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace PD.SDK
{
  [HotFix]
  public class PDSDK : MonoBehaviour
  {
    public bool isDebug = true;
    public bool isVerfy = true;
    public string promotingPayGoodsRegisterID = string.Empty;
    public string stringToEdit = string.Empty;
    private bool m_isRequestPermissionReturn = true;
    public bool IsShowCommandUI;
    public static JsonData goodlistjson;
    public GameObject CommondUIPrefab;
    public bool isExitSuccess;
    public bool m_isCallWebView;
    public const string m_loginTypeLine = "line";
    public const string m_loginTypeGameCenter = "gamecenter";
    public const string m_loginTypeZlongame = "zlongame";
    public const string m_loginTypeGuest = "guest";
    public const string m_loginTypeFacebook = "facebook";
    public const string m_loginTypeGoogle = "google";
    public const string m_loginTypeTwitter = "twitter";
    private static bool isLogin;
    private static bool _isLogining;
    private static bool _isLogouting;
    private static bool isInit;
    private static bool _isIosReview;
    private static PDSDK _instance;
    public static Action<LoginSuccessMsg> m_eventLoginSuccess;
    public static Action<LoginSuccessMsg> m_eventOnSwitchUserSuccess;
    public static Action<string> m_eventLoginFailed;
    public static Action m_eventLogoutSuccess;
    public static Action m_eventInitSuccess;
    public static Action m_eventInitFailed;
    public static Action m_eventDoQuestionSucceed;
    public static Action m_eventAllowToBuySubscription;
    public static Action m_eventDoQuestionFailed;
    public static Action<bool> m_eventOnGetProductsListAck;
    public static Action m_eventOnPaySuccess;
    public static Action m_eventOnPayFailed;
    public static Action m_eventOnPayCancel;
    public Action<string> m_eventOnSDKPromotingPaySuccess;
    public static Action<string> m_eventQRLoginSuccess;
    public static Action<string> m_eventQRLoginFailed;
    public static Action<string> m_eventQRLoginCancel;
    public static Action m_eventOnLowMemoryWarning;
    public static Action m_eventOnBindGuestSuccess;
    private const string m_appKey = "1486458782785";
    private const string m_downloadUrl = "http://www.zlongame.com/html/dispatch/download.html";
    public const string PDChannelGooglePay = "3110021001";
    public const string PDAndroidChannelID = "2010011001";
    public const string PDYingYongBaoChannelID = "2010571001";
    public const string PDOppoChannelID = "2010071001";
    [DoNotToLua]
    private PDSDK.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_get_DownloadClientURL_hotfix;
    private LuaFunction m_PerformIEnumeratorAction_hotfix;
    private LuaFunction m_AsyncRequestPermissionStringString_hotfix;
    private LuaFunction m_requestPermissionStringString_hotfix;
    private LuaFunction m_checkPermissionStringString_hotfix;
    private LuaFunction m_noticeCenterStateInt32String_hotfix;
    private LuaFunction m_onWebViewOpen_hotfix;
    private LuaFunction m_onWebViewClose_hotfix;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_WebInvestigation_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_LoginString_hotfix;
    private LuaFunction m_LoginWithTypeStringString_hotfix;
    private LuaFunction m_LogoutString_hotfix;
    private LuaFunction m_StartGameString_hotfix;
    private LuaFunction m_GetProductsList_hotfix;
    private LuaFunction m_DoPayWithLimitInt32StringInt32StringStringDoubleStringString_hotfix;
    private LuaFunction m_DoPromotingPayStringInt32StringStringDoubleStringString_hotfix;
    private LuaFunction m_onSDKPromotingPaySuccessString_hotfix;
    private LuaFunction m_onSDKPromotingPayFailedString_hotfix;
    private LuaFunction m_onSDKPromotingPayCancelString_hotfix;
    private LuaFunction m_DoPayStringInt32StringStringDoubleStringString_hotfix;
    private LuaFunction m_DoPromotingWithLimitPayInt32StringInt32StringStringDoubleStringString_hotfix;
    private LuaFunction m_doAddLocalPushStringStringStringStringString_hotfix;
    private LuaFunction m_doFirstScreen_hotfix;
    private LuaFunction m_SwitchUser_hotfix;
    private LuaFunction m_userCenter_hotfix;
    private LuaFunction m_exit_hotfix;
    private LuaFunction m_GetPustToken_hotfix;
    private LuaFunction m_GetDeviceID_hotfix;
    private LuaFunction m_GetBattery_hotfix;
    private LuaFunction m_OpenInvestigationInt32_hotfix;
    private LuaFunction m_PathOrder_hotfix;
    private LuaFunction m_ShowPayHelp_hotfix;
    private LuaFunction m_printGameEventLogStringString_hotfix;
    private LuaFunction m_doSetExtDataStringString_hotfix;
    private LuaFunction m_doStartQRLoginString_hotfix;
    private LuaFunction m_getChannelID_hotfix;
    private LuaFunction m_doSaveImageToPhotoLibraryString_hotfix;
    private LuaFunction m_doQQVIP_hotfix;
    private LuaFunction m_callWebViewStringInt32Int32StringString_hotfix;
    private LuaFunction m_clearLocalNotifications_hotfix;
    private LuaFunction m_callCustomerServiceWeb_hotfix;
    private LuaFunction m_callCustomerService_hotfix;
    private LuaFunction m_doshareString_hotfix;
    private LuaFunction m_doOpenRequestReview_hotfix;
    private LuaFunction m_UserLoginType_hotfix;
    private LuaFunction m_doUnBind_hotfix;
    private LuaFunction m_doBindGuestString_hotfix;
    private LuaFunction m_setSDKLangueString_hotfix;
    private LuaFunction m_doStartRecord_hotfix;
    private LuaFunction m_doStopRecord_hotfix;
    private LuaFunction m_doBoradcast_hotfix;
    private LuaFunction m_doGetImagePathInt32Int32_hotfix;
    private LuaFunction m_getSysCountry_hotfix;
    private LuaFunction m_getSysLangue_hotfix;
    private LuaFunction m_doOpenFBFanPage_hotfix;
    private LuaFunction m_getECID_hotfix;
    private LuaFunction m_isEuCountry_hotfix;
    private LuaFunction m_onLoginSuccessString_hotfix;
    private LuaFunction m_onLoginFailedString_hotfix;
    private LuaFunction m_onLoginCancelString_hotfix;
    private LuaFunction m_onQRLoginSuccessString_hotfix;
    private LuaFunction m_onQRLoginFailedString_hotfix;
    private LuaFunction m_onQRLoginCancelString_hotfix;
    private LuaFunction m_onLogoutSuccessString_hotfix;
    private LuaFunction m_onLogoutFailedString_hotfix;
    private LuaFunction m_onLogoutCancelString_hotfix;
    private LuaFunction m_onGetProductsListSuccessString_hotfix;
    private LuaFunction m_onGetProductsListFailedString_hotfix;
    private LuaFunction m_onPaySuccessString_hotfix;
    private LuaFunction m_onPayFailedString_hotfix;
    private LuaFunction m_onPayCancelString_hotfix;
    private LuaFunction m_onGetPayHistorySuccessString_hotfix;
    private LuaFunction m_onGetPayHistoryFailedString_hotfix;
    private LuaFunction m_onInitSuccessString_hotfix;
    private LuaFunction m_onInitFailedString_hotfix;
    private LuaFunction m_onswitchUserSuccessString_hotfix;
    private LuaFunction m_onswitchUserFailedString_hotfix;
    private LuaFunction m_onswitchUserCancelString_hotfix;
    private LuaFunction m_onVerifyTokenSuccessString_hotfix;
    private LuaFunction m_onVerifyTokenFailedString_hotfix;
    private LuaFunction m_onDoSetExtDataSuccessString_hotfix;
    private LuaFunction m_onDoSetExtDataFailedString_hotfix;
    private LuaFunction m_onExitSuccessString_hotfix;
    private LuaFunction m_onExitFailedString_hotfix;
    private LuaFunction m_onMemoryWarningString_hotfix;
    private LuaFunction m_onShareSuccessString_hotfix;
    private LuaFunction m_onShareFailedString_hotfix;
    private LuaFunction m_onShareCancelString_hotfix;
    private LuaFunction m_onRecordStartSuccessString_hotfix;
    private LuaFunction m_onRecordStartFailString_hotfix;
    private LuaFunction m_onRecordStopSuccessString_hotfix;
    private LuaFunction m_onRecordStopFailString_hotfix;
    private LuaFunction m_onBoradcastSuccessString_hotfix;
    private LuaFunction m_onBoradcastFailString_hotfix;
    private LuaFunction m_onDoQuestionSuccessString_hotfix;
    private LuaFunction m_onDoQuestionFailedString_hotfix;
    private LuaFunction m_oncallWebViewSuccessString_hotfix;
    private LuaFunction m_oncallWebViewFailedString_hotfix;
    private LuaFunction m_oncallWebViewCancelString_hotfix;
    private LuaFunction m_onDoSaveImageToPhotoLibrarySuccessString_hotfix;
    private LuaFunction m_onDoSaveImageToPhotoLibraryFailedString_hotfix;
    private LuaFunction m_onBindGuestSuccessString_hotfix;
    private LuaFunction m_onBindGuestFailedString_hotfix;
    private LuaFunction m_onBindGuestCancelString_hotfix;
    private LuaFunction m_onLangueChangeSuccessString_hotfix;
    private LuaFunction m_onLangueChangeFailedString_hotfix;
    private LuaFunction m_onUnBindSuccessString_hotfix;
    private LuaFunction m_onUnBindFailedString_hotfix;
    private LuaFunction m_onUnBindCancelString_hotfix;
    private LuaFunction m_onRequestPermissionSuccessString_hotfix;
    private LuaFunction m_onRequestPermissionFailedString_hotfix;
    private LuaFunction m_onNoticeCenterStateSuccessString_hotfix;
    private LuaFunction m_onNoticeCenterStateFailedString_hotfix;
    private LuaFunction m_showAndroidToastString_hotfix;
    private LuaFunction m_verifyTokenString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PDSDK()
    {
    }

    public static bool IsLogin
    {
      get
      {
        return PDSDK.isLogin;
      }
    }

    public static bool IsInit
    {
      get
      {
        return PDSDK.isInit;
      }
    }

    public static bool IsLogouting
    {
      get
      {
        return PDSDK._isLogouting;
      }
    }

    public static bool IsIosReview
    {
      get
      {
        return PDSDK._isIosReview;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsGooglePlayChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    public static PDSDK Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if ((UnityEngine.Object) PDSDK._instance == (UnityEngine.Object) null)
          Debug.LogError("PDSDK _instance is null.");
        return PDSDK._instance;
      }
    }

    public static bool IsIosLoginWindowsPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DoCoroutine(IEnumerator coroutine, Action onComplete = null)
    {
      PDSDK._instance.StartCoroutine(PDSDK._instance.Perform(coroutine, onComplete));
    }

    public string DownloadClientURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Perform(IEnumerator coroutine, Action onComplete = null)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && this.TryInitHotFix("") && ((LuaVar) this.m_PerformIEnumeratorAction_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed()))
        return (IEnumerator) this.m_PerformIEnumeratorAction_hotfix.call((object) this, (object) coroutine, (object) onComplete);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new PDSDK.\u003CPerform\u003Ec__Iterator0()
      {
        onComplete = onComplete,
        coroutine = coroutine,
        \u0024this = this
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator AsyncRequestPermission(string permission, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool requestPermission(string permission, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool checkPermission(string permission, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void noticeCenterState(int mode, string noticeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onWebViewOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onWebViewClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && this.TryInitHotFix("") && ((LuaVar) this.m_Awake_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed()))
      {
        this.m_Awake_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        PDSDK._instance = this;
        if (!this.IsShowCommandUI || (UnityEngine.Object) null == (UnityEngine.Object) this.CommondUIPrefab)
          return;
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.CommondUIPrefab);
        gameObject.transform.parent = this.transform;
        gameObject.transform.localScale = Vector3.one;
        gameObject.transform.SetSiblingIndex(0);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WebInvestigation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && this.TryInitHotFix("") && ((LuaVar) this.m_Init_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed()))
      {
        this.m_Init_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (PDSDK.isInit)
          return;
        Debug.Log("Windows PDSDK Init.");
        PDSDK_PC.Init();
        BuglyAgent.EnableExceptionHandler();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Login(string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoginWithType(string login_type, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Logout(string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartGame(string gameparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetProductsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPayWithLimit(
      int limitLevel,
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPromotingPay(
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onSDKPromotingPaySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onSDKPromotingPayFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onSDKPromotingPayCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPay(
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPromotingWithLimitPay(
      int limitLevel,
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doAddLocalPush(
      string Title,
      string Content,
      string Date,
      string Hour,
      string Min)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doFirstScreen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void userCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void exit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPustToken()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetDeviceID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattery()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenInvestigation(int enqId = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PathOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPayHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void printGameEventLog(string eventID, string remark = "")
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && this.TryInitHotFix("") && ((LuaVar) this.m_printGameEventLogStringString_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed()))
      {
        this.m_printGameEventLogStringString_hotfix.call((object) this, (object) eventID, (object) remark);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (Application.isEditor)
          return;
        PDSDK_PC.PrintGameEventLog(eventID, remark);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string doSetExtData(string data, string type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doStartQRLogin(string customParams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getChannelID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doSaveImageToPhotoLibrary(string data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doQQVIP()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void callWebView(
      string title,
      int fullscreen_flag,
      int title_flag,
      string action,
      string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void clearLocalNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void callCustomerServiceWeb()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void callCustomerService()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doshare(string data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doOpenRequestReview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UserLoginType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doUnBind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doBindGuest(string bind_type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void setSDKLangue(string langue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doStartRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doStopRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doBoradcast()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doGetImagePath(int nType, int fromType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getSysCountry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getSysLangue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doOpenFBFanPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getECID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool isEuCountry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onQRLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onQRLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onQRLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLogoutSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLogoutFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLogoutCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetProductsListSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetProductsListFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onPaySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onPayFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onPayCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetPayHistorySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetPayHistoryFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onInitSuccess(string msg)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && this.TryInitHotFix("") && ((LuaVar) this.m_onInitSuccessString_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed()))
      {
        this.m_onInitSuccessString_hotfix.call((object) this, (object) msg);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        Debug.Log("PDSDK.onInitSuccess " + msg);
        if (!string.IsNullOrEmpty(msg))
        {
          JsonData jsonData = JsonMapper.ToObject(msg);
          if (((IDictionary) jsonData).Contains((object) "isReview"))
            PDSDK._isIosReview = jsonData["isReview"].ToString() == "1";
        }
        Debug.Log("PDSDK.onInitSuccess _isIosReview=" + PDSDK._isIosReview.ToString());
        PDSDK.isInit = true;
        if (PDSDK.m_eventInitSuccess == null)
          return;
        PDSDK.m_eventInitSuccess();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onInitFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onswitchUserSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onswitchUserFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onswitchUserCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onVerifyTokenSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onVerifyTokenFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSetExtDataSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSetExtDataFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onExitSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onExitFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onMemoryWarning(string recode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onShareSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onShareFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onShareCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStartSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStartFail(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStopSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStopFail(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onBoradcastSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onBoradcastFail(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoQuestionSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoQuestionFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void oncallWebViewSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void oncallWebViewFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void oncallWebViewCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSaveImageToPhotoLibrarySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSaveImageToPhotoLibraryFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onBindGuestSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onBindGuestFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onBindGuestCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onLangueChangeSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onLangueChangeFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onUnBindSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onUnBindFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onUnBindCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onRequestPermissionSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onRequestPermissionFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onNoticeCenterStateSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onNoticeCenterStateFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void showAndroidToast(string info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void verifyToken(string info)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string IOSChannelID
    {
      get
      {
        return "1010031002";
      }
    }

    [DoNotToLua]
    public PDSDK.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      if (this.m_hotfixState != ObjectLuaHotFixState.Uninit)
        return this.m_hotfixState == ObjectLuaHotFixState.InitAvialable;
      bool flag = LuaManager.TryInitHotfixForObj((object) this, luaModuleName, typeof (PDSDK));
      this.m_hotfixState = !flag ? ObjectLuaHotFixState.InitUnavialable : ObjectLuaHotFixState.InitAvialable;
      return flag;
    }

    public class LuaExportHelper
    {
      private PDSDK m_owner;

      public LuaExportHelper(PDSDK owner)
      {
        this.m_owner = owner;
      }

      public static bool isLogin
      {
        get
        {
          return PDSDK.isLogin;
        }
        set
        {
          PDSDK.isLogin = value;
        }
      }

      public static bool _isLogining
      {
        get
        {
          return PDSDK._isLogining;
        }
        set
        {
          PDSDK._isLogining = value;
        }
      }

      public static bool _isLogouting
      {
        get
        {
          return PDSDK._isLogouting;
        }
        set
        {
          PDSDK._isLogouting = value;
        }
      }

      public static bool isInit
      {
        get
        {
          return PDSDK.isInit;
        }
        set
        {
          PDSDK.isInit = value;
        }
      }

      public static bool _isIosReview
      {
        get
        {
          return PDSDK._isIosReview;
        }
        set
        {
          PDSDK._isIosReview = value;
        }
      }

      public static PDSDK _instance
      {
        get
        {
          return PDSDK._instance;
        }
        set
        {
          PDSDK._instance = value;
        }
      }

      public bool m_isRequestPermissionReturn
      {
        get
        {
          return this.m_owner.m_isRequestPermissionReturn;
        }
        set
        {
          this.m_owner.m_isRequestPermissionReturn = value;
        }
      }

      public static string m_appKey
      {
        get
        {
          return "1486458782785";
        }
      }

      public static string m_downloadUrl
      {
        get
        {
          return "http://www.zlongame.com/html/dispatch/download.html";
        }
      }

      public IEnumerator Perform(IEnumerator coroutine, Action onComplete)
      {
        return this.m_owner.Perform(coroutine, onComplete);
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }

      public void onQRLoginSuccess(string msg)
      {
        this.m_owner.onQRLoginSuccess(msg);
      }

      public void onQRLoginFailed(string msg)
      {
        this.m_owner.onQRLoginFailed(msg);
      }

      public void onQRLoginCancel(string msg)
      {
        this.m_owner.onQRLoginCancel(msg);
      }

      public void onBindGuestSuccess(string msg)
      {
        this.m_owner.onBindGuestSuccess(msg);
      }

      public void onBindGuestFailed(string msg)
      {
        this.m_owner.onBindGuestFailed(msg);
      }

      public void onBindGuestCancel(string msg)
      {
        this.m_owner.onBindGuestCancel(msg);
      }

      public void onLangueChangeSuccess(string msg)
      {
        this.m_owner.onLangueChangeSuccess(msg);
      }

      public void onLangueChangeFailed(string msg)
      {
        this.m_owner.onLangueChangeFailed(msg);
      }

      public void onUnBindSuccess(string msg)
      {
        this.m_owner.onUnBindSuccess(msg);
      }

      public void onUnBindFailed(string msg)
      {
        this.m_owner.onUnBindFailed(msg);
      }

      public void onUnBindCancel(string msg)
      {
        this.m_owner.onUnBindCancel(msg);
      }

      public void onRequestPermissionSuccess(string msg)
      {
        this.m_owner.onRequestPermissionSuccess(msg);
      }

      public void onRequestPermissionFailed(string msg)
      {
        this.m_owner.onRequestPermissionFailed(msg);
      }
    }
  }
}
