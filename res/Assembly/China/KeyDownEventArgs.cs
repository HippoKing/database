﻿// Decompiled with JetBrains decompiler
// Type: KeyDownEventArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using UnityEngine;

public class KeyDownEventArgs : EventArgs
{
  private readonly KeyCode _keyCode;

  public KeyDownEventArgs(KeyCode keyCode)
  {
    this._keyCode = keyCode;
  }

  public KeyCode KeyCode
  {
    get
    {
      return this._keyCode;
    }
  }
}
