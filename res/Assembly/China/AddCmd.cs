﻿// Decompiled with JetBrains decompiler
// Type: AddCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;

public class AddCmd : IDebugCmd
{
  private const string _NAME = "intadd";
  private const string _DESC = "intadd [int] [int]: add two int num.";
  private const string _SCHEMA = "i i";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "intadd [int] [int]: add two int num.";
  }

  public string GetName()
  {
    return "intadd";
  }
}
