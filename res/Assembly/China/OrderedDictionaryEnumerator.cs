﻿// Decompiled with JetBrains decompiler
// Type: OrderedDictionaryEnumerator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal class OrderedDictionaryEnumerator : IDictionaryEnumerator, IEnumerator
{
  private IEnumerator<KeyValuePair<string, JsonData>> list_enumerator;

  public OrderedDictionaryEnumerator(
    IEnumerator<KeyValuePair<string, JsonData>> enumerator)
  {
    this.list_enumerator = enumerator;
  }

  public object Current
  {
    get
    {
      return (object) this.Entry;
    }
  }

  public DictionaryEntry Entry
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      KeyValuePair<string, JsonData> current = this.list_enumerator.Current;
      return new DictionaryEntry((object) current.Key, (object) current.Value);
    }
  }

  public object Key
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public object Value
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool MoveNext()
  {
    return this.list_enumerator.MoveNext();
  }

  public void Reset()
  {
    this.list_enumerator.Reset();
  }
}
